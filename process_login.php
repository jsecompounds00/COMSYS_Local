<?php
	//Start session
	session_start();
 
	//Include database connection details
	require_once('/include/database_connect.php');
 
	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
 
 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_login.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		//Sanitize the POST values
		$username = $_POST['username'];
		$pword = $_POST['password'];
		// $password = encrypt($pword, ENCRYPTION_KEY);
		$password = convert_uuencode($pword);
		
		//Input Validations
		if($username == '') {
			$errmsg_arr[] = '* Username is missing.';
			$errflag = true;
		}
		if($password == '') {
			$errmsg_arr[] = '* Password is missing.';
			$errflag = true;
		}

		// If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("location: login.php");
			exit();
		}
		//Create query
		$qry = mysqli_prepare($db, "CALL sp_Login_Query(?, ?)");
		mysqli_stmt_bind_param($qry, 'ss', $username, $password);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
	 	$processError = mysqli_error($db);

	 	if ( !empty($processError) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_login.php'.'</td><td>'.$processError.' near line 59.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			//Check whether the query was successful or not
			if($result) {
				if(mysqli_num_rows($result) > 0) {
					//Login Successful
					session_regenerate_id();
					$member = mysqli_fetch_assoc($result);
					$_SESSION['SESS_USER_ID'] = $member['user_id'];
					$_SESSION['SESS_USER_NAME'] = $member['username'];
					$_SESSION['SESS_PASS_WORD'] = $member['userpass'];
					$_SESSION['SESS_EMAIL'] = $member['email'];
					$_SESSION['SESS_ROLE_ID'] = '0,'.$member['role_id'];
					$_SESSION['SESS_Permit_ID'] = '0,'.$member['perm_id'];
					$_SESSION['SESS_DEPARTMENT_ID'] = $member['department_id'];
					$_SESSION['SESS_DEPARTMENT_ID_2'] = $member['department_id_2'];
					$_SESSION['SESS_FIRST_NAME'] = $member['fname'];
					$_SESSION['SESS_LAST_NAME'] = $member['lname'];
					$_SESSION['SESS_COMPOUNDS'] = $member['compounds'];
					$_SESSION['SESS_PIPES'] = $member['pipes'];
					$_SESSION['SESS_CORPORATE'] = $member['corporate'];
					$_SESSION['SESS_RoleID1'] = $member['RoleID2'];
					$_SESSION['SESS_RoleID2'] = $member['RoleID3'];
					$_SESSION['SESS_RoleID3'] = $member['RoleID4'];

					$_SESSION['SUCCESS']  = 'Successfully Login.';
					session_write_close();
					header("location: comsys.php");
					exit();
				}else {
					//Login failed
					$errmsg_arr[] = '* Either username or password is incorrect.';
					$errflag = true;
					if($errflag) {
						$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
						session_write_close();
						header("location: login.php");
						exit();
					}
				}
			}else {
				echo mysql_error().'<br>';
				die("Query failed");
			}
		}
	}
			require("include/database_close.php");
?>