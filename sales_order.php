<html>
	<head>
		<title>Sales Order - Home</title>
		<?php
			require("include/database_connect.php");

			$so_no = ($_GET['so_no'] ? "%".$_GET['so_no']."%" : "");
			$so_date = ($_GET['so_date'] ? $_GET['so_date'] : NULL);
			$so_type = ($_GET['so_type'] ? $_GET['so_type'] : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
		<script src="js/datetimepicker_css.js"></script>
		<script src="js/jscript.js"></script>
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> <!--   -->
	</head>
	<body>
		<?php
			require("/include/header.php");
			require("/include/unset_value.php");

			if( $_SESSION['sales_order'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["so_no"] = $_GET["so_no"];
			$_SESSION["so_date"] = $_GET["so_date"];
			$_SESSION["so_type"] = $_GET["so_type"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Sales Order </h3> </span>

			<div class="search_box">
				<form method="get" action="sales_order.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table>
						<tr>
							<td>
								<label>No.:</label>
							</td>
							<td>
								<input type="text" name="so_no" value="<?php echo htmlspecialchars($_GET["so_no"]); ?>">
							</td>
							<td>
								<label>Date:</label>
							</td>
							<td>
								<input type="text" name="so_date" id="so_date" value="<?php echo htmlspecialchars($_GET["so_date"]);?>">
							</td>
							<td>
								<img src="js/cal.gif" onclick="javascript:NewCssCal('so_date')" style="cursor:pointer" name="picker" />
							</td>
							<td>
								<label>Type:</label>
							</td>
							<td>
								<select name="so_type">
									<option value="ALL">ALL</option>
									<option value="COML" <?php echo ( $so_type == "COML" ? "selected" : "" );?>>Local</option>
									<option value="EX" <?php echo ( $so_type == "EX" ? "selected" : "" );?>>Export</option>
								</select>
							</td>
								
							<td>
								<input type="submit" value="Search">
							</td>

							<td>
								<?php
									if(array_search(100, $session_Permit)){
								?>
										<input type="button" name="btnAddSO" value="New SO" onclick="location.href='new_sales_order.php?id=0'">
								<?php
										$_SESSION["add_sales_order"] = true;
									}else{
										unset($_SESSION["add_sales_order"]);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>sales_order.php"."</td><td>".$error." near line 42.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Sales_Home(?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, "sss", $so_no, $so_date, $so_type);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "sales_order.php"; 	//your file name  (the name of this file)
					require("include/paginate_sales.php");

					$qry = mysqli_prepare($db, "CALL sp_Sales_Home(?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sssii", $so_no, $so_date, $so_type, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>sales_order.php"."</td><td>".$processError." near line 63.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
						    <th>SO Tpe</th>
						    <th>SO Number</th>
						    <th>SO Date</th>
						    <th>Customer</th>
						    <th></th>
						    <th></th>
						</tr>
						<?php
							$i = 0;
							while( $row = mysqli_fetch_assoc( $result ) )
							{
								$SOID	= $row['SOID'];
								$SOType = $row['SOType'];
								$SONumber = $row['SONumber'];
								$SODate = $row['SODate'];
								$Customer = $row['Customer'];
								$closed = $row['closed'];
								$closed_reason = $row['closed_reason'];
						?>
								<tr>
									<td> <?php echo $SOType; ?> </td>
									<td> <?php echo $SONumber; ?> </td>
									<td> <?php echo $SODate; ?> </td>
									<td> <?php echo $Customer; ?> </td>
									<td>
										<?php
											if(array_search(84, $session_Permit)){
										?>
												<input type='button' value='Edit' onclick="location.href='new_sales_order.php?id=<?php echo $SOID;?>'">
										<?php
												$_SESSION['edit_sales_order'] = true;
											}else{
												unset($_SESSION['edit_sales_order']);
											}
										?>
									</td>
									<td>
										<?php
											if(array_search(101, $session_Permit)){
												if ( $closed == 1 ){
										?>
													<!-- <input type='checkbox' name='closed[]' id='closed<?php echo $i;?>' value='1' checked disabled> -->
													<img src='images/yes.png' height='20'>
													<label><b> Closed : 
														<?php echo $closed_reason; ?>
													</b></label>
											<?php
												}else{
											?>
													<input type='checkbox' value='1' name='closed[]' id='closed<?php echo $i;?>' onchange="closedBox('<?php echo $SOID;?>', '<?php echo $SOType;?>', '<?php echo $SONumber;?>', '<?php echo $i;?>')">
														<label for='closed<?php echo $i;?>'>Close</label>
										<?php
												}
												$_SESSION['edit_sales_order'] = true;
											}else{
												unset($_SESSION['edit_sales_order']);
											}
										?>		
									</td>
								</tr>
						<?php
								$i++;
							}
						?>
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>