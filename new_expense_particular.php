<html>
<head>
	<title>Particulars Specifications</title>
	<script src="js/expense_js.js"></script>  

	<?php
		require("/include/database_connect.php");

		if($errno)
		{
			$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_particular_specifications.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
		}
		else
		{
			$expenseID = $_GET['id'];

			if($expenseID)
				{
					$qry = mysqli_prepare($db, "CALL sp_Budget_Expense_Code_Particulars_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $expenseID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_particular_specifications.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						$particularID = array();
						$particular = array();
						$price = array();
						$created_at = array();
						$created_id = array();
						$uom_id = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$particularID[] = $row['ParticularsID'];
							$expenseID = $row['ExpenseID'];
							$description = $row['description'];
							$particular[] = $row['particulars'];
							$price[] = $row['unit_price'];
							$created_at[] = $row['CreatedAT'];
							$created_id[] = $row['CreatedID'];
							$uom_id[] = $row['uom_id'];
						}
					}
					$db->next_result();
					$result->close();

					$count = count($particularID);

				############ .............
					$qryPI = "SELECT id from comsys.expense_code";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_particular_specifications.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();
				############ .............
					if( !in_array($expenseID, $id) ){ //, TRUE
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_particular_specifications.php</td><td>The user tries to edit a non-existing expense_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "";
				}
				else
				{

					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_specifications.php</td><td>The user tries to edit a non-existing specification_id.</td></tr>', 3, "errors.php");
					header("location: error_message.html");

				}
		}

	?>
</head>
<body>
	<form method='post' action='process_new_expense_particular.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/expense_particular_init_value.php");

				// if( $_SESSION['expense_code_particulars'] == false) 
				// {
				// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
				// 	session_write_close();
				// 	header("Location:comsys.php");
				// 	exit();
				// }
			?>

			<div class="wrapper">

				<span> <h3> Specifications for <?php echo $description;?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>
					<table class="child_tables_form">
						<tr>
							<th>Particulars Specifications</th>
							<th>UOM</th>
							<th>Unit Price</th>
						</tr>
						<?php 
							$i = 0;
							if ( $count == 1 && is_null( $particularID[$i] ) ){
								$count = 5;
								for ( $i = 0; $i < 10; $i++ ){
						?>
								<tr>
									<td>	
										<input type='hidden' name='hidParID[]' value='0'>
										<input type='text' name='txtDescription[]' size='50' value='<?php echo ( $initEXPParticulars[$i] ); ?>'>
									</td>
									<td>
										<select name="sltUOM[]">
											<?php 
												$qryST = "CALL sp_UOM_Dropdown(1)";
												$resultST = mysqli_query($db, $qryST);
												$processError1 = mysqli_error($db);

												if(!empty($processError1))
												{
													error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_expense_code.php'.'</td><td>'.$processError1.' near line 115.</td></tr>', 3, "errors.php");
													header("location: error_message.html");
												}
												else
												{
													while($row = mysqli_fetch_assoc($resultST))
													{
														$uomID = $row['id'];
														$uCode = $row['code'];
														
														if ( $uomID == $initEXPUOM[$i] ){
															echo "<option value='".$uomID."' selected>".$uCode."</option>";
														}else{
															echo "<option value='".$uomID."'>".$uCode."</option>";
														}
													}

													$db->next_result();
													$resultST->close();
												}
											?>
										</select>
									</td>
									<td>
										<input type='text' name='txtUnitPrice[]' size='20' value='<?php echo ( $initEXPUnitPrice[$i] ); ?>'>
									</td>
									<input type='hidden' name='hidCreatedAt[]' value='<?php echo date('Y-m-d H:i:s'); ?>'>
									<input type='hidden' name='hidCreatedID[]' value='<?php echo $_SESSION['SESS_USER_ID']; ?>'>
								</tr>
						<?php
								}		
							}else{
								for ( $i = 0; $i < $count; $i++ ){
						?>
								<tr>	
									<?php
										if ( $particularID[$i] ){
									?>
											<td>
												<input type='hidden' name='hidParID[]' value="<?php echo $particularID[$i];?>">
												<input type='text' name='txtDescription[]' size='50' value="<?php echo $particular[$i];?>">
											</td>
											<td>
												<select name="sltUOM[]">
													<?php 
														$qryST = "CALL sp_UOM_Dropdown(1)";
														$resultST = mysqli_query($db, $qryST);
														$processError1 = mysqli_error($db);

														if(!empty($processError1))
														{
															error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_expense_code.php'.'</td><td>'.$processError1.' near line 115.</td></tr>', 3, "errors.php");
															header("location: error_message.html");
														}
														else
														{
															while($row = mysqli_fetch_assoc($resultST))
															{
																$uomID = $row['id'];
																$uCode = $row['code'];
																
																if ( $uomID == $uom_id[$i] ){
																	echo "<option value='".$uomID."' selected>".$uCode."</option>";
																}else{
																	echo "<option value='".$uomID."'>".$uCode."</option>";
																}
															}

															$db->next_result();
															$resultST->close();
														}
													?>
												</select>
											</td>
											<td>
												<input type='text' name='txtUnitPrice[]' size='20' value="<?php echo $price[$i];?>">
											</td>
									<?php
										}else{
									?>
											<td>
												<input type='hidden' name='hidParID[]' value="0">
												<input type='text' name='txtDescription[]' size='50' value='<?php echo ( $initEXPParticulars[$i] ); ?>'>
											</td>
											<td>
												<select name="sltUOM[]">
													<?php 
														$qryST = "CALL sp_UOM_Dropdown(1)";
														$resultST = mysqli_query($db, $qryST);
														$processError1 = mysqli_error($db);

														if(!empty($processError1))
														{
															error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_expense_code.php'.'</td><td>'.$processError1.' near line 115.</td></tr>', 3, "errors.php");
															header("location: error_message.html");
														}
														else
														{
															while($row = mysqli_fetch_assoc($resultST))
															{
																$uomID = $row['id'];
																$uCode = $row['code'];
																
																if ( $uomID == $initEXPUOM[$i] ){
																	echo "<option value='".$uomID."' selected>".$uCode."</option>";
																}else{
																	echo "<option value='".$uomID."'>".$uCode."</option>";
																}
															}

															$db->next_result();
															$resultST->close();
														}
													?>
												</select>
											</td>
											<td>
												<input type='text' name='txtUnitPrice[]' size='20' value='<?php echo ( $initEXPUnitPrice[$i] ); ?>'>
											</td>
									<?php
										}
										if( $particularID[$i] ){
									?>
											<input type='hidden' name='hidCreatedAt[]' value='<?php echo $created_at[$i]; ?>'>
											<input type='hidden' name='hidCreatedID[]' value='<?php echo $created_id[$i]; ?>'>
									<?php		
										}else{
									?>
											<input type='hidden' name='hidCreatedAt[]' value='<?php echo date('Y-m-d H:i:s'); ?>'>
											<input type='hidden' name='hidCreatedID[]' value='<?php echo $_SESSION['SESS_USER_ID']; ?>'>
									<?php		
										}
									?>
								</tr>
					<?php	
							}	
						}
					?>
					<tbody id='AddParticulars'>
						<tr class="spacing">
							<td>
								<input type='button' value='+' onclick='addExtraParticulars(<?php echo $count; ?>)'>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveParticulars" value="Save">
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='expense_code.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidExpenseID' value="<?php echo $expenseID;?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>