<html>
	 <head>
	 	<title>Data Migration</title>
	 	<script src="js/data_migration_js.js"></script>
		<!--script src="dist/sweetalert.min.js"></script-->
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"><!--  -->
	 </head>
	 <body>
	 	<?php
	 		require("/include/header.php");

	 		if( $_SESSION['migration'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}
	 	?>
	 	<div class="wrapper">
	 		<span> <h3> Data Migration </h3> </span>
	 		<?php

				if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
					echo '<ul class="err">';
					foreach($_SESSION['ERRMSG_ARR'] as $msg) {
						echo '<li>'.$msg.'</li>'; 
					}
					echo '</ul>';
					unset($_SESSION['ERRMSG_ARR']);
				} 
				elseif( isset($_SESSION['SUCCESS'])) 
				{
					echo '<ul id="success">';
					echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
					echo '</ul>';
					unset($_SESSION['SUCCESS']);
				}
			?>

			<table class='home_pages'>
				<?php if(array_search(60, $session_Permit)){ ?>
				<tr >
					<td colspan="3">
						<label><a name='liRun' onclick="confirmation(100)" class='a_as_button'>Migrate all</a></label>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<th>
						Table
					</th>
					<th>
						Task
					</th>
					<th>
						
					</th>
				</tr>
				<?php 
				if(array_search(70, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Ceritificate of Qualities</label>
					</td>
					<td>
						<label>To migrate CIMS - certificate of qualities table to COMSYS - certificate of qualities table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(10)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(65, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Customer</label>
					</td>
					<td>
						<label>To migrate CIMS - customer table to COMSYS - customer table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(5)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(62, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Department</label>
					</td>
					<td>
						<label>To migrate CIMS - departments table to COMSYS - departments table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(2)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(80, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Finished Goods</label>
					</td>
					<td>
						<label>To migrate CIMS - finished goods table to COMSYS - finished goods table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(14)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(66, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Formulas</label>
					</td>
					<td>
						<label>To migrate CIMS - formulas table to COMSYS - formulas table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(6)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(187, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Mixers</label>
					</td>
					<td>
						<label>To migrate CIMS - mixers table to COMSYS - mixers table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(16)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(72, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Personnels</label>
					</td>
					<td>
						<label>To migrate CIMS - personnels table to COMSYS - personnels table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(12)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(73, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Positions</label>
					</td>
					<td>
						<label>To migrate CIMS - positions table to COMSYS - positions table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(13)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(174, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Raw Material Types</label>
					</td>
					<td>
						<label>To migrate CIMS - raw material types table to COMSYS - raw material types table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(15)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(67, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Raw Materials</label>
					</td>
					<td>
						<label>To migrate CIMS - raw materials table to COMSYS - raw materials table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(7)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(64, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Supplier</label>
					</td>
					<td>
						<label>To migrate CIMS - supplier table to COMSYS - supplier table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(4)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(68, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Supplies</label>
					</td>
					<td>
						<label>To migrate CIMS - supplies table to COMSYS - supplies table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(8)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				//if(array_search(68, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Supply Transactions</label>
					</td>
					<td>
						<label>To migrate CIMS - supply transactions table to COMSYS - supply transactions table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(17)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php //} 
				if(array_search(69, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Trucks</label>
					</td>
					<td>
						<label>To migrate CIMS - trucks table to COMSYS - trucks table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(9)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(63, $session_Permit)){ ?>
				<tr>
					<td>
						<label>Unit of Measures</label>
					</td>
					<td>
						<label>To migrate CIMS - unit of measures table to COMSYS - unit of measures table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(3)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				if(array_search(61, $session_Permit)){ ?>
				<tr>
					<td>
						<label>User</label>
					</td>
					<td>
						<label>To migrate CIMS - user table to COMSYS - login table</label>
					</td>
					<td align='center'>
						<label><a name='liRun' onclick="confirmation(1)" class='a_as_button'>Run Migration</a></label>
					</td>
				</tr>
				<?php } 
				?>
				<tr></tr>
			</table>
	 	</div>
	 </body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
 </html>