<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_equipment_status.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hideqptStatusId = $_POST['hideqptStatusId'];
		$txtNewStatus = clean($_POST['txtNewStatus']);

############# Input Validation
		if ( $txtNewStatus == '' || is_numeric($txtNewStatus)){
			$errmsg_arr[] = '* Status is invalid.';
			$errflag = true;
		}

	######### Validation on Checkbox
		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;

		if($hideqptStatusId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hideqptStatusId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_EQUIPMENT_STATUS.$hideqptStatusId);
			exit();
		}
############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_Equipment_Status_CRU( ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'isissii', $hideqptStatusId, $txtNewStatus, $chkActive
											   , $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_equipment_status.php'.'</td><td>'.$processError.' near line 73.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hideqptStatusId)
				$_SESSION['SUCCESS']  = 'Successfully updated equipment status.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new equipment status.';
			//echo $_SESSION['SUCCESS'];
			header("location:equipment_status.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>