<?php
############# Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_budget_monitoring_departmental.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitize the POST values
		$hidBudgetID = $_POST['hidBudgetID'];
		$txtBudgetYear = $_POST['txtBudgetYear'];
		$sltDepartment = $_POST['sltDepartment'];
		$hidBudgetItemID = $_POST['hidBudgetItemID'];
		$sltGLCode = $_POST['sltGLCode'];
		$sltGLParticulars = (isset($_POST['sltGLParticulars']) ? $_POST['sltGLParticulars'] : 0);
		$hidUOM = $_POST['hidUOM'];
		$txtUnitPrice = $_POST['txtUnitPrice'];
		$txtJAN = $_POST['txtJAN'];
		$txtFEB = $_POST['txtFEB'];
		$txtMAR = $_POST['txtMAR'];
		$txtAPR = $_POST['txtAPR'];
		$txtMAY = $_POST['txtMAY'];;
		$txtJUN = $_POST['txtJUN'];;
		$txtJUL = $_POST['txtJUL'];
		$txtAUG = $_POST['txtAUG'];
		$txtSEP = $_POST['txtSEP'];
		$txtOCT = $_POST['txtOCT'];
		$txtNOV = $_POST['txtNOV'];
		$txtDEC = $_POST['txtDEC'];
		$txtRemarks = $_POST['txtRemarks'];

		$hidItemsCreatedAt = $_POST['hidItemsCreatedAt'];
		$hidItemsCreatedId = $_POST['hidItemsCreatedId'];
		$createdAt = $_POST['hidCreatedAt'];
		$createdId = $_POST['hidCreatedId'];
		$updatedAt = date('Y-m-d H:i:s');
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validations

		if( !is_numeric($txtBudgetYear) ){
			$errmsg_arr[] = "* Invalid year.";
			$errflag = true;
		}
		if( !$sltDepartment ){
			$errmsg_arr[] = "* Select department.";
			$errflag = true;
		}
		if( !$sltGLCode ){
			$errmsg_arr[] = "* Select GL code.";
			$errflag = true;
		}
		$transaction = count($hidUOM);
		if( !$transaction ){
			$errmsg_arr[] = "* There must be at least (1) one transaction.";
			$errflag = true;
		}

		foreach ($sltGLParticulars as $key_1 => $Miscellaneous) {
			if( $sltGLParticulars[$key_1] ){
				if( $hidUOM[$key_1] == "" ){
					$errmsg_arr[] = "* Unit of measure can't be blank (line ".($key_1+1).").";
					$errflag = true;
				}
				if( $txtUnitPrice[$key_1] == "" ){
					$errmsg_arr[] = "* Unit price can't be blank (line ".($key_1+1).").";
					$errflag = true;
				}
				if( $txtJAN[$key_1] == "" && $txtFEB[$key_1] == "" && $txtMAR[$key_1] == "" && $txtAPR[$key_1] == "" && $txtMAY[$key_1] == "" && $txtJUN[$key_1] == "" &&
					$txtJUL[$key_1] == "" && $txtAUG[$key_1] == "" && $txtSEP[$key_1] == "" && $txtOCT[$key_1] == "" && $txtNOV[$key_1] == "" && $txtDEC[$key_1] == "" ){
					
					$errmsg_arr[] = "* At least (1) one month should have a value (line ".($key_1+1).").";
					$errflag = true;
				}else{
					if( $txtJAN[$key_1] != "" && !is_numeric($txtJAN[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for January (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtFEB[$key_1] != "" && !is_numeric($txtFEB[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for February (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtMAR[$key_1] != "" && !is_numeric($txtMAR[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for March (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtAPR[$key_1] != "" && !is_numeric($txtAPR[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for April (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtMAY[$key_1] != "" && !is_numeric($txtMAY[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for May (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtJUN[$key_1] != "" && !is_numeric($txtJUN[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for June (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtJUL[$key_1] != "" && !is_numeric($txtJUL[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for July (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtAUG[$key_1] != "" && !is_numeric($txtAUG[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for August (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtSEP[$key_1] != "" && !is_numeric($txtSEP[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for September (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtOCT[$key_1] != "" && !is_numeric($txtOCT[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for October (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtNOV[$key_1] != "" && !is_numeric($txtNOV[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for November (line ".($key_1+1).").";
						$errflag = true;
					}
					if( $txtDEC[$key_1] != "" && !is_numeric($txtDEC[$key_1]) ){
						$errmsg_arr[] = "* Invalid value for December (line ".($key_1+1).").";
						$errflag = true;
					}
				}
			}else{
				if( $txtJAN[$key_1] != "" || $txtFEB[$key_1] != "" || $txtMAR[$key_1] != "" || $txtAPR[$key_1] != "" || $txtMAY[$key_1] != "" || $txtJUN[$key_1] != "" ||
					$txtJUL[$key_1] != "" || $txtAUG[$key_1] != "" || $txtSEP[$key_1] != "" || $txtOCT[$key_1] != "" || $txtNOV[$key_1] != "" || $txtDEC[$key_1] != "" ){
					
					$errmsg_arr[] = "* Select particulars (line ".($key_1+1).").";
					$errflag = true;
				}
			}
		}

############# SESSION

		$_SESSION['SESS_BMD_BudgetYear'] = $txtBudgetYear;
		$_SESSION['SESS_BMD_Department'] = $sltDepartment;
		$_SESSION['SESS_BMD_GLCode'] = $sltGLCode;
		$_SESSION['SESS_BMD_Particulars'] = $sltGLParticulars;
		$_SESSION['SESS_BMD_UOM'] = $hidUOM;
		$_SESSION['SESS_BMD_UnitPrice'] = $txtUnitPrice;
		$_SESSION['SESS_BMD_JAN'] = $txtJAN;
		$_SESSION['SESS_BMD_FEB'] = $txtFEB;
		$_SESSION['SESS_BMD_MAR'] = $txtMAR;
		$_SESSION['SESS_BMD_APR'] = $txtAPR;
		$_SESSION['SESS_BMD_MAY'] = $txtMAY;
		$_SESSION['SESS_BMD_JUN'] = $txtJUN;
		$_SESSION['SESS_BMD_JUL'] = $txtJUL;
		$_SESSION['SESS_BMD_AUG'] = $txtAUG;
		$_SESSION['SESS_BMD_SEP'] = $txtSEP;
		$_SESSION['SESS_BMD_OCT'] = $txtOCT;
		$_SESSION['SESS_BMD_NOV'] = $txtNOV;
		$_SESSION['SESS_BMD_DEC'] = $txtDEC;
		$_SESSION['SESS_BMD_Remarks'] = $txtRemarks;
		// $_SESSION['SESS_BMD_ADD'] = $_POST["hidAdd"];

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_budget_monitoring_departmental.php?id=".$hidBudgetID);
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Budget_Monitoring_Departmental_CRU(?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isissiii', $hidBudgetID, $txtBudgetYear, $sltDepartment, $createdAt, $updatedAt, $createdId, $updatedId, $sltGLCode);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_budget_monitoring_departmental.php'.'</td><td>'.$processError.' near line 180.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidBudgetID ) {
				$BudgetID = $hidBudgetID;
			}else{
				$BudgetID = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['sltGLParticulars'] as $key => $itemValue)
			{	
				if (!$hidBudgetID) {
					$BudgetItemID[$key] = 0;
				}else{
					$BudgetItemID[$key] = $hidBudgetItemID[$key];
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Budget_Monitoring_Departmental_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiidddddddddddddssiis', $BudgetItemID[$key], $BudgetID, $sltGLParticulars[$key]
																			 , $txtUnitPrice[$key], $txtJAN[$key], $txtFEB[$key], $txtMAR[$key], $txtAPR[$key]
																			 , $txtMAY[$key], $txtJUN[$key], $txtJUL[$key], $txtAUG[$key], $txtSEP[$key]
																			 , $txtOCT[$key], $txtNOV[$key], $txtDEC[$key], $hidItemsCreatedAt[$key]
																			 , $updatedAt, $hidItemsCreatedId[$key], $updatedId, $txtRemarks[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_budget_monitoring_departmental.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidBudgetID )
							$_SESSION['SUCCESS']  = "Budget for $txtBudgetYear was successfully updated.";
						else
							$_SESSION['SUCCESS']  = "Budget for $txtBudgetYear was successfully added.";
						header("location: budget_monitoring_departmental.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database	
		require("include/database_close.php");
	}
?>
	