<html>
	<head>
		<title> Catched Errors </title>
		<?php 
			require("/include/database_connect.php");
		?>
	 	<script src="js/catched_error_js.js"></script>
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
	</head>
	<body>

		<?php
			require("/include/header.php");

			if( $_SESSION['error'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}	
		?>

		<div class="wrapper">

			<span> <h3> Errors </h3> </span>

			<table class="home_pages">
				<tr>
					<td>
						<input type="button" value="Clear Error Logs" onclick="Clear()">
					</td>
				</tr>

				<tr>
					<th> Date / Time </th>
					<th> File </th>
					<th> Message </th>
				</tr>

				<tr class="spacing2"></tr>

				<tr><th colspan='3'>Folder - Main</th></tr>

				<?php 
					require("/errors.php");
				?>

				<tr class="spacing2"></tr>

				<tr><th colspan='3'>Folder - Include</th></tr>
				
				<?php
					require("/include/errors.php");
				?>
				
				<tr></tr>
			</table>
			
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>