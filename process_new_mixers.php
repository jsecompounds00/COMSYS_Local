<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_mixers.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidMixerId = clean($_POST['hidMixerId']);
		$txtCapacity = $_POST['txtCapacity'];

		$_SESSION['txtCapacity'] = $txtCapacity;

		//Input Validations
		if ( !is_numeric($txtCapacity) ){
			$errmsg_arr[] = '* Invalid value for mixer capacity.';
			$errflag = true;
		}

		$updatedAt = date('Y-m-d H:i:s');
		$updatedId = $_SESSION['SESS_USER_ID'];

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_mixers.php?id=$hidMixerId");
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Mixers_CRU(?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'idsi', $hidMixerId, $txtCapacity, $updatedAt, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_mixers.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidMixerId)
				$_SESSION['SUCCESS']  = 'Successfully updated mixers.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new mixers.';
			//echo $_SESSION['SUCCESS'];
			header("location:mixers.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	