<html>
	<head>
		<?php
			require("/include/database_connect.php");
			if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_oc.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$budgetID = $_GET["id"];

					if($budgetID)
					{ 
						$qry = mysqli_prepare($db, "CALL sp_Budget_Monitoring_OC_Query(?)");
						mysqli_stmt_bind_param($qry, "i", $budgetID);
						$qry->execute();
						$result = mysqli_stmt_get_result($qry);
						$processError = mysqli_error($db);

						if(!empty($processError))
						{
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_oc.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							while($row = mysqli_fetch_assoc($result))
							{
								$QBudgetID = $row["QBudgetID"];
								$QBudgetYear = $row["QBudgetYear"];
								$QDepartmentID = $row["QDepartmentID"];
								$QCreatedAt = $row["QCreatedAt"];
								$QCreatedID = $row["QCreatedID"];
								$QMiscTypeID = $row["QMiscTypeID"];

								$QBudgetItemID[] = $row["QBudgetItemID"];
							    $QMiscellaneousID[] = $row["QMiscellaneousID"];
							    $QUOMID[] = $row["QUOMID"];
							    $QUnitPrice[] = $row["QUnitPrice"];
							    $QItemsCreatedAt[] = $row["QItemsCreatedAt"];
							    $QItemsCreatedID[] = $row["QItemsCreatedID"];
							    $QRemarks[] = $row["QRemarks"];
							    $QJAN[] = ( $row["QJAN"] != 0 ? $row["QJAN"] : NULL );
							    $QFEB[] = ( $row["QFEB"] != 0 ? $row["QFEB"] : NULL );
							    $QMAR[] = ( $row["QMAR"] != 0 ? $row["QMAR"] : NULL );
							    $QAPR[] = ( $row["QAPR"] != 0 ? $row["QAPR"] : NULL );
							    $QMAY[] = ( $row["QMAY"] != 0 ? $row["QMAY"] : NULL );
							    $QJUN[] = ( $row["QJUN"] != 0 ? $row["QJUN"] : NULL );
							    $QJUL[] = ( $row["QJUL"] != 0 ? $row["QJUL"] : NULL );
							    $QAUG[] = ( $row["QAUG"] != 0 ? $row["QAUG"] : NULL );
							    $QSEP[] = ( $row["QSEP"] != 0 ? $row["QSEP"] : NULL );
							    $QOCT[] = ( $row["QOCT"] != 0 ? $row["QOCT"] : NULL );
							    $QNOV[] = ( $row["QNOV"] != 0 ? $row["QNOV"] : NULL );
							    $QDEC[] = ( $row["QDEC"] != 0 ? $row["QDEC"] : NULL );
							}
						}
						$db->next_result();
						$result->close();

						$count = count($QBudgetItemID);

			############ .............
						$qryPI = "SELECT id from comsys.budget_monitoring_oc";
						$resultPI = mysqli_query($db, $qryPI); 
						$processErrorPI = mysqli_error($db);

						if ( !empty($processErrorPI) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_oc.php"."</td><td>".$processErrorPI." near line 58.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}else{
								$id = array();
							while($row = mysqli_fetch_assoc($resultPI)){
								$id[] = $row["id"];
							}
						}
						$db->next_result();
						$resultPI->close();

			############ .............
						if( !in_array($budgetID, $id, TRUE) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_oc.php</td><td>The user tries to edit a non-existing department_id.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}

						echo "<title>Supplies Budget - Edit</title>";
					}
					else
					{
						echo "<title>Supplies Budget - Add</title>";
					}
				}
		?>
		<script src="js/budget_monitoring_oc_js.js"></script>  
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> 
	</head>
	<body>
	
		<form method="post" action="process_new_budget_monitoring_oc.php">
			
			<?php
				require("/include/header.php");
				require("/include/init_unset_values/budget_monitoring_oc_init_value.php");

				// if ( $budgetID ){
				// 	if( $_SESSION["oc_budget_edit"] == false) 
				// 	{
				// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}
				// }else{	
				// 	if( $_SESSION["oc_budget_add"] == false) 
				// 	{
				// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}			
				// }

			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $budgetID ? "Edit Budget" : "New Budget" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td> Year: </td>
						<td>
							<input type="text" name="txtBudgetYear" value="<?php echo($budgetID && $initBMOBudgetYear == NULL ? $QBudgetYear : $initBMOBudgetYear);?>">
						</td>
					</tr>

					<tr>
						<td> Department: </td>
						<td>
							<select name="sltDepartment">
								<?php
		 							$qryBD = "CALL sp_Budget_Department_Dropdown(1)"; 
									$resultBD = mysqli_query($db, $qryBD);
									$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_oc.php"."</td><td>".$processError1." near line 43.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultBD))
										{
											$DDId = $row["DDId"];
											$DDCode = $row["DDCode"];

											echo "<option value='".$DDId."'".
												  ($budgetID && $initBMODepartment == NULL ? ( $QDepartmentID == $DDId ? "selected" : "" ) : ( $initBMODepartment == $DDId ? "selected" : "" ))
												  .">".$DDCode."</option>";
										}
										
										$db->next_result();
										$resultBD->close();
									}
			 					?>
							</select>
						</td>
					</tr>
					<tr>
						<td> GL Code: </td>
						<td>
							<select name="sltMiscType" id="sltMiscType" onchange="showItem()">
								<?php
									$qryMT = "CALL sp_Budget_Miscellaneous_Type_Dropdown(1)";
									$resultMT = mysqli_query($db, $qryMT);
									$processErrorMT = mysqli_error($db);

									if(!empty($processErrorMT))
									{
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_oc.php"."</td><td>".$processErrorMT." near line 43.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultMT))
										{
											$DDMTID = $row["DDMTID"];
											$DDMTName = $row["DDMTName"];

											echo "<option value='".$DDMTID."'".
												  ($budgetID && $initBMOMiscType == NULL ? ( $QMiscTypeID == $DDMTID ? "selected" : "" ) : ( $initBMOMiscType == $DDMTID ? "selected" : "" ))
												  .">".$DDMTName."</option>";
										}
										
										$db->next_result();
										$resultMT->close();
									}
								?>
							</select>
						</td>
					</tr>
				</table>

				<table class="child_tables_form collapsed">
					<tr>
						<th rowspan="2"> Item </th>
						<th rowspan="2"> UOM </th>
						<th rowspan="2"> Unit Price </th>
						<th colspan="12"> Quantity </th>
						<th rowspan="2"> Remarks </th>
					</tr>
					<tr>
						<th> JAN </th>
						<th> FEB </th>
						<th> MAR </th>
						<th> APR </th>
						<th> MAY </th>
						<th> JUN </th>
						<th> JUL </th>
						<th> AUG </th>
						<th> SEP </th>
						<th> OCT </th>
						<th> NOV </th>
						<th> DEC </th>
					</tr>	

					<?php
						for ($i=0; $i < 50 ; $i++) { 
					?>
							<tr>
								<td>
									<select name="sltMiscellaneous[<?php echo $i;?>]" id="sltMiscellaneous<?php echo $i;?>" onchange="showUOM(<?php echo $i;?>), showUnitPrice(<?php echo $i;?>)">
										<?php
											$qryM = mysqli_prepare( $db, "CALL sp_Budget_Miscellaneous_Dropdown(?,1)" );
											mysqli_stmt_bind_param( $qryM, 'i', ($budgetID && $initBMOMiscType == NULL ? $QMiscTypeID : $initBMOMiscType) );
											$qryM->execute();
											$resultM = mysqli_stmt_get_result( $qryM );
											$processErrorM = mysqli_error($db);
										
											if ($processErrorM){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_budget_monitoring_oc.php'.'</td><td>'.$processErrorM.' near line 21.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultM))
												{
													$DDItemID = $row['DDItemID'];
													$DDItemName = $row['DDItemName'];
													
													echo "<option value='".$DDItemID."'".
														  ($budgetID && $count > $i && $initBMOMiscellaneous[$i] == NULL ? ( $QMiscellaneousID[$i] == $DDItemID ? "selected" : "" ) : ( $initBMOMiscellaneous[$i] == $DDItemID ? "selected" : "" ))
														  .">".$DDItemName."</option>";
												}
												$db->next_result();
												$resultM->close();
											}
										?> 
									</select>
								</td>

								<td id="tdUOM<?php echo $i;?>">
									<?php
										if( $initBMOMiscellaneous[$i] || $budgetID ){
											$qry = mysqli_prepare( $db, "CALL sp_Budget_Miscellaneous_Details_Query(?)" );
											mysqli_stmt_bind_param( $qry, 'i', ($budgetID && $count > $i && $initBMOMiscellaneous[$i] == NULL ? $QMiscellaneousID[$i] : $initBMOMiscellaneous[$i]) );
											$qry->execute();
											$result = mysqli_stmt_get_result( $qry );
											$processError = mysqli_error($db);
										
											if ($processError){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_budget_monitoring_oc'.'</td><td>'.$processError.' near line 21.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($result))
												{
													$MDUOMID = $row['MDUOMID'];
													$MDUOMCode = $row['MDUOMCode'];
													
													echo $MDUOMCode;
									?>
													<input type="hidden" name="hidUOM[]" value="<?php echo $MDUOMID;?>">
									<?php
												}
												$db->next_result();
												$result->close();
											}
										}
									?> 
								</td>

								<td id="tdUnitPrice<?php echo $i;?>">
									<?php
										if( $initBMOMiscellaneous[$i] || $budgetID ){
											$qry = mysqli_prepare( $db, "CALL sp_Budget_Miscellaneous_Details_Query(?)" );
											mysqli_stmt_bind_param( $qry, 'i', ($budgetID && $count > $i && $initBMOMiscellaneous[$i] == NULL ? $QMiscellaneousID[$i] : $initBMOMiscellaneous[$i]) );
											$qry->execute();
											$result = mysqli_stmt_get_result( $qry );
											$processError = mysqli_error($db);
										
											if ($processError){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_budget_monitoring_oc'.'</td><td>'.$processError.' near line 21.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($result))
												{
													$MDUnitPrice = $row['MDUnitPrice'];
													
									?>
													<input type="text" name="txtUnitPrice[<?php echo $i;?>]" 
														value="<?php echo ($budgetID && $count > $i && $initBMOMiscellaneous[$i] == NULL ? $QUnitPrice[$i] : $initBMOUnitPrice[$i]);?>" 
														class="short_text_2">
									<?php
												}
												$db->next_result();
												$result->close();
											}
										}
									?> 
								</td>

								<td>
									<input type="text" name="txtJAN[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOJAN[$i] == NULL ? $QJAN[$i] : $initBMOJAN[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtFEB[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOFEB[$i] == NULL ? $QFEB[$i] : $initBMOFEB[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtMAR[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOMAR[$i] == NULL ? $QMAR[$i] : $initBMOMAR[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtAPR[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOAPR[$i] == NULL ? $QAPR[$i] : $initBMOAPR[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtMAY[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOMAY[$i] == NULL ? $QMAY[$i] : $initBMOMAY[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtJUN[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOJUN[$i] == NULL ? $QJUN[$i] : $initBMOJUN[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtJUL[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOJUL[$i] == NULL ? $QJUL[$i] : $initBMOJUL[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtAUG[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOAUG[$i] == NULL ? $QAUG[$i] : $initBMOAUG[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtSEP[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOSEP[$i] == NULL ? $QSEP[$i] : $initBMOSEP[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtOCT[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMOOCT[$i] == NULL ? $QOCT[$i] : $initBMOOCT[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtNOV[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMONOV[$i] == NULL ? $QNOV[$i] : $initBMONOV[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtDEC[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMODEC[$i] == NULL ? $QDEC[$i] : $initBMODEC[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtRemarks[]" class="long_text" 
										value="<?php echo ($budgetID && $count > $i && $initBMORemarks[$i] == NULL ? $QRemarks[$i] : $initBMORemarks[$i]);?>">
								</td>
									<input type="hidden" name="hidBudgetItemID[]" value="<?php echo ($budgetID && $count > $i ? $QBudgetItemID[$i] : 0 );?>">
									<input type="hidden" name="hidItemsCreatedId[]" value="<?php echo ( $budgetID && $count > $i && $QItemsCreatedID[$i] ? $QItemsCreatedID[$i] : $_SESSION["SESS_USER_ID"] );?>">
									<input type="hidden" name="hidItemsCreatedAt[]" value="<?php echo ( $budgetID && $count > $i && $QItemsCreatedAt[$i] ? $QItemsCreatedAt[$i] : date("Y-m-d H:i:s") );?>">
							</tr>
					<?php
						}
					?>
					<!-- <tbody id="tbExtraFields1"></tbody>	
					<tbody id="tbExtraFields2"></tbody>	
					<tbody id="tbExtraFields3"></tbody>	
					<tr class="align_bottom">
						<td colspan="16">
							<input type="button" value=" + " id="btnAdd" onclick="showExtraFields()">
							<input type="hidden" value="1" name="hidAdd" id="hidAdd">
						</td>
					</tr>	 -->		
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveDept" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='budget_monitoring_oc.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidBudgetID" id="hidBudgetID" value="<?php echo $budgetID;?>">
							<input type="hidden" id="hidCounter" value="<?php echo ( $budgetID ? "" : 0 );?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $budgetID ? $QCreatedID : $_SESSION["SESS_USER_ID"] );?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $budgetID ? $QCreatedAt : date("Y-m-d H:i:s") );?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>