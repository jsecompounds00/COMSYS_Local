<?php
############# Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_batch_ticket.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidBatchTicketId = $_POST['hidBatchTicketId'];
		$hidBatchTicketTrialId = $_POST['hidBatchTicketTrialId'];
		$hidNBatchTicketTrialItemId = $_POST['hidNBatchTicketTrialItemId'];
		$radRequestType = "NRM";
		$sltNRMNumber = $_POST['sltNRMNumber'];
		$sltJRDNumber = NULL;
		$sltActivity = $_POST['sltActivity'];
		$txtBatchTicketNo = $_POST['txtBatchTicketNo'];
		$txtBatchTicketDate = $_POST['txtBatchTicketDate'];
		$txtTERNo = $_POST['txtTERNo'];
		$radSoft = $_POST['radSoft'];
		$radSoft = ( isset($radSoft) && $radSoft == 'Soft' ? 1 : 0 );
		$sltFG = $_POST['sltFG'];
		$sltFormulaType = $_POST['sltFormulaType'];
		$txtTrialNo = $_POST['txtTrialNo'];
		$txtLotNo = $_POST['txtLotNo'];
		$txtTrialDate = $_POST['txtTrialDate'];
		$txtExtDarNo = $_POST['txtExtDarNo'];
		$txtExtZ1 = $_POST['txtExtZ1'];
		$txtExtZ2 = $_POST['txtExtZ2'];
		$txtExtDH = $_POST['txtExtDH'];
		$txtExtScrewSpeed = $_POST['txtExtScrewSpeed'];
		$txtExtCutterSpeed = $_POST['txtExtCutterSpeed'];
		$txtMixDarNo = $_POST['txtMixDarNo'];
		$txtMixParam1 = $_POST['txtMixParam1'];
		$txtMixSequence1 = $_POST['txtMixSequence1'];
		$txtMixParam2 = $_POST['txtMixParam2'];
		$txtMixSequence2 = $_POST['txtMixSequence2'];
		$txtMixParam3 = $_POST['txtMixParam3'];
		$txtMixSequence3 = $_POST['txtMixSequence3'];
		$txtMixParam4 = $_POST['txtMixParam4'];
		$tztMixSequence4 = $_POST['tztMixSequence4'];
		$txtMixParam5 = $_POST['txtMixParam5'];
		$txtMixSequence5 = $_POST['txtMixSequence5'];
		$txtMultiplier = $_POST['txtMultiplier'];
		$txtNRm_type_id = $_POST['txtNRm_type_id'];
		$txtNRm_id = $_POST['txtNRm_id'];
		$txtNPHR = $_POST['txtNPHR'];
		$txtNQuantity = $_POST['txtNQuantity'];
		$txtRemarks = $_POST['txtRemarks'];
		
		// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		
		// $txtRemarks = str_replace('\\', '', $txtRemarks);

		$chkDynamicMilling = ( isset( $_POST['chkDynamicMilling'] ) ? 1 : 0 );
		$chkOilAging = ( isset( $_POST['chkOilAging'] ) ? 1 : 0 );
		$chkOvenAging = ( isset( $_POST['chkOvenAging'] ) ? 1 : 0 );
		$chkStrandInspect = ( isset( $_POST['chkStrandInspect'] ) ? 1 : 0 );
		$chkPelletInspect = ( isset( $_POST['chkPelletInspect'] ) ? 1 : 0 );
		$chkImpactTest = ( isset( $_POST['chkImpactTest'] ) ? 1 : 0 );
		$chkStaticHeating = ( isset( $_POST['chkStaticHeating'] ) ? 1 : 0 );
		$chkColorChange = ( isset( $_POST['chkColorChange'] ) ? 1 : 0 );
		$chkWaterImmersion = ( isset( $_POST['chkWaterImmersion'] ) ? 1 : 0 );
		$chkColdTesting = ( isset( $_POST['chkColdTesting'] ) ? 1 : 0 );

		if($hidBatchTicketId){
			$hidBatCreatedAt = $_POST['hidBatCreatedAt'];
			$hidBatCreatedId = $_POST['hidBatCreatedId'];
			if ( $hidBatchTicketTrialId ){
				$hidBatTrialCreatedAt = $_POST['hidBatTrialCreatedAt'];
				$hidBatTrialCreatedId = $_POST['hidBatTrialCreatedId'];
				$hidBatTrialUpdatedAt = date('Y-m-d H:i:s');
				$hidBatTrialUpdatedId = $_SESSION['SESS_USER_ID'];
			}else{
				$hidBatTrialCreatedAt = date('Y-m-d H:i:s');
				$hidBatTrialCreatedId = $_SESSION['SESS_USER_ID'];
				$hidBatTrialUpdatedAt = NULL;
				$hidBatTrialUpdatedId = NULL;
			}
			$hidBatUpdatedAt = date('Y-m-d H:i:s');
			$hidBatUpdatedId = $_SESSION['SESS_USER_ID'];
		}else{
			$hidBatCreatedAt = date('Y-m-d H:i:s');
			$hidBatCreatedId = $_SESSION['SESS_USER_ID'];
			$hidBatTrialCreatedAt = date('Y-m-d H:i:s');
			$hidBatTrialCreatedId = $_SESSION['SESS_USER_ID'];
			$hidBatUpdatedAt = NULL;
			$hidBatUpdatedId = NULL;
			$hidBatTrialUpdatedAt = NULL;
			$hidBatTrialUpdatedId = NULL;
		}

	############# Trial Items
		foreach ($_POST['txtRM_type_id'] as $RM_type_id) {
			$RM_type_id = array();
			$RM_type_id = $_POST['txtRM_type_id'];
		}
		foreach ($_POST['txtRM_id'] as $RM_id) {
			$RM_id = array();
			$RM_id= $_POST['txtRM_id'];
		}
		foreach ($_POST['txtPhr'] as $Phr) {
			$Phr = array();
			$Phr = $_POST['txtPhr'];
		}
		foreach ($_POST['txtQty'] as $Qty) {
			$Qty = array();
			$Qty = $_POST['txtQty'];
		}
		foreach ($_POST['hidBatchTicketTrialItemId'] as $BatchTicketTrialItemId) {
			$BatchTicketTrialItemId = array();
			$BatchTicketTrialItemId = $_POST['hidBatchTicketTrialItemId'];
		}


		// foreach ($_POST as $key => $value) {
		// 	echo $key.' => '.$value.'<br>';
		// }
############# Input Validation	
			if ( !$sltActivity ){
				$errmsg_arr[] = '* Select activity.';
				$errflag = true;
			}else{
				if ( $radRequestType == 'NRM' && $txtTrialNo != 1 && !$sltNRMNumber ){
					$errmsg_arr[] = '* Select NRM number.';
					$errflag = true;
				}elseif ( $radRequestType == 'NRM' && $txtTrialNo == 1 ){
					$sltNRMNumber = 0;
				}/* elseif ( isset($radRequestType) && $radRequestType == 'JRD' && !$sltNRMNumber ){
				 	$errmsg_arr[] = '* Select JRD number.';
				 	$errflag = true;
				}*/
				$valBatchTicketDate = validateDate($txtBatchTicketDate, 'Y-m-d');
				if ( $valBatchTicketDate != 1 ){
					$errmsg_arr[] = '* Invalid batch ticket date.';
					$errflag = true;
				}
				if ( !isset($radSoft) ){
					$errmsg_arr[] = '* Choose finished good type.';
					$errflag = true;
				}else{
					// if ( isset($radSoft) && $radSoft == 'Rigid' ){
					// 	$radSoft = 0;
					// }else{
					// 	$radSoft = 1;
					// }
					if ( ($sltActivity == 1 || $sltActivity == 3 || $sltActivity == 5) && !$sltFG ){
						$errmsg_arr[] = '* Select FG item.';
						$errflag = true;
					}
					if ( ($sltActivity == 1 || $sltActivity == 3 || $sltActivity == 5) && !$sltFormulaType ){
						$errmsg_arr[] = '* Select FG item.';
						$errflag = true;
					}
				}
				$valTrialDate = validateDate($txtTrialDate, 'Y-m-d');
				if ( $txtTrialDate != '' && $valTrialDate != 1 ){
					$errmsg_arr[] = '* Invalid trial date.';
					$errflag = true;
				}elseif( $valTrialDate == 1 && $txtTrialDate < $txtBatchTicketDate ){
					$errmsg_arr[] = '* Invalid trial date.';
					$errflag = true;
				}elseif ( $txtTrialDate == '' ){
					$txtTrialDate = NULL;
				}
				if ( $txtExtZ1 != '' && !is_numeric($txtExtZ1) ){
					$errmsg_arr[] = '* Invalid parameter for Z1.';
					$errflag = true;
				}elseif ( $txtExtZ1 == '' ){
					$txtExtZ1 = NULL;
				}
				if ( $txtExtZ2 != '' && !is_numeric($txtExtZ2) ){
					$errmsg_arr[] = '* Invalid parameter for Z2.';
					$errflag = true;
				}elseif ( $txtExtZ2 == '' ){
					$txtExtZ2 = NULL;
				}
				if ( $txtExtDH != '' && !is_numeric($txtExtDH) ){
					$errmsg_arr[] = '* Invalid parameter for DH.';
					$errflag = true;
				}elseif ( $txtExtDH == '' ){
					$txtExtDH = NULL;
				}
				if ( $txtExtScrewSpeed != '' && !is_numeric($txtExtScrewSpeed) ){
					$errmsg_arr[] = '* Invalid parameter for Screw Speed.';
					$errflag = true;
				}elseif ( $txtExtScrewSpeed == '' ){
					$txtExtScrewSpeed = NULL;
				}
				if ( $txtExtCutterSpeed != '' && !is_numeric($txtExtCutterSpeed) ){
					$errmsg_arr[] = '* Invalid parameter for Cutter Speed.';
					$errflag = true;
				}elseif ( $txtExtCutterSpeed == '' ){
					$txtExtCutterSpeed = NULL;
				}
				if ( !is_numeric( $txtMultiplier ) ){
					$errmsg_arr[] = '* Invalid multiplier.';
					$errflag = true;
				}
				if ( $txtNRm_id ){
					if ( !is_numeric( $txtNPHR ) ){
						$errmsg_arr[] = '* Invalid phr for New Raw Material.';
						$errflag = true;
					}
				}else{
					if ( !empty( $txtNPHR ) ){
						$errmsg_arr[] = '* Select New Raw Material.';
						$errflag = true;
					}
				}
			}
	############# Validation Trial Items
		$i=0;
		$count = count($RM_id);

		if ( isset( $radSoft ) && $sltActivity ){
			do{
				if ( $RM_type_id[0] == '' || !$RM_type_id[0] ){
					$errmsg_arr[] = '* Formula must contain atleast one row.';
					$errflag = true;
				}
				elseif ( $RM_type_id[$i] ){
					if ( $RM_id[$i] == '' || !$RM_id[$i] ){
						$errmsg_arr[] = '* Invalid Raw Material. (line '.($i+1).')';
						$errflag = true;
					}
					if ( $Phr[$i] == '' || !is_numeric($Phr[$i]) ){
						$errmsg_arr[] = '* Invalid PHR. (line '.($i+1).')';
						$errflag = true;
					}
				}
				$i++;
			} while ( $i < $count );
		}
		echo 'count: '.$count.'<br>';
############# Input Validation

############# SESSION, keeping last input value
	$_SESSION['NRM_sltActivity'] = $sltActivity;
	$_SESSION['NRM_sltNRMNumber'] = $sltNRMNumber;
	$_SESSION['NRM_radSoft'] = $radSoft;
	$_SESSION['NRM_txtBatchTicketDate'] = $txtBatchTicketDate;
	$_SESSION['NRM_sltFG'] = $sltFG;
	$_SESSION['NRM_txtTERNo'] = $txtTERNo;
	$_SESSION['NRM_sltFormulaType'] = $sltFormulaType;
	$_SESSION['NRM_txtLotNo'] = $txtLotNo;
	$_SESSION['NRM_txtTrialDate'] = $txtTrialDate;
	$_SESSION['NRM_txtExtDarNo'] = $txtExtDarNo;
	$_SESSION['NRM_txtMixDarNo'] = $txtMixDarNo;
	$_SESSION['NRM_txtExtZ1'] = $txtExtZ1;
	$_SESSION['NRM_txtMixParam1'] = $txtMixParam1;
	$_SESSION['NRM_txtMixSequence1'] = $txtMixSequence1;
	$_SESSION['NRM_txtExtZ2'] = $txtExtZ2;
	$_SESSION['NRM_txtMixParam2'] = $txtMixParam2;
	$_SESSION['NRM_txtMixSequence2'] = $txtMixSequence2;
	$_SESSION['NRM_txtExtDH'] = $txtExtDH;
	$_SESSION['NRM_txtMixParam3'] = $txtMixParam3;
	$_SESSION['NRM_txtMixSequence3'] = $txtMixSequence3;
	$_SESSION['NRM_txtExtScrewSpeed'] = $txtExtScrewSpeed;
	$_SESSION['NRM_txtMixParam4'] = $txtMixParam4;
	$_SESSION['NRM_tztMixSequence4'] = $tztMixSequence4;
	$_SESSION['NRM_txtExtCutterSpeed'] = $txtExtCutterSpeed;
	$_SESSION['NRM_txtMixParam5'] = $txtMixParam5;
	$_SESSION['NRM_txtMixSequence5'] = $txtMixSequence5;
	$_SESSION['NRM_txtMultiplier'] = $txtMultiplier;

	$_SESSION['NRM_chkDynamicMilling'] = $chkDynamicMilling;
	$_SESSION['NRM_chkOilAging'] = $chkOilAging;
	$_SESSION['NRM_chkOvenAging'] = $chkOvenAging;
	$_SESSION['NRM_chkStrandInspect'] = $chkStrandInspect;
	$_SESSION['NRM_chkPelletInspect'] = $chkPelletInspect;
	$_SESSION['NRM_chkImpactTest'] = $chkImpactTest;
	$_SESSION['NRM_chkStaticHeating'] = $chkStaticHeating;
	$_SESSION['NRM_chkColorChange'] = $chkColorChange;
	$_SESSION['NRM_chkWaterImmersion'] = $chkWaterImmersion;
	$_SESSION['NRM_chkColdTesting'] = $chkColdTesting;
	
	$_SESSION['NRM_txtNRm_id'] = $txtNRm_id;
	$_SESSION['NRM_txtNPHR'] = $txtNPHR;
	$_SESSION['NRM_txtNQuantity'] = $txtNQuantity;

	$_SESSION['NRM_txtRemarks'] = $txtRemarks;

// ############# If there are input validations, redirect back to the login form
	if ( $errflag ){
		$_SESSION["NRM_error_indicator"] = 1;
	}else{
		$_SESSION["NRM_error_indicator"] = 0;
	}

	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		if ( $hidBatchTicketId && $hidBatchTicketTrialId ){
			header("Location:new_batch_ticket_nrm.php?id=".$hidBatchTicketId."&tid=".$hidBatchTicketTrialId);
		}else{
			header("Location:new_batch_ticket_nrm.php?id=".$hidBatchTicketId."&tid=0");
		}
		exit();
	}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Batch_Ticket_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL, ? )");
	mysqli_stmt_bind_param($qry, 'isissiiissiiiiiiiiiiii', $hidBatchTicketId, $radRequestType, $sltActivity, $txtBatchTicketNo
												, $txtBatchTicketDate, $radSoft, $sltFG, $sltFormulaType
											 	, $hidBatCreatedAt, $hidBatUpdatedAt, $hidBatCreatedId, $hidBatUpdatedId
											 	, $chkDynamicMilling, $chkOilAging, $chkOvenAging, $chkStrandInspect, $chkPelletInspect
											 	, $chkImpactTest, $chkStaticHeating, $chkColorChange, $chkWaterImmersion, $chkColdTesting);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_batch_ticket.php'.'</td><td>'.$processError.' near line 275.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastBatchTicketId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastBatchTicketId))
		{
			if ( $hidBatchTicketId ) {
				$BatchTicketId = $hidBatchTicketId;
			}else{
				$BatchTicketId = $row['LAST_INSERT_ID()'];
			}	//end if-else BatchTicketId

		}// end while qryLastBatchTicketId
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			if (!$hidBatchTicketId) {
				$hidBatchTicketTrialId = 0;
			}//end if hidBatchTicketTrialId

			$qryTrials = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trials_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qryTrials, 'iiiisisssdddddsssssssssssdsssii', $hidBatchTicketTrialId, $BatchTicketId, $sltNRMNumber, $sltJRDNumber
																, $txtTERNo, $txtTrialNo, $txtLotNo, $txtTrialDate, $txtExtDarNo, $txtExtZ1, $txtExtZ2
																, $txtExtDH, $txtExtScrewSpeed, $txtExtCutterSpeed, $txtMixDarNo, $txtMixParam1
																, $txtMixSequence1, $txtMixParam2, $txtMixSequence2, $txtMixParam3, $txtMixSequence3
																, $txtMixParam4, $tztMixSequence4, $txtMixParam5, $txtMixSequence5, $txtMultiplier, $txtRemarks
											 					, $hidBatTrialCreatedAt, $hidBatTrialUpdatedAt, $hidBatTrialCreatedId, $hidBatTrialUpdatedId);
			$qryTrials->execute();
			$resultTrials = mysqli_stmt_get_result($qryTrials); //return results of query
			$processError1 = mysqli_error($db);

			if ( !empty($processError1) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_batch_ticket.php'.'</td><td>'.$processError1.' near line 277.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{

				$qryLastBatchTicketTrialId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

				while($row = mysqli_fetch_assoc($qryLastBatchTicketTrialId))
				{
					if ( $hidBatchTicketId && $hidBatchTicketTrialId ) {
						$BatchTicketTrialId = $hidBatchTicketTrialId;
					}else{
						$BatchTicketTrialId = $row['LAST_INSERT_ID()'];
					}	//end if-else BatchTicketTrialId

				}// end while qryLastBatchTicketTrialId
				
				if( mysqli_affected_rows($db) > 0 ) 
				{	
					$i = 0;
					foreach($_POST['txtRM_type_id'] as $key => $itemValue)
					{	
						if (!$hidBatchTicketTrialId) {
							$BatchTicketTrialItemId = 0;
						}//end if hidNBatchTicketTrialItemId

						if ($itemValue)
						{
							$qryTrialItem = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_Items_CRU( ?, ?, ?, ?, ?, ? )");
							mysqli_stmt_bind_param($qryTrialItem, 'iiiidd', $BatchTicketTrialItemId[$key], $BatchTicketTrialId, $RM_type_id[$key]
																		  , $RM_id[$key], $Phr[$key], $Qty[$key]);
							$qryTrialItem->execute();
							$resultTrialItem = mysqli_stmt_get_result($qryTrialItem); //return results of query
							$processError2 = mysqli_error($db);

							if ( !empty($processError2) ){
								error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_batch_ticket.php'.'</td><td>'.$processError2.' near line 312.</td></tr>', 3, "errors.php");
								header("location: error_message.html");
							}elseif ( !$txtNRm_id ){
								if ( $hidBatchTicketId )
									$_SESSION['SUCCESS']  = 'Batch Ticket successfully updated.';
								else
									$_SESSION['SUCCESS']  = 'Batch Ticket successfully added.';
								header("location:new_batch_ticket_nrm.php?id=".$BatchTicketId."&tid=0");
							}else{
								echo 'i: '.$i.'<br>';
								if ( $i == ($count-1) ){
									if (!$hidBatchTicketTrialId) {
										$hidNBatchTicketTrialItemId = 0;
									}//end if hidNBatchTicketTrialItemId

									$qryNTrialItem = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_Items_CRU( ?, ?, ?, ?, ?, ? )");
									mysqli_stmt_bind_param($qryNTrialItem, 'iiiidd', $hidNBatchTicketTrialItemId, $BatchTicketTrialId, $txtNRm_type_id
																				  , $txtNRm_id, $txtNPHR, $txtNQuantity);
									$qryNTrialItem->execute();
									$resultNTrialItem = mysqli_stmt_get_result($qryNTrialItem); //return results of query
									$processError3 = mysqli_error($db);

									if ( !empty($processError3) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_batch_ticket.php'.'</td><td>'.$processError3.' near line 341.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										if ( $hidBatchTicketId )
											$_SESSION['SUCCESS']  = 'Batch Ticket successfully updated.';
										else
											$_SESSION['SUCCESS']  = 'Batch Ticket successfully added.';
										header("location:new_batch_ticket_nrm.php?id=".$BatchTicketId."&tid=0");

									}// end if-else !empty($processError3)

								}// end while $i > $count

							}// end if processError2		

						}// end if itemValue
						$i++;
					}// end foreach $_POST['txtRM']

				}// end if 2nd mysqli_affected_rows($db) > 0

			}// end if-else !empty($processError1)

		}// end if 1st mysqli_affected_rows($db) > 0

	}// end if-else !empty($processError)

############# Committing in Database

	}// end if-else errno
?>