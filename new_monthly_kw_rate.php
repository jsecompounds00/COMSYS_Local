<!-- <html>
<head>
<?php
	

	

?>
  	<script src="js/datetimepicker_css.js"></script>
  	<script src="js/jscript.js"></script>
</head>
<body>
<div class='form'>
	
		<?php

			// if ( $priceID ){
			// 	echo "<span>Edit Monthly Kilowatt Rate</span>";
			// }else{
			// 	echo "<span>New </span>";
			// }


			
		?>
		
	</form>
	<div>
		<?php
			// require('/include/footer.php'); 
			// require("include/database_close.php");
		?>
	</div>
</div>
</body>
</html> -->




<html>
	<head>
		<?php
			require("/include/database_connect.php"); 

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_monthly_kw_rate.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$priceID = $_GET['id'];

				$qry = mysqli_prepare($db, "CALL sp_Monthly_KW_Rate_Query(?)");
				mysqli_stmt_bind_param($qry, 'i', $priceID);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry); //return results of query
				$processError = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_monthly_kw_rate.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{	
					while($row = mysqli_fetch_assoc($result))
					{
						$priceID = $row['id'];
						$month = $row['month'];
						$year = $row['year'];
						$price = $row['price'];

					}
					$db->next_result();
					$result->close();
				}

				if ( $priceID ){
					echo "<title>Monthly Kilowatt Rate - Update</title>";
				}else{
					echo "<title>Monthly Kilowatt Rate - Create</title>";
				}

			}
		?>
	</head>
	<body>

		<form method='post' action='process_new_monthly_kw_rate.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $priceID ? "Edit " : "" );?> Monthly Kilowatt Rate </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form"> 
					<tr>
						<td> Month / Year: </td>
						<td>
							<select name='sltMonth'>
								<option></option>
								<?php
									$qryM = "SELECT * FROM months ORDER BY equal_int";
									$resultM = mysqli_query($db, $qryM);
									$processErrorM = mysqli_error($db);
									if ( !empty($processErrorM) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_monthly_kw_rate.php'.'</td><td>'.$processErrorM.' near line 275.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										while( $row = mysqli_fetch_assoc($resultM) ){
											$month_id = $row['equal_int'];
											$month_name = $row['month'];

											if ( $priceID ){
												if ( $month == $month_id )
													echo "<option value='".$month_id."' selected>".$month_name."</option>";
												else echo "<option value='".$month_id."'>".$month_name."</option>";
											}else echo "<option value='".$month_id."'>".$month_name."</option>";

										}
										$db->next_result();
										$resultM->close();
									}
								?>
							</select>
						</td>
						<td>
							<select name='sltYear'>
								<option></option>
								<?php
									$qryM = "SELECT * FROM years";
									$resultM = mysqli_query($db, $qryM);
									$processErrorM = mysqli_error($db);
									if ( !empty($processErrorM) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_monthly_kw_rate.php'.'</td><td>'.$processErrorM.' near line 275.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										while( $row = mysqli_fetch_assoc($resultM) ){
											$yearid = $row['year'];
											
											if ( $priceID ){
												if ( $year == $yearid )
													echo "<option value='".$yearid."' selected>".$yearid."</option>";
												else echo "<option value='".$yearid."'>".$yearid."</option>";
											}else echo "<option value='".$yearid."'>".$yearid."</option>";

										}
										$db->next_result();
										$resultM->close();
									}
								?>  
							</select>
						</td>
					</tr>
					<tr>
						<td> Price/kW: </td>
						<td colspan='2'> 
							<input type='text' name='txtPrice' value='<?php echo ( $priceID ? $price : "" );?>'>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='monthly_kw_rate.php?page=1&search=&qsone='">
							<input type='hidden' name='hidPriceID' value="<?php echo $priceID;?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>