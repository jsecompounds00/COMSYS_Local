<?php
############# Start session
	session_start();
	ob_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;

############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_jrd.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidJRDID = $_POST['hidJRDID'];	
		$txtJRDNumber = $_POST['txtJRDNumber'];
		$txtRequestedDate = $_POST['txtRequestedDate'];
		$txtNeededDate = $_POST['txtNeededDate'];
		$sltExistingCustomer = $_POST['sltExistingCustomer'];
		$chkNewCustomer = $_POST['chkNewCustomer'];
		$txtNewCustomer = $_POST['txtNewCustomer'];
		$txtContactPerson = $_POST['txtContactPerson'];
		$radRequestType = $_POST['radRequestType'];	

	######### Product Development
		$chkExtrusion = $_POST['chkExtrusion'];	
		$radExtrusionType = $_POST['radExtrusionType'];	
		$txtOtherExtrusionType = $_POST['txtOtherExtrusionType'];
		$radThermalRate = $_POST['radThermalRate'];	
		$txtOtherThermalRate = $_POST['txtOtherThermalRate'];	
		$chkMolding = $_POST['chkMolding'];	
		$radMoldingType = $_POST['radMoldingType'];	
		$txtOtherMoldingType = $_POST['txtOtherMoldingType'];	
		$chkOtherActivity = $_POST['chkOtherActivity'];	
		$txtOtherActivity = $_POST['txtOtherActivity'];
		$chkUL = $_POST['chkUL'];	
		$chkNone = $_POST['chkNone'];	
		$chkPNS = $_POST['chkPNS'];	
		$chkOtherReq = $_POST['chkOtherReq'];	
		$txtOtherReq = $_POST['txtOtherReq'];

	######### Product Modification
		$chkCost = $_POST['chkCost'];
		$chkProcessabilityImprove = $_POST['chkProcessabilityImprove'];
		$chkPropertyEnhance = $_POST['chkPropertyEnhance'];
		$chkLegalReq = $_POST['chkLegalReq'];
		$chkOthers = $_POST['chkOthers'];
		$txtOtherReason = $_POST['txtOtherReason'];

	######### Product Modification
		$txtCompetitorsProduct = $_POST['txtCompetitorsProduct'];
		$txtCompetitorsName = $_POST['txtCompetitorsName'];
		$chkNeedsReplication = $_POST['chkNeedsReplication'];

	######### Product Modification and Evaluation
		$sltFGItem = $_POST['sltFGItem'];

	// $txtJRDNumber = str_replace('\\r\\n', '<br>', $txtJRDNumber);
	// $txtNewCustomer = str_replace('\\r\\n', '<br>', $txtNewCustomer);
	// $txtContactPerson = str_replace('\\r\\n', '<br>', $txtContactPerson);
	// $txtOtherExtrusionType = str_replace('\\r\\n', '<br>', $txtOtherExtrusionType);
	// $txtOtherThermalRate = str_replace('\\r\\n', '<br>', $txtOtherThermalRate);
	// $txtOtherMoldingType = str_replace('\\r\\n', '<br>', $txtOtherMoldingType);
	// $txtOtherActivity = str_replace('\\r\\n', '<br>', $txtOtherActivity);
	// $txtOtherReq = str_replace('\\r\\n', '<br>', $txtOtherReq);
	// $txtOtherReason = str_replace('\\r\\n', '<br>', $txtOtherReason);
	// $txtCompetitorsProduct = str_replace('\\r\\n', '<br>', $txtCompetitorsProduct);
	// $txtCompetitorsName = str_replace('\\r\\n', '<br>', $txtCompetitorsName);
					
	// $txtJRDNumber = str_replace('\\', '', $txtJRDNumber);
	// $txtNewCustomer = str_replace('\\', '', $txtNewCustomer);
	// $txtContactPerson = str_replace('\\', '', $txtContactPerson);
	// $txtOtherExtrusionType = str_replace('\\', '', $txtOtherExtrusionType);
	// $txtOtherThermalRate = str_replace('\\', '', $txtOtherThermalRate);
	// $txtOtherMoldingType = str_replace('\\', '', $txtOtherMoldingType);
	// $txtOtherActivity = str_replace('\\', '', $txtOtherActivity);
	// $txtOtherReq = str_replace('\\', '', $txtOtherReq);
	// $txtOtherReason = str_replace('\\', '', $txtOtherReason);
	// $txtCompetitorsProduct = str_replace('\\', '', $txtCompetitorsProduct);
	// $txtCompetitorsName = str_replace('\\', '', $txtCompetitorsName);

	############# SESSION, keeping last input value
		$_SESSION['txtJRDNumber'] = $txtJRDNumber;
		$_SESSION['txtRequestedDate'] = $txtRequestedDate;
		$_SESSION['txtNeededDate'] = $txtNeededDate;
		$_SESSION['sltExistingCustomer'] = $sltExistingCustomer;
		$_SESSION['chkNewCustomer'] = $chkNewCustomer;
		$_SESSION['txtNewCustomer'] = $txtNewCustomer;
		$_SESSION['txtContactPerson'] = $txtContactPerson;
		$_SESSION['radRequestType'] = $radRequestType;
	############# SESSION, PRODUCT DEVELOPMENT
		$_SESSION['chkExtrusion'] = $chkExtrusion;
		$_SESSION['radExtrusionType'] = $radExtrusionType;
		$_SESSION['txtOtherExtrusionType'] = $txtOtherExtrusionType;
		$_SESSION['radThermalRate'] = $radThermalRate;
		$_SESSION['txtOtherThermalRate'] = $txtOtherThermalRate;
		$_SESSION['chkMolding'] = $chkMolding;
		$_SESSION['radMoldingType'] = $radMoldingType;
		$_SESSION['txtOtherMoldingType'] = $txtOtherMoldingType;
		$_SESSION['chkOtherActivity'] = $chkOtherActivity;
		$_SESSION['txtOtherActivity'] = $txtOtherActivity;
		$_SESSION['chkUL'] = $chkUL;
		$_SESSION['chkNone'] = $chkNone;
		$_SESSION['chkPNS'] = $chkPNS;
		$_SESSION['chkOtherReq'] = $chkOtherReq;
		$_SESSION['txtOtherReq'] = $txtOtherReq;
	############# SESSION, PRODUCT MODIFICATION
		$_SESSION['chkCost'] = $chkCost;
		$_SESSION['chkProcessabilityImprove'] = $chkProcessabilityImprove;
		$_SESSION['chkPropertyEnhance'] = $chkPropertyEnhance;
		$_SESSION['chkLegalReq'] = $chkLegalReq;
		$_SESSION['chkOthers'] = $chkOthers;
		$_SESSION['txtOtherReason'] = $txtOtherReason;
	######### Product Evaluation
		$_SESSION['txtCompetitorsProduct'] = $txtCompetitorsProduct;
		$_SESSION['txtCompetitorsName'] = $txtCompetitorsName;
		$_SESSION['chkNeedsReplication'] = $chkNeedsReplication;
	######### Product Modification and Evaluation
		$_SESSION['sltFGItem'] = $sltFGItem;


############# Input Validation
		if ( $txtJRDNumber == '' ){
			$errmsg_arr[] = "* JRD number can't be blank.";
			$errflag = true;
		}
		$valRequestedDate = validateDate($txtRequestedDate, 'Y-m-d');
		if ( $valRequestedDate != 1 ){
			$errmsg_arr[] = '* Invalid date requested.';
			$errflag = true;
		}
		$valNeededDate = validateDate($txtNeededDate, 'Y-m-d');
		if ( $txtNeededDate != '' && $valNeededDate != 1 ){
			$errmsg_arr[] = '* Invalid date needed.';
			$errflag = true;
		}elseif ( $txtNeededDate == '' ) {
			$txtNeededDate = NULL;
		}
		if ( $chkNewCustomer == 1 ){
			if ( $txtNewCustomer == '' ){
				$errmsg_arr[] = "* New customer can't be blank.";
				$errflag = true;
			}
		}else{
			if ( !($sltExistingCustomer) ){
				$errmsg_arr[] = '* Select customer.';
				$errflag = true;
			}
		}

		if ( !($radRequestType) ){
			$errmsg_arr[] = '* Choose request type.';
			$errflag = true;
		}else{
			if ( $radRequestType == 'Development' ){ ### Product Development
				if ( $chkExtrusion == 1 ){ ### Extrusion
					if ( !($radExtrusionType) ){ ### Extrusion Type
						$errmsg_arr[] = '* Choose extrusion type.';
						$errflag = true;
					}else{
						if ( $radExtrusionType == 'OtherExtrusionType' && $txtOtherExtrusionType == '' ){
							$errmsg_arr[] = "* Other extrusion type can't be blank.";
							$errflag = true;
						}
					}
					
					if ( !($radThermalRate) ){ ### Thermal Rating
						$errmsg_arr[] = '* Choose thermal rating / grade.';
						$errflag = true;
					}else{
						if ( $radThermalRate == 'OtherThermalRate' && $txtOtherThermalRate == '' ){
							$errmsg_arr[] = "* Other thermal rating / grade can't be blank.";
							$errflag = true;
						}
					}
				}

				if ( $chkMolding == 1 ){ ### Molding
					if ( !($radMoldingType) ){ ### Molding Type
						$errmsg_arr[] = '* Choose molding type.';
						$errflag = true;
					}else{
						if ( $radMoldingType == 'OtherMoldingType' && $txtOtherMoldingType == '' ){
							$errmsg_arr[] = "* Other molding type can't be blank.";
							$errflag = true;
						}
					}
				}

				if ( $chkOtherActivity == 1 ){ ### Other Activity
					if ( $txtOtherActivity == '' ){ ### Product's end use
						$errmsg_arr[] = "* State product's end use.";
						$errflag = true;
					}
				}

				if ( $chkOtherReq == 1 ){### Other Requirements
					if ( $txtOtherReq == '' ){
						$errmsg_arr[] = "* Sepcify other requirements.";
						$errflag = true;
					}
				}

			} ### Product Development
			elseif( $radRequestType == 'Modification' ){ ### Product Modification
				if ( !($sltFGItem) ){ ### FG Item
					$errmsg_arr[] = "* Select item to be modified.";
					$errflag = true;
				}

				if ( $chkCost != 1 && $chkProcessabilityImprove != 1 && $chkPropertyEnhance != 1 && $chkLegalReq != 1 && $chkOthers != 1  ){ ### Requirement
					$errmsg_arr[] = "* Choose at least (1) one reason for modification.";
					$errflag = true;
				}else{
					if ( $chkOthers == 1 && $txtOtherReason == '' ){ ### Other Reason
						$errmsg_arr[] = "* Other reason can't be blank.";
						$errflag = true;
					}
				}
			} ### Product Modification
			elseif( $radRequestType == 'Evaluation' ){ ### Product Evaluation
				if ( $txtCompetitorsProduct == '' ){ ### Competitor's Product
					$errmsg_arr[] = "* Product name can't be blank.";
					$errflag = true;
				}

				if ( $txtCompetitorsName == '' ){ ### Competitor's Name
					$errmsg_arr[] = "* Source/Manufacturer can't be blank.";
					$errflag = true;
				}

				if ( !($sltFGItem) ){ ### Product for comparison
					$errmsg_arr[] = "* Select product for comparison.";
					$errflag = true;

				}
			} ### Product Evaluation
		}

	######### Validation on Checkbox
		if($hidJRDID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidJRDID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_jrd.php?id=$hidJRDID");
			exit();
		}
############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_JRD_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'isssiisssissssissisiiiisiiiiisssissiii', $hidJRDID, $txtJRDNumber, $txtRequestedDate, $txtNeededDate
												, $sltExistingCustomer, $chkNewCustomer, $txtNewCustomer, $txtContactPerson, $radRequestType
												, $chkExtrusion, $radExtrusionType, $txtOtherExtrusionType, $radThermalRate, $txtOtherThermalRate
												, $chkMolding, $radMoldingType, $txtOtherMoldingType, $chkOtherActivity, $txtOtherActivity, $chkUL
												, $chkPNS, $chkNone, $chkOtherReq, $txtOtherReq, $chkCost, $chkProcessabilityImprove, $chkPropertyEnhance
												, $chkLegalReq, $chkOthers, $txtOtherReason, $txtCompetitorsProduct, $txtCompetitorsName
												, $chkNeedsReplication, $createdAt, $updatedAt, $createdId, $updatedId, $sltFGItem);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_jrd.php'.'</td><td>'.$processError.' near line 117.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidJRDID)
				$_SESSION['SUCCESS']  = 'Successfully updated JRD.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added JRD.';
			//echo $_SESSION['SUCCESS'];
			header("location:jrd.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>