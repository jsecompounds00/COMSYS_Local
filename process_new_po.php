<?php
############# Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_po.php'.'</td><td>'.$error.' near line 75.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidPoId = $_POST['hidPoId'];
		$sltPRE = implode(',', $_POST['sltPRE']);
		$txtPONum = $_POST['txtPONum'];
		$txtDeliveryDate = $_POST['txtDeliveryDate'];
		$txtPODate = $_POST['txtPODate'];
		$sltSupplier = $_POST['sltSupplier'];
		$txtRemarks = $_POST['txtRemarks'];
		$radItemType = $_POST['radItemType'];
		$hidErrorDuplicate = $_POST['hidErrorDuplicate'];
		$radDivision = $_POST['radDivision'];

		if($hidPoId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
			$updatedAt = date('Y-m-d H:i:s');
		if($hidPoId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
			$updatedId = $_SESSION['SESS_USER_ID'];
	
		foreach ($_POST['hidPREId'] as $PREId) {
			$PREId = array();
			$PREId = $_POST['hidPREId'];
		}
		foreach ($_POST['hidPREItemId'] as $PREItemId) {
			$PREItemId = array();
			$PREItemId = $_POST['hidPREItemId'];
		}
		foreach ($_POST['hidQuantity'] as $Quantity) {
			$Quantity = array();
			$Quantity = $_POST['hidQuantity'];
		}
		foreach ($_POST['hidUOMId'] as $UOMId) {
			$UOMId = array();
			$UOMId = $_POST['hidUOMId'];
		}
		foreach ($_POST['hidSupplyId'] as $SupplyId) {
			$SupplyId = array();
			$SupplyId = $_POST['hidSupplyId'];
		}
		foreach ($_POST['txtUnitPrice'] as $UnitPrice) {
			$UnitPrice = array();
			$UnitPrice = $_POST['txtUnitPrice'];
		}
		foreach ($_POST['hidPOItemId'] as $hidPOTransItemId) {
			$hidPOTransItemId = array();
			$hidPOTransItemId = $_POST['hidPOItemId'];
		}
		foreach ($_POST['chkCanceled'] as $Canceled) {
			$Canceled = array();
			
			if ( $hidPoId ){
				$Canceled = $_POST['chkCanceled'];
			}else{
				$Canceled = 0;
			}
		}
		foreach ($_POST['chkServed'] as $Served) {
			$Served = array();
			
			if ( $hidPoId ){
				$Served = $_POST['chkServed'];
			}else{
				$Served = 0;
			}
		}
		foreach ($_POST['txtReason'] as $txtReason) {
			$txtReason = array();
			$txtReason = $_POST['txtReason'];
		}

############# Input Validation
		if ( isset( $radDivision ) && $radDivision == 'Pipes' ){
			$radDivision = 'Pipes';
		}elseif ( isset( $radDivision ) && $radDivision == 'Compounds' ){
			$radDivision = 'Compounds';
		}elseif ( isset( $radDivision ) && $radDivision == 'Corporate' ){
			$radDivision = 'Corporate';
		}elseif ( isset( $radDivision ) && $radDivision == 'PPR' ){
			$radDivision = 'PPR';
		}else{
			$errmsg_arr[] = '* Division is required.';
			$errflag = true;
		}

		if ( $hidErrorDuplicate == 1 ){
			$errmsg_arr[] = '* PO number already exists..';
			$errflag = true;
		}
		if ( !$hidPoId && strlen($sltPRE) < 1 ){
			$errmsg_arr[] = '* Choose at least one (1) PRE Number to continue.';
			$errflag = true;
		}else{
			if ( $txtPONum == '' ){
				$errmsg_arr[] = '* Invalid PO Number.';
				$errflag = true;
			}
			$valPODate = validateDate($txtPODate, 'Y-m-d');
			if ( $valPODate != 1 ){
				$errmsg_arr[] = '* Invalid PO Date.';
				$errflag = true;
			}
			$valDelDate = validateDate($txtDeliveryDate, 'Y-m-d');
			if ( $valDelDate != 1 ){
				$errmsg_arr[] = '* Invalid Expected Delivery Date.';
				$errflag = true;
			}
			if ( !$sltSupplier ){
				$errmsg_arr[] = '* Supplier is missing.';
				$errflag = true;
			}

			$ctr = count($PREItemId);
			$i = 0;

			do{
				if ( $UnitPrice[$i] == '' || !is_numeric($UnitPrice[$i]) ){
					$errmsg_arr[] = '* Invalid Unit Price.';
					$errflag = true;
				}
				if ( $hidPoId && $Canceled[$i] && $txtReason[$i] == '' ){
					$errmsg_arr[] = '* Remarks is required for canceled item. (line '.($i+1).')';
					$errflag = true;
				}elseif ( !$Canceled[$i] ){
					$Canceled[$i] = 0;
				}else{
					$Canceled[$i] = 1;
				}
				if ( $Served[$i] && $txtReason[$i] == '' ){
					$errmsg_arr[] = '* Remarks is required for manually served item. (line '.($i+1).')';
					$errflag = true;
				}elseif ( !$Served[$i] ){
					$Served[$i] = 0;
				}elseif ( $Served[$i] && $txtReason[$i] != '' ){
					$Served[$i] = 1;
				}

				$i++;
			}while ( $i < $ctr );
		}


	// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);

	// $txtRemarks = str_replace('\\', '', $txtRemarks);

############# SESSION, keeping last input value

	$_SESSION['SESS_PO_COUNT'] = $ctr;
	$_SESSION['SESS_PO_PRE'] = $sltPRE;
	$_SESSION['SESS_PO_PONum'] = $txtPONum;
	$_SESSION['SESS_PO_DeliveryDate'] = $txtDeliveryDate;
	$_SESSION['SESS_PO_PODate'] = $txtPODate;
	$_SESSION['SESS_PO_Supplier'] = $sltSupplier;
	$_SESSION['SESS_PO_Remarks'] = $txtRemarks;
	$_SESSION['SESS_PO_ItemType'] = $radItemType;
	$_SESSION['SESS_PO_Division'] = $radDivision;

	for ($sess_count=0; $sess_count < $ctr ; $sess_count++) { 
		$_SESSION['SESS_PO_Quantity'][$sess_count] = $Quantity[$sess_count];
		$_SESSION['SESS_PO_UnitPrice'][$sess_count] = $UnitPrice[$sess_count];
		$_SESSION['SESS_PO_Reason'][$sess_count] = $txtReason[$sess_count];
	}

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_PO.$hidPoId);
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_PO_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
	mysqli_stmt_bind_param($qry, 'isssisssiiss', $hidPoId, $txtPONum, $txtPODate, $txtDeliveryDate, $sltSupplier
											  , $txtRemarks, $createdAt, $updatedAt, $createdId, $updatedId, $radItemType, $radDivision);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_po.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidPoId ) {
				$poTransId = $hidPoId;
			}else{
				$poTransId = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['hidPREItemId'] as $key => $itemValue)
			{	
				if (!$hidPoId) {
					$hidPOTransItemId = 0;
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_PO_Item_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiiidiidssiiiis', $hidPOTransItemId[$key], $poTransId, $PREId[$key],
																$PREItemId[$key], $Quantity[$key], $UOMId[$key], $SupplyId[$key],
																$UnitPrice[$key], $createdAt, $updatedAt, $createdId, $updatedId, 
											 					$Served[$key], $Canceled[$key], $txtReason[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_po.php'.'</td><td>'.$processError1.' near line 171.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidPoId )
							$_SESSION['SUCCESS']  = "PO# $txtPONum is successfully updated.";
						else
							$_SESSION['SUCCESS']  = "PO# $txtPONum is successfully added.";
						header("location: po_monitoring.php?page=".$_SESSION["page"]."&po_date=".$_SESSION["po_date"]."&po_number=".$_SESSION["po_number"]."&pre_number=".$_SESSION["pre_number"]);
					}				
				}
			}
		}
	}
}
			require("include/database_close.php");
?>