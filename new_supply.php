<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_supply.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$supplyId = $_GET["id"];

				if($supplyId)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Supplies_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $supplyId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_supply.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							// $suppliesId = $row["supply_id"];
							$supplyTypeId = $row["supply_type_id"];
							$supplyType = $row["supply_type"];
							$supply = htmlspecialchars($row["supply"]);
							$description = htmlspecialchars($row["description"]);
							$db_compounds = $row["compounds"];
							$db_pipes = $row["pipes"];
							$db_corporate = $row["corporate"];
							$db_ppr = $row["ppr"];
							$active = $row["active"];
							$uomId = $row["unit_of_measure_id"];
							$uomCode = htmlspecialchars($row["uom_code"]);
							$comments = htmlspecialchars($row["comments"]);
							$pipeLine = "0,".$row["pipe_line"];
							$mtID = $row["miscellaneous_type_id"];
							$createdAt = $row["created_at"];
							$createdId = $row["created_id"];
						}
					}

					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.supplies";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_supply.php"."</td><td>".$processErrorPI." near line 69.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($supplyId, $id, TRUE) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_supply.php</td><td>The user tries to edit a non-existing supply_id.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					
					echo "<title>Supply - Edit</title>";
				}
				else
				{
					echo "<title>Supply - Add</title>";
				}
			}
		?>
	</head>
	<body>

		<form method="post" action="process_new_supply.php">

			<?php
				require("/include/header.php");
			require("/include/init_unset_values/supplies_init_value.php");

				if ( $supplyId ){
					if( $_SESSION["edit_supplies"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION["add_supplies"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $supplyId ? "Edit ".$supply : "New Supply" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td >Supply Type:</td>
						<td>
							<select name='sltSupplyType'>	
								<option value='0'></option>					
								<?php 
									$qryST = "CALL sp_SupplyType_Dropdown(0, 1)";
									$resultST = mysqli_query($db, $qryST);
									$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_supply.php'.'</td><td>'.$processError1.' near line 115.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultST))
										{
											$STypeID = $row['supply_type_id'];
											$STypeName = $row['supply_type'];

											if ( $supplyId ){
												if ( $STypeID == $supplyTypeId ){
													echo "<option value='".$STypeID."' selected>".$STypeName."</option>";
												}else{
													echo "<option value='".$STypeID."'>".$STypeName."</option>";
												}
											}else{
												if ( $STypeID == $initSUPSupplyTypeId ){
													echo "<option value='".$STypeID."' selected>".$STypeName."</option>";
												}else{
													echo "<option value='".$STypeID."'>".$STypeName."</option>";
												}
											}
										}

										$db->next_result();
										$resultST->close();
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td >Supply Name:</td>
						<td>
							<input type="text" name="txtSupply" value="<?php echo ( $supplyId ? $supply : $initSUPSupply );?>">
						</td>
					</tr>
					<tr>
						<td valign='top'>Description:</td>
						<td>
							<textarea name='txtDescription'><?php
								if($supplyId){
									echo $description;
								}
								else{
									echo $initSUPDescription;
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>Applicable To:</td>
						<td colspan="3" >
							<input type='checkbox' name='chkCmpds' id='COMPOUNDS' 
								<?php echo( $supplyId ? ( $db_compounds ? "checked" : "" ) : ( $initSUPCmpds ? "checked" : "" ) );?>>
								<label for='COMPOUNDS'>COMPOUNDS</label>

							<input type='checkbox' name='chkPips' id='PIPES' 
								<?php echo( $supplyId ? ( $db_pipes ? "checked" : "" ) : ( $initSUPPips ? "checked" : "" ) );?>>
								<label for='PIPES'>PIPES</label>

							<input type='checkbox' name='chkCorp' id='CORPORATE' 
								<?php echo( $supplyId ? ( $db_corporate ? "checked" : "" ) : ( $initSUPCorp ? "checked" : "" ) );?>>
								<label for='CORPORATE'>CORPORATE</label>

							<input type='checkbox' name='chkPPR' id='PPR' 
								<?php echo( $supplyId ? ( $db_ppr ? "checked" : "" ) : ( $initSUPPPR ? "checked" : "" ) );?>>
								<label for='PPR'>PPR</label>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type='checkbox' name='chkActive' <?php echo( $supplyId ? ( $active ? "checked" : "" ) : ( $initSUPActive ? "checked" : "" ) );?>>
						</td>
					</tr>
					<tr>
						<td>UOM:</td>
						<td>
							<select name='sltUom'>
								<?php 
									$qryU = "CALL sp_UOM_Dropdown(0)"; //with default blank option for new supply
									$resultU = mysqli_query($db, $qryU);
									$processError2 = mysqli_error($db);

									if(!empty($processError2))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_supply.php'.'</td><td>'.$processError2.' near line 238.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultU))
										{
											$uID = $row['id'];
											$uCode = $row['code'];

											if ( $supplyId ){
												if ( $uID == $uomId ){
													echo "<option value='".$uID."' selected>".$uCode."</option>";
												}else{
													echo "<option value='".$uID."'>".$uCode."</option>";
												}
											}else{
												if ( $uID == $initSUPUom ){
													echo "<option value='".$uID."' selected>".$uCode."</option>";
												}else{
													echo "<option value='".$uID."'>".$uCode."</option>";
												}
											}
										}
										
										$db->next_result();
										$resultU->close();
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td valign='top'>Comments:</td>
						<td>
							<textarea name='txtComments'><?php
								if($supplyId){
									echo $comments;
								}
								else{
									echo $initSUPComments;
								}
							?></textarea>
						</td>
					</tr>
					<?php
						if ( $supplyId && $db_compounds ){
					?>
							<tr>
								<td>Miscellaneous Type:</td>
								<td>
									<select name="sltMiscellaneous">
										<?php
											$qryU = "CALL sp_Budget_Miscellaneous_Type_Dropdown(1)"; //with default blank option for new supply
											$resultU = mysqli_query($db, $qryU);
											$processError2 = mysqli_error($db);

											if(!empty($processError2))
											{
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_supply.php'.'</td><td>'.$processError2.' near line 238.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultU))
												{
													$DDMTID = $row['DDMTID'];
													$DDMTName = $row['DDMTName'];

													if ( $supplyId ){
														if ( $DDMTID == $mtID ){
															echo "<option value='".$DDMTID."' selected>".$DDMTName."</option>";
														}else{
															echo "<option value='".$DDMTID."'>".$DDMTName."</option>";
														}
													}else{
														if ( $DDMTID == $initSUPmisc ){
															echo "<option value='".$DDMTID."' selected>".$DDMTName."</option>";
														}else{
															echo "<option value='".$DDMTID."'>".$DDMTName."</option>";
														}
													}
												}
												
												$db->next_result();
												$resultU->close();
											}
										 ?>
									</select>
								</td>
							</tr>
					<?php
						}
					?>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveSupply" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='supplies.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidSupplyId" value="<?php echo $supplyId;?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $supplyId ? $createdAt : date("Y-m-d H:i:s") );?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $supplyId ? $createdId : 0 );?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>