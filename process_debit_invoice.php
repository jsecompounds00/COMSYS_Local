<?php
############# Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }

############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_debit_invoice.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitize the POST values
		$hidDebitId = $_POST['hidDebitId'];
		$hidProcessType = $_POST['hidProcessType'];
		$sltCustomer = $_POST['sltCustomer'];
		$sltTransType = $_POST['sltTransType'];
		$txtInvoiceDate = $_POST['txtInvoiceDate'];
		$txtCheckDate = $_POST['txtCheckDate'];
		$txtCheckReturnedDate = $_POST['txtCheckReturnedDate'];
		$hidCheckEncode = $_POST['hidCheckEncode'];
		$txtCheckAmount = NULL;
		$BankCode = $_POST['sltBankCode'];
		$Checknumber = $_POST['txtChecknumber'];
		$hidJacketType = $_POST['hidJacketType'];
		// $txtCheckDate = NULL;
	
		foreach ($_POST['hidDebitItemID'] as $DebitItemID) {
			$DebitItemID = array();
			$DebitItemID = $_POST['hidDebitItemID'];
		}
		foreach ($_POST['txtInvoiceNumber'] as $InvoiceNumber) {
			$InvoiceNumber = array();
			$InvoiceNumber = $_POST['txtInvoiceNumber'];
		}
		foreach ($_POST['txtAmount'] as $Amount) {
			$Amount = array();
			$Amount = $_POST['txtAmount'];
		}
		foreach ($_POST['txtDiscount'] as $Discount) {
			$Discount = array();
			$Discount = $_POST['txtDiscount'];
		}

		$ctr = count($InvoiceNumber);

		$i = 0;

############# Input Validations

		if ( !$sltCustomer ){
			$errmsg_arr[] = '* Select customer.';
			$errflag = true;
		}
		$valInvoiceDate = validateDate($txtInvoiceDate, 'Y-m-d');
		$valCheckReturnedDate = validateDate($txtCheckReturnedDate, 'Y-m-d');

		if ( $sltTransType == 'IAR'){

			if ( $valInvoiceDate != 1 ){
				$errmsg_arr[] = '* Invalid invoice date.';
				$errflag = true;
			}

			do{
				if ( $InvoiceNumber[$i] == '' ){
					if ($i == 0){
						$errmsg_arr[] = '* Enter invoice number. (line '.($i+1).')';
						$errflag = true;
					}else{
						if ( !empty( $Amount[$i] ) || !empty( $Discount[$i] ) ){
							$errmsg_arr[] = '* Enter invoice number. (line '.($i+1).')';
							$errflag = true;
						}
					}
				}else{
					// if ( $InvoiceNumber[$i-1] == '' ){
					// 	if ( $i != 0 ){
					// 		$errmsg_arr[] = '* Skip Line is not allowed.';
					// 		$errflag = true;
					// 	}
					// }

					$qryInvoice = "CALL sp_Customer_Jacket_Invoice_Query()";
					$resultInvoice = mysqli_query($db, $qryInvoice);
					$processErrorInvoice = mysqli_error($db);
					if(!empty($processErrorInvoice))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_debit_invoice.php'.'</td><td>'.$processErrorInvoice.' near line 67.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else
					{ 
							$invoice_number = array();
						while ( $rowInvoice = mysqli_fetch_assoc( $resultInvoice ) )	{
							$invoice_number[] = $rowInvoice["invoice_number"];
						}
						$db->next_result();
						$resultInvoice->close();

						if ( !$hidDebitId ){
							if( in_array($InvoiceNumber[$i], $invoice_number, TRUE) ){ 
								$errmsg_arr[] = '* Invoice number already exists. (line '.($i+1).')';
								$errflag = true;
							}else{
								if ( !is_numeric( $Amount[$i] ) ){
									$errmsg_arr[] = '* Invalid value for amount. (line '.($i+1).')';
									$errflag = true;
								}
								if ( !empty( $Discount[$i] ) && !is_numeric( $Discount[$i] ) ){
									$errmsg_arr[] = '* Invalid value for discount. (line '.($i+1).')';
									$errflag = true;
								}
							}
						}
					}

				}

				$i++;
			}while ( $i < $ctr );

		} // if Transaction type is Invoice AR
		elseif ( $sltTransType == 'PRC' ){

			if ( $valCheckReturnedDate != 1 ){
				$errmsg_arr[] = '* Invalid check returned date.';
				$errflag = true;
			}
			if ( !$BankCode ){
				$errmsg_arr[] = '* Select bank code.';
				$errflag = true;
			}
			if ( $Checknumber == '' ){
				$errmsg_arr[] = "* Check number can't be blank.";
				$errflag = true;
			}

			do{
				if ( !empty($hidCheckEncode) ){
					$errmsg_arr[] = "* This transaction can't be saved.";
					$errflag = true;
				}

				$i++;
			}while ( $i < $ctr );
		} // elseif Transaction type is Payment Rejected Check

		if($hidDebitId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidDebitId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# SESSION
		$_SESSION["SESS_CJDI_Customer"] = $sltCustomer;
		$_SESSION["SESS_CJDI_TransType"] = $sltTransType;
		$_SESSION["SESS_CJDI_InvoiceDate"] = $txtInvoiceDate;
		$_SESSION["SESS_CJDI_CheckDate"] = $txtCheckDate;
		$_SESSION["SESS_CJDI_CheckReturnedDate"] = $txtCheckReturnedDate;
		$_SESSION["SESS_CJDI_CheckEncode"] = $hidCheckEncode;
		$_SESSION["SESS_CJDI_CheckAmount"] = $txtCheckAmount;
		$_SESSION["SESS_CJDI_kCode"] = $BankCode;
		$_SESSION["SESS_CJDI_cknumber"] = $Checknumber;

		for ( $x = 0; $x <= 10; $x++ ){
			$_SESSION["SESS_CJDI_InvoiceNumber"][$x] = $InvoiceNumber[$x];
			$_SESSION["SESS_CJDI_Amount"][$x] = $Amount[$x];
			$_SESSION["SESS_CJDI_Discount"][$x] = $Discount[$x];
		}

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			if ( $hidDebitId ){
				header("Location: debit_invoice.php?id=".$hidDebitId."&type=".$hidJacketType);
			}else{
				header("Location: debit_invoice.php?id=".$hidDebitId);
			}
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isissssssiid', $hidDebitId, $hidProcessType, $sltCustomer, $sltTransType
											 , $txtInvoiceDate, $txtCheckDate, $txtCheckReturnedDate
											 , $createdAt, $updatedAt, $createdId, $updatedId, $txtCheckAmount);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_debit_invoice.php'.'</td><td>'.$processError.' near line 180.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidDebitId ) {
				$DebitId = $hidDebitId;
			}else{
				$DebitId = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['txtInvoiceNumber'] as $key => $itemValue)
			{	
				if (!$hidDebitId) {
					$DebitItemID = 0;
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Customer_Jacket_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iisisddss', $DebitItemID[$key], $DebitId, $InvoiceNumber[$key]
															   , $BankCode, $Checknumber, $Amount[$key]
															   , $Discount[$key], $createdAt, $updatedAt );
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_debit_invoice.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidDebitId )
							$_SESSION['SUCCESS']  = 'Transaction successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Transaction successfully added.';
						header("location: customer_jacket.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database	
		require("include/database_close.php");
	}
?>
	