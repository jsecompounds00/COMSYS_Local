<html>
	<head>
		<?php 
			require("/include/header.php");
			require("/include/database_connect.php");
			require("/include/init_value.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_customer.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$custId = $_GET['id'];

				if($custId)
				{ 
					if( $_SESSION['edit_customer_pipes'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}

					$qry = "CALL sp_Customer_Pipes_Query('$custId')";

					$result = mysqli_query($db, $qry);

					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_customer.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$custId = $row['id'];
							$name = $row['customers'];
							$active = $row['active'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.customers_pipes";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_customer.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($custId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_customer.php</td><td>The user tries to edit a non-existing customer_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Customer (Pipes) - Edit</title>";
				}
				else{

					if( $_SESSION['add_customer_pipes'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}

					echo "<title>Customer (Pipes) - Add</title>";

				}

			}
		?>
	</head>
	<body>
		<div class='form'>
			<form method='post' action='process_new_customer_pipes.php'>
				<?php
					if ( $custId ){
						echo "<span>Edit $name (Pipes)</span>";
					}else{
						echo "<span>New Customer (Pipes)</span>";
					}

					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
					echo '<ul class="err">';
					foreach($_SESSION['ERRMSG_ARR'] as $msg) {
						echo '<li>'.$msg.'</li>'; 
						}
					echo '</ul>';
					unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table id="tble">
					<tr>
						<td width="100">Name:</td>
						<td>
							<?php 
								if($custId){
									echo "<input type='text' name='txtName' size='40' value='".$name."'>";
								}else{
									echo "<input type='text' name='txtName' size='40' value='$Name'>";
								}
							?>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td colspan="3" width="300">
							<?php
								if($custId){
									if($active)
										echo "<input type='checkbox' name='chkActive' checked='checked'>";
									else echo "<input type='checkbox' name='chkActive'>";
								}
								else {
									if($Active)
										echo "<input type='checkbox' name='chkActive' checked='checked'>";
									else echo "<input type='checkbox' name='chkActive'>";
								}
							?>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="100">
							<input type="submit" name="btnSaveCust" value="Save">	
							<!-- <input type="button" name="btnReset" value="Reset">	 -->
							<?php
								echo "<input type='button' name='btnCancel' value='Cancel' onclick=\"location.href='customer_pipes.php?page=1&search=&qsone='\">";
								if($custId)	
									echo "<input type='hidden' name='hidcustId' value=".$custId.">";
								else echo "<input type='hidden' name='hidcustId' value=0>";
								if($custId)
									echo "<input type='hidden' name='hidCreatedAt' value='".$createdAt."'>";
								else echo "<input type='hidden' name='hidCreatedAt' value='".date('Y-m-d H:i:s')."'>";
								if($custId)
									echo "<input type='hidden' name='hidCreatedId' value='".$createdId."'>";
								else 
									echo "<input type='hidden' name='hidCreatedId' value=''>";
							?>
						</td>
					</tr>
				</table>
			</form>
			<div>
				<?php	
					require('/include/footer.php'); 
					require("include/database_close.php");
				?>
			</div>
		</div>
	</body>
</html>
