<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_schedule.php'.'</td><td>'.$error.' near line 25.</td></tr>>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidEquipmentId = $_POST['hidEquipmentId'];
		$hidScheduleId = $_POST['hidScheduleId'];
		$hidCal = $_POST['hidCal'];
		$hidVal = $_POST['hidVal'];
		$sltSchedType = clean($_POST['sltSchedType']);
		$txtDate = clean($_POST['txtDate']);
		$status = 0;

########## Input Validations
		if ( !$sltSchedType ){
			$errmsg_arr[] = '*Schedule type is missing.';
			$errflag = true;
		}
		$valDate = validateDate($txtDate, 'Y-m-d');
		if ( $valDate != 1 ){
			$errmsg_arr[] = '* Invalid date.';
			$errflag = true;
		}

		if($hidScheduleId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidScheduleId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# SESSION, keeping last input value
		$_SESSION['sltSchedType'] = $sltSchedType;
		$_SESSION['txtDate'] = $txtDate;

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:new_schedule.php?id=$hidEquipmentId&v=$hidVal&c=$hidCal");
			exit();
		}
########## Committing to Database		
		
		$qry = mysqli_prepare( $db, "CALL sp_Equipment_Schedule_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ? )" );
		mysqli_stmt_bind_param( $qry, "iississii", $hidScheduleId, $hidEquipmentId, $sltSchedType, $txtDate, $status
												 , $createdAt, $updatedAt, $createdId, $updatedId );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_schedule.php'.'</td><td>'.$processError.' near line 82.</td></tr>>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidScheduleId)
				$_SESSION['SUCCESS']  = 'Successfully updated schedule.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added schedule.';
			header("location:equipment.php?page=".$_SESSION['page']."&name_text=".$_SESSION['name_text']."&code_text=".$_SESSION['code_text']."&type_text=".$_SESSION['type_text']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>