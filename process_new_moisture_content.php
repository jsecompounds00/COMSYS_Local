<?php
########## -- Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## -- Array to store validation errors
	$errmsg_arr = array();
 
########## -- Validation error flag
	$errflag = false;
 
########## -- Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}
########## -- Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_moisture_content.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## -- Sanitize the POST values
		$hidMCId = $_POST['hidMCId'];
		$txtDate = $_POST['txtDate'];
		$sltMIRNo = $_POST['sltMIRNo'];
		$sltRMItem = $_POST['sltRMItem'];
		$txtRMIRNo = $_POST['txtRMIRNo'];
		$txtPRMNo = $_POST['txtPRMNo'];
		$txtRemarks = $_POST['txtRemarks'];

########## -- Input Validations
		$valDate = validateDate($txtDate, 'Y-m-d');
		if ( $valDate != 1 ){
			$errmsg_arr[] = '* Invalid MIRS Date.';
			$errflag = true;
		}
		if ( !$sltMIRNo ){
			$errmsg_arr[] = '* Invalid MIRS number.';
			$errflag = true;
		}
		if ( !$sltRMItem ){
			$errmsg_arr[] = '* Invalid Raw Material.';
			$errflag = true;
		}
		if (empty($txtRMIRNo)){
			$errmsg_arr[] = '* Invalid RMIR number.';
			$errflag = true;
		}
		if (empty($txtPRMNo)){
			$errmsg_arr[] = '* Invalid PRM number.';
			$errflag = true;
		}

		if($hidMCId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidMCId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];
	############# Validation on MC Items
		
		foreach ($_POST['hidMCItemId'] as $hidMCItemId) {
			$hidMCItemId = array();
			$hidMCItemId = $_POST['hidMCItemId'];
		}
		foreach ($_POST['txtLotNumber'] as $txtLotNumber) {
			$txtLotNumber = array();
			$txtLotNumber = $_POST['txtLotNumber'];
			// echo $txtLotNumber;
		}
		foreach ($_POST['txtPercentMC'] as $txtPercentMC) {
			$txtPercentMC = array();
			$txtPercentMC = $_POST['txtPercentMC'];
		}
		foreach ($_POST['txtBulkDensity'] as $txtBulkDensity) {
			$txtBulkDensity = array();
			$txtBulkDensity = $_POST['txtBulkDensity'];
		}

		$i=0;

		do{
			$txtLotNumber[$i].'<br>';
			if ( empty($txtLotNumber[$i]) ){
				if ($i == 0){
					$errmsg_arr[] = '* Lot number is missing. (line '.($i+1).')';
					$errflag = true;
				}
				else{
					if ( !empty($txtPercentMC[$i]) || !empty($txtBulkDensity[$i]) ){
						$errmsg_arr[] = '* %MC is missing. (line '.($i+1).')';
						$errflag = true;
					}
				}
			}else{
				if ( empty($txtLotNumber[$i-1]) && $i != 0 ){
					$errmsg_arr[] = '* Line skipping is not allowed';
					$errflag = true;
				}else{
					if ( $txtPercentMC[$i] != '' && !is_numeric( $txtPercentMC[$i] ) ){
						$errmsg_arr[] = '* Invalid %MC. (line '.($i+1).')';
						$errflag = true;
					}
					if ( $txtBulkDensity[$i] != '' && !is_numeric( $txtBulkDensity[$i] ) ){
						$errmsg_arr[] = '* Invalid Bulk Density. (line '.($i+1).')';
						$errflag = true;
					}
				}
			}
			$i++;
		}while ($i<10);
########## -- Input Validation

		// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		
		// $txtRemarks = str_replace('\\', '', $txtRemarks);

########## -- SESSION, keeping last input value
		// $_SESSION["counter"] = 10;

		$_SESSION['SESS_MC_Date'] = $txtDate;
		$_SESSION['SESS_MC_MIRNo'] = $sltMIRNo;
		$_SESSION['SESS_MC_RMItem'] = $sltRMItem;
		$_SESSION['SESS_MC_RMIRNo'] = $txtRMIRNo;
		$_SESSION['SESS_MC_PRMNo'] = $txtPRMNo;
		$_SESSION['SESS_MC_Remarks'] = $txtRemarks;

		for ( $i = 0; $i < 10; $i++ ) {
			$_SESSION['SESS_MC_LotNumber'][$i] = $txtLotNumber[$i];
			$_SESSION['SESS_MC_PercentMC'][$i] = $txtPercentMC[$i];
			$_SESSION['SESS_MC_BulkDensity'][$i] = $txtBulkDensity[$i];
		}

########## -- If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_moisture_content.php?id=$hidMCId");
			exit();
		}
// echo $txtDate;
	$qry = mysqli_prepare($db, "CALL sp_Moisture_Content_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'ississsssii', $hidMCId, $txtDate, $sltMIRNo, $sltRMItem, $txtRMIRNo, $txtPRMNo
											  , $txtRemarks, $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_moisture_content.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidMCId ) {
				$MCTransId = $hidMCId;
			}else{
				$MCTransId = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['txtLotNumber'] as $key => $itemValue)
			{	
				if (!$hidMCId) {
					$hidMCItemId = 0;
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Moisture_Content_Item_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iisddssii', $hidMCItemId[$key], $MCTransId, $txtLotNumber[$key]
																, $txtPercentMC[$key], $txtBulkDensity[$key], $createdAt
																, $updatedAt, $createdId, $updatedId);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_moisture_content.php'.'</td><td>'.$processError1.' near line 237.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidMCId )
							$_SESSION['SUCCESS']  = 'Moisture Content successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Moisture Content successfully added.';
						header("location: moisture_content.php?page=".$_SESSION['page']."&name_text=".$_SESSION['name_text']."&code_text=".$_SESSION['code_text']."&type_text=".$_SESSION['type_text']);
					}				
				}
			}
		}
	}	
############# Committing in Database
		require("include/database_close.php");
	}
?>
	