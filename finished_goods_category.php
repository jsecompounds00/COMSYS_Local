<html>
	<head>
		<title>FG Category - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);

		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/init_unset_values/fg_parameters_unset_value.php");

			// if( $_SESSION['fg_category'] == false) 
			// {
			// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }

			$_SESSION['search'] = $_GET['search'];
			$_SESSION['qsone'] = $_GET['qsone'];
			$_SESSION['page'] = $_GET['page'];
		?>

		<div class="wrapper">
			
			<span> <h3> Finished Goods Category </h3> </span>

			<div class="search_box">
			 	<form method="get" action="finished_goods_category.php">
			 	 	<input type="hidden" name="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]); ?>">
			 		<input type="hidden" name="page" value="<?php echo $page; ?>">
			 		<table class="search_tables_form">
			 			<tr>
			 				<td> Category </td>
			 				<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
			 				<td> <input type='submit' value='Search'> </td>
			 			</tr>
			 		</table>
			 	</form>
			 </div>

			 <?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>finished_goods_category.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryR = mysqli_prepare($db, "CALL sp_FG_Parameters_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryR, 's', $search);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR); //return results of query

					$total_results = mysqli_num_rows($resultR); //return number of rows of result
					
					$db->next_result();
					$resultR->close();

					$targetpage = "finished_goods_category.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_FG_Parameters_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>finished_goods_category.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class='home_pages'>
							<tr>
								<td colspan='3'> <?php echo $pagination;?> </td>
							</tr>
							<tr>
							    <th>Category</th>
							    <th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)) {

									$FGid = $row['id'];
									$FGCategory = $row['Category'];

							?>

									<tr>
										<td> <?php echo $FGCategory; ?> </td>
										<td>
											<input type='button' name='btnEdit' value='Standard Parameter' onclick="location.href='new_fg_standard_parameter.php?category=<?php echo $FGCategory; ?>'"> 
										</td>
									</tr>
							<?php
								}
							?>
							<tr>
								<td colspan='3'> <?php echo $pagination;?> </td>
							</tr>
						</table>	
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>