<?php
############# Start session
	session_start();
	ob_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		// $hidTestingID = clean($_POST['hidTestingID']);
		$hidTrialID = clean($_POST['hidTrialID']);

		$hidDynamicMilling = clean($_POST['hidDynamicMilling']);
		$hidStrandInspection = clean($_POST['hidStrandInspection']);
		$hidPelletInspection = clean($_POST['hidPelletInspection']);
		$hidImpactTest = clean($_POST['hidImpactTest']);
		$hidStaticHeating = clean($_POST['hidStaticHeating']);
		$hidColorChangeTest = clean($_POST['hidColorChangeTest']);
		$hidWaterImmersion = clean($_POST['hidWaterImmersion']);
		$hidColdInspection = clean($_POST['hidColdInspection']);
		$hidOvenAging = clean($_POST['hidOvenAging']);
		$hidOilAging = clean($_POST['hidOilAging']);

		$hidFGSoft = clean($_POST['hidFGSoft']);

	############# Complete Testing
		$hidCompleteTestingID = clean($_POST['hidCompleteTestingID']);
		$txtSpecificGravity = clean($_POST['txtSpecificGravity']);
		$txtHardnessA = clean($_POST['txtHardnessA']);
		$txtVolumeResistivity = clean($_POST['txtVolumeResistivity']);
		$txtHardnessD = clean($_POST['txtHardnessD']);
		$txtUnagedTS = clean($_POST['txtUnagedTS']);
		$txtOvenTS = clean($_POST['txtOvenTS']);
		$txtOilTS = clean($_POST['txtOilTS']);
		$txtUnagedE = clean($_POST['txtUnagedE']);
		$txtOvenE = clean($_POST['txtOvenE']);
		$txtOilE = clean($_POST['txtOilE']);

		$txtSpecificGravity = str_replace('\\', '', $txtSpecificGravity);
		$txtHardnessA = str_replace('\\', '', $txtHardnessA);
		$txtVolumeResistivity = str_replace('\\', '', $txtVolumeResistivity);
		$txtHardnessD = str_replace('\\', '', $txtHardnessD);
		$txtUnagedTS = str_replace('\\', '', $txtUnagedTS);
		$txtOvenTS = str_replace('\\', '', $txtOvenTS);
		$txtOilTS = str_replace('\\', '', $txtOilTS);
		$txtUnagedE = str_replace('\\', '', $txtUnagedE);
		$txtOvenE = str_replace('\\', '', $txtOvenE);
		$txtOilE = str_replace('\\', '', $txtOilE);

		if($hidCompleteTestingID == 0){
			$CTcreatedAt = date('Y-m-d H:i:s');
			$CTcreatedId = $_SESSION['SESS_USER_ID'];
			$CTupdatedAt = NULL;
			$CTupdatedId = NULL;
		}
		else{
			$CTcreatedAt = $_POST['hidCTCreatedAt'];
			$CTcreatedId = $_POST['hidCTCreatedId'];
			$CTupdatedAt = date('Y-m-d H:i:s');
			$CTupdatedId = $_SESSION['SESS_USER_ID'];
		}

	############# Dynamic Milling
		$hidDynamicMillingID = clean($_POST['hidDynamicMillingID']);
		$txtSampling = clean($_POST['txtSampling']);
		$radDMColor = clean($_POST['radDMColor']);
		$radDMHeat = clean($_POST['radDMHeat']);
		$radDMProcess = clean($_POST['radDMProcess']);

		$txtSampling = str_replace('\\', '', $txtSampling);

		if($hidDynamicMillingID == 0){
			$DMcreatedAt = date('Y-m-d H:i:s');
			$DMcreatedId = $_SESSION['SESS_USER_ID'];
			$DMupdatedAt = NULL;
			$DMupdatedId = NULL;
		}
		else{
			$DMcreatedAt = $_POST['hidDMCreatedAt'];
			$DMcreatedId = $_POST['hidDMCreatedId'];
			$DMupdatedAt = date('Y-m-d H:i:s');
			$DMupdatedId = $_SESSION['SESS_USER_ID'];
		}

	############# Strands / Sheets Inspection
		$hidStrandInspectionID = clean($_POST['hidStrandInspectionID']);
		$sltFisheyeEvaluation = clean($_POST['sltFisheyeEvaluation']);
		$sltFisheyeClass = clean($_POST['sltFisheyeClass']);
		$sltOverallRemarks = clean($_POST['sltOverallRemarks']);
		$sltPinholeEvaluation = clean($_POST['sltPinholeEvaluation']);
		// $chkColorConform = clean($_POST['chkColorConform']);
		// $chkPorous = clean($_POST['chkPorous']);
		// $chkFCPresent = clean($_POST['chkFCPresent']);

		if($hidStrandInspectionID == 0){
			$SIcreatedAt = date('Y-m-d H:i:s');
			$SIcreatedId = $_SESSION['SESS_USER_ID'];
			$SIupdatedAt = NULL;
			$SIupdatedId = NULL;
		}
		else{
			$SIcreatedAt = $_POST['hidSICreatedAt'];
			$SIcreatedId = $_POST['hidSICreatedId'];
			$SIupdatedAt = date('Y-m-d H:i:s');
			$SIupdatedId = $_SESSION['SESS_USER_ID'];
		}

	############# Pellet Inspection
		$hidPelletInspectionID = clean($_POST['hidPelletInspectionID']);
		// $chkClarityColorConform = clean($_POST['chkClarityColorConform']);

		if($hidPelletInspectionID == 0){
			$PIcreatedAt = date('Y-m-d H:i:s');
			$PIcreatedId = $_SESSION['SESS_USER_ID'];
			$PIupdatedAt = NULL;
			$PIupdatedId = NULL;
		}
		else{
			$PIcreatedAt = $_POST['hidPICreatedAt'];
			$PIcreatedId = $_POST['hidPICreatedId'];
			$PIupdatedAt = date('Y-m-d H:i:s');
			$PIupdatedId = $_SESSION['SESS_USER_ID'];
		}

	############# Impact Test / Strength
		$hidImpactTestID = clean($_POST['hidImpactTestID']);
		$txtDropWeight = clean($_POST['txtDropWeight']);
		$txtDropHeight = clean($_POST['txtDropHeight']);
		$txtSamplesNo = clean($_POST['txtSamplesNo']);
		$txtPassedNo = clean($_POST['txtPassedNo']);

		$txtDropWeight = str_replace('\\', '', $txtDropWeight);
		$txtDropHeight = str_replace('\\', '', $txtDropHeight);
		$txtSamplesNo = str_replace('\\', '', $txtSamplesNo);
		$txtPassedNo = str_replace('\\', '', $txtPassedNo);

		if($hidImpactTestID == 0){
			$ITcreatedAt = date('Y-m-d H:i:s');
			$ITcreatedId = $_SESSION['SESS_USER_ID'];
			$ITupdatedAt = NULL;
			$ITupdatedId = NULL;
		}
		else{
			$ITcreatedAt = $_POST['hidITCreatedAt'];
			$ITcreatedId = $_POST['hidITCreatedId'];
			$ITupdatedAt = date('Y-m-d H:i:s');
			$ITupdatedId = $_SESSION['SESS_USER_ID'];
		}

	############# Static Heating
		$hidStaticHeatingID = clean($_POST['hidStaticHeatingID']);
		$radSHColor = clean($_POST['radSHColor']);
		$radSHHeat = clean($_POST['radSHHeat']);

		if($hidStaticHeatingID == 0){
			$SHcreatedAt = date('Y-m-d H:i:s');
			$SHcreatedId = $_SESSION['SESS_USER_ID'];
			$SHupdatedAt = NULL;
			$SHupdatedId = NULL;
		}
		else{
			$SHcreatedAt = $_POST['hidSHCreatedAt'];
			$SHcreatedId = $_POST['hidSHCreatedId'];
			$SHupdatedAt = date('Y-m-d H:i:s');
			$SHupdatedId = $_SESSION['SESS_USER_ID'];
		}

	############# Color Change Test
		$hidColorChangeID = clean($_POST['hidColorChangeID']);
		$radCCTColor = clean($_POST['radCCTColor']);
		$radCCTHeat = clean($_POST['radCCTHeat']);

		if($hidColorChangeID == 0){
			$CCTcreatedAt = date('Y-m-d H:i:s');
			$CCTcreatedId = $_SESSION['SESS_USER_ID'];
			$CCTupdatedAt = NULL;
			$CCTupdatedId = NULL;
		}
		else{
			$CCTcreatedAt = $_POST['hidCCTCreatedAt'];
			$CCTcreatedId = $_POST['hidCCTCreatedId'];
			$CCTupdatedAt = date('Y-m-d H:i:s');
			$CCTupdatedId = $_SESSION['SESS_USER_ID'];
		}

	############# Water Immersion
		$hidWaterImmersionID = clean($_POST['hidWaterImmersionID']);
		$txtImmersionStartDate = clean($_POST['txtImmersionStartDate']);
		$txtImmersionEndDate = clean($_POST['txtImmersionEndDate']);
		$txtDay0 = clean($_POST['txtDay0']);
		$txtDay1 = clean($_POST['txtDay1']);
		$txtDay2 = clean($_POST['txtDay2']);
		$txtDay3 = clean($_POST['txtDay3']);
		$txtDay4 = clean($_POST['txtDay4']);
		$txtDay5 = clean($_POST['txtDay5']);
		$txtDay6 = clean($_POST['txtDay6']);
		$txtDay7 = clean($_POST['txtDay7']);
		$txtDay8 = clean($_POST['txtDay8']);
		$txtDay9 = clean($_POST['txtDay9']);
		$txtDay10 = clean($_POST['txtDay10']);
		$txtDay11 = clean($_POST['txtDay11']);
		$txtDay12 = clean($_POST['txtDay12']);
		$txtDay13 = clean($_POST['txtDay13']);
		$txtDay14 = clean($_POST['txtDay14']);

		$txtImmersionStartDate = str_replace('\\', '', $txtImmersionStartDate);
		$txtImmersionEndDate = str_replace('\\', '', $txtImmersionEndDate);
		$txtDay0 = str_replace('\\', '', $txtDay0);
		$txtDay1 = str_replace('\\', '', $txtDay1);
		$txtDay2 = str_replace('\\', '', $txtDay2);
		$txtDay3 = str_replace('\\', '', $txtDay3);
		$txtDay4 = str_replace('\\', '', $txtDay4);
		$txtDay5 = str_replace('\\', '', $txtDay5);
		$txtDay6 = str_replace('\\', '', $txtDay6);
		$txtDay7 = str_replace('\\', '', $txtDay7);
		$txtDay8 = str_replace('\\', '', $txtDay8);
		$txtDay9 = str_replace('\\', '', $txtDay9);
		$txtDay10 = str_replace('\\', '', $txtDay10);
		$txtDay11 = str_replace('\\', '', $txtDay11);
		$txtDay12 = str_replace('\\', '', $txtDay12);
		$txtDay13 = str_replace('\\', '', $txtDay13);
		$txtDay14 = str_replace('\\', '', $txtDay14);

		if($hidWaterImmersionID == 0){
			$WIcreatedAt = date('Y-m-d H:i:s');
			$WIcreatedId = $_SESSION['SESS_USER_ID'];
			$WIupdatedAt = NULL;
			$WIupdatedId = NULL;
		}
		else{
			$WIcreatedAt = $_POST['hidWICreatedAt'];
			$WIcreatedId = $_POST['hidWICreatedId'];
			$WIupdatedAt = date('Y-m-d H:i:s');
			$WIupdatedId = $_SESSION['SESS_USER_ID'];
		}

	############# COLD BEND TEST
		$hidColdInspectionID = clean($_POST['hidColdInspectionID']);
		$txtCIResult1 = clean($_POST['txtCIResult1']);
		$txtCIResult2 = clean($_POST['txtCIResult2']);
		$txtCIResult3 = clean($_POST['txtCIResult3']);
		$txtCICracked1 = clean($_POST['txtCICracked1']);
		$txtCICracked2 = clean($_POST['txtCICracked2']);
		$txtCICracked3 = clean($_POST['txtCICracked3']);

		if($hidColdInspectionID == 0){
			$CIcreatedAt = date('Y-m-d H:i:s');
			$CIcreatedId = $_SESSION['SESS_USER_ID'];
			$CIupdatedAt = NULL;
			$CIupdatedId = NULL;
		}
		else{
			$CIcreatedAt = $_POST['hidCICreatedAt'];
			$CIcreatedId = $_POST['hidCICreatedId'];
			$CIupdatedAt = date('Y-m-d H:i:s');
			$CIupdatedId = $_SESSION['SESS_USER_ID'];
		}


############# Input Validation

	############# Complete Testing
		if ( !is_numeric( $txtSpecificGravity ) ){
			$errmsg_arr[] = '* Invalid value for specific gravity.';
			$errflag = true;
		}
		if ( $hidFGSoft == 1 ){        
			if ( !is_numeric( $txtHardnessA ) ){
				$errmsg_arr[] = '* Invalid value for hardness.';
				$errflag = true;
			}
			if ( empty( $txtVolumeResistivity ) ){
				$errmsg_arr[] = '* Invalid value for volume resistivity.';
				$errflag = true;
			}
			if ( !is_numeric( $txtUnagedTS ) ){
				$errmsg_arr[] = '* Invalid value for tensile strength (unaged).';
				$errflag = true;
			}
			if ( !is_numeric( $txtUnagedE ) ){
				$errmsg_arr[] = '* Invalid value for elongation (unaged).';
				$errflag = true;
			}
			if ( $hidOvenAging ){
				if ( !is_numeric( $txtOvenTS ) ){
					$errmsg_arr[] = '* Invalid value for tensile strength (oven aged).';
					$errflag = true;
				}
				if ( !is_numeric( $txtOvenE ) ){
					$errmsg_arr[] = '* Invalid value for elongation (oven aged).';
					$errflag = true;
				}
			}else{
				$txtOvenTS = NULL;
				$txtOvenE = NULL;
			}
			if ( $hidOilAging == 1 ){
				if ( !is_numeric( $txtOilTS ) ){
					$errmsg_arr[] = '* Invalid value for tensile strength (oil aged).';
					$errflag = true;
				}
				if ( !is_numeric( $txtOilE ) ){
					$errmsg_arr[] = '* Invalid value for elongation (oil aged).';
					$errflag = true;
				}
			}else{
				$txtOilTS = NULL;
				$txtOilE = NULL;
			}

			$txtHardnessD = NULL;
		}else{
			if ( !is_numeric( $txtHardnessD ) ){
				$errmsg_arr[] = '* Invalid value for hardness.';
				$errflag = true;

				$txtHardnessA = NULL;
				$txtVolumeResistivity = '';
				$txtUnagedTS = NULL;
				$txtUnagedE = NULL;
				$txtOvenTS = NULL;
				$txtOvenE = NULL;
				$txtOilTS = NULL;
				$txtOilE = NULL;
			}

		}

		$_SESSION['BT_txtSpecificGravity'] = $txtSpecificGravity;
		$_SESSION['BT_txtHardnessA'] = $txtHardnessA;
		$_SESSION['BT_txtVolumeResistivity'] = $txtVolumeResistivity;
		$_SESSION['BT_txtHardnessD'] = $txtHardnessD;
		$_SESSION['BT_txtUnagedTS'] = $txtUnagedTS;
		$_SESSION['BT_txtOvenTS'] = $txtOvenTS;
		$_SESSION['BT_txtOilTS'] = $txtOilTS;
		$_SESSION['BT_txtUnagedE'] = $txtUnagedE;
		$_SESSION['BT_txtOvenE'] = $txtOvenE;
		$_SESSION['BT_txtOilE'] = $txtOilE;

	############# Dynamic Milling
		if ( $hidDynamicMilling == 1 ){
			if ( $txtSampling == "" ){
				$errmsg_arr[] = "* Sampling can't be blank.";
				$errflag = true;
			}
		}

		$_SESSION['BT_txtSampling'] = $txtSampling;
		$_SESSION['BT_radDMColor'] = $radDMColor;
		$_SESSION['BT_radDMHeat'] = $radDMHeat;
		$_SESSION['BT_radDMProcess'] = $radDMProcess;

	############# Strands / Sheets Inspection
		if ( $hidStrandInspection == 1 ){
			if ( isset( $_POST['chkColorConform'] ) ){
				$chkColorConform = 1;
			}else{
				$chkColorConform = 0;
			}
			if ( isset( $_POST['chkPorous'] ) ){
				$chkPorous = 1;
			}else{
				$chkPorous = 0;
			}
			if ( isset( $_POST['chkFCPresent'] ) ){
				$chkFCPresent = 1;
			}else{
				$chkFCPresent = 0;
			}
		}

		$_SESSION['BT_sltFisheyeEvaluation'] = $sltFisheyeEvaluation;
		$_SESSION['BT_sltFisheyeClass'] = $sltFisheyeClass;
		$_SESSION['BT_sltOverallRemarks'] = $sltOverallRemarks;
		$_SESSION['BT_sltPinholeEvaluation'] = $sltPinholeEvaluation;
		$_SESSION['BT_chkColorConform'] = $chkColorConform;
		$_SESSION['BT_chkPorous'] = $chkPorous;
		$_SESSION['BT_chkFCPresent'] = $chkFCPresent;

	############# Pellet Inspection
		if ( $hidPelletInspection == 1 ){
			if ( isset( $_POST['chkClarityColorConform'] ) ){
				$chkClarityColorConform = 1;
			}else{
				$chkClarityColorConform = 0;
			}
		}

		$_SESSION['BT_chkClarityColorConform'] = $chkClarityColorConform;

	############# Impact Strength / Test
		if ( $hidImpactTest == 1 ){
			if ( empty( $txtDropWeight ) ){
				$errmsg_arr[] = '* Weight of drop can\'t be blank.';
				$errflag = true;
			}
			if ( empty( $txtDropHeight ) ){
				$errmsg_arr[] = '* Height of drop can\'t be blank.';
				$errflag = true;
			}
			if ( empty( $txtSamplesNo ) ){
				$errmsg_arr[] = '* No. of samples can\'t be blank.';
				$errflag = true;
			}
			if ( empty( $txtPassedNo ) ){
				$errmsg_arr[] = '* No. of passed can\'t be blank.';
				$errflag = true;
			}
		}

		$_SESSION['BT_txtDropWeight'] = $txtDropWeight;
		$_SESSION['BT_txtDropHeight'] = $txtDropHeight;
		$_SESSION['BT_txtSamplesNo'] = $txtSamplesNo;
		$_SESSION['BT_txtPassedNo'] = $txtPassedNo;

	############# Static Heating
		$_SESSION['BT_radSHColor'] = $radSHColor;
		$_SESSION['BT_radSHHeat'] = $radSHHeat;

	############# Color Change Test
		$_SESSION['BT_radCCTColor'] = $radCCTColor;
		$_SESSION['BT_radCCTHeat'] = $radCCTHeat;

	############# Water Immersion
		if ( $hidWaterImmersion == 1 ){
			$valImmersionStartDate = validateDate($txtImmersionStartDate, 'Y-m-d');
			if ( $valImmersionStartDate != 1 ){
				$errmsg_arr[] = '* Invalid start date of water immersion.';
				$errflag = true;
			}

			$valImmersionEndDate = validateDate($txtImmersionEndDate, 'Y-m-d');
			if ( $valImmersionEndDate != 1 ){
				$errmsg_arr[] = '* Invalid end date of water immersion.';
				$errflag = true;
			}

			if ( empty($txtDay0) && empty($txtDay1) && empty($txtDay2) && empty($txtDay3) && empty($txtDay4) 
				&& empty($txtDay5) && empty($txtDay6) && empty($txtDay7) && empty($txtDay8) && empty($txtDay9)
				&& empty($txtDay10) && empty($txtDay11) && empty($txtDay12) && empty($txtDay13) && empty($txtDay14))
			{
				$errmsg_arr[] = '* Water immersion can\'t be blank.';
				$errflag = true;
			}
		}
		
		$_SESSION['BT_txtImmersionStartDate'] = $txtImmersionStartDate;
		$_SESSION['BT_txtImmersionEndDate'] = $txtImmersionEndDate;
		$_SESSION['BT_txtDay0'] = $txtDay0;
		$_SESSION['BT_txtDay1'] = $txtDay1;
		$_SESSION['BT_txtDay2'] = $txtDay2;
		$_SESSION['BT_txtDay3'] = $txtDay3;
		$_SESSION['BT_txtDay4'] = $txtDay4;
		$_SESSION['BT_txtDay5'] = $txtDay5;
		$_SESSION['BT_txtDay6'] = $txtDay6;
		$_SESSION['BT_txtDay7'] = $txtDay7;
		$_SESSION['BT_txtDay8'] = $txtDay8;
		$_SESSION['BT_txtDay9'] = $txtDay9;
		$_SESSION['BT_txtDay10'] = $txtDay10;
		$_SESSION['BT_txtDay11'] = $txtDay11;
		$_SESSION['BT_txtDay12'] = $txtDay12;
		$_SESSION['BT_txtDay13'] = $txtDay13;
		$_SESSION['BT_txtDay14'] = $txtDay14;

	############# COLD BEND TEST
		if ( $txtCIResult1 == 'F' ){
			if ( $txtCICracked1 == '' ){
				$errmsg_arr[] = "* Condition for specimen 1 can't be blank.";
				$errflag = true;
			}
		}else{
			$txtCICracked1 = NULL;
		}
		if ( $txtCIResult2 == 'F' ){
			if ( $txtCICracked2 == '' ){
				$errmsg_arr[] = "* Condition for specimen 2 can't be blank.";
				$errflag = true;
			}
		}else{
			$txtCICracked2 = NULL;
		}
		if ( $txtCIResult3 == 'F' ){
			if ( $txtCICracked3 == '' ){
				$errmsg_arr[] = "* Condition for specimen 3 can't be blank.";
				$errflag = true;
			}
		}else{
			$txtCICracked3 = NULL;
		}

		$_SESSION['BT_txtCIResult1'] = $txtCIResult1;
		$_SESSION['BT_txtCIResult2'] = $txtCIResult2;
		$_SESSION['BT_txtCIResult3'] = $txtCIResult3;
		$_SESSION['BT_txtCICracked1'] = $txtCICracked1;
		$_SESSION['BT_txtCICracked2'] = $txtCICracked2;
		$_SESSION['BT_txtCICracked3'] = $txtCICracked3;

############# Input Validation

	############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: batch_ticket_testing.php?tid=$hidTrialID");
			exit();
		}
############# Commiting to Database

	############# Complete Testing	
		$qryCT = mysqli_prepare($db, "CALL sp_BAT_Complete_Testing_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qryCT, 'iidddsddddddssii', $hidCompleteTestingID, $hidTrialID, $txtSpecificGravity, $txtHardnessA
												, $txtHardnessD, $txtVolumeResistivity, $txtUnagedTS, $txtUnagedE, $txtOvenTS
												, $txtOvenE, $txtOilTS, $txtOilE, $CTcreatedAt, $CTupdatedAt, $CTcreatedId, $CTupdatedId);
		$qryCT->execute();
		$resultCT = mysqli_stmt_get_result($qryCT); 
		$processErrorCT = mysqli_error($db);

		if(!empty($processErrorCT))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td>Complete Testing: '.$processErrorCT.' near line 116.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}

	############# Dynamic Milling
		if ( $hidDynamicMilling == 1 ){
			$qryDM = mysqli_prepare($db, "CALL sp_BAT_Dynamic_Milling_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qryDM, 'iisssssiii', $hidDynamicMillingID, $hidTrialID, $radDMColor, $radDMHeat
													, $radDMProcess, $DMcreatedAt, $DMupdatedAt, $DMcreatedId, $DMupdatedId, $txtSampling);
			$qryDM->execute();
			$resultDM = mysqli_stmt_get_result($qryDM); 
			$processErrorDM = mysqli_error($db);

			if(!empty($processErrorDM))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td> .php'.'</td><td>Dynamic Milling: '.$processErrorDM.' near line 116.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		}

	############# Strands / Sheets Inspection
		if ( $hidStrandInspection == 1 ){
			$qrySI = mysqli_prepare($db, "CALL sp_BAT_Strand_Inspection_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qrySI, 'iiiiiiiiissii', $hidStrandInspectionID, $hidTrialID, $sltFisheyeEvaluation, $sltFisheyeClass
													, $sltPinholeEvaluation, $chkColorConform, $chkPorous, $chkFCPresent, $sltOverallRemarks
													, $SIcreatedAt, $SIupdatedAt, $SIcreatedId, $SIupdatedId);
			$qrySI->execute();
			$resultSI = mysqli_stmt_get_result($qrySI); 
			$processErrorSI = mysqli_error($db);

			if(!empty($processErrorSI))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td> Strands Inspection: '.$processErrorSI.' near line 116.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		}

	############# Pellet Inspection
		if ( $hidPelletInspection == 1 ){
			$qryPI = mysqli_prepare($db, "CALL sp_BAT_Pellet_Inspection_CRU( ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qryPI, 'iiissii', $hidPelletInspectionID, $hidTrialID, $chkClarityColorConform
													, $PIcreatedAt, $PIupdatedAt, $PIcreatedId, $PIupdatedId);
			$qryPI->execute();
			$resultPI = mysqli_stmt_get_result($qryPI); 
			$processErrorPI = mysqli_error($db);

			if(!empty($processErrorPI))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td> Pellet Inspection: '.$processErrorPI.' near line 116.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		}

	############# Impact Test
		if ( $hidImpactTest == 1 ){
			$qryIT = mysqli_prepare($db, "CALL sp_BAT_Impact_Test_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qryIT, 'iiddssssii', $hidImpactTestID, $hidTrialID, $txtDropWeight
													, $txtDropHeight, $txtSamplesNo, $txtPassedNo
													, $ITcreatedAt, $ITupdatedAt, $ITcreatedId, $ITupdatedId);
			$qryIT->execute();
			$resultIT = mysqli_stmt_get_result($qryIT); 
			$processErrorIT = mysqli_error($db);

			if(!empty($processErrorIT))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td> Impact Test: '.$processErrorIT.' near line 116.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		}

	############# Static Heat
		if ( $hidStaticHeating == 1 ){
			$qrySH = mysqli_prepare($db, "CALL sp_BAT_Static_Heating_CRU( ?, ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qrySH, 'iissssii', $hidStaticHeatingID, $hidTrialID, $radSHColor, $radSHHeat
													, $SHcreatedAt, $SHupdatedAt, $SHcreatedId, $SHupdatedId);
			$qrySH->execute();
			$resultSH = mysqli_stmt_get_result($qrySH); 
			$processErrorSH = mysqli_error($db);

			if(!empty($processErrorSH))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td> Static Heating: '.$processErrorSH.' near line 116.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		}

	############# Color Change Test
		if ( $hidColorChangeTest == 1 ){
			$qryCCT = mysqli_prepare($db, "CALL sp_BAT_Color_Change_CRU( ?, ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qryCCT, 'iissssii', $hidColorChangeID, $hidTrialID, $radCCTColor, $radCCTHeat
													, $CCTcreatedAt, $CCTupdatedAt, $CCTcreatedId, $CCTupdatedId);
			$qryCCT->execute();
			$resultCCT = mysqli_stmt_get_result($qryCCT); 
			$processErrorCCT = mysqli_error($db);

			if(!empty($processErrorCCT))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td> Color Change Test: '.$processErrorCCT.' near line 116.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		}

	############# Water Immersion
		if ( $hidWaterImmersion == 1 ){
			$qryWI = mysqli_prepare($db, "CALL sp_BAT_Water_Immersion_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qryWI, 'iisssssssssssssssssssii', $hidWaterImmersionID, $hidTrialID, $txtImmersionStartDate, $txtImmersionEndDate
													, $txtDay0, $txtDay1, $txtDay2, $txtDay3, $txtDay4, $txtDay5, $txtDay6, $txtDay7
													, $txtDay8, $txtDay9, $txtDay10, $txtDay11, $txtDay12, $txtDay13, $txtDay14
													, $WIcreatedAt, $WIupdatedAt, $WIcreatedId, $WIupdatedId);
			$qryWI->execute();
			$resultWI = mysqli_stmt_get_result($qryWI); 
			$processErrorWI = mysqli_error($db);

			if(!empty($processErrorWI))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td> Water Immersion: '.$processErrorWI.' near line 116.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		}

	############# COLD BEND TEST
		if ( $hidColdInspection == 1 ){
			$qryCI = mysqli_prepare($db, "CALL sp_BAT_Cold_Inspection_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			mysqli_stmt_bind_param($qryCI, 'iisssdddssii', $hidColdInspectionID, $hidTrialID, $txtCIResult1, $txtCIResult2
														 , $txtCIResult3, $txtCICracked1, $txtCICracked2, $txtCICracked3
														 , $CIcreatedAt, $CIupdatedAt, $CIcreatedId, $CIupdatedId);
			$qryCI->execute();
			$resultCI = mysqli_stmt_get_result($qryCI); 
			$processErrorCI = mysqli_error($db);

			if(!empty($processErrorCI))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_batch_ticket_testing.php'.'</td><td> Water Immersion: '.$processErrorCI.' near line 116.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		}


		if ( empty($processErrorCT) && empty($processErrorDM) && empty($processErrorSI) 
		  && empty($processErrorPI) && empty($processErrorIT) && empty($processErrorSH) 
		  && empty($processErrorCCT) && empty($processErrorWI) && empty($processErrorCI) ){
			$_SESSION['SUCCESS']  = 'Successfully added batch ticket test evaluation results.';
			//echo $_SESSION['SUCCESS'];
			header("location:batch_ticket.php?page=1&search=&qsone=");	
		}
		require("include/database_close.php");
	}
?>