<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_finished_goods.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidFGID = $_POST['hidFGID'];
		// $hidMBTypeID = $_POST['hidMBTypeID'];
		$sltMasterBatch = $_POST['sltMasterBatch'];
		$txtMBDosage = $_POST['txtMBDosage'];

############# SESSION, keeping last input value
		$_SESSION['SESS_FGMB_MasterBatch'] = $sltMasterBatch;
		$_SESSION['SESS_FGMB_MBDosage'] = $txtMBDosage;

########## Input Validations
		if ( !$sltMasterBatch ){
			$errmsg_arr[] = '* Master batch is missing.';
			$errflag = true;
		}
		if ( !is_numeric($txtMBDosage) ){
			$errmsg_arr[] = '* Invalid dosage.';
			$errflag = true;
		}

		if($hidFGID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidFGID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_finished_goods.php?id=$hidFGID");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Finished_Goods_CRU(?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iidssii', $hidFGID, $sltMasterBatch, $txtMBDosage
										   , $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_finished_goods.php'.'</td><td>'.$processError.' near line 77.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		if($hidFGID)
			$_SESSION['SUCCESS']  = 'Successfully updated finished goods.';
		else
			$_SESSION['SUCCESS']  = 'Successfully added new finished goods.';
		//echo $_SESSION['SUCCESS'];
		header("location: finished_goods.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
	}	
		unset($_SESSION['page']);
		unset($_SESSION['search']);
		unset($_SESSION['qsone']);
############# Committing in Database

		require("include/database_close.php");

	}
?>