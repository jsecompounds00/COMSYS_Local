
<html>
	<head>
		<script src="js/datetimepicker_css.js"></script> 
		<script src="js/jscript.js"></script>  
		<?php
			require("/include/database_connect.php");

			if ( !empty($errno) ){
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php'.'</td><td>'.$error.' near line 14.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				$preId = $_GET['id'];

				if ( $preId )
				{
					$qryPH = mysqli_prepare($db, "CALL sp_PRE_Query( ? )");
					mysqli_stmt_bind_param($qryPH, 'i', $preId);
					$qryPH->execute();
					$resultPH = mysqli_stmt_get_result($qryPH); //return results of query
					$processError = mysqli_error($db);

					if ( !empty($processError) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php'.'</td><td>'.$processError.' near line 34.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						$supplies = array();
						$quantity = array();
						$uom = array();
						$comments = array();
						$itemNo = array();
						$preTransId = array();
						$item_canceled = array();
						$item_type = array();
						$with_po = array();
						$pcf = array();
						while($row = mysqli_fetch_assoc($resultPH)){
							$pre_number = $row['pre_number'];
							$pre_date = $row['pre_date'];
							$date_needed = $row['date_needed'];
							$date_received = $row['date_received'];
							$approved_date = $row['approved_date'];
							$division = $row['division'];
							$division_other = $row['division_other'];
							$dept_id = $row['department_id'];
							$requisitioner_id = $row['requisitioner_id'];
							$purpose_st = $row['purpose_st'];
							$purpose_de = $row['purpose_de'];
							$purpose_capex = $row['purpose_capex'];
							$capex_number = $row['capex_number'];
							$capex_name= $row['capex_name'];
							$purpose_other = $row['purpose_other'];
							$purpose_other_comments = $row['purpose_other_comments'];
							$remarks = $row['remarks'];
							$remarks_array = explode('<br>', $remarks);
							$tag_as_cancel = $row['canceled'];
							$cancel_reason = $row['cancel_reason'];
							$supply_subtype = $row['supply_subtype_id'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];

							$supplies[] = $row['supply_id'];						
						
							$quantity[] = $row['quantity'];
						
							$uom[] = $row['uom_id'];
						
							$comments[] = $row['comments'];
						
							$itemNo[]= $row['item_number'];

							$preTransId[] = $row['pre_trans_id'];

							$item_canceled[] = $row['item_canceled'];

							$item_type[] = $row['item_type'];

							$with_po[] = $row['with_po'];

							$pcf[] = $row['pcf'];
						}
					}

					$db->next_result();
					$resultPH->close();

					############ .............
					$qryPI = "SELECT id from comsys.pre where canceled=0";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php'.'</td><td>'.$processErrorPI.' near line 89.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($preId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php</td><td>The user tries to edit a non-existing or canceled pre_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>PRE - Edit</title>";

				}
				else
				{

					echo "<title>PRE - Add</title>";

				}
				
			}
		?>
	</head>
	<body onload="<?php echo ( $preId ? "showUser($dept_id, $requisitioner_id)" : "showPREItem()" );?>">

		<form method='post' action='process_new_pre.php'>

			<?php      
				require("/include/header.php");
				require("/include/init_value.php");

				if ( $preId ){
					if( $_SESSION['edit_pre'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['add_pre'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>  

			<div class="wrapper">

				<span> <h3> <?php echo ( $preId ? "Edit PRE No. ".$pre_number : "New P.R.E." );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form collapsed">
					<colgroup>
						<col width="180px"></col><col width="350px"></col><col width="180px"></col><col width="50px"></col><col width="320px"></col>
					</colgroup>

					<tr>
		    			<td>PRE No.:</td>

		    			<td>
		    				<input type='<?php echo ( $preId ? "hidden" : "text" ); ?>' name='txtPRENo' id='txtPRENo' 
		    					value='<?php echo ( $preId ? $pre_number : $PRENo ); ?>' 
		    					<?php echo ( $preId ? "" : "onkeyup='checkDuplicatePRE()'" ); ?>>
		    				<b> <?php echo ( $preId ? $pre_number : "" ); ?> </b>
		    			</td>

		    			<td>PRE Date:</td>

		    			<td colspan="2">  
		    				<input type='text' name='txtPREDate' id='txtPREDate' value='<?php echo ( $preId ? $pre_date : $PREDate ); ?>'>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtPREDate')" style="cursor:pointer" name="picker" />
		    			</td>
		    		</tr>

		    		<tr>
		    			<td></td>
		    			<td id='tdPRENo' colspan='5'></td>
		    		</tr>

		    		<tr>
		    			<td>Date Received:</td> 

						<td> 
							<input type='text' name='txtDateRec' id='txtDateRec' value='<?php echo ( $preId ? $date_received : $DateRec );?>'>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDateRec')" style="cursor:pointer" name="picker" />             
						</td>  

						<td>Date Needed:</td>

						<td colspan="2">
							<input type='text' name='txtDateNeed' id='txtDateNeed' value='<?php echo ( $preId ? $date_needed : $DateNeed );?>'>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDateNeed')" style="cursor:pointer" name="picker" />             
						</td>  
		    		</tr>

		    		<tr class="spacing" valign="top">
		    			<td>Approved Date:</td>

		    			<td>
		    				<input type='text' name='txtDateApproved' id='txtDateApproved' value='<?php echo ( $preId ? $approved_date : $DateApproved );?>'>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDateApproved')" style="cursor:pointer" name="picker" />
						</td>
		    		</tr>

		    		<tr> 
						<td valign='top' class="border_top border_left border_bottom" rowspan="5"> Division: </td>

						<td valign='top' class="border_top">
							<input type='radio' name='radDivision' id='radDivision' value='Compounds' 
								onchange='textOther(), showPREDepartment(0,0), showPREItem()'
								<?php echo ( $preId ? ( $division == "Compounds" ? "checked" : "" ) : "" );?>>
								<label> Compounds</label>
						</td>

						<td valign="top" rowspan="7"  class="border_top border_left border_bottom"> Purpose: </td>

						<td colspan="2" class="border_top border_right"></td>
					</tr>

					<tr>
						<td class="border_right">
							<input type='radio' name='radDivision' id='radDivision' value='Pipes' 
								onchange='textOther(), showPREDepartment(0,0), showPREItem()'
								<?php echo ( $preId ? ( $division == "Pipes" ? "checked" : "" ) : "" );?>> 
								<label> Pipes </label>
						</td>

						<td>
							<input type='checkbox' name='chkStock' id="chkStock"
								<?php echo ( $preId ? ( $purpose_st ? "checked" : "" ) : ( $Stock ? "checked" : "" ) );?>>
						</td>

						<td class="border_right">
							<label for="chkStock"> For Stock Inventory </label>
						</td>
					</tr>

					<tr>
						<td>
							<input type='radio' name='radDivision' id='radDivision' value='Corporate' 
								onchange='textOther(), showPREDepartment(0,0), showPREItem()'
								<?php echo ( $preId ? ( $division == "Corporate" ? "checked" : "" ) : "" );?>> 
								<label> Corporate </label>
						</td>

						<td>
							<input type='checkbox' name='chkDirec' id='chkDirec'
								<?php echo ( $preId ? ( $purpose_de ? "checked" : "" ) : ( $Direct ? "checked" : "" ) );?>>
						</td>

						<td class="border_right">
							<label for="chkDirec"> For Direct Expense </label>
						</td>
					</tr>

					<tr>
						<td>
							<input type='radio' name='radDivision' id='radDivision' value='PPR' 
								onchange='textOther(), showPREDepartment(0,0), showPREItem()'
								<?php echo ( $preId ? ( $division == "PPR" ? "checked" : "" ) : "" );?>> 
								<label> PPR</label>	
						</td>

						<td>
							<input type='checkbox' id='chkCapex' name='chkCapex'
								onchange='changetextbox()'
								<?php echo ( $preId ? ( $purpose_capex ? "checked" : "" ) : ( $Capex ? "checked" : "" ) );?>>
						</td>

						<td class="border_right">
							<label for="chkCapex"> For Capex </label>
						</td>
					</tr>

					<tr>
						<td class="border_bottom">
							<input type='radio' name='radDivision' id='radDivision' value='Others' 
								onchange='textOther(), showPREDepartment(0,0), showPREItem()'
								<?php echo ( $preId ? ( $division == "Others" ? "checked" : "" ) : "" );?>> 
								<label> Others </label>

							<input type='text' name='txtOtherDiv' id='txtOtherDiv' 
								<?php echo ( $preId ? ( $division == "Others" ? "" : "readOnly" ) : "readOnly" );?>
								value="<?php echo ( $preId ? ( $division == "Others" ? $division_other : "" ) : "" );?>"> 
						</td>

						<td></td>

						<td class="border_right">
							<label> Capex No.: </label>

							<input type='text' id='txtCapexNo' name='txtCapexNo' 
								<?php echo ( $preId ? ( $purpose_capex ? "" : "readOnly" ) : ( $Capex ? "" : "readOnly" ) );?>
								value="<?php echo ( $preId ? ( $purpose_capex ? $capex_number : "" ) : ( $Capex ? $CapexNo : "" ) );?>">
						</td>
					</tr>

					<tr>
						<td class='border_left'> Department </td>

    					<td>
							<?php
								$user_value = ( $preId ? $requisitioner_id : 0 );
							?>
							<select name='sltDept' id='sltDept' onchange="showUser(this.value,<?php echo $user_value;?>)">  
								<option vlaue='0'></option>
								<?php
									if ( $preId ){
										$qryD = mysqli_prepare($db, "CALL sp_Department_Dropdown_New(?, 1)");
										mysqli_stmt_bind_param($qryD, 's', $division);
										$qryD->execute();
										$resultD = mysqli_stmt_get_result($qryD); //return results of query
								  		$processError2 = mysqli_error($db);

										if(!empty($processError2))
										{
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php'.'</td><td>'.$processError2.' near line 343.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultD))
											{
												
												$deptID = $row['id'];
												$deptCode = $row['code'];

												if ( $preId ){
													if ( $dept_id == $deptID )
														echo "<option value=".$deptID." selected>".$deptCode."</option>";
													else echo "<option value=".$deptID.">".$deptCode."</option>";
												}
												else{
													if ( $Dept == $deptID )
														echo "<option value=".$deptID." selected>".$deptCode."</option>";
													else echo "<option value=".$deptID.">".$deptCode."</option>";
												}
											}
											$db->next_result();
											$resultD->close();
										}
									}
								?>
							</select>
						</td>

						<td></td>

						<td class="border_right">
							<label > Capex Name: </label>

							<input type='text' id='txtCapexName' name='txtCapexName'
								<?php echo ( $preId ? ( $purpose_capex ? "" : "readOnly" ) : ( $Capex ? "" : "readOnly" ) );?>
								value="<?php echo ( $preId ? ( $purpose_capex ? $capex_name : "" ) : ( $Capex ? $CapexName : "" ) );?>">
						</td>
					</tr>

					<tr>
						<td class='border_left border_bottom'> Requisitioner </td>

						<td class='border_bottom'>

							<select id='sltIssToUser' name='sltIssToUser'>
								<option></option> 
							</select>

						</td>

						<td class="border_bottom">
							<input type='checkbox' id='chkOther' name='chkOther' 
								onchange='changetextbox()'
								<?php echo ( $preId ? ( $purpose_other ? "checked" : "" ) : ( $Other ? "checked" : "" ) );?>>
						</td>

						<td class="border_right border_bottom">
							<label> Others:</label> 

							<input type='text' name='txtOther' id='txtOther'
								<?php echo ( $preId ? ( $purpose_other ? "" : "readOnly" ) : ( $Other ? "" : "readOnly" ) );?>
								value="<?php echo ( $preId ? ( $purpose_other ? $purpose_other_comments : "" ) : ( $Other ? $OtherPurpose : "" ) );?>">
						</td>
					</tr>

					<tr class="spacing" valign="bottom">
		    			<td> Item Type </td>

		    			<td colspan="5">
		    				<select name='sltSupplyType' id='sltSupplyType' onchange='showPREItem()'>
								<option></option>
		    					<?php
			    					$qryST = "CALL sp_PRE_Item_Type_Dropdown()"; 
									$resultST = mysqli_query($db, $qryST);
									$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php'.'</td><td>'.$processError1.' near line 400.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultST))
										{
											$supplyTypeID = $row['id'];
											$supplyTypeDd = $row['supply_type'];

											if ( $preId ) {
												if ( $supply_subtype == $supplyTypeID )
													echo "<option value=".$supplyTypeID." selected>".$supplyTypeDd."</option>";
											}
											else {
												if ( $SupplyType == $supplyTypeID )
													echo "<option value=".$supplyTypeID." selected>".$supplyTypeDd."</option>";
												else echo "<option value=".$supplyTypeID.">".$supplyTypeDd."</option>";
											}

										}
										$db->next_result();
										$resultST->close();
									}
								?>
							</select>
						</td>
		    		</tr>
				</table>

				<table class="child_tables_form">
					<tr> 
						<?php 
							if( $preId ){
						?> 
								<th>Cancelled?</th>
						<?php 
							}
						?>
						<th>PCF?</th>
						<th>Item No.</th>
						<th>Quantity</th>
						<th>UOM</th>
						<th>Item</th>
						<th>Comments</th>
					</tr>

					<?php
						if ( $preId ){
							$count = count($supplies);

							for ( $i=0 ; $i<$count ; $i++ ){
					?>
								<tr>
									<td>
										<?php
											if(array_search(38, $session_Permit)){
												if ( $item_canceled[$i] == 1 ){
										?>
													<input type='checkbox' checked disabled>
													<input type='hidden' name='chkDelete[<?php echo ($i+1);?>]' value='1'>
										<?php
												}
												elseif ( $with_po[$i] == 1 ){ 
										?>
													<input type='checkbox' name='chkDelete[<?php echo ($i+1);?>]' value='1' disabled>
										<?php
												}
												else{
										?>
													<input type='checkbox' name='chkDelete[<?php echo ($i+1);?>]' value='1'>
										<?php
												}
												$_SESSION['delete_pre'] = true;
											}else{
												unset($_SESSION['delete_pre']);
											}
										?>
									</td>

									<td>
										<?php
											if ( $item_canceled[$i] == 1 ){
												if ( $pcf[$i] == 1 ){
										?>
													<input type='checkbox' checked disabled>
													<input type='hidden' name='chkPCF[<?php echo ($i+1);?>]' value='1' disabled>
										<?php
												}else{
										?>
													<input type='checkbox'  disabled>
													<input type='hidden' name='chkPCF[<?php echo ($i+1);?>]' value='0'>
										<?php
												}
											}
											elseif ( $with_po[$i] == 1 ){
												if ( $pcf[$i] == 1 ){
										?>
													<input type='checkbox' checked disabled>
													<input type='hidden' name='chkPCF[<?php echo ($i+1);?>]' value='1' disabled>
										<?php
												}else{
										?>
													<input type='checkbox'  disabled>
													<input type='hidden' name='chkPCF[<?php echo ($i+1);?>]' value='0'>
										<?php
												}
											}
											else{
												if ( $pcf[$i] == 1 ){
										?>
													<input type='checkbox' name='chkPCF[<?php echo ($i+1);?>]' value='1' checked>
										<?php
												}else{
										?>
													<input type='checkbox' name='chkPCF[<?php echo ($i+1);?>]' value='1'>
										<?php
												}
											}
										?>
									</td>

									<td>
										<?php echo ($i+1);?> 
										<input type='hidden' name='hidItemNo[<?php ($i+1);?>]' value='<?php echo ($i+1);?>'>
										<input type='text' name='hidpreTransItemId[<?php ($i+1);?>]' value='<?php echo $preTransId[$i];?>'>
									</td>

									<td>
										<input type='text' name='txtQuantity[<?php echo ($i+1);?>]' value='<?php echo $quantity[$i];?>' size="10">
									</td>

									<td>
										<select name='txtUom[<?php echo ($i+1);?>]'>
											<?php
												$qryU = "CALL sp_UOM_Dropdown(0)";
												$resultU = mysqli_query($db, $qryU);
												$processError2 = mysqli_error($db);

												if(!empty($processError2))
												{
													error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php'.'</td><td>'.$processError2.' near line 475.</td></tr>', 3, "errors.php");
													header("location: error_message.html");
												}
												else
												{
													while($row = mysqli_fetch_assoc($resultU))
													{
														$uomID = $row['id'];
														$uomDd = $row['code'];

														if ( $uom[$i] == $uomID )
															echo "<option value=".$uomID." selected>".$uomDd."</option>";
														else echo "<option value=".$uomID.">".$uomDd."</option>";
													}
													$db->next_result();
													$resultU->close();
												}
											?>
										</select>
									</td>

									<td>
										<select name='supplyDD[<?php echo ($i+1);?>]'>
											<?php
											######### modify dropdown,add filter
											if ( $item_type[$i] == 'r' ){

												if( $division == "PPR" ){
													$qryS = mysqli_prepare($db, "CALL sp_RawMaterials_PICSYS_Dropdown(?)");
												}else{
													$qryS = mysqli_prepare($db, "CALL sp_RawMaterials_Dropdown(?)");
												}

												mysqli_stmt_bind_param($qryS, 's', $division);
												$qryS->execute();
												$resultS = mysqli_stmt_get_result($qryS);

											}elseif ( $item_type[$i] == 's' ){

												$qryS = mysqli_prepare($db, "CALL sp_Suppliesa_Dropdown(?, ?)");
												mysqli_stmt_bind_param($qryS, 'is', $supply_subtype, $division);
												$qryS->execute();
												$resultS = mysqli_stmt_get_result($qryS);

											}
												$processError3 = mysqli_error($db);
											
												if ($processError3){
													error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php'.'</td><td>'.$processError3.' near line 501.</td></tr>', 3, "errors.php");
													header("location: error_message.html");
												}
												else
												{
													echo "<option value='0'></option>";
													while($row = mysqli_fetch_assoc($resultS))
													{
														$supplyId = $row['id'];
														$supply = $row['name'];
														
														if ( $supplies[$i] == $supplyId ){

															if ( $item_type[$i] == 'r' ) {
																echo "<option value='".$supplyId.":r' selected>".$supply."</option>";
															}elseif ( $item_type[$i] == 's' ){
																echo "<option value='".$supplyId.":s' selected>".$supply."</option>";
															}

														}else{

															if ( $item_type[$i] == 'r' ) {
																echo "<option value='".$supplyId.":r'>".$supply."</option>";
															}elseif ( $item_type[$i] == 's' ){
																echo "<option value='".$supplyId.":s'>".$supply."</option>";
															}

														}
													}
													$db->next_result();
													$resultS->close();
												}
											?>
										</select>
									</td>

									<td>
										<input type='text' name='txtComments[<?php echo ($i+1);?>]' value='<?php echo $comments[$i];?>'>
									</td>
								</tr>
					<?php
							}
						}else{
							for ( $i=1; $i<=8; $i++ ){
					?> 
								<tr>
									<input type='hidden' name='hidpreTransItemId[<?php echo $i;?>]' value="0">	

									<td>
										<input type='checkbox' name='chkPCF[<?php echo $i;?>]' value='1'>
									</td>	

									<td>
										<?php echo $i;?> <input type='hidden' name='hidItemNo[<?php echo $i;?>]' value='<?php echo $i;?>'>
									</td>

									<td>
										<input type='text' name='txtQuantity[<?php echo $i;?>]' size="10">
									</td>

									<td>
										<select name='txtUom[<?php echo $i;?>]'>
											<?php
											$qryU = "CALL sp_UOM_Dropdown(0)"; //with default blank option for new supply
											$resultU = mysqli_query($db, $qryU);
											$processError2 = mysqli_error($db);

											if(!empty($processError2))
											{
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_pre.php'.'</td><td>'.$processError2.' near line 556.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultU))
												{
													$uomID = $row['id'];
													$uomDd = $row['code'];

														echo "<option value=".$uomID.">".$uomDd."</option>";
												}
												$db->next_result();
												$resultU->close();
											}
											?>
										</select>
									</td>

									<td>
										<select id='supplyDD<?php echo $i;?>' name='supplyDD[<?php echo $i;?>]'>

										</select>
									</td>

									<td>
										<input type='text' name='txtComments[<?php echo $i;?>]'>
									</td>
								</tr>
					<?php 
							}
						}
					?>
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<input type="submit" name="btnSavePRE" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='<?php echo PG_PRE_HOME;?>'">
							<input type='hidden' name='hidPreId' value="<?php echo $preId;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $preId ? $createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $preId ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>