<html>
	<head>
		<?php 
			require '/include/header.php';
			require ("include/unset_value.php");
			require("include/database_connect.php");
			if( $_SESSION['customer_pipes'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}
		?>
		<title>Customer (Pipes) - Home</title>
		<link rel="stylesheet" type="text/css" href="css/row_color.css">
	</head>
	<body>
		<div class="form">
			<span>Customers (Pipes)</span>
			<?php	
				$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
				$page = ($_GET['page'] ? $_GET['page'] : 1);
				$qsone = "";

			echo"<div id='search'>
					<form method='get' action='customer_pipes.php'>
						<input type='text' name='search' value='".$_GET['search']."'>&nbsp;&nbsp;
						<input type='hidden' name='page' value=$page>
						<input type='hidden' name='qsone' value=$qsone>
						<input type='submit' value='Search'>&nbsp;&nbsp;";
			if(array_search(103, $session_Permit)){
				echo "	<input type='button' name='btnAddCustomer' value='Add Customer' onclick=\"location.href='new_customer_pipes.php?id=0'\">";	
				$_SESSION['add_customer_pipes'] = true;
			}else{
				unset($_SESSION['add_customer_pipes']);
			}

			echo "	</form>
				 </div>";

				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>customer_pipes.php'.'</td><td>'.$error.' near line 45.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryC= "CALL sp_Customer_Pipes_Home('$search', NULL, NULL)"; //query all rows
					$resultC = mysqli_query($db, $qryC); //return results of query
					$total_results = mysqli_num_rows($resultC); //return number of rows of result

					$db->next_result();
					$resultC->close();

					$targetpage = "customer_pipes.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = "CALL sp_Customer_Pipes_Home('$search', '$start', '$end')";
					$result = mysqli_query($db, $qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>customer_pipes.php'.'</td><td>'.$processError.' near line 66.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
							if( isset($_SESSION['SUCCESS'])) 
							{
								echo '<ul id="success">';
								echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
								echo '</ul>';
								unset($_SESSION['SUCCESS']);
							}
					} 
				}
			?>
			<table id='width' class='home6'>
				<tr>
					<td class='borderless' colspan='6' height='50' valign='top'>
						<?php echo $pagination;?>
					</td>
				</tr>
				<tr>
				    <th>Name</th>
				    <th>Active?</th>
				    <th></th>
				</tr>
				<?php
					while( $row = mysqli_fetch_assoc($result) ){
				?>
						<tr>
							<td> <?php echo $row['customers']; ?> </td>
							<td> <?php echo $row['active']; ?> </td>
							<?php

								if(array_search(104, $session_Permit)){
							?>
									<td> <input type='button' value='Edit' onclick="location.href='new_customer_pipes.php?id=<?php echo $row['id']; ?>'"> </td>
							<?php
									$_SESSION['edit_customer_pipes'] = true;
								}else{
									unset($_SESSION['edit_customer_pipes']);
								}

							?>
						</tr>
				<?php
					}
				?>
				<tr>
					<td class='borderless' colspan='6' height='50' valign='bottom'>
						<?php echo $pagination;?>
					</td>
				</tr>
			</table>
			<div>
			    <?php 
			    	require('/include/footer.php'); 
			    	require("include/database_close.php");
			    ?>
			</div>
		</div>
	</body>
</html>