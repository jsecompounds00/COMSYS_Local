<html>
	<head>
		<title>PAM Analysis - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = "";
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/unset_value.php");
			require("/include/init_unset_values/material_balance_unset_value.php");

			if( $_SESSION['pam_analysis'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION['page'] = $_GET['page'];
			$_SESSION['search'] = $_GET['search'];
			$_SESSION['qsone'] = $_GET['qsone'];
		?>

		<div class="wrapper">

			<span> <h3> PAM Analysis </h3> </span>

			<div class="search_box">
				<form method='get' action='pam_analysis.php'>
					<input type='hidden' name='page' value="<?php echo $page;?>">
					<input type='hidden' name='qsone' value="<?php echo $qsone;?>">
					<table class="search_tables_form">
						<tr>
							<td> FG Name: </td>
							<td> <input type='text' name='search' value='<?php echo $_GET['search'];?>'> </td>
							<td> <input type='submit' value='Search'> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pam_analysis.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryLC = mysqli_prepare($db, "CALL sp_PAM_Analysis_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryLC, 's', $search);
					$qryLC->execute();
					$resultLC = mysqli_stmt_get_result($qryLC); //return results of query

					$total_results = mysqli_num_rows($resultLC); //return number of rows of result

					$db->next_result();
					$resultLC->close();

					$targetpage = "pam_analysis.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_PAM_Analysis_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pam_analysis.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
				<table class="home_pages">
					<tr>
						<td colspan='7'>
							<?php echo $pagination;?>
						</td>
					</tr>
					<tr>
					    <th>Product Grade</th>
					    <th>Active</th>
					    <?php
					    	if(array_search(85, $session_Permit)){ 
					   	?>
					    		<th>Electricity Consumption</th>
					    <?php 
							}
					    	if(array_search(86, $session_Permit)){ 
					    ?>
					    		<th>Extrusion Parameters</th>
					    <?php 
							}
					    	if(array_search(87, $session_Permit)){ 
					    ?>
					    		<th>Machine Rate</th>
					    <?php 
					    	} 
					    ?>
					    <th colspan='2'>Material Balance</th>
					</tr>
					<?php 
						while($row = mysqli_fetch_assoc($result)) { 
					?>
							<tr>
								<td> <?php echo $row['FG'];?></td>

								<td> <?php echo ($row['active'] ? 'Y' : 'N');?></td>

								<?php
									if(array_search(85, $session_Permit)){
								?>
										<td>
											<input type='button' name='btnK' value='KwH' onclick="location.href='new_electricity_consumption_monitoring.php?id=<?php echo $row['FGId'];?>'">
										</td>
								<?php
										$_SESSION['current_reading'] = true;
									}else{
										unset($_SESSION['current_reading']);
									}

									if(array_search(86, $session_Permit)){
								?>
										<td>
											<input type='button' name='btnP' value='Parameters' onclick="location.href='new_extrusion_parameters.php?id=<?php echo $row['FGId'];?>'">
										</td>
								<?php
										$_SESSION['parameters'] = true;
									}else{
										unset($_SESSION['parameters']);
									}

									if(array_search(87, $session_Permit)){
								?>
										<td>
											<input type='button' name='btnM' value='Rate' onclick="location.href='new_machine_rate.php?id=<?php echo $row['FGId'];?>'">
										</td>
								<?php
										$_SESSION['rates'] = true;
									}else{
										unset($_SESSION['rates']);
									}
								?>
								<td>
									<input type='button' name='btnC' value='Add Computation' onclick="location.href='new_material_balance_computation.php?fg_id=<?php echo $row['FGId'];?>&mbc_id=0'">
								</td>

								<td>
									<input type='button' name='btnC' value='History' onclick="location.href='material_balance_history.php?id=<?php echo $row['FGId'];?>&page=1'">
								</td>
							</tr>
					<?php	
						}
					?>
					<tr>
						<td colspan='7'>
							<?php echo $pagination;?>
						</td>
					</tr>
				</table>
			<?php
				}
			?>		
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>