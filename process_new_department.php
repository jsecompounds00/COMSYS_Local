<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_department.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidDeptid = $_POST['hidDeptid'];
		$txtCode = $_POST['txtCode'];
		$txtDepartment = $_POST['txtDepartment'];
		$chkBudget = $_POST['chkBudget'];
		$txtAccountCode = $_POST['txtAccountCode'];

		if ( isset($_POST['chkActive']) )
			$chkActive = 1;
		else
			$chkActive = 0;
		if ( isset($_POST['chkCmpds']) )
			$chkCmpds = 1;
		else
			$chkCmpds = 0;
		if ( isset($_POST['chkPips']) )
			$chkPips = 1;
		else
			$chkPips = 0;
		if ( isset($_POST['chkCorp']) )
			$chkCorp = 1;
		else
			$chkCorp = 0;
		if ( isset($_POST['chkPPR']) )
			$chkPPR = 1;
		else
			$chkPPR = 0;

		$_SESSION['SESS_DEP_Code'] = $txtCode;
		$_SESSION['SESS_DEP_Department'] = $txtDepartment;
		$_SESSION['SESS_DEP_Active'] = $chkActive;
		$_SESSION['SESS_DEP_Cmpds'] = $chkCmpds;
		$_SESSION['SESS_DEP_Pips'] = $chkPips;
		$_SESSION['SESS_DEP_Corp'] = $chkCorp;
		$_SESSION['SESS_DEP_PPR'] = $chkPPR;
		$_SESSION['SESS_DEP_Budget'] = $chkBudget;
		$_SESSION['SESS_DEP_AcctCode'] = $txtAccountCode;

		//Input Validations
		if ( empty($txtCode) ){
			$errmsg_arr[] = '* Code is missing.';
			$errflag = true;
		}
		if (is_numeric($txtCode) || strlen($txtCode) > 3){
			$errmsg_arr[] = '* Invalid code.';
			$errflag = true;
		}
		if ( empty($txtDepartment) ){
			$errmsg_arr[] = '* Department is missing.';
			$errflag = true;
		}
		if (is_numeric($txtDepartment)){
			$errmsg_arr[] = '* Invalid name.';
			$errflag = true;
		}
		if ( isset($_POST["chkBudget"]) && $txtAccountCode == "" ){
			$errmsg_arr[] = "* Account Code can't be blank.";
			$errflag = true;
		}
		if( $txtAccountCode == "" ){
			$txtAccountCode = NULL;
		}


		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_DEPARTMENT.$hidDeptid);
			exit();
		}

		if($hidDeptid == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidDeptid == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		$qry = mysqli_prepare($db, "CALL sp_Department_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'issiiiissiiiis', $hidDeptid, $txtCode, $txtDepartment
												  , $chkActive, $chkCmpds, $chkPips, $chkCorp
												  , $createdAt, $updatedAt, $createdId, $updatedId
												  , $chkPPR, $chkBudget, $txtAccountCode);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_department.php'.'</td><td>'.$processError.' near line 89.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidDeptid)
				$_SESSION['SUCCESS']  = 'Successfully updated department.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new department.';
			//echo $_SESSION['SUCCESS'];
			header("location:department.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	