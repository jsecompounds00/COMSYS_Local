<html>
	<head>
		<script src="js/datetimepicker_css.js"></script> 
		<script src="js/batch_ticket_js.js"></script>
		<?php
			require("/include/database_connect.php");      
		?>
		<title> Additional Tests (Product Evalution - No Replication) - Add </title>
	</head>
	<body onload="showJRDDataNoReplicate()">

		<form method='post' action='process_new_jrd_test_evaluation_noreplicate.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/jrd_init_value.php");
			?>

			<div class="wrapper">
				
				<span> <h3> Add Additional Tests </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);
					}
					elseif( isset($_SESSION['SUCCESS']) ){
						echo '<ul id="success">';
						echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
						echo '</ul>';
						unset($_SESSION['SUCCESS']);
						echo '<br><br><br>';
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Request Type:</td>
						<td><b>JRD</b></td>
					</tr>
					<tr>
						<td>JRD Number:</td>
						<td>
							<select name="sltJRDNumber" id="sltJRDNumber" onchange="showJRDDataNoReplicate()">
							<?php 
								$jrd = 1;
								$qryJ = mysqli_prepare($db, "CALL sp_JRD_Number_NoReplicate_Dropdown(?)");
								mysqli_stmt_bind_param($qryJ, 'i', $jrd);
								$qryJ->execute();
								$resultJ = mysqli_stmt_get_result($qryJ);
								$processErrorJ = mysqli_error($db);
							
								if ($processErrorJ){
									error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_jrd_test_evaluation_noreplicate.php'.'</td><td>'.$processErrorJ.' near line 20.</td></tr>', 3, "errors.php");
									header("location: error_message.html");
								}
								else
								{	
									// echo "<option value=0></option>";
									while($row = mysqli_fetch_assoc($resultJ))
									{
										$jrd_id = $row['jrd_id'];
										$jrd_number = $row['jrd_number'];
										if ( $initJRDNumberID == $jrd_id ){
											echo "<option value=".$jrd_id." selected>".$jrd_number."</option>";
										}else{
											echo "<option value=".$jrd_id." >".$jrd_number."</option>";
										}
											
									}
									$db->next_result();
									$resultJ->close();
								}
							?>
							</select>
						</td>
					</tr>
				</table>

				<div id="tbJRDData">
				</div>

				<table class="parent_tables_form">
					<tr class='align_bottom'>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type="submit" name="btnSaveNAdd" value="Save & Add Evaluation">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='jrd.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
						</td>
					</tr> 
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>