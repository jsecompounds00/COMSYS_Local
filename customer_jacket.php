<html>
	<head>
		<title>Customer Jacket - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/init_unset_values/debit_invoice_unset_value.php");
			require("/include/init_unset_values/credit_payment_unset_value.php");

			if( $_SESSION['customer_jacket'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Customer Jacket </h3> </span>

			<div class="search_box">
				<form method="get" action="customer_jacket.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<input type="hidden" name="qsone" value="<?php echo $qsone; ?>">
					<table class="search_tables_form">
						<tr>
							<td> Customer: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php 
									if(array_search(141, $session_Permit)) {
								?>
										<input type='button' value='Debit (Invoice)' onclick="location.href='debit_invoice.php?id=0'">
								<?php 
										$_SESSION['debit_invoice'] = true;
									}else{
										unset($_SESSION['debit_invoice']);
									}
								?>
							</td>
							<td>
								<?php
									if(array_search(142, $session_Permit)) {
								?>
										<input type='button' value='Credit (Payment)' onclick="location.href='credit_payment.php?id=0'">
								<?php 
										$_SESSION['credit_payment'] = true;
									}else{
										unset($_SESSION['credit_payment']);
									}
								?>
							</td>
							<td>
								<?php
									if(array_search(167, $session_Permit)) {
								?>
										<input type='button' value='Debit Memo' onclick="location.href='debit_memo.php?id=0'">
								<?php 
										$_SESSION['debit_memo'] = true;
									}else{
										unset($_SESSION['debit_memo']);
									}
								?>
							</td>
							<td>
								<?php
									if(array_search(166, $session_Permit)) {
								?>
										<input type='button' value='Credit Memo' onclick="location.href='credit_memo.php?id=0'">
								<?php 
										$_SESSION['credit_memo'] = true;
									}else{
										unset($_SESSION['credit_memo']);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>customer_jacket.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Customer_Jacket_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 's', $search);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "customer_jacket.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>customer_jacket.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan="7"> <?php echo $pagination;?> </td>
						</tr>
						<tr>
						    <th>Customer</th>
						    <th>Active</th>
						    <th>Credit Limit</th>
						    <th>Agent</th>
						    <th>Last Trxn Date</th>
						    <th>Total A/R</th>
						    <th></th>
						</tr>
						<?php
							while ($row = mysqli_fetch_assoc($result)) {
								$CustomerID = $row['CustomerID'];
								$CustomerName = $row['CustomerName'];
								$Active = $row['Active'];
								$CreditLimitAmount = $row['CreditLimitAmount'];
								$Agent = $row['Agent'];
								$LastTrxnDate = $row['LastTrxnDate'];
								$ARAmount = $row['ARAmount'];
								$Currency = $row['Currency'];
						?>
								<tr>
									<td> <?php echo $CustomerName; ?> </td>
									<td> <?php echo $Active; ?> </td>
									<td> <?php echo $Currency." ".number_format((float)($CreditLimitAmount), 2, '.', ','); ?> </td>
									<td> <?php echo $Agent; ?> </td>
									<td> <?php echo $LastTrxnDate; ?> </td>
									<td> <?php echo $Currency." ".number_format((float)($ARAmount), 2, '.', ','); ?> </td>
									<td> 
										<?php 
											if(array_search(143, $session_Permit)) {
										?>
											<input type='button' value='History' onclick="location.href='customer_jacket_history.php?page=1&id=<?php echo $CustomerID; ?>'"> 
										<?php 
												$_SESSION['customer_jacket_history'] = true;
											} else{
												unset($_SESSION['customer_jacket_history']);
											}
										?>
									</td>
								</tr>
						<?php			
							}
						?>
						<tr>
							<td colspan="7"> <?php echo $pagination;?> </td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php
			require("include/database_close.php");
		?>
	</footer>
</html>