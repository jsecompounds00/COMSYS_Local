<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_fg_standard_parameter.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$FGCategory = strval($_GET['category']);

				$qryFGC = mysqli_prepare($db, "CALL sp_FG_Parameters_ID_Query( ? )");
				mysqli_stmt_bind_param($qryFGC, 's', $FGCategory);
				$qryFGC->execute();
				$resultFGC = mysqli_stmt_get_result($qryFGC);
				$processErrorFGC = mysqli_error($db);

				if(!empty($processErrorFGC))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_fg_standard_parameter.php'.'</td><td>'.$processErrorFGC.' near line 35.</td></tr>', 3, "errors.php");
					// header("location: error_message.html");
				}else{
					$counter = mysqli_num_rows($resultFGC);

					while( $rowFGC = mysqli_fetch_assoc($resultFGC) ){
						$FGCID = (int)$rowFGC["id"];
					}
						$db->next_result();
						$resultFGC->close();

				}

				if ( $counter == 0 ){ 
					$FGCID = 0;
				}

				if($FGCID)
				{ 
					echo "<title> FG Standard Parameter - Edit</title>";

					$qry = mysqli_prepare($db, "CALL sp_FG_Parameters_Query( ? )");
					mysqli_stmt_bind_param($qry, 'i', $FGCID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_fg_standard_parameter.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$FGZ1 = $row["z1"];
							$FGZ2 = $row["z2"];
							$FGDH = $row["dh"];
							$FGCS = $row["cutter_speed"];
							$FGSS = $row["screw_speed"];
							$FGcreated_at = $row["created_at"];
							$FGcreated_id = $row["created_id"];
						

						}
						$db->next_result();
						$result->close();

						############ .............
						$qryPI = "SELECT id from comsys.fg_standard_parameters";
						$resultPI = mysqli_query($db, $qryPI); 
						$processErrorPI = mysqli_error($db);

						if ( !empty($processErrorPI) ){
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_fg_standard_parameter.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}else{
								$id = array();
							while($row = mysqli_fetch_assoc($resultPI)){
								$id[] = (int)$row['id'];
							}
						}
						$db->next_result();
						$resultPI->close();

						############ .............
						if( !in_array($FGCID, $id, TRUE) ){ //
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_fg_standard_parameter.php</td><td>The user tries to edit a non-existing Finished Good id.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
							// print_r(!in_array($custId, $id, TRUE));
						}
					}

					
				}
				else
				{
					echo "<title>Finished Good Category - Add</title>";

				}
			}
		?>
	</head>
	<body>

		<form method='post' action='process_new_fg_standard_parameters.php'>

			<?php 
				require("/include/header.php");
				require("/include/init_unset_values/fg_parameters_init_value.php");
				
				if ( $FGCID ){
					// if( $_SESSION['edit_customer'] == false) 
					// {
					// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
					// 	session_write_close();
					// 	header("Location:comsys.php");
					// 	exit();
					// }
				}else{
					// if( $_SESSION['edit_customer'] == false) 
					// {
					// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
					// 	session_write_close();
					// 	header("Location:comsys.php");
					// 	exit();
					// }
				}
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $FGCID ? "Edit " : "New " );?> FG Standard Parameter for <?php echo $FGCategory;?> </h3> </span>

				<?php 
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					
					<input type="hidden" name="hidFGCategory" value="<?php echo $FGCategory; ?>">
					<tr>
						<td>Zone 1:</td>
						<td colspan='2'>
							<input type="text" name="txtFGZ1" value="<?php echo ($FGCID  ? $FGZ1 : $initFGCFGZ1); ?>">
						</td>
					</tr>
					<tr>
						<td>Zone 2:</td>
						<td colspan='2'>
							<input type="text" name="txtFGZ2" value="<?php echo ($FGCID  ? $FGZ2 : $initFGCFGZ2); ?>">
						</td>
					</tr>
					<tr>
						<td>Die Head:</td>
						<td colspan='2'>
							<input type="text" name="txtFGDH" value="<?php echo ($FGCID  ? $FGDH : $initFGCFGDH); ?>">
						</td>
					</tr>
					<tr>
						<td>Die Head Cutter:</td>
						<td colspan='2'>
							<input type="text" name="txtFGCS" value="<?php echo ($FGCID  ? $FGCS : $initFGCFGCS); ?>">
						</td>
					</tr>
					<tr>
						<td>Extruder Speed:</td>
						<td colspan='2'>
							<input type="text" name="txtFGSS" value="<?php echo ($FGCID  ? $FGSS : $initFGCFGSS); ?>">
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveFGCategory" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='finished_goods_category.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidFGCID" value="<?php echo ( $FGCID  ); ?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $FGCID  ? $FGcreated_at : date('Y-m-d H:i:s') ); ?>">
							<input type="hidden" name="hidcreatedId" value="<?php echo ( $FGCID  ? $FGcreated_id : $_SESSION["SESS_USER_ID"] ); ?>">
						</td>
					</tr>

				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>