<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ticket.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$ticketID = $_GET['id'];

				if ( $ticketID ){

					// if( $_SESSION['ticket_view'] == false) 
					// {
					// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
					// 	session_write_close();
					// 	header("Location:comsys.php");
					// 	exit();
					// }
		
					echo "<title> Request - View Details </title>";
					$qry = mysqli_prepare($db, "CALL sp_Ticket_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $ticketID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ticket.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{ 
						while ( $row = mysqli_fetch_assoc( $result ) ){
							$attention_to_id = $row["attention_to_id"];
							$Sender = $row["Sender"];
							$request_date = $row["request_date"];
							$subject = $row["subject"];
							// $subject_array = explode('<br>',$row["subject"]);
							$request = $row["request"];
							// $request_array = explode('<br>',$row["request"]);
							$created_at = $row["created_at"];
							$created_id = $row["created_id"];
							$status_remarks = $row["status_remarks"];
							// $status_remarks_array = explode('<br>',$row["status_remarks"]);
							$status = $row["status"];
							$viewed = $row["viewed"];
						}
						$db->next_result();
						$result->close();

					}

				}else{

					// if( $_SESSION['ticket_add'] == false) 
					// {
					// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
					// 	session_write_close();
					// 	header("Location:comsys.php");
					// 	exit();
					// }
					echo "<title> Request - New </title>";
				}
			}
		?>
		<script src="js/tickets_js.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>

		<form method='post' action='process_new_ticket.php'>

			<?php
				require("/include/header.php");
				// require("/include/init_value.php");

				if ( $ticketID ){
					if( $_SESSION["SESS_USER_ID"] == $attention_to_id ){
						$qry = mysqli_prepare($db, "CALL sp_Ticket_Viewed_CRU(?)");
						mysqli_stmt_bind_param($qry, 'i', $ticketID);
						$qry->execute();
						$result = mysqli_stmt_get_result($qry); //return results of query
						$processError = mysqli_error($db);

						if(!empty($processError))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ticket.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $ticketID ? "View " : "New " ); ?> Request <?php echo ( $ticketID ? " from ".( $_SESSION["SESS_USER_ID"] == $created_id ? "Me" : $Sender ) : "" ); ?> </h3> </span>

				<?php	
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<colgroup><col width="200px"></col></colgroup>
					<?php
						if ( !$ticketID ){
					?>
							<tr>
								<td> Division: </td>
								<td>
									<input type="radio" name="radDivision" id="Compounds" value="Compounds" onchange="showDepartment()">
										<label for="Compounds"> Compounds </label>

									<input type="radio" name="radDivision" id="Corporate" value="Corporate" onchange="showDepartment()">
										<label for="Corporate"> Corporate </label>
								</td>
							</tr>

							<tr class="spacing2"></tr>

							<tr>
								<td> Department: </td>
								<td>
									<select name="sltDepartment" id="sltDepartment" onchange="showAttentionTo()">
										
									</select>
								</td>
							</tr>

							<tr class="spacing2"></tr>

							<tr>
								<td>Attention To:</td>
								<td>
									<select name="sltAttentionTo" id="sltAttentionTo">
									</select>
								</td>
							</tr>
							<tr class="spacing2"></tr>
					<?php
						}
					?>
					<tr>
						<td>Request Date:</td>
						<td>
							<input type="hidden" name="txtRequestDate" value="<?php echo ( $ticketID ? $request_date : date("Y-m-d") ); ?>" readonly>
							<b> <?php echo ( $ticketID ? $request_date : date('Y-m-d') ); ?> </b>
						</td>
					</tr>
					<tr class="spacing2"></tr>
					<tr valign='top'>
						<td> Subject: </td>
						<td>
							<?php 
								if ( $ticketID ){
									echo $subject;
								} else{
							?>		<textarea class="subject" name="txtSubject"></textarea>
							<?php
								}
							?>
						</td>
					</tr>

					<tr class="spacing2"></tr>
					<tr valign='top'>
						<td>Request:</td>
						<td>
							<?php 
								if ( $ticketID ){
									echo $request;
								} else{
							?>		<textarea class="paragraph" name="txtRequest"></textarea>
							<?php
								}
							?>
						</td>
					</tr>
					<tr class="spacing2"></tr>
					<?php
						if ( $ticketID ){
					?>	
							<tr>
								<td>Status:</td>
								<td> <b> <?php echo ( is_null( $status ) ? "Pending" : "Done" ); ?> </b> </td>
							</tr>
							<tr class="spacing2"></tr>
							<tr valign='top'>
								<td>Action Taken:</td>
								<td>
									<?php
										echo $status_remarks;
									?>
								</td>
							</tr>
					<?php
						}
					?>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="<?php echo ( $ticketID ? "hidden" : "submit"); ?>" name="btnSave" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='ticketing_system.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidTicketID" value="<?php echo $ticketID; ?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $ticketID ? $created_at : date('Y-m-d H:i:s') ); ?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $ticketID ? $created_id : $_SESSION["SESS_USER_ID"] ); ?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>