<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<?php 
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_jrd.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$jrdID = $_GET['id'];
				
				if($jrdID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_JRD_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $jrdID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_jrd.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							$jrd_number = htmlspecialchars($row['jrd_number']);
							$date_requested = $row['date_requested'];
							$date_needed = htmlspecialchars($row['date_needed']);
							$customer_id = $row['customer_id'];
							$tag_new_customer = $row['tag_new_customer'];
							$new_customer = htmlspecialchars($row['new_customer']);
							$contact_person = $row['contact_person'];
							$type_of_request = $row['type_of_request'];
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.jrd";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_jrd.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($jrdID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_jrd.php</td><td>The user tries to edit a non-existing jrd_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>JRD - Edit</title>";
				}
				else{

					echo "<title>JRD - New</title>";

				}
			}
		?>
		<script src="js/batch_ticket_js.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body onload="showCustomerReq('<?php echo $jrdID;?>'), disableNewCustomer()">

		<form method='post' action='process_new_jrd.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/jrd_init_value.php");
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $jrdID ? "Edit JRD No. ".$jrd_number : "New JRD" );?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td> JRD No.: </td>
						<td> <input type="text" name="txtJRDNumber" value="<?php echo ($jrdID ? $jrd_number : $initJRDNumber); ?>"> </td>
					</tr>
					<tr valign="bottom">
						<td> Date Requested: </td>
						<td>
							<input type="text" name="txtRequestedDate" id="txtRequestedDate" value="<?php echo ($jrdID ? $date_requested : $initDateRequested); ?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtRequestedDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr>
						<td> Date Needed: </td>
						<td>
							<input type="text" name="txtNeededDate" id="txtNeededDate" value="<?php echo ($jrdID ? $date_needed : $initDateNeedeed); ?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtNeededDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr valign='bottom'>
						<td> Customer: </td>
						<td>
							<select name='sltExistingCustomer' id='sltExistingCustomer'>
								<?php
									$qryC = "CALL sp_Customer_Dropdown()";
									$resultC = mysqli_query($db, $qryC);
									$processErrorC = mysqli_error($db);

									if ($processErrorC){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>user_dropdown.php'.'</td><td>'.$processErrorC.' near line 25.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										echo "<option value='0'></option>";
										$i = 0;
										while($row = mysqli_fetch_assoc($resultC)){
											
											if ( $jrdID ){
												if ( $customer_id == $row["id"] ){
													echo "<option value='".$row['id']."' id='optionCustomer".$i."' selected>".$row['name']."</option>";
												}else{
													echo "<option value='".$row['id']."' id='optionCustomer".$i."'>".$row['name']."</option>";
												}
											}else{
												if ( $initCustomerID == $row["id"] ){
													echo "<option value='".$row['id']."' id='optionCustomer".$i."' selected>".$row['name']."</option>";
												}else{
													echo "<option value='".$row['id']."' id='optionCustomer".$i."'>".$row['name']."</option>";
												}
											}
											
											$i++;
										}
										$db->next_result();
										$resultC->close();
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td> 
							<input type="checkbox" name="chkNewCustomer" id="chkNewCustomer" onchange="disableNewCustomer()" value="1" <?php echo ( $jrdID ? ( $tag_new_customer ? "checked" : "" ) : ( $initTagNewCustomer ? "checked" : "" ) ); ?>>
							<label for="chkNewCustomer"> New Customer: </label>
						</td>
						<td>  
							<input type="text" name="txtNewCustomer" id="txtNewCustomer" value="<?php echo ($jrdID ? $new_customer : $initNewCustomer); ?>" size="40">  
						</td>
					</tr>
					<tr>
						<td> Contact Person: </td>
						<td> 
							<input type="text" name="txtContactPerson" value="<?php echo ($jrdID ? $contact_person : $initContactPerson); ?>" size="40">  
						</td>
					</tr>
					<tr 
					</tr>
					<tr>
						<td valign="top"> Type of Request: </td>
						<td>
							<input type="radio" name="radRequestType" id="Development" value="Development" onchange="showCustomerReq(<?php echo $jrdID;?>)" <?php echo ( $jrdID ? ( $type_of_request == "Development" ? "checked" : "" ) : ( $initRequestType == "Development" ? "checked" : "" ) ); ?>> 
								<label for="Development">Product Development</label>
							<br>

							<input type="radio" name="radRequestType" id="Modification" value="Modification" onchange="showCustomerReq(<?php echo $jrdID;?>)"<?php echo ( $jrdID ? ( $type_of_request == "Modification" ? "checked" : "" ) : ( $initRequestType == "Modification" ? "checked" : "" ) ); ?>> 
								<label for="Modification">Product Modification</label>
							<br>

							<input type="radio" name="radRequestType" id="Evaluation" value="Evaluation" onchange="showCustomerReq(<?php echo $jrdID;?>)"<?php echo ( $jrdID ? ( $type_of_request == "Evaluation" ? "checked" : "" ) : ( $initRequestType == "Evaluation" ? "checked" : "" ) ); ?>> 
								<label for="Evaluation">Product Evaluation</label>

						</td>
					</tr>
				</table>

				<div id="tbodyCustomerReq">
				</div>
				
				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='jrd.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidJRDID' value='<?php echo $jrdID;?>'>
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $jrdID ? $createdAt : date('Y-m-d H:i:s') ); ?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $jrdID ? $createdId : 0 ); ?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>