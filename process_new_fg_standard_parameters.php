<?php 
	//Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_fg_standard_parameters.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$hidFGCID = $_POST['hidFGCID'];
		$hidFGCategory = $_POST['hidFGCategory'];
		$txtFGZ1 = $_POST['txtFGZ1'];
		$txtFGZ2 = $_POST['txtFGZ2'];
		$txtFGDH = $_POST['txtFGDH'];
		$txtFGCS = $_POST['txtFGCS'];
		$txtFGSS = $_POST['txtFGSS'];

		//Input Validations
		if ( $txtFGZ1 == "" ){
			$errmsg_arr[] = "* Zone 1 can't be blank.";
			$errflag = true;
		}
		if ( !is_numeric($txtFGZ1) ){
			$errmsg_arr[] = "* Zone 1 should be numeric.";
			$errflag = true;
		}

		if ( $txtFGZ2 == "" ){
			$errmsg_arr[] = "* Zone 2 can't be blank.";
			$errflag = true;
		}
		if ( !is_numeric($txtFGZ2) ){
			$errmsg_arr[] = "* Zone 2 should be numeric.";
			$errflag = true;
		}

		if ( $txtFGDH == "" ){
			$errmsg_arr[] = "* Die Head can't be blank.";
			$errflag = true;
		}

		if ( !is_numeric($txtFGDH) ){
			$errmsg_arr[] = "* Die Head should be numeric.";
			$errflag = true;
		}

		if ( $txtFGCS == "" ){
			$errmsg_arr[] = "* Die Cutter Speed can't be blank.";
			$errflag = true;
		}
		if ( !is_numeric($txtFGCS) ){
			$errmsg_arr[] = "* Die Cutter Speed should be numeric.";
			$errflag = true;
		}

		if ( $txtFGSS == "" ){
			$errmsg_arr[] = "* Extruder Speed can't be blank.";
			$errflag = true;
		}
		if ( !is_numeric($txtFGSS) ){
			$errmsg_arr[] = "* Extruder Speed should be numeric.";
			$errflag = true;
		}

		//SESSION stored in init_value
		$_SESSION['SESS_FGC_FGZ1'] = $txtFGZ1;
		$_SESSION['SESS_FGC_FGZ2'] = $txtFGZ2;
		$_SESSION['SESS_FGC_FGDH'] = $txtFGDH;
		$_SESSION['SESS_FGC_FGCS'] = $txtFGCS;
		$_SESSION['SESS_FGC_FGSS'] = $txtFGSS;

		//If there are input validations, redirect back to the login form
		if($errflag) 
		{
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:new_fg_standard_parameter.php?category=$hidFGCategory");
			exit();
		}

		if($hidFGCID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
			$updatedAt = date('Y-m-d H:i:s');
		if($hidFGCID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidcreatedId'];
			$updatedId = $_SESSION['SESS_USER_ID'];


		// COMMIT TO DATABASE
		$qry = mysqli_prepare($db, "CALL sp_FG_Parameters_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isdddddssii', $hidFGCID, $hidFGCategory, $txtFGZ1 
										  ,	$txtFGZ2, $txtFGDH, $txtFGCS, $txtFGSS
										  , $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

			
		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_fg_standard_parameters.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidFGCID)
				$_SESSION['SUCCESS']  = 'Successfully updated category.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new category.';
			//echo $_SESSION['SUCCESS'];
			header("location:finished_goods_category.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		// $db->next_result();
		// $result->close(); 

		require("include/database_close.php");
	}
?>