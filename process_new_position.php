<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_positions.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidPosID = $_POST['hidPosID'];
		$txtName = clean($_POST['txtName']);
		$txtRate = clean($_POST['txtRate']);

		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;

		$_SESSION['txtName'] = $txtName;
		$_SESSION['chkActive'] = $chkActive;

		//Input Validations
		if ( empty($txtName) ){
			$errmsg_arr[] = '* Position name is missing.';
			$errflag = true;
		}
		if (is_numeric($txtName)){
			$errmsg_arr[] = '* Invalid Position name.';
			$errflag = true;
		}
		if (!is_numeric($txtRate)){
			$errmsg_arr[] = '* Invalid daily rate.';
			$errflag = true;
		}

		if($hidPosID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidPosID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_positions.php?id=$hidPosID");
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Positions_CRU(?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isdissii', $hidPosID, $txtName, $txtRate, $chkActive
											   , $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_positions.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidPosID)
				$_SESSION['SUCCESS']  = 'Successfully updated positions.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new positions.';
			//echo $_SESSION['SUCCESS'];
			header("location:positions.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	