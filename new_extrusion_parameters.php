<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_extrusion_parameters.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$fgid = $_GET['id'];

				$qryE = "SELECT id FROM extrusion_parameters WHERE finished_good_id = $fgid";
				$resultE = mysqli_query($db, $qryE);
				$processErrorE = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_extrusion_parameters.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{	
					$row = mysqli_fetch_assoc($resultE);
					$eid = $row['id'];
					$db->next_result();
					$resultE->close();
				}

				if ( $eid )
					$qry = mysqli_prepare($db, "CALL sp_Extrusion_Parameters_Query_2(?)");
				else 
					$qry = mysqli_prepare($db, "CALL sp_Extrusion_Parameters_Query(?)");
				mysqli_stmt_bind_param($qry, 'i', $fgid);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry); //return results of query
				$processError = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_extrusion_parameters.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{	
					$ExtrusionParametersItemID = array();
					$extrusion_id = array();
					$machine_name = array();
					$zone_1 = array();
					$zone_2 = array();
					$zone_3 = array();
					$zone_4 = array();
					$zone_5 = array();
					$zone_6 = array();
					$zone_7 = array();
					$die_head_a = array();
					$die_head_b = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$ExtrusionParametersID = $row['ExtrusionParametersID'];
						$FGName = $row['FGName'];
						$FGLocal = $row['FGLocal'];
						$FGSoft = $row['FGSoft'];
						$createdAt = $row['created_at'];
						$createdId = $row['created_id'];

						$ExtrusionParametersItemID[] = $row['ExtrusionParametersItemID'];

						$extrusion_id[] = $row['extrusion_id'];

						$machine_name[] = $row['machine_name'];

						$zone_1[] = $row['zone_1'];

						$zone_2[] = $row['zone_2'];

						$zone_3[] = $row['zone_3'];

						$zone_4[] = $row['zone_4'];

						$zone_5[] = $row['zone_5'];

						$zone_6[] = $row['zone_6'];

						$zone_7[] = $row['zone_7'];

						$die_head_a[] = $row['die_head_a'];

						$die_head_b[] = $row['die_head_b'];

					}
					$db->next_result();
					$result->close();
				}

				if ( $ExtrusionParametersID ){
					echo "<title>Extrusion Parameters - Update</title>";
				}else{
					echo "<title>Extrusion Parameters - Create</title>";
				}

			}

		?>
	</head>
	<body>

		<form method='post' action='process_new_extrusion_parameters.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if( $_SESSION['parameters'] == false) 
				{
					$_SESSION['ERRMSG_ARR'] ='Access denied!';
					session_write_close();
					header("Location:comsys.php");
					exit();
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $ExtrusionParametersID ? "Update " : "Create " );?> Extrusion Parameters </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form"> 
					<tr>
						<td> Product Type: </td>
						<td> <b> <?php echo ( $FGLocal == 0 ? 'Export' : 'Local' ); ?> </b> </td>
					</tr>
					<tr>
						<td> Product Category: </td>
						<td> <b> <?php echo ( $FGSoft == 0 ? 'Rigid' : 'Soft PVC' ); ?> </b> </td>
					</tr>
					<tr>
						<td> Product Item: </td>
						<td> <b> <?php echo $FGName; ?> </b> </td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th> Line </th>
						<th> Zone 1 </th>
						<th> Zone 2 </th>
						<th> Zone 3 </th>
						<th> Zone 4 </th>
						<th> Zone 5 </th>
						<th> Zone 6 </th>
						<th> Zone 7 </th>
						<th> Die Head A </th>
						<th> Die Head B </th>
					</tr>
					<?php
						foreach ($extrusion_id as $i => $ExtrusionID) {
					?>
							<tr>
								<td> 
									<?php echo $machine_name[$i]; ?>
									<input type='hidden' name='txtMachineName[]' value='<?php echo $machine_name[$i];?>'>
									<input type='hidden' name='hidExtrusionParametersItemID[]' value='<?php echo $ExtrusionParametersItemID[$i];?>'>
									<input type='hidden' name='txtExtrusionID[]' value='<?php echo $extrusion_id[$i];?>'>
								</td>
								<?php
									if ( $FGSoft == 0 && $extrusion_id[$i] != 1 ){
								?>
										<td>
											<input type='text' class="short_text_2" name='txtZone1[]' value='<?php echo $zone_1[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone2[]' value='<?php echo $zone_2[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone3[]' value='<?php echo $zone_3[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone4[]' value='<?php echo $zone_4[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone5[]' value='<?php echo $zone_5[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone6[]' value='<?php echo $zone_6[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone7[]' value='<?php echo $zone_7[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtDieHeadA[]' value='<?php echo $die_head_a[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtDieHeadB[]' value='<?php echo $die_head_b[$i];?>' readOnly>
										</td>

								<?php
									}elseif ( $FGSoft == 1 && $extrusion_id[$i] == 1 ){
								?>
										<td>
											<input type='text' class="short_text_2" name='txtZone1[]' value='<?php echo $zone_1[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone2[]' value='<?php echo $zone_2[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone3[]' value='<?php echo $zone_3[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone4[]' value='<?php echo $zone_4[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone5[]' value='<?php echo $zone_5[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone6[]' value='<?php echo $zone_6[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone7[]' value='<?php echo $zone_7[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtDieHeadA[]' value='<?php echo $die_head_a[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtDieHeadB[]' value='<?php echo $die_head_b[$i];?>' readOnly>
										</td>

								<?php
									}else{
								?>
										<td>
											<input type='text' class="short_text_2" name='txtZone1[]' value='<?php echo $zone_1[$i];?>'>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone2[]' value='<?php echo $zone_2[$i];?>'>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone3[]' value='<?php echo $zone_3[$i];?>'>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone4[]' value='<?php echo $zone_4[$i];?>'>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone5[]' value='<?php echo $zone_5[$i];?>'>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone6[]' value='<?php echo $zone_6[$i];?>'>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtZone7[]' value='<?php echo $zone_7[$i];?>'>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtDieHeadA[]' value='<?php echo $die_head_a[$i];?>'>
										</td>

										<td>
											<input type='text' class="short_text_2" name='txtDieHeadB[]' value='<?php echo $die_head_b[$i];?>'>
										</td>

								<?php
									}
								?>
							</tr>
					<?php
						}
					?>
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='pam_analysis.php?page=1&search=&qsone='">
							<input type='hidden' name='hidFinishedGoodID' value="<?php echo $fgid;?>">
							<input type='hidden' name='hidExtrusionParametersID' value="<?php echo ( $ExtrusionParametersID ? $ExtrusionParametersID : 0 );?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $ExtrusionParametersID ? $createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $ExtrusionParametersID ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>