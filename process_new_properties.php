<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_cpiar_verification.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidPropertyID = $_POST['hidPropertyID'];
		$sltPropertyType = $_POST['sltPropertyType'];
		$txtPropertyName = $_POST['txtPropertyName'];
		$sltUom = $_POST['sltUom'];
		$txtComments = $_POST['txtComments'];
		
		// $txtComments = str_replace('\\r\\n', '<br>', $txtComments);
		
		// $txtComments = str_replace('\\', '', $txtComments);

############# Input Validation
		if ( !$sltPropertyType ){
			$errmsg_arr[] = '* Select asset type.';
			$errflag = true;
		}
		if ( $txtPropertyName == '' ){
			$errmsg_arr[] = '* Asset name cannot be blank.';
			$errflag = true;
		}
		if ( !$sltUom ){
			$errmsg_arr[] = '* Select unit of measures.';
			$errflag = true;
		}

		if(isset($_POST['chkCmpds']))
			$chkCmpds = 1;
		else
			$chkCmpds = 0;
		if(isset($_POST['chkPips']))
			$chkPips = 1;
		else
			$chkPips = 0;
		if(isset($_POST['chkCorp']))
			$chkCorp = 1;
		else
			$chkCorp = 0;
		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0; 
		if(isset($_POST['chkPPR']))
			$chkPPR = 1;
		else
			$chkPPR = 0;                             

	######### Validation on Checkbox
		if($hidPropertyID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidPropertyID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation

	############# SESSION, keeping last input value
		$_SESSION['sltPropertyType'] = $sltPropertyType;
		$_SESSION['txtPropertyName'] = htmlspecialchars($txtPropertyName);
		$_SESSION['sltUom'] = $sltUom;
		$_SESSION['chkCmpds'] = $chkCmpds;
		$_SESSION['chkPips'] = $chkPips;
		$_SESSION['chkCorp'] = $chkCorp;
		$_SESSION['chkActive'] = $chkActive;
		$_SESSION['txtComments'] = htmlspecialchars($txtComments);

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_properties.php?id=$hidPropertyID");
			exit();
		}
############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_Company_Properties_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'iisiiiiisssiii', $hidPropertyID, $sltPropertyType, $txtPropertyName, $sltUom
												, $chkCmpds, $chkPips, $chkCorp, $chkActive, $txtComments
												, $createdAt, $updatedAt, $createdId, $updatedId, $chkPPR);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_cpiar_verification.php'.'</td><td>'.$processError.' near line 116.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidPropertyID)
				$_SESSION['SUCCESS']  = 'Successfully updated asset.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added asset.';
			//echo $_SESSION['SUCCESS'];
			header("location:property_transfer.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>