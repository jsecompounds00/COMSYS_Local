<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_supply.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$hidSupplyId = $_POST['hidSupplyId'];
		$supplyTypeId = $_POST['sltSupplyType'];
		$txtSupply = $_POST['txtSupply'];
		$txtDescription = $_POST['txtDescription'];
		$comments = $_POST['txtComments'];
		$sltUom = $_POST['sltUom'];
		$sltPipeLine = implode(',' , $_POST['sltPipeLine']);
		$sltMiscellaneous = $_POST['sltMiscellaneous'];
		//echo $sltPipeLine;

		if(isset($_POST['chkCmpds']))
			$chkCmpds = 1;
		else
			$chkCmpds = 0;
		if(isset($_POST['chkPips']))
			$chkPips = 1;
		else
			$chkPips = 0;
		if(isset($_POST['chkCorp']))
			$chkCorp = 1;
		else
			$chkCorp = 0;
		if(isset($_POST['chkPPR']))
			$chkPPR = 1;
		else
			$chkPPR = 0;
		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;

		$_SESSION['SESS_SUP_SupplyTypeId'] = $supplyTypeId;
		$_SESSION['SESS_SUP_Supply'] = $txtSupply;
		$_SESSION['SESS_SUP_Description'] = $txtDescription;
		$_SESSION['SESS_SUP_Comments'] = $comments;
		$_SESSION['SESS_SUP_Uom'] = $sltUom;
		$_SESSION['SESS_SUP_Cmpds'] = $chkCmpds;
		$_SESSION['SESS_SUP_Pips'] = $chkPips;
		$_SESSION['SESS_SUP_Corp'] = $chkCorp;
		$_SESSION['SESS_SUP_PPR'] = $chkPPR;
		$_SESSION['SESS_SUP_Active'] = $chkActive;
		
		if($hidSupplyId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidSupplyId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		################ INPUT VALIDATION
		if ( empty($supplyTypeId) ){
			$errmsg_arr[] = '* Supply type is missing.';
			$errflag = true;
		}
		if ( empty($txtSupply) ){
			$errmsg_arr[] = '* Supply name is missing.';
			$errflag = true;
		}
		if ( is_numeric($txtSupply) ){
			$errmsg_arr[] = '* Invalid supply name.';
			$errflag = true;
		}
		if ( empty($sltUom) ){
			$errmsg_arr[] = '* UOM is missing.';
			$errflag = true;
		}

		if ($chkPips == 0){
			if ( $sltPipeLine != 0 ){
				$errmsg_arr[] = '* Machine line applicable for Pipes only.';
				$errflag = true;
			}
		}

		if ( !$sltMiscellaneous ){
			$sltMiscellaneous = NULL;
		}

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_SUPPLY.$hidSupplyId);
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Supplies_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'iisssiiiiisssiiii', $hidSupplyId, $supplyTypeId, $txtSupply
												  , $txtDescription, $comments, $sltUom, $chkActive
												  , $chkCmpds, $chkPips, $chkCorp, $sltPipeLine, $createdAt
												  , $updatedAt, $createdId, $updatedId, $chkPPR, $sltMiscellaneous);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_supply.php'.'</td><td>'.$processError.' near line 118.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidSupplyId)
				$_SESSION['SUCCESS']  = 'Successfully updated supply.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new supply.';
			//echo $_SESSION['SUCCESS'];
			header("location: supplies.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close();
		require("include/database_close.php");
				
	}

?>