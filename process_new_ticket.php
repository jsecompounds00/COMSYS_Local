<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_ticket.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidTicketID = clean($_POST['hidTicketID']);
		$sltAttentionTo = clean($_POST['sltAttentionTo']);
		$txtRequestDate = clean($_POST['txtRequestDate']);
		$txtSubject = clean($_POST['txtSubject']);
		$txtRequest = clean($_POST['txtRequest']);

		$_SESSION['sltAttentionTo'] = $sltAttentionTo;
		$_SESSION['txtRequestDate'] = $txtRequestDate;
		$_SESSION['txtSubject'] = $txtSubject;
		$_SESSION['txtRequest'] = $txtRequest;

		//Input Validations
		if ( !($sltAttentionTo) ){
			$errmsg_arr[] = '* Select recipient of request (Attention To).';
			$errflag = true;
		}
		if ( $txtRequest == '' ){
			$errmsg_arr[] = "* Request can't be blank.";
			$errflag = true;
		}

		$txtRequest = str_replace('\\r\\n', '<br>', $txtRequest);
		$txtSubject = str_replace('\\r\\n', '<br>', $txtSubject);
		
		$txtRequest = str_replace('\\', '', $txtRequest);
		$txtSubject = str_replace('\\', '', $txtSubject);


		if($hidTicketID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];

		if($hidTicketID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];

		$updatedAt = date('Y-m-d H:i:s');
		$updatedId = $_SESSION['SESS_USER_ID'];

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_ticket.php?id=$hidTicketID");
			exit();
		}

		require ("process_send_email.php");

		if(!$mail->send()) {
		    error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_ticket.php'.'</td><td>'.$mail->ErrorInfo.' near line 93.</td></tr>', 3, "errors.php");
			$errmsg_arr[] = '* Message could not be sent. Please check your internet connection.';
		    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_ticket.php?id=$hidTicketID");
			exit();
		} else {
			$qry = mysqli_prepare($db, "CALL sp_Ticket_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?)");
			mysqli_stmt_bind_param($qry, 'iisssssii', $hidTicketID, $sltAttentionTo, $txtRequestDate, $txtSubject, $txtRequest, $createdAt, $updatedAt, $createdId, $updatedId);
			$qry->execute();
			$result = mysqli_stmt_get_result($qry); //return results of query
			$processError = mysqli_error($db);

			if(!empty($processError))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_ticket.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{ 
				if($hidTicketID)
					$_SESSION['SUCCESS']  = 'Successfully updated ticket.';
				else
					$_SESSION['SUCCESS']  = 'Successfully added new ticket.';
				//echo $_SESSION['SUCCESS'];
				header("location:ticketing_system.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
			}
			unset($_SESSION['page']);
			unset($_SESSION['search']);
			unset($_SESSION['qsone']);

			$db->next_result();
			$result->close(); 
		}
		require("include/database_close.php");
	}
?>
	