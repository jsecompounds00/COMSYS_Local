<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_role.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidRoleid = $_POST['hidRoleid'];
		$txtName = $_POST['txtName'];
		$sltDept1 = $_POST['sltDept1'];
		$sltDept2 = $_POST['sltDept2'];

		$txtPermID = implode(',' , $_POST['chkPermission']);

		$_SESSION['txtName'] = $txtName;

		if ( empty($txtName) ){
			$errmsg_arr[] = "* Role can't be blank.";
			$errflag = true;
		}
		if (is_numeric($txtName)){
			$errmsg_arr[] = "* Role can't be numeric.";
			$errflag = true;
		}
		if (!($sltDept1)){
			$errmsg_arr[] = "* Select department.";
			$errflag = true;
		}

		if ( isset($_POST['chkCompounds']))
			$chkCompounds = 1;
		else
			$chkCompounds = 0;
		if ( isset($_POST['chkPipes']))
			$chkPipes = 1;
		else
			$chkPipes = 0;
		if ( isset($_POST['chkCorporate']))
			$chkCorporate = 1;
		else
			$chkCorporate = 0;
		if ( isset($_POST['chkPPR']))
			$chkPPR = 1;
		else
			$chkPPR = 0;


		$_SESSION['SESS_RLE_Name'] = $txtName;
		$_SESSION['SESS_RLE_Compounds'] = $chkCompounds;
		$_SESSION['SESS_RLE_Pipes'] = $chkPipes;
		$_SESSION['SESS_RLE_Corporate'] = $chkCorporate;
		$_SESSION['SESS_RLE_PPR'] = $chkPPR;

		//If there are input validations, redirect back to the login form

		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_ROLE.$hidRoleid);
			exit();
		}

		if($hidRoleid == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidRoleid == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		$qry = mysqli_prepare($db, "CALL sp_Role_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'issssiiiiiiii', $hidRoleid, $txtName, $txtPermID, $createdAt, $updatedAt, $createdId, $updatedId
												  , $chkCompounds, $chkPipes, $chkCorporate, $chkPPR, $sltDept1, $sltDept2);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_role.php'.'</td><td>'.$processError.' near line 72.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$qryMR = "CALL migrate_Role_RoleHierarchy()";
			$resultMR = mysqli_query($db, $qryMR);

			if($hidRoleid)
				$_SESSION['SUCCESS']  = 'Successfully updated role.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new role.';
			//echo $_SESSION['SUCCESS'];
			header("location: role.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	