<?php
############# Start session
	session_start();
	ob_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_jrd_test_evaluation_noreplicate.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidNoReplicateJRDID = $_POST['hidNoReplicateJRDID'];	
		$hidProductforComparisonID = $_POST['hidProductforComparisonID'];	
		$hidProductforEvaluation = $_POST['hidProductforEvaluation'];
		$sltJRDNumber = $_POST['sltJRDNumber'];
		$txtTERNo = $_POST['txtTERNo'];
		$sltFormulaType = $_POST['sltFormulaType'];
		$txtRemarks = $_POST['txtRemarks'];	

		// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		
		// $txtRemarks = str_replace('\\', '', $txtRemarks);	

		$chkDynamicMilling = ( isset( $_POST['chkDynamicMilling'] ) ? 1 : 0 );
		$chkOilAging = ( isset( $_POST['chkOilAging'] ) ? 1 : 0 );
		$chkOvenAging = ( isset( $_POST['chkOvenAging'] ) ? 1 : 0 );
		$chkStrandInspect = ( isset( $_POST['chkStrandInspect'] ) ? 1 : 0 );
		$chkPelletInspect = ( isset( $_POST['chkPelletInspect'] ) ? 1 : 0 );
		$chkImpactTest = ( isset( $_POST['chkImpactTest'] ) ? 1 : 0 );
		$chkStaticHeating = ( isset( $_POST['chkStaticHeating'] ) ? 1 : 0 );
		$chkColorChange = ( isset( $_POST['chkColorChange'] ) ? 1 : 0 );
		$chkWaterImmersion = ( isset( $_POST['chkWaterImmersion'] ) ? 1 : 0 );
		$chkColdTesting = ( isset( $_POST['chkColdTesting'] ) ? 1 : 0 );

	############# SESSION, keeping last input value
		$_SESSION['sltJRDNumber'] = $sltJRDNumber;
		$_SESSION['txtTERNo'] = $txtTERNo;
		$_SESSION['sltFormulaType'] = $sltFormulaType;
		$_SESSION['chkDynamicMilling'] = $chkDynamicMilling;
		$_SESSION['chkOilAging'] = $chkOilAging;
		$_SESSION['chkOvenAging'] = $chkOvenAging;
		$_SESSION['chkStrandInspect'] = $chkStrandInspect;
		$_SESSION['chkPelletInspect'] = $chkPelletInspect;
		$_SESSION['chkImpactTest'] = $chkImpactTest;
		$_SESSION['chkStaticHeating'] = $chkStaticHeating;
		$_SESSION['chkColorChange'] = $chkColorChange;
		$_SESSION['chkWaterImmersion'] = $chkWaterImmersion;
		$_SESSION['txtRemarks'] = $txtRemarks;


	######### Input Validation
		if ( !$sltJRDNumber ){
			$errmsg_arr[] = "* Select JRD number.";
			$errflag = true;
		}
		if ( !$sltFormulaType ){
			$errmsg_arr[] = "* Select formula type.";
			$errflag = true;
		}


		if($hidNoReplicateJRDID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidNoReplicateJRDID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_jrd_test_evaluation_noreplicate.php");
			exit();
		}
############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_JRD_Tests_NoReplicate_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'iissiiiiiiiiiiisssiii', $hidNoReplicateJRDID, $sltJRDNumber, $txtTERNo, $hidProductforEvaluation, $hidProductforComparisonID
														   , $sltFormulaType, $chkDynamicMilling, $chkOilAging, $chkOvenAging, $chkStrandInspect, $chkPelletInspect
														   , $chkImpactTest, $chkStaticHeating, $chkColorChange, $chkWaterImmersion, $txtRemarks
														   , $createdAt, $updatedAt, $createdId, $updatedId, $chkColdTesting);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_jrd_test_evaluation_noreplicate.php'.'</td><td>'.$processError.' near line 117.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidNoReplicateJRDID)
				$_SESSION['SUCCESS']  = 'Successfully updated JRD tests.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added JRD tests.';
			//echo $_SESSION['SUCCESS'];
			if ( isset($_POST["btnSave"]) == 1 )
			{
				header("location:jrd.php?page=1&search=&qsone=");
			}
			elseif ( isset($_POST["btnSaveNAdd"]) == 1 )
			{
				$qryLastBatchTicketId = mysqli_query($db, "SELECT MAX(id) AS id FROM jrd_tests_noreplicate WHERE jrd_id = $sltJRDNumber");

				while($row = mysqli_fetch_assoc($qryLastBatchTicketId))
				{
					$JRDTestID = $row['id'];

				}
				header("location:option_for_evaluation.php?jrd_id=".$sltJRDNumber."&test_id=".$JRDTestID);
			}
		}	

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>