<html>
	<head>
		<title>O/C Budget Distribution - Home</title>
		<?php
			require("/include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : "");
			$page = ($_GET["page"] ? $_GET["page"] : 1);	
			$budgetID = $_GET["id"];	
		?>
	</head>
	<body>
		<?php
			require("/include/header.php");
			// require("/include/init_unset_values/budget_monitoring_oc_distribution_unset_value.php");

			// if( $_SESSION['oc_budget_distribution'] == false) 
			// {
			// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }

			$_SESSION['page_b'] = $_GET['page'];
			$_SESSION['search_b'] = $_GET['search'];
			$_SESSION['qsone_b'] = $_GET['qsone'];
			$_SESSION['id_b'] = $_GET['id'];
		?>

		<div class="wrapper">
			<?php
				$qryDH = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_OC_Distribution_Query( ? )" );
				mysqli_stmt_bind_param( $qryDH, "i", $budgetID);
				$qryDH->execute();
				$resultDH = mysqli_stmt_get_result($qryDH);
				while( $rowDH = mysqli_fetch_assoc( $resultDH ) ){
					$DHBudgetYear = $rowDH["DHBudgetYear"];
					$DHDepartment = $rowDH["DHDepartment"];
					$DHGLCode = $rowDH["DHGLCode"];
				}
				$db->next_result();
				$resultDH->close();
			
				$page_a = $_SESSION["page"];
				$search_a = htmlspecialchars($_SESSION["search"]);
				$qsone_a = htmlspecialchars($_SESSION["qsone"]);
				$search_a = ( (strpos(($search_a), "\\")+1) > 0 ? str_replace("\\", "", $search_a) : $search_a );
				$search_a = ( (strpos(($search_a), "'")+1) > 0 ? str_replace("'", "\'", $search_a) : $search_a );
				$qsone_a = ( (strpos(($qsone_a), "\\")+1) > 0 ? str_replace("\\", "", $qsone_a) : $qsone_a );
				$qsone_a = ( (strpos(($qsone_a), "'")+1) > 0 ? str_replace("'", "\'", $qsone_a) : $qsone_a );
			?>
			
			<span> <h3> <?php echo $DHGLCode;?> Budget for <?php echo $DHDepartment." - Y".$DHBudgetYear;?> </h3> </span>

			<a class='back' href='budget_monitoring_oc.php?page=<?php echo $page_a;?>&search=<?php echo $search_a;?>&qsone=<?php echo $qsone_a;?>'><img src='images/back.png' height="20" name='txtBack'> Home </a>
			
			<br><br><br>

			<div class="search_box">

	 			<form method="get" action="budget_monitoring_oc_distribution.php">
	 				<input type="hidden" name="qsone" value="<?php echo $qsone;?>">
	 				<input type="hidden" name="page" value="<?php echo $page;?>">
	 				<input type="hidden" name="id" value="<?php echo $budgetID;?>">
					<table class="search_tables_form">
						<tr>
							<td> Item: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
							<td> <input type="submit" value="Search"> </td>
						</tr>
					</table>

	 			</form>

	 		</div>

	 		<?php
				if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_oc_distribution.php"."</td><td>".$error." near line 27.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryBudget = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_OC_Distribution_Home( ?, ?, ?, NULL, NULL )" );
					mysqli_stmt_bind_param( $qryBudget, "isi", $budgetID, $search , $qsone);
					$qryBudget->execute();
					$resultBudget = mysqli_stmt_get_result($qryBudget);
					$total_results = mysqli_num_rows($resultBudget); //return number of rows of result

					$db->next_result();
					$resultBudget->close();

					$targetpage = "budget_monitoring_oc_distribution.php"; 	//your file name  (the name of this file)
					require("include/paginate_budget_distribution.php");

					$qryBudgetLimit = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_OC_Distribution_Home( ?, ?, ?, ?, ? )" );
					mysqli_stmt_bind_param( $qryBudgetLimit, "isiii", $budgetID, $search , $qsone, $start, $end );
					$qryBudgetLimit->execute();
					$resultBudgetLimit = mysqli_stmt_get_result($qryBudgetLimit);
					$processError3 = mysqli_error($db);	

					if(!empty($processError3))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_oc_distribution.php"."</td><td>".$processError3." near line 137.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
				?>
						<table class="home_pages">
							<tr>
								<td colspan="9">
									<?php echo $pagination; ?>
								</td>
							</tr>
							 <tr>
								<th> Item </th>
								<th> UOM </th>
								<th> Proposed (Qty) </th>
								<th> Actual (Qty) </th>
							</tr>
							<?php
								while ($row = mysqli_fetch_assoc($resultBudgetLimit)) {
									$DMiscellaneousTYpe = $row["DMiscellaneousTYpe"];
									$DMiscellaneousID = $row["DMiscellaneousID"];
									$DMiscellaneous = $row["DMiscellaneous"];
									$DUOM = $row["DUOM"];
									$DProposeQty = $row["DProposeQty"];
									$DActualQty = $row["DActualQty"];
							?>
									<tr>
										<td> <?php echo $DMiscellaneous;?> </td>
										<td> <?php echo $DUOM;?> </td>
										<td> <?php echo $DProposeQty;?> </td>
										<td> 
											<label class="<?php echo ( $DActualQty > $DProposeQty ? 'warning_background_stop' : ( $DActualQty == $DProposeQty ? "warning_background_slow" : "" ) );?>">
												<?php echo $DActualQty;?> 
											</label>
										</td>
									</tr>
							<?php
								}
								$db->next_result();
								$resultBudgetLimit->close();
							?>
							<tr>
								<td colspan="9">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
	