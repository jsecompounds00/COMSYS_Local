<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>add_property_inventory.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$addInventoryID = $_GET['id'];

				if($addInventoryID)
				{ 

					echo "<title>Inventory Transaction - Edit</title>";

					$qry = mysqli_prepare($db, "CALL sp_Property_Transfer_Add_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $addInventoryID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>add_property_inventory.php'.'</td><td>'.$processError.' near line 29.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
							$add_inventory_item_id = array();
							$property_type_id = array();
							$property_type = array();
							$company_property_id = array();
							$company_property_specification_id = array();
							$quantity_received = array();
							$unit_price = array();
							$remarks = array();
							$asset_condition = array();
							$reference_pts = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$add_inventory_id = $row['add_inventory_id'];
							$inventory_type = $row['inventory_type'];
							$inventory_source = $row['inventory_source'];
							$other_inventory_source = htmlspecialchars($row['other_inventory_source']);
							$reference_number = $row['reference_number'];
							$reference_date = $row['reference_date'];
							$received_type = $row['received_type'];
							$received_from_id = $row['received_from_id'];
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];

							$add_inventory_item_id[] = $row['add_inventory_item_id'];

							$property_type_id[] = $row['property_type_id'];

							$property_type[] = $row['property_type'];

							$company_property_id[] = $row['company_property_id'];

							$company_property_specification_id[] = $row['company_property_specification_id'];

							$quantity_received[] = $row['quantity_received'];

							$unit_price[] = $row['unit_price'];

							$remarks[] = htmlspecialchars($row['remarks']);

							$asset_condition[] = $row['asset_condition'];

							$reference_pts[] = htmlspecialchars($row['reference_pts']);
						}
						$ctr = count($add_inventory_item_id);
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.property_transfer_transactions";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>add_property_inventory.php'.'</td><td>'.$processErrorPI.' near line 88.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($addInventoryID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>add_property_inventory.php</td><td>The user tries to edit a non-existing add inventory transaction id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}elseif ( $inventory_type == 'sub' ){
						header("location: deduct_property_inventory.php?id=$addInventoryID");
					}

				}
				else{

					echo "<title>Inventory Transaction - New</title>";

				}
			}

			require("/include/header.php");
			require("/include/init_value.php");
		?>
		<script src="js/jscript.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<?php
		if ( $addInventoryID ){
	?>
			<body onload="showReceivedType(<?php echo $addInventoryID; ?>, '<?php echo $received_type; ?>'), enableRefRR()">
	<?php
		}else{
	?>
			<body onload="showReceivedType(0,'<?php echo $ReceivedType; ?>'), showReceivedFrom(0, 0, <?php echo $ReceivedFrom; ?>), enableRefRR()">
	<?php
		}
	?>

		<form method='post' action='process_add_property_inventory.php'>

			<?php
				if( $addInventoryID ){
					if( $_SESSION['property_trans_edit'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['property_trans_add'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $addInventoryID ? "Edit " : "New " ) ;?> Inventory Transaction </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Inventory Source:</td>
						<td>
							<select name='sltInventorySource' id='sltInventorySource' onchange="showReceivedType(0,'0'), showReceivedFrom(0,0,0), enableRefRR()">
								<option value='0'></option>
								<option value='TRF' <?php echo ( $addInventoryID ? ( $inventory_source == "TRF" ? "selected" : "" ) : ( $InventorySource == "TRF" ? "selected" : "" ) ); ?>>TRF</option>
								<option value='PRE' <?php echo ( $addInventoryID ? ( $inventory_source == "PRE" ? "selected" : "" ) : ( $InventorySource == "PRE" ? "selected" : "" ) ); ?>>PRE</option>
								<option value='ClientGift' <?php echo ( $addInventoryID ? ( $inventory_source == "ClientGift" ? "selected" : "" ) : ( $InventorySource == "ClientGift" ? "selected" : "" ) ); ?>>ClientGift</option>
								<option value='Others' <?php echo ( $addInventoryID ? ( $inventory_source == "Others" ? "selected" : "" ) : ( $InventorySource == "Others" ? "selected" : "" ) ); ?>>Others</option>
							</select>
						</td>
						<td>
							<input type="text" name="txtOtherSource" id="txtOtherSource" value="<?php echo ( $addInventoryID ? $other_inventory_source : "" ); ?>" readOnly>
							<label class="instruction">
		    				  	(Please specify other inventory source)
		    				</label>
						</td>
					</tr>	
					<tr>
						<td>Reference No.:</td>
						<td>
							<input type="text" name="txtReferenceNo" value="<?php echo ( $addInventoryID ? $reference_number : $ReferenceNo ); ?>">
						</td>
					</tr>
					<tr>
						<td>Reference Date:</td>
						<td>
							<input type="text" name="txtReferenceDate" id="txtReferenceDate" value="<?php echo ( $addInventoryID ? $reference_date : $ReferenceDate ); ?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtReferenceDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr>
						<td>Received Type:</td>
						<td>
							<select name='sltReceivedType' id='sltReceivedType' onchange='showReceivedFrom(this.value, 0,0)'>
							</select>
						</td>
					</tr>
					<tr>
						<td>Received From:</td>
						<td colspan='2'>
							<select name='sltReceivedFrom' id='sltReceivedFrom'>
								<?php
									if ( $addInventoryID ){
										$qry = mysqli_prepare($db, "CALL sp_Received_From_Dropdown(?)");
										mysqli_stmt_bind_param($qry, 's', $received_type);
										$qry->execute();
										$result = mysqli_stmt_get_result($qry);
										$processError = mysqli_error($db);

										if(!empty($processError))
										{
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>received_from_dropdown.php'.'</td><td>'.$processError.' near line 226.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{		
											echo "";
											echo "	<option value=0></option>";
											while($row = mysqli_fetch_assoc($result))
											{
												
												$IssuerID = $row['id'];
												$Issuer = $row['c_name'];

												if ( $received_from_id == $IssuerID ){
													echo "<option value=".$IssuerID." selected>".$Issuer."</option>";	
												}else{
													echo "<option value=".$IssuerID.">".$Issuer."</option>";
												}
												
											}
											$db->next_result();
											$result->close();
										}
									}
								?>
							</select>
						</td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th>Type</th>
						<th>Name</th>
						<th>Specification</th>
						<th>Condition</th>
						<th>Quantity Received</th>
						<th>Unit Price</th>
						<th>Remarks</th>
						<th>Reference PTS Number</th>
						<th>Reference RR Number</th>
					</tr>
					<?php 
						if ( $addInventoryID ){
							for ( $i = 0; $i < $ctr ; $i++ ){
					?>
								<tr>
									<input type='hidden' name='hidAddInventoryItemID[]' value="<?php echo $add_inventory_item_id[$i]; ?>">
									<td>
										<input type='hidden' name='sltAssetType[]' value="<?php echo $property_type_id[$i]; ?>">
								 		<?php echo $property_type[$i]; ?>
									</td>
									<td>
										<?php
											$qryA = mysqli_prepare($db, "CALL sp_Asset_Dropdown(?, 2)");
											mysqli_stmt_bind_param($qryA, 'i', $property_type_id[$i]);
											$qryA->execute();
											$resultA = mysqli_stmt_get_result($qryA); //return results of query
											$processErrorA = mysqli_error($db);

											if(!empty($processErrorA))
											{
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>add_property_inventory.php'.'</td><td>'.$processErrorA.' near line 288.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultA))
												{
													
													$PrpertyID = $row['id'];
													$PropertyName = $row['property_name'];

													if ( $company_property_id[$i] == $PrpertyID ){
														echo "<input type='hidden' name='sltAsset[]' value=".$company_property_id[$i].">";
														echo "$PropertyName";
													}
												}
											}
											$db->next_result();
											$resultA->close();
										?>
									</td>

									<td>
										<?php
											$qryAS = mysqli_prepare($db, "CALL sp_Asset_Specification_Dropdown(?, 'add', 1)");
											mysqli_stmt_bind_param($qryAS, 'i', $company_property_id[$i]);
											$qryAS->execute();
											$resultAS = mysqli_stmt_get_result($qryAS); //return results of query
											$processErrorAS = mysqli_error($db);

											if(!empty($processErrorAS))
											{
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>add_property_inventory.php'.'</td><td>'.$processErrorAS.' near line 320.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultAS))
												{
													
													$SpecificationID = $row['SpecificationID'];
													$Specifications = $row['Specifications'];

													if ( $company_property_specification_id[$i] == $SpecificationID ){
														echo "<input type='hidden' name='sltAssetSpecification[]' value=".$SpecificationID.">";
														echo "$Specifications";
													}
												}
											}
											$db->next_result();
											$resultAS->close();
										?>
									</td>

									<td>
										<input type='hidden' name='sltAssetCondition[]' value="<?php echo $asset_condition[$i]; ?>">
								 		<?php echo $asset_condition[$i]; ?>
									</td>

									<td> <input type='text' name='txtQuantityReceived[]' size="10" value="<?php echo $quantity_received[$i]; ?>"> </td>

									<td> <input type="text" name="txtUnitPrice[]" size="10" value="<?php echo $unit_price[$i]; ?>"> </td>

									<td> <input type="text" name="txtRemarks[]" value="<?php echo $remarks[$i]; ?>"> </td>

									<td> <input type="text" name="txtRefPTSNumber[]" size="10" value="<?php echo $reference_pts[$i]; ?>"> </td>

									<td> <input type="text" name="txtRefRRNumber[]" size="10" id="txtRefRRNumber<?php echo $i;?>" value="<?php echo $reference_pts[$i]; ?>" readOnly> </td>

								</tr>
					<?php
							}
						}else{
							for ( $i = 0; $i < 10; $i++ ){ 

					?>
								<tr>
									<input type='hidden' name='hidAddInventoryItemID'>
									<td>
										<select name='sltAssetType[]' id='sltAssetType<?php echo $i; ?>' onchange="showAsset2('0', '<?php echo $i?>'),showAssetSpecification(0, 0, <?php echo $i;?>)">
											<?php
												if($addInventoryID){
													$qryAT = "CALL sp_PropertyType_Dropdown(0, 2)";
												}else {
													$qryAT = "CALL sp_PropertyType_Dropdown(0, 1)";
												}
												$resultAT = mysqli_query($db, $qryAT);
												$processErrorAT = mysqli_error($db);

												if ( !empty($processErrorAT) ){
													error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>add_property_inventory.php'.'</td><td>'.$processErrorAT.' near line 378.</td></tr>', 3, "errors.php");
													header("location: error_message.html");
												}else{
													if ( !$addInventoryID ) echo "<option></option>";
													while($row = mysqli_fetch_assoc($resultAT)){
														$propertyTypeID = $row['property_type_id'];
														$propertyTypeDd = $row['property_type'];

														if ( $addInventoryID ){
															if ( $property_type_id == $propertyTypeID ){
																echo "<option value='".$propertyTypeID."' selected>".$propertyTypeDd."</option>";
															}
														}else{
															if ( $AssetType == $propertyTypeID ){
																echo "<option value='".$propertyTypeID."' selected>".$propertyTypeDd."</option>";
															}else{
																echo "<option value='".$propertyTypeID."' >".$propertyTypeDd."</option>";
															}
														}
													}
												}
												$db->next_result();
												$resultAT->close();
											?>
										</select>
									</td>

									<td>
										<select name='sltAsset[]' id='sltAsset<?php echo $i; ?>' onchange='showAssetSpecification(this.value, 0, <?php echo $i;?>)'>
											<option></option>
										</select>
									</td>

									<td>
										<select name='sltAssetSpecification[]' id='sltAssetSpecification<?php echo $i; ?>'>
											<option></option>
										</select>
									</td>

									<td>
										<select name='sltAssetCondition[]' id='sltAssetCondition<?php echo $i; ?>'>
											<option></option>
											<option value='Good'>Good</option>
											<option value='Damaged'>Damaged</option>
										</select>
									</td>

									<td>
										<input type="text" name="txtQuantityReceived[]" size="10" value="<?php echo $QuantityReceived[$i]; ?>">
									</td>

									<td>
										<input type="text" name="txtUnitPrice[]" size="10" value="<?php echo $UnitPrice[$i]; ?>">
									</td>

									<td>
										<input type="text" name="txtRemarks[]" value="<?php echo $Remarks1[$i]; ?>">
									</td>

									<td>
										<input type="text" name="txtRefPTSNumber[]" size="10" value="<?php echo $RefPTSNumber[$i]; ?>">
									</td>

									<td>
										<input type="text" name="txtRefRRNumber[]" size="10" id="txtRefRRNumber<?php echo $i; ?>" value="<?php echo $RefRRNumber[$i]; ?>" readOnly>
									</td>
								</tr>
					<?php
							} 
						}
					?>
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='property_transfer.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidAddInventoryID" id="hidAddInventoryID" value="<?php echo $addInventoryID; ?>">
							<input type="hidden" name="hidInventoryType" id="hidInventoryType" value="<?php echo ( $addInventoryID ? $inventory_type : "add" ); ?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $addInventoryID ? $created_at : date("Y-m-d H:i:s")); ?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $addInventoryID ? $created_id : "0" ); ?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>