<html>
	<head>
		<script src="js/jscript.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>deduct_property_inventory.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$subInventoryID = $_GET['id'];

				if($subInventoryID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Property_Transfer_Sub_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $subInventoryID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>deduct_property_inventory.php'.'</td><td>'.$processError.' near line 36.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
							$sub_inventory_item_id = array();
							$company_property_id = array();
							$company_property_specification_id = array();
							$date_received = array();
							$quantity_received = array();
							$unit_price = array();
							$remarks = array();
							$asset_condition = array();
							$property_type_id = array();
							$property_type = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$sub_inventory_id = $row['sub_inventory_id'];
							$inventory_type = $row['inventory_type'];
							$inventory_source = $row['inventory_source'];
							$reference_number = $row['reference_number'];
							$reference_date = $row['reference_date'];
							// $property_type_id = $row['property_type_id'];
							$received_from_id = $row['received_from_id'];
							$issued_to_division = $row['issued_to_division'];
							$issued_to_department_id = $row['issued_to_department_id'];
							$issued_to_user_id = $row['issued_to_user_id'];
							// $purpose = $row['purpose'];
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];

							$sub_inventory_item_id[] = $row['sub_inventory_item_id'];

							$company_property_id[] = $row['company_property_id'];

							$property_type_id[] = $row['property_type_id'];

							$property_type[] = $row['property_type'];

							$company_property_specification_id[] = $row['company_property_specification_id'];

							$quantity_received[] = $row['quantity_received'];

							$date_received[] = $row['date_received'];

							$unit_price[] = $row['unit_price'];

							$remarks[] = htmlspecialchars($row['remarks']);

							$asset_condition[] = $row['asset_condition'];
						}
						$ctr = count($sub_inventory_item_id);
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.property_transfer_transactions";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>deduct_property_inventory.php'.'</td><td>'.$processErrorPI.' near line 92.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($subInventoryID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>deduct_property_inventory.php</td><td>The user tries to edit a non-existing add inventory transaction id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}elseif ( $inventory_type == 'add' ){
						header("location: add_property_inventory.php?id=$subInventoryID");
					}

					echo "<title>Inventory Transaction - Edit</title>";
					
				}
				else{

					echo "<title>Inventory Transaction - New</title>";

				}
			}
			require("/include/header.php");
			require("/include/init_value.php");
		?>
	</head>
	<body onload="<?php echo ( $subInventoryID ? "disablePTSNo(),  showPTSReceiver($subInventoryID, $issued_to_user_id)" : "disablePTSNo()" );?>">

		<form method='post' action='process_deduct_property_inventory.php'>

			<?php
				if ( $subInventoryID ){
					if( $_SESSION['property_trans_edit'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['property_trans_sub'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $subInventoryID ? "Edit " : "New " ) ;?> Inventory Transaction </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form collapsed">
					<colgroup>
						<col width="180px"></col><col width="350px"></col><col width="150px"></col><col width="380px"></col>
					</colgroup>
					<tr>
						<td>Inventory Source:</td>

						<td>
							<select name='sltInventorySource' id='sltInventorySource' onchange='disablePTSNo()'>
								<option></option>

								<option value='PTS' <?php echo ( $subInventoryID ? ( $inventory_source == 'PTS' ? "selected" : "" ) : ( $InventorySource == 'PTS' ? "selected" : "" ) ); ?>>
									PTS
								</option>

								<option value='NonPTS'<?php echo ( $subInventoryID ? ( $inventory_source == 'NonPTS' ? "selected" : "" ) : ( $InventorySource == 'NonPTS' ? "selected" : "" ) ); ?>>
									Non PTS
								</option>
							</select>
						</td>
					</tr>	

					<tr class="spacing" valign="top">
						<td>PTS No.:</td>

						<td>
							<input type="text" name="txtPTSNo" id="txtPTSNo" value="<?php echo ( $subInventoryID ? $reference_number : $PTSNo );?>">
						</td>

						<td>Date:</td>

						<td>
							<input type="text" name="txtReferenceDate" id="txtReferenceDate" value="<?php echo ( $subInventoryID ? $reference_date : $ReferenceDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtReferenceDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
						<td class='border_top border_left'>From:</td>

						<td class='border_top border_right'><b>Corporate</b></td>

						<td class='border_top border_left'>To:</td>

						<td class='border_top border_right'>
							<input type='radio' name='radToDivision' id='Compounds' value='Compounds'
								onchange='showToDepartment(0,0)'
								<?php echo ( $subInventoryID ? ($issued_to_division == "Compounds" ? "checked" : "" ) : "" );?>>
								<label for='Compounds'>Compounds</label>

							<input type='radio' name='radToDivision' id='Pipes' value='Pipes' 
								onchange='showToDepartment(0,0)'
								<?php echo ( $subInventoryID ? ($issued_to_division == "Pipes" ? "checked" : "" ) : "" );?>>
								<label for='Pipes'>Pipes</label>

							<input type='radio' name='radToDivision' id='Corporate' value='Corporate' 
								onchange='showToDepartment(0,0)'
								<?php echo ( $subInventoryID ? ($issued_to_division == "Corporate" ? "checked" : "" ) : "" );?>>
								<label for='Corporate'>Corporate</label>

							<input type='radio' name='radToDivision' id='PPR' value='PPR' 
								onchange='showToDepartment(0,0)'
								<?php echo ( $subInventoryID ? ($issued_to_division == "PPR" ? "checked" : "" ) : "" );?>>
								<label for='PPR'>PPR</label>
						</td>
					</tr>

					<tr>
						<td class='border_left'>Department:</td>

						<td class='border_right'><b>Admin</b></td>

						<td class='border_left'>Department:</td>

						<td class='border_right'>
							<select name='sltToDepartment' id='sltToDepartment' onchange='showPTSReceiver(0, 0, 0)'>
								<option vlaue='0'></option>
								<?php
									if ( $subInventoryID ){
										$qryD = mysqli_prepare( $db, "CALL sp_Department_Dropdown_New(?, 1)");
										mysqli_stmt_bind_param( $qryD, 's', $issued_to_division );
										$qryD->execute();
										$resultD = mysqli_stmt_get_result($qryD);
							    		$processError2 = mysqli_error($db);

										if(!empty($processError2))
										{
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>deduct_property_inventory.php'.'</td><td>'.$processError2.' near line 260.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultD))
											{
												
												$deptID = $row['id'];
												$deptCode = $row['code'];

												if ( $subInventoryID ){
													if ( $issued_to_department_id == $deptID )
														echo "<option value=".$deptID." selected>".$deptCode."</option>";
													else
														echo "<option value=".$deptID.">".$deptCode."</option>";
												}
												else{
													if ( $ToDepartment == $deptID )
														echo "<option value=".$deptID." selected>".$deptCode."</option>";
													else
														echo "<option value=".$deptID.">".$deptCode."</option>";
												}
											}
											$db->next_result();
											$resultD->close();
										}
									}
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td class='border_left border_bottom'>Name:</td>

						<td class='border_right border_bottom'>
							<select name='sltFromUser'>
								<?php
									$qryRF = "CALL sp_User_Dropdown(1001, 0, 1, 'corporate')";
									$resultRF = mysqli_query($db, $qryRF);
									$processErrorRF = mysqli_error($db);

									if(!empty($processErrorRF))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>deduct_property_inventory.php'.'</td><td>'.$processErrorRF.' near line 303.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
											echo "<option></option>";
										while($row = mysqli_fetch_assoc($resultRF))
										{
											
											$IssuerID = $row['user_id'];
											$Issuer = $row['User'];

											if ( $subInventoryID ){
												if ( $received_from_id == $IssuerID )
													echo "<option value=".$IssuerID." selected>".$Issuer."</option>";
												else
													echo "<option value=".$IssuerID.">".$Issuer."</option>";
											}
											else{
												if ( $FromUser == $IssuerID )
													echo "<option value=".$IssuerID." selected>".$Issuer."</option>";
												else
													echo "<option value=".$IssuerID.">".$Issuer."</option>";
											}
											
										}
									}
									$db->next_result();
									$resultRF->close();
								?>
							</select>
						</td>

						<td class='border_left border_bottom'>Name:</td>

						<td class='border_right border_bottom'>
							<select name='sltToUser' id='sltToUser'>
							</select>
						</td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th>Type</th>
						<th>Name</th>
						<th>Specification</th>
						<th>Condition</th>
						<th>Issued Quantity</th>
						<th>Unit Price</th>
						<th colspan="2">Received Date</th>
						<th>Remarks</th>
					</tr>
					<?php 
						if ( $subInventoryID ){
							for ( $i = 0; $i < $ctr ; $i++ ){
					?>
								<tr>
									<input type='hidden' name='hidSubInventoryItemID[]' value="<?php echo $sub_inventory_item_id[$i];?>">
									<td>
										<input type='hidden' name='sltAssetType[]' value="<?php echo $property_type_id[$i];?>">
										<?php echo $property_type[$i];;?>
									</td>

									<td>
										<?php
											$qryA = mysqli_prepare($db, "CALL sp_Asset_Dropdown(?, 2)");
											mysqli_stmt_bind_param($qryA, 'i', $property_type_id);
											$qryA->execute();
											$resultA = mysqli_stmt_get_result($qryA); //return results of query
											$processErrorA = mysqli_error($db);

											if(!empty($processErrorA))
											{
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>deduct_property_inventory.php'.'</td><td>'.$processErrorA.' near line 417.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultA))
												{
													
													$PrpertyID = $row['id'];
													$PropertyName = $row['property_name'];

													if ( $company_property_id[$i] == $PrpertyID ){
														echo "<input type='hidden' name='sltAsset[]' value=".$company_property_id[$i].">";
														echo "$PropertyName";
													}
												}
											}
											$db->next_result();
											$resultA->close();
										?>
									</td>

									<td>
										<?php
											$qryAS = mysqli_prepare($db, "CALL sp_Asset_Specification_Dropdown(?, 'sub', 1)");
											mysqli_stmt_bind_param($qryAS, 'i', $company_property_id[$i]);
											$qryAS->execute();
											$resultAS = mysqli_stmt_get_result($qryAS); //return results of query
											$processErrorAS = mysqli_error($db);

											if(!empty($processErrorAS))
											{
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>deduct_property_inventory.php'.'</td><td>'.$processErrorAS.' near line 447.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultAS))
												{
													
													$SpecificationID = $row['SpecificationID'];
													$Specifications = $row['Specifications'];
													$Balance = $row['Balance'];

													if ( $company_property_specification_id[$i] == $SpecificationID ){
														echo "<input type='hidden' name='sltAssetSpecification[]' value=".$SpecificationID."-".$Balance.">";
														echo "$Specifications";
													}
												}
											}
											$db->next_result();
											$resultAS->close();
										?>
									</td>

									<td>
										<input type='hidden' name='sltAssetCondition[]' value="<?php echo $asset_condition[$i];?>">
										<?php echo $asset_condition[$i];?>
									</td>

									<td>
										<input type='text' name='txtQuantityReceived[]' value="<?php echo $quantity_received[$i];?>" size="10">
										<input type='hidden' name='hidQuantityReceived[]' value="<?php echo $quantity_received[$i];?>">
									</td>

									<td><input type='text' name='txtUnitPrice[]' value="<?php echo $unit_price[$i];?>" size="10"></td>

									<td>
										<input type='text' name='txtDateReceived[]' id='txtDateReceived<?php echo $i;?>' value="<?php echo $date_received[$i];?>">
									</td>
									
									<td>
										<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDateReceived<?php echo $i; ?>')" style="cursor:pointer" name="picker" />
									</td>

									<td><input type='text' name='txtRemarks[]' value="<?php echo $remarks[$i];?>"></td>

								</tr>	
					<?php
							}
						}else{
							for ( $i = 0; $i < 10; $i++ ){ 
					?>
								<tr>
									<td>
										<select name='sltAssetType[]' id='sltAssetType<?php echo $i; ?>' onchange="showAsset2('0', '<?php echo $i?>'),showAssetSpecification(0, 0, <?php echo $i;?>)">
											<?php
												if($addInventoryID){
													$qryAT = "CALL sp_PropertyType_Dropdown(0, 2)";
												}else {
													$qryAT = "CALL sp_PropertyType_Dropdown(0, 1)";
												}

												$resultAT = mysqli_query($db, $qryAT);
												$processErrorAT = mysqli_error($db);
												if ( !empty($processErrorAT) ){
													error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>add_property_inventory.php'.'</td><td>'.$processErrorAT.' near line 253.</td></tr>', 3, "errors.php");
													header("location: error_message.html");
												}else{
													if ( !$addInventoryID ) echo "<option></option>";
													while($row = mysqli_fetch_assoc($resultAT)){
														$propertyTypeID = $row['property_type_id'];
														$propertyTypeDd = $row['property_type'];

														if ( $addInventoryID ){
															if ( $property_type_id == $propertyTypeID ){
																echo "<option value='".$propertyTypeID."' selected>".$propertyTypeDd."</option>";
															}
														}else{
															if ( $AssetType == $propertyTypeID ){
																echo "<option value='".$propertyTypeID."' selected>".$propertyTypeDd."</option>";
															}else{
																echo "<option value='".$propertyTypeID."' >".$propertyTypeDd."</option>";
															}
														}
													}
												}
												$db->next_result();
												$resultAT->close();
											?>
										</select>
									</td>

									<td>
										<input type='hidden' name='hidSubInventoryItemID'>

										<select name='sltAsset[]' id='sltAsset<?php echo $i; ?>' onchange='showAssetSpecification(this.value, 0, <?php echo $i;?>)'>
											<option></option>
										</select>
									</td>

									<td>
										<select name='sltAssetSpecification[]' id='sltAssetSpecification<?php echo $i; ?>'>
											<option></option>
										</select>
									</td>

									<td>
										<input type="hidden" name='sltAssetCondition[]' id="sltAssetCondition<?php echo $i; ?>" value="Good">
										Good
										<!-- <select name='sltAssetCondition[]' id='sltAssetCondition<?php //echo $i; ?>'>
											<option></option>
											<option value='Good'>Good</option>
											<option value='Damaged'>Damaged</option>
										</select> -->
									</td>

									<td>
										<input type="text" name="txtQuantityReceived[]" value="<?php echo $QuantityReceived[$i]; ?>" size="10">
										<input type="hidden" name="hidQuantityReceived[]" value="0">
									</td>

									<td>
										<input type="text" name="txtUnitPrice[]" value="<?php echo $UnitPrice[$i]; ?>" size="10">
									</td>

									<td>
										<input type="text" name="txtDateReceived[]" id="txtDateReceived<?php echo $i; ?>" value="<?php echo $QuantityIssued[$i]; ?>">
									</td>

									<td>
										<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDateReceived<?php echo $i; ?>')" style="cursor:pointer" name="picker" />
									</td>

									<td>
										<input type="text" name="txtRemarks[]" value="<?php echo $Remarks1[$i]; ?>">
									</td>
								</tr>
					<?php
							} 
						}
					?>
				</table>

				<table class="comments_buttons">
					<tr>
						<td >
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='property_transfer.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidSubInventoryID' value="<?php echo $subInventoryID;?>">
							<input type='hidden' name='hidInventoryType' id='hidInventoryType' value='<?php echo ( $subInventoryID ? $inventory_type : "sub" );?>'>
							<input type='hidden' name='hidCreatedAt' value="<?php echo ( $subInventoryID ? $created_at : date('Y-m-d H:i:s') );?>">
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $subInventoryID ? $created_id : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>