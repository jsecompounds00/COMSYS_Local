<html>
	<head>
		<?php
			require("include/database_connect.php");

			$name_text = ($_GET['name_text'] ? "%".$_GET['name_text']."%" : "");
			$code_text = ($_GET['code_text'] ? "%".$_GET['code_text']."%" : "");
			$type_text = ($_GET['type_text'] ? $_GET['type_text'] : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
		<title>Moisture Content - Home</title>
	</head>
	<body>

		<?php
			require '/include/header.php';
			require("/include/init_unset_values/moisture_content_init_value.php");
			
			if( $_SESSION['mc'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION['name_text'] = $_GET['name_text'];
			$_SESSION['code_text'] = $_GET['code_text'];
			$_SESSION['type_text'] = $_GET['type_text'];
			$_SESSION['page'] = $_GET['page'];
		?>

		<div class="wrapper">

			<span> <h3> Moisture Content </h3> </span>

			<div class="search_box">
				<form method="get" action="moisture_content.php">
				 		<input type="hidden" name="page" value="<?php echo $page;?>">
						
					<table class="search_tables_form">
						<tr>
							<td> Name: </td>
					 		<td> <input type="text" name="name_text" value="<?php echo $_GET["name_text"];?>"> </td>
							<td> Code: </td>
					 		<td> <input type="text" name="code_text" value="<?php echo $_GET["code_text"];?>"> </td>
							<td> Type: </td>
							<td>
						 		<select name="type_text">
						 			<?php
							 			$qryRMT = "CALL sp_RMType_Dropdown(1)"; 
							 			$resultRMT = mysqli_query($db, $qryRMT);
							 			$processError1 = mysqli_error($db);

							 			if(!empty($processError1))
							 			{
							 				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>moisture_content.php'.'</td><td>'.$processError1.' near line 43.</td></tr>', 3, "errors.php");
							 				header("location: error_message.html");
							 			}
							 			else
							 			{
							 				while($row = mysqli_fetch_assoc($resultRMT))
							 				{
							 					$rmTypeID = $row['id'];
							 					$rmType = $row['code'];

							 					if ($rmType == $type_text)
							 					{
							 						echo "<option value='".$rmTypeID."' selected>".$rmType."</option>";
							 					}
							 					else echo "<option value='".$rmTypeID."'>".$rmType."</option>";
							 				}
											
							 				$db->next_result();
							 				$resultRMT->close();
							 			}
							 		?>
						 		</select>
						 	</td>
					 		<td> <input type="submit" value="Search"> </td>
					 		<td>
						 		<?php
									if(array_search(125, $session_Permit)){
								?>
										<input type="button" value="Add %MC" onclick="location.href='new_moisture_content.php?id=0'">
								<?php
									 	$_SESSION['new_moisture_content'] = true;
									}else{
										unset($_SESSION['new_moisture_content']);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php	
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>moisture_content.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryR = mysqli_prepare($db, "CALL sp_Moisture_Content_Home(?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryR, 'sss', $name_text, $code_text, $type_text);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR); //return results of query

					$total_results = mysqli_num_rows($resultR); //return number of rows of result
					
					$db->next_result();
					$resultR->close();

					$targetpage = "moisture_content.php"; 	//your file name  (the name of this file)
					require("include/paginate_raw_material.php");

					$qry = mysqli_prepare($db, "CALL sp_Moisture_Content_Home(?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sssii', $name_text, $code_text, $type_text, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>moisture_content.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th> Transaction Date </th>
							    <th> MIRS Number </th>
							    <th> Raw Materials </th>
							    <th> Lot Number </th>
							    <th> %MC </th>
							    <th> Bulk Density </th>
							    <th> </th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)) {
									$MCID = $row['MCID'];
									$MIRSDate = $row['MIRSDate'];
									$RM = $row['RM'];
									$MIRSNo = $row['MIRSNo'];
									$LotNumbers = $row['LotNumbers'];
									$PercentMCs = $row['PercentMCs'];
									$BulkDensities = $row['BulkDensities'];
							?>
									<tr>
									   	<td> <?php echo $MIRSDate;?> </td>
									   	<td> <?php echo $MIRSNo;?> </td>
									   	<td> <?php echo $RM;?> </td>
									   	<td> <?php echo $LotNumbers;?> </td>
									   	<td> <?php echo $PercentMCs;?> </td>
									   	<td> <?php echo $BulkDensities;?> </td>
									   	<td>
									   		<?php
								  				if(array_search(124, $session_Permit)){
								  			?>
													<input type='button' value='Edit' onclick="location.href='new_moisture_content.php?id=<?php echo $MCID;?>'">
											<?php
													$_SESSION['edit_moisture_content'] = true;
												}else{
													unset($_SESSION['edit_moisture_content']);
												}
											?>
										</td>
									</tr>
							<?php
								}
							?>
							<tr>
								<td colspan='6'>
									<?php echo $pagination; ?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
			
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
