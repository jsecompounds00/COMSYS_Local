<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_update_schedule.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$hidEquipmentId = $_POST['hidEquipmentId'];
########## Input Validations

	foreach ( $_POST['hidScheduleId'] as $key => $hidScheduleId ) {
		$hidScheduleId = array();
		$hidScheduleId = $_POST['hidScheduleId'];
		$createdAt = array();
		$createdAt = $_POST['hidCreatedAt'];
		$createdId = array();
		$createdId = $_POST['hidCreatedId'];
	}
	foreach ( $_POST['hidScheduleType'] as $hidScheduleType ) {
		$hidScheduleType = array();
		$hidScheduleType = $_POST['hidScheduleType'];
	}
	foreach ( $_POST['hidSchedule'] as $hidSchedule ) {
		$hidSchedule = array();
		$hidSchedule = $_POST['hidSchedule'];
	}
	foreach ( $_POST['txtDateConduct'] as $txtDateConduct ) {
		$txtDateConduct = array();
		$txtDateConduct = $_POST['txtDateConduct'];
	}
	foreach ( $_POST['txtConductBy'] as $txtConductBy ) {
		$txtConductBy = array();
		$txtConductBy = $_POST['txtConductBy'];
	}
	foreach ( $_POST['txtRemarks'] as $txtRemarks ) {
		$txtRemarks = array();
		$txtRemarks = $_POST['txtRemarks'];
	}
	foreach ( $_POST['chkStatus'] as $chkStatus ) {
		$chkStatus = array();
		$chkStatus = $_POST['chkStatus'];
	}

		$i=0;

		do{

			if ( isset($chkStatus[$i]) ) {
				$valDateConduct = validateDate($txtDateConduct[$i], 'Y-m-d');
				if ( $valDateConduct != 1 ){
					$errmsg_arr[] = "* Invalid date conducted.";
					$errflag = true;
				}
				if ( $txtConductBy[$i] == '' || is_numeric( $txtConductBy[$i] ) ){
					$errmsg_arr[] = '* Invalid conducted by.';
					$errflag = true;
				}
				if ( $hidScheduleType[$i] == 'Calibration' ){
					$calibration_date[$i] = $txtDateConduct[$i];
				}else{
					$calibration_date[$i] = NULL;
				}
				if ( $hidScheduleType[$i] == 'Validation' ){
					$validation_date[$i] = $txtDateConduct[$i];
				}else{
					$validation_date[$i] = NULL;
				}
				$updatedAt = date('Y-m-d H:i:s');
				$updatedId = $_SESSION['SESS_USER_ID'];
					// echo $hidScheduleId[$i];
					// echo '<br>';
					// echo $hidEquipmentId;
					// echo '<br>';
					// echo $hidScheduleType[$i];
					// echo '<br>';
					// echo $hidSchedule[$i];
					// echo '<br>';
					// echo $txtDateConduct[$i];
					// echo '<br>';
					// echo $txtConductBy[$i];
					// echo '<br>';
					// echo $chkStatus[$i];
					// echo '<br>';
					// echo $txtRemarks[$i];
					// echo '<br>';
					// echo $createdAt[$i];
					// echo '<br>';
					// echo $createdId[$i];
					// echo '<br>';
					// echo $updatedAt = date('Y-m-d H:i:s');
					// echo '<br>';
					// echo $updatedId = $_SESSION['SESS_USER_ID'];
					// echo '<br>';
					// echo '<br>';
			}

			$i++;
		}while ( $i < count( $chkStatus ) );

############# SESSION, keeping last input value
		// $_SESSION['sltSchedType'] = $sltSchedType;
		// $_SESSION['txtDate'] = $txtDate;

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:schedule_history.php?id=".$hidEquipmentId."&page=1&search=&qsone=");
			exit();
		}
########## Committing to Database

		foreach($_POST['chkStatus'] as $key => $itemValue)
		{	

			if ($itemValue)
			{
				$qry = mysqli_prepare($db, "CALL sp_Equipment_Update_Schedule_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
				mysqli_stmt_bind_param($qry, 'iisssssisssii', $hidScheduleId[$key], $hidEquipmentId, $hidScheduleType[$key],
															$hidSchedule[$key], $calibration_date[$key], $validation_date[$key],
															$txtConductBy[$key], $chkStatus[$key], $txtRemarks[$key], $createdAt[$key], 
															$updatedAt, $createdId[$key], $updatedId);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry); //return results of query
				$processError1 = mysqli_error($db);

				if ( !empty($processError1) ){
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_update_schedule.php'.'</td><td>'.$processError1.' near line 146.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}else{
					if ( $hidScheduleId )
						$_SESSION['SUCCESS']  = 'Schedule successfully updated.';
					else
						$_SESSION['SUCCESS']  = 'Schedule successfully added.';
					header("location: schedule_history.php?id=".$hidEquipmentId."&page=1&search=&qsone=");
				}				
			}
		}

		// $db->next_result();
		// $result->close(); 
		require("include/database_close.php");

	}
?>