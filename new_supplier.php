<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_supplier.php"."</td><td>".$error." near line 9.</td></tr></tbody>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$supplierId = $_GET["id"];

				if($supplierId)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Supplier_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $supplierId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_supplier.php"."</td><td>".$processError." near line 34.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$name = htmlspecialchars($row["name"]);
							$active = $row["active"];
							$rm = $row["rm"];
							$fg = $row["fg"];
							$supplies = $row["supplies"];
							$db_compounds = $row["compounds"];
							$db_pipes = $row["pipes"];
							$db_ppr = $row["ppr"];
							$db_corporate = $row["corporate"];
							$createdAt = $row["created_at"];
							$createdId = $row["created_id"];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.suppliers";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_supplier.php"."</td><td>".$processErrorPI." near line 59.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
						$db->next_result();
						$resultPI->close();
					}

		############ .............
					if( !in_array($supplierId, $id, TRUE) ){
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_supplier.php</td><td>The user tries to edit a non-existing supplier_id.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					
					echo "<title>Supplier - Edit</title>";
				}
				else
				{
					echo "<title>Supplier - Add</title>";
				}
			}
		?>
	</head>
	<body>
		<form method="post" action="process_new_supplier.php">
			<?php
				require("/include/header.php");
				require("/include/init_unset_values/supplier_init_value.php");

				if ( $supplierId ){
					if( $_SESSION["edit_supplier"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{	
					if( $_SESSION["add_supplier"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}				
				}
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $supplierId ? "Edit ".$name : "New Supplier" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {
						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);
					}
				?>

				<table class="parent_tables_form">
					<tr>
					    <td>Supplier Name:</td>
					    <td>
					    	<input type="text" name="txtSupplier" size="40" value="<?php echo ( $supplierId ? $name : $initSPRSupplier ); ?>" <?php echo ( $supplierId ? ( $supplierId < 10000 ? "readOnly" : "" ) : "" );?>>
					    </td>
					</tr>
					<tr>
						<td>Division:</td>
						<td> 
							<input type="checkbox" name="chkCmpds" id="Cmpds" value="Cmpds" <?php echo ( $supplierId ? ( $db_compounds ? "checked" : "" ) : ( $initSPRCmpds ? "checked" : "" ));?>>
								<label for="Cmpds"> Compounds</label>

							<input type="checkbox" name="chkPips" id="Pips" value="Pips" <?php echo ( $supplierId ? ( $db_pipes ? "checked" : "" ) :  ( $initSPRPips ? "checked" : "" ) );?>>
								<label for="Pips"> Pipes</label>

							<input type="checkbox" name="chkCorp" id="Corp" value="Corp" <?php echo ( $supplierId ? ( $db_corporate ? "checked" : "" ) :  ( $initSPRCorp ? "checked" : "" ) );?>>
								<label for="Corp"> Corporate</label>

							<input type="checkbox" name="chkPPR" id="PPR" value="PPR" <?php echo ( $supplierId ? ( $db_ppr ? "checked" : "" ) :  ( $initSPRPPR ? "checked" : "" ));?>>
								<label for="PPR"> PPR</label>
						</td>
					</tr>
					<tr>
						<td>Applicable To:</td>
						<td>
							<input type="checkbox" name="chkRm" id="chkRm" <?php echo ( $supplierId ? ( $rm ? "checked" : "" ) : ( $initSPRRm ? "checked" : "" ) );?>>
								<label for="chkRm"> RM</label>

							<input type="checkbox" name="chkFg" id="chkFg" <?php echo ( $supplierId ? ( $fg ? "checked" : "" ) : ( $initSPRFg ? "checked" : "" ) );?>>
								<label for="chkFg"> FG</label>

							<input type="checkbox" name="chkSupplies" id="chkSupplies" <?php echo ( $supplierId ? ( $supplies ? "checked" : "" ) : ( $initSPRSupplies ? "checked" : "" ) );?>>
								<label for="chkSupplies"> SUPPLIES</label>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type="checkbox" name="chkActive" <?php echo ( $supplierId ? ( $active ? "checked" : "" ) : ( $initSPRActive ? "checked" : "" ) );?>>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveDept" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='supplier.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidSupplierid" value="<?php echo $supplierId;?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $supplierId ? $createdAt : date("Y-m-d H:i:s") );?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $supplierId ? $createdId : 0 );?>">
						</td>
					</tr>
				</table>

			</div>
		</form>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>