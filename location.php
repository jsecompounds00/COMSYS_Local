<html>
	<head>
		<title>Location - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = "";
		?>
	</head>
	<body>

		<?php
			require ('/include/header.php');

			if( $_SESSION["location"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Location </h3> </span>

			<div class='search_box'>
		 		<form method='get' action='location.php'>
		 			<input type='hidden' name='page' value="<?php echo $page;?>">
		 			<input type='hidden' name='qsone' value="<?php echo $qsone;?>">
		 			<table class="search_table">
		 				<tr>
		 					<td> Location: </td>
		 					<td> <input type='text' name='search' value="<?php echo $_GET['search'];?>"> </td>
		 					<td> <input type='submit' value='Search'> </td>
		 					<td> <input type='button' name='btnAddLocation' value='Add Location' onclick="location.href='<?php echo PG_NEW_LOCATION;?>0'"> </td>
		 				</tr>
		 			</table>
		 		</form>
		 	 </div>

		 	 <?php
	 	 		if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>location.php'.'</td><td>'.$error.' near line 27.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryL = mysqli_prepare($db, "CALL sp_Location_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryL, 's', $search);
					$qryL->execute();
					$resultL = mysqli_stmt_get_result($qryL);
					$total_results = mysqli_num_rows($resultL); //return number of rows of result

					$db->next_result();
					$resultL->close();

					$targetpage = "location.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Location_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>location.php'.'</td><td>'.$processError.' near line 48.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>

						<table class="home_pages">
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>Location</th>
							    <th>Comments</th>
							    <th>Active?</th>
							    <th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)) { 
							?>
								<tr>
									<td><?php echo $row['name']; ?></td>
									<td><?php echo $row['comments']; ?></td>
									<td><?php echo ($row['active'] ? 'Y': 'N'); ?></td>
									<td><input type='button' name='btnLoc' value='Edit' onclick="location.href='<?php echo PG_NEW_LOCATION.$row['id'];?>'"></td>
								</tr>
							<?php 
								} 
							?>
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
