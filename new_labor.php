<!-- <head>
</head>
<?php 



?>
	
	
	
</form>
	<div>
		<?php	
	// 	require('/include/footer.php'); 
	// require("include/database_close.php");?>
	</div>
</div>
</body> -->





<html>
	<head>
		<script src="js/jscript.js"></script>  
		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_labor.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$laborID = $_GET['id'];

				if($laborID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Labor_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $laborID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_labor.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$del_date = $row['del_date'];
							$cims_dsq_id = $row['cims_dsq_id'];
							$class = $row['class'];
							$regular_hrs_driver = $row['regular_hrs_driver'];
							$ot_driver = $row['ot_driver'];
							$regular_hrs_helper_1 = $row['regular_hrs_helper_1'];
							$ot_helper_1 = $row['ot_helper_1'];
							$regular_hrs_helper_2 = $row['regular_hrs_helper_2'];
							$ot_helper_2 = $row['ot_helper_2'];
							$remarks = $row['remarks'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();
					$vehicle_class = $cims_dsq_id."-".$class;

		############ .............
					$qryPI = "SELECT id from comsys.labor";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_labor.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($laborID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_labor.php</td><td>The user tries to edit a non-existing labor_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					
					echo "<title>Labor - Edit</title>";
				}
				else
				{
					echo "<title>Labor - Add</title>";
				}
			}

			require("/include/header.php");
			require("/include/init_value.php");
			
			
		?>
	</head>
	<body onload="<?php echo ( $laborID ? "showDSQa($cims_dsq_id), showPersonnel()" : "showDSQa(0)" )?>">

		<form method='post' action='process_new_labor.php'>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $laborID ? "Edit " : "New " );?> Labor Computation <?php echo ( $laborID ? "Dated ".$del_date : "" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Date:</td>
						<td>
							<input type='text' name='txtDate' id='txtDate' 
								value='<?php echo ( $laborID ? $del_date : $Date );?>' 
								onchange='showDSQa(<?php echo ( $laborID ? $cims_dsq_id : 0 );?>)' 
								<?php echo ( $laborID ? "readOnly" : "" );?>>
								<?php 
									if(!$laborID){
								?>		<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDate')" style="cursor:pointer" name="picker" />&nbsp;&nbsp;
								<?php
									}
								?>
						</td>
					</tr>
					<tr>
						<td>Delivery Schedule:</td>
						<td>
							<?php
								if ( $laborID ){
									$qryDSQA = mysqli_prepare( $db, "CALL sp_DSQ_a_Dropdown(?, 1)" );
									mysqli_stmt_bind_param( $qryDSQA, 's', $del_date );
									$qryDSQA->execute();
									$resultDSQA = mysqli_stmt_get_result( $qryDSQA );
									$processError = mysqli_error($db);
								
									if ($processError){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dsq_dropdown.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{	
										while($row = mysqli_fetch_assoc($resultDSQA))
										{
											$dsq_id = $row['DSQId'];
											$control_no = $row['ControlNumber'];
											$delivery_date = $row['DeliveryDate'];
											$delivery_time = $row['DeliveryTime'];
											$truck_no = $row['Trucknumber'];
											$class = $row['Class'];

											if ( $cims_dsq_id == $dsq_id ){
												echo "<input type='text' value='".$control_no." : ".$truck_no." : ".$delivery_time."' readOnly>";
												echo "<input type='hidden' name='sltDSQ' id='sltDSQ' value='".$dsq_id."-".$class."'>";
											}
										}
										$db->next_result();
										$resultDSQA->close();
									}	
								}else{
							?>
									<select name='sltDSQ' id='sltDSQ' onchange='showPersonnel()'>
									</select>
							<?php 
								} 
							?>
						</td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th></th>
						<th>Driver</th>
						<th>Delivery Helper 1</th>
						<th>Delivery Helper 2</th>
					</tr>
					<tr id='hidPersonnel'>
						<td>Personnel: </td>
					</tr>
					<tr>
						<td>No. of Regular Hrs.:</td>
						<td>
							<input type='text' name='txtDriverRegular' value='<?php echo ( $laborID ? $regular_hrs_driver : "" );?>'>
						</td>
						<td>
							<input type='text' name='txtHelperRegular1' value='<?php echo ( $laborID ? $regular_hrs_helper_1 : "" );?>'>
						</td>
						<td>
							<input type='text' name='txtHelperRegular2' value='<?php echo ( $laborID ? $regular_hrs_helper_2 : "" );?>'>
						</td>
					</tr>
					<tr>
						<td>Overtime:</td>
						<td>
							<input type='text' name='txtDriverOT' value='<?php echo ( $laborID ? $ot_driver : "" );?>'>
						</td>
						<td>
							<input type='text' name='txtHelperOT1' value='<?php echo ( $laborID ? $ot_helper_1 : "" );?>'>
						</td>
						<td>
							<input type='text' name='txtHelperOT2' value='<?php echo ( $laborID ? $ot_helper_2 : "" );?>'>
						</td>
					</tr>
				</table>

				<table class="comments_buttons">
					<tr>
						<td valign='top'>Remarks:</td>
						<td>
							<textarea name='txtRemarks'><?php
								if ( $laborID ){
									$remarks_array = explode("<br>", $remarks);

									foreach ($remarks_array as $remarks_key => $remarks_value) {
										echo $remarks_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" name="btnSaveLabor" value="Save">
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='labor.php?page=1&search=&qsone='">
							<input type='hidden' name='hidLaborID' value="<?php echo $laborID;?>">
							<input type='hidden' name='hidCreatedAt' value="<?php echo ( $laborID ? $createdAt : date('Y-m-d H:i:s') );?>">
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $laborID ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
