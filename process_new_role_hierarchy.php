<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_role_hierarchy.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$hidHierarchyID = $_POST['hidHierarchyID'];
		$txtJobLevel = $_POST['txtJobLevel'];
		$sltSuperior = $_POST['sltSuperior'];
		$hidJobTitle = $_POST['hidJobTitle'];

		$_SESSION['SESS_ROH_JobLevel'] = $txtJobLevel;
		$_SESSION['SESS_ROH_Superior'] = $sltSuperior;
		
		$updatedAt = date('Y-m-d H:i:s');
		$updatedId = $_SESSION['SESS_USER_ID'];

		################ INPUT VALIDATION
		if ( $txtJobLevel != "" && !is_numeric($txtJobLevel) ){
			$errmsg_arr[] = '* Invalid format of job level.';
			$errflag = true;
		}
		if ( !$sltSuperior ){
			$errmsg_arr[] = '* Select superior..';
			$errflag = true;
		}

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_role_hierarchy.php?id=".$hidHierarchyID);
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Role_Hierarchy_CRU(?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'iiisi', $hidHierarchyID, $txtJobLevel, $sltSuperior, $updatedAt, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_role_hierarchy.php'.'</td><td>'.$processError.' near line 118.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS']  = "Successfully added superior for ".$hidJobTitle.".";
			header("location: role_hierarchy.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close();
		require("include/database_close.php");
				
	}

?>