<?php
############# Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }

############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_credit_memo.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitize the POST values
		$hidMemoId = $_POST['hidMemoId'];
		$sltCustomer = $_POST['sltCustomer'];
		$sltInvoiceNum = $_POST['sltInvoiceNum'];
		$txtMemoDate = $_POST['txtMemoDate'];
		$txtReferenceNum = $_POST['txtReferenceNum'];
		$txtAmount = $_POST['txtAmount'];
		$hidJacketType = $_POST['hidJacketType'];

############# Input Validations

		$InvoiceNum = substr($sltInvoiceNum, 0, strpos($sltInvoiceNum, '-'));
		$Balance = substr($sltInvoiceNum, (strpos($sltInvoiceNum, '-')+1));

		if ( !$sltCustomer ){
			$errmsg_arr[] = "* Select customer.";
			$errflag = true;
		}
		if ( !$sltInvoiceNum ){
			$errmsg_arr[] = "* Select invoice number.";
			$errflag = true;
		}
		$valMemoDate = validateDate($txtMemoDate, 'Y-m-d');
		if ( $valMemoDate != 1 ){
			$errmsg_arr[] = '* Invalid reference date.';
			$errflag = true;
		}
		if ( $txtReferenceNum == ''){
			$errmsg_arr[] = "* Reference number can't be blank.";
			$errflag = true;
		}
		if ( $txtAmount == ''){
			$errmsg_arr[] = "* Amount can't be blank.";
			$errflag = true;
		}
		if ( !is_numeric($txtAmount) ){
			$errmsg_arr[] = "* Amount must be a number.";
			$errflag = true;
		}

		if ( $txtAmount > $Balance ){
			$errmsg_arr[] = "* Amount is greater than the balance of selected invoice.";
			$errflag = true;
		}

		if($hidMemoId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidMemoId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		
		$_SESSION['sltCustomer'] = $sltCustomer;
		$_SESSION['txtInvoiceNo'] = $InvoiceNum;
		$_SESSION['txtMemoDate'] = htmlspecialchars($txtMemoDate);
		$_SESSION['txtReferenceNo'] = htmlspecialchars($txtReferenceNum);
		$_SESSION['txtAmount'] = htmlspecialchars($txtAmount);

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			if ( $hidMemoId ){
				header("Location: credit_memo.php?id=".$hidMemoId."&type=".$hidJacketType);
			}else{
				header("Location: credit_memo.php?id=".$hidMemoId);
			}
			exit();
		}

############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_Credit_Memo_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'iisssdssii', $hidMemoId, $sltCustomer, $InvoiceNum
										  , $txtMemoDate, $txtReferenceNum, $txtAmount
										  , $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_credit_memo.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidMemoId)
				$_SESSION['SUCCESS']  = 'Successfully updated credit memo.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new credit memo.';
			//echo $_SESSION['SUCCESS'];
			header("location: customer_jacket.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	