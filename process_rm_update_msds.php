<?php
	session_start();
	// if( $_SESSION['cancel_pre'] == false) 
	// {
	// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
	// 	session_write_close();
	// 	header("Location:comsys.php");
	// 	exit();
	// }	

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require("include/database_connect.php");

	$id = intval($_GET['id']);
	
	$qry = mysqli_prepare($db, "CALL sp_RM_Update_MSDS(?)");
	mysqli_stmt_bind_param($qry, 'i', $id);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_rm_update_msds.php'.'</td><td>'.$processError.' near line 28.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS'] = 'Raw material was successfully updated.';
			header("location:raw_materials.php?page=".$_SESSION['page']."&name_text=".$_SESSION['name_text']."&code_text=".$_SESSION['code_text']."&type_text=".$_SESSION['type_text']);
		}
			require("include/database_close.php");
?>