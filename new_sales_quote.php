<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_quote.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$quoteID = $_GET['id'];

				if($quoteID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Quote_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $quoteID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_quote.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while ( $row = mysqli_fetch_assoc($result) ){
							$db_month = date('m', strtotime($row["quote_date"]));
							$db_year = date('Y', strtotime($row["quote_date"]));
							$db_customer_id = $row["customer_id"];
							$db_customer_name = $row["customer_name"];
							$CreatedAt = $row["created_at"];
							$CreatedID = $row["created_id"];
						}
					}
					$db->next_result();
					$result->close();

					// $count = count($sfai_id);

		############ .............
					$qryPI = "SELECT id from comsys.quotes";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_quote.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($quoteID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_quote.php</td><td>The user tries to edit a non-existing quote_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Sales Quote - Edit</title>";
				}
				else{

					echo "<title>Sales Quote - Add</title>";

					$month = date('m');
					$year = date('Y');

				}
			}
		?>
		<script src="js/sales_quote_js.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body onload='showFGSalesQuote()'>

		<form method='post' action='process_new_sales_quote.php'>

			<?php
				require("/include/header.php");
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $quoteID ? "Edit " : "New " );?> Sales Quote </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td> Quote Year :</td>
						<td>
							<select name='sltYear' id='sltYear' onchange="showFGSalesQuote()">
								<?php
									$qryC = "CALL sp_Year_Dropdown()";
									$resultC = mysqli_query($db, $qryC);
									$processErrorC = mysqli_error($db);

									if ( !empty($processErrorC) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_quote.php'.'</td><td>'.$processErrorPI.' near line 157.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										while($row = mysqli_fetch_assoc($resultC)){
											if ( $quoteID ){
												if ( $db_year == $row['year'] ){
													echo "<option value='".$row['year']."' selected>".$row['year']."</option>";
												}
											}
											else{
												if ( $year == $row['year'] ){
													echo "<option value='".$row['year']."' selected>".$row['year']."</option>";
												}else{
													echo "<option value='".$row['year']."'>".$row['year']."</option>";
												}
											}
										}
										$db->next_result();
										$resultC->close();
									}
								?>
							</select>

							<select name='sltMonth' id='sltMonth' onchange="showFGSalesQuote()">
								<?php
									$qryC = "CALL sp_Month_Dropdown()";
									$resultC = mysqli_query($db, $qryC);
									$processErrorC = mysqli_error($db);

									if ( !empty($processErrorC) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_quote.php'.'</td><td>'.$processErrorPI.' near line 157.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										while($row = mysqli_fetch_assoc($resultC)){
											if ( $quoteID ){
												if ( $db_month == $row['month_id'] ){
													echo "<option value='".$row['month_id']."' selected>".$row['month_id']."</option>";
												}
											}
											else{
												if ( $month == $row['month_id'] ){
													echo "<option value='".$row['month_id']."' selected>".$row['month_id']."</option>";
												}else{
													echo "<option value='".$row['month_id']."'>".$row['month_id']."</option>";
												}
											}
										}
										$db->next_result();
										$resultC->close();
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Customer :</td>
						<td>
							<?php
								if ( $quoteID ){
									echo '<b>'.$db_customer_name.'</b>';
							?>		<input type='hidden' name='sltCustomer' id='sltCustomer' value='<?php echo $db_customer_id; ?>'>
							<?php
								}else{
							?>		
									<select name='sltCustomer' id='sltCustomer' onchange="showFGSalesQuote()">
										<option></option>
										<?php
											$qryC = "CALL sp_Customer_Dropdown()";
											$resultC = mysqli_query($db, $qryC);
											$processErrorC = mysqli_error($db);

											if ( !empty($processErrorC) ){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_quote.php'.'</td><td>'.$processErrorPI.' near line 157.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}else{
												while($row = mysqli_fetch_assoc($resultC)){
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
												$db->next_result();
												$resultC->close();
											}
										?>
									</select>
							<?php
								}
							?>
						</td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th>FG Item</th>
						<th>Last Month <br> Cost</th>
						<th>Current Month <br> Cost</th>
						<th>Cost Difference</th>
						<th>Last Month <br> Quote</th>
						<th>Quote Difference</th>
						<th>Current Month <br> Quote</th>
						<th>Quote Difference <br> (Revised-2nd Discount)</th>
						<th>Current Month <br> Quote (Revised)</th>
					</tr>
					<tbody id="tbQuote"></tbody>
				</table>

				<table class="comments_buttons">
				<tr>
					<td>
						<?php
							$page = $_SESSION["page"];
							$search = htmlspecialchars($_SESSION["search"]);
							$qsone = htmlspecialchars($_SESSION["qsone"]);
							$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
							$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
							$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
							$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
						?>
						<input type="submit" name="btnSave" value="Save">	
						<input type='button' name='btnCancel' value='Cancel' onclick="location.href='sales_quote.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
						<input type='hidden' name='hidQuoteID' id='hidQuoteID' value="<?php echo $quoteID;?>">
						<input type='hidden' name='hidCreatedAt' value='<?php echo ( $quoteID ? $CreatedAt : date('Y-m-d H:i:s') );?>'>
						<input type='hidden' name='hidCreatedId' value='<?php echo ( $quoteID ? $CreatedID : $_SESSION["SESS_USER_ID"] );?>'>
					</td>
				</tr>
			</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>