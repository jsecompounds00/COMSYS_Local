<html>
	<head>
		<title>Toll Matrix - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : "");
		?>
	</head>
	<body>

		<?php
			require '/include/header.php';

			if( $_SESSION['toll_matrix'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Toll Matrix </h3> </span>
			
			<div class='search_box'>
		 		<form method='get' action='toll_matrix.php'>
		 			<input type='hidden' name='page' value="<?php echo $page;?>">
		 			<input type='hidden' name='qsone' value="<?php echo $qsone;?>">
		 			<table class="search_tables_form">
		 				<tr>
		 					<td> Point: </td>
		 					<td> <input type='text' name='search' value="<?php echo $_GET['search'];?>"> </td>
		 					<td> <input type='submit' value='Search'> </td>
		 					<td>
		 						<?php
			 						if(array_search(55, $session_Permit)){
			 					?>
										<input type='button' name='btnAddTollMatrix' value='Add Toll Matrix' onclick="location.href='new_toll_matrix.php?id=0'">
								<?php
										$_SESSION['add_matrix'] = true;
									}else{
										unset($_SESSION['add_matrix']);
									}	
								?>
		 					</td>
		 				</tr>
		 			</table>
		 		</form>
		 	</div>

		 	<?php	
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>toll_matrix.php'.'</td><td>'.$error.' near line 27.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryTM = "CALL sp_TollMatrix_Home('$search', NULL, NULL)"; //query all rows
					$resultTM = mysqli_query($db, $qryTM); //return results of query
					$total_results = mysqli_num_rows($resultTM); //return number of rows of result

					$db->next_result();
					$resultTM->close();

					$targetpage = "toll_matrix.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = "CALL sp_TollMatrix_Home('$search', '$start', '$end')";
					$result = mysqli_query($db, $qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>toll_matrix.php'.'</td><td>'.$processError.' near line 48.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='7'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
								<th>Area</th>
								<th>Vehicle Class</th>
								<th>Entry Point</th>
								<th>Exit Point</th>
								<th>Toll</th>
								<th>Active?</th>
								<th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)) { 
							?>
								<tr>
									<td> <?php echo $row['area']; ?> </td>
									<td> <?php echo 'Class '.$row['vehicle_class']; ?> </td>
									<td> <?php echo $row['entry_point']; ?> </td>
									<td> <?php echo $row['exit_point']; ?> </td>
									<td> <?php echo $row['toll_fee']; ?> </td>
									<td> <?php echo ( $row['active'] ? 'Y' : 'N' ); ?> </td>
									<td>
										<?php
											if(array_search(56, $session_Permit)){
										?>
												<input type='button' name='btnTP' value='Edit' onclick="location.href='new_toll_matrix.php?id=<?php echo $row['id'];?>'">
										<?php
												$_SESSION['edit_matrix'] = true;
											}else{
												unset($_SESSION['edit_matrix']);
											}
										?>
									</td>
								</tr>
							<?php 
								} 
							?>
							<tr>
								<td colspan='7'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>