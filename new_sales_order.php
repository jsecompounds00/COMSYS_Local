<!--<?php //if ( !$salesID && $Tag == 0 ) { ?>
	<body onload="showFGItemType(''), showFGSales1('FG', <?php //echo $Customer; ?>)">
	<?php //}if ( !$salesID && $Tag == 1 ) { ?>
	<body onload="showFGItemType(''), showFGSales1('FG', <?php //echo $Customer; ?>), showExtraItems(1)">
	<?php //} ?> -->




<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_order.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$salesID = $_GET['id'];

				if($salesID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Sales_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $salesID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_order.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						$soi_id = array();
						$item_id = array();
						$item_type = array();
						$quantity = array();
						$unit_price = array();
						$required_delivery_date = array();

						$flag_close_so_item = array();
						$closed_item_at = array();
						$closed_item_by = array();
						$closed_item_quantity = array();
						$closed_item_reason = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$so_type = $row['so_type'];
							$customer_id = $row['customer_id'];
							$so_number = $row['so_number'];
							$so_date = $row['so_date'];
							$po_number = htmlspecialchars($row['po_number']);
							$po_date = $row['po_date'];
							$remarks = htmlspecialchars($row['remarks']);
							// $remarks_array = explode('<br>', $remarks);
							$credit_terms_id = $row['credit_terms_id'];
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];

							$soi_id[] = $row['soi_id'];

							$item_id[] = $row['item_id'];

							$item_type[] = $row['item_type'];

							$quantity[] = $row['quantity'];

							$unit_price[] = $row['unit_price'];

							$required_delivery_date[] = htmlspecialchars($row['required_delivery_date']);


							$flag_close_so_item[] = $row['flag_close_so_item'];

							$closed_item_at[] = $row['closed_item_at'];

							$closed_item_by[] = $row['closed_item_by'];

							$closed_item_quantity[] = $row['closed_item_quantity'];

							$closed_item_reason[] = htmlspecialchars($row['closed_item_reason']);

						}
						$db->next_result();
						$result->close();
					}

		############ .............
					$qryPI = "SELECT id from comsys.sales_order";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_order.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($salesID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_order.php</td><td>The user tries to edit a non-existing sales_order_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Sales Order - Edit</title>";
					
				}
				else{

					echo "<title>Sales Order - Add</title>";

				}
			}
		?>
		<script src="js/jscript.js"></script>  
		<script src="js/sales_order_js.js"></script>  
		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>

		<form method="post" action="process_new_sales_order.php">

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if ( $salesID ){
					if( $_SESSION["edit_sales_order"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION["add_sales_order"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $salesID ? "Edit Sales Order No. ".$so_type." ".$so_number : "New Sales Order" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<colgroup>
						<col width="140px"></col><col width="350px"></col><col width="140px"></col><col width="350px"></col>
					</colgroup>

					<tr>
						<td>Sales Type:</td>

						<td>
							<select name="sltSOType" id="sltSOType">
								<option></option>
								<option value="COML" <?php echo( $salesID ? ( $so_type == "COML" ? "selected" : "" ) : ( $SOType == "COML" ? "selected" : "" ) );?>>Local</option>
								<option value="EX" <?php echo( $salesID ? ( $so_type == "EX" ? "selected" : "" ) : ( $SOType == "EX" ? "selected" : "" ) );?>>Export</option>
							</select>
						</td>

						<td>Customer:</td>

						<td>
							<!-- <select name="sltCustomer" id="sltCustomer" onchange="showFGItemType("), showFGSales1("FG", 0), showLCNumber()"> -->
							<select name="sltCustomer" id="sltCustomer" <?php echo ( $salesID ? "" : "onchange=\"showFGItemType(''), showFGSales1('FG', '0'), showLCNumber()\"" );?>>
								<?php
									$qryC = "CALL sp_Customer_Dropdown()";
									$resultC = mysqli_query($db, $qryC);
									$processErrorC = mysqli_error($db);

									if ($processErrorC){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_order.php'.'</td><td>'.$processErrorC.' near line 25.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										echo "<option value='0'></option>";

										while($row = mysqli_fetch_assoc($resultC)){
											if ( $salesID ){
												if ( $customer_id == $row['id'] ){
													echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
												}
											}else{
												if ( $Customer == $row['id'] ){
													echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
												}else{
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
											}
										}
										$db->next_result();
										$resultC->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td>SO Number:</td>

						<td>
							<input type="text" name="txtSONumber" value="<?php echo ( $salesID ? $so_number : $SONumber );?>">
						</td>

						<td>SO Date:</td>

						<td>
							<input type="text" name="txtSODate" id="txtSODate" value="<?php echo ( $salesID ? $so_date : $SODate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtSODate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr>
						<td>PO Number:</td>

						<td>
							<input type="text" name="txtPONumber" value="<?php echo ( $salesID ? $po_number : $PONumber );?>">
						</td>

						<td>PO Date:</td>

						<td>
							<input type="text" name="txtPODate" id="txtPODate" value="<?php echo ( $salesID ? $po_date : $PODate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtPODate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
						<td>LC Number:</td>
						<td>
							<select name='sltLCNumber' id='sltLCNumber'>
								<?php
									if ( $salesID ){
										$qry = mysqli_prepare($db, "CALL sp_Credit_Terms_Dropdown(?)");
										mysqli_stmt_bind_param($qry, 'i', $customer_id);
										$qry->execute();
										$result = mysqli_stmt_get_result($qry);
										$processError = mysqli_error($db);
									
										if ($processError){
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>lc_number_dropdown.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											echo "	<option value='0'></option>";
											while($row = mysqli_fetch_assoc($result))
											{
												$id = $row['id'];
												$lc_number = $row['lc_number'];
												
												if ($id == $credit_terms_id){
													echo "<option value=".$id." selected>".$lc_number."</option>";
												}else {
													echo "<option value=".$id.">".$lc_number."</option>";
												}
											}

											$db->next_result();
											$result->close();
										}
									}
								?>
							</select>
						</td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th>Item Type</th>
						<th>Product</th>
						<th>Quantity (kgs)</th>
						<th>Unit Price</th>
						<th colspan="2">Require Delivery Date</th>
						<?php
							if ( $salesID ){
						?>
								<th> Close SO Item</th>
								<th> Qty to be Closed </th>
								<th> Reason </th>
						<?php
							}
						?>
					</tr>

					<tr>
					<?php
						if ( $salesID ){
							$count = count($soi_id);

							for ( $i = 0; $i < $count; $i++ ) { 
					?>
								<tr>
									<input type="hidden" name="hidSalesItemId[]" value="<?php echo $soi_id[$i]; ?>">

									<td>
										<select name="sltItemType[]" id="sltItemType<?php echo $i; ?>">
											<?php 
												if ( $item_type[$i] == 'FG' )
													echo "<option value='FG' selected>Finished Goods</option>";
												elseif ( $item_type[$i] == 'TG' )
													echo "<option value='TG' selected>Trading Goods</option>";
												elseif ( $item_type[$i] == 'Supplies' )
													echo "<option value='Supplies' selected>Supplies</option>";
											?>
										</select>
									</td>

									<td>
										<select name='sltProduct[]' id="sltProduct<?php echo $i; ?>" >
											<?php
												$qryS = mysqli_prepare($db, "CALL sp_Sales_FG_Dropdown(?, ?)");
												mysqli_stmt_bind_param($qryS, 'si', $item_type[$i], $customer_id);
												$qryS->execute();
												$resultS = mysqli_stmt_get_result($qryS); //return results of query
												$processError = mysqli_error($db);

												if(!empty($processErrorS))
												{
													error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_order.php'.'</td><td>'.$processErrorS.' near line 22.</td></tr>', 3, "errors.php");
													header("location: error_message.html");
												}
												else
												{
													while($row = mysqli_fetch_assoc($resultS))
													{
														
														$FgId = $row['FGID'];
														$Fg = $row['FGItem'];

														if ($FgId == $item_id[$i]){
															echo "<option value='".$FgId."' selected>".$Fg."</option>";
														}

													}
													$db->next_result();
													$resultS->close();
												}
											?>
										</select>
									</td>

									<td>
										<input type="text" name="txtQuantity[]" size="10" value="<?php echo $quantity[$i];?>">
									</td>

									<td>
										<input type="text" name="txtUnitPrice[]" size="10" value="<?php echo $unit_price[$i];?>"> 
									</td>

									<td>
										<input type="text" name="txtRequiredDate[]" size="10" id="txtRequiredDate<?php echo $i;?>" value="<?php echo $required_delivery_date[$i];?>"> 
									</td>
									<td>
										<img src="js/cal.gif" onclick="javascript:NewCssCal('txtRequiredDate<?php echo $i;?>')" style="cursor:pointer" name="picker" />
									</td>

									<td>
										<input type="checkbox" name="chkCloseSOItem[<?php echo $i;?>]" id="chkCloseSOItem<?php echo $i;?>" value="1" 
											onchange="enableCloseSOQty(<?php echo $i;?>)" 
											<?php echo ( $flag_close_so_item[$i] ? "checked" : "" );?>> 
									</td>

									<td>
										<input type="text" name="txtCloseQuantity[]" size="10" id="txtCloseQuantity<?php echo $i;?>" 
											value="<?php echo $closed_item_quantity[$i];?>"
											<?php echo ( $flag_close_so_item[$i] ? "" : "readOnly" );?>>
									</td>

									<td>
										<input type="text" name="txtCloseReason[]" size="15" id="txtCloseReason<?php echo $i;?>" 
											value="<?php echo $closed_item_reason[$i];?>"
											<?php echo ( $flag_close_so_item[$i] ? "" : "readOnly" );?>>
									</td>

									<input type="hidden" name="hidClosedBy[]" value="<?php echo date('Y-m-d H:i:s', strtotime('+6 hours'));?>">
									<input type="hidden" name="hidClosedAt[]" value="<?php echo $_SESSION["SESS_USER_ID"];?>">
								</tr>
					<?php			
							}
						}else{
							for ( $i = 0; $i < 15; $i++ ) { 
					?>
								<tr>
									<input type='hidden' name='hidSalesItemId[]'>	

									<td>
										<select name='sltItemType[]' id='sltItemType<?php echo $i; ?>' onchange="showFGSales(this.value, '0', '<?php echo $i; ?>')">
											<option value='FG'>Finished Goods</option>
											<option value='TG'>Trading Goods</option>
											<option value='Supplies'>Supplies</option>
										</select>
									</td>

									<td id='tdProduct<?php echo $i; ?>'>
										<select name='sltProduct[]' id='sltProduct<?php echo $i; ?>'>
										</select>
									</td>

									<td>
										<input type="text" name="txtQuantity[]" value="<?php echo $Quantity[$i];?>">
									</td>

									<td>
										<input type="text" name="txtUnitPrice[]" value="<?php echo $UnitPrice[$i];?>"> 
									</td>

									<td>
										<input type="text" name="txtRequiredDate[]" id="txtRequiredDate<?php echo $i;?>" value="<?php echo $RequiredDate[$i];?>"> 
									</td>
									<td>
										<img src="js/cal.gif" onclick="javascript:NewCssCal('txtRequiredDate<?php echo $i;?>')" style="cursor:pointer" name="picker" />
									</td>
								</tr>
					<?php 
							} 
						}
					?>
					</tr>

					<?php
						if ( !$salesID ){
					?>
							<tbody id='extraItem'>
								<tr class="align_bottom">
									<td>
										<input type='button' value='+' onclick='showExtraItems(1)'>
									</td>
								</tr>
							</tbody>
					<?php
						}
					?>
				</table>

				<table class="comments_buttons">
					<tr valign="top">
						<td>Remarks:</td>
						<td>
							<textarea name='txtRemarks'><?php
								if ( $salesID ){
									echo $remarks;
								}else{
									echo $Remarks;
								}
							?></textarea>
						</td>
					</tr>

					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$so_no = htmlspecialchars($_SESSION["so_no"]);
								$so_date = htmlspecialchars($_SESSION["so_date"]);
								$so_type = htmlspecialchars($_SESSION["so_type"]);
								$so_no = ( (strpos(($so_no), "\\")+1) > 0 ? str_replace("\\", "", $so_no) : $so_no );
								$so_no = ( (strpos(($so_no), "'")+1) > 0 ? str_replace("'", "\'", $so_no) : $so_no );
								$so_date = ( (strpos(($so_date), "\\")+1) > 0 ? str_replace("\\", "", $so_date) : $so_date );
								$so_date = ( (strpos(($so_date), "'")+1) > 0 ? str_replace("'", "\'", $so_date) : $so_date );
								$so_type = ( (strpos(($so_type), "\\")+1) > 0 ? str_replace("\\", "", $so_type) : $so_type );
								$so_type = ( (strpos(($so_type), "'")+1) > 0 ? str_replace("'", "\'", $so_type) : $so_type );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='sales_order.php?page=<?php echo $page;?>&so_no=<?php echo $so_no;?>&so_date=<?php echo $so_date;?>&so_type=<?php echo $so_type;?>'">
							<input type='hidden' name='hidSalesID' id='hidSalesID' value="<?php echo $salesID;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $salesID ? $created_at : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $salesID ? $created_id : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>