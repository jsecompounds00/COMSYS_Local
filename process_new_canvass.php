<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_canvass.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidCanvassID = $_POST['hidCanvassID'];
		$hidControlNumber = $_POST['hidControlNumber'];
		$txtDate = $_POST['txtDate'];
		$radDivision = $_POST['radDivision'];
		$txtRemarks = $_POST['txtRemarks'];
		$sltPRE = implode(',', $_POST['sltPRE']);

		// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);

		// $txtRemarks = str_replace('\\', '', $txtRemarks);

		$valDate = validateDate($txtDate, 'Y-m-d');

		if ( $txtDate != '' && $valDate != 1 ){
			$errmsg_arr[] = "* Invalid date.";
			$errflag = true;
		}

		if ( !isset($_POST['radDivision']) ){
			$errmsg_arr[] = '* Select division.';
			$errflag = true;
		}

		if ( strlen($sltPRE) < 1 ){
			$errmsg_arr[] = '* Choose at least one (1) PRE Number to continue.';
			$errflag = true;
		}else{

			if ( !isset( $_POST["chkInclude"] ) ){

				$errmsg_arr[] = "* This transaction cannot be saved. Select at least (1) request to canvass.";
				$errflag = true;

			}else{

				if ( !isset( $_POST["SupplierMarking"] ) ){

					$errmsg_arr[] = "* This transaction cannot be saved. Select or enter at least (1) one supplier";
					$errflag = true;

				}

			}

		}

		$hidCanvassSuppliersID = $_POST["hidCanvassSuppliersID"];
		$sltSupplier = $_POST["sltSupplier"];
		$chkNewSupplierMark = $_POST["chkNewSupplierMark"];
		$txtNewsupplier = $_POST["txtNewsupplier"];
		$txtDeliveryTerms = $_POST["txtDeliveryTerms"];
		$txtPaymentDays = $_POST["txtPaymentDays"];
		$txtPaymentTerms = $_POST["txtPaymentTerms"];
		$txtContactPerson = $_POST["txtContactPerson"];
		$txtTelephoneNum = $_POST["txtTelephoneNum"];
		$txtFaxNum = $_POST["txtFaxNum"];

		$hidQuantity = $_POST["hidQuantity"];
		$hidPREItemID = $_POST["hidPREItemID"];
		$hidItemID = $_POST["hidItemID"];
		$hidItemType = $_POST["hidItemType"];
		$hidLastTransInfo = $_POST["hidLastTransInfo"];

		foreach ($_POST["SupplierMarking"] as $x => $y) {

			foreach ($_POST["chkInclude"] as $key => $value) {

				$hidCanvassItemDetailsID = $_POST["hidCanvassItemDetailsID"];
				$txtCanvassDetails = $_POST["txtCanvassDetails"];
				$chkAvailability = $_POST["chkAvailability"];

				// $txtCanvassDetails = str_replace('\\r\\n', '<br>', $txtCanvassDetails);

				// $txtCanvassDetails = str_replace('\\', '', $txtCanvassDetails);

			}

			if ( isset($chkNewSupplierMark[$x]) && $txtNewsupplier[$x] == "" ){
				$errmsg_arr[] = "* New supplier can't be blank.";
				$errflag = true;
			}

			if ( !isset($chkNewSupplierMark[$x]) ){
				$txtNewsupplier[$x] = "";
			}

			if ( isset($chkNewSupplierMark[$x]) ){
				$sltSupplier[$x] = NULL;
			}

			if ( $sltSupplier[$x] || $txtNewsupplier[$x] != "" ){

				if ( !$txtDeliveryTerms[$x] ){
					$errmsg_arr[] = "* Select delivery terms for supplier (".($x+1).").";
					$errflag = true;
				}
				if ( !$txtPaymentTerms[$x] ){
					$errmsg_arr[] = "* Select payment terms for supplier (".($x+1).").";
					$errflag = true;
				}
				if ( $txtPaymentDays[$x] == "" ){
					$txtPaymentDays[$x] = NULL;
				}
				if ( $txtContactPerson[$x] == "" ){
					$errmsg_arr[] = "* Contact person can't be blank for supplier (".($x+1).").";
					$errflag = true;
				}
				if ( $txtTelephoneNum[$x] == "" && $txtFaxNum[$x] == "" ){
					$errmsg_arr[] = "* Telephone and fax number can't be both blank for supplier (".($x+1).").";
					$errflag = true;
				}

			}

		}
		
		######### Validation on Checkbox
		if($hidCanvassID == 0)
			$createdAt = date("Y-m-d H:i:s");
		else $createdAt = $_POST["hidCreatedAt"];
		$updatedAt = date("Y-m-d H:i:s");
		if($hidCanvassID == 0)
			$createdId = $_SESSION["SESS_USER_ID"];
		else $createdId = $_POST["hidCreatedId"];
		$updatedId = $_SESSION["SESS_USER_ID"];

		########## SESSION

		$_SESSION["SESS_CANVASS_Date"] = $txtDate;
		$_SESSION["SESS_CANVASS_Division"] = $radDivision;
		$_SESSION["SESS_CANVASS_Remarks"] = $txtRemarks;
		$_SESSION["SESS_CANVASS_PRE"] = $sltPRE;

		for ( $xx = 0; $xx < 4; $xx++ ){
			$_SESSION["SESS_CANVASS_Supplier"][$xx] = $sltSupplier[$xx];
			$_SESSION["SESS_CANVASS_Newsupplier"][$xx] = $txtNewsupplier[$xx];
			$_SESSION["SESS_CANVASS_DeliveryTerms"][$xx] = $txtDeliveryTerms[$xx];
			$_SESSION["SESS_CANVASS_PaymentDays"][$xx] = $txtPaymentDays[$xx];
			$_SESSION["SESS_CANVASS_PaymentTerms"][$xx] = $txtPaymentTerms[$xx];
			$_SESSION["SESS_CANVASS_ContactPerson"][$xx] = $txtContactPerson[$xx];
			$_SESSION["SESS_CANVASS_TelephoneNum"][$xx] = $txtTelephoneNum[$xx];
			$_SESSION["SESS_CANVASS_FaxNum"][$xx] = $txtFaxNum[$xx];
		}

		foreach ($_POST["hidItemID"] as $sess_key => $sess_value) {
			$_SESSION["SESS_CANVASS_Include"][$sess_key] = $_POST["hidItemID"][$sess_key];
			echo $_SESSION["SESS_CANVASS_Include"][$sess_key];
			echo "<br>";
			for ( $yy = 0; $yy < 4; $yy++ ){
				$_SESSION["SESS_CANVASS_Availability"][$sess_key][$yy] = ( $chkAvailability[$sess_key][$yy] == "on" ? 1 : 0 );
				$_SESSION["SESS_CANVASS_CanvassDetails"][$sess_key][$yy] = $txtCanvassDetails[$sess_key][$yy];
			}
		}

		$_SESSION["SESS_CANVASS_IncludeCount"] = count($_POST["hidItemID"]);

		########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION["ERRMSG_ARR"] = $errmsg_arr;
			session_write_close();
			header("Location: new_canvass.php?id=$hidCanvassID");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Canvass_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ? )");
	mysqli_stmt_bind_param($qry, "issssssii", $hidCanvassID, $hidControlNumber, $txtDate, $radDivision, $txtRemarks
											, $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>process_new_canvass.php"."</td><td>".$processError." near line 280.</td></tr>", 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidCanvassID ) {
				$CanvassID = $hidCanvassID;
			}else{
				$CanvassID = $row["LAST_INSERT_ID()"];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST["SupplierMarking"] as $key => $itemValue)
			{	
				if (!$hidCanvassID) {
					$hidCanvassSuppliersID = 0;
				}

				$itemValue = $itemValue + 1;

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Canvass_Suppliers_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, "iiississss", $hidCanvassSuppliersID[$key], $CanvassID, $sltSupplier[$key]
																  , $txtNewsupplier[$key], $txtDeliveryTerms[$key], $txtPaymentDays[$key]
																  , $txtPaymentTerms[$key], $txtContactPerson[$key], $txtTelephoneNum[$key]
												 				  , $txtFaxNum[$key]);
					$qryItems->execute();
					$resultItems = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>process_new_canvass.php"."</td><td>".$processError1." near line 171.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}else{

						$qryLastSupplierId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

						while($row = mysqli_fetch_assoc($qryLastSupplierId))
						{
							if ( $hidCanvassSuppliersID[$key] ) {
								$CanvassSuppliersID = $hidCanvassSuppliersID[$key];
							}else{
								$CanvassSuppliersID = $row["LAST_INSERT_ID()"];
							}	
						}
						
						if(mysqli_affected_rows($db) > 0 ) 
						{
							foreach($_POST["chkInclude"] as $is_key => $item_supplier_Value)
							{	
								if (!$hidCanvassSuppliersID[$key]) {
									$hidCanvassItemDetailsID = 0;
								}

								if ($item_supplier_Value)
								{
									$chkAvailability[$is_key][$key] = ( $chkAvailability[$is_key][$key] == "on" ? 1 : 0 );

									$qryDetails = mysqli_prepare($db, "CALL sp_Canvass_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ? )");
									mysqli_stmt_bind_param($qryDetails, "iiisdiisi", $hidCanvassItemDetailsID[$is_key][$key], $CanvassSuppliersID
																				   , $hidItemID[$is_key], $hidItemType[$is_key]
																				   , $hidQuantity[$is_key], $hidPREItemID[$is_key]
																				   , $hidLastTransInfo[$is_key], $txtCanvassDetails[$is_key][$key]
																				   , $chkAvailability[$is_key][$key]);
									$qryDetails->execute();
									$resultDetails = mysqli_stmt_get_result($qryDetails); //return results of query
									$processError2 = mysqli_error($db);

									if ( !empty($processError2) ){
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>process_new_canvass.php"."</td><td>".$processError2." near line 171.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}else{

										if($hidCanvassID)
											$_SESSION["SUCCESS"]  = "Successfully updated canvass number ".$hidControlNumber.".";
										else
											$_SESSION["SUCCESS"]  = "Successfully created new canvass.";
										header("location:canvass.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);

									} // if ( !empty($processError2) ) - else
								} // if ($item_supplier_Value)
							} // foreach($_POST['chkInclude'] as $is_key => $item_supplier_Value)
						} // 2nd if(mysqli_affected_rows($db) > 0 ) 
					} // if ( !empty($processError1) ) - else				
				} // if ($itemValue)
			} // foreach($_POST['SupplierMarking'] as $key => $itemValue)
		} // 1st if(mysqli_affected_rows($db) > 0 ) 
	} // if ( !empty($processError) ) - else
		

		require("include/database_close.php");

	}
?>