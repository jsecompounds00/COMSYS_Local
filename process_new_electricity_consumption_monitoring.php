<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	} 
########## -- Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_electricity_consumption_monitoring.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidElctMonitoringID = clean($_POST['hidElctMonitoringID']);
		$hidFinishedGoodID = $_POST['hidFinishedGoodID'];

		if($hidElctMonitoringID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidElctMonitoringID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

########## Input Validations
	
		foreach ($_POST['hidElctItemMonitoringID'] as $ElctMonitoringItemID) {
			$ElctMonitoringItemID = array();
			$ElctMonitoringItemID = $_POST['hidElctItemMonitoringID'];
		}
		foreach ($_POST['txtMachineName'] as $MachineName) {
			$MachineName = array();
			$MachineName = $_POST['txtMachineName'];
		}
		foreach ($_POST['txtMachineID'] as $MachineID) {
			$MachineID = array();
			$MachineID = $_POST['txtMachineID'];
		}
		foreach ($_POST['txtMachineType'] as $MachineType) {
			$MachineType = array();
			$MachineType = $_POST['txtMachineType'];
		}
		foreach ($_POST['txtTrial1'] as $Trial1) {
			$Trial1 = array();
			$Trial1 = $_POST['txtTrial1'];
		}
		foreach ($_POST['txtTrial2'] as $Trial2) {
			$Trial2 = array();
			$Trial2 = $_POST['txtTrial2'];
		}
		foreach ($_POST['txtTrial3'] as $Trial3) {
			$Trial3 = array();
			$Trial3 = $_POST['txtTrial3'];
		}
		foreach ($_POST['txtTrialDate1'] as $TrialDate1) {
			$TrialDate1 = array();
			$TrialDate1 = $_POST['txtTrialDate1'];
		}
		foreach ($_POST['txtTrialDate2'] as $TrialDate2) {
			$TrialDate2 = array();
			$TrialDate2 = $_POST['txtTrialDate2'];
		}
		foreach ($_POST['txtTrialDate3'] as $TrialDate3) {
			$TrialDate3 = array();
			$TrialDate3 = $_POST['txtTrialDate3'];
		}

		$ctr = count($MachineID);
		$i = 0;

		do{
			if ( $Trial1[$i] != '' && !is_numeric($Trial1[$i]) ){
				$errmsg_arr[] = "* Invalid current reading for Trial 1 - ".$MachineName[$i].".";
				$errflag = true;
			}
			if ( $Trial2[$i] != '' && !is_numeric($Trial2[$i]) ){
				$errmsg_arr[] = "* Invalid current reading for Trial 2 - ".$MachineName[$i].".";
				$errflag = true;
			}
			if ( $Trial3[$i] != '' && !is_numeric($Trial3[$i]) ){
				$errmsg_arr[] = "* Invalid current reading for Trial 3 - ".$MachineName[$i].".";
				$errflag = true;
			}
			$valTrialDate1[$i] = validateDate($TrialDate1[$i], 'Y-m-d');
			if ( $Trial1[$i] != '' && $Trial1[$i] != 0 && $valTrialDate1[$i] != 1 ){
				$errmsg_arr[] = "* Invalid date for Trial 1 - ".$MachineName[$i].".";
				$errflag = true;
			}
			if ( $TrialDate1[$i] != '' && $Trial1[$i] == '' ){
				$errmsg_arr[] = "* Current reading for Trial 1 - ".$MachineName[$i]." is missing.";
				$errflag = true;
			}
			$valTrialDate2[$i] = validateDate($TrialDate2[$i], 'Y-m-d');
			if ( $Trial2[$i] != '' && $Trial2[$i] != 0 && $valTrialDate2[$i] != 1 ){
				$errmsg_arr[] = "* Invalid date for Trial 2 - ".$MachineName[$i].".";
				$errflag = true;
			}
			if ( $TrialDate2[$i] != '' && $Trial2[$i] == '' ){
				$errmsg_arr[] = "* Current reading for Trial 2 - ".$MachineName[$i]." is missing.";
				$errflag = true;
			}
			$valTrialDate3[$i] = validateDate($TrialDate3[$i], 'Y-m-d');
			if ( $Trial3[$i] != '' && $Trial3[$i] != 0 && $valTrialDate3[$i] != 1 ){
				$errmsg_arr[] = "* Invalid date for Trial 3 - ".$MachineName[$i].".";
				$errflag = true;
			}
			if ( $TrialDate3[$i] != '' && $Trial3[$i] == '' ){
				$errmsg_arr[] = "* Current reading for Trial 3 - ".$MachineName[$i]." is missing.";
				$errflag = true;
			}

			if ( $Trial1[$i] == '' ){
				$Trial1[$i] = NULL;
			}
			if ( $Trial2[$i] == '' ){
				$Trial2[$i] = NULL;
			}
			if ( $Trial3[$i] == '' ){
				$Trial3[$i] = NULL;
			}

			if ( $TrialDate1[$i] == '' ){
				$TrialDate1[$i] = NULL;
			}
			if ( $TrialDate2[$i] == '' ){
				$TrialDate2[$i] = NULL;
			}
			if ( $TrialDate3[$i] == '' ){
				$TrialDate3[$i] = NULL;
			}

			$i++;

		}while ( $i < $ctr );

// ############# SESSION, keeping last input value
// 		$_SESSION['txtName'] = $txtName;
// 		$_SESSION['radMachineType'] = $radMachineType;
// 		$_SESSION['chkActive'] = $chkActive;
// 		$_SESSION['txtRemarks'] = $txtRemarks;

		// $MachineType = substr($hidMachineType, 0, 1);
########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_electricity_consumption_monitoring.php?id=".$hidFinishedGoodID);
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Electricity_Consumption_Monitoring_CRU(?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iissii', $hidElctMonitoringID, $hidFinishedGoodID, $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_electricity_consumption_monitoring.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidElctMonitoringID ) {
				$ElctMonitoringID = $hidElctMonitoringID;
			}else{
				$ElctMonitoringID = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['txtMachineID'] as $key => $temValue)
			{	
				if (!$hidElctMonitoringID) {
					$ElctMonitoringItemID = 0;
				}

				if ($temValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Electricity_Consumption_Monitoring_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iisidddsss', $ElctMonitoringItemID[$key], $ElctMonitoringID, $MachineType[$key]
																, $MachineID[$key], $Trial1[$key], $Trial2[$key], $Trial3[$key]
																, $TrialDate1[$key], $TrialDate2[$key], $TrialDate3[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_electricity_consumption_monitoring.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidElctMonitoringID )
							$_SESSION['SUCCESS']  = 'Electricity Consumstion Monitoring successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Electricity Consumstion Monitoring successfully created.';
						header("location: pam_analysis.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database

		require("include/database_close.php");

	}
?>