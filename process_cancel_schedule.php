<?php
	session_start();

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require("include/database_connect.php");

	$schedule_id = intval($_GET['schedule_id']);
	$equipment_Id = intval($_GET['equipment_Id']);
	$reason = strval($_GET['reason']);
	$date_cancelled = date('Y-m-d H:i:s');
	$cancel=1;
	
	$qry = mysqli_prepare($db, "CALL sp_Equipment_Schedule_Cancel(?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iissi', $schedule_id, $cancel, $reason, $date_cancelled, $_SESSION["SESS_USER_ID"]);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_cancel_schedule.php'.'</td><td>'.$processError.' near line 31.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS'] = 'Schedule was succesfully cancelled.';
			header("location:schedule_history.php?id=$equipment_Id	&page=1&search=&qsone=");
		}
			require("include/database_close.php");
?>