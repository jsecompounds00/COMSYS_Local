<?php
	session_start();
	if( $_SESSION['cancel_po'] == false) 
	{
		$_SESSION['ERRMSG_ARR'] ='Access denied!';
		session_write_close();
		header("Location:comsys.php");
		exit();
	}	

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require("include/database_connect.php");

	$id = intval($_GET['id']);
	$reason = strval($_GET['reason']);
	$date_cancelled = date('Y-m-d H:i:s');
	$cancel=1;
	
	$qry = mysqli_prepare($db, "CALL sp_PO_Cancel(?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iiss', $id, $cancel, $reason, $date_cancelled);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_cancel_pre.php'.'</td><td>'.$processError.' near line 31.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS'] = 'PO was succesfully cancelled.';
			header("location:po_monitoring.php?page=".$_SESSION["page"]."&po_date=".$_SESSION["po_date"]."&po_number=".$_SESSION["po_number"]."&pre_number=".$_SESSION["pre_number"]);
		}
			require("include/database_close.php");
?>