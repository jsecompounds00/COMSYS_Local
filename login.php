

 <html>
     <head>
          <link rel="stylesheet" type="text/css" href="css/mystyle.css">
          <script type="text/javascript">
               function FocusOnInput()
               {
                    document.getElementById("username").focus();
               }
          </script>
          <?php
             session_start();
          ?>
          <!-- {{H1:PHP: Reload count}} -->

          <!-- <nowiki> -->
          <?php
          if (empty($_SESSION['counter']))
            $_SESSION['counter'] = 1;
          else
            $_SESSION['counter']++;
          ?>
          <!-- </nowiki> -->

          <!-- You've (re-)loaded this page  -->
          <?php //echo $_SESSION['counter']; ?> <!-- times. --> 
          <!-- Press F5 to reload it again. -->
          <?php
            $login = "login.php";
            $sec = "1";

            if (  $_SESSION['counter'] == 1 ){
          ?>
              <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $login?>'">
          <?php }?>
          <title>COMSYS LogIn</title>
     </head>
     <body onload='FocusOnInput()'>
          <form name="loginform" action="process_login.php" method="post">
                 <div class="head_title">
                      <a href="login.php" id="head"> 
                           <h1> C O M S Y S </h1> 
                      </a>
                 </div>
               <div class="wrapper">
                    <span> <h3> Sign In </h3> </span>
                   <?php
                    // session_start();
                    if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
                         echo '<ul class="err">';

                         foreach($_SESSION['ERRMSG_ARR'] as $msg) {
                              echo '<li>'.$msg.'</li>'; 
                          }

                         echo '</ul>';
                         unset($_SESSION['ERRMSG_ARR']);
                    }
                  ?>
                    <table class="login">
                         <tr>
                              <td>Username:</td>
                              <td><input name="username" type="text" id='username' autocomplete="off"/></td>
                         </tr>
                         <tr>
                              <td>Password:</td>
                              <td><input name="password" type="password" /></td>
                         </tr>
                         <tr class="align_bottom">
                              <td></td>
                              <td>
                                   <input name="" type="submit" value="Log In" />
                              </td>
                         </tr>
                    </table>
               </div>
          </form>
     </body>
     <footer>
     </footer>
</html>