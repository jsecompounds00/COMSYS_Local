<html>
	<head>
		<title>O/C Budget Distribution - Home</title>
		<?php
			require("/include/database_connect.php");

			$page = ($_GET["page"] ? $_GET["page"] : 1);	
			$budgetID = $_GET["id"];	
			$itemID = $_GET["item_id"];	
		?>
	</head>
	<body>
		<?php
			require("/include/header.php");
			// require("/include/init_unset_values/budget_monitoring_oc_distribution_unset_value.php");

			// if( $_SESSION['supplies'] == false) 
			// {
			// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }

		?>

		<div class="wrapper">
			<?php
				$qryDH = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_OC_Distribution_2_Query( ?, ? )" );
				mysqli_stmt_bind_param( $qryDH, "ii", $budgetID, $itemID);
				$qryDH->execute();
				$resultDH = mysqli_stmt_get_result($qryDH);
				while( $rowDH = mysqli_fetch_assoc( $resultDH ) ){
					$DHBudgetYear = $rowDH["DHBudgetYear"];
					$DHDepartment = $rowDH["DHDepartment"];
					$DHItemName = $rowDH["DHItemName"];
				}
				$db->next_result();
				$resultDH->close();
			
				$page_b = $_SESSION["page_b"];
				$search_b = htmlspecialchars($_SESSION["search_b"]);
				$qsone_b = htmlspecialchars($_SESSION["qsone_b"]);
				$id_b = htmlspecialchars($_SESSION["id_b"]);
				$page_b = ( (strpos(($page_b), "\\")+1) > 0 ? str_replace("\\", "", $page_b) : $page_b );
				$search_b = ( (strpos(($search_b), "'")+1) > 0 ? str_replace("'", "\'", $search_b) : $search_b );
				$qsone_b = ( (strpos(($qsone_b), "\\")+1) > 0 ? str_replace("\\", "", $qsone_b) : $qsone_b );
				$id_b = ( (strpos(($id_b), "'")+1) > 0 ? str_replace("'", "\'", $id_b) : $id_b );
			?>
			
			<span> <h3> History of <?php echo $DHItemName; ?> for <?php echo $DHDepartment." - Y".$DHBudgetYear;?> </h3> </span>

			<a class='back' href='budget_monitoring_oc_distribution.php?page=<?php echo $page_b;?>&search=<?php echo $search_b;?>&qsone=<?php echo $qsone_b;?>&id=<?php echo $id_b;?>'><img src='images/back.png' height="20" name='txtBack'> Distribution </a>
			
	 		<?php
				if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_oc_history.php"."</td><td>".$error." near line 27.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryBudget = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_OC_Distribution_History( ?, ?, NULL, NULL )" );
					mysqli_stmt_bind_param( $qryBudget, "ii", $budgetID, $itemID);
					$qryBudget->execute();
					$resultBudget = mysqli_stmt_get_result($qryBudget);
					$total_results = mysqli_num_rows($resultBudget); //return number of rows of result

					$db->next_result();
					$resultBudget->close();

					$targetpage = "budget_monitoring_oc_history.php"; 	//your file name  (the name of this file)
					require("include/paginate_budget_history.php");

					$qryBudgetLimit = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_OC_Distribution_History( ?, ?, ?, ? )" );
					mysqli_stmt_bind_param( $qryBudgetLimit, "iiii", $budgetID, $itemID, $start, $end );
					$qryBudgetLimit->execute();
					$resultBudgetLimit = mysqli_stmt_get_result($qryBudgetLimit);
					$processError3 = mysqli_error($db);	

					if(!empty($processError3))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_oc_history.php"."</td><td>".$processError3." near line 137.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
				?>
						<table class="home_pages">
							<tr>
								<td colspan="9">
									<?php echo $pagination; ?>
								</td>
							</tr>
							 <tr>
								<th> Transaction Date </th>
								<th> UOM </th>
								<th> Quantity </th>
								<th> Unit Price </th>
								<th> Reference Number </th>
								<th> Comments </th>
							</tr>
							<?php
								while ($row = mysqli_fetch_assoc($resultBudgetLimit)) {
									$HTransactionDate = $row["HTransactionDate"];
									$HUOM = $row["HUOM"];
									$HQuantity = $row["HQuantity"];
									$HUnitPrice = $row["HUnitPrice"];
									$HComments = $row["HComments"];
									$HRefNumber = $row["HRefNumber"];
							?>
									<tr>
										<td> <?php echo $HTransactionDate;?> </td>
										<td> <?php echo $HUOM;?> </td>
										<td> <?php echo $HQuantity;?> </td>
										<td> <?php echo $HUnitPrice;?> </td>
										<td> <?php echo $HRefNumber;?> </td>
										<td> <?php echo $HComments;?> </td>
									</tr>
							<?php
								}
								$db->next_result();
								$resultBudgetLimit->close();
							?>
							<tr>
								<td colspan="9">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
	