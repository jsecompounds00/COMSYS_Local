<html>
	<head>
		<?php
			require("/include/database_connect.php");
			if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_department.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$deptId = $_GET["id"];

					if($deptId)
					{ 
						$qry = mysqli_prepare($db, "CALL sp_Department_Query(?)");
						mysqli_stmt_bind_param($qry, "i", $deptId);
						$qry->execute();
						$result = mysqli_stmt_get_result($qry);
						$processError = mysqli_error($db);

						if(!empty($processError))
						{
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_department.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							while($row = mysqli_fetch_assoc($result))
							{
								$code = htmlspecialchars($row["code"]);
								$name = htmlspecialchars($row["name"]);
								$db_compounds = $row["compounds"];
								$db_pipes = $row["pipes"];
								$db_corporate = $row["corporate"];
								$db_ppr = $row["ppr"];
								$active = $row["active"];
								$tag_budget = $row['tag_budget'];
								$acct_code = $row['acct_code'];
								$createdAt = $row["created_at"];
								$createdId = $row["created_id"];
							}
						}
						$db->next_result();
						$result->close();

					############ .............
						$qryPI = "SELECT id from comsys.departments";
						$resultPI = mysqli_query($db, $qryPI); 
						$processErrorPI = mysqli_error($db);

						if ( !empty($processErrorPI) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_department.php"."</td><td>".$processErrorPI." near line 58.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}else{
								$id = array();
							while($row = mysqli_fetch_assoc($resultPI)){
								$id[] = $row["id"];
							}
						}
						$db->next_result();
						$resultPI->close();

					############ .............
						if( !in_array($deptId, $id, TRUE) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_department.php</td><td>The user tries to edit a non-existing department_id.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}

						echo "<title>Department - Edit</title>";
					}
					else
					{
						echo "<title>Department - Add</title>";
					}
				}
		?>
		<script src="js/department_js.js"></script>
	</head>
	<body onload="enableBudget()">
	
		<form method="post" action="process_new_department.php">
			
			<?php
				require("/include/header.php");
				require("/include/init_unset_values/department_init_value.php");

				if ( $deptId ){
					if( $_SESSION["edit_department"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{	
					if( $_SESSION["add_department"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}			
				}

			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $deptId ? "Edit ".$name : "New Department" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
					    <td>Code:</td>
					    <td>
					    	<input type="text" name="txtCode" value="<?php echo ( $deptId ? $code : $initDEPCode );?>" <?php echo ( $deptId > 0 && $deptId < 1000 ? "readOnly" : "" );?>>
					    	<label class="instruction">(must be 3 letters only)</label>
					    </td>
					</tr>
					<tr>
					    <td>Department:</td>
					    <td>
					    	<input type="text" name="txtDepartment" value="<?php echo ( $deptId ? $name : $initDEPDepartment );?>" <?php echo ( $deptId > 0 && $deptId < 1000 ? "readOnly" : "" );?>>
					    </td>
					</tr>
					<tr>
						<td>Division:</td>
						<td>
							<?php
								if($deptId){
									if ( $deptId < 1000 ){
							?>	
										<b>
										<?php
					  						if($db_compounds) {
					  							echo " Compounds";
					  					?>
					  							<input type="checkbox" name="chkCmpds" id="Cmpds" class="outcast" <?php echo ( $db_compounds ? "checked" : "" );?>><br>
					  					<?php
					  						}
					  						if($db_pipes) {
					  							echo " Pipes";
					  					?>
												<input type="checkbox" name="chkPips" id="Pips" class="outcast" <?php echo ( $db_pipes ? "checked" : "" );?>>  <br>
					  					<?php
					  						}
					  						if($db_corporate) {
					  							echo " Corporate";
					  					?>
												<input type="checkbox" name="chkCorp" id="Corp" class="outcast" <?php echo ( $db_corporate ? "checked" : "" );?>> <br>
					  					<?php
					  						}
					  						if($db_ppr) {
					  							echo " Corporate ";
					  					?>
												<input type="checkbox" name="chkPPR" id="PPR" class="outcast" <?php echo ( $db_ppr ? "checked" : "" );?>> 
					  					<?php
					  						}
					  					?>
				  						</b>
				  			<?php
									}else{
							?>
										<input type="checkbox" name="chkCmpds" id="Cmpds" <?php echo ( $db_compounds ? "checked" : "" );?>><label for="Cmpds"> Compounds</label>
										<input type="checkbox" name="chkPips" id="Pips" <?php echo ( $db_pipes ? "checked" : "" );?>>  <label for="Pips"> Pipes</label>
										<input type="checkbox" name="chkCorp" id="Corp" <?php echo ( $db_corporate ? "checked" : "" );?>> <label for="Corp"> Corporate</label>
										<input type="checkbox" name="chkPPR" id="PPR" <?php echo ( $db_ppr ? "checked" : "" );?>> <label for="PPR"> PPR</label>
				  			<?php
				  					}
								}
								else {
							?>
									<input type="checkbox" name="chkCmpds" id="Cmpds" <?php echo ( $initDEPCmpds ? "checked" : "" );?>><label for="Cmpds"> Compounds</label>
									<input type="checkbox" name="chkPips" id="Pips" <?php echo ( $initDEPPips ? "checked" : "" );?>><label for="Pips"> Pipes</label>
									<input type="checkbox" name="chkCorp" id="Corp" <?php echo ( $initDEPCorp ? "checked" : "" );?>><label for="Corp"> Corporate</label>
									<input type="checkbox" name="chkPPR" id="PPR" <?php echo ( $initDEPPPR ? "checked" : "" );?>><label for="PPR"> PPR</label>
			  				<?php
								}
							?>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<?php
								if($deptId){
									if ( $deptId < 1000 ){
										echo "<b>".($active ? "Yes" : "No")."</b>";
							?>
										<input type="checkbox" name="chkActive" class="outcast" <?php echo ( $active ? "checked" : "" );?>>
							<?php
									}else{
							?>
										<input type="checkbox" name="chkActive" <?php echo ( $active ? "checked" : "" );?>>
							<?php
									}
								}
								else {
							?>
									<input type="checkbox" name="chkActive" <?php echo ( $initDEPActive ? "checked" : "" );?>>
							<?php
								}
							?>
						</td>
					</tr>
					<tr>
						<td>
						<label class='instruction'>*For Compounds Only</label>
						</td>
					</tr>
					<tr>
						<td>Budget :</td>
						<td>
							<input type='checkbox' name='chkBudget' id='chkBudget' value='1' onchange='enableBudget()' <?php echo ( $deptId ? ( $tag_budget ? "checked" : "" ) : ( $initDEPBudget ? "checked" : "" ) ); ?>>
						</td>
					</tr>
					<tr>
						<td>Account Code :</td>
						<td>
							<input type='text' name='txtAccountCode' id='txtAccountCode' value='<?php echo ( $deptId ? $acct_code : $initDEPAccountCode ); ?>' readOnly>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveDept" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='department.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidDeptid" value="<?php echo $deptId;?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $deptId ? $createdId : $_SESSION["SESS_USER_ID"] );?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $deptId ? $createdAt : date("Y-m-d H:i:s") );?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>