<html>
	<head>
		<?php
			require("/include/database_connect.php");
			if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_expense_transactions.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$expenseTransID = $_GET["id"];

					if($expenseTransID)
					{ 
						$qry = mysqli_prepare($db, "CALL sp_Expense_Transactions_Query(?)");
						mysqli_stmt_bind_param($qry, "i", $expenseTransID);
						$qry->execute();
						$result = mysqli_stmt_get_result($qry);
						$processError = mysqli_error($db);

						if(!empty($processError))
						{
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_expense_transactions.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							while($row = mysqli_fetch_assoc($result))
							{
								$TQTransactionDate = $row["TQTransactionDate"];
								$TQDepartmentID = $row["TQDepartmentID"];
								$TQGLCodeID = $row["TQGLCodeID"];
								$TQComments = $row["TQComments"];
								$TQCreatedAt = $row["TQCreatedAt"];
								$TQCreatedID = $row["TQCreatedID"];

								$TQExpenseItemsID[] = $row["TQExpenseItemsID"];
							    $TQParticularsID[] = $row["TQParticularsID"];
							    $TQItemsCreatedAt[] = $row["TQItemsCreatedAt"];
							    $TQItemsCreatedID[] = $row["TQItemsCreatedID"];
							    $TQRemarks[] = htmlspecialchars($row["TQRemarks"]);
							    $TQUOMID[] = $row["TQUOMID"];
							    $TQQuantity[] = ( $row["TQQuantity"] != 0 ? $row["TQQuantity"] : NULL );
							    $TQUnitPrice[] = ( $row["TQUnitPrice"] != 0 ? $row["TQUnitPrice"] : NULL );
							}
						}
						$db->next_result();
						$result->close();

						$count = count($TQExpenseItemsID);

			############ .............
						$qryPI = "SELECT id from comsys.expense_transactions";
						$resultPI = mysqli_query($db, $qryPI); 
						$processErrorPI = mysqli_error($db);

						if ( !empty($processErrorPI) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_expense_transactions.php"."</td><td>".$processErrorPI." near line 58.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}else{
								$id = array();
							while($row = mysqli_fetch_assoc($resultPI)){
								$id[] = $row["id"];
							}
						}
						$db->next_result();
						$resultPI->close();

			############ .............
						if( !in_array($expenseTransID, $id, TRUE) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_expense_transactions.php</td><td>The user tries to edit a non-existing department_id.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}

						echo "<title>Expense Transactions - Edit</title>";
					}
					else
					{
						echo "<title>Expense Transactions - Add</title>";
					}
				}
		?>
		<script src="js/budget_monitoring_departmental_js.js"></script>  
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> 
		<script src="js/datetimepicker_css.js"></script>  
	</head>
	<body>
	
		<form method="post" action="process_new_expense_transactions.php">
			
			<?php
				require("/include/header.php");
				require("/include/init_unset_values/expense_transaction_init_value.php");

				// if ( $expenseTransID ){
				// 	if( $_SESSION["edit_expense_transaction"] == false) 
				// 	{
				// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}
				// }else{	
				// 	if( $_SESSION["add_expense_transaction"] == false) 
				// 	{
				// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}			
				// }

			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $expenseTransID ? "Edit Expense Transaction" : "New Expense Transaction" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td> Transaction Date: </td>
						<td>
							<input type="text" name="txtTransactionDate" id="txtTransactionDate" value="<?php echo($expenseTransID && $initBETTransactionDate == NULL ? $TQTransactionDate : $initBETTransactionDate);?>" onchange="showParticularsTrans()">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtTransactionDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr>
						<td> Department: </td>
						<td>
							<select name="sltDepartment" id="sltDepartment" onchange="showGLCode(), showParticularsTrans(), showUOM(52)">
								<?php
		 							$qryBD = "CALL sp_Budget_Department_Dropdown(1)"; 
									$resultBD = mysqli_query($db, $qryBD);
									$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_departmental.php"."</td><td>".$processError1." near line 43.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultBD))
										{
											$DDId = $row["DDId"];
											$DDCode = $row["DDCode"];

											echo "<option value='".$DDId."'".
												  ($expenseTransID && $initBETDepartment == NULL ? ( $TQDepartmentID == $DDId ? "selected" : "" ) : ( $initBETDepartment == $DDId ? "selected" : "" ))
												  .">".$DDCode."</option>";
										}
										
										$db->next_result();
										$resultBD->close();
									}
			 					?>
							</select>
						</td>
					</tr>

					<tr>
						<td> GL Code: </td>
						<td>
							<select name="sltGLCode" id="sltGLCode" onchange="showParticularsTrans(), showUOM(52)">
								<?php
									if ( $initBETDepartment || $expenseTransID ){
										$qryMT = "CALL sp_Budget_Dept_GL_Code_Dropdown(1)";
										$resultMT = mysqli_query($db, $qryMT);
										$processErrorMT = mysqli_error($db);

										if(!empty($processErrorMT))
										{
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_departmental.php"."</td><td>".$processErrorMT." near line 43.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultMT))
											{
												$DDMTID = $row["DDMTID"];
												$DDMTName = $row["DDMTName"];

												echo "<option value='".$DDMTID."'".
													  ($expenseTransID && $count > $i && $initBETGLCode == NULL ? ( $TQGLCodeID == $DDMTID ? "selected" : "" ) : ( $initBETGLCode == $DDMTID ? "selected" : "" ))
													  .">".$DDMTName."</option>";
											}
											
											$db->next_result();
											$resultMT->close();
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td> For voucher? </td>
						<td>
							<input type="checkbox" name="chkVoucher">
						</td>
					</tr>
				</table>

				<table class="child_tables_form collapsed">
					<tr>
						<th> Particulars </th>
						<th> UOM </th>
						<th> Quantity </th>
						<th> Unit Price </th>
						<th> Remarks </th>
					</tr>

					<?php
						for ($i=0; $i < 10 ; $i++) { 
					?>
							<tr>
								<td>
									<select name="sltGLParticulars[<?php echo $i;?>]" id="sltGLParticulars<?php echo $i;?>" onchange="showUOM(<?php echo $i;?>)">
										<?php
											$qryM = mysqli_prepare( $db, "CALL sp_Budget_GL_Particulars_Trans_Dropdown(?,?,?)" );
											mysqli_stmt_bind_param( $qryM, 'iii', ($expenseTransID && $initBETGLCode == NULL ? $TQDepartmentID : $initBETDepartment), ($expenseTransID && $initBETGLCode == NULL ? $TQGLCodeID : $initBETGLCode), ($expenseTransID && $initBETGLCode == NULL ? $TQTransactionDate : $initBETTransactionDate) );
											$qryM->execute();
											$resultM = mysqli_stmt_get_result( $qryM );
											$processErrorM = mysqli_error($db);
										
											if ($processErrorM){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_budget_monitoring_departmental.php'.'</td><td>'.$processErrorM.' near line 258.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultM))
												{
													$DDItemID = $row['DDItemID'];
													$DDItemName = $row['DDItemName'];
													
													echo "<option value='".$DDItemID."'".
														  ($expenseTransID && $count > $i && $initBETParticulars[$i] == NULL ? ( $TQParticularsID[$i] == $DDItemID ? "selected" : "" ) : ( $initBETParticulars[$i] == $DDItemID ? "selected" : "" ))
														  .">".$DDItemName."</option>";
												}
												$db->next_result();
												$resultM->close();
											}
										?> 
									</select>
								</td>

								<td id="tdUOM<?php echo $i;?>">
									<?php
										if( $initBETParticulars[$i] || $expenseTransID ){
											$qry = mysqli_prepare( $db, "CALL sp_Budget_GL_Particulars_Details_Query(?,?)" );
											mysqli_stmt_bind_param( $qry, 'ii', ($expenseTransID && $count > $i && $initBETParticulars[$i] == NULL ? $TQParticularsID[$i] : $initBETParticulars[$i]), ($expenseTransID && $count > $i && $initBETParticulars[$i] == NULL ? $TQDepartmentID : $initBETDepartment) );
											$qry->execute();
											$result = mysqli_stmt_get_result( $qry );
											$processError = mysqli_error($db);
										
											if ($processError){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_budget_monitoring_departmental'.'</td><td>'.$processError.' near line 289.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($result))
												{
													$MDUOMID = $row['MDUOMID'];
													$MDUOMCode = $row['MDUOMCode'];
													
													echo $MDUOMCode;
									?>
													<input type="hidden" name="hidUOM[]" value="<?php echo $MDUOMID;?>">
									<?php
												}
												$db->next_result();
												$result->close();
											}
										}
									?> 
								</td>
								<td>
									<input type="text" name="txtQuantity[]" value="<?php echo($expenseTransID && $count > $i  && $initBETQuantity[$i] == NULL ? $TQQuantity[$i] : $initBETQuantity[$i]);?>">
								</td>
								<td>
									<input type="text" name="txtUnitPrice[]" value="<?php echo($expenseTransID && $count > $i  && $initBETUnitPrice[$i] == NULL ? $TQUnitPrice[$i] : $initBETUnitPrice[$i]);?>">
								</td>
								<td>
									<input type="text" name="txtRemarks[]" class="long_text" value="<?php echo($expenseTransID && $count > $i  && $initBETRemarks[$i] == NULL ? $TQRemarks[$i] : htmlspecialchars($initBETRemarks[$i]));?>">
								</td>
									<input type="hidden" name="hidExpenseTransItemID[]" value="<?php echo ($expenseTransID && $count > $i ? $TQExpenseItemsID[$i] : 0 );?>">
									<input type="hidden" name="hidItemsCreatedId[]" value="<?php echo ( $expenseTransID && $count > $i && $TQItemsCreatedID[$i] ? $TQItemsCreatedID[$i] : $_SESSION["SESS_USER_ID"] );?>">
									<input type="hidden" name="hidItemsCreatedAt[]" value="<?php echo ( $expenseTransID && $count > $i && $TQItemsCreatedAt[$i] ? $TQItemsCreatedAt[$i] : date("Y-m-d H:i:s") );?>">
							</tr>
					<?php
						}
					?>	
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveDept" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='budget_monitoring_departmental.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidExpenseTransID" id="hidExpenseTransID" value="<?php echo $expenseTransID;?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $expenseTransID ? $TQCreatedID : $_SESSION["SESS_USER_ID"] );?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $expenseTransID ? $TQCreatedAt : date("Y-m-d H:i:s") );?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>