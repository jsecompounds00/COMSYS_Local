<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_equipment_status.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidCCRID = $_POST['hidCCRID'];
		$txtCCRNo = $_POST['txtCCRNo'];
		$sltCustomer = $_POST['sltCustomer'];
		$txtCCRDate = $_POST['txtCCRDate'];
		$sltComplaintType = $_POST['sltComplaintType'];
		$txtOtherComplaintType = $_POST['txtOtherComplaintType'];
		$sltProduct_CCR = $_POST['sltProduct_CCR'];
		$txtLotNum = $_POST['txtLotNum'];
		$txtBagNum = $_POST['txtBagNum'];
		$txtComplaint = $_POST['txtComplaint'];

		$chkSalesContainment = $_POST['chkSalesContainment'];
		$chkTechnicalContainment = $_POST['chkTechnicalContainment'];
		$chkWPDContainment = $_POST['chkWPDContainment'];
		
		$txtContainment = $_POST['txtContainment'];
		$txtImplementContainment = $_POST['txtImplementContainment'];
		$txtResponsibleContainment = $_POST['txtResponsibleContainment'];
		
		$chkTechnicalRootCause = $_POST['chkTechnicalRootCause'];
		$chkPAMRootCause = $_POST['chkPAMRootCause'];
		$chkWPDRootCause = $_POST['chkWPDRootCause'];
		
		$chkMan = $_POST['chkMan'];
		$chkMethod = $_POST['chkMethod'];
		$chkMachine = $_POST['chkMachine'];
		$chkMaterial = $_POST['chkMaterial'];
		$chkMeasurement = $_POST['chkMeasurement'];
		$chkMotherNature = $_POST['chkMotherNature'];

		$chkMotherNature = $_POST['chkMotherNature'];
		
		$txtRootCause = $_POST['txtRootCause'];
		$txtCorrective = $_POST['txtCorrective'];
		$txtImplementCorrective = $_POST['txtImplementCorrective'];
		$txtResponsibleCorrective = $_POST['txtResponsibleCorrective'];
		$txtPreventive = $_POST['txtPreventive'];
		$txtImplementPreventive = $_POST['txtImplementPreventive'];
		$txtResponsiblePreventive = $_POST['txtResponsiblePreventive'];
		$txtRemarks = $_POST['txtRemarks'];

		if( isset($_POST['chkSalesContainment']) ){
			$chkSalesContainment = 1;
		}else{
			$chkSalesContainment = 0;
		}
		if( isset($_POST['chkTechnicalContainment']) ){
			$chkTechnicalContainment = 1;
		}else{
			$chkTechnicalContainment = 0;
		}
		if( isset($_POST['chkWPDContainment']) ){
			$chkWPDContainment = 1;
		}else{
			$chkWPDContainment = 0;
		}

		if( isset($_POST['chkInvalid']) ){
			$chkInvalid = 1;
		}else{
			$chkInvalid = 0;
		}

		if( isset($_POST['chkTechnicalRootCause']) ){
			$chkTechnicalRootCause = 1;
		}else{
			$chkTechnicalRootCause = 0;
		}
		if( isset($_POST['chkPAMRootCause']) ){
			$chkPAMRootCause = 1;
		}else{
			$chkPAMRootCause = 0;
		}
		if( isset($_POST['chkWPDRootCause']) ){
			$chkWPDRootCause = 1;
		}else{
			$chkWPDRootCause = 0;
		}

		if( isset($_POST['chkMan']) ){
			$chkMan = 1;
		}else{
			$chkMan = 0;
		}
		if( isset($_POST['chkMethod']) ){
			$chkMethod = 1;
		}else{
			$chkMethod = 0;
		}
		if( isset($_POST['chkMachine']) ){
			$chkMachine = 1;
		}else{
			$chkMachine = 0;
		}
		if( isset($_POST['chkMaterial']) ){
			$chkMaterial = 1;
		}else{
			$chkMaterial = 0;
		}
		if( isset($_POST['chkMeasurement']) ){
			$chkMeasurement = 1;
		}else{
			$chkMeasurement = 0;
		}
		if( isset($_POST['chkMotherNature']) ){
			$chkMotherNature = 1;
		}else{
			$chkMotherNature = 0;
		}

############# Input Validation
		if ( $txtCCRNo == '' ){
			$errmsg_arr[] = '* Invalid CCR Number.';
			$errflag = true;
		}
		$valCCRDate = validateDate($txtCCRDate, 'Y-m-d');
		if ( $valCCRDate != 1 ){
			$errmsg_arr[] = '* Invalid CCR date.';
			$errflag = true;
		}	
		if ( !$sltCustomer ){
			$errmsg_arr[] = '* Select customer.';
			$errflag = true;
		}
		if ( !$sltComplaintType ){
			$errmsg_arr[] = '* Select complaint type.';
			$errflag = true;
		}
		if ( $sltComplaintType == 'Others' && $txtOtherComplaintType == '' ){
			$errmsg_arr[] = '* Specify other complaint type.';
			$errflag = true;
		}
		if ( !$sltProduct_CCR ){
			$errmsg_arr[] = '* Select product.';
			$errflag = true;
		}
		if ( $txtComplaint == '' ){
			$errmsg_arr[] = '* Complaint cannot be blank.';
			$errflag = true;
		}

		if($hidCCRID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidCCRID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation

	$txtComplaint = str_replace('\\r\\n', '<br>', $txtComplaint);
	$txtContainment = str_replace('\\r\\n', '<br>', $txtContainment);
	$txtRootCause = str_replace('\\r\\n', '<br>', $txtRootCause);
	$txtCorrective = str_replace('\\r\\n', '<br>', $txtCorrective);
	$txtPreventive = str_replace('\\r\\n', '<br>', $txtPreventive);
	$txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
	
	$txtComplaint = str_replace('\\', '', $txtComplaint);
	$txtContainment = str_replace('\\', '', $txtContainment);
	$txtRootCause = str_replace('\\', '', $txtRootCause);
	$txtCorrective = str_replace('\\', '', $txtCorrective);
	$txtPreventive = str_replace('\\', '', $txtPreventive);
	$txtRemarks = str_replace('\\', '', $txtRemarks);

	############# SESSION, keeping last input value
		$_SESSION['SESS_CCM_CCRNo'] = $txtCCRNo;
		$_SESSION['SESS_CCM_Invalid'] = $chkInvalid;
		$_SESSION['SESS_CCM_Customer'] = $sltCustomer;
		$_SESSION['SESS_CCM_CCRDate'] = $txtCCRDate;
		$_SESSION['SESS_CCM_ComplaintType'] = $sltComplaintType;
		$_SESSION['SESS_CCM_OtherComplaintType'] = $txtOtherComplaintType;
		$_SESSION['SESS_CCM_LotNum'] = $txtLotNum;
		$_SESSION['SESS_CCM_BagNum'] = $txtBagNum;
		$_SESSION['SESS_CCM_Complaint'] = $txtComplaint;
		$_SESSION['SESS_CCM_SalesContainment'] = $chkSalesContainment;
		$_SESSION['SESS_CCM_TechnicalContainment'] = $chkTechnicalContainment;
		$_SESSION['SESS_CCM_WPDContainment'] = $chkWPDContainment;
		$_SESSION['SESS_CCM_Containment'] = $txtContainment;
		$_SESSION['SESS_CCM_ImplementContainment'] = $txtImplementContainment;
		$_SESSION['SESS_CCM_ResponsibleContainment'] = $txtResponsibleContainment;
		$_SESSION['SESS_CCM_TechnicalRootCause'] = $chkTechnicalRootCause;
		$_SESSION['SESS_CCM_PAMRootCause'] = $chkPAMRootCause;
		$_SESSION['SESS_CCM_WPDRootCause'] = $chkWPDRootCause;
		$_SESSION['SESS_CCM_Man'] = $chkMan;
		$_SESSION['SESS_CCM_Method'] = $chkMethod;
		$_SESSION['SESS_CCM_Machine'] = $chkMachine;
		$_SESSION['SESS_CCM_Material'] = $chkMaterial;
		$_SESSION['SESS_CCM_Measurement'] = $chkMeasurement;
		$_SESSION['SESS_CCM_MotherNature'] = $chkMotherNature;
		$_SESSION['SESS_CCM_RootCause'] = $txtRootCause;
		$_SESSION['SESS_CCM_Corrective'] = $txtCorrective;
		$_SESSION['SESS_CCM_ImplementCorrective'] = $txtImplementCorrective;
		$_SESSION['SESS_CCM_ResponsibleCorrective'] = $txtResponsibleCorrective;
		$_SESSION['SESS_CCM_Preventive'] = $txtPreventive;
		$_SESSION['SESS_CCM_ImplementPreventive'] = $txtImplementPreventive;
		$_SESSION['SESS_CCM_ResponsiblePreventive'] = $txtResponsiblePreventive;
		$_SESSION['SESS_CCM_Remarks'] = $txtRemarks;

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_ccr_monitoring.php?id=$hidCCRID");
			exit();
		}

############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_CCR_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'iisisisisssiiissssiiiiiiiiisssssssssii', $hidCCRID, $chkInvalid
												, $txtCCRNo, $sltCustomer, $txtCCRDate
												, $sltComplaintType, $txtOtherComplaintType, $sltProduct_CCR
												, $txtLotNum, $txtBagNum, $txtComplaint
												, $chkSalesContainment, $chkTechnicalContainment, $chkWPDContainment
												, $txtContainment, $txtImplementContainment, $txtResponsibleContainment
												, $txtRootCause, $chkTechnicalRootCause, $chkPAMRootCause
												, $chkWPDRootCause, $chkMan, $chkMethod
												, $chkMachine, $chkMaterial, $chkMeasurement
												, $chkMotherNature, $txtCorrective, $txtImplementCorrective
												, $txtResponsibleCorrective, $txtPreventive, $txtImplementPreventive
												, $txtResponsiblePreventive, $txtRemarks, $createdAt
												, $updatedAt, $createdId, $updatedId
												);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_cpiar_monitoring.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidCCRID)
				$_SESSION['SUCCESS']  = 'Successfully updated CCR.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new CCR.';
			//echo $_SESSION['SUCCESS'];
			header("location:ccr_monitoring.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}
		$db->next_result();
		$result->close(); 
############# Commiting to Database

		require("include/database_close.php");

	}
?>