<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_customer_pipes.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidcustId = $_POST['hidcustId'];
		$txtName = clean($_POST['txtName']);

		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;

		$_SESSION['txtName'] = $txtName;
		$_SESSION['chkActive'] = $chkActive;

		//Input Validations
		if ( empty($txtName) ){
			$errmsg_arr[] = '*Customer name is missing.';
			$errflag = true;
		}
		if (is_numeric($txtName)){
			$errmsg_arr[] = '* Invalid Customer name.';
			$errflag = true;
		}

		if($hidcustId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidcustId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:new_customer_pipes.php?id=".$hidcustId);
			exit();
		}

		$qry = "CALL sp_Customer_Pipes_CRU('$hidcustId', '$txtName', '$chkActive', '$createdAt', '$updatedAt', '$createdId', '$updatedId')";

		$result = mysqli_query($db, $qry);

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_customer_pipes.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidcustId)
				$_SESSION['SUCCESS']  = 'Successfully updated customer.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new customer.';
			//echo $_SESSION['SUCCESS'];
			header("location:customer_pipes.php?page=1&search=&qsone=");	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	