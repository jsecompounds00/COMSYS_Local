<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_raw_materials.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidRMId = $_POST['hidRMId'];
		$chkCompounds = $_POST['chkCompounds'];
		$chkPipes = $_POST['chkPipes'];
		$chkCorporate = $_POST['chkCorporate'];
		$chkPPR = $_POST['chkPPR'];
		$sltRMType = $_POST['sltRMType'];
		$txtRMCode = $_POST['txtRMCode'];
		$txtRMName = $_POST['txtRMName'];
		$txtDescription = $_POST['txtDescription'];
		$sltUOM = $_POST['sltUOM'];
		$chkLocal = $_POST['chkLocal'];
		$chkActive = $_POST['chkActive'];
		$txtPackaging = $_POST['txtPackaging'];
		$txtAveMonthlyUsage = $_POST['txtAveMonthlyUsage'];

		if(isset($_POST['chkCompounds']))
			$chkCompounds = 1;
		else
			$chkCompounds = 0;
		if(isset($_POST['chkPipes']))
			$chkPipes = 1;
		else
			$chkPipes = 0;
		if(isset($_POST['chkCorporate']))
			$chkCorporate = 1;
		else
			$chkCorporate = 0;
		if(isset($_POST['chkPPR']))
			$chkPPR = 1;
		else
			$chkPPR = 0;
		if(isset($_POST['chkLocal']))
			$chkLocal = 1;
		else
			$chkLocal = 0;
		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;

		if ( $chkCompounds == 0 && $chkPipes == 0 && $chkCorporate == 0 && $chkPPR == 0  ){
			$errmsg_arr[] = "* Division is required.";
			$errflag = true;
		}

		if ( $txtRMName == "" ){
			$errmsg_arr[] = "* RM name can't be blank.";
			$errflag = true;
		}

		if ( !$sltUOM ){
			$errmsg_arr[] = "* Select unit of measures.";
			$errflag = true;
		}

		if ( $txtPackaging == '' ){
			$txtPackaging = NULL;
		}elseif( $txtPackaging != '' && !is_numeric($txtPackaging) ){
			$errmsg_arr[] = "* Packaging must be numeric.";
			$errflag = true;
		}

		if ( $txtAveMonthlyUsage == '' ){
			$txtAveMonthlyUsage = NULL;
		}elseif( $txtAveMonthlyUsage != '' && !is_numeric($txtAveMonthlyUsage) ){
			$errmsg_arr[] = "* Average monthly usage must be numeric.";
			$errflag = true;
		}

		$_SESSION['SESS_RMS_Compounds'] = $chkCompounds;
		$_SESSION['SESS_RMS_Pipes'] = $chkPipes;
		$_SESSION['SESS_RMS_Corporate'] = $chkCorporate;
		$_SESSION['SESS_RMS_PPR'] = $chkPPR;
		$_SESSION['SESS_RMS_RMType'] = $sltRMType;
		$_SESSION['SESS_RMS_RMCode'] = $txtRMCode;
		$_SESSION['SESS_RMS_RMName'] = $txtRMName;
		$_SESSION['SESS_RMS_Description'] = $txtDescription;
		$_SESSION['SESS_RMS_UOM'] = $sltUOM;
		$_SESSION['SESS_RMS_Local'] = $chkLocal;
		$_SESSION['SESS_RMS_Active'] = $chkActive;
		$_SESSION['SESS_RMS_Packaging'] = $txtPackaging;
		$_SESSION['SESS_RMS_AveMonthlyUsage'] = $txtAveMonthlyUsage;

		if($hidRMId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidRMId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];


		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:new_raw_materials.php?id=".$hidRMId."&comp=".$chkCompounds);
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_RawMaterials_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'isssiiiidiiiiissii', $hidRMId, $txtRMCode, $txtRMName, $txtDescription, $sltRMType, $chkActive, $chkLocal
														 , $txtPackaging, $txtAveMonthlyUsage, $sltUOM, $chkCompounds, $chkPipes, $chkCorporate
														 , $chkPPR, $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_raw_materials.php'.'</td><td>'.$processError.' near line 55.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidRMId)
				$_SESSION['SUCCESS']  = 'Successfully updated raw materials.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new raw materials.';
			//echo $_SESSION['SUCCESS'];
			header("location: raw_materials.php?page=".$_SESSION['page']."&name_text=".$_SESSION['name_text']."&code_text=".$_SESSION['code_text']."&type_text=".$_SESSION['type_text']);	
		}

		$db->next_result();
		$result->close(); 

		require("include/database_close.php");
	}
?>