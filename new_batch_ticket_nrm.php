<html>
	<head>
		<script src="js/datetimepicker_css.js"></script> 
		<script src="js/batch_ticket_js.js"></script>
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> <!--   -->
		<?php
			require("/include/database_connect.php"); 
			require("/include/header.php");
			require("/include/init_unset_values/batch_ticket_nrm_init_value.php");

			$BatchTicketID = $_GET['id'];
			$BatchTicketTrialID = $_GET['tid'];
			require("/include/queries_new_batch_ticket_nrm.php");

			if($BatchTicketID)
			{ 
				echo "<title>Batch Ticket - Edit</title>";
			}
			else{
				echo "<title>Batch Ticket - Add</title>";

			}
		?>
	</head>
	<?php
		if ( $BatchTicketID && !$BatchTicketTrialID && $activity_id != 2 && $activity_id != 4 ){
	?>		<body onload='showFormulaType(<?php echo $finished_good_id; ?>, <?php echo $formula_type_id; ?>, <?php echo $BatchTicketID; ?>)
				, showFG(<?php echo $finished_good_id; ?>, <?php echo $BatchTicketID; ?>)
				, showFormula(<?php echo $formula_type_id; ?>, <?php echo $db_BatchTicketTrial; ?>)
				, autoMultiply(), autoSum()'>
	<?php
		}elseif ( $BatchTicketID && $BatchTicketTrialID && $activity_id != 2 && $activity_id != 4 ) {
	?>		<body onload="showFormulaType(<?php echo $finished_good_id; ?>, <?php echo $formula_type_id; ?>, <?php echo $BatchTicketID; ?>)
				, showFG(<?php echo $finished_good_id; ?>, <?php echo $BatchTicketID; ?>)
				, autoMultiply(), autoSum()">
	<?php
		}elseif ( $session_error_indicator_NRM == 1 && $initActivityID_NRM <> 2 ){
	?>		<body onload="autoMultiply(), autoSum()
				, showFG('<?php echo $initFGID_NRM; ?>', '<?php echo $BatchTicketID; ?>')
				, showFormulaType('<?php echo $initFGID_NRM; ?>', '<?php echo $initFormulaTypeID_NRM; ?>', '<?php echo $BatchTicketID; ?>')
				, showFormula('<?php echo $initFormulaTypeID_NRM; ?>', '<?php echo $db_BatchTicketTrial; ?>')">
	<?php
		}else{
	?>
			<body onload="autoMultiply(), autoSum(), showFG('<?php echo $initFGID_NRM; ?>', '<?php echo $BatchTicketID; ?>')">
	<?php		
		}
	?>
				<form method="post" action="process_new_batch_ticket_nrm.php">

					<div class="wrapper">

						<span> <h3> <?php echo ( $BatchTicketID ? "Edit Batch Ticket No. <b>".$batch_ticket_no."</b>" : "New Batch Ticket" ); ?> </h3> </span>	

						<?php
							if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) 
							{
								echo '<ul class="err">';
								foreach($_SESSION['ERRMSG_ARR'] as $msg) {
									echo '<li>'.$msg.'</li>'; 
								}
								echo '</ul>';
								unset($_SESSION['ERRMSG_ARR']);
							}
							elseif( isset($_SESSION['SUCCESS']) )
							{
								echo '<ul id="success">';
								echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
								echo '</ul>';
								unset($_SESSION['SUCCESS']);
								echo '<br><br><br>';
							}
						?>

						<table class="parent_tables_form">
							<tr>
								<td>Request Type:</td>
								<td><b>NRM</b></td>
							</tr>
							<tr>
								<td>Activity:</td>
								<td colspan="4">	
									<select name="sltActivity" id="sltActivity" onchange="showFormulaType(0, 0), disabledOptionFG()">
										<?php
											$qry = "CALL sp_Batch_Ticket_Activity_Dropdown('NRM')";
											$result = mysqli_query( $db, $qry );
											$processError = mysqli_error($db);
										
											if ($processError){
												error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_batch_ticket_nrm.php"."</td><td>".$processError." near line 20.</td></tr>", 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												if ( !$BatchTicketID ){
													echo "<option></option>";
												}
												while($row = mysqli_fetch_assoc($result))
												{
													$id = $row["id"];
													$activity = $row["activity"];
										?>		
													<option value="<?php echo $id; ?>" 
														<?php echo ( $BatchTicketID ? ( $id == $activity_id ? "selected" : "hidden" ) : ( $id == $initActivityID_NRM ? "selected" : "" ) ); ?>>
														<?php echo $activity; ?>
													</option>
										<?php
												}
												$db->next_result();
												$result->close();
											}
										?>
									</select>
								</td>
							</tr>
							<tr valign="bottom" class="spacing">
								<td>NRM Number:</td>
								<td colspan="3">
									<select name="sltNRMNumber" id="sltNRMNumber">
										<?php 
											$nrm = 1;     
											$qry = "CALL sp_NRM_Number_Dropdown(1)";
											$result = mysqli_query($db, $qry);
											$processError = mysqli_error($db);
										
											if ($processError){
												error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_batch_ticket_nrm.php"."</td><td>".$processError." near line 20.</td></tr>", 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{	
												if ( $nrm )
													echo "<option value='0'></option>";
												while($row = mysqli_fetch_assoc($result))
												{
													$nrm_id = $row["nrm_id"];
													$nrm_no = $row["nrm_no"];
										?>			
													<option value="<?php echo $nrm_id; ?>" 
														<?php echo ( $BatchTicketTrialID ? ( $nrm_id == $BTnrm_id ? "selected" : "" ) : ( $nrm_id == $initNRMNumberID_NRM ? "selected" : "" ) ); ?>>
														<?php echo $nrm_no; ?>
													</option>
										<?php					
												}
												$db->next_result();
												$result->close();
											}	
										?>
									</select>
								</td>
							</tr
							<tr>
								<td>Batch Ticket Number:</td>
								<td>
									<input type="text" name="txtBatchTicketNo" value="<?php echo ($BatchTicketID ? $batch_ticket_no : "") ; ?>" <?php echo ( $db_BatchTicketTrial != 1 ? "readOnly" : "" );?>>
								</td>
								<td>FG Type:</td>
								<td>
									<input type="radio" name="radSoft" id="Rigid" value="Rigid" <?php echo ( $BatchTicketID ? ( $fg_soft == 1 ? "disabled" : "checked" ) : ( $initTagSofPVC_NRM == 1 ? "onchange='showFG(0, 0)'" : "checked onchange='showFG(0, 0)'" ) ); ?>>
										<label for='Rigid'>Rigid</label>
										
									<input type='radio' name='radSoft' id='Soft' value='Soft' <?php echo ( $BatchTicketID ? ( $fg_soft == 1 ? "checked" : "disabled" ) : ( $initTagSofPVC_NRM == 1 ? "checked onchange='showFG(0, 0)'" : "onchange='showFG(0, 0)'" ) ); ?>>
										<label for='Soft'>Soft</label>
								</td>
							</tr>
							<tr>
								<td>Batch Ticket Date:</td>
								<td>
									<input type="text" name="txtBatchTicketDate" id="txtBatchTicketDate" value="<?php echo ( $BatchTicketID ? $batch_ticket_date : $initBatchTicketDate_NRM ); ?>" <?php echo ( $BatchTicketID ? "readOnly" : "" ); ?>>
									<?php 
										if ( !$BatchTicketID ){ 
									?>
											<img src="js/cal.gif" onclick="javascript:NewCssCal('txtBatchTicketDate')" style="cursor:pointer" name="picker" />
									<?php 
										} 
									?>
								</td>
								<td>Finished Goods:</td>
								<td>
									<select name="sltFG" id="sltFG" onchange="showFormulaType(this.value, 0)">
										<option value="0"></option>
									</select>
								</td>
							</tr>
							<tr>
								<td>TER No.:</td>
								<td>
									<input type="text" name="txtTERNo" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTter_no : $initTERNumber_NRM ); ?>">
								</td>
								<td>Formula Type:</td>
								<td>
									<select name="sltFormulaType" id="sltFormulaType" <?php echo ( $BatchTicketID ? "" : "onchange='showFormula(this.value, $db_BatchTicketTrial)'" ); ?> >
										<option value="0"></option>
									</select>
								</td>
							</tr>
						</table>

						<div class="trials_wrapper">
							
							<table class="trials_parent_tables_form">
								<tr>
									<td>Trial No.:</td>
									<td>
										<input type="hidden" name="txtTrialNo" id="txtTrialNo" value="<?php echo ( $db_BatchTicketTrial ); ?>">
										<b> <?php echo ( $db_BatchTicketTrial ); ?> </b>
									</td>
								</tr>
								<tr>
									<td>Lot No.:</td>
									<td>
										<input type="text" name="txtLotNo" id="txtLotNo" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTlot_no : $initLotNumber_NRM ); ?>">
									</td>
									<td>Trial Date:</td>
									<td>
										<input type="text" name="txtTrialDate" id="txtTrialDate" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTtrial_date : $initTrialDate_NRM ); ?>">
										<img src="js/cal.gif" onclick="javascript:NewCssCal('txtTrialDate')" style="cursor:pointer" name="picker" />
									</td>
								</tr>
							</table>
							<table class="trials_child_tables_form">

								<colgroup> <col width="150px"></col><col></col><col width="150px"></col> </colgroup>

								<tr class="spacing">
									<td colspan="2"><b>EXTRUSION</b></td>

									<td rowspan="7"></td>

									<td><b>MIXING</b></td>
									<td>
										<input type="checkbox" name="chkEdit" id="chkEdit" onchange="editable()"> 
										<label for="chkEdit">Edit</label>
									</td>
								</tr>
								<tr>
									<td>DAR No.:</td>
									<td>
										<input type="text" name="txtExtDarNo" id="txtExtDarNo" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BText_dar_no : $initExtDarNo_NRM ); ?>">
									</td>
									
									<td>DAR No.:</td>
									<td>
										<input type="text" name="txtMixDarNo" id="txtMixDarNo" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_dar_no : $initMixDarno_NRM ); ?>">
									</td>
								</tr>
								<tr>
									<td>Z1:</td>
									<td>
										<input type="text" name="txtExtZ1" id="txtExtZ1" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BText_z1 : $initExtZ1_NRM ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixParam1" id="txtMixParam1" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_parameter1 : ( $initMixParam1_NRM != NULL ? $initMixParam1_NRM : "0" ) ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixSequence1" id="txtMixSequence1" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_sequence1 : ( $initMixSequence1_NRM != NULL ? $initMixSequence1_NRM : "CMR + CSS" ) ); ?>" readOnly>
									</td>
								</tr>
								<tr>
									<td>Z2:</td>
									<td>
										<input type="text" name="txtExtZ2" id="txtExtZ2" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BText_z2 : $initExtZ2_NRM ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixParam2" id="txtMixParam2" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_parameter2 : ( $initMixParam2_NRM != NULL ? $initMixParam2_NRM : "1" ) ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixSequence2" id="txtMixSequence2" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_sequence2 : ( $initMixSequence2_NRM != NULL ? $initMixSequence2_NRM : "CMP" ) ); ?>" readOnly>
									</td>
								</tr>
								<tr>
									<td>DH:</td>
									<td>
										<input type="text" name="txtExtDH" id="txtExtDH" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BText_dh : $initExtDH_NRM ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixParam3" id="txtMixParam3" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_parameter3 : ( $initMixParam3_NRM != NULL ? $initMixParam3_NRM : "60C" ) ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixSequence3" id="txtMixSequence3" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_sequence3 : ( $initMixSequence3_NRM != NULL ? $initMixSequence3_NRM : "CC + CSL + COL" ) ); ?>" readOnly>
									</td>
								</tr>
								<tr>
									<td>Screw Speed:</td>
									<td>
										<input type="text" name="txtExtScrewSpeed" id="txtExtScrewSpeed" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BText_screw_speed : $initExtScrewSpeed_NRM ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixParam4" id="txtMixParam4" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_parameter4 : ( $initMixParam4_NRM != NULL ? $initMixParam4_NRM : "80C" ) ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixSequence4" id="txtMixSequence4" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_sequence4 : ( $initMixSequence4_NRM != NULL ? $initMixSequence4_NRM : "Open H2O valve" ) ); ?>" readOnly>
									</td>
								</tr>
								<tr>
									<td>Cutter Speed:</td>
									<td>
										<input type="text" name="txtExtCutterSpeed" id="txtExtCutterSpeed" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BText_cutter_speed : $initExtCutterSpeed_NRM ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixParam5" id="txtMixParam5" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_parameter5 : ( $initMixParam5_NRM != NULL ? $initMixParam5_NRM : "D/C" ) ); ?>">
									</td>
									<td>
										<input type="text" name="txtMixSequence5" id="txtMixSequence5" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmix_sequence5 : ( $initMixSequence5_NRM != NULL ? $initMixSequence5_NRM : "50C" ) ); ?>" readOnly>
									</td>
								</tr>
								<tr>
									<td class="spacing">Multiplier:</td>
									<td>
										<input type="text" name="txtMultiplier" id="txtMultiplier" size="5"  onblur="autoMultiply(), autoSum()" onkeyup="autoMultiply(), autoSum()" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $BTmultiplier : $initMultiplier_NRM ); ?>">
									</td>
								</tr>
							</table>

							<?php
								if ( $BatchTicketID && $BatchTicketTrialID ){
							?>
									<table class="trials_child_tables_form" id="standard_formula">
										<tr class="spacing">
											<th colspan="2">Raw Materials</th>
											<th>PHR</th>
											<th>Quantity</th>
										</tr>
										<?php
											$count = count($BTrm_id);
											if ( $BTraw_material_type_id[$count-1] == 0 ){
												$counter = $count - 1;
											}else{
												$counter = $count;
											}
										?>
										<tbody>
											<?php
												for ( $i=0 ; $i<$counter ; $i++ ){
											?>
													<tr>
														<td colspan="2">
															<input <?php echo ($BTphr[$i] == 0 ? "class='excluded'" : ""); ?> type="text" name="txtRM[]" id="txtRM<?php echo $i;?>" value='<?php echo $BTrm[$i]; ?>' readonly size='40'>
															<input type="hidden" name="txtRM_type_id[]" value="<?php echo $BTraw_material_type_id[$i]; ?>">
															<input type="hidden" name="txtRM_id[]" value="<?php echo $BTrm_id[$i]; ?>">
															<input type="hidden" name="hidBatchTicketTrialItemId[]" value="<?php echo $trialItemId[$i]; ?>">
														</td>
														<td>
															<input <?php echo ($BTphr[$i] == 0 ? "class='excluded'" : ""); ?> type="text" name="txtPhr[]" id="txtPhr<?php echo $i;?>" value="<?php echo $BTphr[$i]; ?>" readonly onkeyup="autoMultiply(), autoSum()">
														</td>
														<td>
															<input <?php echo ($BTphr[$i] == 0 ? "class='excluded'" : ""); ?> type="text" name="txtQty[]" id="txtQty<?php echo $i;?>" value="<?php echo $BTqty[$i]; ?>" readonly>
														</td>
													</tr>
											<?php
												}
											?>
										</tbody>
										<?php
											if ( $i == ($count-1) ){
										?>
												<tr class="spacing">
													<td >
														<input type="text" name="txtNRM" value="<?php echo $rm[$i];?>" readOnly>
												  		<input type="hidden" name="txtNRm_type_id" value="<?php echo $rm_type_id[$i];?>">
												  		<input type="hidden" name="txtNRm_id" value="<?php echo $rm_id[$i];?>">
												  		<input type="hidden" name="hidNBatchTicketTrialItemId" value="<?php echo $trialItemId[$i];?>">
													</td>
													<td >
														<input type="text" name="txtNPHR" id="txtNPHR" onchange="autoMultiply(), autoSum()" onkeyup="autoMultiply(), autoSum()" value="<?php echo $phr[$i];?>" readonly>
													
													</td>
													<td >
														<input type="text" name="txtNQuantity" id="txtNQuantity" value="<?php echo $qty[$i];?>" readonly>					
													</td>
												</tr>
							<?php
											}
								}else{
							?>
									<table class="trials_child_tables_form" id="standard_formula">
										 <!-- ##############  FORMULA FOR PRODUCT DEVELOPMENT, EVALUATION W/ REPLICATION, AND SAMPLE PREPARATION (NEW)  ##############  -->
										<tr class="spacing">
											<th>RM Type</th>
											<th>Raw Materials</th>
											<th>PHR</th>
											<th>Quantity</th>
										</tr>
										<tbody>
											<?php
												for ( $i = 0; $i < 15; $i++ ){
											?>
													<tr>
														<td>
															<input type="hidden" name="hidBatchTicketTrialItemId[]" value="0">
															<select name="txtRM_type_id[]" id="txtRM_type_id<?php echo $i;?>" onchange="showRM(this.value, <?php echo $i?>)">
																<?php
																	$qryRT = "CALL sp_RMType_BAT_Dropdown(0)";
																	$resultRT = mysqli_query( $db, $qryRT );
																	$processErrorRT = mysqli_error( $db ) ;
																	if ($processErrorRT){
																		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_batch_ticket.php'.'</td><td>'.$processErrorRT.' near line 327.</td></tr>', 3, "errors.php");
																		header("location: error_message.html");
																	}else{
																		echo "<option></option>";

																		while ( $row = mysqli_fetch_assoc( $resultRT ) ){
																			$RMTypeID = $row['rm_type_id'];
																			$code = $row['code'];

																			echo "<option value='".$RMTypeID."'>".$code."</option>";
																		}
																		$db->next_result();
																		$resultRT->close();
																	}
																?>
															</select>
														</td>
														<td>
															<select name="txtRM_id[]" id="txtRM_id<?php echo $i;?>">
																
															</select>
														</td>
														<td>
															<input type="text" name="txtPhr[]" id="txtPhr<?php echo $i; ?>" onchange="autoMultiply(), autoSum()" onkeyup="autoMultiply(), autoSum()">
															<input type="hidden" name="hidPhr[]" id="hidPhr<?php echo $i; ?>">
														</td>
														<td>
															<input type="text" name="txtQty[]" id="txtQty<?php echo $i; ?>" value="0" readOnly>
														</td>
													</tr>
											<?php
												}
											 ?>
										</tbody>
							<?php 
								} 
							?>
										<tr>
											<td>
												<input type="hidden" name="txtNRm_type_id" value="1000">
											</td>
											<td>
												<input type="hidden" name="txtNRm_id">
												<input type="hidden" name="hidNBatchTicketTrialItemId" value="0">
											</td>
											<td>
												<input type="hidden" name="txtNPHR" id="txtNPHR" value="0">
											</td>
											<td>
												<input type="hidden" name="txtNQuantity" id="txtNQuantity" readonly value="0">
											</td>
										</tr>
										<tr id="total" class="spacing border_top">
											<td colspan="3" align="right"><b>TOTAL</b></td>
											<td>
												<input type="text" name="txtTotalQty" id="txtTotalQty" readonly value="">
											</td>
										</tr>
									</table>

							<!-- ##############  ADDITIONAL TESTS  ############## -->
							<table class="trials_child_tables_form">
								<tr>
									<th colspan="5">Additional Tests:</th>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="chkDynamicMilling" id="DM" <?php echo ( $BatchTicketID ? ( $dynamic_milling ? "checked" : "" ) : ( $initDynamicMilling_NRM ? "checked" : "" ) ); ?> >
											<label for="DM">Dynamic Milling</label>
									</td>
									<td>
										<input type="checkbox" name="chkOilAging" id="OIA" <?php echo ( $BatchTicketID ? ( $oil_aging ? "checked" : "" ) : ( $initOilAging_NRM ? "checked" : "" ) ); ?> >
											<label for="OIA">Oil Aging</label>
									</td>
									<td>
										<input type="checkbox" name="chkOvenAging" id="OVA" <?php echo ( $BatchTicketID ? ( $oven_aging ? "checked" : "" ) : ( $initOvenAging_NRM ? "checked" : "" ) ); ?> >
											<label for="OVA">Oven Aging</label>
									</td>
									<td>
										<input type="checkbox" name="chkStrandInspect" id="Strand" <?php echo ( $BatchTicketID ? ( $strand_inspection ? "checked" : "" ) : ( $initStrandInspect_NRM ? "checked" : "" ) ); ?> >
											<label for="Strand">Strands Inspection</label>
									</td>
									<td>
										<input type="checkbox" name="chkPelletInspect" id="Pellet" <?php echo ( $BatchTicketID ? ( $pellet_inspection ? "checked" : "" ) : ( $initPelletInspect_NRM ? "checked" : "" ) ); ?> >
											<label for="Pellet">Pellet Inspection</label>
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="chkImpactTest" id="IT" <?php echo ( $BatchTicketID ? ( $impact_test ? "checked" : "" ) : ( $initImpactTest_NRM ? "checked" : "" ) ); ?> >
											<label for="IT">Impact Test</label>
									</td>
									<td>
										<input type="checkbox" name="chkStaticHeating" id="SH" <?php echo ( $BatchTicketID ? ( $static_heating ? "checked" : "" ) : ( $initStaticHeating_NRM ? "checked" : "" ) ); ?> >
											<label for="SH">Static Heating</label>
									</td>
									<td>
										<input type="checkbox" name="chkColorChange" id="ColorChange" <?php echo ( $BatchTicketID ? ( $color_change_test ? "checked" : "" ) : ( $initColorChange_NRM ? "checked" : "" ) ); ?> >
											<label for="ColorChange">Color Change Test</label>
									</td>
									<td>
										<input type="checkbox" name="chkWaterImmersion" id="WI" <?php echo ( $BatchTicketID ? ( $water_immersion ? "checked" : "" ) : ( $initWaterImmersion_NRM ? "checked" : "" ) ); ?>>
											<label for="WI">Water Immersion</label>
									</td>
									<td>
										<input type="checkbox" name="chkColdTesting" id="CI" <?php echo ( $BatchTicketID ? ( $cold_inspection ? "checked" : "" ) : ( $initColdTesting_NRM ? "checked" : "" ) ); ?>>
											<label for="CI">Cold Bend Test</label>
									</td>
								</tr>
							<table class="comments_buttons">
								<tr>
									<td valign="top">Remarks:</td>
									<td colspan="3">
										<textarea name="txtRemarks" class="paragraph"><?php
												if ( $BatchTicketID && $BatchTicketTrialID ){
													echo $remarks;
												}else{
													echo $initRemarks_NRM;
												}
										?></textarea>
									</td>
								</tr> 
							</table>

						</div>

						<div class="trial_results_wrapper">
							<table class="trials_child_tables_form">
								<tr>
									<th>Trial No.</th>
									<th>Lot No.</th>
									<th>Trial Date</th>
									<th>New Raw Material</th>
									<th>PHR</th>
									<th>Quantity</th>
									<th></th>
								</tr>
								<?php
									$qryAllTrials = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_Home(?)");
									mysqli_stmt_bind_param($qryAllTrials, "i", $BatchTicketID);
									$qryAllTrials -> execute();
									$resultAllTrials = mysqli_stmt_get_result($qryAllTrials);
									$processErrorAllTrials = mysqli_error($db);

									if(!empty($processErrorAllTrials))
									{
										error_log("<tr><td>".date('F d, Y H:i:s').'</td><td>new_nrm.php'.'</td><td>'.$processErrorAllTrials.' near line 538.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										while($row = mysqli_fetch_assoc($resultAllTrials))
										{
											$bat_id = $row['batch_ticket_id'];
											$bat_trial_id = $row['id'];
											$bat_trial_no = $row['trial_no'];
											$bat_trial_date = $row['trial_date'];
											$bat_lot_no = $row['lot_no'];
											$bat_new_rm = $row['new_rm'];
											$bat_phr = $row['phr'];
											$bat_quantity = $row['quantity'];
											$cancelled = $row['cancelled'];
											$cancelled_reason = $row['cancelled_reason'];
											
											if ( $cancelled == 1 ){
								?>
												<tr>
													<td><?php echo $bat_trial_no;?></td>
													<td colspan='6'>
														<b>Cancelled :</b> 
														<?php echo $cancelled_reason;?>
													</td>
												</tr>
								<?php
											}else{
								?>
												<tr>
													<td> <?php echo $bat_trial_no;?></td>
													<td> <?php echo $bat_lot_no;?></td>
													<td> <?php echo $bat_trial_date;?></td>
													<td> <?php echo $bat_new_rm;?></td>
													<td> <?php echo $bat_phr;?></td>
													<td> <?php echo $bat_quantity;?></td>
													<td>
														<input type='button' value='Edit Trial' onclick="location.href='new_batch_ticket_nrm.php?id=<?php echo $bat_id."&tid=".$bat_trial_id;?>'">
														<input type='button' value='Add Evaluation' onclick="location.href='batch_ticket_testing.php?tid=<?php echo $bat_trial_id;?>'">
														<input type='button' value='Cancel Trial' onclick='cancelBox(<?php echo $row['id'];?>, <?php echo $bat_trial_no;?>)'>
													</td>
												</tr>
								<?php
											}
										}
										$db->next_result();
										$resultAllTrials->close();
									}
								?>
							</table>

						</div>

						<table>
							<tr class="align_bottom">
								<td colspan="2">
									<input type="submit" name="btnSave" value="Save & Next">	
									<?php 
										if ( $_GET["tid"] != 0 ){ 
									?>
											<input type='button' name='btnAdd' value='Add Trial' onclick="location.href='new_batch_ticket_nrm.php?id=<?php echo $BatchTicketID;?>&tid=0'">
									<?php 
										}

										$page = $_SESSION["page"];
										$search = htmlspecialchars($_SESSION["search"]);
										$qsone = htmlspecialchars($_SESSION["qsone"]);
										$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
										$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
										$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
										$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
									?>
									<input type='button' name='btnCancel' value='Cancel' onclick="location.href='batch_ticket.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
									<input type='hidden' name='hidBatchTicketId' value="<?php echo $BatchTicketID;?>">
									<input type='hidden' name='hidBatCreatedAt' value='<?php echo ( $BatchTicketID ? $hidBatCreatedAt : date('Y-m-d H:i:s') );?>'>
									<input type='hidden' name='hidBatCreatedId' value='<?php echo ( $BatchTicketID ? $hidBatCreatedId : $_SESSION['SESS_USER_ID'] ); ?>'>
									<input type='hidden' name='hidBatchTicketTrialId' value="<?php echo $BatchTicketTrialID; ?>">
									<input type='hidden' name='hidBatTrialCreatedAt' value='<?php echo ( $BatchTicketID ? ( $BatchTicketTrialID ? $bat_trial_created_at : date('Y-m-d H:i:s') ) : date('Y-m-d H:i:s') ); ?>'>
									<input type='hidden' name='hidBatTrialCreatedId' value='<?php echo ( $BatchTicketID ? ( $BatchTicketTrialID ? $bat_trial_created_id : $_SESSION['SESS_USER_ID'] ) : $_SESSION['SESS_USER_ID'] ); ?>'>
								</td>
							</tr> 
						</table>

					</div>
					
				</form>
			</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>