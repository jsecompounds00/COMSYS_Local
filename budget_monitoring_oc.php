<html>
	<head>
		<title>Supplies Budget - Home</title>
		<?php
			require("/include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : "");
			$page = ($_GET["page"] ? $_GET["page"] : 1);	
		?>
	</head>
	<body>
		<?php
			require("/include/header.php");
			require("/include/init_unset_values/budget_monitoring_oc_unset_value.php");

			// if( $_SESSION['oc_budget_home'] == false) 
			// {
			// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }

			$_SESSION['page'] = $_GET['page'];
			$_SESSION['search'] = $_GET['search'];
			$_SESSION['qsone'] = $_GET['qsone'];
		?>

		<div class="wrapper">
			
			<span> <h3> Budget Monitoring - Supplies </h3> </span>

			<div class="search_box">

	 			<form method="get" action="budget_monitoring_oc.php">
	 				<input type="hidden" name="page" value="<?php echo $page;?>">
					<table class="search_tables_form">
						<tr>
							<td> Year: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
							<td> Department: </td>
							<td>
								<select name="qsone" value="<?php echo $_GET["qsone"];?>">
				 					<?php
			 							$qryBD = "CALL sp_Budget_Department_Dropdown(0)"; 
										$resultBD = mysqli_query($db, $qryBD);
										$processErrorBD = mysqli_error($db);

										if(!empty($processErrorBD))
										{
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_oc.php"."</td><td>".$processErrorBD." near line 43.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultBD))
											{
												$DDId = $row["DDId"];
												$DDCode = $row["DDCode"];

												if ($DDId == $qsone)
												{
													echo "<option value='".$DDId."' selected>".$DDCode."</option>";
												}
												else echo "<option value='".$DDId."'>".$DDCode."</option>";
											}
											
											$db->next_result();
											$resultBD->close();
										}
				 					?>
								</select>
							</td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php
				 					// if(array_search(205, $session_Permit)){	
				 				?>
				 						<input type="button" name="btnAddBudget" value="Add Budget" onclick="location.href='new_budget_monitoring_oc.php?id=0'">
				 				<?php
									//  	$_SESSION["oc_budget_add"] = true;
									// }else{
									//  	unset($_SESSION["oc_budget_add"]);
									// }
								?>
							</td>
						</tr>
					</table>

	 			</form>

	 		</div>

	 		<?php
				if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_oc.php"."</td><td>".$error." near line 27.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryBudget = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_OC_Home( ?, ?, NULL, NULL )" );
					mysqli_stmt_bind_param( $qryBudget, "ii", $search , $qsone);
					$qryBudget->execute();
					$resultBudget = mysqli_stmt_get_result($qryBudget);
					$total_results = mysqli_num_rows($resultBudget); //return number of rows of result

					$db->next_result();
					$resultBudget->close();

					$targetpage = "budget_monitoring_oc.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qryBudgetLimit = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_OC_Home( ?, ?, ?, ? )" );
					mysqli_stmt_bind_param( $qryBudgetLimit, "iiii", $search, $qsone, $start, $end );
					$qryBudgetLimit->execute();
					$resultBudgetLimit = mysqli_stmt_get_result($qryBudgetLimit);
					$processError3 = mysqli_error($db);	

					if(!empty($processError3))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_oc.php"."</td><td>".$processError3." near line 137.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
				?>
						<table class="home_pages">
							<tr>
								<td colspan="9">
									<?php echo $pagination; ?>
								</td>
							</tr>
							 <tr>
								<th> Year </th>
								<th> Deaprtment </th>
								<th> GL Code </th>
								<th> Proposed (AMT) </th>
								<th> Actual (AMT) </th>
								<th colspan="2"> </th>
							</tr>
							<?php
								while ($row = mysqli_fetch_assoc($resultBudgetLimit)) {
									$BudgetID = $row["BudgetID"];
									$BudgetYear = $row["BudgetYear"];
									$Department = $row["Department"];
									$MiscellaneousType = $row["MiscellaneousType"];
									$ProposedBudget = $row["ProposedBudget"];
									$ActualBudget = $row["ActualBudget"];
							?>
									<tr>
										<td> <?php echo $BudgetYear;?> </td>
										<td> <?php echo $Department;?> </td>
										<td> <?php echo $MiscellaneousType;?> </td>
										<td> <?php echo $ProposedBudget;?> </td>
										<td> 
											<label class="<?php echo ( $ActualBudget > $ProposedBudget ? 'warning_background_stop' : ( $ActualBudget == $ProposedBudget ? "warning_background_slow" : "" ) );?>">
												<?php echo $ActualBudget;?> 
											</label>
										</td>
										<td>
											<?php
							 					// if(array_search(206, $session_Permit)){	
							 				?>
													<input type="button" value="Edit" onclick="location.href='new_budget_monitoring_oc.php?id=<?php echo $BudgetID;?>'">
							 				<?php
												//  	$_SESSION["oc_budget_edit"] = true;
												// }else{
												//  	unset($_SESSION["oc_budget_edit"]);
												// }
											?>
										</td>
										<td>
											<?php
							 					// if(array_search(207, $session_Permit)){	
							 				?>
													<input type="button" value="Distribution" onclick="location.href='budget_monitoring_oc_distribution.php?page=1&search=&qsone=&id=<?php echo $BudgetID;?>'">
							 				<?php
												//  	$_SESSION["oc_budget_distribution"] = true;
												// }else{
												//  	unset($_SESSION["oc_budget_distribution"]);
												// }
											?>
										</td>
									</tr>
							<?php
								}
								$db->next_result();
								$resultBudgetLimit->close();
							?>
							<tr>
								<td colspan="9">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
	