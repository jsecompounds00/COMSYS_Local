<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_permission.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$hidPermitid = $_POST['hidPermitid'];
		$txtPermission = $_POST['txtPermission'];
		$txtModule = $_POST['txtModule'];

		$_SESSION['SESS_PER_Permission'] = $txtPermission;
		$_SESSION['SESS_PER_Module'] = $txtModule;

		if ( $txtPermission == '' ){
			$errmsg_arr[] = "* Permission can't be blank.";
			$errflag = true;
		}
		if ( $txtModule == "" ){
			$errmsg_arr[] = "* Module can't be blank.";
			$errflag = true;
		}

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_PERMISSION.$hidPermitid);
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Permission_CRU(?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'iss', $hidPermitid, $txtPermission, $txtModule);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_permission.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidPermitid)
				$_SESSION['SUCCESS']  = 'Successfully updated permission.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new permission.';
			//echo $_SESSION['SUCCESS'];
			header("location: permission.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}


		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>