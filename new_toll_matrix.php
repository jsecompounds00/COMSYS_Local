<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_toll_matrix.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$tollMatrixId = $_GET['id'];

				if($tollMatrixId)
				{ 					
					$qry = mysqli_prepare($db, "CALL sp_TollMatrix_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $tollMatrixId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_toll_matrix.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$area = $row['area'];
							$vehicle_class = $row['vehicle_class'];
							$entry_point_id = $row['entry_point_id'];
							$exit_point_id = $row['exit_point_id'];
							$entry_point = $row['entry_point'];
							$exit_point = $row['exit_point'];
							$toll_fee = $row['toll_fee'];
							$active = $row['active'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.toll_matrix";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_toll_matrix.php'.'</td><td>'.$processErrorPI.' near line 58.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI))
						{
							$id[] = $row['id'];
						} // end while loop

					} // end if-else processErrorPI

					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($tollMatrixId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_toll_matrix.php</td><td>The user tries to edit a non-existing toll_points_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");

					} //end !in_array
					
					echo "<title>Toll Matrix - Edit</title>";
					
				} // end if tollMatrixId
				else
				{
					echo "<title>Toll Matrix - Add</title>";

				} //end else tollMatrixId

			} // if-else errno
		?>
		<script src="js/jscript.js"></script> 
	</head>
	<body>

		<form method='post' action='process_new_toll_matrix.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if ( $tollMatrixId ){
					if( $_SESSION['edit_matrix'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['add_matrix'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $tollMatrixId ? "Edit " : "New " );?> Toll Matrix </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}// end for loop

						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);

					} // end if isset session error message
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Vehicle Class:</td>
						<td>
							<?php
								if ( $tollMatrixId ){
							?>
									<input type='hidden' name='sltClass' value='<?php echo $vehicle_class;?>'>
									<label class="constant"> Class <?php echo $vehicle_class;?> </label>
							<?php
								}else{
							?>
								<select name="sltClass">
									<option value=''></option>	
									<option value='1'>Class 1</option>
									<option value='2'>Class 2</option>
									<option value='3'>Class 3</option>
								</select>
							<?php 
								} 
							?>
						</td>
					</tr>
					<tr>
						<td>Area:</td>
						<td>
							<?php
								if ( $tollMatrixId ){
							?>	
									<label class="constant"> <?php echo $area;?> </label>
									<input type='hidden' name='sltArea' value='<?php echo $area?>' readonly>
							<?php
								}else{
							?>
								<select name="sltArea" id="sltArea" onchange='showPoints()'>
									<option value=''></option>	
									<option value='NLEX'>NLEX</option>
									<option value='SLEX'>SLEX</option>
									<option value='CAVITEX'>Cavitex</option>
									<option value='SCTEX'>SCTEX</option>
								</select>
							<?php 
								} 
							?>	
						</td>
					</tr>
					<tr>
						<td>Entry Point:</td>
						<td>
							<?php
								if($tollMatrixId){
							?>
									<label class="constant"> <?php echo $entry_point;?> </label>
									<input type='hidden' name='sltEntryPoint' value='<?php echo $entry_point_id;?>' readonly>
							<?php
								}
								else {
							?>
									<select type='text' name='sltEntryPoint' id='sltEntryPoint' onchange='disableOption(this.value)'>
										<option value=''> </option>
									</select>
							<?php
								}
							?>
						</td>
					</tr>
					<tr>
						<td>Exit Point:</td>
						<td>
							<?php
								if($tollMatrixId){
							?>
									<label class="constant"> <?php echo $exit_point;?> </label>
									<input type='hidden' name='sltExitPoint' value='<?php echo $exit_point_id;?>' readonly>
							<?php
								}
								else {
							?>
									<select type='text' name='sltExitPoint' id='sltExitPoint'>
										<option value=''> </option>
									</select>
							<?php
								}
							?>
						</td>
					</tr>
					<tr>
						<td>Toll:</td>
						<td>
							<input type='text' name='txtToll' value='<?php echo ( $tollMatrixId ? $toll_fee : "" );?>'>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type='checkbox' name='chkActive' <?php echo ( $tollMatrixId ? ( $active ? "checked" : "" ) : "" );?>>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<input type="submit" name="btnSaveTollMatrix" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='toll_matrix.php?page=1&search=&qsone='">
							<input type='hidden' name='hidtollMatrixId' value="<?php echo $tollMatrixId;?>">
							<input type='hidden' name='hidCreatedAt' value="<?php echo ( $tollMatrixId ? $createdAt : date('Y-m-d H:i:s') );?>">
							<input type='hidden' name='hidCreatedId' value="<?php echo ( $tollMatrixId ? $createdId : $_SESSION["SESS_USER_ID"] );?>">
						</td>	
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>