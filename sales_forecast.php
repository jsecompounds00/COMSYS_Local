<html>
	<head>
		<title>Sales Forecast - Home</title>
		<script src="js/datetimepicker_css.js"></script>  
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/unset_value.php");
			

			if( $_SESSION['sales_forecast'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Sales Forecast (Monthly) </h3> </span>

			<div class="search_box">
				<form method="get" action="sales_forecast.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_form">
						<tr>
							<td> <label>FG Item.:</label> </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <label>Date:</label> </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>">  </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
							<td> <input type='submit' value='Search'> </td>
							<td> <input type='button' name='btnAddForecast' value='New Forecast' onclick="location.href='new_sales_forecast.php?id=0'"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>sales_forecast.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Sales_Forecast_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'ss', $search, $qsone);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "sales_forecast.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Sales_Forecast_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssii', $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>sales_forecast.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>Date</th>
							    <th>Item Description</th>
							    <th>Quantity</th>
							    <th>Remarks</th>
							    <th></th>
							</tr>
						 	<?php
								$i = 0;
								while( $row = mysqli_fetch_assoc( $result ) )
								{
									$SFID	= $row['SFID'];
									$ForecastDate = $row['ForecastDate'];
									$FG = $row['FG'];
									$Quantity = $row['Quantity'];
									$Remarks = $row['Remarks'];
							?>
									<tr >
										<td> <?php echo $ForecastDate; ?> </td>
										<td> <?php echo $FG; ?> </td>
										<td> <?php echo $Quantity; ?> </td>
										<td> <?php echo $Remarks; ?> </td>
										<td> 
											<input type='button' value='Edit' onclick="location.href='new_sales_forecast.php?id=<?php echo $SFID;?>'">
										</td>
									</tr>
							<?php
									$i++;
								}
							?>
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>