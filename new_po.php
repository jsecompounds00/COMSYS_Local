<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if ( !empty($errno) )
			{
				$error = mysqli_connect_error();
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_po.php"."</td><td>".$error." near line 14.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}else
			{
				$poId = $_GET["id"];

				if ( $poId ){

					$qry = mysqli_prepare($db, "CALL sp_PO_Query( ? )");
					mysqli_stmt_bind_param($qry, "i", $poId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if ( !empty($processError) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_po.php"."</td><td>".$processError." near line 25.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}else{

							$db_POItemId = array();
							$db_PREId = array();
							$db_PREItemId = array();
							$db_Quantity = array();
							$db_UOMId = array();
							$db_SupplyId = array();
							$db_UnitPrice = array();

						while( $row = mysqli_fetch_assoc( $result ) ){
							// $POId = $row["POId"];
							$db_PONumber = $row["PONumber"];
							$db_PODate = $row["PODate"];
							$db_ExpDeliveryDate = $row["ExpDeliveryDate"];
							$db_SupplierId = $row["SupplierId"];
							$db_Remarks = htmlspecialchars($row["Remarks"]);
							// $db_Remarks_array = explode("<br>", $db_Remarks);
							$db_ItemType = $row["ItemType"];
							$db_createdAt = $row["POCreatedAt"];
							$db_createdId = $row["POCreatedId"];
							$Division = $row["Division"];

							$db_POItemId[] = $row["POItemId"];
							$db_PREId[] = $row["PREId"];
							$db_PREItemId[] = $row["PREItemId"];
							$db_Quantity[] = $row["Quantity"];
							$db_UOMId[] = $row["UOMId"];
							$db_SupplyId[] = $row["SupplyId"];
							$db_UnitPrice[] = $row["UnitPrice"];

						}
						$db->next_result();
						$result->close();

						$PREIDsString = implode(",",  array_unique($db_PREId));
					}

					############ .............
					$qryPI = "SELECT id from comsys.po where po_close IS NULL OR po_cancel IS NULL";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_po.php"."</td><td>".$processErrorPI." near line 89.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($poId, $id, TRUE) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_po.php</td><td>The user tries to edit a non-existing or cancelled or closed po_id.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}


					echo "<title>P.O. - Edit</title>";

				}else{

					echo "<title>P.O. - Add</title>";

				}

			}
			require("/include/header.php");
			require("/include/init_unset_values/po_monitoring_init_value.php");
		?>   
		<script src="js/datetimepicker_css.js"></script> 
		<script src="js/jscript.js"></script>  
	</head>
	<body onload="showPOSupplier(<?php echo ( $poId ? $db_SupplierId : $initPOMSupplier );?>), showPONum(<?php echo $poId;?>), showPOItem(<?php echo $poId;?>), autoProduct(), autoAdd()">

		<form method="post" action="process_new_po.php" name="POForm">

			<?php
				// require("/include/header.php");
				// require("/include/init_unset_values/po_monitoring_init_value.php");

				if ( $poId ){
					if( $_SESSION["edit_po"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION["add_po"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $poId ? "Edit PO# ".$db_PONumber : "New P.O." );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">

					<tr>
						<td> Division: </td>

						<td>
							<input type="radio" name="radDivision" id="Compounds" value="Compounds" onchange="showPONum(<?php echo $poId;?>), showPOSupplier(0)"
								<?php echo ( $poId ? ( $Division == "Compounds" ? "checked" : "" ) : ( $initPOMDivision == "Compounds" ? "checked" : "" ) );?>
								<?php echo ( $poId ? ( $Division == "Compounds" ? "" : "disabled" ) : "" );?>> 
								<label for="Compounds"> Compounds </label>

							<input type="radio" name="radDivision" id="Pipes" value="Pipes" onchange="showPONum(<?php echo $poId;?>), showPOSupplier(0)"
								<?php echo ( $poId ? ( $Division == "Pipes" ? "checked" : "" ) : ( $initPOMDivision == "Pipes" ? "checked" : "" ) );?>
								<?php echo ( $poId ? ( $Division == "Pipes" ? "" : "disabled" ) : "" );?>>
								<label for="Pipes"> Pipes </label>

							<input type="radio" name="radDivision" id="Corporate" value="Corporate" onchange="showPONum(<?php echo $poId;?>), showPOSupplier(0)"
								<?php echo ( $poId ? ( $Division == "Corporate" ? "checked" : "" ) : ( $initPOMDivision == "Corporate" ? "checked" : "" ) );?>
								<?php echo ( $poId ? ( $Division == "Corporate" ? "" : "disabled" ) : "" );?>>
								<label for="Corporate"> Corporate </label>

							<input type="radio" name="radDivision" id="PPR" value="PPR" onchange="showPONum(<?php echo $poId;?>), showPOSupplier(0)"
								<?php echo ( $poId ? ( $Division == "PPR" ? "checked" : "" ) : ( $initPOMDivision == "PPR" ? "checked" : "" ) );?>
								<?php echo ( $poId ? ( $Division == "PPR" ? "" : "disabled" ) : "" );?>>
								<label for="PPR"> PPR </label>

							<input type="radio" name="radDivision" id="Others" value="Others" onchange="showPONum(<?php echo $poId;?>), showPOSupplier(0)"
								<?php echo ( $poId ? ( $Division == "Others" ? "checked" : "" ) : ( $initPOMDivision == "Others" ? "checked" : "" ) );?>
								<?php echo ( $poId ? ( $Division == "Others" ? "" : "disabled" ) : "" );?>>
								<label for="Others"> Others </label>
						</td>
					</tr>

					<tr>
						<td> Item Type : </td>

						<td>
							<input type="radio" name="radItemType" id="s" value="s" onchange="showPONum(<?php echo $poId;?>)"
								<?php echo ( $poId ? ( $db_ItemType == "s" ? "checked" : "" ) : ( $initPOMItemType == "s" ? "checked" : "" ) );?>
								<?php echo ( $poId ? ( $db_ItemType == "s" ? "" : "disabled" ) : "" );?>>
								<label for="s">Supplies</label>

							<input type="radio" name="radItemType" id="r" value="r" onchange="showPONum(<?php echo $poId;?>)"
								<?php echo ( $poId ? ( $db_ItemType == "r" ? "checked" : "" ) : ( $initPOMItemType == "r" ? "checked" : "" ) );?>
								<?php echo ( $poId ? ( $db_ItemType == "r" ? "" : "disabled" ) : "" );?>>
								<label for="r">Raw Materials</label>
						</td>
					</tr>

					<tr>
						<td valign="top">PRE No.:</td>

						<td colspan="4">
							<select name="sltPRE[]" id="sltPRE" multiple="true" size="7" onchange="sethidPREIDs(), showPOItem(<?php echo $poId;?>)" <?php echo ( $poId ? "disabled" : "" );?>>
							</select>

							<label class="instruction">
								(Hold down the Ctrl button to select multiple PRE).
							</label>
						</td>
					</tr>

					<tr class="spacing" valign="bottom">
						<td>P.O. No.:</td>

						<td>
							<?php
								if ( $poId )
									echo "<input type='hidden' name='txtPONum' value='".$db_PONumber."'> <b>".$db_PONumber."</b>";
								else
									echo "<input type='text' name='txtPONum' id='txtPONum' value='".$initPOMPONum."' onkeyup='checkDuplicatePO()'>";
							?>
						</td>

						<td>Expected Delivery Date:</td>

						<td>
							<input type="text" name="txtDeliveryDate" id="txtDeliveryDate" value="<?php echo ( $poId ? $db_ExpDeliveryDate : $initPOMDeliveryDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDeliveryDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
		    			<td></td>
		    			<td id='tdPONo' colspan='5'></td>
		    		</tr>

					<tr>
						<td>P.O. Date.:</td>

						<td>
							<input type="text" name="txtPODate" id="txtPODate" value="<?php echo ( $poId ? $db_PODate : $initPOMPODate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtPODate')" style="cursor:pointer" name="picker" />
						</td>

						<td>Supplier:</td>

						<td>
							<select name='sltSupplier' id='sltSupplier'>
							</select>
						</td>
					</tr>
					
				</table>

				<table class="child_tables_form">   	      
					<tr> 
						<?php 
							if( $poId ){
						?> 
							<th> Canceled? </th>
							<th> Close PO </th>
						<?php
							}
						?>
						<th> PRE No. </th>
						<th> Include? </th>
						<th> Quantity </th>
						<th> UoM </th>
						<th> Description </th>
						<th> Unit Price </th>
						<th> Amount </th>
						<?php 
							if( $poId ){
						?> 
							<th> Remarks </th>
						<?php 
							}
						?>
					</tr>

					<tbody id='preItems'>
					</tbody>
				</table>

				<table class="comments_buttons">
					<tr>
						<td valign='top' colspan='2'>Remarks:</td>

						<td>
							<textarea name='txtRemarks'><?php
								if ( $poId ){
									echo $db_Remarks;
								}else{
									echo $initPOMRemarks;
								}
							?></textarea>
						</td>
					</tr>

					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$po_date = htmlspecialchars($_SESSION["po_date"]);
								$po_number = htmlspecialchars($_SESSION["po_number"]);
								$pre_number = htmlspecialchars($_SESSION["pre_number"]);
								$po_date = ( (strpos(($po_date), "\\")+1) > 0 ? str_replace("\\", "", $po_date) : $po_date );
								$po_date = ( (strpos(($po_date), "'")+1) > 0 ? str_replace("'", "\'", $po_date) : $po_date );
								$po_number = ( (strpos(($po_number), "\\")+1) > 0 ? str_replace("\\", "", $po_number) : $po_number );
								$po_number = ( (strpos(($po_number), "'")+1) > 0 ? str_replace("'", "\'", $po_number) : $po_number );
								$pre_number = ( (strpos(($pre_number), "\\")+1) > 0 ? str_replace("\\", "", $pre_number) : $pre_number );
								$pre_number = ( (strpos(($pre_number), "'")+1) > 0 ? str_replace("'", "\'", $pre_number) : $pre_number );
							?>
							<input type="submit" name="btnSavePO" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='po_monitoring.php?page=<?php echo $page;?>&po_date=<?php echo $po_date;?>&po_number=<?php echo $po_number;?>&pre_number=<?php echo $pre_number;?>'">
							<input type='hidden' name='hidPoId' id='hidPoId' value="<?php echo $poId;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $poId ? $db_createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $poId ? $db_createdId : $_SESSION["SESS_USER_ID"] );?>'>
							<input type='hidden' id='hidPREIDs' value='<?php echo ( $poId ? $PREIDsString : $initPOMPREIds );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>