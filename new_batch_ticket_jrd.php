<html>
	<head>
		<script src="js/datetimepicker_css.js"></script> 
		<script src="js/batch_ticket_js.js"></script>
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> <!--   -->

		<?php
			require("/include/database_connect.php");  

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_batch_ticket_jrd.php'.'</td><td>'.$error.' near line 13.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$BatchTicketID = $_GET['id'];
				$BatchTicketTrialID = $_GET['tid'];
				require("/include/queries_new_batch_ticket_jrd.php");

				if ( $BatchTicketID ){
					echo "<title>Batch Ticket - Edit</title>";
				}
				else{

					echo "<title>Batch Ticket - Add</title>";

				}
			}
		?>
	</head>
	<body onload="showJRDData()">

		<form method='post' action='process_new_batch_ticket_jrd.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/batch_ticket_jrd_init_value.php");
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $BatchTicketID ? "Edit Batch Ticket No. <b>".$bat_num."</b>" : "New Batch Ticket" ); ?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);
					}
					elseif( isset($_SESSION['SUCCESS']) ){
						echo '<ul id="success">';
						echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
						echo '</ul>';
						unset($_SESSION['SUCCESS']);
						echo '<br><br><br>';
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Request Type:</td>
						<td>
							<b>JRD</b>
							<input type="hidden" name="hidRequestType" value="JRD">
						</td>
					</tr>
					<tr class="spacing" valign="bottom">
						<td>JRD Number:</td>
						<td>
							<select name="sltJRDNumber" id="sltJRDNumber" onchange="showJRDData()">
							<?php 
								$qryJ = mysqli_prepare($db, "CALL sp_JRD_Number_Dropdown(?)");
								mysqli_stmt_bind_param($qryJ, "i", $BatchTicketID);
								$qryJ->execute();
								$resultJ = mysqli_stmt_get_result($qryJ);
								$processErrorJ = mysqli_error($db);
							
								if ($processErrorJ){
									error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_batch_ticket_jrd.php"."</td><td>".$processErrorJ." near line 20.</td></tr>", 3, "errors.php");
									header("location: error_message.html");
								}
								else
								{	
									while($row = mysqli_fetch_assoc($resultJ))
									{
										$jrd_id = $row["jrd_id"];
										$jrd_number = $row["jrd_number"];
										
										if ( $BatchTicketTrialID ){
											if ( $jrd_id == $db_jrd_id )
												echo "<option value=".$jrd_id." selected>".$jrd_number."</option>";
											else
												echo "<option value=".$jrd_id." >".$jrd_number."</option>";
										}else{
											if ( $jrd_id == $initJRDNumberID_JRD )
												echo "<option value=".$jrd_id." selected>".$jrd_number."</option>";
											else
												echo "<option value=".$jrd_id." >".$jrd_number."</option>";
										}
											
									}
									$db->next_result();
									$resultJ->close();
								}
							?>
							</select>
						</td>
						<td>TER No.:</td>
						<td>
							<input type="text" name="txtTERNo" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_ter_no : $initTERNo_JRD ); ?>">
						</td>
					</tr
					<tr>
						<td>Batch Ticket Number:</td>
						<td>
							<input type="text" name="txtBatchTicketNo" value="<?php echo ($BatchTicketID ? $db_BatchTicketNo : "") ; ?>" <?php echo ( $db_BatchTicketTrial != 1 ? "readOnly" : "" );?>>
						</td>
						<td>Batch Ticket Date:</td>
						<td>
							<input type="text" name="txtBatchTicketDate" id="txtBatchTicketDate" value="<?php echo ( $BatchTicketID ? $db_BatchTicketDate : $initBatchTicketDate_JRD ); ?>" <?php echo ( $BatchTicketID ? "readOnly" : "" ); ?>>
							<?php 
								if ( !$BatchTicketID ){ 
							?>
									<img src="js/cal.gif" onclick="javascript:NewCssCal('txtBatchTicketDate')" style="cursor:pointer" name="picker" /> 
							<?php 
								}
							?>
						</td>
					</tr>
				</table>

				<div id="tbJRDData">
				</div>

				<div class="trial_results_wrapper">
					<table class="trials_child_tables_form">
						<tr>
							<th>Trial No.</th>
							<th>Lot No.</th>
							<th>Trial Date</th>
							<th>PHR</th>
							<th>Quantity</th>
							<th></th>
						</tr>
						<?php
							$qryAllTrials = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_Home(?)");
							mysqli_stmt_bind_param($qryAllTrials, "i", $BatchTicketID);
							$qryAllTrials -> execute();
							$resultAllTrials = mysqli_stmt_get_result($qryAllTrials);
							$processErrorAllTrials = mysqli_error($db);

							if(!empty($processErrorAllTrials))
							{
								error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_jrd.php"."</td><td>".$processErrorAllTrials." near line 538.</td></tr>", 3, "errors.php");
								header("location: error_message.html");
							}else{
								while($row = mysqli_fetch_assoc($resultAllTrials))
								{
									$bat_id = $row["batch_ticket_id"];
									$bat_trial_id = $row["id"];
									$bat_trial_no = $row["trial_no"];
									$bat_trial_date = $row["trial_date"];
									$bat_lot_no = $row["lot_no"];
									$bat_phr = $row["phr"];
									$bat_quantity = $row["quantity"];
									$cancelled = $row["cancelled"];
									$cancelled_reason = $row["cancelled_reason"];
									$JRDReplication = $row["JRDReplication"];
									
									if ( $cancelled == 1 ){
						?>
										<tr>
											<td><?php echo $bat_trial_no;?></td>
											<td colspan="6" >
												<b>Cancelled :</b> 
												<?php echo $cancelled_reason;?>
											</td>
										</tr>
						<?php
									}else{
						?>
										<tr>
											<td><?php echo $bat_trial_no;?></td>
											<td><?php echo $bat_lot_no;?></td>
											<td><?php echo $bat_trial_date;?></td>
											<td><?php echo $bat_phr;?></td>
											<td><?php echo $bat_quantity;?></td>
											<td>
												<input type="button" value="Edit Trial" onclick="location.href='new_batch_ticket_jrd.php?id=<?php echo $bat_id.'&tid='.$bat_trial_id;?>'">
												<input type='button' value='Add Evaluation' onclick="location.href='batch_ticket_testing.php?tid=<?php echo $bat_trial_id;?>'">
												<input type='button' value='Cancel Trial' onclick='cancelBox(<?php echo $row['id'];?>, <?php echo $bat_trial_no;?>)'>
											</td>
										</tr>
						<?php
										}
									}
							}
								$db->next_result();
								$resultAllTrials->close();
						?>
					</table>
				</div>

				<table>
					<tr class="align_bottom">
						<td colspan="2">
							<input type="submit" name="btnSave" value="Save & Next" onclick="enableRM_ID()">	
							<?php
								if ( $_GET["tid"] != 0 ){ 
							?>
									<input type='button' name='btnAdd' value='Add Trial' onclick="location.href='new_batch_ticket_jrd.php?id=<?php echo $BatchTicketID;?>&tid=0'">
							<?php 
								}

								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='batch_ticket.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidBatchTicketId' id='hidBatchTicketId' value="<?php echo $BatchTicketID;?>">
							<input type='hidden' name='hidBatchTicketTrialId' id='hidBatchTicketTrialId' value="<?php echo $BatchTicketTrialID; ?>">
							<input type='hidden' name='hidBatCreatedAt' value='<?php echo ( $BatchTicketID ? $db_hidBatCreatedAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidBatCreatedId' value='<?php echo ( $BatchTicketID ? $db_hidBatCreatedID : $_SESSION['SESS_USER_ID'] ); ?>'>
							<input type='hidden' name='hidBatTrialCreatedAt' value='<?php echo ( $BatchTicketID ? ( $BatchTicketTrialID ? $db_bat_trial_created_at : date('Y-m-d H:i:s') ) : date('Y-m-d H:i:s') ); ?>'>
							<input type='hidden' name='hidBatTrialCreatedId' value='<?php echo ( $BatchTicketID ? ( $BatchTicketTrialID ? $db_bat_trial_created_id : $_SESSION['SESS_USER_ID'] ) : $_SESSION['SESS_USER_ID'] ); ?>'>
						</td>
					</tr> 
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>