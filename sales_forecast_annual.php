<html>
	<head>
		<title>Sales Forecast - Home</title>
		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("include/database_connect.php");

			$fg = ($_GET['fg'] ? "%".$_GET['fg']."%" : "");
			$year = ($_GET['year'] ? $_GET['year'] : NULL);
			$customer = ($_GET['customer'] ? "%".$_GET['customer']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/unset_value.php");

			if( $_SESSION['sales_forecast_annual'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["fg_a"] = $_GET["fg"];
			$_SESSION["year_a"] = $_GET["year"];
			$_SESSION["customer_a"] = $_GET["customer"];
			$_SESSION["page_a"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Sales Forecast </h3> </span>

			<div class="search_box">
				<form method="get" action="sales_forecast_annual.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_form">
						<tr>
							<td> <label>FG Item.:</label> </td>
							<td> <input type="text" name="fg" value="<?php echo htmlspecialchars($_GET["fg"]); ?>"> </td>
							<td> <label>Customer:</label> </td>
							<td> <input type="text" name="customer" value="<?php echo htmlspecialchars($_GET["customer"]); ?>"> </td>
							<td> <label>Year:</label> </td>
							<td> <input type="text" name="year" id="year" value="<?php echo htmlspecialchars($_GET["year"]);?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php
									if(array_search(106, $session_Permit)){
								?>		
										<input type='button' name='btnAddForecast' value='New Forecast' onclick="location.href='new_sales_forecast_annual.php?id=0'">
								<?php
										$_SESSION['new_sales_forecast_annual'] = true;
									}else{
										unset($_SESSION['new_sales_forecast_annual']);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>sales_forecast_annual.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Sales_Forecast_Annual_Home(?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'ssi', $fg, $customer, $year);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "sales_forecast_annual.php"; 	//your file name  (the name of this file)
					require("include/paginate_sales_forecast_annual.php");

					$qry = mysqli_prepare($db, "CALL sp_Sales_Forecast_Annual_Home(?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssiii', $fg, $customer, $year, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>sales_forecast_annual.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='16'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr class='padjustified'>
							    <th>Year</th>
							    <th>Item Description</th>
							    <th>Customer</th>
							    <th>Jan</th>
							    <th>Feb</th>
							    <th>Mar</th>
							    <th>Apr</th>
							    <th>May</th>
							    <th>Jun</th>
							    <th>Jul</th>
							    <th>Aug</th>
							    <th>Sep</th>
							    <th>Oct</th>
							    <th>Nov</th>
							    <th>Dec</th>
							    <th></th>
							</tr>
						 	<?php
								$i = 0;
								while( $row = mysqli_fetch_assoc( $result ) )
								{
									$SFID	= $row['SFID'];
									$ForecastYear = $row['ForecastYear'];
									$FG = $row['FG'];
									$Customer = $row['Customer'];
									$Remarks = $row['Remarks'];
							?>
									<tr>
										<td> <?php echo $ForecastYear; ?> </td>
										<td> <?php echo $FG; ?> </td>
										<td> <?php echo $Customer; ?> </td>
									    <td> <?php echo ( !is_null($row['january']) && $row['january'] != 0 ? $row['january'] : NULL ); ?> </td>
									    <td> <?php echo ( !is_null($row['february']) && $row['february'] != 0 ? $row['february'] : NULL ); ?> </td>
									    <td> <?php echo ( !is_null($row['march']) && $row['march'] != 0 ? $row['march'] : NULL ); ?> </td>
									    <td> <?php echo ( !is_null($row['april']) && $row['april'] != 0 ? $row['april'] : NULL ); ?> </td>
									    <td> <?php echo ( !is_null($row['may']) && $row['may'] != 0 ? $row['may'] : NULL ); ?> </td>
									    <td> <?php echo ( !is_null($row['june']) && $row['june'] != 0 ? $row['june'] : NULL ); ?> </td>
									    <td> <?php echo ( !is_null($row['july']) && $row['july'] != 0 ? $row['july'] : NULL ); ?> </td>
										<td> <?php echo ( !is_null($row['august']) && $row['august'] != 0 ? $row['august'] : NULL ); ?> </td>
										<td> <?php echo ( !is_null($row['september']) && $row['september'] != 0 ? $row['september'] : NULL ); ?> </td>
										<td> <?php echo ( !is_null($row['october']) && $row['october'] != 0 ? $row['october'] : NULL ); ?> </td>
										<td> <?php echo ( !is_null($row['november']) && $row['november'] != 0 ? $row['november'] : NULL ); ?> </td>
										<td> <?php echo ( !is_null($row['december']) && $row['december'] != 0 ? $row['december'] : NULL ); ?> </td>
										 <td> 
											<?php
												if(array_search(107, $session_Permit)){
											?>		
													<input type='button' value='Edit' onclick="location.href='new_sales_forecast_annual.php?id=<?php echo $SFID;?>'">
											<?php
													$_SESSION['edit_sales_forecast_annual'] = true;
												}else{
													unset($_SESSION['edit_sales_forecast_annual']);
												}
											?>
										</td>
									</tr>
							<?php
									$i++;
								}
							?>
							<tr>
								<td colspan='16'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>