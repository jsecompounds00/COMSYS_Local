<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_monitoring.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$ccrID = $_GET['id'];

				if($ccrID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_CCR_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $ccrID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_monitoring.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							$CCRID = $row['id'];
							$ccr_no = $row['ccr_no'];
							$customer_id = $row['customer_id'];
							$complaint_date = $row['complaint_date'];
							$complaint_type_id = $row['complaint_type_id'];
							$complaint_type_others = htmlspecialchars($row['complaint_type_others']);
							$finished_good_id = $row['finished_good_id'];
							$lot_number = htmlspecialchars($row['lot_number']);
							$bag_number = htmlspecialchars($row['bag_number']);
							$complaint = htmlspecialchars($row['complaint']);
							$containment_smd = $row['containment_smd'];
							$containment_tec = $row['containment_tec'];
							$containment_wpd = $row['containment_wpd'];
							$containment_action = htmlspecialchars($row['containment_action']);
							$containment_implementation = htmlspecialchars($row['containment_implementation']);
							$containment_responsible = htmlspecialchars($row['containment_responsible']);
							$root_cause_analysis = htmlspecialchars($row['root_cause_analysis']);
							$root_cause_tec = $row['root_cause_tec'];
							$root_cause_pam = $row['root_cause_pam'];
							$root_cause_wpd = $row['root_cause_wpd'];
							$root_cause_man = $row['root_cause_man'];
							$root_cause_method = $row['root_cause_method'];
							$root_cause_material = $row['root_cause_material'];
							$root_cause_measurement = $row['root_cause_measurement'];
							$root_cause_machine = $row['root_cause_machine'];
							$root_cause_mother_nature = $row['root_cause_mother_nature'];
							$corrective_action = htmlspecialchars($row['corrective_action']);
							$corrective_implementation = htmlspecialchars($row['corrective_implementation']);
							$corrective_responsible = htmlspecialchars($row['corrective_responsible']);
							$preventive_action = htmlspecialchars($row['preventive_action']);
							$preventive_implementation = htmlspecialchars($row['preventive_implementation']);
							$preventive_responsible = htmlspecialchars($row['preventive_responsible']);
							$remarks = htmlspecialchars($row['remarks']);
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];
							$invalid_complaint = $row['invalid_complaint'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.ccr_monitoring";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_monitoring.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($ccrID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_monitoring.php</td><td>The user tries to edit a non-existing ccr_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>CCR Monitoring - Edit</title>";

				}
				else{

					echo "<title>CCR Monitoring - Add</title>";

				}
			}
		?>
		<script src="js/jscript.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body onload="<?php echo ( $ccrID ? "showCCRFGDropdown($ccrID, $finished_good_id)" : "disabledInvalid(),showCCRFGDropdown(0,0)" );?>,enableOtherComplaint()">

		<form method='post' action='process_new_ccr_monitoring.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/ccr_monitoring_init_value.php");

				if ( $ccrID ){
					if( $_SESSION['edit_ccr'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['add_ccr'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $ccrID ? "Edit CCR# ".$ccr_no : "New CCR Monitoring" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<colgroup>
						<col width="180px"></col><col width="350px"></col><col width="180px"></col><col width="350px"></col>
					</colgroup>

					<tr>
						<td>CCR No.:</td>

						<td>
							<input type="text" name="txtCCRNo" value="<?php echo ( $ccrID ? $ccr_no : $initCCMCCRNo );?>">
						</td>

						<td>
							<label for="chkInvalid">Invalid Complaint</label>
						</td>
	
						<td>
							<input type="checkbox" name="chkInvalid" id="chkInvalid" 
								onchange="disabledInvalid()"
								<?php echo ( $ccrID ? ( $invalid_complaint ? "checked" : "" ) : ( $initCCMInvalid ? "checked" : "" ) );?>> 
						</td>

						<td></td>
					</tr>

					<tr>
						<td>Customer Name:</td>
						<td>
							<select name="sltCustomer" id="sltCustomer" onchange="showCCRFGDropdown(0,0)">
								<option></option>
								<?php
									$qryC = "CALL sp_Customer_Dropdown()";
									$resultC = mysqli_query($db, $qryC);
									$processErrorC = mysqli_error($db);

									if ( !empty($processErrorPI) ){
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_ccr_monitoring.php"."</td><td>".$processErrorPI." near line 157.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}else{
										while( $row = mysqli_fetch_assoc($resultC) ){
											if( $ccrID ){
												if ( $row["id"] == $customer_id ){
													echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
												}else{
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
											}else{
												if ( $row['id'] == $initCCMCustomer ){
													echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
												}else{
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
											}
										}
										$db->next_result();
										$resultC->close();
										
									}
								?>
							</select>
						</td>

						<td>Complaint Date:</td>

						<td>
							<input type="text" name="txtCCRDate" id="txtCCRDate" value="<?php echo ( $ccrID ? $complaint_date : $initCCMCCRDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtCCRDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
						<td>Complaint Type:</td>

						<td >
							<select name="sltComplaintType" id="sltComplaintType" onchange="enableOtherComplaint()">
								<?php
									$qryCT = "SELECT * FROM ccr_type";
									$resultCT = mysqli_query($db, $qryCT);
									$processErrorCT = mysqli_error($db);
									if ( !empty($processErrorCT) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_monitoring.php'.'</td><td>'.$processErrorCT.' near line 189.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
											echo "<option></option>";	
										while($row = mysqli_fetch_assoc($resultCT)){
											if ( $ccrID ){
												if ( $complaint_type_id == $row['id'] ){
													echo "<option value='".$row['id']."' selected>".$row['description']."</option>";											
												}else{
													echo "<option value='".$row['id']."'>".$row['description']."</option>";
												}		
											}else{
												if ( $initCCMComplaintType == $row['id'] ){
													echo "<option value='".$row['id']."' selected>".$row['description']."</option>";											
												}else{
													echo "<option value='".$row['id']."'>".$row['description']."</option>";
												}		
											}
										}
										$db->next_result();
										$resultCT->close();
									}
								?>
							</select>

							<label class="instruction">
			    			  	(Please specify other complaint type)
			    			</label>
						</td>

						<td>
							<input type="text" name="txtOtherComplaintType" id="txtOtherComplaintType" value="<?php echo ( $ccrID ? $complaint_type_others : $initCCMOtherComplaintType );?>" readOnly>
						</td>
					</tr>

					<tr>
						<td>Product Name:</td>

						<td>
							<select name="sltProduct_CCR" id="sltProduct">
							</select>
						</td>
					</tr>

					<tr class="spacing" valign="top">
						<td>Lot Number:</td>

						<td>
							<input type="text" name="txtLotNum" value="<?php echo ( $ccrID ? $lot_number : $initCCMLotNum );?>">
						</td>

						<td>Bag Number/s:</td>

						<td>
							<input type="text" name="txtBagNum" value="<?php echo ( $ccrID ? $bag_number : $initCCMBagNum )?>">
						</td>
					</tr>

					<tr valign="top">
						<td> Complaint: </td>

						<td colspan="3">
							<textarea class="paragraph" name="txtComplaint"><?php
								if ( $ccrID ){
									echo $complaint;
								}else{
									echo $initCCMComplaint;
								}
							?></textarea>
						</td>
					</tr>

					<tr>
						<td> Containment Actions: </td>

						<td class="spacing">
							<input type='checkbox' name='chkSalesContainment' id='SalesCont' value='Sales'
								<?php echo ( $ccrID ? ( $containment_smd ? "checked" : "" ) : ( $initCCMSalesContainment ? "checked" : "" ) );?>> 
								<label for='SalesCont'>Sales</label>

							<input type='checkbox' name='chkTechnicalContainment' id='TechnicalCont' value='Technical'
								<?php echo ( $ccrID ? ( $containment_tec ? "checked" : "" ) : ( $initCCMTechnicalContainment ? "checked" : "" ) );?>> 
								<label for='TechnicalCont'>Technical</label>

							<input type='checkbox' name='chkWPDContainment' id='WPDCont' value='WPD'
								<?php echo ( $ccrID ? ( $containment_wpd ? "checked" : "" ) : ( $initCCMWPDContainment ? "checked" : "" ) );?>> 
								<label for='WPDCont'>WPD</label>
						</td>
					</tr>

					<tr>
						<td rowspan="2"></td>
						<td colspan="3">
							<textarea class="paragraph" name='txtContainment'><?php
								if ( $ccrID ){
									echo $containment_action;
								}else{
									echo $initCCMContainment;
								}
							?></textarea>
						</td>
					</tr>

					<tr>
						<td colspan='2'>
							Implementation Date:
							<input type="text" name="txtImplementContainment" value="<?php echo ( $ccrID ? $containment_implementation : $initCCMImplementContainment )?>">
						</td>

						<td colspan="2">
							Person Responsible:
							<input type="text" name="txtResponsibleContainment" value="<?php echo ( $ccrID ? $containment_responsible : $initCCMResponsibleContainment );?>">
						</td>
					</tr>

					<tr class="spacing">
						<td> Root Cause Analysis: </td>

						<td>
							<input type="checkbox" name="chkTechnicalRootCause" id="TechnicalRoot" value="Technical"
								<?php echo ( $ccrID ? ( $root_cause_tec ? "checked" : "" ) : ( $initCCMTechnicalRootCause ? "checked" : "" ) );?>>  
								<label for="TechnicalRoot"> Technical </label>

							<input type="checkbox" name="chkPAMRootCause" id="PAMRoot" value="PAM"
								<?php echo ( $ccrID ? ( $root_cause_pam ? "checked" : "" ) : ( $initCCMPAMRootCause ? "checked" : "" ) );?>>  
								<label for="PAMRoot"> PAM </label>

							<input type="checkbox" name="chkWPDRootCause" id="WPDRoot" value="WPD"
								<?php echo ( $ccrID ? ( $root_cause_wpd ? "checked" : "" ) : ( $initCCMWPDRootCause ? "checked" : "" ) );?>>  
								<label for="WPDRoot"> WPD </label>
						</td>
					</tr>

					<tr>
						<td rowspan="2"></td>

						<td colspan="3">
							<input type="checkbox" name="chkMan" id="Man"
								<?php echo ( $ccrID ? ( $root_cause_man ? "checked" : "" ) : ( $initCCMMan ? "checked" : "" ) );?>>  
								<label for="Man">Man</label>

							<input type="checkbox" name="chkMethod" id="Method"
								<?php echo ( $ccrID ? ( $root_cause_method ? "checked" : "" ) : ( $initCCMMethod ? "checked" : "" ) );?>>  
								<label for="Method">Method</label>

							<input type="checkbox" name="chkMachine" id="Machine"
								<?php echo ( $ccrID ? ( $root_cause_machine ? "checked" : "" ) : ( $initCCMMachine ? "checked" : "" ) );?>>  
								<label for="Machine">Machine</label>

							<input type="checkbox" name="chkMaterial" id="Material"
								<?php echo ( $ccrID ? ( $root_cause_material ? "checked" : "" ) : ( $initCCMMaterial ? "checked" : "" ) );?>> 
								<label for="Material">Material</label>

							<input type="checkbox" name="chkMeasurement" id="Measurement"
								<?php echo ( $ccrID ? ( $root_cause_measurement ? "checked" : "" ) : ( $initCCMMeasurement ? "checked" : "" ) );?>> 
								<label for="Measurement">Measurement</label>

							<input type="checkbox" name="chkMotherNature" id="MotherNature"
								<?php echo ( $ccrID ? ( $root_cause_mother_nature ? "checked" : "" ) : ( $initCCMMotherNature ? "checked" : "" ) );?>> 
								<label for="MotherNature">Mother Nature</label>
						</td>
					</tr>
						
					<tr>
						<td colspan="3">
							<textarea class="paragraph" name="txtRootCause" id="txtRootCause"><?php
								if ( $ccrID ){
									echo $root_cause_analysis;
								}else{
									echo $initCCMRootCause;
								}
							?></textarea>
						</td>
					</tr>

					<tr valign="top">
						<td rowspan="2"> Corrective Actions: </td>

						<td colspan="3">
							<textarea class="paragraph" name="txtCorrective" id="txtCorrective"><?php
								if ( $ccrID ){
									echo $corrective_action;
								}else{
									echo $initCCMCorrective;
								}
							?></textarea>
						</td>
					</tr>

					<tr class="spacing" valign="top">
						<td colspan="2">
							Implementation Date:
							<input type="text" name="txtImplementCorrective" id="txtImplementCorrective" value="<?php echo ( $ccrID ? $corrective_implementation : $initCCMImplementCorrective );?>">
						</td>

						<td colspan="2">
							Person Responsible:
							<input type="text" name="txtResponsibleCorrective" id="txtResponsibleCorrective" value="<?php echo ( $ccrID ? $corrective_responsible : $initCCMResponsibleCorrective );?>">
						</td>
					</tr>

					<tr valign="top">
						<td rowspan="2"> Preventive Actions: </td>

						<td colspan="3">
							<textarea class="paragraph" name="txtPreventive" id="txtPreventive"><?php
								if ( $ccrID ){
									echo $preventive_action;
								}else{
									echo $initCCMPreventive;
								}
							?></textarea>
						</td>
					</tr>

					<tr class="spacing" valign="top">
						<td colspan='2'>
							Implementation Date:
							<input type="text" name="txtImplementPreventive" id="txtImplementPreventive" value="<?php echo ( $ccrID ? $preventive_implementation : $initCCMImplementPreventive );?>">
						</td>

						<td colspan="2">
							Person Responsible:
							<input type="text" name="txtResponsiblePreventive" id="txtResponsiblePreventive" value="<?php echo ( $ccrID ? $preventive_responsible : $initCCMResponsiblePreventive );?>">
						</td>
					</tr>

					<tr valign="top">
						<td> Remarks: </td>
						<td colspan="3">
							<textarea class="paragraph" name="txtRemarks"><?php
								if ( $ccrID ){
									echo $remarks;
								}else{
									$initCCMRemarks;
								}
							?></textarea>
						</td>
					</tr>

					<tr class="align_bottom">
						<td >
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='ccr_monitoring.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidCCRID' value="<?php echo $ccrID;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $ccrID ? $created_at : "" );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $ccrID ? $created_id : "" );?>'>
						</td>
					</tr>
					
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>