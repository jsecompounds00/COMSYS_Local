<?php
	session_start();
	// if( $_SESSION['cancel_trial'] == false) 
	// {
	// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
	// 	session_write_close();
	// 	header("Location:comsys.php");
	// 	exit();
	// }	

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require("include/database_connect.php");
	require ("include/constant.php");

	$id = intval($_GET['id']);
	$reason = strval($_GET['reason']);
	$date_cancelled = date('Y-m-d H:i:s');
	$cancel_id = $_SESSION['SESS_USER_ID'];
	$cancel=1;
	
	$qry = mysqli_prepare($db, "CALL sp_Trial_Cancel(?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iissi', $id, $cancel, $reason, $date_cancelled, $cancel_id);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_cancel_trial.php'.'</td><td>'.$processError.' near line 32.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS'] = 'Succesfully cancelled.';
			header("location: batch_ticket.php?page=1&search=&qsone=");
		}
			require("include/database_close.php");
?>