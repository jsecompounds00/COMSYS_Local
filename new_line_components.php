<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_line_components.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$componentID = $_GET['id'];

				if($componentID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Line_Component_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $componentID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_line_components.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
							$LineComponentCapacityID = array();
							$MachineID = array();
							$RatedCapacity = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$LineComponentID = $row['LineComponentID'];
							$ComponentName = $row['ComponentName'];
							$MachineType = $row['MachineType'];
							$active = $row['Active'];
							$remarks = $row['Remarks'];
							$CreatedAt = $row['CreatedAt'];
							$CreatedID = $row['CreatedID'];

							$LineComponentCapacityID[] = $row['LineComponentCapacityID'];
							$MachineID[] = $row['MachineID'];
							$RatedCapacity[] = $row['RatedCapacity'];

						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.line_components";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_line_components.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($componentID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_line_components.php</td><td>The user tries to edit a non-existing component_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Line Components - Edit</title>";
				}
				else{

					echo "<title>Line Components - Add</title>";

				}
			}
		?>
		<script src="js/jscript.js"></script> 
	</head>
	<body onload='showMachine()'>

		<form method='post' action='process_new_line_components.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $componentID ? "Edit ".$ComponentName : "New Line Component " );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<colgroup> <col width="200px"></col> </colgroup>
					<tr>
						<td>Component Name:</td>
						<td>
							<input type='text' name='txtName' value='<?php echo ( $componentID ? $ComponentName : $Name );?>'>
						</td>
					</tr>
					<tr>
						<td>Machine Type:</td>
						<td>
							<input type='radio' name='radMachineType' id='M' value='M' onchange='showMachine()'
								<?php if ( $componentID ) echo "onchange='showMachine()'";?>
								<?php echo ( $componentID ? ( $MachineType == "Mixer" ? "checked" : "disabled" ) : "" );?>>
								<label for='M'>Mixer</label>

							<input type='radio' name='radMachineType' id='E' value='E' onchange='showMachine()'
								<?php if ( $componentID ) echo "onchange='showMachine()'";?>
								<?php echo ( $componentID ? ( $MachineType == "Extruder" ? "checked" : "disabled" ) : "checked" );?>>
								<label for='E'>Extruder</label>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type='checkbox' name='chkActive' <?php echo ( $componentID ? ( $active ? "checked" : "" ) : ( $Active ? "checked" : "" ) );?>>
						</td>
					</tr>
				</table>
				<table class="parent_tables_form" <?php if ( !$componentID ) echo "id='MachineLines'";?>>
					<?php
						if ( $componentID ){
					?>
							<colgroup> <col width="200px"></col> </colgroup>
							<tr class="spacing">
								<td><b>Rated Capacity</b></td>
							</tr>
							<?php
								if( $MachineType == 'Mixer' ){
									$qry = "CALL sp_Mixer_Dropdown()";
								}elseif( $MachineType == 'Extruder' ){
									$qry = "CALL sp_Extruder_Dropdown()";
								}

								$result = mysqli_query($db, $qry);
								$processError = mysqli_error($db);

								if(!empty($processError))
								{
									error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>machine_dropdown.php'.'</td><td>'.$processError.' near line 26.</td></tr>', 3, "errors.php");
									header("location: error_message.html");
								}
								else
								{
									$i = 0;

									while($row = mysqli_fetch_assoc($result)){
										$RatedCapacity[$i] = ( $RatedCapacity[$i] == 0 ? '' : $RatedCapacity[$i] );
							?>
										<tr>
											<td>
												<?php echo $row['name'];?>
												<input type='hidden' name='hidLineComponentCapacityID[]' value='<?php echo $LineComponentCapacityID[$i];?>'>
												<input type='hidden' name='hidMachineID[]' value='<?php echo $row['id'];?>'>
											</td>
											<td>
												<input type='text' name='txtRatedCapacity[]' value='<?php echo $RatedCapacity[$i];?>'>
											</td>
										</tr>
							<?php
										$i++;
									}
									$db->next_result();
									$result->close();

								}
						}
					?>
				</table>
				<table class="comments_buttons">
					<colgroup> <col width="200px"></col> </colgroup>
					<tr>
						<td valign='top'>Remarks:</td>
						<td>
							<textarea name='txtRemarks'><?php
								if ( $componentID ){
									$remarks_array = explode("<br>", $remarks);

									foreach ($remarks_array as $rcomponentIDemarks_key => $remarks_value) {
										echo $remarks_value."\n";
									}
								}else{
									$Remarks_array = explode("<br>", $Remarks);

									foreach ($Remarks_array as $Remarks_key => $Remarks_value) {
										echo $Remarks_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" name="btnSaveComponent" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='line_components.php?page=1&search=&qsone='">
							<input type='hidden' name='hidComponentID' value="<?php echo $componentID;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $componentID ? $CreatedAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $componentID ? $CreatedID : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>