<?php
########## Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_order.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidSalesID = $_POST['hidSalesID'];
		$sltSOType = $_POST['sltSOType'];
		$txtSONumber = $_POST['txtSONumber'];
		$txtSODate = $_POST['txtSODate'];
		$txtPONumber = $_POST['txtPONumber'];
		$txtPODate = $_POST['txtPODate'];
		$sltCustomer = $_POST['sltCustomer'];
		$txtRemarks = $_POST['txtRemarks'];
		$sltLCNumber = $_POST['sltLCNumber'];

########## Input Validations
		if ( !$sltSOType ){
			$errmsg_arr[] = '* Select SO type.';
			$errflag = true;
		}
		if ( !$sltCustomer ){
			$errmsg_arr[] = '* Select customer.';
			$errflag = true;
		}
		if ( $txtSONumber == '' ){
			$errmsg_arr[] = '* SO number cannot be blank.';
			$errflag = true;
		}
		$valtxtSODate = validateDate($txtSODate, 'Y-m-d');
		if ( $valtxtSODate != 1 ){
			$errmsg_arr[] = '* Invalid SO date.';
			$errflag = true;
		}
		if ( $txtPONumber == '' ){
			$errmsg_arr[] = '* PO number cannot be blank.';
			$errflag = true;
		}
		$valtxtPODate = validateDate($txtPODate, 'Y-m-d');
		if ( $valtxtPODate != 1 ){
			$errmsg_arr[] = '* Invalid PO date.';
			$errflag = true;
		}

		if($hidSalesID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidSalesID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

########## SALES ORDER ITEMS

		$hidSalesItemId = $_POST["hidSalesItemId"];
		$sltItemType = $_POST["sltItemType"];
		$sltProduct = $_POST["sltProduct"];
		$txtQuantity = $_POST["txtQuantity"];
		$txtUnitPrice = $_POST["txtUnitPrice"];
		$txtRequiredDate = $_POST["txtRequiredDate"];	

		$chkCloseSOItem = $_POST["chkCloseSOItem"];
		$txtCloseQuantity = $_POST["txtCloseQuantity"];	
		$txtCloseReason = $_POST["txtCloseReason"];	
		$hidClosedBy = $_POST["hidClosedBy"];	
		$hidClosedAt = $_POST["hidClosedAt"];

		// $chkCloseSOItem = ( isset($chkCloseSOItem) ? 1 : 0 )."<br>";

		foreach ( $sltProduct as $product_array => $product_value )	{

			if ( $hidSalesItemId[$product_array] ){
				$qrySI = "SELECT * FROM sales_order_items WHERE id = ".$hidSalesItemId[$product_array];
				$resultSI = mysqli_query( $db, $qrySI );
				while( $rowSI = mysqli_fetch_assoc( $resultSI ) ){
					$closed_item_quantity = $rowSI["closed_item_quantity"];
				}
				$db->next_result();
				$resultSI->close();
			}

			if( $product_value ) {

				if ( $txtQuantity[$product_array] == "" ) {
					$errmsg_arr[] = "* Quantity can't be blank (line ".($product_array+1).").";
					$errflag = true;
				}elseif( !is_numeric( $txtQuantity[$product_array] ) ){
					$errmsg_arr[] = "* Quantity must be numeric (line ".($product_array+1).").";
					$errflag = true;
				}

				if ( $txtUnitPrice[$product_array] == "" ) {
					$errmsg_arr[] = "* Unit price can't be blank (line ".($product_array+1).").";
					$errflag = true;
				}elseif( !is_numeric( $txtUnitPrice[$product_array] ) ){
					$errmsg_arr[] = "* Unit price must be numeric (line ".($product_array+1).").";
					$errflag = true;
				}

				if ( isset( $chkCloseSOItem[$product_array] ) ){

					if ( $txtCloseQuantity[$product_array] == "" ){
						$errmsg_arr[] = "* Must specify quantity to be closed (line ".($product_array+1).").";
						$errflag = true;
					}elseif ( ( $txtCloseQuantity[$product_array] != "" ) && ( !is_numeric( $txtCloseQuantity[$product_array] ) ) ) {
						$errmsg_arr[] = "* Quantity to be closed must be numeric (line ".($product_array+1).").";
						$errflag = true;
					}

					if ( $txtCloseReason[$product_array] == "" ){
						$errmsg_arr[] = "* Reason of closure can't be blank (line ".($product_array+1).").";
						$errflag = true;
					}

				}else{
					$chkCloseSOItem[$product_array] = 0;
					$txtCloseQuantity[$product_array] = NULL;
					$txtCloseReason[$product_array] = NULL;
					$hidClosedBy[$product_array] = NULL;
					$hidClosedAt[$product_array] = NULL;
				}

			}else{

				if ( ( $txtQuantity[$product_array] != "" ) || ( $txtUnitPrice[$product_array] != "" ) ) {
					$errmsg_arr[] = "* Select product (line ".($product_array+1).").";
					$errflag = true;
				}

			}// if( $product_value ) else

		}// foreach ( $sltProduct as $product_array => $product_value )

############# SESSION, keeping last input value
		// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);

		// $txtRemarks = str_replace('\\', '', $txtRemarks);

		$hidTag = $_POST['hidTag'];

		$_SESSION['counter'] = $ctr;
		$_SESSION['sltSOType'] = $sltSOType;
		$_SESSION['sltCustomer'] = $sltCustomer;
		$_SESSION['txtSONumber'] = htmlspecialchars($txtSONumber);
		$_SESSION['txtSODate'] = htmlspecialchars($txtSODate);
		$_SESSION['txtPONumber'] = htmlspecialchars($txtPONumber);
		$_SESSION['txtPODate'] = htmlspecialchars($txtPODate);
		$_SESSION['txtRemarks'] = htmlspecialchars($txtRemarks);

		$_SESSION['hidTag'] = $hidTag;

		foreach ($_POST['sltProduct'] as $prodKey => $prodValue) {
			$_SESSION['sltProduct'][$prodKey] = $prodValue;
			$_SESSION['txtQuantity'][$prodKey] = htmlspecialchars($txtQuantity[$prodKey]);
			$_SESSION['txtUnitPrice'][$prodKey] = htmlspecialchars($txtUnitPrice[$prodKey]);
			$_SESSION['txtRequiredDate'][$prodKey] = htmlspecialchars($txtRequiredDate[$prodKey]);

			$_SESSION['chkCloseSOItem'][$prodKey] = $chkCloseSOItem[$prodKey];
			$_SESSION['txtCloseQuantity'][$prodKey] = htmlspecialchars($txtCloseQuantity[$prodKey]);
			$_SESSION['txtCloseReason'][$prodKey] = htmlspecialchars($txtCloseReason[$prodKey]);
			$_SESSION['hidClosedBy'][$prodKey] = $hidClosedBy[$prodKey];
			$_SESSION['hidClosedAt'][$prodKey] = $hidClosedAt[$prodKey];
		}

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_sales_order.php?id=$hidSalesID");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Sales_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isssssisssiii', $hidSalesID, $sltSOType, $txtSONumber, $txtSODate, $txtPONumber
											   , $txtPODate, $sltCustomer, $txtRemarks, $createdAt, $updatedAt
											   , $createdId, $updatedId, $sltLCNumber);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_order.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidSalesID ) {
				$SalesID = $hidSalesID;
			}else{
				$SalesID = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['sltProduct'] as $key => $temValue)
			{	
				if (!$hidSalesID) {
					$SalesItemId = 0;
				}

				if ($temValue)
				{

					$qryItems = mysqli_prepare($db, "CALL sp_Sales_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iisiddsisids', $hidSalesItemId[$key], $SalesID, $sltItemType[$key], $sltProduct[$key]
																, $txtQuantity[$key], $txtUnitPrice[$key], $txtRequiredDate[$key]
																, $chkCloseSOItem[$key], $hidClosedBy[$key], $hidClosedAt[$key]
																, $txtCloseQuantity[$key], $txtCloseReason[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_order.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidSalesID )
							$_SESSION['SUCCESS']  = 'Sales order successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Sales order successfully added.';
						header("location: sales_order.php?page=".$_SESSION['page']."&so_no=".$_SESSION['so_no']."&so_date".$_SESSION['so_date']."=&so_type=".$_SESSION['so_type']);
					}				
				}
			}
		}
	}	
############# Committing in Database

		require("include/database_close.php");

	}
?>