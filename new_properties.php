<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_properties.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$propertyID = $_GET['id'];

				if($propertyID)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Company_Properties_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $propertyID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_properties.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							$property_type_id = $row['property_type_id'];
							$property_name = htmlspecialchars($row['property_name']);
							$unit_of_measure_id = $row['unit_of_measure_id'];
							$db_compounds = $row['compounds'];
							$db_pipes = $row['pipes'];
							$db_corporate = $row['corporate'];
							$db_ppr = $row['ppr'];
							$comments = htmlspecialchars($row['comments']);
							$active = $row['active'];
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.company_properties";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_properties.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($propertyID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_properties.php</td><td>The user tries to edit a non-existing property_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Asset - Edit</title>";

				}
				else{

					echo "<title>Asset - Add</title>";

				}
			}
		?>
	</head>
	<body>

		<form method='post' action='process_new_properties.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if ( $propertyID ){
					if( $_SESSION['property_edit'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['property_add'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $propertyID ? "Edit ".$property_name : "New Asset" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Asset Type:</td>
						<td>
							<select name='sltPropertyType'>
								<?php
									if($propertyID){
										$qryST = "CALL sp_PropertyType_Dropdown(0, 2)";
									}else {
										$qryST = "CALL sp_PropertyType_Dropdown(0, 1)";
									}
									
									$resultST = mysqli_query($db, $qryST);
									$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_properties.php'.'</td><td>'.$processError1.' near line 115.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										if ( !$propertyID ){
											echo "<option value='0'></option>";
										}

										while($row = mysqli_fetch_assoc($resultST))
										{
											$propertyTypeID = $row['property_type_id'];
											$propertyTypeDd = $row['property_type'];
											//$active = $row['active'];

											if ( $propertyID ){
												if ($propertyTypeID == $property_type_id){
													echo "<option value=".$propertyTypeID." selected>".$propertyTypeDd."</option>";
												}else {
													echo "<option value=".$propertyTypeID.">".$propertyTypeDd."</option>";
												}
											}else{
												if ($propertyTypeID == $PropertyType){
													echo "<option value=".$propertyTypeID." selected>".$propertyTypeDd."</option>";
												}else {
													echo "<option value=".$propertyTypeID.">".$propertyTypeDd."</option>";
												}
											}
										}

										$db->next_result();
										$resultST->close();
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Asset Name:</td>
						<td>
							<input type="text" name="txtPropertyName" value="<?php echo ( $propertyID ? $property_name : $PropertyName );?>">
						</td>
					</tr>
					<tr>
						<td>UoM:</td>
						<td>
							<select name="sltUom">
								<?php
									$qryU = "CALL sp_UOM_Dropdown(0)"; //with default blank option for new supply
									$resultU = mysqli_query($db, $qryU);
									$processError2 = mysqli_error($db);

									if(!empty($processError2))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_supply.php'.'</td><td>'.$processError2.' near line 238.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										if ( !$propertyID ){
											echo "<option value='0'></option>";
										}

										while($row = mysqli_fetch_assoc($resultU))
										{
											$uomID = $row['id'];
											$uomDd = $row['code'];

											if( $propertyID ){
												if ( $uomID == $unit_of_measure_id ){
													echo "<option value=".$uomID." selected>".$uomDd."</option>";
												}else {
													echo "<option value=".$uomID.">".$uomDd."</option>";
												}
											}else{
												if ( $uomID == $Uom ){
													echo "<option value=".$uomID." selected>".$uomDd."</option>";
												}else {
													echo "<option value=".$uomID.">".$uomDd."</option>";
												}
											}
										}
										
										$db->next_result();
										$resultU->close();
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Applicable To:</td>
						<td colspan="3">
							<input type="checkbox" name="chkCmpds" id="chkCmpds" 
								<?php echo ( $propertyID ? ( $db_compounds ? "checked" : "" ) : "" );?>>
								<label for="chkCmpds">Compounds</label>

							<input type="checkbox" name="chkPips" id="chkPips"
								<?php echo ( $propertyID ? ( $db_pipes ? "checked" : "" ) : "" );?>>
								<label for="chkPips">Pipes</label>

							<input type="checkbox" name="chkCorp" id="chkCorp"
								<?php echo ( $propertyID ? ( $db_corporate ? "checked" : "" ) : "" );?>>
								<label for="chkCorp">Corporate</label>

							<input type="checkbox" name="chkPPR" id="chkPPR"
								<?php echo ( $propertyID ? ( $db_ppr ? "checked" : "" ) : "" );?>>
								<label for="chkPPR">PPR</label>		
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type="checkbox" name="chkActive" <?php echo ( $propertyID ? ( $active ? "checked" : "" ) : ( $Active ? "checked" : "" ) );?>>
						</td>
					</tr>
					<tr>
						<td valign="top">Comments:</td>
						<td>
							<textarea name="txtComments"><?php
								if ( $propertyID ){
									$comments_array = explode("<br>", $comments);

									foreach($comments_array as $key => $comments_value){
										echo $comments_value."\n";
									}
								}else{
									$Comments_array = explode("<br>", $Comments);
									foreach($Comments_array as $key => $Comments_value){
										echo $Comments_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='property_transfer.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidPropertyID" value="<?php echo $propertyID;?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $propertyID ? $created_at : date('Y-m-d H:i:s') ); ?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $propertyID ? $created_id : $_SESSION["SESS_USER_ID"] ); ?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>