<html>
	<head>
		<script src="js/jscript.js"></script>
  		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_memo.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$memoId = $_GET['id'];

				if($memoId)
				{ 
					$transType = $_GET['type'];

					echo "<title>Debit Memo - Edit</title>";

					$qry = mysqli_prepare($db, "CALL sp_Debit_Memo_Query( ? )");
					mysqli_stmt_bind_param($qry, 'i', $memoId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if ( !empty($processError) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_memo.php'.'</td><td>'.$processError.' near line 12.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						while($row = mysqli_fetch_assoc($result)){
							$customer_id = $row['customer_id'];
							$reference_date = $row['reference_date'];
							$reference_number = htmlspecialchars($row['reference_number']);
							$invoice_number = $row['invoice_number'];
							$amount = $row['amount'];
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];
							$name = $row['name'];
						}
						$db->next_result();
						$result->close();
					}

					############ .............
					$qryPI = "SELECT id from comsys.debit_memo";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_memo.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($memoId, $id, TRUE) && $transType == 'dm' ){
						if ( $transType == 'cm' ){
							header("location: credit_memo.php?id=".$memoId."&type=cm");
						}elseif ( $transType == 'cp' ){
							header("location: credit_payment.php?id=".$memoId."&type=cp");
						}elseif ( $transType == 'di' ){
							header("location: debit_invoice.php?id=".$memoId."&type=di");
						}else{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_memo.php</td><td>The user tries to edit a non-existing jacket_id.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
					}

				}
				else
				{
				
					echo "<title>Debit Memo - Add</title>";
				}
			}
				require("/include/header.php");
				require("/include/init_value.php");
		?>
	</head>
	<body onload="showInvoiceNumber('<?php echo ( $memoId ? $invoice_number : $InvoiceNo );?>')">

		<form method='post' action='process_debit_memo.php'>

			<?php 
				if ( $memoId ){
					if( $_SESSION['edit_debit_memo'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['debit_memo'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $memoId ? "Edit " : "New " );?> Debit Memo </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Customer:</td>

						<td>
							<?php
								if ( $memoId ) {
									echo '<b>'.$name.'</b>';
							?>		
									<input type='hidden' name='sltCustomer' id='sltCustomer' value='<?php echo $customer_id; ?>'>
							<?php		
								}else{
							?>
									<select name='sltCustomer' id='sltCustomer' onchange='showInvoiceNumber(0)'>
										<option></option>
										<?php
											$qry = "CALL sp_Customer_Dropdown()";
											$result = mysqli_query($db, $qry);
											$processError = mysqli_error($db);

											if ( !empty($processError) ){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_memo.php'.'</td><td>'.$processError.' near line 12.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}else{
												while($row = mysqli_fetch_assoc($result)){
													$customer_id = $row['id'];
													$customer_name = $row['name'];

													if ( $customer_id ==  $Customer){
														echo "<option value='".$customer_id."' selected>".$customer_name."</option>";
													}else{
														echo "<option value='".$customer_id."'>".$customer_name."</option>";
													}
												}
												$db->next_result();
												$result->close();
											}
										?>
									</select>
							<?php
								}
							?>
						</td>
					</tr>

					<tr>
						<td>Invoice No.:</td>

						<td>
							<select name='sltInvoiceNum' id='sltInvoiceNum'></select>
						</td>
					</tr>

					<tr>
						<td>Reference Date:</td>

						<td>
							<input type='text' name='txtMemoDate' id='txtMemoDate' value='<?php echo ( $memoId ? $reference_date :$MemoDate );?>'>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtMemoDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
						<td>Reference No.:</td>

						<td>
							<input type='text' name='txtReferenceNum' value='<?php echo ( $memoId ? $reference_number : $ReferenceNo );?>'>
						</td>
					</tr>

					<tr>
						<td>Amount:</td>

						<td> 
							<input type='text' name='txtAmount' value='<?php echo ( $memoId ? $amount : $Amount );?>'>
						</td>
					</tr>

					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveCust" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='customer_jacket.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidMemoId' id='hidMemoId' value='<?php echo $memoId; ?>'>
							<input type='hidden' name='hidProcessType' id='hidProcessType' value='add'>
							<input type='hidden' name='hidJacketType' value='dm'>
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $memoId ? $created_at : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $memoId ? $created_id : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
