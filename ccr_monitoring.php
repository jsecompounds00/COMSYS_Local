<html>
	<head>
		<title>CCR Monitoring - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
		?>
		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/init_unset_values/ccr_monitoring_unset_value.php");
			require("/include/init_unset_values/ccr_verification_unset_value.php");
			

			if( $_SESSION['ccr'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> CCR Monitoring </h3> </span>

			<div class="search_box">
				<form method='get' action='ccr_monitoring.php'>
					<input type='hidden' name='page' value=<?php echo $page; ?>>
					<table class="search_tables_form">
			 			<tr>
			 				<td> CCR No.: </td>
			 				<td> <input type='text' name='search' value='<?php echo htmlspecialchars($_GET['search']); ?>'> </td>
			 				<td> CCR Date: </td>
			 				<td> <input type='text' name='qsone' id='qsone' value='<?php echo htmlspecialchars($_GET['qsone']); ?>'> </td>
			 				<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
			 				<td> <input type='submit' value='Search'> </td>
			 				<td>
			 					<?php
									if(array_search(116, $session_Permit)){
								?>
										<input type='button' value='Add Monitoring' onclick="location.href='new_ccr_monitoring.php?id=0'">
								<?php
									 	$_SESSION['add_ccr'] = true;
									}else{
									 	unset($_SESSION['add_ccr']);
									}
								?>
			 				</td>
			 			</tr>
			 		</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>ccr_monitoring.php'.'</td><td>'.$error.' near line 49.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_CCR_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'ss', $search, $qsone);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "ccr_monitoring.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_CCR_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssii', $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>ccr_monitoring.php'.'</td><td>'.$processError.' near line 75.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='10'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
						    <th>CCR No.</th>
						    <th>CCR Date</th>
						    <th>Customer</th>
						    <th>Product</th>
						    <th>Complaint Type</th>
						    <!-- <th>Complaint</th> -->
						    <th>Status</th>
						    <th colspan="3"></th>
						</tr>
						<?php 
							while($row = mysqli_fetch_assoc($result)) { 
								$status_id = $row['status_id'];
								$c_status = $row['c_status'];
						?>
								<tr>
									<td> <?php echo $row['ccr_no']; ?> </td>
									<td> <?php echo $row['complaint_date']; ?> </td>
									<td> <?php echo $row['customer']; ?> </td>
									<td> <?php echo $row['FG']; ?> </td>
									<td> <?php echo $row['complaint_type']; ?> </td>
									<!-- <td> <?php //echo $row['complaint']; ?> </td> -->
									<td> <?php echo ( $status_id == 2 ? $c_status : '<b><i>'.$c_status.'</i></b>' ); ?> </td>
									<td>
										<?php
											if(array_search(117, $session_Permit)){
										?>
												<input type='button' name='btnTP' value='Edit' onclick="location.href='new_ccr_monitoring.php?id=<?php echo $row['id'];?>'">
										<?php
												$_SESSION['edit_ccr'] = true;
											}else{
												unset($_SESSION['edit_ccr']);
											}
										?>
									</td>
									<td>
										<?php
											if(array_search(120, $session_Permit)){
										
												if ( $status_id == 2 ){
										?>
													<input type='button' name='btnTV' value='Update' onclick="location.href='new_ccr_verification.php?id=<?php echo $row['id'];?>&vid=0'">
										<?php
												}else{
										?>
													<input type='button' name='btnTV' value='Update' disabled>
										<?php
											}

												$_SESSION['update_ccr'] = true;
											}else{
												unset($_SESSION['update_ccr']);
											}
										?>
									</td>
									<td>
										<?php
											if(array_search(121, $session_Permit)){
										?>
												<input type='button' name='btnTH' value='Verification History' onclick="location.href='ccr_history.php?page=1&id=<?php echo $row['id'];?>'">
										<?php
												$_SESSION['history_ccr'] = true;
											}else{
												unset($_SESSION['history_ccr']);
											}
										?>						
									</td>
								</tr>
						<?php 
							} 
						?>
						<tr>
							<td colspan='10'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>