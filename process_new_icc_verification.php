<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;

############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_icc_verification.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidICCID = $_POST['hidICCID'];
		$hidVerificationID = $_POST['hidVerificationID'];
		$txtVerifyDate = $_POST['txtVerifyDate'];
		$sltDeptID = $_POST['sltDeptID'];
		$sltAuditorID = $_POST['sltAuditorID'];
		$txtVerification = $_POST['txtVerification'];
		$chkStatus = $_POST['chkStatus'];
		$sltMonth = $_POST['sltMonth'];
		$sltYear = $_POST['sltYear'];	
		$radDivision = $_POST['radDivision'];	

############# Input Validation
		$valVerifyDate = validateDate($txtVerifyDate, 'Y-m-d');
		if ( $valVerifyDate != 1 ){
			$errmsg_arr[] = '* Invalid Verification Date.';
			$errflag = true;
		}
		if ( !$sltAuditorID ){
			$errmsg_arr[] = '* Invalid Auditor name.';
			$errflag = true;
		}
		if ( $txtVerification == '' ){
			$errmsg_arr[] = '* Verification cannot be blank.';
			$errflag = true;
		}
		if ( !isset($chkStatus) ){
			$errmsg_arr[] = '* Choose ICC Status.';
			$errflag = true;
		}


		if ( isset( $radDivision ) && $radDivision == 'Pipes' ){
			$radDivision = 'Pipes';
		}elseif ( isset( $radDivision ) && $radDivision == 'Compounds' ){
			$radDivision = 'Compounds';
		}elseif ( isset( $radDivision ) && $radDivision == 'Corporate' ){
			$radDivision = 'Corporate';
		}


		foreach ($_POST as $key => $value) {
			echo $key." => ".$value."<br>";
		}

	######### Validation on Checkbox
		if($hidVerificationID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidVerificationID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];
			
		// $txtVerification = str_replace('\\r\\n', '<br>', $txtVerification);
		
		// $txtVerification = str_replace('\\', '', $txtVerification);

############# Input Validation

	############# SESSION, keeping last input value
		$_SESSION['SESS_ICV_VerifyDate'] = $txtVerifyDate;
		$_SESSION['SESS_ICV_Verification'] = $txtVerification;
		$_SESSION['SESS_ICV_Status'] = $chkStatus;
		$_SESSION['SESS_ICV_Month'] = $sltMonth;
		$_SESSION['SESS_ICV_Year'] = $sltYear;
		$_SESSION['SESS_ICV_Division'] = $radDivision;

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_icc_verification.php?id=".$hidICCID."&vid=".$hidVerificationID);
			exit();
		}
############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_ICC_Verification_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'iisisisissiiis', $hidVerificationID, $hidICCID, $txtVerifyDate, $sltAuditorID
												, $txtVerification, $chkStatus, $sltMonth, $sltYear, $createdAt
												, $updatedAt, $createdId, $updatedId, $sltDeptID, $radDivision);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_icc_verification.php'.'</td><td>'.$processError.' near line 117.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidVerificationID)
				$_SESSION['SUCCESS']  = 'Successfully updated ICC verification.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added ICC verification.';
			//echo $_SESSION['SUCCESS'];
			header("location:icc_monitoring.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>