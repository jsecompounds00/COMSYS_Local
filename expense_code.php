<html>
<head>
	<title>Expense Code - Home</title>
	<?php
		require("include/database_connect.php");
			
		$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
		$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
		$page = ($_GET["page"] ? $_GET["page"] : 1);
	?>
</head>
<body>
	<?php
		require ("/include/header.php");
		require("/include/init_unset_values/expense_code_unset_value.php");
		require("/include/init_unset_values/expense_particular_unset_value.php");

		// if( $_SESSION["expense_code"] == false) 
		// {
		// 	$_SESSION["ERRMSG_ARR"] ="Access denied!";
		// 	session_write_close();
		// 	header("Location:comsys.php");
		// 	exit();
		// }

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
	?>

	<div class="wrapper"> 

		<span> <h3> Expense Code </h3>  </span>

		<div class="search_box">
			<form method="get" action="expense_code.php">
				<input type="hidden" name="page" value="<?php echo $page; ?>">
				<input type="hidden" name="qsone" value="<?php echo $qsone; ?>">
				<table class="search_tables_form">
					<tr>
						<td> Description: </td>
						<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
						<td> <input type="submit" value="Search"> </td>
						<td>
							<?php
								// if(array_search(152, $session_Permit)){
							?>
									<input type="button" name="btnAddCode" value="Add Code" onclick="location.href='<?php echo PG_NEW_EXPENSE;?>0'">
							<?php
								// 	$_SESSION["add_expense"] = true;
								// }else{
								// 	unset($_SESSION["add_expense"]);
								// }
							?>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<?php
			if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>expense_code.php"."</td><td>".$error." near line 41.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryD = mysqli_prepare($db, "CALL sp_Budget_Expense_Code_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryD, "s", $search);
					$qryD->execute();
					$resultD = mysqli_stmt_get_result($qryD);
					$total_results = mysqli_num_rows($resultD); //return number of rows of result

					$db->next_result();
					$resultD->close();
					
					$targetpage = "expense_code.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Budget_Expense_Code_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
				
					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>expense_code.php"."</td><td>".$processError." near line 62.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) {
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
		?>
					<table class="home_pages">
						<tr>
							<td colspan="8">
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
							<th> Code </th>
							<th> Description </th>
							<th> Active </th>
							<th colspan="2"> </th>
						</tr>
						<?php 
							while($row = mysqli_fetch_assoc($result)){
								$code = htmlspecialchars($row["code"]);
								$description = htmlspecialchars($row["description"]);
								$tag_miscellaneous = htmlspecialchars($row["tag_miscellaneous"]);
								$active = ( $row["active"] ? "Y" : "N" );
						?>
							<tr>
							    <td> <?php echo $code;?> </td>
							    <td> <?php echo $description;?> </td>
							    <td> <?php echo $active;?> </td>
							    <td>
									<?php
										// if(array_search(152, $session_Permit)){	    
									?>
											<input type="button" name="btnEdit" value="EDIT" onclick="location.href='<?php echo PG_NEW_EXPENSE.$row["id"];?>'"> 
									<?php
										// 	$_SESSION["edit_expense"] = true;
										// }else{
										// 	unset($_SESSION["edit_expense"]);
										// }
										?>
								</td>
								<td>
									<?php
										// if(array_search(152, $session_Permit)){	    
									?>
											<input type="<?php echo ($tag_miscellaneous ? "hidden" : "button");?>" 
												name="btnAddParticulars" value="Add Particulars" 
												onclick="location.href='<?php echo PG_NEW_PARTICULARS.$row["id"];?>'"> 
									<?php
										// $_SESSION["add_particulars"] = true;
										// }else{
										// 	unset($_SESSION["add_particulars"]);
										// }
										?>
								</td>
							</tr>
						<?php
							}
							$db->next_result();
							$result->close();
						?>
							<tr>
								<td colspan="8">
									<?php echo $pagination;?>
								</td>
							</tr>
					</table>
			<?php
					}
				}
			?>
	</div>
</body>
<footer>
		<?php	
			require("include/database_close.php");
		?>
</footer>
</html>