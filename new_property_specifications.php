<html>
	<head>
		<title> Specifications </title>
		<script src="js/jscript.js"></script>  
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_specifications.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$propertyID = $_GET['id'];

				if($propertyID)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Company_Property_Specifications_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $propertyID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_specifications.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						$specificationID = array();
						$specifications = array();
						$good = array();
						$damaged = array();
						$created_at = array();
						$created_id = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$propertyID = $row['propertyID'];
							$property_name = $row['property_name'];

							$specificationID[] = $row['specificationID'];

							$specifications[] = htmlspecialchars($row['specifications']);

							$good[] = $row['good'];

							$damaged[] = $row['damaged'];

							$created_at[] = $row['created_at'];

							$created_id[] = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

					$count = count($specificationID);

		############ .............
					$qryPI = "SELECT id from comsys.company_properties";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_specifications.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($propertyID, $id) ){ //, TRUE
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_specifications.php</td><td>The user tries to edit a non-existing specification_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "";
				}
				else{

					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_specifications.php</td><td>The user tries to edit a non-existing specification_id.</td></tr>', 3, "errors.php");
					header("location: error_message.html");

				}
			}
					
		?>
	</head>
	<body>

		<form method='post' action='process_new_property_specifications.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if( $_SESSION['property_specs'] == false) 
				{
					$_SESSION['ERRMSG_ARR'] ='Access denied!';
					session_write_close();
					header("Location:comsys.php");
					exit();
				}
			?>

			<div class="wrapper">

				<span> <h3> Specifications for <?php echo $property_name;?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="child_tables_form">
					<tr>
						<th>Asset Specifications</th>
						<!-- <th>Condition</th> -->
					</tr>
					<?php
						if ( $count == 1 && is_null( $specificationID[$i] ) ){
							$count = 5;
							for ( $i = 0; $i < 5; $i++ ){
					?>
								<tr class='pads2'>
									<td>	
										<input type='hidden' name='hidSpecificationID[]' value='0'>
										<input type='text' name='txtSpecifications[]' size='40'>
									</td>
										<input type='hidden' name='hidCreatedAt[]' value='<?php echo date('Y-m-d H:i:s'); ?>'>
										<input type='hidden' name='hidCreatedID[]' value='<?php echo $_SESSION['SESS_USER_ID']; ?>'>
								</tr>
					<?php
							}		
						}else{
							for ( $i = 0; $i < $count; $i++ ){
					?>
								<tr class='pads2'>
									<td>	
										<?php
											if ( $specificationID[$i] ){
										?>
												<input type='hidden' name='hidSpecificationID[]' value="<?php echo $specificationID[$i];?>">
												<input type='text' name='txtSpecifications[]' size='40' value="<?php echo $specifications[$i];?>">
										<?php
											}else{
										?>
												<input type='hidden' name='hidSpecificationID[]' value="0">
												<input type='text' name='txtSpecifications[]' size='40' value="">
										<?php
											}
										?>
									</td>	
										<?php
											if( $specificationID[$i] ){
										?>
												<input type='hidden' name='hidCreatedAt[]' value='<?php echo $created_at[$i]; ?>'>
												<input type='hidden' name='hidCreatedID[]' value='<?php echo $created_id[$i]; ?>'>
										<?php		
											}else{
										?>
												<input type='hidden' name='hidCreatedAt[]' value='<?php echo date('Y-m-d H:i:s'); ?>'>
												<input type='hidden' name='hidCreatedID[]' value='<?php echo $_SESSION['SESS_USER_ID']; ?>'>
										<?php		
											}
										?>
								</tr>
					<?php	
							}	
						}
					?>
					<tbody id='AddSpecs'>
						<tr class="spacing">
							<td>
								<input type='button' value='+' onclick='addExtraSpecs(<?php echo $count; ?>)'>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='property_transfer.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidPropertyID' value="<?php echo $propertyID;?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>