<?php
	//Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_customer.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidcustId = $_POST['hidcustId'];
		$radCustType = $_POST['radCustType'];
		$txtTerms = $_POST['txtTerms'];
		$sltTerms = $_POST['sltTerms'];
		$radCreditLimitUnit = $_POST['radCreditLimitUnit'];
		$txtCreditLimit = $_POST['txtCreditLimit'];
		$sltAgent = $_POST['sltAgent'];
		$txtAddressStreet = $_POST['txtAddressStreet'];
		$txtAddressBaranggay = $_POST['txtAddressBaranggay'];
		$txtAddressCityProvince = $_POST['txtAddressCityProvince'];
		$txtZipCode = $_POST['txtZipCode'];
		$radTitle = $_POST['radTitle'];
		$txtContactFName = $_POST['txtContactFName'];
		$txtContactLName = $_POST['txtContactLName'];
		$txtContactNumber = $_POST['txtContactNumber'];
		$txtGracePeriod = $_POST['txtGracePeriod'];

		//Input Validations
		if ( ($txtAddressCityProvince == "") ){
			$errmsg_arr[] = "* Customer's city/province address can't be blank.";
			$errflag = true;
		}

		if ( $txtAddressStreet == "" ){
			$txtAddressStreet = NULL;
		}
		if ( $txtAddressBaranggay == "" ){
			$txtAddressBaranggay = NULL;
		}
		if ( $txtZipCode == "" ){
			$txtZipCode = NULL;
		}

		if ( $txtContactFName == "" ){
			$txtContactFName = NULL;
		}
		if ( $txtContactLName == "" ){
			$txtContactLName = NULL;
		}
		if ( $txtContactNumber == "" ){
			$txtContactNumber = NULL;
		}
		if ( $txtContactNumber == "" ){
			$txtContactNumber = NULL;
		}
		if ( !isset($radTitle) ){
			$radTitle = NULL;
		}

		if ( isset( $radCustType ) && $radCustType == 'local' ){
			$radCustType = 1;
		}elseif ( isset( $radCustType ) && $radCustType == 'export' ){
			$radCustType = 0;
		}else{
			$errmsg_arr[] = '* Customer type is required.';
			$errflag = true;
		}

		if ( $sltTerms == 'COD'){
			$txtTerms = NULL;
		}elseif ( !$sltTerms && !is_numeric( $txtTerms )){
			$errmsg_arr[] = "* Terms can't be blank.";
			$errflag = true;
		}
		if ( !$sltTerms ){
			$sltTerms = NULL;
		}
		if ( $txtTerms == "" ){
			$txtTerms = NULL;
		}elseif( $txtTerms != "" && !is_numeric($txtTerms) ){
			$errmsg_arr[] = '* Terms must be numeric.';
			$errflag = true;
		}

		if ( $txtGracePeriod == "" ){
			$txtGracePeriod = NULL;
		}elseif( $txtGracePeriod != "" && !is_numeric($txtGracePeriod) ){
			$errmsg_arr[] = '* Grace period must be numeric.';
			$errflag = true;
		}

		if ( isset( $radCreditLimitUnit ) && $radCreditLimitUnit == 'Php' ){
			$radCreditLimitUnit = 'Php';
		}elseif ( isset( $radCreditLimitUnit ) && $radCreditLimitUnit == 'USD' ){
			$radCreditLimitUnit = 'USD';
		}else{
			$errmsg_arr[] = "* Currency can't be blank.";
			$errflag = true;
		}

		if ( $txtCreditLimit != "" && !is_numeric( $txtCreditLimit ) ){
			$errmsg_arr[] = "* Credit limit must be numeric.";
			$errflag = true;
		}

		if ( !( $sltAgent ) ){
			$errmsg_arr[] = '* Select agent.';
			$errflag = true;
		}

		$updatedAt = date('Y-m-d H:i:s');
		$updatedId = $_SESSION['SESS_USER_ID'];

		
		$_SESSION['SESS_CUS_CustType'] = $radCustType;
		$_SESSION['SESS_CUS_TermsDays'] = $txtTerms;
		$_SESSION['SESS_CUS_Terms'] = $sltTerms;
		$_SESSION['SESS_CUS_CreditLimitUnit'] = $radCreditLimitUnit;
		$_SESSION['SESS_CUS_CreditLimit'] = $txtCreditLimit;
		$_SESSION['SESS_CUS_Agent'] = $sltAgent;
		$_SESSION['SESS_CUS_AddressStreet'] = $txtAddressStreet;
		$_SESSION['SESS_CUS_AddressBaranggay'] = $txtAddressBaranggay;
		$_SESSION['SESS_CUS_AddressCityProvince'] = $txtAddressCityProvince;
		$_SESSION['SESS_CUS_ZipCode'] = $txtZipCode;
		$_SESSION['SESS_CUS_Title'] = $radTitle;
		$_SESSION['SESS_CUS_ContactFName'] = $txtContactFName;
		$_SESSION['SESS_CUS_ContactLName'] = $txtContactLName;
		$_SESSION['SESS_CUS_ContactNumber'] = $txtContactNumber;
		$_SESSION['SESS_CUS_GracePeriod'] = $txtGracePeriod;

		
		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_CUSTOMER.$hidcustId);
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Customer_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isiiisdssssssssssi', $hidcustId, $updatedAt, $updatedId
										  , $radCustType, $txtTerms, $sltTerms, $txtCreditLimit
										  , $radCreditLimitUnit, $sltAgent
										  , $txtAddressStreet, $txtAddressBaranggay
										  , $txtAddressCityProvince, $txtZipCode
										  , $radTitle, $txtContactFName, $txtContactLName
										  , $txtContactNumber, $txtGracePeriod);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_customer.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidcustId)
				$_SESSION['SUCCESS']  = 'Successfully updated customer.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new customer.';
			//echo $_SESSION['SUCCESS'];
			header("location:customer.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	