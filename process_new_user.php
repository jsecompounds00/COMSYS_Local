<?php
	//Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection

	define("ENCRYPTION_KEY", "!@#$%^&*");
	/**
	 * Returns an encrypted & utf8-encoded
	 */
	function encrypt($pure_string, $encryption_key) {
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
	    return $encrypted_string;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_user.php'.'</td><td>'.$error.' near line 37.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidUserid = $_POST['hidUserid'];
		$txtFirstName = $_POST['txtFirstName'];
		$txtLastName = $_POST['txtLastName'];
		$txtUserName = $_POST['txtUserName'];
		$txthidUserpass = $_POST['txthidUserpass'];
		$txtUserpass = $_POST['txtUserpass'];
		$txtConfUserpass = $_POST['txtConfUserpass'];
		$sltDept1 = $_POST['sltDept1'];
		$sltDept2 = $_POST['sltDept2'];
		$txtEmail = $_POST['txtEmail'];

		$_SESSION['SESS_USR_FirstName'] = $txtFirstName;
		$_SESSION['SESS_USR_LastName'] = $txtLastName;
		$_SESSION['SESS_USR_UserName'] = $txtUserName;
		$_SESSION['SESS_USR_Active'] = $chkActive;
		$_SESSION['SESS_USR_Compounds'] = $chkCompounds;
		$_SESSION['SESS_USR_Pipes'] = $chkPipes;
		$_SESSION['SESS_USR_Corporate'] = $chkCorporate;
		$_SESSION['SESS_USR_PPR'] = $chkPPR;

		//Input Validations

		if ( $hidUserid ) //Password validation (start)
		{
			if ( empty($txtUserpass) ){
				if (!empty($txtConfUserpass) ){
					$errmsg_arr[] = '* Password does not match.';
			 		$errflag = true;
				}else{
					$txtUserpass = $txthidUserpass;
				}
			}else{
				if ( $txtUserpass!=$txtConfUserpass ) {
					$errmsg_arr[] = '* Password does not match.';
			 		$errflag = true;
				}
				else{
					$txtUserpass;
				}
			}
		}else{
			if ( empty( $txtFirstName ) ){
				$errmsg_arr[] = '* First name cannot be blank.';
				$errflag = true;
			}
			if ( empty( $txtLastName ) ){
				$errmsg_arr[] = '* Last name cannot be blank.';
				$errflag = true;
			}
			// if ( empty( $txtUserName ) ){
			// 	$errmsg_arr[] = '* Username cannot be blank.';
			// 	$errflag = true;
			// }
			// if ( empty($txtUserpass) ){
			// 	$errmsg_arr[] = '* Password is missing.';
			// 	$errflag = true;
			// }
			if ( !empty($txtUserpass) && $txtUserpass!=$txtConfUserpass ){
				$errmsg_arr[] = '* Password does not match.';
				$errflag = true;
			}
			if ( empty($txtUserpass) ){
				$txtUserpass = NULL;
			}
		} //Password validation (end)

		if ( empty($chkRole) ){
			$chkRole = '0';
		}
		if ( empty($sltDept1) ){
			$errmsg_arr[] = '* Department is missing.';
			$errflag = true;
		}

		if ( !$sltDept2 ){
			$sltDept2 = NULL;
		}

		if ( count($_POST['chkRole']) >3 ){
			$errmsg_arr[] = '* User exceeds its maximum role of three (3).';
			$errflag = true;
		}else{
			$chkRole = implode(',', $_POST['chkRole']);
		}

		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;

		if ( isset($_POST['chkCompounds']))
			$chkCompounds = 1;
		else
			$chkCompounds = 0;
		if ( isset($_POST['chkPipes']))
			$chkPipes = 1;
		else
			$chkPipes = 0;
		if ( isset($_POST['chkCorporate']))
			$chkCorporate = 1;
		else
			$chkCorporate = 0;
		if ( isset($_POST['chkPPR']))
			$chkPPR = 1;
		else
			$chkPPR = 0;

		$txtUserpass = convert_uuencode($txtUserpass);

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_USER.$hidUserid);
			exit();
		}

		if($hidUserid == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidUserid == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		$qry = mysqli_prepare($db, "CALL sp_User_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isssssiissiiiiiiis', $hidUserid, $txtFirstName, $txtLastName, $txtUserName
													, $txtUserpass, $chkRole, $sltDept1, $chkActive
													, $createdAt, $updatedAt, $createdId, $updatedId
													, $sltDept2, $chkCompounds, $chkPipes, $chkCorporate, $chkPPR, $txtEmail);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_user.php'.'</td><td>'.$processError.' near line 134.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidUserid)
				$_SESSION['SUCCESS']  = 'Successfully updated user.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new user.';
			// echo $_SESSION['SUCCESS'];
			header("location:user.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>