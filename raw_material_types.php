<html>
	<head>
		<title>Raw Material Types - Home</title>
		<?php 
			require("include/database_connect.php");
			
			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>
		<?php
			require("/include/header.php");
			require("/include/init_unset_values/raw_material_types_unset_value.php");

			if( $_SESSION["rm_type"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>
		
		<div class="wrapper">

			<span> <h3> Raw Material Types </h3> </span>

			<div class="search_box">
				<form method="get" action="raw_material_types.php">
			 	 	<input type="hidden" name="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]); ?>">
			 		<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_form">
						<tr>
							<td> Code: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php
						 			if(array_search(176, $session_Permit)){
						 		?>
						 				<input type="button" name="btnAdd" value="Add Raw Material Type" onclick="location.href='new_raw_material_types.php?id=0'">
						 		<?php
										$_SESSION["add_rm_type"] = true;
						 			}else{
						 				unset($_SESSION["add_rm_type"]);
						 			}
						 		?>
							</td>
						</tr>
					</table>
			 	</form>
			</div>
			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>raw_material_types.php"."</td><td>".$error." near line 42.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryR = mysqli_prepare($db, "CALL sp_RawMaterial_Type_Home(?, ?, ?, ?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryR, "siiiis", $search, $compounds, $pipes, $corporate, $ppr, $_SESSION["SESS_ROLE_ID"]);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR); //return results of query

					$total_results = mysqli_num_rows($resultR); //return number of rows of result
					
					$db->next_result();
					$resultR->close();

					$targetpage = "raw_material_types.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_RawMaterial_Type_Home(?, ?, ?, ?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, "siiiisii", $search, $compounds, $pipes, $corporate, $ppr, $_SESSION["SESS_ROLE_ID"], $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>raw_material_types.php"."</td><td>".$processError." near line 63.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
				?>
						<table class="home_pages">
							<tr>
								<td colspan="7"> <?php echo $pagination;?> </td>
							</tr>
							<tr>
							    <th> Code </th>
							    <th> Description </th>
							    <th> Compounds </th>
							    <th> Pipes </th>
							    <th> Corporate </th>
							    <th> PPR </th>
							    <th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)) {
									$code = htmlspecialchars($row["code"]);
									$description = nl2br(htmlspecialchars($row["description"]));
									$Compounds = $row["Compounds"];
									$Pipes = $row["Pipes"];
									$Corporate = $row["Corporate"];
									$PPR = $row["PPR"];
									$id = $row["id"];
							?>
									<tr valign="top">
										<td> <?php echo $code; ?> </td>
										<td> <?php echo $description; ?> </td>
										<td> <?php echo ( $row["Compounds"] == "Y" ? "<img src='images/yes.png' height='20'>" : "" ); ?> </td>
										<td> <?php echo ( $row["Pipes"] == "Y" ? "<img src='images/yes.png' height='20'>" : "" ); ?> </td>
										<td> <?php echo ( $row["Corporate"] == "Y" ? "<img src='images/yes.png' height='20'>" : "" ); ?> </td>
										<td> <?php echo ( $row["PPR"] == "Y" ? "<img src='images/yes.png' height='20'>" : "" ); ?> </td>
										<td> 

										<?php 
											if(array_search(177, $session_Permit)){ ?>
												<input type="button" name="btnEdit" value="Edit" onclick="location.href='new_raw_material_types.php?id=<?php echo $id; ?>'"> 
										 <?php 
										 		$_SESSION["edit_rm_type"] = true; 
											}else{
											 	unset($_SESSION["edit_rm_type"]);
											}
										?>
										</td>
									</tr>
							<?php
								}
							?>
							<tr>
								<td colspan="7"> <?php echo $pagination;?> </td>
							</tr>
						</table>	
			<?php
					}
				}
			?>
		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>