<html>
	<head>
		<?php 
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_credit_terms.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$creditID = $_GET['id'];

				if($creditID)
				{ 
					$qry = mysqli_prepare( $db, "CALL sp_Credit_Terms_Query(?)" );
					mysqli_stmt_bind_param( $qry, 'i', $creditID );
					$qry->execute();
					$result = mysqli_stmt_get_result( $qry );
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_credit_terms.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$customer_id = $row['customer_id'];
							$lc_number = htmlspecialchars($row['lc_number']);
							$lc_date = $row['lc_date'];
							$quantity = $row['quantity'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.credit_terms";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_credit_terms.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($creditID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_credit_terms.php</td><td>The user tries to edit a non-existing customer_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Credit Terms - Edit</title>";

				}
				else{

					echo "<title>Credit Terms - Add</title>";

				}

			}
		?>
		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>
		<form method='post' action='process_new_credit_terms.php'>
			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if ( $creditID ){
					if( $_SESSION['edit_credit'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['add_credit'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $creditID ? "Edit LC# ".$lc_number : "New LC" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>LC Number:</td>
						<td>
							<input type='text' name='txtLCNum' value="<?php echo ( $creditID ? $lc_number : "" );?>">
						</td>
					</tr>
					<tr>
						<td>Date:</td>
						<td >
							<input type='text' name='txtDate' id='txtDate' value="<?php echo ( $creditID ? $lc_date : "" );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr>
						<td>Total LC Quantity:</td>
						<td>
							<input type='text' name='txtLCQuantity' value="<?php echo ( $creditID ? $quantity : "" );?>">
						</td>
					</tr>
					<tr valign='top'>
						<td> Nth Contract:</td>
						<td>
							<select name='sltContractNum' multiple>
								<option value='1'>1</option>
								<option value='2'>2</option>
								<option value='3'>3</option>
								<option value='4'>4</option>
								<option value='5'>5</option>
								<option value='6'>6</option>
								<option value='7'>7</option>
								<option value='8'>8</option>
								<option value='9'>9</option>
								<option value='10'>10</option>
								<option value='11'>11</option>
								<option value='12'>12</option>
								<option value='13'>13</option>
								<option value='14'>14</option>
								<option value='15'>15</option>
							</select>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveCust" value="Save">		
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='credit_terms.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidCreditId' value="<?php echo ( $creditID );?>">
							<input type='hidden' name='hidCreatedAt' value="<?php echo ( $creditID ? $createdAt : date('Y-m-d H:i:s') );?>">
							<input type='hidden' name='hidCreatedId' value="<?php echo ( $creditID ? $createdId : $_SESSION["SESS_USER_ID"] );?>">
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
