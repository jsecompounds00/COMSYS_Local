<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_supplier.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidSupplierid = $_POST['hidSupplierid'];
		$txtSupplier = $_POST['txtSupplier'];
		//Input Validations
		if ( empty($txtSupplier) ){
			$errmsg_arr[] = '* Supplier name is missing.';
			$errflag = true;
		}
		if (is_numeric($txtSupplier)){
			$errmsg_arr[] = '* Invalid supplier name.';
			$errflag = true;
		}
		if ( isset($_POST['chkActive']) )
			$chkActive = 1;
		else
			$chkActive = 0;
		if ( isset($_POST['chkRm']) )
			$chkRM = 1;
		else
			$chkRM = 0;
		if ( isset($_POST['chkFg']) )
			$chkFG = 1;
		else
			$chkFG = 0;
		if ( isset($_POST['chkSupplies']) )
			$chkSupplies = 1;
		else
			$chkSupplies = 0;

		if ( isset($_POST['chkCmpds'])) //&& $_POST['radDivision'] == 'Cmpds'
			$chkCmpds = 1;
		else
			$chkCmpds = 0;
		if ( isset($_POST['chkPips'])) //&& $_POST['radDivision'] == 'Pips'
			$chkPips = 1;
		else
			$chkPips = 0;
		if ( isset($_POST['chkCorp'])) //&& $_POST['radDivision'] == 'Corp'
			$chkCorp = 1;
		else
			$chkCorp = 0;
		if ( isset($_POST['chkPPR'])) //&& $_POST['radDivision'] == 'PPR'
			$chkPPR = 1;
		else
			$chkPPR = 0;

		$_SESSION['SESS_SPR_Supplier'] = $txtSupplier;
		$_SESSION['SESS_SPR_Rm'] = $chkRM;
		$_SESSION['SESS_SPR_Fg'] = $chkFG;
		$_SESSION['SESS_SPR_Supplies'] = $chkSupplies;
		$_SESSION['SESS_SPR_Active'] = $chkActive;
		$_SESSION['SESS_SPR_Cmpds'] = $chkCmpds;
		$_SESSION['SESS_SPR_Pips'] = $chkPips;
		$_SESSION['SESS_SPR_Corp'] = $chkCorp;
		$_SESSION['SESS_SPR_PPR'] = $chkPPR;


		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_SUPPLIER.$hidSupplierid);
			exit();
		}

		if($hidSupplierid == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidSupplierid == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];


		$qry = mysqli_prepare($db, "CALL sp_Supplier_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isiiiissiiiiii', $hidSupplierid, $txtSupplier, $chkActive, $chkRM
													, $chkFG, $chkSupplies, $createdAt, $updatedAt, $createdId
													, $updatedId, $chkCmpds, $chkPips, $chkCorp, $chkPPR);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_supplier.php'.'</td><td>'.$processError.' near line 92.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidSupplierid)
				$_SESSION['SUCCESS']  = 'Successfully updated supplier.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new supplier.';
			//echo $_SESSION['SUCCESS'];
			header("location:supplier.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	