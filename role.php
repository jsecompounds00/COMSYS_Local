<html>
	<head>
		<title>Role - Home</title>

		<?php 
			require("include/database_connect.php");
			
			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>

	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/init_unset_values/role_unset_value.php");

			if( $_SESSION["role"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["page"] = $_GET["page"];
			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
		?>

		<div class="wrapper">

			<span> <h3> Role </h3> </span>

			<div class="search_box">
				 <form method="get" action="role.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<input type="hidden" name="qsone" value="<?php echo $qsone; ?>">
					<table class="search_tables_form">
						<tr>
							<td> Name: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php 	
									if(array_search(29, $session_Permit)){ 
								?>
										<input type="button" name="btnAddRole" value="Add Role" onclick="location.href='<?php echo PG_NEW_ROLE; ?>0'">
								<?php
										$_SESSION["add_role"] = true;
									}else{
										unset($_SESSION["add_role"]);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>role.php"."</td><td>".$error." near line 42.</td></tr></tbody>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryR = mysqli_prepare($db, "CALL sp_Role_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryR, "s", $search);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR); //return results of query

					$total_results = mysqli_num_rows($resultR); //return number of rows of result
					
					$db->next_result();
					$resultR->close();

					$targetpage = "role.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Role_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>role.php"."</td><td>".$processError." near line 63.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) {
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>			
						<table class="home_pages">
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th rowspan="2">Name</th>
							    <th colspan="2"> Applicable To </th>
						   		<th rowspan="2"></th>
							</tr>
							<tr>
								<th> Division/s </th>
								<th> Department/s </th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result))
								{
							?>
									<tr>
										<td> <?php echo $row["name"]; ?> </td>
										<td> <?php echo $row["Divisions"]; ?> </td>
										<td> <?php echo $row["Dept"]; ?> </td>
										<td>
											<?php 	
												if (array_search(30, $session_Permit)){		
											?>
													<input type="button" name="btnEdit" value="EDIT" onclick="location.href='<?php echo PG_NEW_ROLE.$row["id"]; ?>'">
											<?php			
													$_SESSION["edit_role"] = true;
												}else{
													unset($_SESSION["edit_role"]);
												}
											?>	
											</td>					
									</tr>
							<?php				
								}
								$db->next_result();
								$result->close();
							?>
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php					 
					}
				}
			?>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
