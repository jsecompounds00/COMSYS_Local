<html>
	<head>
		<title>Test Evaluation Results</title>
		<script src="js/datetimepicker_css.js"></script> 
		<script src="js/batch_ticket_js.js"></script>
		<?php
			require("/include/database_connect.php"); 

			$jrd_id = $_GET['jrd_id'];
			$test_id = $_GET['test_id'];

			$qryCT = mysqli_prepare($db, "CALL sp_JRD_Choices_For_Evaluation_Query( ? )");
			mysqli_stmt_bind_param($qryCT, 'i', $jrd_id);
			$qryCT->execute();
			$resultCT = mysqli_stmt_get_result($qryCT);
			$processErrorCT = mysqli_error($db);

			if ( !empty($processErrorCT) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>option_for_evaluation.php'.'</td><td>'.$processErrorCT.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultCT)){
					$jrd_number = $row['jrd_number'];
					$mod_eval_finished_good_id = $row['mod_eval_finished_good_id'];
					$product_for_comparison = $row['product_for_comparison'];
					$product_for_evaluation = $row['product_for_evaluation'];
				}
				$db->next_result();
				$resultCT->close();
			}
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			// require("/include/init_value.php");	
		?>

		<div class="wrapper">

			<span> <h3> Choose Item to Evaluate: </h3> </span>

			<table class="parent_tables_form">
				<tr>
					<td class="borderless">
						<input type='button' value='<?php echo $product_for_comparison;?>' onclick="location.href='jrd_testing.php?test_id=<?php echo $test_id; ?>&jrd_id=<?php echo $jrd_id; ?>&fg_id=<?php echo $mod_eval_finished_good_id;?>'">
					</td>
					<td class="borderless">
						<input type='button' value='<?php echo $product_for_evaluation; ?>' onclick="location.href='jrd_testing.php?test_id=<?php echo $test_id; ?>&jrd_id=<?php echo $jrd_id; ?>&fg_id=0'">
					</td>
					<td class="borderless">
						<input type='button' value='Back' onclick="location.href='new_jrd_test_evaluation_noreplicate.php'">
					</td>
				</tr>
			</table>
			
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>