<html>
	<head>
		<?php
			require("/include/database_connect.php");
			if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_departmental.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$budgetID = $_GET["id"];

					if($budgetID)
					{ 
						$qry = mysqli_prepare($db, "CALL sp_Budget_Monitoring_Departmental_Query(?)");
						mysqli_stmt_bind_param($qry, "i", $budgetID);
						$qry->execute();
						$result = mysqli_stmt_get_result($qry);
						$processError = mysqli_error($db);

						if(!empty($processError))
						{
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_departmental.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							while($row = mysqli_fetch_assoc($result))
							{
								$QBudgetID = $row["QBudgetID"];
								$QBudgetYear = $row["QBudgetYear"];
								$QDepartmentID = $row["QDepartmentID"];
								$QCreatedAt = $row["QCreatedAt"];
								$QCreatedID = $row["QCreatedID"];
								$QGLCodeID = $row["QGLCodeID"];

								$QBudgetItemID[] = $row["QBudgetItemID"];
							    $QGLParicularsID[] = $row["QGLParicularsID"];
							    $QUOMID[] = $row["QUOMID"];
							    $QUnitPrice[] = $row["QUnitPrice"];
							    $QItemsCreatedID[] = $row["QItemsCreatedID"];
							    $QItemsCreatedAt[] = $row["QItemsCreatedAt"];
							    $QRemarks[] = $row["QRemarks"];
							    $QJAN[] = ( $row["QJAN"] != 0 ? $row["QJAN"] : NULL );
							    $QFEB[] = ( $row["QFEB"] != 0 ? $row["QFEB"] : NULL );
							    $QMAR[] = ( $row["QMAR"] != 0 ? $row["QMAR"] : NULL );
							    $QAPR[] = ( $row["QAPR"] != 0 ? $row["QAPR"] : NULL );
							    $QMAY[] = ( $row["QMAY"] != 0 ? $row["QMAY"] : NULL );
							    $QJUN[] = ( $row["QJUN"] != 0 ? $row["QJUN"] : NULL );
							    $QJUL[] = ( $row["QJUL"] != 0 ? $row["QJUL"] : NULL );
							    $QAUG[] = ( $row["QAUG"] != 0 ? $row["QAUG"] : NULL );
							    $QSEP[] = ( $row["QSEP"] != 0 ? $row["QSEP"] : NULL );
							    $QOCT[] = ( $row["QOCT"] != 0 ? $row["QOCT"] : NULL );
							    $QNOV[] = ( $row["QNOV"] != 0 ? $row["QNOV"] : NULL );
							    $QDEC[] = ( $row["QDEC"] != 0 ? $row["QDEC"] : NULL );
							}
						}
						$db->next_result();
						$result->close();

						$count = count($QBudgetItemID);

			############ .............
						$qryPI = "SELECT id from comsys.budget_monitoring_departmental";
						$resultPI = mysqli_query($db, $qryPI); 
						$processErrorPI = mysqli_error($db);

						if ( !empty($processErrorPI) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_departmental.php"."</td><td>".$processErrorPI." near line 58.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}else{
								$id = array();
							while($row = mysqli_fetch_assoc($resultPI)){
								$id[] = $row["id"];
							}
						}
						$db->next_result();
						$resultPI->close();

			############ .............
						if( !in_array($budgetID, $id, TRUE) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_departmental.php</td><td>The user tries to edit a non-existing department_id.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}

						echo "<title>Departmental Budget - Edit</title>";
					}
					else
					{
						echo "<title>Departmental Budget - Add</title>";
					}
				}
		?>
		<script src="js/budget_monitoring_departmental_js.js"></script>  
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> 
	</head>
	<body>
	
		<form method="post" action="process_new_budget_monitoring_departmental.php">
			
			<?php
				require("/include/header.php");
				require("/include/init_unset_values/budget_monitoring_departmental_init_value.php");

				// if ( $budgetID ){
				// 	if( $_SESSION["departmental_budget_edit"] == false) 
				// 	{
				// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}
				// }else{	
				// 	if( $_SESSION["departmental_budget_add"] == false) 
				// 	{
				// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}			
				// }

			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $budgetID ? "Edit Budget" : "New Budget" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td> Year: </td>
						<td>
							<input type="text" name="txtBudgetYear" value="<?php echo($budgetID && $initBMDBudgetYear == NULL ? $QBudgetYear : $initBMDBudgetYear);?>">
						</td>
					</tr>

					<tr>
						<td> Department: </td>
						<td>
							<select name="sltDepartment" id="sltDepartment" onchange="showGLCode(), showItem(), showUOM(51), showUnitPrice(51)">
								<?php
		 							$qryBD = "CALL sp_Budget_Department_Dropdown(1)"; 
									$resultBD = mysqli_query($db, $qryBD);
									$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_departmental.php"."</td><td>".$processError1." near line 43.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultBD))
										{
											$DDId = $row["DDId"];
											$DDCode = $row["DDCode"];

											echo "<option value='".$DDId."'".
												  ($budgetID && $initBMDDepartment == NULL ? ( $QDepartmentID == $DDId ? "selected" : "" ) : ( $initBMDDepartment == $DDId ? "selected" : "" ))
												  .">".$DDCode."</option>";
										}
										
										$db->next_result();
										$resultBD->close();
									}
			 					?>
							</select>
						</td>
					</tr>
					<tr>
						<td> GL Code: </td>
						<td>
							<select name="sltGLCode" id="sltGLCode" onchange="showItem(), showUOM(51), showUnitPrice(51)">
								<?php
									if ( $initBMDDepartment || $budgetID ){
										$qryMT = "CALL sp_Budget_Dept_GL_Code_Dropdown(1)";
										$resultMT = mysqli_query($db, $qryMT);
										$processErrorMT = mysqli_error($db);

										if(!empty($processErrorMT))
										{
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_departmental.php"."</td><td>".$processErrorMT." near line 43.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultMT))
											{
												$DDMTID = $row["DDMTID"];
												$DDMTName = $row["DDMTName"];

												echo "<option value='".$DDMTID."'".
													  ($budgetID && $initBMDGLCode == NULL ? ( $QGLCodeID == $DDMTID ? "selected" : "" ) : ( $initBMDGLCode == $DDMTID ? "selected" : "" ))
													  .">".$DDMTName."</option>";
											}
											
											$db->next_result();
											$resultMT->close();
										}
									}
								?>
							</select>
						</td>
					</tr>
				</table>

				<table class="child_tables_form collapsed">
					<tr>
						<th rowspan="2"> Particulars </th>
						<th rowspan="2"> UOM </th>
						<th rowspan="2"> Unit Price </th>
						<th colspan="12"> Quantity </th>
						<th rowspan="2"> Remarks </th>
					</tr>
					<tr>
						<th> JAN </th>
						<th> FEB </th>
						<th> MAR </th>
						<th> APR </th>
						<th> MAY </th>
						<th> JUN </th>
						<th> JUL </th>
						<th> AUG </th>
						<th> SEP </th>
						<th> OCT </th>
						<th> NOV </th>
						<th> DEC </th>
					</tr>	

					<?php
						for ($i=0; $i < 50 ; $i++) { 
					?>
							<tr>
								<td>
									<select name="sltGLParticulars[<?php echo $i;?>]" id="sltGLParticulars<?php echo $i;?>" onchange="showUOM(<?php echo $i;?>), showUnitPrice(<?php echo $i;?>)">
										<?php
											$qryM = mysqli_prepare( $db, "CALL sp_Budget_GL_Particulars_Dropdown(?,?,1)" );
											mysqli_stmt_bind_param( $qryM, 'ii', ($budgetID && $initBMDGLCode == NULL ? $QDepartmentID : $initBMDDepartment), ($budgetID && $initBMDGLCode == NULL ? $QGLCodeID : $initBMDGLCode) );
											$qryM->execute();
											$resultM = mysqli_stmt_get_result( $qryM );
											$processErrorM = mysqli_error($db);
										
											if ($processErrorM){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_budget_monitoring_departmental.php'.'</td><td>'.$processErrorM.' near line 258.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($resultM))
												{
													$DDItemID = $row['DDItemID'];
													$DDItemName = $row['DDItemName'];
													
													echo "<option value='".$DDItemID."'".
														  ($budgetID && $count > $i && $initBMDParticulars[$i] == NULL ? ( $QGLParicularsID[$i] == $DDItemID ? "selected" : "" ) : ( $initBMDParticulars[$i] == $DDItemID ? "selected" : "" ))
														  .">".$DDItemName."</option>";
												}
												$db->next_result();
												$resultM->close();
											}
										?> 
									</select>
								</td>

								<td id="tdUOM<?php echo $i;?>">
									<?php
										if( $initBMDParticulars[$i] || $budgetID ){
											$qry = mysqli_prepare( $db, "CALL sp_Budget_GL_Particulars_Details_Query(?,?)" );
											mysqli_stmt_bind_param( $qry, 'ii', ($budgetID && $count > $i && $initBMDParticulars[$i] == NULL ? $QGLParicularsID[$i] : $initBMDParticulars[$i]), ($budgetID && $count > $i && $initBMDParticulars[$i] == NULL ? $QDepartmentID : $initBMDDepartment) );
											$qry->execute();
											$result = mysqli_stmt_get_result( $qry );
											$processError = mysqli_error($db);
										
											if ($processError){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_budget_monitoring_departmental'.'</td><td>'.$processError.' near line 289.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($result))
												{
													$MDUOMID = $row['MDUOMID'];
													$MDUOMCode = $row['MDUOMCode'];
													
													echo $MDUOMCode;
									?>
													<input type="hidden" name="hidUOM[]" value="<?php echo $MDUOMID;?>">
									<?php
												}
												$db->next_result();
												$result->close();
											}
										}
									?> 
								</td>

								<td id="tdUnitPrice<?php echo $i;?>">
									<?php
										if( $initBMDParticulars[$i] || $budgetID ){
											$qry = mysqli_prepare( $db, "CALL sp_Budget_GL_Particulars_Details_Query(?,?)" );
											mysqli_stmt_bind_param( $qry, 'ii', ($budgetID && $count > $i && $initBMDParticulars[$i] == NULL ? $QGLParicularsID[$i] : $initBMDParticulars[$i]), ($budgetID && $count > $i && $initBMDParticulars[$i] == NULL ? $QDepartmentID : $initBMDDepartment) );
											$qry->execute();
											$result = mysqli_stmt_get_result( $qry );
											$processError = mysqli_error($db);
										
											if ($processError){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_budget_monitoring_departmental'.'</td><td>'.$processError.' near line 321.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}
											else
											{
												while($row = mysqli_fetch_assoc($result))
												{
													$MDUnitPrice = $row['MDUnitPrice'];
													
									?>
													<input type="text" name="txtUnitPrice[<?php echo $i;?>]" 
														value="<?php echo ($budgetID && $count > $i && $initBMDParticulars[$i] == NULL ? $QUnitPrice[$i] : $initBMDUnitPrice[$i]);?>" 
														class="short_text_2">
									<?php
												}
												$db->next_result();
												$result->close();
											}
										}
									?> 
								</td>

								<td>
									<input type="text" name="txtJAN[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDJAN[$i] == NULL ? $QJAN[$i] : $initBMDJAN[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtFEB[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDFEB[$i] == NULL ? $QFEB[$i] : $initBMDFEB[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtMAR[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDMAR[$i] == NULL ? $QMAR[$i] : $initBMDMAR[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtAPR[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDAPR[$i] == NULL ? $QAPR[$i] : $initBMDAPR[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtMAY[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDMAY[$i] == NULL ? $QMAY[$i] : $initBMDMAY[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtJUN[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDJUN[$i] == NULL ? $QJUN[$i] : $initBMDJUN[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtJUL[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDJUL[$i] == NULL ? $QJUL[$i] : $initBMDJUL[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtAUG[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDAUG[$i] == NULL ? $QAUG[$i] : $initBMDAUG[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtSEP[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDSEP[$i] == NULL ? $QSEP[$i] : $initBMDSEP[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtOCT[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDOCT[$i] == NULL ? $QOCT[$i] : $initBMDOCT[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtNOV[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDNOV[$i] == NULL ? $QNOV[$i] : $initBMDNOV[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtDEC[]" class="short_text_2" 
										value="<?php echo ($budgetID && $count > $i && $initBMDDEC[$i] == NULL ? $QDEC[$i] : $initBMDDEC[$i]);?>">
								</td>

								<td>
									<input type="text" name="txtRemarks[]" class="long_text" 
										value="<?php echo ($budgetID && $count > $i && $initBMDRemarks[$i] == NULL ? $QRemarks[$i] : $initBMDRemarks[$i]);?>">
								</td>
									<input type="hidden" name="hidBudgetItemID[]" value="<?php echo ($budgetID && $count > $i ? $QBudgetItemID[$i] : 0 );?>">
									<input type="hidden" name="hidItemsCreatedId[]" value="<?php echo ( $budgetID && $count > $i && $QItemsCreatedID[$i] ? $QItemsCreatedID[$i] : $_SESSION["SESS_USER_ID"] );?>">
									<input type="hidden" name="hidItemsCreatedAt[]" value="<?php echo ( $budgetID && $count > $i && $QItemsCreatedAt[$i] ? $QItemsCreatedAt[$i] : date("Y-m-d H:i:s") );?>">
							</tr>
					<?php
						}
					?>
					<!-- <tbody id="tbExtraFields1"></tbody>	
					<tbody id="tbExtraFields2"></tbody>	
					<tbody id="tbExtraFields3"></tbody>	
					<tr class="align_bottom">
						<td colspan="16">
							<input type="button" value=" + " id="btnAdd" onclick="showExtraFields()">
							<input type="hidden" value="1" name="hidAdd" id="hidAdd">
						</td>
					</tr>	 -->		
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveDept" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='budget_monitoring_departmental.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidBudgetID" id="hidBudgetID" value="<?php echo $budgetID;?>">
							<input type="hidden" id="hidCounter" value="<?php echo ( $budgetID ? "" : 0 );?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $budgetID ? $QCreatedID : $_SESSION["SESS_USER_ID"] );?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $budgetID ? $QCreatedAt : date("Y-m-d H:i:s") );?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>