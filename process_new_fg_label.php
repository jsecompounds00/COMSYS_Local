<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_fg_label.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidFGID = $_POST['hidFGID'];
		$txtFGabel = $_POST['txtFGabel'];

########## Input Validations
		if ( $txtFGabel == "" ){
			$errmsg_arr[] = "* Label can't be blank.";
			$errflag = true;
		}

		$updatedAt = date('Y-m-d H:i:s');
		$updatedId = $_SESSION['SESS_USER_ID'];

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_fg_label.php?id=$hidFGID");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Finished_Goods_Label_CRU(?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'issi', $hidFGID, $txtFGabel, $updatedAt, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_fg_label.php'.'</td><td>'.$processError.' near line 77.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		if($hidFGID)
			$_SESSION['SUCCESS']  = 'Successfully updated finished goods - label.';
		else
			$_SESSION['SUCCESS']  = 'Successfully added new finished goods - label.';
		//echo $_SESSION['SUCCESS'];
		header("location: finished_goods.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
	}	
		unset($_SESSION['page']);
		unset($_SESSION['search']);
		unset($_SESSION['qsone']);
############# Committing in Database

		require("include/database_close.php");

	}
?>