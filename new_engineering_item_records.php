<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_engineering_item_records.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$recordID = $_GET['id'];

				if($recordID)
				{ 
					echo "<title>Engineering Item Record - Edit</title>";

					$qry = mysqli_prepare( $db, "CALL sp_Engineering_Item_Record_Query( ? )" );
					mysqli_stmt_bind_param( $qry, 'i', $recordID );
					$qry->execute();
					$result = mysqli_stmt_get_result( $qry );
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_engineering_item_records.php'.'</td><td>'.$processError.' near line 38.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$eng_item_id = $row['eng_item_id'];
							$record_date = $row['record_date'];
							$comments = $row['comments'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

					############ .............
					$qryPI = "SELECT id from comsys.engineering_items_records";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_engineering_item_records.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($recordID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_engineering_item_records.php</td><td>The user tries to edit a non-existing enginering item records.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

				}
				else
				{
					echo "<title>Engineering Item Record - Add</title>";
				}
			}
		?>
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>

		<form method='post' action='process_new_engineering_item_records.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $recordID ? "Edit " : "New " ); ?> Record </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Engineering Item:</td>
						<td>
							<select name='sltEngItem'>
								<?php 
									$qryEI = "CALL sp_Engineering_Item_Dropdown()";
									$resultEI = mysqli_query($db, $qryEI);
									$processErrorEI = mysqli_error($db);

									if( !empty($processErrorEI) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_engineering_item_records.php'.'</td><td>'.$processErrorEI.' near line 120.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{

										while( $row = mysqli_fetch_assoc($resultEI) ){
											$item_id = $row['id'];
											$item = $row['name'];

											if ( $recordID && $eng_item_id == $item_id ){
												echo "<option value='".$item_id."' selected>".$item."</option>";
											}elseif(!$recordID){
												if ( $EngItem == $item_id )
													echo "<option value='".$item_id."'selected>".$item."</option>";
												else
													echo "<option value='".$item_id."'>".$item."</option>";
											}
										}
										$db->next_result();
										$resultEI->close();
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Record Date:</td>
						<td>
							<input type='text' name='txtRecordDate' id='txtRecordDate' value="<?php echo ( $recordID ? $record_date : $RecordDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtRecordDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr>
						<td valign='top'>Comments:</td>
						<td>
							<textarea name='txtComments' class="paragraph"><?php
								if( $recordID ){
									$comments_array = explode("<br>", $comments);

									foreach ($comments_array as $comments_key => $comments_value) {
										echo $comments_value."\n";
									}
								}else{
									$Comments_array = explode("<br>", $Comments);

									foreach ($Comments_array as $Comments_key => $Comments_value) {
										echo $Comments_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr class="align_bottOm">
						<td>
							<input type="submit" name="btnSaveRecord" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='engineering_item_records.php?page=1&search=&qsone='">
							<input type='hidden' name='hidRecordID' value="<?php echo $recordID;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $recordID ? $createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $recordID ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>