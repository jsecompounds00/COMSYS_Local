<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_equipment.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidEquipmentId = $_POST['hidEquipmentId'];
		$txtCode = clean($_POST['txtCode']);
		$sltStatus = clean($_POST['sltStatus']);
		$txtEquipment = clean($_POST['txtEquipment']);
		$sltMeasure = clean($_POST['sltMeasure']);
		$txtModel = clean($_POST['txtModel']);
		$sltLocation = clean($_POST['sltLocation']);
		$txtTolerancePercent = clean($_POST['txtTolerancePercent']);
		$txtToleranceValue = clean($_POST['txtToleranceValue']);
		$txtInputRange = clean($_POST['txtInputRange']);
		$txtOutputRange = clean($_POST['txtOutputRange']);
		$sltFrequencyValidation = clean($_POST['sltFrequencyValidation']);
		$sltFrequencyCalibration = clean($_POST['sltFrequencyCalibration']);
		$txtComments = clean($_POST['txtComments']);
		
		$txtComments = str_replace('\\r\\n', '<br>', $txtComments);

		$txtComments = str_replace('\\', '', $txtComments);

		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;

########## Input Validations
		if ( empty($txtCode) ){
			$errmsg_arr[] = '*Code is invalid.';
			$errflag = true;
		}
		if ( !$sltStatus ){
			$errmsg_arr[] = '*Status is missing.';
			$errflag = true;
		}
		if ( empty($txtEquipment) || is_numeric($txtEquipment) ){
			$errmsg_arr[] = '*Equipment is invalid.';
			$errflag = true;
		}
		if ( !$sltMeasure ){
			$errmsg_arr[] = '*Measure is missing.';
			$errflag = true;
		}
		if ( is_numeric($txtModel) ){
			$errmsg_arr[] = '*Brand Model is invalid.';
			$errflag = true;
		}
		if ( !$sltLocation ){
			$errmsg_arr[] = '*Location is missing.';
			$errflag = true;
		}
		if ( !empty($txtTolerancePercent) && !is_numeric($txtTolerancePercent) ){
			$errmsg_arr[] = '*Tolerance Percentage is invalid.';
			$errflag = true;
		}
		if ( !empty($txtToleranceValue) && !is_numeric($txtToleranceValue) ){
			$errmsg_arr[] = '*Tolerance Value is invalid.';
			$errflag = true;
		}
		if ( !empty($txtInputRange) && !is_numeric($txtInputRange) ){
			$errmsg_arr[] = '*Input Range is invalid.';
			$errflag = true;
		}
		if ( !empty($txtOutputRange) && !is_numeric($txtOutputRange) ){
			$errmsg_arr[] = '*Output Range is invalid.';
			$errflag = true;
		}
		if ( !$sltFrequencyValidation && !$sltFrequencyCalibration ){
			$errmsg_arr[] = '*At least one frequency is required.';
			$errflag = true;
		}

		if($hidEquipmentId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidEquipmentId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# SESSION, keeping last input value
		$_SESSION['txtCode'] = $txtCode;
		$_SESSION['sltStatus'] = $sltStatus;
		$_SESSION['txtEquipment'] = $txtEquipment;
		$_SESSION['sltMeasure'] = $sltMeasure;
		$_SESSION['txtModel'] = $txtModel;
		$_SESSION['sltLocation'] = $sltLocation;
		$_SESSION['txtTolerancePercent'] = $txtTolerancePercent;
		$_SESSION['txtToleranceValue'] = $txtToleranceValue;
		$_SESSION['txtInputRange'] = $txtInputRange;
		$_SESSION['txtOutputRange'] = $txtOutputRange;
		$_SESSION['sltFrequencyValidation'] = $sltFrequencyValidation;
		$_SESSION['sltFrequencyCalibration'] = $sltFrequencyCalibration;
		$_SESSION['txtComments'] = $txtComments;
		$_SESSION['chkActive'] = $chkActive;

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_EQUIPMENT.$hidEquipmentId);
			exit();
		}
########## Committing to Database
		$qry = mysqli_prepare($db, "CALL sp_Equipment_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'isssddiiissssisssii', $hidEquipmentId, $txtCode, $txtEquipment, $txtModel, $txtTolerancePercent
									 , $txtToleranceValue, $sltLocation, $sltStatus, $sltMeasure, $txtInputRange
									 , $txtOutputRange, $sltFrequencyValidation, $sltFrequencyCalibration, $chkActive, $txtComments, $createdAt
									 , $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_equipment.php'.'</td><td>'.$processError.' near line 139.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidEquipmentId)
				$_SESSION['SUCCESS']  = 'Successfully updated equipment.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new equipment.';
			//echo $_SESSION['SUCCESS'];
			header("location: equipment.php?page=".$_SESSION['page']."&name_text=".$_SESSION['name_text']."&code_text=".$_SESSION['code_text']."&type_text=".$_SESSION['type_text']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>