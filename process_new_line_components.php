<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_line_components.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidComponentID = $_POST['hidComponentID'];
		$txtName = clean($_POST['txtName']);
		// $radMachineType = clean($_POST['radMachineType']);
		$txtRemarks = clean($_POST['txtRemarks']);
			
		$txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		
		$txtRemarks = str_replace('\\', '', $txtRemarks);

		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;

		if(isset($_POST['radMachineType']) && $_POST['radMachineType'] == 'M' )
			$radMachineType = 'Mixer';
		elseif(isset($_POST['radMachineType']) && $_POST['radMachineType'] == 'E' )
			$radMachineType = 'Extruder';

########## Input Validations
		if ( $txtName == '' ){
			$errmsg_arr[] = '* Component name is invalid.';
			$errflag = true;
		}
		if ( !$radMachineType ){
			$errmsg_arr[] = '* Choose machine type.';
			$errflag = true;
		}

		if($hidComponentID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidComponentID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

########## Input Validations (Rated Capacity)
	
		foreach ($_POST['hidLineComponentCapacityID'] as $LineComponentCapacityID) {
			$LineComponentCapacityID = array();
			$LineComponentCapacityID = $_POST['hidLineComponentCapacityID'];
		}
		foreach ($_POST['hidMachineID'] as $MachineID) {
			$MachineID = array();
			$MachineID = $_POST['hidMachineID'];
		}
		foreach ($_POST['txtRatedCapacity'] as $RatedCapacity) {
			$RatedCapacity = array();
			$RatedCapacity = $_POST['txtRatedCapacity'];

		}

		$ctr = count($MachineID);
		$i = 0;

		do{
			if ( $RatedCapacity[$i] != '' && !is_numeric($RatedCapacity[$i]) ){
				$errmsg_arr[] = '* Invalid rated capacity.';
				$errflag = true;
			}elseif( $RatedCapacity[$i] == '' ){
				$RatedCapacity[$i] = 0;
			}
			$i++;
		}while ( $i < $ctr );

############# SESSION, keeping last input value
		$_SESSION['txtName'] = $txtName;
		$_SESSION['radMachineType'] = $radMachineType;
		$_SESSION['chkActive'] = $chkActive;
		$_SESSION['txtRemarks'] = $txtRemarks;

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_line_components.php?id=$hidComponentID");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Line_Component_CRU(?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'issis', $hidComponentID, $txtName, $radMachineType, $chkActive, $txtRemarks);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_line_components.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidComponentID ) {
				$ComponentID = $hidComponentID;
			}else{
				$ComponentID = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['hidMachineID'] as $key => $temValue)
			{	
				if (!$hidComponentID) {
					$LineComponentCapacityID = 0;
				}
				if ($temValue)
				{
					echo $MachineID[$key];
					$qryItems = mysqli_prepare($db, "CALL sp_Line_Component_Capacity_CRU( ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiidssii', $LineComponentCapacityID[$key], $ComponentID, $MachineID[$key]
																, $RatedCapacity[$key], $createdAt, $updatedAt, $createdId
																, $updatedId);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_line_components.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidComponentID )
							$_SESSION['SUCCESS']  = 'Line Component successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Line Component successfully added.';
						header("location: line_components.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database

		require("include/database_close.php");

	}
?>