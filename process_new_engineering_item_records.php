<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_engineering_item_records.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidRecordID = $_POST['hidRecordID'];
		$sltEngItem = clean($_POST['sltEngItem']);
		$txtRecordDate = clean($_POST['txtRecordDate']);
		$txtComments = clean($_POST['txtComments']);
	
		$txtComments = str_replace('\\r\\n', '<br>', $txtComments);
		
		$txtComments = str_replace('\\', '', $txtComments);

		$_SESSION['sltEngItem'] = $sltEngItem;
		$_SESSION['txtRecordDate'] = $txtRecordDate;
		$_SESSION['txtComments'] = $txtComments;

		//Input Validations
		if ( !$sltEngItem ){
			$errmsg_arr[] = '* Invalied Engineering Item.';
			$errflag = true;
		}
		$valRecordDate = validateDate($txtRecordDate, 'Y-m-d');
		if ( $valRecordDate != 1 ){
			$errmsg_arr[] = '* Invalid Record Date.';
			$errflag = true;
		}
		if ( $txtComments == '' ){
			$errmsg_arr[] = '* Comments cannot be blank.';
			$errflag = true;
		}

		if($hidRecordID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidRecordID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:new_engineering_item_records.php?id=$hidRecordID");
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Engineering_Item_Record_CRU(?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'iissssii', $hidRecordID, $sltEngItem, $txtRecordDate, $txtComments
											   , $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_engineering_item_records.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidRecordID)
				$_SESSION['SUCCESS']  = 'Successfully updated record.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new record.';
			//echo $_SESSION['SUCCESS'];
			header("location:engineering_item_records.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	