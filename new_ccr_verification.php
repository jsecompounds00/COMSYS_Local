<html>
	<head>
		<script src="js/jscript.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_verification.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$ccrID = $_GET['id'];
				$verificationID = $_GET['vid'];

				$qryCN = mysqli_prepare($db, "CALL sp_CCR_Query(?)");
				mysqli_stmt_bind_param($qryCN, 'i', $ccrID);
				$qryCN->execute();
				$resultCN = mysqli_stmt_get_result($qryCN); //return results of query
				$processErrorCN = mysqli_error($db);

				if(!empty($processErrorCN))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_verification.php'.'</td><td>'.$processErrorCN.' near line 35.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{	
					while($row = mysqli_fetch_assoc($resultCN))
					{
						$ccr_no = $row['ccr_no'];
					}
					$db->next_result();
					$resultCN->close();
				}

				if($verificationID)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_CCR_Verification_Query(?, ?)");
					mysqli_stmt_bind_param($qry, 'ii', $ccrID, $verificationID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_verification.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							$verification_date = $row['verification_date'];
							$verified_by = $row['verified_by'];
							$verification = htmlspecialchars($row['verification']);
							$status = $row['status'];
							$next_verification_month = $row['next_verification_month'];
							$next_verification_year = $row['next_verification_year'];
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];
							$department_id = $row['department_id'];
							$division = $row['division'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.ccr_monitoring_verification WHERE ccr_monitoring_id = $ccrID";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_verification.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($verificationID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_ccr_verification.php</td><td>The user tries to edit a non-existing ccr_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>CCR Status - Update</title>";

				}
				else{

					echo "<title>CCR Status - Update</title>";

				}
			}
		?>
	</head>
	<body <?php if( !$verificationID ){ ?> onload="showRecDepartment(0,0)" <?php } ?>>

		<form method='post' action='process_new_ccr_verification.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/ccr_verification_init_value.php");

				if ( $verificationID ){
					if( $_SESSION['edit_ccr_verification'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['update_ccr'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">
				
				<span> <h3> Update CCR No. <?php echo $ccr_no;?> Status </h3> </span>
				
				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<colgroup> <col width="250px"></col> <col width="100px"></col> </colgroup>
					<tr>
						<td>Verification Date:</td>

						<td colspan="2">
							<input type='text' name='txtVerifyDate' id='txtVerifyDate' value='<?php echo ( $verificationID ? $verification_date : $initCCVVerifyDate );?>'>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtVerifyDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr class="spacing">
						<td> Division: </td>

						<td colspan="2">
							<input type='radio' name='radDivision' id='ToCompounds' value='Compounds'
								onchange='showRecDepartment(0,0), showCPIARVerifier(0, 0, 0)'
								<?php echo ( $verificationID ? ( $division == "Compounds" ? "checked" : "" ) : ( $initCCVDivision == "Compounds" ? "checked" : "" ) );?>>
								<label for='ToCompounds'>Compounds</label>

							<input type='radio' name='radDivision' id='ToCorporate' value='Corporate' 
								onchange='showRecDepartment(0,0), showCPIARVerifier(0, 0, 0)'
								<?php echo ( $verificationID ? ( $division == "Corporate" ? "checked" : "" ) : ( $initCCVDivision == "Corporate" ? "checked" : "" ) );?>>
								<label for='ToCorporate'>Corporate</label>

							<input type='radio' name='radDivision' id='ToPipes' class="outcast">
						</td>
					</tr>

					<tr>
						<td>Area:</td>

						<td>
							<select name='sltDeptID' id='sltReceiveDeptID' onchange='showCPIARVerifier(0, this.value, 0)'>
								<?php
									if ( $verificationID ){
										$qryID = mysqli_prepare($db, "CALL sp_Department_Dropdown_New(?, 1)");
										mysqli_stmt_bind_param($qryID, 's', $division);
										$qryID->execute();
										$resultID = mysqli_stmt_get_result($qryID);
										$processErrorID = mysqli_error($db);
										if ( !empty($processErrorID) ){
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_cpiar_monitoring.php'.'</td><td>'.$processErrorID.' near line 61.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}else{
											while($row = mysqli_fetch_assoc($resultID)){
												if ( $department_id == $row['id'] ){
													echo "<option value='".$row['id']."' selected>".$row['code']."</option>";
												}else{
													echo "<option value='".$row['id']."'>".$row['code']."</option>";
												}
											}
										}
										$db->next_result();
										$resultID->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td>Verified by:</td>

						<td>
							<select name='sltAuditorID' <?php echo ( $verificationID ? "" : "id='sltAuditorID'" );?>>
								<?php
									if ( $verificationID ){
										$zero = 0;
										$one = 1;

										$qryII = mysqli_prepare( $db, "CALL sp_User_Dropdown(?, ?, ?, ?)" );
										mysqli_stmt_bind_param( $qryII, 'iiis', $department_id, $zero, $one, $division );
										$qryII->execute();
										$resultII = mysqli_stmt_get_result($qryII);
										$processErrorII = mysqli_error($db);
										if ( !empty($processErrorII) ){
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_cpiar_monitoring.php'.'</td><td>'.$processErrorII.' near line 61.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}else{
											while($row = mysqli_fetch_assoc($resultII)){

													if ( $verified_by == $row['user_id'] ){
														echo "<option value='".$row['user_id']."' selected>".$row['User']."</option>";											
													}
													else{
														echo "<option value='".$row['user_id']."'>".$row['User']."</option>";
													}

											}
											$db->next_result();
											$resultII->close();
										}
									}
								?>
							</select>
						</td>
					</tr>

					<tr valign="top">
						<td> Comments: </td>

						<td colspan="2">
							<textarea class="paragraph" name='txtVerification'><?php
								if ( $verificationID ){
									echo $verification;
								}else{
									echo $initCCVVerification;
								}
							?></textarea>
						</td>
					</tr>

					<tr valign='top'>
						<td> Status: </td>

						<td colspan="2">
							<dt>
								<?php
									$qryCS = "SELECT * FROM cpiar_status";
									$resultCS = mysqli_query($db, $qryCS);
									$processErrorCS = mysqli_error($db);
									if ( !empty($processErrorCS) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_cpiar_verification.php'.'</td><td>'.$processErrorCS.' near line 275.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										while( $row = mysqli_fetch_assoc($resultCS) ){
											$cs_id = $row['id'];
											$cs_name = $row['c_status'];

											if ( $verificationID ){
												if( $status == $cs_id ){
													echo "<input type='radio' name='chkStatus' id='".$cs_name."' value='".$cs_id."' checked> <label for='".$cs_name."'>".$cs_name."</label> <br>";
												}else{
													echo "<input type='radio' name='chkStatus' id='".$cs_name."' value='".$cs_id."'> <label for='".$cs_name."'>".$cs_name."</label> <br>";
												}		
											}else{
												if( $initCCVStatus == $cs_id ){
													echo "<input type='radio' name='chkStatus' id='".$cs_name."' value='".$cs_id."' checked> <label for='".$cs_name."'>".$cs_name."</label> <br>";
												}else{
													echo "<input type='radio' name='chkStatus' id='".$cs_name."' value='".$cs_id."'> <label for='".$cs_name."'>".$cs_name."</label> <br>";
												}
											}
										}
										$db->next_result();
										$resultCS->close();
									}
								?>
							</dt>
						</td>
					</tr>
					
					<tr>
						<td rowspan="3"> Next Verification Date <br>(For "follow-up verification" only): </td>
					</tr>

					<tr>
						<td> Month: </td>
						<td>
							<select name='sltMonth'>
								<?php
									$qryM = "SELECT * FROM months ORDER BY equal_int";
									$resultM = mysqli_query($db, $qryM);
									$processErrorM = mysqli_error($db);
									if ( !empty($processErrorM) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_cpiar_verification.php'.'</td><td>'.$processErrorM.' near line 275.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										echo "<option></option>";
										while( $row = mysqli_fetch_assoc($resultM) ){
											$month_id = $row['equal_int'];
											$month_name = $row['month'];


											if ( $verificationID ){
												if( $next_verification_month == $month_id ){
													echo "<option value='".$month_id."' selected>".$month_name."</option>";
												}else{
													echo "<option value='".$month_id."'>".$month_name."</option>";
												}		
											}else{
												if( $initCCVMonth == $month_id ){
													echo "<option value='".$month_id."' selected>".$month_name."</option>";
												}else{
													echo "<option value='".$month_id."'>".$month_name."</option>";
												}		
											}
											

										}
										$db->next_result();
										$resultM->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td> Year: </td>
						<td>
							<select name='sltYear'>
								<?php
									$qryM = "SELECT * FROM years";
									$resultM = mysqli_query($db, $qryM);
									$processErrorM = mysqli_error($db);
									if ( !empty($processErrorM) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_cpiar_verification.php'.'</td><td>'.$processErrorM.' near line 275.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
										echo "<option></option>";
										while( $row = mysqli_fetch_assoc($resultM) ){
											$year = $row['year'];

											if ( $verificationID ){
												if( $next_verification_year == $year ){
													echo "<option value='".$year."' selected>".$year."</option>";
												}else{
													echo "<option value='".$year."'>".$year."</option>";
												}		
											}else{
												if( $initCCVYear == $year ){
													echo "<option value='".$year."' selected>".$year."</option>";
												}else{
													echo "<option value='".$year."'>".$year."</option>";
												}		
											}

										}
										$db->next_result();
										$resultM->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='ccr_monitoring.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidCCRID' value="<?php echo $ccrID;?>">
							<input type='hidden' name='hidVerificationID' value='<?php echo $verificationID;?>'>
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $verificationID? $created_at : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $verificationID ? $created_id : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>