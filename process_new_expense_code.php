<?php
############# Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_credit_payment.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitize the POST values
		$hidEXPID = $_POST['hidEXPID'];
		$txtCode = $_POST['txtCode'];
		$txtDescription = $_POST['txtDescription'];
		$sltUOM = NULL;
		$chkActive = ($_POST['chkActive'] ? 1 : 0);
		$chkApplicableTo = $_POST['chkApplicableTo'];
		$chkMiscellaneous = ($_POST['chkMiscellaneous'] ? 1 : 0);

		$createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		$createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		$i = 0;

############# Input Validations		
		if ( $txtCode == "" ){
			$errmsg_arr[] = "* Code can't be blank.";
			$errflag = true;
		}	
		if ( $txtDescription == "" ){
			$errmsg_arr[] = "* Description can't be blank.";
			$errflag = true;
		}
		// if ( $chkMiscellaneous ){
		// 	$sltUOM = NULL;
		// }elseif ( !$sltUOM ){
		// 	$errmsg_arr[] = "* Select unit of measures.";
		// 	$errflag = true;
		// }



############# SESSION

		$_SESSION['SESS_EXP_Code'] = $txtCode;
		$_SESSION['SESS_EXP_Description'] = $txtDescription;
		$_SESSION['SESS_EXP_Active'] = $chkActive;
		$_SESSION['SESS_EXP_Department'] = $chkApplicableTo;
		$_SESSION['SESS_EXP_Miscellaneous'] = $chkMiscellaneous;


############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_expense_code.php?id=".$hidEXPID);
			exit();
		}

		$qryDel = mysqli_prepare($db, "CALL sp_Budget_Expense_Code_CRU_Delete(?)");
		mysqli_stmt_bind_param($qryDel, 'i', $hidEXPID);

		$qryDel->execute();
		$result = mysqli_stmt_get_result($qryDel);
		$processError = mysqli_error($db);

		if ( !empty($processError) ) {
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_expense_code.php'.'</td><td>'.$processError.' near line 180.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}			
		

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Budget_Expense_Code_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'issiisisi', $hidEXPID, $txtCode, $txtDescription, $chkActive
											 ,$chkMiscellaneous , $createdAt, $createdId, $updatedAt, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_expense_code.php'.'</td><td>'.$processError.' near line 180.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		if( isset($_POST['chkApplicableTo']) ){}
		else{
			if ( $hidEXPID )
				$_SESSION['SUCCESS']  = 'Expense code was successfully updated.';
			else
				$_SESSION['SUCCESS']  = 'Expense code was successfully added.';
			header("location: expense_code.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidEXPID ) {
				$ExpenseID = $hidEXPID;
			}else{
				$ExpenseID = $row['LAST_INSERT_ID()'];
			}	
		}
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['chkApplicableTo'] as $key => $itemValue)
			{	
				if (!$ExpenseID) {
					$ExpenseItemID = 0;
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Budget_Expense_Code_Items_CRU( ?, ?)");
					mysqli_stmt_bind_param($qryItems, 'ii', $ExpenseID, $chkApplicableTo[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_expense_code.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidEXPID )
							$_SESSION['SUCCESS']  = 'Expense code was successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Expense code was successfully added.';
						header("location: expense_code.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}
############# Committing in Database	
		require("include/database_close.php");
	}	
?>
	