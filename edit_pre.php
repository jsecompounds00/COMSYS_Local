<html>
	<head>
		<script src="js/datetimepicker_css.js"></script> 
		<title> Edit P.R.E. </title>

		<?php
			require("/include/database_connect.php");
		?>
	</head>
	<body>

		<form method='post' action='process_edit_pre.php'>

			<?php   
				require("/include/header.php");
				require("/include/init_value.php");     
				
				if( $_SESSION['edit_pre'] == false) 
				{
					$_SESSION['ERRMSG_ARR'] ='Access denied!';
					session_write_close();
					header("Location:comsys.php");
					exit();
				}   
			?>    

			<div class="wrapper">
				
				<span> <h3> Edit P.R.E. </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="home_pages">
					<tr></tr>
					<tr>
						<th>Item Type</th>
						<th>PRE No.</th>
						<th>PRE Date</th>
						<th>Received Date</th>
						<th>Approved Date</th>
					</tr>
					<?php
						if(!empty($errno))
						{
							$error = mysqli_connect_error();
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pre_monitoring.php'.'</td><td>'.$error.' near line 60.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							$qry = "CALL sp_PRE_Dropdown_Received_Approved_Date()";
							$result = mysqli_query($db, $qry);
							$processError = mysqli_error( $db );
							if(!empty($processError))
							{
								error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pre_monitoring.php'.'</td><td>'.$processError.' near line 86.</td></tr>', 3, "errors.php");
								header("location: error_message.html");
							}
							else
							{
								$i = 0;
								while ( $row = mysqli_fetch_assoc( $result ) ){
					?>
									<tr>
										<td>
											<?php echo $row["ItemType"]; ?> 
										</td>

										<td>
											<input type='hidden' name='hidPREID[]' id='hidPREID<?php echo $i;?>' value='<?php echo $row['pre_id']; ?>'> 
											<input type='hidden' name='hidPRENumber[]' id='hidPRENumber<?php echo $i;?>' value='<?php echo $row['pre_number']; ?>'> 
											<?php echo $row["pre_number"]; ?> 
										</td>

										<td>
											<?php echo $row["pre_date"]; ?> 
										</td>

										<td> 
											<input type='text' name='txtReceivedDate[]' id='txtReceivedDate<?php echo $i;?>' value='<?php echo $row["date_received"];?>'>
											<img src="js/cal.gif" onclick="javascript:NewCssCal('txtReceivedDate<?php echo $i;?>')" style="cursor:pointer" name="picker" /> 
										</td>

										<td> 
											<input type='text' name='txtApprovedDate[]' id='txtApprovedDate<?php echo $i;?>' value='<?php echo $row["approved_date"];?>'>
											<img src="js/cal.gif" onclick="javascript:NewCssCal('txtApprovedDate<?php echo $i;?>')" style="cursor:pointer" name="picker" /> 
										</td>
									</tr>
					<?php
									$i++;
								}
								$db->next_result();
								$result->close();
							}
						}
					?>
					<tr></tr>
				</table>
				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$pre_number  = htmlspecialchars($_SESSION["pre_number"]);
								$item  = htmlspecialchars($_SESSION["item"]);
								$pre_date = htmlspecialchars($_SESSION["pre_date"]);
								$pre_number = ( (strpos(($pre_number), "\\")+1) > 0 ? str_replace("\\", "", $pre_number) : $pre_number );
								$pre_number = ( (strpos(($pre_number), "'")+1) > 0 ? str_replace("'", "\'", $pre_number) : $pre_number );
								$item = ( (strpos(($item), "\\")+1) > 0 ? str_replace("\\", "", $item) : $item );
								$item = ( (strpos(($item), "'")+1) > 0 ? str_replace("'", "\'", $item) : $item );
								$pre_date = ( (strpos(($pre_date), "\\")+1) > 0 ? str_replace("\\", "", $pre_date) : $pre_date );
								$pre_date = ( (strpos(($pre_date), "'")+1) > 0 ? str_replace("'", "\'", $pre_date) : $pre_date );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='pre_monitoring.php?page=<?php echo $page;?>&pre_number=<?php echo $pre_number;?>&item=<?php echo $item;?>&pre_date=<?php echo $pre_date;?>'">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>