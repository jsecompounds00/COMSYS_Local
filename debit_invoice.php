<html>
	<head>
		<!--<script src="js/jscript.js"></script>-->
		<script src="js/debit_invoice_js.js"></script>
  		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>debit_invoice.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$debitId = $_GET["id"];

				if($debitId)
				{ 
					$transType = $_GET['type'];

					echo "<title>Debit: Invoice - Edit</title>";

					$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_Query( ? )");
					mysqli_stmt_bind_param($qry, 'i', $debitId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if ( !empty($processError) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_invoice.php'.'</td><td>'.$processError.' near line 12.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						$db_jacket_item_id = array();
						$db_invoice_number = array();
						$db_check_number = array();
						$db_amount = array();
						$db_discount = array();
						$db_bank_code_id = array();
						$db_bank_code = array();
						while($row = mysqli_fetch_assoc($result)){
							$db_jacket_id = $row['jacket_id'];
							$db_process_type = $row['process_type'];
							$db_customer_id = $row['customer_id'];
							$db_transaction_type = $row['transaction_type'];
							$db_invoice_date = $row['invoice_date'];
							$db_check_returned_date = $row['check_returned_date'];
							$db_check_date = $row['check_date'];
							$db_created_at = $row['created_at'];
							$db_created_id = $row['created_id'];
							$db_name = $row['name'];

							$db_jacket_item_id[] = $row['jacket_item_id'];
							$db_invoice_number[] = $row['invoice_number'];
							$db_check_number[] = $row['check_number'];
							$db_amount[] = $row['amount'];
							$db_discount[] = $row['discount'];
							$db_bank_code_id[] = $row['bank_code_id'];
							$db_bank_code[] = $row['bank_code'];
						}
						$db->next_result();
						$result->close();
						$count = count( $db_jacket_item_id );
					}

					############ .............
					$qryPI = "SELECT id from comsys.customer_jackets WHERE process_type='add'";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_invoice.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($debitId, $id, TRUE) && $transType == 'di' ){
						if ( $transType == 'dm' ){
							header("location: debit_memo.php?id=".$debitId."&type=dm");
						}elseif ( $transType == 'cp' ){
							header("location: credit_payment.php?id=".$debitId."&type=cp");
						}elseif ( $transType == 'cm' ){
							header("location: credit_memo.php?id=".$debitId."&type=cm");
						}else{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_invoice.php</td><td>The user tries to edit a non-existing jacket_id.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
					}
				}
				else
				{
				
					echo "<title>Debit: Invoice - Add</title>";
				}
			}
		?>
	</head>
	<body onload="changeTransactionType()" onmouseover="showInvoices('<?php echo $debitId;?>'), showAmounts('<?php echo $debitId;?>'), showDiscounts('<?php echo $debitId;?>')">

		<form method='post' action='process_debit_invoice.php'>

			<?php 
				require("/include/header.php");
				require("/include/init_unset_values/debit_invoice_init_value.php");

				if( $debitId ){
					if( $_SESSION['edit_debit_invoice'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['debit_invoice'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $debitId ? "Edit Debit: Invoice" : "New Debit: Invoice" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Customer:</td>

						<td>
							<?php
								if ( $debitId ) {
									echo '<b>'.$db_name.'</b>';
							?>		<input type="hidden" name="sltCustomer" id="sltCustomer" value="<?php echo $db_customer_id; ?>">
							<?php		
								}else{
							?>
									<select name="sltCustomer" id="sltCustomer">
										<option></option>
										<?php
											$qry = "CALL sp_Customer_Dropdown()";
											$result = mysqli_query($db, $qry);
											$processError = mysqli_error($db);

											if ( !empty($processError) ){
												error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>debit_invoice.php"."</td><td>".$processError." near line 12.</td></tr>", 3, "errors.php");
												header("location: error_message.html");
											}else{
												while($row = mysqli_fetch_assoc($result)){
													$customer_id = $row["id"];
													$customer_name = $row["name"];

													if ( $customer_id ==  $initCJDICustomer){
														echo "<option value='".$customer_id."' selected>".$customer_name."</option>";
													}else{
														echo "<option value='".$customer_id."'>".$customer_name."</option>";
													}
												}
												$db->next_result();
												$result->close();
											}
										?>
									</select>
							<?php
								}
							?>
						</td>
					</tr>

					<tr>
						<td>Transaction Type:</td>

						<td>
							<select name="sltTransType" id="sltTransType" onchange="changeTransactionType()"><!-- onchange="enableCheck()" -->
								<option value="IAR" <?php echo ( $debitId ? ( $db_transaction_type == "IAR" ? "selected" : "disabled" ) : ( $initCJDITransType == "IAR" ? "selected" : "" )  );?>>
									Invoice AR
								</option>

								<option value="PRC" <?php echo ( $debitId ? ( $db_transaction_type == "PRC" ? "selected" : "disabled" ) : ( $initCJDITransType == "PRC" ? "selected" : "" )  );?>>
									PMT-RC
								</option>
							</select>
						</td>
					</tr>

					<tr>
						<td>Invoice Date:</td>

						<td> 
							<input type="text" name="txtInvoiceDate" id="txtInvoiceDate" value="<?php echo ( $debitId ? $db_invoice_date : $initCJDIInvoiceDate );?>"> 
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtInvoiceDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
						<td>Check Returned Date:</td>
						<td> 
							<input type="text" name="txtCheckReturnedDate" id="txtCheckReturnedDate" value="<?php echo ( $debitId ? $db_check_returned_date : $initCJDICheckReturnedDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtCheckReturnedDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
				</table>

				<table class="child_tables_form" id="tbDebitInvoiceItems">
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveCust" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='customer_jacket.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidDebitId" id="hidDebitId" value="<?php echo $debitId; ?>">
							<input type="hidden" name="hidProcessType" value="add">
							<input type="hidden" name="hidJacketType" value="di">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $debitId ? $db_created_at : date("Y-m-d H:i:s") );?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $debitId ? $db_created_id : $_SESSION["SESS_USER_ID"] );?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php 
			require("include/database_close.php");
		?>
	</footer>
</html>
