<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_forecast.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidForecastID = $_POST['hidForecastID'];
		$txtMonthYear = $_POST['txtMonthYear'];
		$txtRemarks = $_POST['txtRemarks'];

		// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		
		// $txtRemarks = str_replace('\\', '', $txtRemarks);

		$radFGType = $_POST['radFGType'];
		if ( isset( $radFGType ) && $radFGType == 'Local' ){
			$radFGType = 1;
		}elseif ( isset( $radFGType ) && $radFGType == 'Export' ){
			$radFGType = 0;
		}

########## Input Validations
		$valtxtMonthYear = validateDate($txtMonthYear, 'Y-m-d');
		if ( $valtxtMonthYear != 1 ){
			$errmsg_arr[] = '* Invalid Forecast date.';
			$errflag = true;
		}
		if ( !isset( $radFGType ) ){
			$errmsg_arr[] = '* Choose FG type.';
			$errflag = true;
		}

		if($hidForecastID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidForecastID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

########## Input Validations (Rated Capacity)
	
		foreach ($_POST['hidForecastItemID'] as $ForecastItemID) {
			$ForecastItemID = array();
			$ForecastItemID = $_POST['hidForecastItemID'];
		}
		foreach ($_POST['sltFGItem'] as $FGItem) {
			$FGItem = array();
			$FGItem = $_POST['sltFGItem'];
		}
		foreach ($_POST['txtQuantity'] as $Quantity) {
			$Quantity = array();
			$Quantity = $_POST['txtQuantity'];
		}

		$ctr = count($FGItem);
		$i = 0;

		do{
			if ( $FGItem[$i] == 0 ){
				if ($i == 0){
					$errmsg_arr[] = '* Select FG Item. (line '.($i+1).')';
					$errflag = true;
				}
				else{
					if ( $Quantity[$i] !='' ){
						$errmsg_arr[] = '* Select FG Item. (line '.($i+1).')';
						$errflag = true;
					}
				}
			}else{
				if ( $FGItem[$i-1] == 0 ){
					if ( $i != 0 ){
						$errmsg_arr[] = '* Skip Line is not allowed';
						$errflag = true;
					}
				}
				if ( $Quantity[$i] == '' ){
					$errmsg_arr[] = '* Quantity is missing. (line '.($i+1).')';
					$errflag = true;
				}elseif ( !is_numeric($Quantity[$i]) ){
					$errmsg_arr[] = '* Invalid quantity. (line '.($i+1).')';
					$errflag = true;
				}
			}
			$i++;
		}while ($i < $ctr);

		$date = date_create($txtMonthYear);
		$FMonth = date_format($date, "m");
		$FDay = '01';
		$FYear = date_format($date, "Y");

		$ForecastDate = $FYear."-".$FMonth."-".$FDay;

############# SESSION, keeping last input value
		$_SESSION['counter1'] = $ctr;
		$_SESSION['txtMonthYear'] = htmlspecialchars($txtMonthYear);
		$_SESSION['radFGType'] = $radFGType;
		$_SESSION['txtRemarks'] = htmlspecialchars($txtRemarks);

		foreach ($_POST['sltFGItem'] as $key => $prodValue) {
			$_SESSION['sltFGItem'][$key] = $prodValue;
			$_SESSION['txtQuantity'][$key] = htmlspecialchars($Quantity[$key]);
		}

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_sales_forecast.php?id=$hidForecastID");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Sales_Forecast_CRU(?, ?, ?, ?, ?, ?, ?, ? )");
	mysqli_stmt_bind_param($qry, 'isisssii', $hidForecastID, $ForecastDate, $radFGType, $txtRemarks, $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_forecast.php'.'</td><td>'.$processError.' near line 147.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidForecastID ) {
				$ForecastID = $hidForecastID;
			}else{
				$ForecastID = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['sltFGItem'] as $key => $temValue)
			{	
				if (!$hidForecastID) {
					$ForecastItemID = 0;
				}

				if ($temValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Sales_Forecast_Items_CRU( ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiii', $ForecastItemID[$key], $ForecastID, $FGItem[$key], $Quantity[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_forecast.php'.'</td><td>'.$processError1.' near line 172.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidForecastID )
							$_SESSION['SUCCESS']  = 'Sales forecast successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Sales forecast successfully added.';
						header("location: sales_forecast.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database

		require("include/database_close.php");

	}
?>