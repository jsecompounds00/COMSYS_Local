<html>
	<head>
		<title>CPIAR Monitoring - Home</title>
		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$page = ($_GET["page"] ? $_GET["page"] : 1);
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			// require("/include/unset_value.php");
			require("/include/init_unset_values/cpiar_verification_unset_value.php");
			require("/include/init_unset_values/cpiar_monitoring_unset_value.php");

			if( $_SESSION["cpr"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}
			
			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> CPIAR Monitoring </h3> </span>

			<div class="search_box">
				<form method="get" action="cpiar_monitoring.php">
					<input type="hidden" name="page" value="<?php echo $page;?>">
					<table class="search_tables_form">
						<tr>
							<td> CPIAR No.: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> CPIAR Date: </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]); ?>"> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" > </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php 		
									if(array_search(113, $session_Permit)){ 
								?>
									<input type="button" value="Add Monitoring" onclick="location.href='new_cpiar_monitoring.php?id=0'">
								<?php			
										$_SESSION["add_cpr"] = true;
									}else{
										unset($_SESSION["add_cpr"]);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>cpiar_monitoring.php"."</td><td>".$error." near line 49.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_CPIAR_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, "ss", $search, $qsone);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "cpiar_monitoring.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_CPIAR_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, "ssii", $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>cpiar_monitoring.php"."</td><td>".$processError." near line 75.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan="11" >
								<?php echo $pagination; ?>
							</td>
						</tr>
						<tr>
						    <th>CPIAR No.</th>
						    <th>CPIAR Date</th>
						    <th>Source of Feedback</th>
						    <th>From</th>
						    <th>To</th>
						    <th>Due Date</th>
						    <th>Returned?</th>
						    <th>Status</th>
						    <th colspan="3"></th>
						</tr>
						<?php 

							$date = getdate();
							$month = $date["month"];
							$day = $date["mday"];
							$year = $date["year"];
							$today = date("Y-m-d", strtotime($month." ".$day." ".$year));

							while($row = mysqli_fetch_assoc($result)) { 
								$status_id = $row["status_id"];
								$c_status = $row["c_status"];
						?>
								<tr>
									<td>
										<?php echo $row["cpr_no"];?>
									</td>
									<td>
										<?php echo $row["cpr_date"];?>
									</td>
									<td>
										<?php echo $row["feedback_source"];?>
									</td>
									<td>
										<?php echo $row["initiator"];?>
									</td>
									<td>
										<?php echo $row["receiver"];?>
									</td>
										<?php
											if ( $row["due_date"] == $today  && $row["returned"] == "N" ){
												echo "<td bgcolor='#FFFFB5'>".$row["due_date"]."</td>";
											}elseif ( $row["returned"] == "N" && $row["due_date"] < $today ){
												echo "<td bgcolor='#FFBBBB'>".$row["due_date"]."</td>";
											}else{
												echo "<td>".$row["due_date"]."</td>";
											}
										?>
									<td>
										<?php echo $row["returned"]; ?>
									</td>
									<td> 
										<?php echo ( $status_id == 2 ? $c_status : "<b><i>".$c_status."</i></b>" ); ?> 
									</td>
									<td>
										<?php
											if(array_search(114, $session_Permit)){
										?>
												<input type="button" name="btnTP" value="Edit" onclick="location.href='new_cpiar_monitoring.php?id=<?php echo $row['id'];?>'">
										<?php
												$_SESSION["edit_cpr"] = true;
											}else{
												unset($_SESSION["edit_cpr"]);
											}
										?>
									</td>
									<td>
										<?php
											if(array_search(118, $session_Permit)){
												if ( $status_id == 2 ){
										?>
											 		<input type="button" name="btnTV" value="Update" onclick="location.href='new_cpiar_verification.php?id=<?php echo $row['id'];?>&cid=0'">
										<?php
											 	}else{
										?>
											 		<input type="button" name="btnTV" value="Update" disabled>
										<?php
											 	}

											 	$_SESSION["update_cpr"] = true;
											 }else{
											 	unset($_SESSION["update_cpr"]);
											 }
										?>
									</td>
									<td>
										<?php
											 if(array_search(119, $session_Permit)){
										?>
												<input type="button" name="btnTH" value="Verification History" onclick="location.href='cpiar_history.php?page=1&id=<?php echo $row['id'];?>'">
										<?php
											 	$_SESSION["history_cpr"] = true;
											}else{
												unset($_SESSION["history_cpr"]);
											}
										?>
									</td>
								</tr>
						<?php 
							} 
						?>
						<tr>
							<td colspan="11">
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>