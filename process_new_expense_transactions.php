<?php
############# Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_expense_transactions.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitize the POST values
		$hidExpenseTransID = $_POST['hidExpenseTransID'];
		$txtTransactionDate = $_POST['txtTransactionDate'];
		$sltDepartment = $_POST['sltDepartment'];
		$sltGLCode = $_POST['sltGLCode'];
		$txtComments = NULL;
		$sltGLParticulars = $_POST['sltGLParticulars'];
		$txtQuantity = $_POST['txtQuantity'];
		$hidUOM = $_POST['hidUOM'];
		$txtUnitPrice = $_POST['txtUnitPrice'];
		$hidExpenseTransItemID = $_POST['hidExpenseTransItemID'];
		$hidItemsCreatedId = $_POST['hidItemsCreatedId'];
		$hidItemsCreatedAt = $_POST['hidItemsCreatedAt'];
		$txtRemarks = $_POST['txtRemarks'];
		$chkVoucher = ($_POST['chkVoucher'] ? 1 : 0) ;

		$createdAt = $_POST['hidCreatedAt'];
		$createdId = $_POST['hidCreatedId'];
		$updatedAt = date('Y-m-d H:i:s');
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validations

		$valtxtTransactionDate = validateDate($txtTransactionDate, 'Y-m-d');
		if ( $valtxtTransactionDate != 1 ){
			$errmsg_arr[] = '* Invalid transaction date.';
			$errflag = true;
		}
		if( !($sltDepartment) ){
			$errmsg_arr[] = "* Select department.";
			$errflag = true;
		}
		if( !$sltGLCode ){
			$errmsg_arr[] = "* Select GL code.";
			$errflag = true;
		}
		$transaction = count($sltGLParticulars);
		if( !$transaction ){
			$errmsg_arr[] = "* There must be at least (1) one transaction.";
			$errflag = true;
		}

		foreach ($sltGLParticulars as $key_1 => $Miscellaneous) {
			if( $sltGLParticulars[$key_1] ){
				if( $hidUOM[$key_1] == "" ){
					$errmsg_arr[] = "* Unit of measure can't be blank (line ".($key_1+1).").";
					$errflag = true;
				}

				if( $txtQuantity[$key_1] == "" ){
					$errmsg_arr[] = "* Quantity can't be blank (line ".($key_1+1).").";
					$errflag = true;
				}elseif( !is_numeric($txtQuantity[$key_1]) ){
					$errmsg_arr[] = "* Invalid quantity (line ".($key_1+1).").";
					$errflag = true;
				}

				if( $txtUnitPrice[$key_1] == "" ){
					$errmsg_arr[] = "* Unit price can't be blank (line ".($key_1+1).").";
					$errflag = true;
				}elseif( !is_numeric($txtUnitPrice[$key_1]) ){
					$errmsg_arr[] = "* Invalid unit price (line ".($key_1+1).").";
					$errflag = true;
				}

				if( $txtRemarks[$key_1] == "" ){
					$errmsg_arr[] = "* Remarks can't be blank (line ".($key_1+1).").";
					$errflag = true;
				}
			}else{
				if( $txtQuantity[$key_1] != "" || $txtUnitPrice[$key_1] != "" || $txtRemarks[$key_1] != "" ){
					
					$errmsg_arr[] = "* Select particulars (line ".($key_1+1).").";
					$errflag = true;
				}
			}
		}

############# SESSION

		$_SESSION['SESS_BET_TransactionDate'] = $txtTransactionDate;
		$_SESSION['SESS_BET_Department'] = $sltDepartment;
		$_SESSION['SESS_BET_GLCode'] = $sltGLCode;
		$_SESSION['SESS_BET_Particulars'] = $sltGLParticulars;
		$_SESSION['SESS_BET_UOM'] = $hidUOM;
		$_SESSION['SESS_BET_Quantity'] = $txtQuantity;
		$_SESSION['SESS_BET_UnitPrice'] = $txtUnitPrice;
		$_SESSION['SESS_BET_Comments'] = $txtComments;
		$_SESSION['SESS_BET_Remarks'] = $txtRemarks;

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_expense_transactions.php?id=".$hidExpenseTransID);
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Expense_Transaction_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isiisssiii', $hidExpenseTransID, $txtTransactionDate, $sltDepartment, $sltGLCode, $txtComments
											, $createdAt, $updatedAt, $createdId, $updatedId, $chkVoucher);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_expense_transactions.php'.'</td><td>'.$processError.' near line 180.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidExpenseTransID ) {
				$ExpenseTransID = $hidExpenseTransID;
			}else{
				$ExpenseTransID = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['sltGLParticulars'] as $key => $itemValue)
			{	
				if (!$hidExpenseTransID) {
					$ExpenseTransItemID[$key] = 0;
				}else{
					$ExpenseTransItemID[$key] = $hidExpenseTransItemID[$key];
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Expense_Transaction_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiiddssiis', $ExpenseTransItemID[$key], $ExpenseTransID, $sltGLParticulars[$key]
																 , $txtQuantity[$key], $txtUnitPrice[$key], $hidItemsCreatedAt[$key], $updatedAt
																 , $hidItemsCreatedId[$key], $updatedId, $txtRemarks[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_expense_transactions.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidExpenseTransID )
							$_SESSION['SUCCESS']  = "Expense transaction was successfully updated.";
						else
							$_SESSION['SUCCESS']  = "Expense transaction was successfully added.";
						header("location: budget_monitoring_departmental.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database	
		require("include/database_close.php");
	}
?>
	