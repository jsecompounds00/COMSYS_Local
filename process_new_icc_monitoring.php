<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;

############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_equipment_status.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidICCID = $_POST['hidICCID'];
		$txtICCNo = $_POST['txtICCNo'];
		$txtICCDate = $_POST['txtICCDate'];
		$txtDueDate = $_POST['txtDueDate'];
		$sltIssueDeptID = $_POST['sltIssueDeptID'];
		$sltReceiveDeptID = $_POST['sltReceiveDeptID'];
		$txtNatureOfComplaint = $_POST['txtNatureOfComplaint'];
		$txtRootCause = $_POST['txtRootCause'];
		$txtContainmentAction = $_POST['txtContainmentAction'];
		$txtContainmentImplement = $_POST['txtContainmentImplement'];
		$txtContainmentResponsible = $_POST['txtContainmentResponsible'];
		$txtCorrectiveAction = $_POST['txtCorrectiveAction'];
		$txtCorrectiveImplementation = $_POST['txtCorrectiveImplementation'];
		$txtCorrectiveResponsible = $_POST['txtCorrectiveResponsible'];
		$txtMemoDate = $_POST['txtMemoDate'];
		$txtFinalDueDate = $_POST['txtFinalDueDate'];
		$txtReceiveDateDCC = $_POST['txtReceiveDateDCC'];
		$sltMonth = $_POST['sltMonth'];
		$sltYear = $_POST['sltYear'];
		$txtRemarks = $_POST['txtRemarks'];

		$radFromDivision = $_POST['radFromDivision'];
		$radToDivision = $_POST['radToDivision'];

		$sltMonth = ( !$sltMonth ? NULL : $sltMonth );
		$sltYear = ( !$sltYear ? NULL : $sltYear );

		if( isset($_POST['chkMan']) ){
			$chkMan = 1;
		}else{
			$chkMan = 0;
		}
		if( isset($_POST['chkMethod']) ){
			$chkMethod = 1;
		}else{
			$chkMethod = 0;
		}
		if( isset($_POST['chkMachine']) ){
			$chkMachine = 1;
		}else{
			$chkMachine = 0;
		}
		if( isset($_POST['chkMaterial']) ){
			$chkMaterial = 1;
		}else{
			$chkMaterial = 0;
		}
		if( isset($_POST['chkMeasurement']) ){
			$chkMeasurement = 1;
		}else{
			$chkMeasurement = 0;
		}
		if( isset($_POST['chkMotherNature']) ){
			$chkMotherNature = 1;
		}else{
			$chkMotherNature = 0;
		}

############# Input Validation
		if ( $txtICCNo == '' ){
			$errmsg_arr[] = '* Invalid ICC number.';
			$errflag = true;
		}

		$valICCDate = validateDate($txtICCDate, 'Y-m-d');
		if ( $valICCDate != 1 ){
			$errmsg_arr[] = '* Invalid ICC date.';
			$errflag = true;
		}

		$valDueDate = validateDate($txtDueDate, 'Y-m-d');
		if ( $valDueDate != 1 ){
			$errmsg_arr[] = '* Invalid Due Date.';
			$errflag = true;
		}

		if ( isset( $radFromDivision ) && $radFromDivision == 'Pipes' ){
			$radFromDivision = 'Pipes';
		}elseif ( isset( $radFromDivision ) && $radFromDivision == 'Compounds' ){
			$radFromDivision = 'Compounds';
		}elseif ( isset( $radFromDivision ) && $radFromDivision == 'Corporate' ){
			$radFromDivision = 'Corporate';
		}else{
			$errmsg_arr[] = '* Choose division for issuance portion.';
			$errflag = true;
		}

		if ( !$sltIssueDeptID ){
			$errmsg_arr[] = '* Invalid Issuing Area.';
			$errflag = true;
		}

		if ( isset( $radToDivision ) && $radToDivision == 'Pipes' ){
			$radToDivision = 'Pipes';
		}elseif ( isset( $radToDivision ) && $radToDivision == 'Compounds' ){
			$radToDivision = 'Compounds';
		}elseif ( isset( $radToDivision ) && $radToDivision == 'Corporate' ){
			$radToDivision = 'Corporate';
		}else{
			$errmsg_arr[] = '* Choose division for receiving portion.';
			$errflag = true;
		}

		if ( !$sltReceiveDeptID ){
			$errmsg_arr[] = '* Invalid Receiving Area.';
			$errflag = true;
		}

		if ( $txtNatureOfComplaint == '' ){
			$errmsg_arr[] = '* Nature of complaint can\'t be blank.';
			$errflag = true;
		}

		$valMemoDate = validateDate($txtMemoDate, 'Y-m-d');
		if ( $valMemoDate != 1 && $txtMemoDate != '' ){
			$errmsg_arr[] = '* Invalid date of issuance of reminder.';
			$errflag = true;
		}elseif ( $txtMemoDate == '' ){
			$txtMemoDate = NULL;
		}

		$valFinalDueDate = validateDate($txtFinalDueDate, 'Y-m-d');
		if ( $valFinalDueDate != 1 && $txtFinalDueDate != '' ){
			$errmsg_arr[] = '* Invalid final due date.';
			$errflag = true;
		}elseif ( $txtFinalDueDate == '' ){
			$txtFinalDueDate = NULL;
		}

		$valReceiveDateDCC = validateDate($txtReceiveDateDCC, 'Y-m-d');
		if ( $txtReceiveDateDCC != '' && $valReceiveDateDCC != 1 ){
			$errmsg_arr[] = '* Invalid receipt date of DCC.';
			$errflag = true;
		}elseif ( $txtReceiveDateDCC == '' ){
			$txtReceiveDateDCC = NULL;
		}

		foreach ($_POST as $key => $value) {
			echo $key." => ".$value."<br>";
		}

	######### Validation on Checkbox
		if($hidICCID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidICCID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation

	// $txtNatureOfComplaint = str_replace('\\r\\n', '<br>', $txtNatureOfComplaint);
	// $txtRootCause = str_replace('\\r\\n', '<br>', $txtRootCause);
	// $txtContainmentAction = str_replace('\\r\\n', '<br>', $txtContainmentAction);
	// $txtCorrectiveAction = str_replace('\\r\\n', '<br>', $txtCorrectiveAction);
	// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
	
	// $txtNatureOfComplaint = str_replace('\\', '', $txtNatureOfComplaint);
	// $txtRootCause = str_replace('\\', '', $txtRootCause);
	// $txtContainmentAction = str_replace('\\', '', $txtContainmentAction);
	// $txtCorrectiveAction = str_replace('\\', '', $txtCorrectiveAction);
	// $txtRemarks = str_replace('\\', '', $txtRemarks);

	############# SESSION, keeping last input value
		$_SESSION['SESS_ICM_ICCNo'] = $txtICCNo;
		$_SESSION['SESS_ICM_ICCDate'] = $txtICCDate;
		$_SESSION['SESS_ICM_DueDate'] = $txtDueDate;
		$_SESSION['SESS_ICM_IssueDeptID'] = $sltIssueDeptID;
		$_SESSION['SESS_ICM_ReceiveDeptID'] = $sltReceiveDeptID;
		$_SESSION['SESS_ICM_NatureOfComplaint'] = $txtNatureOfComplaint;
		$_SESSION['SESS_ICM_RootCause'] = $txtRootCause;
		$_SESSION['SESS_ICM_ContainmentAction'] = $txtContainmentAction;
		$_SESSION['SESS_ICM_ContainmentImplement'] = $txtContainmentImplement;
		$_SESSION['SESS_ICM_ContainmentResponsible'] = $txtContainmentResponsible;
		$_SESSION['SESS_ICM_CorrectiveAction'] = $txtCorrectiveAction;
		$_SESSION['SESS_ICM_CorrectiveImplementation'] = $txtCorrectiveImplementation;
		$_SESSION['SESS_ICM_CorrectiveResponsible'] = $txtCorrectiveResponsible;
		$_SESSION['SESS_ICM_MemoDate'] = $txtMemoDate;
		$_SESSION['SESS_ICM_FinalDueDate'] = $txtFinalDueDate;
		$_SESSION['SESS_ICM_ReceiveDateDCC'] = $txtReceiveDateDCC;
		$_SESSION['SESS_ICM_Month'] = $sltMonth;
		$_SESSION['SESS_ICM_Year'] = $sltYear;
		$_SESSION['SESS_ICM_Remarks'] = $txtRemarks;
		$_SESSION['SESS_ICM_FromDivision'] = $radFromDivision;
		$_SESSION['SESS_ICM_ToDivision'] = $radToDivision;
		$_SESSION['SESS_ICM_Man'] = $chkMan;
		$_SESSION['SESS_ICM_Method'] = $chkMethod;
		$_SESSION['SESS_ICM_Machine'] = $chkMachine;
		$_SESSION['SESS_ICM_Material'] = $chkMaterial;
		$_SESSION['SESS_ICM_Measurement'] = $chkMeasurement;
		$_SESSION['SESS_ICM_MotherNature'] = $chkMotherNature;

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_icc_monitoring.php?id=$hidICCID");
			exit();
		}
############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_ICC_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'issssisisiiiiiissssssssssiisssii', $hidICCID, $txtICCNo, $txtICCDate, $txtDueDate, $radFromDivision
												, $sltIssueDeptID, $radToDivision, $sltReceiveDeptID, $txtNatureOfComplaint, $chkMan, $chkMethod
												, $chkMaterial, $chkMachine, $chkMeasurement, $chkMotherNature, $txtRootCause, $txtContainmentAction
												, $txtContainmentImplement, $txtContainmentResponsible, $txtCorrectiveAction, $txtCorrectiveImplementation
												, $txtCorrectiveResponsible, $txtMemoDate, $txtFinalDueDate, $txtReceiveDateDCC, $sltMonth, $sltYear
												, $txtRemarks, $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_icc_monitoring.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidICCID)
				$_SESSION['SUCCESS']  = 'Successfully updated ICC.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new ICC.';
			//echo $_SESSION['SUCCESS'];
			header("location:icc_monitoring.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>