<html>
	<head>
		<title>Permission - Home</title>

		<?php 
			require("include/database_connect.php");
			
			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>

	</head>
	<body>

		<?php 
			require "/include/header.php";
			require("/include/init_unset_values/permission_unset_value.php");

			if( $_SESSION["permission"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Permissions </h3> </span>

			<div class="search_box">
	 			<form method="get" action="permission.php">
		 			<input type="hidden" name="page" value="<?php echo $page;?>">
		 			<input type="hidden" name="qsone" value="<?php echo $qsone;?>">
					<table class="search_tables_form">
						<tr>
							<td> Name: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php
					 				if(array_search(32, $session_Permit)){
					 			?>
								 		<input type="button" name="btnAddPermission" value="Add Permission" onclick="location.href='new_permission.php?id=0'">
								<?php
										$_SESSION["add_permission"] = true;
									}else{
										unset($_SESSION["add_permission"]);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
	 		</div>
	 		<?php
		 		if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>permission.php"."</td><td>".$error." near line 42.</td></tr></tbody>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryP = mysqli_prepare($db, "CALL sp_Permission_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryP, "s", $search);
					$qryP->execute();
					$resultP = mysqli_stmt_get_result($qryP); //return results of query

					$total_results = mysqli_num_rows($resultP); //return number of rows of result
					
					$db->next_result();
					$resultP->close();

					$targetpage = "permission.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Permission_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if ( !empty($processError) ){
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>permission.php"."</td><td>".$processError." near line 63.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}else{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
								<th > Name</th>
								<th ></th>
							</tr>
							   	<?php 
								   	while($row = mysqli_fetch_assoc($result))
									{
										$permitId = $row["id"];
								?>
										<tr>
											<td> <?php echo $row["permission"];?> </td>
											<td>
												<?php
													if(array_search(33, $session_Permit)){
												?>
														<input type="button" name="btnEdit" value="EDIT" onclick="location.href='new_permission.php?id=<?php echo $permitId;?>'">
												<?php
														$_SESSION["edit_permission"] = true;
													}else{
														unset($_SESSION["edit_permission"]);
													}
												?>
											</td>
										</tr>
								<?php
									}
									$db->next_result();
									$result->close();
								?>
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
					</table>
			<?php 
					} 
				}
			?>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>

</html>