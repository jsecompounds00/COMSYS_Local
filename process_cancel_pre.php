<?php
	session_start();
	if( $_SESSION['cancel_pre'] == false) 
	{
		$_SESSION['ERRMSG_ARR'] ='Access denied!';
		session_write_close();
		header("Location:comsys.php");
		exit();
	}	

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require("include/database_connect.php");

	$id = intval($_GET['id']);
	$reason = strval($_GET['reason']);
	$date_cancelled = date('Y-m-d H:i:s');
	$cancel=1;
	
	$qry = mysqli_prepare($db, "CALL sp_PRE_Cancel(?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iiss', $id, $cancel, $reason, $date_cancelled);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_cancel_pre.php'.'</td><td>'.$processError.' near line 31.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS'] = 'PRE was successfully cancelled.';
			header("location: pre_monitoring.php?page=".$_SESSION["page"]."&item=".$_SESSION["item"]."&pre_number=".$_SESSION["pre_number"]."&pre_date=".$_SESSION["pre_date"]);
		}
			require("include/database_close.php");
?>