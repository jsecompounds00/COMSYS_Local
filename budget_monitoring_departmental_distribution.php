<html>
	<head>
		<title>Departmental Budget Distribution - Home</title>
		<?php
			require("/include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : "");
			$page = ($_GET["page"] ? $_GET["page"] : 1);	
			$budgetID = $_GET["id"];	
		?>
	</head>
	<body>
		<?php
			require("/include/header.php");

			// if( $_SESSION['departmental_budget_distribution'] == false) 
			// {
			// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }

			$_SESSION['page_b'] = $_GET['page'];
			$_SESSION['search_b'] = $_GET['search'];
			$_SESSION['qsone_b'] = $_GET['qsone'];
			$_SESSION['id_b'] = $_GET['id'];
		?>

		<div class="wrapper">
			<?php
				$qryDH = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_Departmental_Distribution_Query( ? )" );
				mysqli_stmt_bind_param( $qryDH, "i", $budgetID);
				$qryDH->execute();
				$resultDH = mysqli_stmt_get_result($qryDH);
				while( $rowDH = mysqli_fetch_assoc( $resultDH ) ){
					$DHBudgetYear = $rowDH["DHBudgetYear"];
					$DHDepartment = $rowDH["DHDepartment"];
					$DHGLCode = $rowDH["DHGLCode"];
				}
				$db->next_result();
				$resultDH->close();
			
				$page_a = $_SESSION["page"];
				$search_a = htmlspecialchars($_SESSION["search"]);
				$qsone_a = htmlspecialchars($_SESSION["qsone"]);
				$search_a = ( (strpos(($search_a), "\\")+1) > 0 ? str_replace("\\", "", $search_a) : $search_a );
				$search_a = ( (strpos(($search_a), "'")+1) > 0 ? str_replace("'", "\'", $search_a) : $search_a );
				$qsone_a = ( (strpos(($qsone_a), "\\")+1) > 0 ? str_replace("\\", "", $qsone_a) : $qsone_a );
				$qsone_a = ( (strpos(($qsone_a), "'")+1) > 0 ? str_replace("'", "\'", $qsone_a) : $qsone_a );
			?>
			
			<span> <h3> <?php echo $DHGLCode;?> Budget for <?php echo $DHDepartment." - Y".$DHBudgetYear;?> </h3> </span>

			<a class='back' href='budget_monitoring_departmental.php?page=<?php echo $page_a;?>&search=<?php echo $search_a;?>&qsone=<?php echo $qsone_a;?>'><img src='images/back.png' height="20" name='txtBack'> Home </a>
			
			<br><br><br>

			<div class="search_box">

	 			<form method="get" action="budget_monitoring_departmental_distribution.php">
	 				<input type="hidden" name="qsone" value="<?php echo $qsone;?>">
	 				<input type="hidden" name="page" value="<?php echo $page;?>">
	 				<input type="hidden" name="id" value="<?php echo $budgetID;?>">
					<table class="search_tables_form">
						<tr>
							<td> Particulars: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
							<td> <input type="submit" value="Search"> </td>
						</tr>
					</table>

	 			</form>

	 		</div>

	 		<?php
				if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_departmental_distribution.php"."</td><td>".$error." near line 27.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryBudget = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_Departmental_Distribution_Home( ?, ?, NULL, NULL )" );
					mysqli_stmt_bind_param( $qryBudget, "is", $budgetID, $search);
					$qryBudget->execute();
					$resultBudget = mysqli_stmt_get_result($qryBudget);
					$total_results = mysqli_num_rows($resultBudget); //return number of rows of result

					$db->next_result();
					$resultBudget->close();

					$targetpage = "budget_monitoring_departmental_distribution.php"; 	//your file name  (the name of this file)
					require("include/paginate_budget_distribution.php");

					$qryBudgetLimit = mysqli_prepare( $db, "CALL sp_Budget_Monitoring_Departmental_Distribution_Home( ?, ?, ?, ? )" );
					mysqli_stmt_bind_param( $qryBudgetLimit, "isii", $budgetID, $search, $start, $end );
					$qryBudgetLimit->execute();
					$resultBudgetLimit = mysqli_stmt_get_result($qryBudgetLimit);
					$processError3 = mysqli_error($db);	

					if(!empty($processError3))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_monitoring_departmental_distribution.php"."</td><td>".$processError3." near line 137.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
				?>
						<table class="home_pages">
							<tr>
								<td colspan="9">
									<?php echo $pagination; ?>
								</td>
							</tr>
							 <tr>
								<th rowspan="2"> Particulars </th>
								<th rowspan="2"> UOM </th>
								<th colspan="2"> AMOUNT </th>
								<th colspan="2"> QUANTITY </th>
								<th rowspan="2"></th>
							</tr>
							<tr>
								<th> Proposed </th>
								<th> Actual </th>
								<th> Proposed </th>
								<th> Actual </th>
							</tr>
							<?php
								while ($row = mysqli_fetch_assoc($resultBudgetLimit)) {
									$ParticularsID = $row["ParticularsID"];
									$GLParticulars = $row["GLParticulars"];
									$UOM = $row["UOM"];
									$ProposedBudget = $row["ProposedBudget"];
									$ActualBudget = $row["ActualBudget"];
									$TotalBudgetQty = $row["TotalBudgetQty"];
									$TransQuantity = $row["TransQuantity"];
							?>
									<tr>
										<td> <?php echo $GLParticulars;?> </td>
										<td> <?php echo $UOM;?> </td>
										<td style="background:#A5FF8A"> <?php echo $ProposedBudget;?> </td>
										<td style="background:#A5FF8A"> 
											<label class="<?php echo ( $ActualBudget > $ProposedBudget ? 'warning_background_stop' : ( $ActualBudget == $ProposedBudget ? "warning_background_slow" : "" ) );?>">
												<?php echo $ActualBudget;?> 
											</label>
										</td>
										<td style="background:#FFFF99"> <?php echo $TotalBudgetQty;?> </td>
										<td style="background:#FFFF99"> 
											<label class="<?php echo ( $TransQuantity > $TotalBudgetQty ? 'warning_background_stop' : ( $TransQuantity == $TotalBudgetQty ? "warning_background_slow" : "" ) );?>">
												<?php echo $TransQuantity;?> 
											</label>
										</td>
										<td>
											<?php
							 					// if(array_search(215, $session_Permit)){	
							 				?>
													<input type="button" value="History" onclick="location.href='budget_monitoring_departmental_history.php?page=1&search=&qsone=&id=<?php echo $budgetID;?>&item_id=<?php echo $ParticularsID;?>'">
							 				<?php
												//  	$_SESSION["departmental_budget_history"] = true;
												// }else{
												//  	unset($_SESSION["departmental_budget_history"]);
												// }
											?>
										</td>
									</tr>
							<?php
								}
								$db->next_result();
								$resultBudgetLimit->close();
							?>
							<tr>
								<td colspan="9">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
	