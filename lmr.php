<html>
	<head>
		<title>Load Manifest Report - Home</title> 
		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>

		<?php
			require '/include/header.php';
			require ("include/unset_value.php");

			if( $_SESSION['lmr'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Load Manifest Report </h3> </span>

			<div class='search_box'>
		 		<form method='get' action='lmr.php'>
		 			<input type='hidden' name='page' value="<?php echo $page;?>">
		 			<input type='hidden' name='search' value='<?php echo $_GET['search'];?>'>
		 			<table class="search_tables_form">
		 				<tr>
		 					<td> LMR Date: </td>
		 					<td> <input type='text' name='qsone' id='qsone' value='<?php echo $_GET['qsone'];?>'> </td>
		 					<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
		 					<td> <input type='submit' value='Search'> </td>
		 					<td>
								<?php
								 	if(array_search(53, $session_Permit)){
								 ?>
								 		<input type='button' name='btnAddLmr' value='Add LMR' onclick="location.href='new_lmr.php?id=0'">
								<?php
										$_SESSION['add_lmr'] = true;
									}else{
										unset($_SESSION['add_lmr']);
									}
								?>
		 					</td>
		 				</tr>
		 			</table>
		 		</form>
		 	</div>

		 	<?php	
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>lmr.php'.'</td><td>'.$error.' near line 34.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryTM = mysqli_prepare($db, "CALL sp_LMR_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryTM, 's', $qsone);
					$qryTM->execute();
					$resultTM = mysqli_stmt_get_result($qryTM);
					$total_results = mysqli_num_rows($resultTM); //return number of rows of result

					$db->next_result();
					$resultTM->close();

					$targetpage = "lmr.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_LMR_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>lmr.php'.'</td><td>'.$processError.' near line 54.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>

						<table class="home_pages">
							<tr>
								<td colspan='10'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
								<th rowspan='2'>LMR No.</th>
								<th rowspan='2'>Delivery Date</th>
								<th rowspan='2'>Departure Time</th>
								<th rowspan='2'>Arrival Time</th>
								<th colspan='3'>DSQ</th>
								<th colspan='2'>Total Amount</th>
								<th rowspan="2"> </th>
							</tr>
							<tr>
								<th>DSQ No.</th>
								<th>Truck No.</th>
								<th>Time of Delivery</th>
								<th>Diesel</th>
								<th>Toll</th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)) { 
							?>
									<tr>
										<td> <?php echo $row['LMRNo']; ?> </td>
										<td> <?php echo $row['DeliveryDate']; ?> </td>
										<td> <?php echo $row['Departure']; ?> </td>
										<td> <?php echo $row['Arrival']; ?> </td>
										<td> <?php echo $row['ControlNo']; ?> </td>
										<td> <?php echo $row['TruckNo']; ?> </td>
										<td> <?php echo $row['DeliveryTime']; ?> </td>
										<td> <?php echo number_format((float)($row['DieselPrice']), 4, '.', ''); ?> </td>
										<td> <?php echo $row['TollFee']; ?> </td>
										<td>
											<?php
												if(array_search(54, $session_Permit)){
											?>
													<input type='button' name='btnTP' value='Edit' onclick="location.href='new_lmr.php?id=<?php echo $row['LMRId'];?>'">
											<?php
													$_SESSION['edit_lmr'] = true;
												}else{
													unset($_SESSION['edit_lmr']);
												}
											?>
										</tr>
									</tr>
							<?php 
								} 
							?>
							<tr>
								<td colspan='10'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>