<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_extrusion_parameters.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidExtrusionParametersID = clean($_POST['hidExtrusionParametersID']);
		$hidFinishedGoodID = $_POST['hidFinishedGoodID'];

		if($hidExtrusionParametersID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidExtrusionParametersID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

########## Input Validations
	
		foreach ($_POST['hidExtrusionParametersItemID'] as $ExtrusionParametersItemID) {
			$ExtrusionParametersItemID = array();
			$ExtrusionParametersItemID = $_POST['hidExtrusionParametersItemID'];
		}
		foreach ($_POST['txtExtrusionID'] as $ExtrusionID) {
			$ExtrusionID = array();
			$ExtrusionID = $_POST['txtExtrusionID'];
		}
		foreach ($_POST['txtMachineName'] as $ExtrusionName) {
			$ExtrusionName = array();
			$ExtrusionName = $_POST['txtMachineName'];
		}
		foreach ($_POST['txtZone1'] as $Zone1) {
			$Zone1 = array();
			$Zone1 = $_POST['txtZone1'];
		}
		foreach ($_POST['txtZone2'] as $Zone2) {
			$Zone2 = array();
			$Zone2 = $_POST['txtZone2'];
		}
		foreach ($_POST['txtZone3'] as $Zone3) {
			$Zone3 = array();
			$Zone3 = $_POST['txtZone3'];
		}
		foreach ($_POST['txtZone4'] as $Zone4) {
			$Zone4 = array();
			$Zone4 = $_POST['txtZone4'];
		}
		foreach ($_POST['txtZone5'] as $Zone5) {
			$Zone5 = array();
			$Zone5 = $_POST['txtZone5'];
		}
		foreach ($_POST['txtZone6'] as $Zone6) {
			$Zone6 = array();
			$Zone6 = $_POST['txtZone6'];
		}
		foreach ($_POST['txtZone7'] as $Zone7) {
			$Zone7 = array();
			$Zone7 = $_POST['txtZone7'];
		}
		foreach ($_POST['txtDieHeadA'] as $DieHeadA) {
			$DieHeadA = array();
			$DieHeadA = $_POST['txtZone3'];
		}
		foreach ($_POST['txtDieHeadB'] as $DieHeadB) {
			$DieHeadB = array();
			$DieHeadB = $_POST['txtDieHeadB'];
		}

		$ctr = count($ExtrusionID);
		$i = 0;

		do{
			if ( $Zone1[$i] != '' && !is_numeric($Zone1[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Zone 1 - ".$ExtrusionName[$i].".";
				$errflag = true;
			}
			if ( $Zone2[$i] != '' && !is_numeric($Zone2[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Zone 2 - ".$ExtrusionName[$i].".";
				$errflag = true;
			}
			if ( $Zone3[$i] != '' && !is_numeric($Zone3[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Zone 3 - ".$ExtrusionName[$i].".";
				$errflag = true;
			}
			if ( $Zone4[$i] != '' && !is_numeric($Zone4[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Zone 4 - ".$ExtrusionName[$i].".";
				$errflag = true;
			}
			if ( $Zone5[$i] != '' && !is_numeric($Zone5[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Zone 5 - ".$ExtrusionName[$i].".";
				$errflag = true;
			}
			if ( $Zone6[$i] != '' && !is_numeric($Zone6[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Zone 6 - ".$ExtrusionName[$i].".";
				$errflag = true;
			}
			if ( $Zone7[$i] != '' && !is_numeric($Zone7[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Zone 7 - ".$ExtrusionName[$i].".";
				$errflag = true;
			}
			if ( $DieHeadA[$i] != '' && !is_numeric($DieHeadA[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Die Head A - ".$ExtrusionName[$i].".";
				$errflag = true;
			}
			if ( $DieHeadB[$i] != '' && !is_numeric($DieHeadB[$i]) ){
				$errmsg_arr[] = "* Invalid parameters for Die Head B - ".$ExtrusionName[$i].".";
				$errflag = true;
			}

			$i++;

		}while ( $i < $ctr );

// ############# SESSION, keeping last input value
// 		$_SESSION['txtName'] = $txtName;
// 		$_SESSION['radExtrusionType'] = $radExtrusionType;
// 		$_SESSION['chkActive'] = $chkActive;
// 		$_SESSION['txtRemarks'] = $txtRemarks;

		// $ExtrusionType = substr($hidExtrusionType, 0, 1);
########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_extrusion_parameters.php?id=".$hidFinishedGoodID);
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Extrusion_Parameters_CRU(?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iissii', $hidExtrusionParametersID, $hidFinishedGoodID, $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_extrusion_parameters.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidExtrusionParametersID ) {
				$ExtrusionParametersID = $hidExtrusionParametersID;
			}else{
				$ExtrusionParametersID = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['txtExtrusionID'] as $key => $temValue)
			{	
				if (!$hidExtrusionParametersID) {
					$ExtrusionParametersItemID = 0;
				}

				if ($temValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Extrusion_Parameter_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiiiiiiiiiii', $ExtrusionParametersItemID[$key], $ExtrusionParametersID
																, $ExtrusionID[$key], $Zone1[$key], $Zone2[$key], $Zone3[$key], $Zone4[$key]
																, $Zone5[$key], $Zone6[$key], $Zone7[$key], $DieHeadA[$key], $DieHeadB[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_extrusion_parameters.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidExtrusionParametersID )
							$_SESSION['SUCCESS']  = 'Extrusion Parameters successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Extrusion Parameters successfully created.';
						header("location: pam_analysis.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database

		require("include/database_close.php");

	}
?>