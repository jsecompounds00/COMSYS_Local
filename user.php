<html>
	<head>
		<title>User - Home</title>
		<?php 
			require("include/database_connect.php");
			
			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>

		<?php
			require ("/include/header.php");
				require("/include/init_unset_values/user_unset_value.php");

			if( $_SESSION["user"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Users </h3> </span>

			<div class="search_box">
		 		<form method="get" action="user.php">
		 			<input type="hidden" name="page" value="<?php echo htmlspecialchars($_GET["page"]);?>">
		 			<input type="hidden" name="qsone" value="<?php echo $qsone;?>">
		 			<table class="search_tables_form">
		 				<tr>
		 					<td> Name: </td>
		 					<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
		 					<td> <input type="submit" value="Search"> </td>
		 					<td>
		 						<?php
									 if(array_search(4, $session_Permit)){
								?>
									 	<input type="button" name="btnAddUser" value="Add User" onclick="location.href='<?php echo PG_NEW_USER;?>0'">
								<?php
									 	$_SESSION["add_user"] = true;
									 }else{
									 	unset($_SESSION["add_user"]);
									 }
					 			?>
		 					</td>
		 				</tr>
		 			</table>
		 		</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>user.php"."</td><td>".$error." near line 41.</td></tr></tbody>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryU = mysqli_prepare($db, "CALL sp_User_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryU, "s", $search);
					$qryU->execute();
					$resultU = mysqli_stmt_get_result($qryU);
					$total_results = mysqli_num_rows($resultU); //return number of rows of result

					$db->next_result();
					$resultU->close();

					$targetpage = "user.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_User_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);

					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>user.php"."</td><td>".$processError." near line 62.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>
						<table class="home_pages">
							<tr>
								<td class='borderless' colspan='6' height='50' valign='top'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>Username</th>
							    <th>Name</th>
							    <th>Roles</th>
							    <th/th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)){
									$userId = $row["user_id"];
									$role = $row["role"];
									$roleId = $row["role_id"];
									$username = $row["username"];
									$fname = $row["fname"];
									$lname = $row["lname"];

									if ( $row["active"]==0 ){
										$role="Resigned (No access)";
									}
							?>

									<tr>
									    <td> <?php echo $username;?> </td>
									    <td> <?php echo $fname.' '.$lname;?> </td>
					 					<td> <?php echo $role;?> </td>
					 					<td>
											<?php
												if(array_search(18, $session_Permit)){
											?>
													<input type="button" name="btnEdit" value="EDIT" onclick="location.href='<?php echo PG_NEW_USER.$userId;?>'">
											<?php
													$_SESSION["edit_user"] = true;
												}else{
													unset($_SESSION["edit_user"]);
												}
											?>
										</td>
									</tr>
							<?php
								}
								$db->next_result();
								$result->close();
							?>
							
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
<?php
					}
				}
?>
			
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>