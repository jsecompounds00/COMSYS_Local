<html>
	<head>
		<title>Property Transfer - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/unset_value.php");

			if( $_SESSION['property_transfer'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Property Transfer </h3> </span>

			<div class="search_box">
				 <form method="get" action="property_transfer.php">
		 			<input type="hidden" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>">
					<input type="hidden" name="page" value="<?php echo $page;?>">
					<table class="search_tables_form">
						<tr>
							<td> Asset: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php
								 	if(array_search(93, $session_Permit)){
								 ?>
								 		<input type='button' value='Add Asset' onclick="location.href='new_properties.php?id=0'">
								 <?php
								 		$_SESSION['property_add'] = true;
								 	}else{
								 		unset($_SESSION['property_add']);
								 	}// Add asset
								 ?>
							</td>
							<td>
								<?php
								 	if(array_search(97, $session_Permit)){
								 ?>
								 		<input type='button' value='Add Inventory' onclick="location.href='add_property_inventory.php?id=0'">
								 <?php
								 		$_SESSION['property_trans_add'] = true;
								 	}else{
								 		unset($_SESSION['property_trans_add']);
								 	}// Add Inventory
								 ?>
							</td>
							<td>
								<?php
								 	if(array_search(98, $session_Permit)){
								 ?>
								 		<input type='button' value='Deduct Inventory' onclick="location.href='deduct_property_inventory.php?id=0'">
								 <?php
								 		$_SESSION['property_trans_sub'] = true;
								 	}else{
								 		unset($_SESSION['property_trans_sub']);
									}// Deduct Inventory
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php	
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>property_transfer.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Property_Transfer_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 's', $search);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "property_transfer.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Property_Transfer_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>property_transfer.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='11'> <?php echo $pagination;?> </td>
						</tr>
						<tr>
						    <th rowspan='2'>Asset Type</th>
						    <th rowspan='2'>Asset</th>
						    <th rowspan='2'>UoM</th>
						    <th rowspan='2'>Applicable To</th>
						    <th rowspan='2'>Active</th>
						    <th colspan='2'>Stock</th>
						    <th rowspan='2'>Total Stock</th>
						    <th rowspan='2' colspan="3"></th>
						</tr>
						<tr>
						    <th>Good</th>
						    <th>Damaged</th>
						</tr>
						<?php 
							while($row = mysqli_fetch_assoc($result)) { 
						?>
								<tr>
									<td> <?php echo $row['property_type']; ?> </td>
									<td> <?php echo $row['property_name']; ?> </td>
									<td> <?php echo $row['uom']; ?> </td>
									<td> <?php echo $row['applicable_to']; ?> </td>
									<td> <?php echo $row['active']; ?> </td>
									<td> <?php echo $row['balance_good']; ?> </td>
									<td> <?php echo $row['balance_damaged']; ?> </td>
									<td> <?php echo $row['TotalStock']; ?> </td>
									<td>	
										<?php
											if(array_search(94, $session_Permit)){
										?>
												<input type='button' value='Edit Asset' onclick="location.href='new_properties.php?id=<?php echo $row['id'];?>'">
										<?php
												$_SESSION['property_edit'] = true;
											}else{
												unset($_SESSION['property_edit']);
											}
										?>
									</td>
									<td>
										<?php
											if(array_search(95, $session_Permit)){
										?>
												<input type='button' value='Add Assset Specifications' onclick="location.href='new_property_specifications.php?id=<?php echo $row['id'];?>'">
										<?php
												$_SESSION['property_specs'] = true;
											}else{
												unset($_SESSION['property_specs']);
											}
										?>
									</td>
									<td>
										<?php
											if(array_search(96, $session_Permit)){
										?>
												<input type='button' value='History' onclick="location.href='property_transfer_history.php?page=1&id=<?php echo $row['id'];?>'">
										<?php
												$_SESSION['property_transfer_history'] = true;
											}else{
												unset($_SESSION['property_transfer_history']);
											}
										?>
									</td>
								</tr>
						<?php
							} 
						?>
						<tr>
							<td colspan='11'> <?php echo $pagination;?> </td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>	
</html>