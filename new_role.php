<html>
	<head>

		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_role.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$roleID = $_GET["id"];

				if($roleID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Role_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $roleID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_role.php"."</td><td>".$processError." near line 32.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$name = htmlspecialchars($row["name"]);
							$db_compounds = $row["compounds"];
							$db_pipes = $row["pipes"];
							$db_corporate = $row["corporate"];
							$db_ppr = $row["ppr"];
							$db_department1_id = $row["department1_id"];
							$db_department2_id = $row["department2_id"];
							$perm_id = "0,".$row["perm_id"];
							$perm = array();
							$perm = explode(",", $perm_id);
							$createdAt = $row["created_at"];
							$createdId = $row["created_id"];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.role";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_role.php'.'</td><td>'.$processErrorPI.' near line 57.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($roleID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_role.php</td><td>The user tries to edit a non-existing role_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Role - Edit</title>";
				}
				else
				{

					echo "<title>Role - Add</title>";
				}
			}
		?>
		<script src="js/jscript.js"></script>  
	</head>
	<?php
		if ( !$roleID ){
	?>		<body onload='showDepartmentNew(0,0)'>
	<?php
		}
	?>

		<form method="post" action="process_new_role.php">
			<?php
				require("/include/header.php");
			require("/include/init_unset_values/role_init_value.php");

				if ( $roleID ){
					if( $_SESSION['edit_role'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
					}
				}else{
					if( $_SESSION['add_role'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $roleID ? "Edit ".$name : "New Role" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
					    <td  >Role:</td>
					    <td colspan="2">
					    	<input type="text" name="txtName" value="<?php echo ( $roleID ? $name : $initRLEName ); ?>">
					    </td>
					</tr>
					<tr>
						<td> Applicable to: </td>
							<td>
								<input type="checkbox" name="chkCompounds" id="Compounds" value="Compounds" 
									onchange="showDepartmentNew(0,0)" 
									<?php echo ( $roleID ? ( $db_compounds ? "checked" : "" ) : ( $initRLECompounds ? "checked" : "" ) ); ?> >
									<label for="Compounds"> Compounds</label>

								<input type="checkbox" name="chkPipes" id="Pipes" value="Pipes" 
									onchange="showDepartmentNew(0,0)" 
									<?php echo ( $roleID ? ( $db_pipes ? "checked" : "" ) : ( $initRLEPipes ? "checked" : "" ) ); ?> >
									<label for="Pipes"> Pipes</label>

								<input type="checkbox" name="chkCorporate" id="Corporate" value="Corporate" 
									onchange="showDepartmentNew(0,0)" 
									<?php echo ( $roleID ? ( $db_corporate ? "checked" : "" ) : ( $initRLECorporate ? "checked" : "" ) ); ?> >
									<label for="Corporate"> Corporate</label>

								<input type="checkbox" name="chkPPR" id="PPR" value="PPR" 
									onchange="showDepartmentNew(0,0)" 
									<?php echo ( $roleID ? ( $db_ppr ? "checked" : "" ) : ( $initRLEPPR ? "checked" : "" ) ); ?> >
									<label for="PPR"> PPR</label>
							</td>
					</tr>
					<tr>
						<td> Department: </td>
						<td> 
							<select name="sltDept1" id="sltDept1">
								<?php
						    		if($roleID)
									{
										$qryD = mysqli_prepare($db, "CALL sp_Department_Dropdown(1, ?, ?, ?, ?)");
										mysqli_stmt_bind_param($qryD, 'iiii', $db_compounds, $db_pipes, $db_corporate, $db_ppr);
										$qryD->execute();
										$resultD = mysqli_stmt_get_result($qryD);
							    		$processError2 = mysqli_error($db);

										if(!empty($processError2))
										{
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_user.php'.'</td><td>'.$processError2.' near line 177.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultD))
											{
												
												$deptID = $row['id'];
												$deptCode = $row['code'];

												if ($deptID == $db_department1_id && $deptID != 0){
													echo "<option value='".$deptID."' selected>".$deptCode."</option>";
												}else {
													echo "<option value='".$deptID."'>".$deptCode."</option>";
												}
											}
											$db->next_result();
											$resultD->close();
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>  </td>
						<td> 
							<select name="sltDept2" id="sltDept2">
								<?php
						    		if($roleID)
									{
										$qryD = mysqli_prepare($db, "CALL sp_Department_Dropdown(1, ?, ?, ?, ?)");
										mysqli_stmt_bind_param($qryD, 'iiii', $db_compounds, $db_pipes, $db_corporate, $db_ppr);
										$qryD->execute();
										$resultD = mysqli_stmt_get_result($qryD);
							    		$processError2 = mysqli_error($db);

										if(!empty($processError2))
										{
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_user.php'.'</td><td>'.$processError2.' near line 177.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultD))
											{
												
												$deptID = $row['id'];
												$deptCode = $row['code'];

												if ($deptID == $db_department2_id && $deptID != 0){
													echo "<option value='".$deptID."' selected>".$deptCode."</option>";
												}else {
													echo "<option value='".$deptID."'>".$deptCode."</option>";
												}
											}
											$db->next_result();
											$resultD->close();
										}
									}
								?>
							</select>
						</td>
					</tr>
				</table>
				<table class="table_permissions_role" >
					<tr>
						<th >Module</td>
						<th colspan='2' >Permission</td>
					</tr>
					<?php
						$qryP = "CALL sp_Permission_Dropdown(0)";

						$resultP = mysqli_query($db, $qryP);

						$processError1 = mysqli_error($db);

						if(!empty($processError))
							{
								// $error = mysqli_connect_error();
								echo ("$processError");
								error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_role.php'.'</td><td>'.$processError.' near line 125.</td></tr>', 3, "errors.php");
								header("location: error_message.html");
							}
							else
							{
								while($row1 = mysqli_fetch_assoc($resultP))
								{
									$id = array();
									$id[] = $row1['id'];
									$module = $row1['module'];
									$permission = $row1['permission'];

									if ($roleID){
										foreach($id as $key => $itemValue) {
											// echo $id[0];]
											if(array_search($itemValue, $perm))
											echo"
												<tr>
													<td valign='top'>
														$module
													</td>
													<td>
														<input type='checkbox' name='chkPermission[]' value='".$itemValue."' checked><label> ".$permission."</label><br>
													</td>
												</tr>";
											else
												echo"
												<tr>
													<td valign='top'>
														$module
													</td>
													<td>
														<input type='checkbox' name='chkPermission[]' value='".$itemValue."'><label> ".$permission."</label><br>
													</td>
												</tr>";
										}
									}else{
										foreach($id as $key => $itemValue)
										echo"
												<tr>
													<td valign='top'>
														$module
													</td>
													<td>
														<input type='checkbox' name='chkPermission[]' value='".$itemValue."'><label> ".$permission."</label><br>
													</td>
												</tr>";
									}
								}
								$db->next_result();
								$resultP->close();
							}
					?>
				</table>
				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveRole" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='role.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidRoleid' value="<?php echo $roleID; ?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $roleID ? $createdAt : date('Y-m-d H:i:s') ); ?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $roleID ? $createdId : 0 ); ?>'>
						</td>
					</tr>
				</table>
				
			</div>
		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>