<?php
//logout.php
// require("include/database_close.php");
session_start();
// session_destroy();

	unset($_SESSION['SESS_USER_ID']);
	unset($_SESSION['SESS_USER_NAME']);
	unset($_SESSION['SESS_PASS_WORD']);
	unset($_SESSION['SESS_EMAIL']);
	unset($_SESSION['SESS_ROLE_ID']);
	unset($_SESSION['SESS_Permit_ID']);
	unset($_SESSION['SESS_DEPARTMENT_ID']);
	unset($_SESSION['SESS_FIRST_NAME']);
	unset($_SESSION['SESS_LAST_NAME']);
	unset($_SESSION['SESS_COMPOUNDS']);
	unset($_SESSION['SESS_PIPES']);
	unset($_SESSION['SESS_CORPORATE']);
	unset($_SESSION['SUCCESS']);
	unset($_SESSION['ERRMSG_ARR']);

	require("include/unset_value.php");
	
	require("include/init_unset_values/batch_ticket_jrd_unset_value.php");
	require("include/init_unset_values/batch_ticket_nrm_unset_value.php");
	require("include/init_unset_values/batch_ticket_testing_unset_value.php");
	require("include/init_unset_values/canvass_unset_value.php");
	require("include/init_unset_values/ccr_monitoring_unset_value.php");
	require("include/init_unset_values/ccr_verification_unset_value.php");
	require("include/init_unset_values/cpiar_verification_unset_value.php");
	require("include/init_unset_values/credit_payment_unset_value.php");
	require("include/init_unset_values/debit_invoice_unset_value.php");
	require("include/init_unset_values/fg_parameters_unset_value.php");
	require("include/init_unset_values/icc_verification_unset_value.php");
	require("include/init_unset_values/jrd_unset_value.php");
	require("include/init_unset_values/material_balance_unset_value.php");
	require("include/init_unset_values/moisture_content_unset_value.php");
	require("include/init_unset_values/nrm_unset_value.php");
	require("include/init_unset_values/po_monitoring_unset_value.php");

header("location: login.php");
?>