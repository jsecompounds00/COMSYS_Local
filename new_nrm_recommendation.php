<html>
	<head>
		<?php
			require("include/database_connect.php");
		?>
		<title>Recommendation</title>
		<script src="js/batch_ticket_js.js"></script>
	</head>
	<body onload='showNRMResults()'>

		<form method='post' action='process_new_nrm_recommendation.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/nrm_init_value.php");
			?>

			<div class="wrapper">
				
				<span> <h3> New Recommendation </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class='parent_tables_form'>
					<tr>
						<td>
							New Raw Materials:
						</td>
						<td>
							<select name='sltNRM' id='sltNRM' onchange='showNRMResults()'>
								<?php
									$qry1 = "CALL sp_NRM_Dropdown()";
									$result1 = mysqli_query($db, $qry1);
									$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>nrm_recommendation.php'.'</td><td>'.$processError1.' near line 31.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{ 
										while ( $row = mysqli_fetch_assoc( $result1 ) ){
											$nrm_id = $row['id'];
											$material_description = $row['material_description'];
											$nrm_no = $row['nrm_no'];

											if ( $initNRMID == $nrm_id ){
												echo "<option value='".$nrm_id."' selected>NRM ".$nrm_no." - ".$material_description."</option>";	
											}else{
												echo "<option value='".$nrm_id."'>NRM ".$nrm_no." - ".$material_description."</option>";
											}
											
										}
										$db->next_result();
										$result1->close();
									}
								?>
							</select>
						</td>
					</tr>
				</table>

				<div id='NRMResults'>

				</div>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>