<?php
########## -- Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## -- Array to store validation errors
	$errmsg_arr = array();
 
########## -- Validation error flag
	$errflag = false;
 
########## -- Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}
########## -- Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_labor.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## -- Sanitize the POST values
		$hidLaborID = $_POST['hidLaborID'];
		$txtDate = clean($_POST['txtDate']);
		$sltDSQ = clean($_POST['sltDSQ']);
		$txtDriverID = clean($_POST['txtDriverID']);
		$txtHelper1ID = clean($_POST['txtHelper1ID']);
		$txtHelper2ID = clean($_POST['txtHelper2ID']);
		$txtDriverRegular = clean($_POST['txtDriverRegular']);
		$txtHelperRegular1 = clean($_POST['txtHelperRegular1']);
		$txtHelperRegular2 = clean($_POST['txtHelperRegular2']);
		$txtDriverOT = clean($_POST['txtDriverOT']);
		$txtHelperOT1 = clean($_POST['txtHelperOT1']);
		$txtHelperOT2 = clean($_POST['txtHelperOT2']);
		$txtRemarks = clean($_POST['txtRemarks']);

		$DSQ = substr($sltDSQ, 0, strpos($sltDSQ, '-'));
		$Class = substr($sltDSQ, (strpos($sltDSQ, '-')+1));
			
		$txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		
		$txtRemarks = str_replace('\\', '', $txtRemarks);

########## -- Input Validations
		$valDate = validateDate($txtDate, 'Y-m-d');
		if ( $valDate != 1 ){
			$errmsg_arr[] = '* Invalid Delivery Date.';
			$errflag = true;
		}
		if ( !$sltDSQ ){
			$errmsg_arr[] = '* Invalid DSQ.';
			$errflag = true;
		}
		if (!empty($txtDriverRegular) && !is_numeric($txtDriverRegular)){
			$errmsg_arr[] = '* Invalid regular hrs for Driver.';
			$errflag = true;
		}
		if (!empty($txtHelperRegular1) && !is_numeric($txtHelperRegular1)){
			$errmsg_arr[] = '* Invalid regular hrs for Delivery Helper 1.';
			$errflag = true;
		}
		if (!empty($txtHelperRegular2) && !is_numeric($txtHelperRegular2)){
			$errmsg_arr[] = '* Invalid regular hrs for Delivery Helper 2.';
			$errflag = true;
		}
		if (!empty($txtDriverOT) && !is_numeric($txtDriverOT)){
			$errmsg_arr[] = '* Invalid overtime for Driver.';
			$errflag = true;
		}
		if (!empty($txtHelperOT1) && !is_numeric($txtHelperOT1)){
			$errmsg_arr[] = '* Invalid overtime for Delivery Helper 1.';
			$errflag = true;
		}
		if (!empty($txtHelperOT2) && !is_numeric($txtHelperOT2)){
			$errmsg_arr[] = '* Invalid overtime for Delivery Helper 2.';
			$errflag = true;
		}
		if (empty($txtDriverRegular) && empty($txtHelperRegular1) && empty($txtHelperRegular2) 
				&& empty($txtDriverOT) && empty($txtHelperOT1) && empty($txtHelperOT2)){
			$errmsg_arr[] = '';
			$errflag = true;
		}

		if($hidLaborID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidLaborID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];
########## -- Input Validation

########## -- SESSION, keeping last input value
		$_SESSION['txtDate'] = $txtDate;
		$_SESSION['sltDSQ'] = $sltDSQ;

########## -- If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_labor.php?id=$hidLaborID");
			exit();
		}
########## -- Committing to database
		$qry = mysqli_prepare($db, "CALL sp_Labor_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isiiiddiddiddsssii', $hidLaborID, $txtDate, $DSQ, $Class
										, $txtDriverID, $txtDriverRegular, $txtDriverOT
										, $txtHelper1ID, $txtHelperRegular1, $txtHelperOT1
										, $txtHelper2ID, $txtHelperRegular2, $txtHelperOT2
										, $txtRemarks, $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_labor.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidLaborID)
				$_SESSION['SUCCESS']  = 'Successfully updated labor.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new labor.';
			//echo $_SESSION['SUCCESS'];
			header("location:labor.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	