<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_trucks.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$truck_id = $_GET['id'];

				if($truck_id)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Trucks_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $truck_id);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_trucks.php'.'</td><td>'.$processError.' near line 32.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$plate_number = $row['plate_number'];
							$vehicle_class = $row['vehicle_class'];
							$createdAt = $row['created_at'];
						}
					}
					$db->next_result();
					$result->close();

		########### .............
					$qryPI = "SELECT id from comsys.trucks";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_trucks.php'.'</td><td>'.$processErrorPI.' near line 57.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($truck_id, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_trucks.php</td><td>The user tries to edit a non-existing truck_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					

					echo "<title>Trucks - Edit</title>";
				}
				else
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_trucks.php</td><td>The user tries to edit a non-existing truck_id.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
			}
					
		?>
	</head>
	<body>

		<form method='post' action='process_new_trucks.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");
			?>

			<div class="wrapper">

				<span> <h3> Edit <?php echo $plate_number;?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
					    <td>Vehicle Class:</td>

					    <td>
					    	<select name='sltClass'>
					    		<option value='0'></option>
					    		<option value='1' <?php echo ( $truck_id ? ( $vehicle_class == "1" ? "selected" : "" ) : "" );?>>Class 1</option>
								<option value='2' <?php echo ( $truck_id ? ( $vehicle_class == "2" ? "selected" : "" ) : "" );?>>Class 2</option>
								<option value='3' <?php echo ( $truck_id ? ( $vehicle_class == "2" ? "selected" : "" ) : "" );?>>Class 3</option>
					    	</select>
					    </td>
					</tr>
					<tr class="align_bottom">
						<td >
							<input type="submit" name="btnSaveTrucks" value="Save">
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='trucks.php?page=1&search=&qsone='">
							<input type='hidden' name='hidTruckId' value="<?php echo $truck_id;?>">
							<input type='hidden' name='hidCreatedAt' value="<?php echo ( $truck_id ? $createdAt : date('Y-m-d H:i:s') ); ?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>