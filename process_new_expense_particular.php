<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_expense_particular.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidParID = $_POST['hidParID'];
		$hidExpenseID = $_POST['hidExpenseID'];
		$txtDescription = $_POST['txtDescription'];
		$txtUnitPrice = $_POST['txtUnitPrice'];
		$sltUOM = $_POST['sltUOM'];

		$CreatedAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		$CreatedID = $_POST['hidCreatedID'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# SESSION

		$_SESSION['SESS_EXP_Particulars'] = $txtDescription;
		$_SESSION['SESS_EXP_UnitPrice'] = $txtUnitPrice;
		$_SESSION['SESS_EXP_UOM'] = $sltUOM;

############# Input Validation
		foreach ($txtDescription as $key => $specsValue) {	
			if ( $txtUnitPrice[$key] == "" && $specsValue ){
				$errmsg_arr[] = "* Unit Price can't be blank. (line ".($key+1).")";
				$errflag = true;
			}

			if ( !$sltUOM[$key] && $specsValue ){
				$errmsg_arr[] = "* Select unit of measures. (line ".($key+1).")";
				$errflag = true;
			}

			if( ($sltUOM[$key] || $txtUnitPrice[$key] != "") && $specsValue == "" ){
				$errmsg_arr[] = "* Particulars can't be blank. (line ".($key+1).")";
				$errflag = true;
			}

			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("Location:new_expense_particular.php?id=".$hidExpenseID);
				exit();
			}

############# Commiting to Database		
			if ( $specsValue ){
				$qry = mysqli_prepare($db, "CALL sp_Budget_Expense_Particulars_CRU( ?, ?, ?, ?, ?, ?, ?, ? , ? )");
				mysqli_stmt_bind_param($qry, 'iisdsisii', $hidParID[$key], $hidExpenseID, $txtDescription[$key] 
														, $txtUnitPrice[$key], $CreatedAt[$key] 
														, $CreatedID[$key], $updatedAt, $updatedId, $sltUOM[$key]);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry); 
				$processError = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_expense_particular.php'.'</td><td>'.$processError.' near line 116.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{ 
					if( $hidParID ){
						$_SESSION['SUCCESS']  = 'Expense particulars was successfully updated.';
					}else{
						$_SESSION['SUCCESS']  = 'Expense particulars was successfully added.';
					}
					header("location:expense_code.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
				}//end of if(!empty($processError) else

			}// end of if ( $Specifications[$key] != '' )
			else{
				if( $hidParID ){
					$_SESSION['SUCCESS']  = 'Expense particulars was successfully updated.';
				}else{
					$_SESSION['SUCCESS']  = 'Expense particulars was successfully added.';
				}
				header("location:expense_code.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
			}

		} // end of foreach ($_POST['txtSpecifications'] as $key => $specsValue)

		require("include/database_close.php");
	}
?>