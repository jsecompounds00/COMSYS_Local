<html>
	<head>
		<script src="js/jscript.js"></script>  
		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_moisture_content.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$mcid = $_GET['id'];

				if ($mcid)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Moisture_Content_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $mcid);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);

					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_moisture_content.php'.'</td><td>'.$processError.' near line 32.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						$MCItemID = array();
						$LotNumber = array();
						$PercentMC = array();
						$BulkDensity = array();

						while($row = mysqli_fetch_assoc($result))
						{
							$MCID = $row['MCID'];
							$MIRSDate = $row['MIRSDate'];
							$RM = $row['RM'];
							$RMID = $row['RMID'];
							$MIRNumber = $row['MIRNumber'];
							$RMIRNumber = $row['RMIRNumber'];
							$PRMNumber = $row['PRMNumber'];
							$Remarks = htmlspecialchars($row['remarks']);
							$CreatedAt = $row['CreatedAt'];
							$CreatedId = $row['CreatedID'];

							$MCItemID[] = $row['MCItemID'];

							$LotNumber[] = htmlspecialchars($row['LotNumber']);

							$PercentMC[] = $row['PercentMC'];

							$BulkDensity[] = $row['BulkDensity'];
						}
					}
					$db->next_result();
					$result->close();

		########### .............

					$qryMI = "SELECT id from comsys.moisture_content";
					$resultMI = mysqli_query($db, $qryMI); 
					$processErrorMI = mysqli_error($db);

					if ( !empty($processErrorMI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_moisture_content.php'.'</td><td>'.$processErrorMI.' near line 95.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$mid = array();
						while($row = mysqli_fetch_assoc($resultMI)){
							$mid[] = $row['id'];
						}
					}
					$db->next_result();
					$resultMI->close();

		############ .............
					if( !in_array($mcid, $mid, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_moisture_content.php</td><td>The user tries to edit a non-existing mc_id .</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					

					echo "<title>Raw Materials - Edit Moisture Content</title>";
					// echo "<span> </span>";
				}
				else
				{
					echo "<title>Raw Materials - Add Moisture Content</title>";
					// echo "<span>Add %MC </span>";
				}
					

					
			}
		?>
	</head>
	<body onload="showMIRSNo(), showRMItem()">

		<form method='post' action='process_new_moisture_content.php'>

			<?php 
				require("/include/header.php");
				require("/include/init_unset_values/moisture_content_init_value.php");

				if( $mcid ){
					if( $_SESSION['edit_moisture_content'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
					}
				}else{
					if( $_SESSION['new_moisture_content'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $mcid ? "Edit %MC" : "Add %MC" ); ?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
					    <td>MIRS Date:</td>
					    <td >
					    	<input type="text" name="txtDate" id="txtDate" value="<?php echo ( $mcid ? $MIRSDate : $initMCDate );?>"  onchange="showMIRSNo()">
					    	<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDate')" style="cursor:pointer" name="picker" />
					    </td>
					</tr>
					<tr>
					    <td>MIRS No:</td>
					    <td >
							<?php
								if ( $mcid ){
									echo "<input type='text' value='".$MIRNumber."' name='sltMIRNo' id='sltMIRNo' readonly onchange='showRMItem()'>";
								}else{
							?>
							    	<select name='sltMIRNo' id='sltMIRNo' onchange='showRMItem()'>
							    	</select>
							<?php
								}
							?>	
					    </td>
					</tr>
					<tr>
					    <td>Raw Materials:</td>
					    <td >
							<?php
								if ( $mcid ){
									echo "<input type='text' value='".$RM."' readonly>";
									echo "<input type='hidden' value='".$RMID."' name='sltRMItem' id='sltRMItem' readonly>";
								}else{
							?>
							    	<select name='sltRMItem' id='sltRMItem'>
							    	</select>
							<?php
								}
							?>	
					    </td>
					</tr>
					<tr>
					    <td>RMIR No:</td>
					    <td >
					    	<input type="text" name="txtRMIRNo" id="txtRMIRNo" value="<?php echo ( $mcid ? $RMIRNumber : $initMCRMIRNo );?>" >
					    </td>
					</tr>
					<tr>
					    <td>PRM No:</td>
					    <td >
					    	<input type="text" name="txtPRMNo" id="txtPRMNo" value="<?php echo ( $mcid ? $PRMNumber : $initMCPRMNo );?>" >
					    </td>
					</tr>
				</table>
				<table class="child_tables_form">
					<tr>
						<th>Lot Number</th>
						<th>% MC</th>
						<th>Bulk Density</th>
					</tr>
					<?php
						if ( $mcid ){
							$ctr = count($MCItemID); 
							for ( $i = 0; $i < $ctr; $i++ ) { 
					?>
								<input type="hidden" name="hidMCItemId[]" value="<?php echo $MCItemID[$i];?>">	
								<tr>
								    <td class="pad">
								    	<input type="text" name="txtLotNumber[]" id="txtLotNumber" value="<?php echo $LotNumber[$i];?>" >
								    	</td>
								    <td class="pad">
								    	<input type="text" name="txtPercentMC[]" id="txtPercentMC" value="<?php echo $PercentMC[$i];?>" >
								    </td>
								    <td class="pad">
								    	<input type="text" name="txtBulkDensity[]" id="txtBulkDensity" value="<?php echo $BulkDensity[$i];?>" >
								    </td>
								</tr>
					<?php
							}

						}else{
							for ($i = 0; $i < 10 ; $i++) { 
					?>
								<input type="hidden" name="hidMCItemId[]" value="">	
								<tr>
								    <td class="pad">
								    	<input type="text" name="txtLotNumber[]" id="txtLotNumber" value="<?php echo $initMCLotNumber[$i];?>" >
								    	</td>
								    <td class="pad">
								    	<input type="text" name="txtPercentMC[]" id="txtPercentMC" value="<?php echo $initMCPercentMC[$i];?>" >
								    </td>
								    <td class="pad">
								    	<input type="text" name="txtBulkDensity[]" id="txtBulkDensity" value="<?php echo $initMCBulkDensity[$i];?>" >
								    </td>
								</tr>
					<?php
							}
						}
					?>
				</table>
				<table class="comments_buttons">
					<tr>
					    <td valign="top">Remarks:</td>
					    <td >
							<textarea name="txtRemarks"><?php
								if ( $mcid ){
									echo $Remarks;
								}else{
									echo $initMCRemarks;
								}
							?></textarea>
					    </td>
					</tr>
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$name_text = htmlspecialchars($_SESSION["name_text"]);
								$code_text = htmlspecialchars($_SESSION["code_text"]);
								$type_text = htmlspecialchars($_SESSION["type_text"]);
								$name_text = ( (strpos(($name_text), "\\")+1) > 0 ? str_replace("\\", "", $name_text) : $name_text );
								$name_text = ( (strpos(($name_text), "'")+1) > 0 ? str_replace("'", "\'", $name_text) : $name_text );
								$code_text = ( (strpos(($code_text), "\\")+1) > 0 ? str_replace("\\", "", $code_text) : $code_text );
								$code_text = ( (strpos(($code_text), "'")+1) > 0 ? str_replace("'", "\'", $code_text) : $code_text );
								$type_text = ( (strpos(($type_text), "\\")+1) > 0 ? str_replace("\\", "", $type_text) : $type_text );
								$type_text = ( (strpos(($type_text), "'")+1) > 0 ? str_replace("'", "\'", $type_text) : $type_text );
							?>
							<input type="submit" name="btnSaveRM" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='moisture_content.php?page=<?php echo $page;?>&name_text=<?php echo $name_text;?>&code_text=<?php echo $code_text;?>&type_text=<?php echo $type_text;?>'">
							<input type='hidden' name='hidMCId' value='<?php echo $mcid;?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $mcid ? $CreatedId : "" );?>'>
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $mcid ? $CreatedAt : date('Y-m-d H:i:s') );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>