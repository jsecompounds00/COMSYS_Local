<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_raw_materials.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$rm_id = $_GET['id'];
				$comp = $_GET['comp'];

				if($rm_id)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_RawMaterials_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $rm_id);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_raw_materials.php'.'</td><td>'.$processError.' near line 32.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							// $rm_id = $row['RMID'];
							$rmt_id = $row['RMTypeID'];
							$rm_type = htmlspecialchars($row['RMType']);
							$rm_code = htmlspecialchars($row['RMCode']);
							$rm_name = htmlspecialchars($row['RMName']);
							$description = htmlspecialchars($row['description']);
							$unit_of_measures_id = $row['unit_of_measures_id'];
							$active = $row['Active'];
							$local = $row['local'];
							$db_compounds = $row['compounds'];
							$db_pipes = $row['pipes'];
							$db_corporate = $row['corporate'];
							$db_ppr = $row['ppr'];
							$packaging = htmlspecialchars($row['Packaging']);
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
							$CriticalLevel = htmlspecialchars($row['CriticalLevel']);
							$AveMonthlyUsage = htmlspecialchars($row['AveMonthlyUsage']);
						}
					}
					$db->next_result();
					$result->close();

			########### .............
					$qryPI = "SELECT id from comsys.raw_materials";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_raw_materials.php'.'</td><td>'.$processErrorPI.' near line 57.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

			############ .............
					if( !in_array($rm_id, $id, TRUE) ){	//, TRUE
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_raw_materials.php</td><td>The user tries to edit a non-existing rm_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					
					echo "<title>Raw Materials - Edit</title>";

				}
				else
				{

					echo "<title>Raw Materials - Add</title>";
				}


					
			}
		?>
		<script src="js/jscript.js"></script>  
	</head>
	<body onload="showRMType('<?php echo ( $rm_id ? $rmt_id : 0 ); ?>'	)">

		<form method='post' action='process_new_raw_materials.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/raw_materials_init_value.php");	

				$role_count = count($session_roleId);		

				if ( $rm_id ){
					if( $_SESSION['edit_rm'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
					}
				}else{
					if( $_SESSION['add_rm'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
					}
				}
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $rm_id ? "Edit ".$rm_type." ".$rm_code." : ".$rm_name : "New Raw Materials" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Applicable to:</td>
						<td>
							<input type="checkbox" name="chkCompounds" id="Compounds" value="1" onchange="showRMType(<?php echo ( $rm_id ? $rmt_id : 0 ); ?>)" <?php echo ( $rm_id ? ( $db_compounds ? "checked" : "" ) : ( $initRMSCompounds ? "checked" : "" ) ); ?>> 
								<label for="Compounds">Compounds</label> 

							<input type="checkbox" name="chkPipes" id="Pipes" value="1" onchange="showRMType(<?php echo ( $rm_id ? $rmt_id : 0 ); ?>)" <?php echo ( $rm_id ? ( $db_pipes ? "checked" : "" ) : ( $initRMSPipes ? "checked" : "" ) ); ?>> 
								<label for="Pipes">Pipes</label>

							<input type="checkbox" name="chkCorporate" id="Corporate" value="1" onchange="showRMType(<?php echo ( $rm_id ? $rmt_id : 0 ); ?>)" <?php echo ( $rm_id ? ( $db_corporate ? "checked" : "" ) : ( $initRMSCorporate ? "checked" : "" ) ); ?>> 
								<label for="Corporate">Corporate</label>

							<input type="checkbox" name="chkPPR" id="PPR" value="1" onchange="showRMType(<?php echo ( $rm_id ? $rmt_id : 0 ); ?>)" <?php echo ( $rm_id ? ( $db_ppr ? "checked" : "" ) : ( $initRMSPPR ? "checked" : "" ) ); ?>> 
								<label for="PPR">PPR</label>
						</td>
					</tr>	
					<tr>
						<td>Raw Material Type:</td>
						<td>
							<select name="sltRMType" id="sltRMType">
							</select>
						</td>
					</tr>
					<tr>
						<td>Code:</td>
						<td>
							<input type="text" name="txtRMCode" value="<?php echo ( $rm_id ? $rm_code : $initRMSRMCode ); ?>">
						</td>
					</tr>
					<tr>
						<td>Name:</td>
						<td>
							<input type="text" name="txtRMName" value="<?php echo ( $rm_id ? $rm_name : $initRMSRMName ); ?>">
						</td>
					</tr>
					<tr valign="top">
						<td>Description:</td>
						<td>
							<textarea name="txtDescription" cols='20' rows='5'><?php
								if ( $rm_id ){
									echo $description;
								}else{
									echo $initRMSDescription;
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>Unit of Measures:</td>
						<td>
							<select name="sltUOM">
								<?php
									$qryU = "CALL sp_UOM_Dropdown(0)"; 
									$resultU = mysqli_query($db, $qryU);
									$processError2 = mysqli_error($db);

									if(!empty($processError2))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_supply.php'.'</td><td>'.$processError2.' near line 238.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultU))
										{
											$uomID = $row['id'];
											$uomDd = $row['code'];
								?>
											<option value="<?php echo $uomID; ?>" 
												<?php echo ( $rm_id ? ( $uomID == $unit_of_measures_id ? "selected" : "" ) : ( $uomID == $initRMSUOM ? "selected" : "" ) ); ?> >
												<?php echo $uomDd; ?>
											</option>
								<?php
										}
										
										$db->next_result();
										$resultU->close();
									}
						 		?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Local:</td>
						<td>
							<input type="checkbox" name="chkLocal" <?php echo ( $rm_id ? ( $local ? "checked" : "" ) : ( $initRMSLocal ? "checked" : "" ) ); ?>>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type="checkbox" name="chkActive" <?php echo ( $rm_id ? ( $active ? "checked" : "" ) : ( $initRMSActive ? "checked" : "" ) ); ?>>
						</td>
					</tr>
					<?php 
						if ( $comp == 1 ){ 
					?>
							<tr>
							    <td>KG / Packaging:</td>
							    <td  class='borderless'>
							    	<input type='text' name='txtPackaging' value="<?php echo ( $rm_id ? ( $packaging ? $packaging : "" ) : ( $initRMSPackaging ? $initRMSPackaging : "" ) ); ?>" >
							    </td>
							</tr>
							<tr>
							    <td>Critical Quantity:</td>
							    <td  class='borderless'> <?php echo ( $rm_id ? $CriticalLevel : "-" ); ?> </td>
							</tr>
							<tr>
							    <td>Average Monthly Usage:</td>
							    <td>
							    	<input type='text' name='txtAveMonthlyUsage' value="<?php echo ( $rm_id ? ( $AveMonthlyUsage == 0 ? "" : $AveMonthlyUsage ) : ( $initRMSAveMonthlyUsage == 0 ? "" : $initRMSAveMonthlyUsage ) ); ?>" >
							    </td>
							</tr>
					<?php 
						} 
					?>
				</table>
				<table class="parent_tables_form">
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$name_text = htmlspecialchars($_SESSION["name_text"]);
								$code_text = htmlspecialchars($_SESSION["code_text"]);
								$type_text = htmlspecialchars($_SESSION["type_text"]);
								$name_text = ( (strpos(($name_text), "\\")+1) > 0 ? str_replace("\\", "", $name_text) : $name_text );
								$name_text = ( (strpos(($name_text), "'")+1) > 0 ? str_replace("'", "\'", $name_text) : $name_text );
								$code_text = ( (strpos(($code_text), "\\")+1) > 0 ? str_replace("\\", "", $code_text) : $code_text );
								$code_text = ( (strpos(($code_text), "'")+1) > 0 ? str_replace("'", "\'", $code_text) : $code_text );
								$type_text = ( (strpos(($type_text), "\\")+1) > 0 ? str_replace("\\", "", $type_text) : $type_text );
								$type_text = ( (strpos(($type_text), "'")+1) > 0 ? str_replace("'", "\'", $type_text) : $type_text );
							?>
							<input type="submit" name="btnSaveRM" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='raw_materials.php?page=<?php echo $page;?>&name_text=<?php echo $name_text;?>&code_text=<?php echo $code_text;?>&type_text=<?php echo $type_text;?>'">
							<input type='hidden' name='hidRMId' value="<?php echo $rm_id; ?>">
							<input type='hidden' name='hidCreatedAt' value="<?php echo ( $rm_id ? $createdAt : date("Y-m-d H:i:s") ); ?>">
							<input type='hidden' name='hidCreatedId' value="<?php echo ( $rm_id ? $createdId : 0 ); ?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>