<html>
	<head>
		<title>Engineering Item Records - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = "";
		?>
	</head>
	<body>

		<?php
			require '/include/header.php';
			require ("include/unset_value.php");

			if( $_SESSION['e_records'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Engineering Item Records </h3> </span>

			<div class="search_box">
				<form method='get' action='engineering_item_records.php'>
					<input type='hidden' name='page' value='<?php echo $page; ?>'>
					<input type='hidden' name='qsone' value='<?php echo $qsone; ?>'>
					<table class="search_tables_form">
						<tr>
							<td> Item: </td>
							<td> <input type='text' name='search' value='<?php echo $_GET['search']; ?>'> </td>
							<td> <input type='submit' value='Search'> </td>
							<td> <input type='button' name='btnAddEngineeringItemRecords' value='Add Records' onclick="location.href='new_engineering_item_records.php?id=0'"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>engineering_item_records.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryL = mysqli_prepare($db, "CALL sp_Engineering_Item_Record_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryL, 's', $search);
					$qryL->execute();
					$resultL = mysqli_stmt_get_result($qryL);
					$total_results = mysqli_num_rows($resultL); //return number of rows of result

					$db->next_result();
					$resultL->close();

					$targetpage = "engineering_item_records.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Engineering_Item_Record_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>engineering_item_records.php'.'</td><td>'.$processError.' near line 67.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th> Engineering Items </th>
							    <th width="100px"> Last Record </th>
							    <th> Comments </th>
							    <th>  </th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)){
									$eng_item_id = $row['eng_item_id'];
									$eng_item = $row['EngineeringItem'];
									$last_date = $row['last_date'];
									$comments = $row['comments'];
							?>
									<tr>
										<td> <?php echo $eng_item;?> </td>
										<td> <?php echo $last_date;?> </td>
										<td> <?php echo $comments;?> </td>
										<td>
											<input type='button' value='History' onclick="location.href='engineering_item_records_history.php?page=1&id=<?php echo $eng_item_id;?>'">
										</td>
									</tr>
							<?php
								}
									$db->next_result();
									$result->close();
							?>
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>