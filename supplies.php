<html>
	<head>
		<title>Supplies - Home</title>
		<?php
			require("/include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : "");
			$page = ($_GET["page"] ? $_GET["page"] : 1);	
		?>
	</head>
	<body>
		<?php
			require("/include/header.php");
			require("/include/init_unset_values/supplies_unset_value.php");

			if( $_SESSION['supplies'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION['page'] = $_GET['page'];
			$_SESSION['search'] = $_GET['search'];
			$_SESSION['qsone'] = $_GET['qsone'];
		?>

		<div class="wrapper">
			
			<span> <h3> Supplies </h3> </span>

			<div class="search_box">

	 			<form method="get" action="supplies.php">
	 				<input type="hidden" name="page" value="<?php echo $page;?>">
					<table class="search_tables_form">
						<tr>
							<td> Name: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
							<td> Supply Type: </td>
							<td>
								<select name="qsone" value="<?php echo $_GET["qsone"];?>">
				 					<?php
			 							$qryST = "CALL sp_SupplyType_Dropdown(1, 0)"; 
										$resultST = mysqli_query($db, $qryST);
										$processError1 = mysqli_error($db);

										if(!empty($processError1))
										{
											error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>supplies.php"."</td><td>".$processError1." near line 43.</td></tr></tbody>", 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultST))
											{
												$supplyTypeID = $row["supply_type_id"];
												$supplyType = $row["supply_type"];

												if ($supplyTypeID == $qsone)
												{
													echo "<option value='".$supplyTypeID."' selected>".$supplyType."</option>";
												}
												else echo "<option value='".$supplyTypeID."'>".$supplyType."</option>";
											}
											
											$db->next_result();
											$resultST->close();
										}
				 					?>
								</select>
							</td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php
				 					if(array_search(10, $session_Permit)){	
				 				?>
				 						<input type="button" name="btnAddSupply" value="Add Supply" onclick="location.href='new_supply.php?id=0'">
				 				<?php
									 	$_SESSION["add_supplies"] = true;
									}else{
									 	unset($_SESSION["add_supplies"]);
									}
								?>
							</td>
						</tr>
					</table>

	 			</form>

	 		</div>

	 		<?php
				if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>supplies.php"."</td><td>".$error." near line 27.</td></tr></tbody>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qrySupply = mysqli_prepare( $db, "CALL sp_Supplies_Home( ?, ?, NULL, NULL )" );
					mysqli_stmt_bind_param( $qrySupply, "is", $qsone , $search);
					$qrySupply->execute();
					$resultSupply = mysqli_stmt_get_result($qrySupply);
					$total_results = mysqli_num_rows($resultSupply); //return number of rows of result

					$db->next_result();
					$resultSupply->close();

					$targetpage = "supplies.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qrySupplyLimit = mysqli_prepare( $db, "CALL sp_Supplies_Home( ?, ?, ?, ? )" );
					mysqli_stmt_bind_param( $qrySupplyLimit, "isii", $qsone, $search, $start, $end );
					$qrySupplyLimit->execute();
					$resultSupplyLimit = mysqli_stmt_get_result($qrySupplyLimit);
					$processError3 = mysqli_error($db);	

					if(!empty($processError3))
					{
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>supplies.php"."</td><td>".$processError3." near line 137.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
				?>
						<table class="home_pages">
							<tr>
								<td colspan="9">
									<?php echo $pagination; ?>
								</td>
							</tr>
							 <tr>
								<th>Type</th>
								<th>Supply Name</th>
								<th>Active</th>
								<th>UOM</th>
								<th>Compounds</th>
								<th>Pipes</th>
								<th>Corporate</th>
								<th>PPR</th>
								<th></th>
							</tr>
							<?php
								while($row = mysqli_fetch_assoc($resultSupplyLimit))
								{ 
									$supplyId = $row["id"];
									$supplyType = $row["supply_type"];
									$supplyName = $row["name"];
									$active = ($row["active"] ? "Y" : "N");
									$uom = $row["code"];
									$comments = $row["comments"];
							?>

									<tr>
										<td> <?php echo $supplyType;?> </td>
										<td> <?php echo $supplyName;?></td>
										<td> <?php echo $active;?></td>
										<td> <?php echo $uom;?></td>
										<td> <?php echo ( $row["compounds"] ? "<img src='images/yes.png' height='20'>" : "" );?> </td>
										<td> <?php echo ( $row["pipes"] ? "<img src='images/yes.png' height='20'>" : "" );?> </td>
										<td> <?php echo ( $row["corporate"] ? "<img src='images/yes.png' height='20'>" : "" );?> </td>
										<td> <?php echo ( $row["ppr"] ? "<img src='images/yes.png' height='20'>" : "" );?> </td>		
										<td> 
											<?php
												if(array_search(9, $session_Permit)){
											?>
													<input type="button" name="btnEdit" value="EDIT" onclick="location.href='new_supply.php?id=<?php echo $supplyId;?>'">
											<?php
													$_SESSION["edit_supplies"] = true;
												}else{
													unset($_SESSION["edit_supplies"]);
												}
											?>

										</td>
							<?php
								} 
									$db->next_result();
									$resultSupplyLimit->close();
							?>

							<tr>
								<td colspan="9">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
	