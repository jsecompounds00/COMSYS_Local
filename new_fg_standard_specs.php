<html>
	<head>
		<title>FG Specification</title>
		<?php 
			require("include/database_connect.php");

			$FGID = $_GET['id'];

			$qry = mysqli_prepare($db, "CALL sp_BAT_FG_Standard_Specs_Query( ? )");
			mysqli_stmt_bind_param($qry, 'i', $FGID);
			$qry->execute();
			$result = mysqli_stmt_get_result($qry);
			$processError = mysqli_error($db);

			if ( !empty($processError) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_fg_standard_specs.php'.'</td><td>'.$processError.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($result)){
					$FGName = $row['name'];
					$fg_soft = $row['soft_pvc'];
					$StandardSpecsID = $row['StandardSpecsID'];
					$from_specific_gravity = $row['from_specific_gravity'];
					$to_specific_gravity = $row['to_specific_gravity'];
					$from_hardness_a = $row['from_hardness_a'];
					$to_hardness_a = $row['to_hardness_a'];
					$from_hardness_d = $row['from_hardness_d'];
					$to_hardness_d = $row['to_hardness_d'];
					$volume_resistivity = $row['volume_resistivity'];
					$unaged_tensile = $row['unaged_tensile'];
					$unaged_elongation = $row['unaged_elongation'];
					$ovenaged_tensile = $row['ovenaged_tensile'];
					$ovenaged_elongation = $row['ovenaged_elongation'];
					$oilaged_tensile = $row['oilaged_tensile'];
					$oilaged_elongation = $row['oilaged_elongation'];
					$created_at = $row['created_at'];
					$created_id = $row['created_id'];
				}
				$db->next_result();
				$result->close();
			}
		?>
	</head>
	<body>

		<form method='post' action='process_new_fg_standard_specs.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/fg_specs_init_value.php");

				// if( $_SESSION['batch_ticket'] == false) 
				// {
				// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
				// 	session_write_close();
				// 	header("Location:comsys.php");
				// 	exit();
				// }
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $StandardSpecsID ? "Update " : "" );?> Standard Specification for <?php echo $FGName; ?> </h3> </span>

				<?php						
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class='parent_tables_form'>
					<!-- <colgroup><col width='130'></col></colgroup> -->
					<input type='hidden' name='hidFGID' value="<?php echo $FGID; ?>">
					<input type='hidden' name='hidStandardSpecsID' value="<?php echo $StandardSpecsID; ?>">
					<input type='hidden' name='hidFGSoft' value="<?php echo $fg_soft; ?>">

					<tr>
						<th>  </th>
						<th> From </th>
						<th> To </th>
					</tr>

					<tr>
						<td>Specific Gravity:</td>
						<td>
							<input type='text' name='txtFromSpecificGravity' value="<?php echo ( $StandardSpecsID ? $from_specific_gravity : $initFGSPECSFromSpecificGravity ); ?>">
						</td>

						<td>
							<input type='text' name='txtToSpecificGravity' value="<?php echo ( $StandardSpecsID ? $to_specific_gravity : $initFGSPECSToSpecificGravity ); ?>">
						</td>
					</tr>

					<?php
						if ( $fg_soft == 1 ){
					?>
							<tr>
								<td>H - Shore A:</td>

								<td>
									<input type='text' name='txtFromHardnessA' value="<?php echo ( $StandardSpecsID ? $from_hardness_a : $initFGSPECSFromHardnessA ); ?>">
								</td>

								<td>
									<input type='text' name='txtToHardnessA' value="<?php echo ( $StandardSpecsID ? $to_hardness_a : $initFGSPECSToHardnessA ); ?>">
								</td>
							</tr>

							<tr>
								<td>Volume Resistivity:</td>

								<td colspan='2'>
									<input type='text' name='txtVolumeResistivity' value="<?php echo ( $StandardSpecsID ? $volume_resistivity : $initFGSPECSVolumeResistivity ); ?>">

									<label class="instruction">
				    				  	(e.g. 1.23e12)
				    				</label>
								</td>
							</tr>
					<?php
						}else{
					?>
							<tr>
								<td>H - Shore D:</td>

								<td>
									<input type='text' name='txtFromHardnessD' value="<?php echo ( $StandardSpecsID ? $from_hardness_d : $initFGSPECSFromHardnessD ); ?>">
								</td>

								<td>
									<input type='text' name='txtToHardnessD' value="<?php echo ( $StandardSpecsID ? $to_hardness_d : $initFGSPECSToHardnessD ); ?>">
								</td>
							</tr>
					<?php
						}
					?>
				</table>
				
				<?php
					if ( $fg_soft == 1 ){
				?>
						<table class="child_tables_form">

							<tr>
								<th> </th>
								<th> Unaged Properties </th>
								<th> Oven Aged </th>
								<th> Oil Aged </th>
							</tr>

							<tr>
								<td> Tensile Strength:</td>

								<td>
									<input type='text' name='txtUnagedTS' value="<?php echo ( $StandardSpecsID ? $unaged_tensile : $initFGSPECSUnagedTS ); ?>">
								</td>

								<td>
									<input type='text' name='txtOvenTS' value="<?php echo ( $StandardSpecsID ? $ovenaged_tensile : $initFGSPECSOvenTS ); ?>">
								</td>

								<td>
									<input type='text' name='txtOilTS' value="<?php echo ( $StandardSpecsID ? $oilaged_tensile :  $initFGSPECSOilTS ); ?>">
								</td>
							</tr>

							<tr>
								<td>Elongation:</td>

								<td>
									<input type='text' name='txtUnagedE' value="<?php echo ( $StandardSpecsID ? $unaged_elongation : $initFGSPECSUnagedE ); ?>">
								</td>

								<td>
									<input type='text' name='txtOvenE' value="<?php echo ( $StandardSpecsID ? $ovenaged_elongation : $initFGSPECSOvenE ); ?>">
								</td>

								<td>
									<input type='text' name='txtOilE' value="<?php echo ( $StandardSpecsID ? $oilaged_elongation : $initFGSPECSOilE ); ?>">
								</td>
							</tr>
						</table>
				<?php
					}
				?>
				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='finished_goods.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $StandardSpecsID ? $created_at : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $StandardSpecsID ? $created_id  : $_SESSION['SESS_USER_ID'] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>