<html>
	<head>
		<title>Customer - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$page = ($_GET["page"] ? $_GET["page"] : 1);
			$qsone = "";
		?>
	</head>
	<body>

		<?php
			require ("/include/header.php");
			require ("include/unset_value.php");

			if( $_SESSION["mixers"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Mixers </h3> </span>

			<div class="search_box">
				<form method="get" action="mixers.php">
					<input type="hidden" name="page" value="<?php echo $_GET["page"]; ?>">
					<input type="hidden" name="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]); ?>">
					<table class="search_tables_form">
						<tr>
							<td> Name: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
						</tr>
					</table>					
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>mixers.php'.'</td><td>'.$error.' near line 40.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryC = mysqli_prepare($db, "CALL sp_Mixer_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryC, "s", $search);
					$qryC->execute();
					$resultC = mysqli_stmt_get_result($qryC); //return results of query

					$total_results = mysqli_num_rows($resultC); //return number of rows of result
					
					$db->next_result();
					$resultC->close();

					$targetpage = "mixers.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Mixer_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>mixers.php'.'</td><td>'.$processError.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
						    <th>Name</th>
						    <th>Brand</th>
						    <th>Capacity (kg)</th>
						    <th>Active?</th>
					    	<th></th>
						</tr>
						<?php 
							while($row = mysqli_fetch_assoc($result))
							{
						?>		
								<tr>
									<td> <?php echo $row['name']; ?> </td>
									<td> <?php echo $row['brand']; ?> </td>
									<td> <?php echo number_format((float)($row['capacity']), 2, '.', ','); ?> </td>
									<td> <?php echo $row['active']; ?> </td>
									<td>
										<?php
											if(array_search(147, $session_Permit)){
										?>
												<input type='button' name='btnEdit' value='Define Capacity' onclick="location.href='new_mixers.php?id=<?php echo $row['id'];?>'">
										<?php
												$_SESSION['edit_mixers'] = true;
											}else{
												unset($_SESSION['edit_mixers']);
											}
							?>	
									</td>
								</tr>
						<?php
							}
								$db->next_result();
								$result->close();
						?>
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>