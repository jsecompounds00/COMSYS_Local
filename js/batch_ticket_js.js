!function(e,t,n){"use strict";!function o(e,t,n){function a(s,l){if(!t[s]){if(!e[s]){var i="function"==typeof require&&require;if(!l&&i)return i(s,!0);if(r)return r(s,!0);var u=new Error("Cannot find module '"+s+"'");throw u.code="MODULE_NOT_FOUND",u}var c=t[s]={exports:{}};e[s][0].call(c.exports,function(t){var n=e[s][1][t];return a(n?n:t)},c,c.exports,o,e,t,n)}return t[s].exports}for(var r="function"==typeof require&&require,s=0;s<n.length;s++)a(n[s]);return a}({1:[function(o){var a,r,s,l,i=function(e){return e&&e.__esModule?e:{"default":e}},u=o("./modules/handle-dom"),c=o("./modules/utils"),d=o("./modules/handle-swal-dom"),f=o("./modules/handle-click"),p=o("./modules/handle-key"),m=i(p),v=o("./modules/default-params"),y=i(v),h=o("./modules/set-params"),g=i(h);s=l=function(){function o(e){var t=s;return t[e]===n?y["default"][e]:t[e]}var s=arguments[0];if(u.addClass(t.body,"stop-scrolling"),d.resetInput(),s===n)return c.logStr("SweetAlert expects at least 1 attribute!"),!1;var i=c.extend({},y["default"]);switch(typeof s){case"string":i.title=s,i.text=arguments[1]||"",i.type=arguments[2]||"";break;case"object":if(s.title===n)return c.logStr('Missing "title" argument!'),!1;i.title=s.title;for(var p in y["default"])i[p]=o(p);i.confirmButtonText=i.showCancelButton?"Confirm":y["default"].confirmButtonText,i.confirmButtonText=o("confirmButtonText"),i.doneFunction=arguments[1]||null;break;default:return c.logStr('Unexpected type of argument! Expected "string" or "object", got '+typeof s),!1}g["default"](i),d.fixVerticalPosition(),d.openModal(arguments[1]);for(var v=d.getModal(),h=v.querySelectorAll("button"),b=["onclick","onmouseover","onmouseout","onmousedown","onmouseup","onfocus"],w=function(e){return f.handleButton(e,i,v)},C=0;C<h.length;C++)for(var S=0;S<b.length;S++){var x=b[S];h[C][x]=w}d.getOverlay().onclick=w,a=e.onkeydown;var k=function(e){return m["default"](e,i,v)};e.onkeydown=k,e.onfocus=function(){setTimeout(function(){r!==n&&(r.focus(),r=n)},0)},l.enableButtons()},s.setDefaults=l.setDefaults=function(e){if(!e)throw new Error("userParams is required");if("object"!=typeof e)throw new Error("userParams has to be a object");c.extend(y["default"],e)},s.close=l.close=function(){var o=d.getModal();u.fadeOut(d.getOverlay(),5),u.fadeOut(o,5),u.removeClass(o,"showSweetAlert"),u.addClass(o,"hideSweetAlert"),u.removeClass(o,"visible");var s=o.querySelector(".sa-icon.sa-success");u.removeClass(s,"animate"),u.removeClass(s.querySelector(".sa-tip"),"animateSuccessTip"),u.removeClass(s.querySelector(".sa-long"),"animateSuccessLong");var l=o.querySelector(".sa-icon.sa-error");u.removeClass(l,"animateErrorIcon"),u.removeClass(l.querySelector(".sa-x-mark"),"animateXMark");var i=o.querySelector(".sa-icon.sa-warning");return u.removeClass(i,"pulseWarning"),u.removeClass(i.querySelector(".sa-body"),"pulseWarningIns"),u.removeClass(i.querySelector(".sa-dot"),"pulseWarningIns"),setTimeout(function(){var e=o.getAttribute("data-custom-class");u.removeClass(o,e)},300),u.removeClass(t.body,"stop-scrolling"),e.onkeydown=a,e.previousActiveElement&&e.previousActiveElement.focus(),r=n,clearTimeout(o.timeout),!0},s.showInputError=l.showInputError=function(e){var t=d.getModal(),n=t.querySelector(".sa-input-error");u.addClass(n,"show");var o=t.querySelector(".sa-error-container");u.addClass(o,"show"),o.querySelector("p").innerHTML=e,setTimeout(function(){s.enableButtons()},1),t.querySelector("input").focus()},s.resetInputError=l.resetInputError=function(e){if(e&&13===e.keyCode)return!1;var t=d.getModal(),n=t.querySelector(".sa-input-error");u.removeClass(n,"show");var o=t.querySelector(".sa-error-container");u.removeClass(o,"show")},s.disableButtons=l.disableButtons=function(){var e=d.getModal(),t=e.querySelector("button.confirm"),n=e.querySelector("button.cancel");t.disabled=!0,n.disabled=!0},s.enableButtons=l.enableButtons=function(){var e=d.getModal(),t=e.querySelector("button.confirm"),n=e.querySelector("button.cancel");t.disabled=!1,n.disabled=!1},"undefined"!=typeof e?e.sweetAlert=e.swal=s:c.logStr("SweetAlert is a frontend module!")},{"./modules/default-params":2,"./modules/handle-click":3,"./modules/handle-dom":4,"./modules/handle-key":5,"./modules/handle-swal-dom":6,"./modules/set-params":8,"./modules/utils":9}],2:[function(e,t,n){Object.defineProperty(n,"__esModule",{value:!0});var o={title:"",text:"",type:null,allowOutsideClick:!1,showConfirmButton:!0,showCancelButton:!1,closeOnConfirm:!0,closeOnCancel:!0,confirmButtonText:"OK",confirmButtonColor:"#8CD4F5",cancelButtonText:"Cancel",imageUrl:null,imageSize:null,timer:null,customClass:"",html:!1,animation:!0,allowEscapeKey:!0,inputType:"text",inputPlaceholder:"",inputValue:"",showLoaderOnConfirm:!1};n["default"]=o,t.exports=n["default"]},{}],3:[function(t,n,o){Object.defineProperty(o,"__esModule",{value:!0});var a=t("./utils"),r=(t("./handle-swal-dom"),t("./handle-dom")),s=function(t,n,o){function s(e){m&&n.confirmButtonColor&&(p.style.backgroundColor=e)}var u,c,d,f=t||e.event,p=f.target||f.srcElement,m=-1!==p.className.indexOf("confirm"),v=-1!==p.className.indexOf("sweet-overlay"),y=r.hasClass(o,"visible"),h=n.doneFunction&&"true"===o.getAttribute("data-has-done-function");switch(m&&n.confirmButtonColor&&(u=n.confirmButtonColor,c=a.colorLuminance(u,-.04),d=a.colorLuminance(u,-.14)),f.type){case"mouseover":s(c);break;case"mouseout":s(u);break;case"mousedown":s(d);break;case"mouseup":s(c);break;case"focus":var g=o.querySelector("button.confirm"),b=o.querySelector("button.cancel");m?b.style.boxShadow="none":g.style.boxShadow="none";break;case"click":var w=o===p,C=r.isDescendant(o,p);if(!w&&!C&&y&&!n.allowOutsideClick)break;m&&h&&y?l(o,n):h&&y||v?i(o,n):r.isDescendant(o,p)&&"BUTTON"===p.tagName&&sweetAlert.close()}},l=function(e,t){var n=!0;r.hasClass(e,"show-input")&&(n=e.querySelector("input").value,n||(n="")),t.doneFunction(n),t.closeOnConfirm&&sweetAlert.close(),t.showLoaderOnConfirm&&sweetAlert.disableButtons()},i=function(e,t){var n=String(t.doneFunction).replace(/\s/g,""),o="function("===n.substring(0,9)&&")"!==n.substring(9,10);o&&t.doneFunction(!1),t.closeOnCancel&&sweetAlert.close()};o["default"]={handleButton:s,handleConfirm:l,handleCancel:i},n.exports=o["default"]},{"./handle-dom":4,"./handle-swal-dom":6,"./utils":9}],4:[function(n,o,a){Object.defineProperty(a,"__esModule",{value:!0});var r=function(e,t){return new RegExp(" "+t+" ").test(" "+e.className+" ")},s=function(e,t){r(e,t)||(e.className+=" "+t)},l=function(e,t){var n=" "+e.className.replace(/[\t\r\n]/g," ")+" ";if(r(e,t)){for(;n.indexOf(" "+t+" ")>=0;)n=n.replace(" "+t+" "," ");e.className=n.replace(/^\s+|\s+$/g,"")}},i=function(e){var n=t.createElement("div");return n.appendChild(t.createTextNode(e)),n.innerHTML},u=function(e){e.style.opacity="",e.style.display="block"},c=function(e){if(e&&!e.length)return u(e);for(var t=0;t<e.length;++t)u(e[t])},d=function(e){e.style.opacity="",e.style.display="none"},f=function(e){if(e&&!e.length)return d(e);for(var t=0;t<e.length;++t)d(e[t])},p=function(e,t){for(var n=t.parentNode;null!==n;){if(n===e)return!0;n=n.parentNode}return!1},m=function(e){e.style.left="-9999px",e.style.display="block";var t,n=e.clientHeight;return t="undefined"!=typeof getComputedStyle?parseInt(getComputedStyle(e).getPropertyValue("padding-top"),10):parseInt(e.currentStyle.padding),e.style.left="",e.style.display="none","-"+parseInt((n+t)/2)+"px"},v=function(e,t){if(+e.style.opacity<1){t=t||16,e.style.opacity=0,e.style.display="block";var n=+new Date,o=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){e.style.opacity=+e.style.opacity+(new Date-n)/100,n=+new Date,+e.style.opacity<1&&setTimeout(o,t)});o()}e.style.display="block"},y=function(e,t){t=t||16,e.style.opacity=1;var n=+new Date,o=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){e.style.opacity=+e.style.opacity-(new Date-n)/100,n=+new Date,+e.style.opacity>0?setTimeout(o,t):e.style.display="none"});o()},h=function(n){if("function"==typeof MouseEvent){var o=new MouseEvent("click",{view:e,bubbles:!1,cancelable:!0});n.dispatchEvent(o)}else if(t.createEvent){var a=t.createEvent("MouseEvents");a.initEvent("click",!1,!1),n.dispatchEvent(a)}else t.createEventObject?n.fireEvent("onclick"):"function"==typeof n.onclick&&n.onclick()},g=function(t){"function"==typeof t.stopPropagation?(t.stopPropagation(),t.preventDefault()):e.event&&e.event.hasOwnProperty("cancelBubble")&&(e.event.cancelBubble=!0)};a.hasClass=r,a.addClass=s,a.removeClass=l,a.escapeHtml=i,a._show=u,a.show=c,a._hide=d,a.hide=f,a.isDescendant=p,a.getTopMargin=m,a.fadeIn=v,a.fadeOut=y,a.fireClick=h,a.stopEventPropagation=g},{}],5:[function(t,o,a){Object.defineProperty(a,"__esModule",{value:!0});var r=t("./handle-dom"),s=t("./handle-swal-dom"),l=function(t,o,a){var l=t||e.event,i=l.keyCode||l.which,u=a.querySelector("button.confirm"),c=a.querySelector("button.cancel"),d=a.querySelectorAll("button[tabindex]");if(-1!==[9,13,32,27].indexOf(i)){for(var f=l.target||l.srcElement,p=-1,m=0;m<d.length;m++)if(f===d[m]){p=m;break}9===i?(f=-1===p?u:p===d.length-1?d[0]:d[p+1],r.stopEventPropagation(l),f.focus(),o.confirmButtonColor&&s.setFocusStyle(f,o.confirmButtonColor)):13===i?("INPUT"===f.tagName&&(f=u,u.focus()),f=-1===p?u:n):27===i&&o.allowEscapeKey===!0?(f=c,r.fireClick(f,l)):f=n}};a["default"]=l,o.exports=a["default"]},{"./handle-dom":4,"./handle-swal-dom":6}],6:[function(n,o,a){var r=function(e){return e&&e.__esModule?e:{"default":e}};Object.defineProperty(a,"__esModule",{value:!0});var s=n("./utils"),l=n("./handle-dom"),i=n("./default-params"),u=r(i),c=n("./injected-html"),d=r(c),f=".sweet-alert",p=".sweet-overlay",m=function(){var e=t.createElement("div");for(e.innerHTML=d["default"];e.firstChild;)t.body.appendChild(e.firstChild)},v=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){var e=t.querySelector(f);return e||(m(),e=v()),e}),y=function(){var e=v();return e?e.querySelector("input"):void 0},h=function(){return t.querySelector(p)},g=function(e,t){var n=s.hexToRgb(t);e.style.boxShadow="0 0 2px rgba("+n+", 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)"},b=function(n){var o=v();l.fadeIn(h(),10),l.show(o),l.addClass(o,"showSweetAlert"),l.removeClass(o,"hideSweetAlert"),e.previousActiveElement=t.activeElement;var a=o.querySelector("button.confirm");a.focus(),setTimeout(function(){l.addClass(o,"visible")},500);var r=o.getAttribute("data-timer");if("null"!==r&&""!==r){var s=n;o.timeout=setTimeout(function(){var e=(s||null)&&"true"===o.getAttribute("data-has-done-function");e?s(null):sweetAlert.close()},r)}},w=function(){var e=v(),t=y();l.removeClass(e,"show-input"),t.value=u["default"].inputValue,t.setAttribute("type",u["default"].inputType),t.setAttribute("placeholder",u["default"].inputPlaceholder),C()},C=function(e){if(e&&13===e.keyCode)return!1;var t=v(),n=t.querySelector(".sa-input-error");l.removeClass(n,"show");var o=t.querySelector(".sa-error-container");l.removeClass(o,"show")},S=function(){var e=v();e.style.marginTop=l.getTopMargin(v())};a.sweetAlertInitialize=m,a.getModal=v,a.getOverlay=h,a.getInput=y,a.setFocusStyle=g,a.openModal=b,a.resetInput=w,a.resetInputError=C,a.fixVerticalPosition=S},{"./default-params":2,"./handle-dom":4,"./injected-html":7,"./utils":9}],7:[function(e,t,n){Object.defineProperty(n,"__esModule",{value:!0});var o='<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert"><div class="sa-icon sa-error">\n      <span class="sa-x-mark">\n        <span class="sa-line sa-left"></span>\n        <span class="sa-line sa-right"></span>\n      </span>\n    </div><div class="sa-icon sa-warning">\n      <span class="sa-body"></span>\n      <span class="sa-dot"></span>\n    </div><div class="sa-icon sa-info"></div><div class="sa-icon sa-success">\n      <span class="sa-line sa-tip"></span>\n      <span class="sa-line sa-long"></span>\n\n      <div class="sa-placeholder"></div>\n      <div class="sa-fix"></div>\n    </div><div class="sa-icon sa-custom"></div><h2>Title</h2>\n    <p>Text</p>\n    <fieldset>\n      <input type="text" tabIndex="3" />\n      <div class="sa-input-error"></div>\n    </fieldset><div class="sa-error-container">\n      <div class="icon">!</div>\n      <p>Not valid!</p>\n    </div><div class="sa-button-container">\n      <button class="cancel" tabIndex="2">Cancel</button>\n      <div class="sa-confirm-button-container">\n        <button class="confirm" tabIndex="1">OK</button><div class="la-ball-fall">\n          <div></div>\n          <div></div>\n          <div></div>\n        </div>\n      </div>\n    </div></div>';n["default"]=o,t.exports=n["default"]},{}],8:[function(e,t,o){Object.defineProperty(o,"__esModule",{value:!0});var a=e("./utils"),r=e("./handle-swal-dom"),s=e("./handle-dom"),l=["error","warning","info","success","input","prompt"],i=function(e){var t=r.getModal(),o=t.querySelector("h2"),i=t.querySelector("p"),u=t.querySelector("button.cancel"),c=t.querySelector("button.confirm");if(o.innerHTML=e.html?e.title:s.escapeHtml(e.title).split("\n").join("<br>"),i.innerHTML=e.html?e.text:s.escapeHtml(e.text||"").split("\n").join("<br>"),e.text&&s.show(i),e.customClass)s.addClass(t,e.customClass),t.setAttribute("data-custom-class",e.customClass);else{var d=t.getAttribute("data-custom-class");s.removeClass(t,d),t.setAttribute("data-custom-class","")}if(s.hide(t.querySelectorAll(".sa-icon")),e.type&&!a.isIE8()){var f=function(){for(var o=!1,a=0;a<l.length;a++)if(e.type===l[a]){o=!0;break}if(!o)return logStr("Unknown alert type: "+e.type),{v:!1};var i=["success","error","warning","info"],u=n;-1!==i.indexOf(e.type)&&(u=t.querySelector(".sa-icon.sa-"+e.type),s.show(u));var c=r.getInput();switch(e.type){case"success":s.addClass(u,"animate"),s.addClass(u.querySelector(".sa-tip"),"animateSuccessTip"),s.addClass(u.querySelector(".sa-long"),"animateSuccessLong");break;case"error":s.addClass(u,"animateErrorIcon"),s.addClass(u.querySelector(".sa-x-mark"),"animateXMark");break;case"warning":s.addClass(u,"pulseWarning"),s.addClass(u.querySelector(".sa-body"),"pulseWarningIns"),s.addClass(u.querySelector(".sa-dot"),"pulseWarningIns");break;case"input":case"prompt":c.setAttribute("type",e.inputType),c.value=e.inputValue,c.setAttribute("placeholder",e.inputPlaceholder),s.addClass(t,"show-input"),setTimeout(function(){c.focus(),c.addEventListener("keyup",swal.resetInputError)},400)}}();if("object"==typeof f)return f.v}if(e.imageUrl){var p=t.querySelector(".sa-icon.sa-custom");p.style.backgroundImage="url("+e.imageUrl+")",s.show(p);var m=80,v=80;if(e.imageSize){var y=e.imageSize.toString().split("x"),h=y[0],g=y[1];h&&g?(m=h,v=g):logStr("Parameter imageSize expects value with format WIDTHxHEIGHT, got "+e.imageSize)}p.setAttribute("style",p.getAttribute("style")+"width:"+m+"px; height:"+v+"px")}t.setAttribute("data-has-cancel-button",e.showCancelButton),e.showCancelButton?u.style.display="inline-block":s.hide(u),t.setAttribute("data-has-confirm-button",e.showConfirmButton),e.showConfirmButton?c.style.display="inline-block":s.hide(c),e.cancelButtonText&&(u.innerHTML=s.escapeHtml(e.cancelButtonText)),e.confirmButtonText&&(c.innerHTML=s.escapeHtml(e.confirmButtonText)),e.confirmButtonColor&&(c.style.backgroundColor=e.confirmButtonColor,c.style.borderLeftColor=e.confirmLoadingButtonColor,c.style.borderRightColor=e.confirmLoadingButtonColor,r.setFocusStyle(c,e.confirmButtonColor)),t.setAttribute("data-allow-outside-click",e.allowOutsideClick);var b=e.doneFunction?!0:!1;t.setAttribute("data-has-done-function",b),e.animation?"string"==typeof e.animation?t.setAttribute("data-animation",e.animation):t.setAttribute("data-animation","pop"):t.setAttribute("data-animation","none"),t.setAttribute("data-timer",e.timer)};o["default"]=i,t.exports=o["default"]},{"./handle-dom":4,"./handle-swal-dom":6,"./utils":9}],9:[function(t,n,o){Object.defineProperty(o,"__esModule",{value:!0});var a=function(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e},r=function(e){var t=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);return t?parseInt(t[1],16)+", "+parseInt(t[2],16)+", "+parseInt(t[3],16):null},s=function(){return e.attachEvent&&!e.addEventListener},l=function(t){e.console&&e.console.log("SweetAlert: "+t)},i=function(e,t){e=String(e).replace(/[^0-9a-f]/gi,""),e.length<6&&(e=e[0]+e[0]+e[1]+e[1]+e[2]+e[2]),t=t||0;var n,o,a="#";for(o=0;3>o;o++)n=parseInt(e.substr(2*o,2),16),n=Math.round(Math.min(Math.max(0,n+n*t),255)).toString(16),a+=("00"+n).substr(n.length);return a};o.extend=a,o.hexToRgb=r,o.isIE8=s,o.logStr=l,o.colorLuminance=i},{}]},{},[1]),"function"==typeof define&&define.amd?define(function(){return sweetAlert}):"undefined"!=typeof module&&module.exports&&(module.exports=sweetAlert)}(window,document);
function showFormulaType(str, fg_type, bat_id)
{
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltFormulaType").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/formula_type_dropdown.php?fg_id="+str+"&fg_type="+fg_type+"&bat_id="+bat_id,true);
    xmlhttp.send();
}

function showFormula(str, trial)
{
  var sltActivity = document.getElementById('sltActivity').value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("standard_formula").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/standard_formula.php?formula_type="+str+"&trial="+trial+"&sltActivity="+sltActivity,true);
    xmlhttp.send();
}

function showFG(fg, batID)
{
  var soft = document.getElementById('Soft');
  var rigid = document.getElementById('Rigid');
  var sltActivity = document.getElementById('sltActivity').value;
  var txtExtDarNo = document.getElementById('txtExtDarNo');
  var txtMixDarNo = document.getElementById('txtMixDarNo');
  var txtExtZ1 = document.getElementById('txtExtZ1');
  var txtMixParam1 = document.getElementById('txtMixParam1');
  var txtMixSequence1 = document.getElementById('txtMixSequence1');
  var txtExtZ2 = document.getElementById('txtExtZ2');
  var txtMixParm2 = document.getElementById('txtMixParm2');
  var txtMixSequence2 = document.getElementById('txtMixSequence2');
  var txtExtDH = document.getElementById('txtExtDH');
  var txtMixParam3 = document.getElementById('txtMixParam3');
  var txtMixSequence3 = document.getElementById('txtMixSequence3');
  var txtExtScrewSpeed = document.getElementById('txtExtScrewSpeed');
  var txtMixParam4 = document.getElementById('txtMixParam4');
  var txtMixSequence4 = document.getElementById('txtMixSequence4');
  var txtExtCutterSpeed = document.getElementById('txtExtCutterSpeed');
  var txtMixParam5 = document.getElementById('txtMixParam5');
  var txtSequence5 = document.getElementById('txtSequence5');

  if ( soft.checked == true )
  {
    txtExtDarNo.readOnly = false;
    txtExtZ1.readOnly = false;
    txtExtZ2.readOnly = false;
    txtExtDH.readOnly = false;
    txtExtScrewSpeed.readOnly = false;
    txtExtCutterSpeed.readOnly = false;
    var soft_pvc = 1;
  }
  else if ( rigid.checked == true )
  {
    txtExtDarNo.readOnly = true;
    txtExtZ1.readOnly = true;
    txtExtZ2.readOnly = true;
    txtExtDH.readOnly = true;
    txtExtScrewSpeed.readOnly = true;
    txtExtCutterSpeed.readOnly = true;
    var soft_pvc = 0;
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltFG").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/fg_dropdown.php?soft_pvc=" + soft_pvc + "&fg=" + fg + "&batID=" + batID + "&sltActivity=" + sltActivity,true);
    xmlhttp.send();

}

function exclude(){
  var chk_count = document.getElementsByName('chkExclude[]').length;
  var chk_val = new Array();
  var phr_val = new Array();
  var total = document.getElementById('txtTotalQty').value;
  var sltActivity = document.getElementById('sltActivity').value;

  for (i = 0; i < chk_count; i++) {
    chk_val[i] = document.getElementById('chkExclude'+i);
    phr_val[i] = document.getElementById('hidPhr'+i).value;

    if ( chk_val[i].checked == true ){
      if ( sltActivity != 5 ){
        document.getElementById('txtRM'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtPhr'+i).value = 0.0000;
      }
      else{
        document.getElementById('txtPhr'+i).readOnly = true;
        document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtQty'+i).readOnly = true;
        document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtPhr'+i).value = 0.0000;
      }
    }//end of if
    else{
      if ( sltActivity != 5 ){
        document.getElementById('txtRM'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
        document.getElementById('txtPhr'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
        document.getElementById('txtQty'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
        document.getElementById('txtPhr'+i).value = 0;
        document.getElementById('txtPhr'+i).value = phr_val[i];
      }else{
        document.getElementById('txtPhr'+i).readOnly = false;
        document.getElementById('txtPhr'+i).style.cssText = 'background: #ffffff;';
        document.getElementById('txtQty'+i).readOnly = true;
        document.getElementById('txtQty'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
        document.getElementById('txtPhr'+i).value = 0;
        document.getElementById('txtPhr'+i).value = phr_val[i];
      }
    }//end of else

  }//end of for loop

}//end of functin exclude()

function excludeModify(){
  var chk_count = document.getElementsByName('chkExclude[]').length;
  var chk_val = new Array();
  var phr_val = new Array();
  var total = document.getElementById('txtTotalQty').value;
  var hidActivity = document.getElementById('hidActivity').value;

  for (i = 0; i < chk_count; i++) {
    chk_val[i] = document.getElementById('chkExclude'+i);
    phr_val[i] = document.getElementById('hidPhr'+i).value;

    if ( chk_val[i].checked == true ){
      document.getElementById('txtRM_id'+i).disabled = true;
      document.getElementById('txtRM_id'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;';
      document.getElementById('txtPhr'+i).readOnly = true;
      document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;';
      document.getElementById('txtQty'+i).readOnly = true;
      document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;';
      document.getElementById('txtPhr'+i).value = 0.0000;
    }//end of if
    else{
      document.getElementById('txtRM_id'+i).disabled = false;
      document.getElementById('txtRM_id'+i).style.cssText = 'background: none;';
      document.getElementById('txtPhr'+i).readOnly = false;
      document.getElementById('txtPhr'+i).style.cssText = 'background: #ffffff;';
      document.getElementById('txtQty'+i).readOnly = true;
      document.getElementById('txtQty'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
      document.getElementById('txtPhr'+i).value = 0;
      document.getElementById('txtPhr'+i).value = phr_val[i];
    }//end of else

  }//end of for loop

}//end of functin excludeModify()

function enableRM_ID(){
  var rm_id_length = document.getElementsByName('txtRM_id[]').length;

  for ( var i = 0; i < rm_id_length; i++ ){
    document.getElementById('txtRM_id'+i).disabled = false;
  }

}

function autoMultiply(){
  var multiplier = document.getElementById("txtMultiplier");
  var phr = document.getElementById("txtNPHR");

  if ( document.getElementById('txtNPHR').disabled == true ){
    var result = 0 * phr.value;
  }else{
    var result = multiplier.value * phr.value;
  }
  
  if(!isNaN(result)){
      document.getElementById('txtNQuantity').value = result.toFixed(5);
  }//end of 1st if

  var i
  var phr_count = document.getElementsByName('txtPhr[]').length;
  var results = new Array();
  var phrs = new Array();
  var qty = new Array();

  for (i = 0; i < phr_count; i++) {
    phrs[i] = document.getElementById('txtPhr'+i).value;

    if ( document.getElementById('txtPhr'+i).disabled == true ){
      results[i] = 0 * parseFloat(phrs[i]);
    }else{
      results[i] = parseFloat(multiplier.value) * parseFloat(phrs[i]);
    }

    if(!isNaN(results[i])){
        document.getElementById('txtQty'+i).value = results[i].toFixed(5);
    }//end of 2nd if

  }//end of for loop

}//end of functin autoMultiply()

function autoSum(){
  var qty_count = document.getElementsByName('txtQty[]').length;
  var nqty = document.getElementById('txtNQuantity').value;
  var qty = new Array();
  var total = 0;

  for (i = 0; i < qty_count; i++) {
    qty[i] = document.getElementById('txtQty'+i).value;
    total = parseFloat(total) + parseFloat(qty[i]);
  }//end of for loop

  total = parseFloat(total) + parseFloat(nqty);
  total = parseFloat(total);

  if(!isNaN(total)){
      document.getElementById('txtTotalQty').value = total.toFixed(5);
  }//end of if

}//end of functin autoSum()

function changeCheckBox()
{
  var chkExclude = document.getElementsByName("chkExclude[]");
  var clength = chkExclude.length;
  var PreQual = document.getElementById("PreQual");
  var SamplePrep = document.getElementById("SamplePrep");

  if ( PreQual.checked == true ){
     for (var i = 0; i < clength; i++) {
      document.getElementById("chkExclude"+i).disabled=true;
      document.getElementById("chkExclude"+i).checked=false;
      document.getElementById('txtRM'+i).readOnly = true;
      document.getElementById('txtPhr'+i).readOnly = true;
      document.getElementById('txtQty'+i).readOnly = true;
     }
     document.getElementById("txtNewRm").disabled=true;
     document.getElementById("txtNPHR").disabled=true;
     document.getElementById("txtNQuantity").disabled=true;
     document.getElementById("txtNQuantity").readOnly=false;
  }else if ( SamplePrep.checked == true ){
     for (var i = 0; i < clength; i++) {
      document.getElementById("chkExclude"+i).disabled=false;
     }
     document.getElementById("txtNewRm").disabled=false;
     document.getElementById("txtNPHR").disabled=false;
     document.getElementById("txtNQuantity").disabled=false;
     document.getElementById("txtNQuantity").readOnly=true;
  }

}

function editable()
{
  if ( document.getElementById('chkEdit').checked == true )
  {
    document.getElementById('txtMixParam1').readOnly = false;
    document.getElementById('txtMixSequence1').readOnly = false;
    document.getElementById('txtMixParam2').readOnly = false;
    document.getElementById('txtMixSequence2').readOnly = false;
    document.getElementById('txtMixParam3').readOnly = false;
    document.getElementById('txtMixSequence3').readOnly = false;
    document.getElementById('txtMixParam4').readOnly = false;
    document.getElementById('txtMixSequence4').readOnly = false;
    document.getElementById('txtMixParam5').readOnly = false;
    document.getElementById('txtMixSequence5').readOnly = false;
  }
  else
  {
    document.getElementById('txtMixParam1').readOnly = true;
    document.getElementById('txtMixSequence1').readOnly = true;
    document.getElementById('txtMixParam2').readOnly = true;
    document.getElementById('txtMixSequence2').readOnly = true;
    document.getElementById('txtMixParam3').readOnly = true;
    document.getElementById('txtMixSequence3').readOnly = true;
    document.getElementById('txtMixParam4').readOnly = true;
    document.getElementById('txtMixSequence4').readOnly = true;
    document.getElementById('txtMixParam5').readOnly = true;
    document.getElementById('txtMixSequence5').readOnly = true;
  }

}

function enableExtrusion(){

  var soft = document.getElementById('Soft');
  var rigid = document.getElementById('Rigid');

  if ( soft.checked == true )
  {
    txtExtDarNo.readOnly = false;
    txtExtZ1.readOnly = false;
    txtExtZ2.readOnly = false;
    txtExtDH.readOnly = false;
    txtExtScrewSpeed.readOnly = false;
    txtExtCutterSpeed.readOnly = false;
    var soft_pvc = 1;
  }
  else if ( rigid.checked == true )
  {
    txtExtDarNo.readOnly = true;
    txtExtZ1.readOnly = true;
    txtExtZ2.readOnly = true;
    txtExtDH.readOnly = true;
    txtExtScrewSpeed.readOnly = true;
    txtExtCutterSpeed.readOnly = true;
    var soft_pvc = 0;
  }
  
}

function disabledOptionFG(){
  var sltFGLen = document.getElementById('sltFG').length;
  var sltActivity = document.getElementById('sltActivity').value;

  for ( var i = 0; i < sltFGLen; i++ ){
    if ( sltActivity == 4 || sltActivity == 2 ){
      document.getElementById('optionFG'+i).disabled = true;
    }else{
      document.getElementById('optionFG'+i).disabled = false;
    }
  }

}

function enableOtherSupplier(){
  var otherSupplier = document.getElementById('otherSupplier');
  var optionLength = document.getElementById('sltSupplier').length;

  if ( otherSupplier.checked == true ) {
    document.getElementById("txtOtherSupplier").readOnly = false;
    for ( var i = 0; i < optionLength; i++ ){
      document.getElementById('optionSupplier'+i).disabled = true;
    }
    // alert(optionLength);
  }else if ( otherSupplier.checked == false ){
    document.getElementById("txtOtherSupplier").readOnly = true;
    for ( var i = 0; i < optionLength; i++ ){
      document.getElementById('optionSupplier'+i).disabled = false;
    }
  }
}

function enableClassOthers(){
  var radClassOthers = document.getElementById('radClassOthers');
  var radClassResin = document.getElementById('radClassResin');
  var radClassStabilizer = document.getElementById('radClassStabilizer');
  var radClassFillers = document.getElementById('radClassFillers');
  var radClassPlasticizer = document.getElementById('radClassPlasticizer');
  var radClassLubricant = document.getElementById('radClassLubricant');
  var radClassColorant = document.getElementById('radClassColorant');

  if ( radClassOthers.checked == true ) {
    document.getElementById("txtOtherClass").readOnly = false;
  }else if ( radClassOthers.checked == false 
            || radClassResin.checked == true
            || radClassStabilizer.checked == true
            || radClassFillers.checked == true
            || radClassPlasticizer.checked == true
            || radClassLubricant.checked == true
            || radClassColorant.checked == true ){
    document.getElementById("txtOtherClass").readOnly = true;
  }
}

function enableSampleOthers(){
  var radSamplePowder = document.getElementById('radSamplePowder');
  var radSampleLiquid = document.getElementById('radSampleLiquid');
  var radSampleOthers = document.getElementById('radSampleOthers');
  var radSamplePellet = document.getElementById('radSamplePellet');

  if ( radSampleOthers.checked == true ) {
    document.getElementById("txtOtherSample").readOnly = false;
  }else if ( radSampleOthers.checked == false 
            || radSamplePowder.checked == true
            || radSampleLiquid.checked == true
            || radSamplePellet.checked == true){
    document.getElementById("txtOtherSample").readOnly = true;
  }
}

function showRNDActivity()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      rnd_activity = NRM.value;
    }else if ( JRD.checked == true ){
      rnd_activity = JRD.value;
    }

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltActivity").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/activity_dropdown.php?rnd_activity=" + rnd_activity,true);
    xmlhttp.send();
}

function showNRMNumber()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      nrm = 1;
    }else if ( JRD.checked == true ){
      nrm = 0;
    }

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltNRMNumber").innerHTML=xmlhttp.responseText;
      }
    }

        xmlhttp.open("GET","include/nrm_number_dropdown.php?nrm=" + nrm,true);
        xmlhttp.send();
}

function showJRDNumber()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      jrd = 0;
    }else if ( JRD.checked == true ){
      jrd = 1;
    }
    // alert('nrm '+ nrm + 'jrd '+ jrd );
    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltJRDNumber").innerHTML=xmlhttp.responseText;
      }
    }

        xmlhttp.open("GET","include/jrd_number_dropdown.php?jrd=" + jrd,true);
        xmlhttp.send();
}

function showRM(rm_type, index){
  var txtRM_type_id = document.getElementById('txtRM_type_id');

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("txtRM_id"+index).innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/rm_bat_dropdown.php?rm_type=" + rm_type + "&index=" + index,true);
    xmlhttp.send();
}

function showNRMResults(){

  var sltNRM = document.getElementById("sltNRM").value;

    // alert(sltNRM);

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("NRMResults").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/nrm_results.php?sltNRM=" + sltNRM,true);
    xmlhttp.send();

}
function disableNewCustomer(){
  var chkNewCustomer = document.getElementById("chkNewCustomer");
  var txtNewCustomer = document.getElementById("txtNewCustomer");
  var sltExistingCustomer = document.getElementById("sltExistingCustomer").length;
  var i = 0;

  if ( chkNewCustomer.checked == true ){
    txtNewCustomer.disabled = false;
    for ( i = 0; i < sltExistingCustomer; i++ ){
      document.getElementById("optionCustomer"+i).disabled = true;
    }
  }else{
    txtNewCustomer.disabled = true;
    for ( i = 0; i < sltExistingCustomer; i++ ){
      document.getElementById("optionCustomer"+i).disabled = false;
    }

  }
}
function showCustomerReq(JRDId){
  var radRequestType = document.getElementsByName("radRequestType")
  
  for ( var i = 0; i < radRequestType.length; i++ ){

    if ( radRequestType[i].checked == true ){
      var RequestType = document.getElementsByName("radRequestType")[i].value;
    }

  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("tbodyCustomerReq").innerHTML=xmlhttp.responseText;
        }
      }
    if ( RequestType == 'Development' ){
      xmlhttp.open("GET","include/product_development.php?JRDId=" + JRDId, true);
      xmlhttp.send();
    }else if ( RequestType == 'Modification' ){
      xmlhttp.open("GET","include/product_modification.php?JRDId=" + JRDId, true);
      xmlhttp.send();
    }else if ( RequestType == 'Evaluation' ){
      xmlhttp.open("GET","include/product_evaluation.php?JRDId=" + JRDId, true);
      xmlhttp.send();
    }

}
function enableOtherReason(){
  var Others = document.getElementById("Others");

  if ( Others.checked == true ){
    document.getElementById("txtOtherReason").disabled = false;
  }else{
    document.getElementById("txtOtherReason").disabled = true;
  }
}
function enableOtherExtrusionType(){
  var radExtrusionType = document.getElementsByName("radExtrusionType");

  for ( var i = 0; i < radExtrusionType.length; i++ ){

    if ( radExtrusionType[i].checked == true ){
      var ExtrusionType = document.getElementsByName("radExtrusionType")[i].value;
    }

  }

  if ( ExtrusionType == 'OtherExtrusionType' ){
    document.getElementById("txtOtherExtrusionType").disabled = false;
  }else{
    document.getElementById("txtOtherExtrusionType").disabled = true;
  }
}
function enableOtherThermalRate(){
  var radThermalRate = document.getElementsByName("radThermalRate");
  
  for ( var i = 0; i < radThermalRate.length; i++ ){

    if ( radThermalRate[i].checked == true ){
      var ThermalRate = document.getElementsByName("radThermalRate")[i].value;
    }

  }

  if ( ThermalRate == 'OtherThermalRate' ){
    document.getElementById("txtOtherThermalRate").disabled = false;
  }else{
    document.getElementById("txtOtherThermalRate").disabled = true;
  }
}
function enableOtherMoldingType(){
  var radMoldingType = document.getElementsByName("radMoldingType");
  
  for ( var i = 0; i < radMoldingType.length; i++ ){

    if ( radMoldingType[i].checked == true ){
      var ThermalRate = document.getElementsByName("radMoldingType")[i].value;
    }

  }

  if ( ThermalRate == 'OtherMoldingType' ){
    document.getElementById("txtOtherMoldingType").disabled = false;
  }else{
    document.getElementById("txtOtherMoldingType").disabled = true;
  }
}
function enableOtherReq(){
  var OtherReq = document.getElementById("OtherReq");

  if ( OtherReq.checked == true ){
    document.getElementById("txtOtherReq").disabled = false;
  }else{
    document.getElementById("txtOtherReq").disabled = true;
  }

}
function enableSubCategories(){
  var chkExtrusion = document.getElementById("chkExtrusion");
  var chkMolding = document.getElementById("chkMolding");
  var chkOtherActivity = document.getElementById("chkOtherActivity");

  if ( chkExtrusion.checked == true ){
    document.getElementById("Jacket").disabled = false;
    document.getElementById("Insulation").disabled = false;
    document.getElementById("OtherExtrusionType").disabled = false;
    document.getElementById("60C").disabled = false;
    document.getElementById("75C").disabled = false;
    document.getElementById("90C").disabled = false;
    document.getElementById("105C").disabled = false;
    document.getElementById("OtherThermalRate").disabled = false;
  }else{
    document.getElementById("Jacket").disabled = true;
    document.getElementById("Insulation").disabled = true;
    document.getElementById("OtherExtrusionType").disabled = true;
    document.getElementById("txtOtherExtrusionType").disabled = true;
    document.getElementById("60C").disabled = true;
    document.getElementById("75C").disabled = true;
    document.getElementById("90C").disabled = true;
    document.getElementById("105C").disabled = true;
    document.getElementById("OtherThermalRate").disabled = true;
    document.getElementById("txtOtherThermalRate").disabled = true;
  }

  if ( chkMolding.checked == true ){
    document.getElementById("30").disabled = false;
    document.getElementById("55").disabled = false;
    document.getElementById("Tube").disabled = false;
    document.getElementById("40").disabled = false;
    document.getElementById("Film").disabled = false;
    document.getElementById("OtherMoldingType").disabled = false;
    document.getElementById("45").disabled = false;
    document.getElementById("Bottle").disabled = false;
  }else{
    document.getElementById("30").disabled = true;
    document.getElementById("55").disabled = true;
    document.getElementById("Tube").disabled = true;
    document.getElementById("40").disabled = true;
    document.getElementById("Film").disabled = true;
    document.getElementById("OtherMoldingType").disabled = true;
    document.getElementById("45").disabled = true;
    document.getElementById("Bottle").disabled = true;
  }

  if ( chkOtherActivity.checked == true ){
    document.getElementById("txtOtherActivity").disabled = false;
  }else{
    document.getElementById("txtOtherActivity").disabled = true;
  }

}

function showBatchTicketTrialsM(){

  var sltFormulaType = document.getElementById("sltFormulaType").value;
  
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("standard_formula").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/standard_formula.php?formula_type="+sltFormulaType+"&trial=1"+"&sltActivity=5",true);
    xmlhttp.send();

}

function showJRDDataNoReplicate(){

  var sltJRDNumber = document.getElementById("sltJRDNumber").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("tbJRDData").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/jrd_data_noreplicate_dropdown.php?sltJRDNumber=" + sltJRDNumber,true);
  xmlhttp.send();

}

function showFGtoEvaluate(){

  var sltFGEvaluate = document.getElementById("sltFGEvaluate").value;

  var product_id = sltFGEvaluate.substring(0, sltFGEvaluate.indexOf('-'));
  var product_name = sltFGEvaluate.substring(sltFGEvaluate.indexOf('-')+1);

  document.getElementById("hidProductID").value = product_id;
  document.getElementById("hidProductName").value = product_name;

}

function showJRDData(){

  var sltJRDNumber = document.getElementById("sltJRDNumber").value;
  var hidBatchTicketId = document.getElementById("hidBatchTicketId").value;
  var hidBatchTicketTrialId = document.getElementById("hidBatchTicketTrialId").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("tbJRDData").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/jrd_data_dropdown.php?sltJRDNumber=" + sltJRDNumber + "&hidBatchTicketId=" + hidBatchTicketId + "&hidBatchTicketTrialId=" + hidBatchTicketTrialId,true);
  xmlhttp.send();

}

function showJRDResults(){

  var sltJRD = document.getElementById("sltJRD").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("JRDResults").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/jrd_results.php?sltJRD=" + sltJRD,true);
    xmlhttp.send();

}
function cancelBox(id, trial_no){
  swal(
      {
        title: "Are you sure you want to \ncancel trial# " + trial_no + "?",   
        text: "Enter reason for cancellation:",   
        type: "input",   
        confirmButtonColor: "#DD6B55", 
        confirmButtonText: "YES", 
        showCancelButton: true,
        closeOnConfirm: false,   
        inputPlaceholder: "Enter reason"
      },
        function(inputValue){
          if (inputValue === false) {
            return false;      
          }
          if (inputValue.trim() === "") {     
            swal.showInputError("You need to write something!");     
            return false; 
          }else{
            swal({title: "Processing ...", type: "info", showConfirmButton:false});  
            setTimeout(
              function() {
                window.location.assign('process_cancel_trial.php?id=' + id + '&reason=' + inputValue);
              }
              , 2000
            ); 
          }
        }
    );
}