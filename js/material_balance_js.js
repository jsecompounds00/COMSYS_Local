function showLotProdDate()
{

  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;
  var hidFGID =  document.getElementById("hidFGID").value;
  var hidComputationID =  document.getElementById("hidComputationID").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("tbProductionDetails").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_lot_proddate.php?batch_id="+sltIssuedFormula+"&computation_id="+hidComputationID+"&fg_id="+hidFGID,true);
    xmlhttp.send();
}

function showLotNumber()
{

  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("hidLotNumber").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_lot_number.php?batch_id="+sltIssuedFormula,true);
    xmlhttp.send();
}

function showProductionDate()
{

  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("hidProductionDate").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_production_date.php?batch_id="+sltIssuedFormula,true);
    xmlhttp.send();
}

function showExtruder()
{

  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("hidExtruder").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_extruder.php?batch_id="+sltIssuedFormula,true);
    xmlhttp.send();
}

function showMixer()
{

  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("hidMixer").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_mixer.php?batch_id="+sltIssuedFormula,true);
    xmlhttp.send();
}

function showBigSmallBatch()
{

  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("hidBigSmallBatch").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_big_small_batch.php?batch_id="+sltIssuedFormula,true);
    xmlhttp.send();
}

function showOutput()
{
  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("hidOutput").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_output.php?batch_id="+sltIssuedFormula,true);
    xmlhttp.send();
}

function showTotalInput()
{
  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("tdTotalInput").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_total_input.php?batch_id="+sltIssuedFormula,true);
    xmlhttp.send();
}

function showTotalInput2()
{
  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("tdOverall").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_overall_input.php?batch_id="+sltIssuedFormula,true);
    xmlhttp.send();
}

function showOverallInput(){
  var txtMasterBatch = document.getElementById("txtMasterBatch").value;
  var hidTotalInput = document.getElementById("hidTotalInputFixed").value;
  var txtRecoveryOnprocess = document.getElementById("txtRecoveryOnprocess").value;
  var txtRecoveryLeftOver = document.getElementById("txtRecoveryLeftOver").value;

  if( txtMasterBatch == "" ){
    var txtMasterBatch = 0;
  }
  if( hidTotalInput == "" ){
    var hidTotalInput = 0;
  }
  if( txtRecoveryOnprocess == "" ){
    var txtRecoveryOnprocess = 0;
  }
  if( txtRecoveryLeftOver == "" ){
    var txtRecoveryLeftOver = 0;
  }

  var TotalInput = parseFloat(hidTotalInput) + parseFloat(txtMasterBatch) + parseFloat(txtRecoveryOnprocess) + parseFloat(txtRecoveryLeftOver);

  document.getElementById("tdTotalInput").innerHTML = "<label class='constant_total'>" + TotalInput.toFixed(5) + "</label>";
  document.getElementById("hidTotalInput").value = TotalInput.toFixed(5);

}

function showTotalOutput(){
  var txtMaterialIncorporation = document.getElementById("txtMaterialIncorporation").value;
  var hidTotalOutput = document.getElementById("hidTotalOutput").value;

  if( txtMaterialIncorporation == "" ){
    var txtMaterialIncorporation = 0;
  }
  if( hidTotalOutput == "" ){
    var hidTotalOutput = 0;
  }

  var TotalOutput = parseFloat(hidTotalOutput) - parseFloat(txtMaterialIncorporation);

  document.getElementById("tdTotalOutput").innerHTML = "<label class='constant_total'>" + TotalOutput.toFixed(5) + "</label>";
  document.getElementById("hidGoodOutput").value = TotalOutput.toFixed(5);

}

function showTotalScrap(){
  var txtLabSamples = document.getElementById("txtLabSamples").value;
  var txtUnplanLabSamples = document.getElementById("txtUnplanLabSamples").value;
  var txtScreenChange = document.getElementById("txtScreenChange").value;
  var txtFloorSweepingPellet = document.getElementById("txtFloorSweepingPellet").value;
  var txtChangeGrade = document.getElementById("txtChangeGrade").value;
  var txtFloorSweepingPowder = document.getElementById("txtFloorSweepingPowder").value;
  var txtEndRun = document.getElementById("txtEndRun").value;
  var txtScrapedCooling = document.getElementById("txtScrapedCooling").value;
  var txtScrapedMixer = document.getElementById("txtScrapedMixer").value;
  var txtStartUp = document.getElementById("txtStartUp").value;
  var txtMixedPellets = document.getElementById("txtMixedPellets").value;
  var txtUnplannedScrap = document.getElementById("txtUnplannedScrap").value;

  if( txtLabSamples == "" ){
    var txtLabSamples = 0;
  }
  if( txtUnplanLabSamples == "" ){
    var txtUnplanLabSamples = 0;
  }
  if( txtScreenChange == "" ){
    var txtScreenChange = 0;
  }
  if( txtFloorSweepingPellet == "" ){
    var txtFloorSweepingPellet = 0;
  }
  if( txtChangeGrade == "" ){
    var txtChangeGrade = 0;
  }
  if( txtFloorSweepingPowder == "" ){
    var txtFloorSweepingPowder = 0;
  }
  if( txtEndRun == "" ){
    var txtEndRun = 0;
  }
  if( txtScrapedCooling == "" ){
    var txtScrapedCooling = 0;
  }
  if( txtScrapedMixer == "" ){
    var txtScrapedMixer = 0;
  }
  if( txtStartUp == "" ){
    var txtStartUp = 0;
  }
  if( txtMixedPellets == "" ){
    var txtMixedPellets = 0;
  }
  if( txtUnplannedScrap == "" ){
    var txtUnplannedScrap = 0;
  }

  var TotalScrap =  parseFloat(txtLabSamples) +
                    parseFloat(txtUnplanLabSamples) +
                    parseFloat(txtScreenChange) +
                    parseFloat(txtFloorSweepingPellet) +
                    parseFloat(txtChangeGrade) +
                    parseFloat(txtFloorSweepingPowder) +
                    parseFloat(txtEndRun) +
                    parseFloat(txtScrapedCooling) +
                    parseFloat(txtScrapedMixer) +
                    parseFloat(txtStartUp) +
                    parseFloat(txtMixedPellets) +
                    parseFloat(txtUnplannedScrap);

  document.getElementById("tdTotalScrap").innerHTML = "<label class='constant_total'>" + TotalScrap.toFixed(5) + "</label>";
  document.getElementById("hidTotalScrap").value = TotalScrap.toFixed(5);

}

function showGoodOutputEPM(){
  var FromBagLength = document.getElementsByName("txtFromBagEPM[]").length;
  var txtFromBagEPM = document.getElementsByName("txtFromBagEPM[]");
  var txtThruBagEPM = document.getElementsByName("txtThruBagEPM[]");
  var MaterialIncorporation = document.getElementById("txtMaterialIncorporation").value;

  // alert(FromBagLength);

  if( MaterialIncorporation == "" ){
    var MaterialIncorporation = 0;
  }

  var FromBagEPM = new Array();
  var ThruBagEPM = new Array();

  var GoodBags = 0;
  var TotalBags = 0;

  for( var i = 0; i < FromBagLength; i++ ){
    FromBagEPM[i] = document.getElementById("txtFromBagEPM"+i).value;
    ThruBagEPM[i] = document.getElementById("txtThruBagEPM"+i).value;

    if( FromBagEPM[i] == "" ){
      FromBagEPM[i] = 0;
    }
    if( ThruBagEPM[i] == "" ){
      ThruBagEPM[i] = 0;
    }

    if ( FromBagEPM[i] != 0 || ThruBagEPM[i] ){
      GoodBags = parseFloat(GoodBags) + ( parseFloat(ThruBagEPM[i]) - parseFloat(FromBagEPM[i]) + 1 );
      TotalBags = parseFloat(TotalBags) + ( parseFloat(ThruBagEPM[i]) - parseFloat(FromBagEPM[i]) + 1 );
    }

  }
  
  var GoodOutput = (parseFloat(GoodBags) * 25) - parseFloat(MaterialIncorporation);
  var TotalOutput = (parseFloat(TotalBags) * 25);

  document.getElementById("tdGoodOutputEPM").innerHTML = "<label class='constant_total'>" + GoodOutput.toFixed(5) + "</label>";
  document.getElementById("hidGoodOutputEPM").value = GoodOutput.toFixed(5);
  document.getElementById("tdTotalOutputEPM").innerHTML = "<label class='constant_total'>" + TotalOutput.toFixed(5) + "</label>";

}

function showUnderpackEPM(){
  var UnderpackLength = document.getElementsByName("txtUnderpackEPM[]").length;
  var txtUnderpackEPM = document.getElementsByName("txtUnderpackEPM[]");

  var UnderpackEPM = new Array();

  var GoodUnderpack = 0;
  var TotalUnderpack = 0;

  for( var i = 0; i < UnderpackLength; i++ ){
    UnderpackEPM[i] = document.getElementById("txtUnderpackEPM"+i).value;

    if( UnderpackEPM[i] == "" ){
      UnderpackEPM[i] = 0;
    }

    if ( UnderpackEPM[i] != 0 ){
      GoodUnderpack = parseFloat(GoodUnderpack) + ( parseFloat(UnderpackEPM[i]) );
      TotalUnderpack = parseFloat(TotalUnderpack) + ( parseFloat(UnderpackEPM[i]) );
    }

  }

    document.getElementById("tdGoodUnderpackEPM").innerHTML = "<label class='constant_total'>" + GoodUnderpack.toFixed(5) + "</label>";
    document.getElementById("hidGoodUnderpackEPM").value = GoodUnderpack.toFixed(5);
    document.getElementById("tdTotalUnderpackEPM").innerHTML = "<label class='constant_total'>" + TotalUnderpack.toFixed(5) + "</label>";

}

// function showGoodOutputEPM(){
//   var txtFromBagEPM = document.getElementById("txtFromBagEPM").value;
//   var txtThruBagEPM = document.getElementById("txtThruBagEPM").value;
//   var txtMaterialIncorporation = document.getElementById("txtMaterialIncorporation").value;

//   if( txtFromBagEPM == "" ){
//     var txtFromBagEPM = 0;
//   }
//   if( txtThruBagEPM == "" ){
//     var txtThruBagEPM = 0;
//   }
//   if( txtMaterialIncorporation == "" ){
//     var txtMaterialIncorporation = 0;
//   }

//   var ThruBagEPM = parseFloat(txtThruBagEPM);
//   var FromBagEPM = parseFloat(txtFromBagEPM);
//   var GoodOutput = (( parseFloat(txtThruBagEPM) - parseFloat(txtFromBagEPM) + 1 ) * 25) - parseFloat(txtMaterialIncorporation);
//   var TotalOutput = (( parseFloat(txtThruBagEPM) - parseFloat(txtFromBagEPM) + 1 ) * 25);

//   if ( txtFromBagEPM == "" && txtThruBagEPM == "" ){
//     document.getElementById("tdGoodOutputEPM").innerHTML = "0.00000 ";
//     document.getElementById("hidGoodOutputEPM").value = "0.00000 ";
//     document.getElementById("tdTotalOutputEPM").innerHTML = "0.00000 ";
//   }else if ( txtFromBagEPM == "" ){
//     document.getElementById("tdGoodOutputEPM").innerHTML = "0.00000";
//     document.getElementById("hidGoodOutputEPM").value = "0.00000";
//     document.getElementById("tdTotalOutputEPM").innerHTML = "0.00000 ";
//   }else if ( txtFromBagEPM == 0 || txtFromBagEPM < 1 ){
//     document.getElementById("tdGoodOutputEPM").innerHTML = "0.00000";
//     document.getElementById("hidGoodOutputEPM").value = "0.00000";
//     document.getElementById("tdTotalOutputEPM").innerHTML = "0.00000 ";
//   }else if ( txtThruBagEPM == "" ){
//     document.getElementById("tdGoodOutputEPM").innerHTML = "0.00000";
//     document.getElementById("hidGoodOutputEPM").value = "0.00000";
//     document.getElementById("tdTotalOutputEPM").innerHTML = "0.00000 ";
//   }else if ( txtThruBagEPM == 0 || txtThruBagEPM < 1 ){
//     document.getElementById("tdGoodOutputEPM").innerHTML = "0.00000";
//     document.getElementById("hidGoodOutputEPM").value = "0.00000";
//     document.getElementById("tdTotalOutputEPM").innerHTML = "0.00000 ";
//   }else if ( ThruBagEPM < FromBagEPM ){
//     document.getElementById("tdGoodOutputEPM").innerHTML = "0.00000";
//     document.getElementById("hidGoodOutputEPM").value = "0.00000";
//     document.getElementById("tdTotalOutputEPM").innerHTML = "0.00000 ";
//   }else if ( ThruBagEPM >= FromBagEPM ){
//     document.getElementById("tdGoodOutputEPM").innerHTML = GoodOutput.toFixed(5);
//     document.getElementById("hidGoodOutputEPM").value = GoodOutput.toFixed(5);
//     document.getElementById("tdTotalOutputEPM").innerHTML = TotalOutput.toFixed(5);
//   }

// }

// function showUnderpackEPM(){
//   var txtUnderpackEPM = document.getElementById("txtUnderpackEPM").value;

//   if( txtUnderpackEPM == "" ){
//     var txtUnderpackEPM = 0;
//   }

//   var TotalOutput = parseFloat(txtUnderpackEPM);

//     document.getElementById("tdGoodUnderpackEPM").innerHTML = TotalOutput.toFixed(5);
//     document.getElementById("hidGoodUnderpackEPM").value = TotalOutput.toFixed(5);
//     document.getElementById("tdTotalUnderpackEPM").innerHTML = TotalOutput.toFixed(5);

// }


function showOverallTotalOutput(){

  var hidGoodOutputEPM = document.getElementById("hidGoodOutputEPM").value;
  var hidGoodUnderpackEPM = document.getElementById("hidGoodUnderpackEPM").value;
  var txtLabSamples = document.getElementById("txtLabSamples").value;
  var txtUnplanLabSamples = document.getElementById("txtUnplanLabSamples").value;
  var txtOnholdPellet = document.getElementById("txtOnholdPellet").value;
  var txtScreenChange = document.getElementById("txtScreenChange").value;
  var txtFloorSweepingPellet = document.getElementById("txtFloorSweepingPellet").value;
  var txtOnholdPowder = document.getElementById("txtOnholdPowder").value;
  var txtChangeGrade = document.getElementById("txtChangeGrade").value;
  var txtFloorSweepingPowder = document.getElementById("txtFloorSweepingPowder").value;
  var txtEndRun = document.getElementById("txtEndRun").value;
  var txtScrapedCooling = document.getElementById("txtScrapedCooling").value;
  var txtScrapedMixer = document.getElementById("txtScrapedMixer").value;
  var txtStartUp = document.getElementById("txtStartUp").value;
  var txtMixedPellets = document.getElementById("txtMixedPellets").value;
  var txtUnplannedScrap = document.getElementById("txtUnplannedScrap").value;

  if( hidGoodOutputEPM == "" ){
    var hidGoodOutputEPM = 0;
  }
  if( hidGoodUnderpackEPM == "" ){
    var hidGoodUnderpackEPM = 0;
  }
  if( txtLabSamples == "" ){
    var txtLabSamples = 0;
  }
  if( txtUnplanLabSamples == "" ){
    var txtUnplanLabSamples = 0;
  }
  if( txtOnholdPellet == "" ){
    var txtOnholdPellet = 0;
  }
  if( txtScreenChange == "" ){
    var txtScreenChange = 0;
  }
  if( txtFloorSweepingPellet == "" ){
    var txtFloorSweepingPellet = 0;
  }
  if( txtOnholdPowder == "" ){
    var txtOnholdPowder = 0;
  }
  if( txtChangeGrade == "" ){
    var txtChangeGrade = 0;
  }
  if( txtFloorSweepingPowder == "" ){
    var txtFloorSweepingPowder = 0;
  }
  if( txtEndRun == "" ){
    var txtEndRun = 0;
  }
  if( txtScrapedCooling == "" ){
    var txtScrapedCooling = 0;
  }
  if( txtScrapedMixer == "" ){
    var txtScrapedMixer = 0;
  }
  if( txtStartUp == "" ){
    var txtStartUp = 0;
  }
  if( txtMixedPellets == "" ){
    var txtMixedPellets = 0;
  }
  if( txtUnplannedScrap == "" ){
    var txtUnplannedScrap = 0;
  }

  var TotalScrap =  parseFloat(txtLabSamples) +
                    parseFloat(txtUnplanLabSamples) +
                    parseFloat(txtScreenChange) +
                    parseFloat(txtFloorSweepingPellet) +
                    parseFloat(txtChangeGrade) +
                    parseFloat(txtFloorSweepingPowder) +
                    parseFloat(txtEndRun) +
                    parseFloat(txtScrapedCooling) +
                    parseFloat(txtScrapedMixer) +
                    parseFloat(txtStartUp) +
                    parseFloat(txtMixedPellets) +
                    parseFloat(txtUnplannedScrap);

  var OverallTotalOutput = parseFloat(hidGoodOutputEPM) + parseFloat(hidGoodUnderpackEPM) + parseFloat(TotalScrap) + parseFloat(txtOnholdPellet) + parseFloat(txtOnholdPowder);

  document.getElementById("tdOverallTotalOutput").innerHTML = "<label class='constant_total'>" + OverallTotalOutput.toFixed(5) + "</label>";
  document.getElementById("hidOverallTotalOutput").value = OverallTotalOutput.toFixed(5);

}

function showConsumedTime(index){
  var txtProductionStart = document.getElementById("txtProductionStart"+index).value;
  var txtProductionEnd = document.getElementById("txtProductionEnd"+index).value;
  var ProductionStart = new Date(txtProductionStart);
  var ProductionEnd = new Date(txtProductionEnd);

  if ( txtProductionStart != "" && txtProductionEnd != "" ){
    // document.getElementById("tdTimeConsumed"+index).innerHTML = (ProductionEnd.getTime() - ProductionStart.getTime())/1000/60;
    // document.getElementById("hidTimeConsumed"+index).value = (ProductionEnd.getTime() - ProductionStart.getTime())/1000/60;
    document.getElementById("tdTimeConsumed"+index).innerHTML = "<label class='constant_total'>" + (ProductionEnd.getTime() - ProductionStart.getTime())/1000/60 + "</label>";
    document.getElementById("hidTimeConsumed"+index).value = ((ProductionEnd.getTime() - ProductionStart.getTime())/1000/60);
  }else{
    document.getElementById("tdTimeConsumed"+index).innerHTML = null;
    document.getElementById("hidTimeConsumed"+index).value = null;
  }

}

function showMCRate(){

  var txtPlannedShutdown = document.getElementById("txtPlannedShutdown").value;
  var txtUnplannedShutdown = document.getElementById("txtUnplannedShutdown").value;
  var hidGoodOutputEPM = document.getElementById("hidGoodOutputEPM").value;
  var hidGoodUnderpackEPM = document.getElementById("hidGoodUnderpackEPM").value;

  var TimeConsumedLength = document.getElementsByName("hidTimeConsumed[]").length;
  var TimeConsumed = new Array();
  var TotalTimeConsumed = 0;

  for ( var i = 0; i < TimeConsumedLength; i++ ){
    TimeConsumed[i] = document.getElementById("hidTimeConsumed"+i).value;

    if ( TimeConsumed[i] == "" ){
      TimeConsumed[i] = 0;
    }

    TotalTimeConsumed = parseFloat(TotalTimeConsumed) + parseFloat(TimeConsumed[i]);
  }

  if( txtPlannedShutdown == "" ){
    var txtPlannedShutdown = 0;
  }
  if( txtUnplannedShutdown == "" ){
    var txtUnplannedShutdown = 0;
  }
  if( hidGoodOutputEPM == "" ){
    var hidGoodOutputEPM = 0;
  }
  if( hidGoodUnderpackEPM == "" ){
    var hidGoodUnderpackEPM = 0;
  }

  var Numerator = parseFloat(hidGoodOutputEPM) + parseFloat(hidGoodUnderpackEPM);
  var MCRate = parseFloat(Numerator) / (parseFloat(TotalTimeConsumed) - parseFloat(txtPlannedShutdown) - parseFloat(txtUnplannedShutdown));

  document.getElementById("tdMCRate").innerHTML = "<label class='constant_total'>" + MCRate.toFixed(2) + "</label>";
  document.getElementById("hidMCRate").value = MCRate.toFixed(2);


}

function showMCPerformance(){
  
  var txtLabSamples = document.getElementById("txtLabSamples").value;
  var txtUnplanLabSamples = document.getElementById("txtUnplanLabSamples").value;
  var txtOnholdPellet = document.getElementById("txtOnholdPellet").value;
  var txtFloorSweepingPellet = document.getElementById("txtFloorSweepingPellet").value;
  var txtStartUp = document.getElementById("txtStartUp").value;
  var txtMixedPellets = document.getElementById("txtMixedPellets").value;
  var txtIdealSpeed = document.getElementById("txtIdealSpeed").value;

  var txtPlannedShutdown = document.getElementById("txtPlannedShutdown").value;
  var txtUnplannedShutdown = document.getElementById("txtUnplannedShutdown").value;
  var hidGoodUnderpackEPM = document.getElementById("hidGoodUnderpackEPM").value;
  var hidGoodOutputEPM = document.getElementById("hidGoodOutputEPM").value;

  var TimeConsumedLength = document.getElementsByName("hidTimeConsumed[]").length;
  var TimeConsumed = new Array();
  var TotalTimeConsumed = 0;

  for ( var i = 0; i < TimeConsumedLength; i++ ){
    TimeConsumed[i] = document.getElementById("hidTimeConsumed"+i).value;

    if ( TimeConsumed[i] == "" ){
      TimeConsumed[i] = 0;
    }

    TotalTimeConsumed = parseFloat(TotalTimeConsumed) + parseFloat(TimeConsumed[i]);
  }

  if( txtPlannedShutdown == "" ){
    var txtPlannedShutdown = 0;
  }
  if( txtUnplannedShutdown == "" ){
    var txtUnplannedShutdown = 0;
  }
  if( hidGoodUnderpackEPM == "" ){
    var hidGoodUnderpackEPM = 0;
  }
  if( hidGoodOutputEPM == "" ){
    var hidGoodOutputEPM = 0;
  }
  if( txtLabSamples == "" ){
    var txtLabSamples = 0;
  }
  if( txtUnplanLabSamples == "" ){
    var txtUnplanLabSamples = 0;
  }
  if( txtOnholdPellet == "" ){
    var txtOnholdPellet = 0;
  }
  if( txtFloorSweepingPellet == "" ){
    var txtFloorSweepingPellet = 0;
  }
  if( txtStartUp == "" ){
    var txtStartUp = 0;
  }
  if( txtMixedPellets == "" ){
    var txtMixedPellets = 0;
  }
  if( txtIdealSpeed == "" ){
    var txtIdealSpeed = 1;
  }

  var Numerator = parseFloat(hidGoodOutputEPM) 
                + parseFloat(hidGoodUnderpackEPM) 
                + parseFloat(txtOnholdPellet) 
                + parseFloat(txtLabSamples) 
                + parseFloat(txtUnplanLabSamples) 
                + parseFloat(txtMixedPellets) 
                + parseFloat(txtStartUp) 
                + parseFloat(txtFloorSweepingPellet);

  var Denominator = (parseFloat(TotalTimeConsumed) - parseFloat(txtPlannedShutdown) - parseFloat(txtUnplannedShutdown)) * parseFloat(txtIdealSpeed);

  var MCPerformance = parseFloat(Numerator) / parseFloat(Denominator);

  var MCPerformancePer = parseFloat(MCPerformance) * 100;

  document.getElementById("tdMCPerformance").innerHTML =  "<label class='constant_total'>" + MCPerformancePer.toFixed(2) + "</label>";
  document.getElementById("hidMCPerformance").value =  MCPerformancePer.toFixed(2);

}

function showActReqd(){
  var txtRequiredOutput = document.getElementById("txtRequiredOutput").value;
  var hidGoodOutputEPM = document.getElementById("hidGoodOutputEPM").value;

  if( txtRequiredOutput == "" ){
    var txtRequiredOutput = 0;
  }
  if( hidGoodOutputEPM == "" ){
    var hidGoodOutputEPM = 0;
  }


  var ActReqd = parseFloat(hidGoodOutputEPM) - parseFloat(txtRequiredOutput);

  document.getElementById("tdActReqd").innerHTML = "<label class='constant_total'>" + ActReqd.toFixed(5) + "</label>";
  document.getElementById("hidActReqd").value = ActReqd.toFixed(5);

}

function showTOSAct(){

  var hidGoodOutputEPM = document.getElementById("hidGoodOutputEPM").value;
  var hidGoodUnderpackEPM = document.getElementById("hidGoodUnderpackEPM").value;

  var hidGoodOutput = document.getElementById("hidGoodOutput").value;
  var hidUnderpack = document.getElementById("hidUnderpack").value;

  if( hidGoodOutputEPM == "" ){
    var hidGoodOutputEPM = 0;
  }
  if( hidGoodUnderpackEPM == "" ){
    var hidGoodUnderpackEPM = 0;
  }

  if( hidGoodOutput == "" ){
    var hidGoodOutput = 0;
  }
  if( hidUnderpack == "" ){
    var hidUnderpack = 0;
  }

  var Actual = parseFloat(hidGoodOutputEPM) + parseFloat(hidGoodUnderpackEPM);

  var TOS = parseFloat(hidGoodOutput) + parseFloat(hidUnderpack);

  var TOSAct = parseFloat(TOS) - parseFloat(Actual);

  document.getElementById("tdTOSAct").innerHTML = "<label class='constant_total'>" + TOSAct.toFixed(5) + "</label>";
  document.getElementById("hidTOSAct").value = TOSAct.toFixed(5);

}

function showVarInOut(){

  var hidBigBatch = document.getElementById("hidBigBatch").value;
  var hidSmallBatch = document.getElementById("hidSmallBatch").value;
  var hidRMQtyPerBatchBig = document.getElementById("hidRMQtyPerBatchBig").value;
  var hidRMQtyPerBatchSmall = document.getElementById("hidRMQtyPerBatchSmall").value;

  var txtMasterBatch = document.getElementById("txtMasterBatch").value;
  
  var txtRecoveryOnprocess = document.getElementById("txtRecoveryOnprocess").value;
  var txtRecoveryLeftOver = document.getElementById("txtRecoveryLeftOver").value;

  var txtReturnedBatch = document.getElementById("txtReturnedBatch").value;
  var txtReturnedBatchSmall = document.getElementById("txtReturnedBatchSmall").value;

  var hidOverallTotalOutput = document.getElementById("hidOverallTotalOutput").value;

  if( hidBigBatch == "" ){
    var hidBigBatch = 0;
  }
  if( hidSmallBatch == "" ){
    var hidSmallBatch = 0;
  }
  if( hidRMQtyPerBatchBig == "" ){
    var hidRMQtyPerBatchBig = 0;
  }
  if( hidRMQtyPerBatchSmall == "" ){
    var hidRMQtyPerBatchSmall = 0;
  }

  if( txtMasterBatch == "" ){
    var txtMasterBatch = 0;
  }

  if( txtRecoveryOnprocess == "" ){
    var txtRecoveryOnprocess = 0;
  }
  if( txtRecoveryLeftOver == "" ){
    var txtRecoveryLeftOver = 0;
  }

  if( txtReturnedBatch == "" ){
    var txtReturnedBatch = 0;
  }
  if( txtReturnedBatchSmall == "" ){
    var txtReturnedBatchSmall = 0;
  }

  if( hidOverallTotalOutput == "" ){
    var hidOverallTotalOutput = 0;
  }

  if ( hidBigBatch == "" && hidRMQtyPerBatchBig == "" ){
    var BigBatchTotal = 0;
  }else{

    var BigBatch = parseFloat(hidBigBatch) * parseFloat(hidRMQtyPerBatchBig);

    if ( txtReturnedBatch > 0 && txtReturnedBatch <= hidBigBatch ){
      var BigBatchReturned = parseFloat(txtReturnedBatch) * parseFloat(hidRMQtyPerBatchBig);
    }else{
      var BigBatchReturned = 0;
    }

    var BigBatchTotal = parseFloat(BigBatch) - parseFloat(BigBatchReturned);

  }

  if ( hidSmallBatch == "" && hidRMQtyPerBatchSmall == "" ){
    var SmallBatchTotal = 0;
  }else{

    var SmallBatch = parseFloat(hidSmallBatch) * parseFloat(hidRMQtyPerBatchSmall);

    if ( txtReturnedBatchSmall > 0 && txtReturnedBatchSmall <= hidSmallBatch ){
      var SmallBatchReturned = parseFloat(txtReturnedBatchSmall) * parseFloat(hidRMQtyPerBatchSmall);
    }else{
      var SmallBatchReturned = 0;
    }

    var SmallBatchTotal = parseFloat(SmallBatch) - parseFloat(SmallBatchReturned);

  }

  var Input = (parseFloat(BigBatchTotal) + parseFloat(SmallBatchTotal)) + parseFloat(txtMasterBatch) + parseFloat(txtRecoveryOnprocess) + parseFloat(txtRecoveryLeftOver);

  var VarInOut = parseFloat(hidOverallTotalOutput) - parseFloat(Input);

  document.getElementById("tdVarInOut").innerHTML = "<label class='constant_total'>" + VarInOut.toFixed(5) + "</label>";
  document.getElementById("hidVarInOut").value = VarInOut.toFixed(5);

}

function showMaterialYield(){

  var hidGoodOutputEPM = document.getElementById("hidGoodOutputEPM").value;
  var hidGoodUnderpackEPM = document.getElementById("hidGoodUnderpackEPM").value;

  var hidBigBatch = document.getElementById("hidBigBatch").value;
  var hidSmallBatch = document.getElementById("hidSmallBatch").value;
  var hidRMQtyPerBatchBig = document.getElementById("hidRMQtyPerBatchBig").value;
  var hidRMQtyPerBatchSmall = document.getElementById("hidRMQtyPerBatchSmall").value;

  var txtMasterBatch = document.getElementById("txtMasterBatch").value;
  var txtRecoveryOnprocess = document.getElementById("txtRecoveryOnprocess").value;
  var txtRecoveryLeftOver = document.getElementById("txtRecoveryLeftOver").value;

  var txtReturnedBatch = document.getElementById("txtReturnedBatch").value;
  var txtReturnedBatchSmall = document.getElementById("txtReturnedBatchSmall").value;

  if( hidBigBatch == "" ){
    var hidBigBatch = 0;
  }
  if( hidSmallBatch == "" ){
    var hidSmallBatch = 0;
  }
  if( hidRMQtyPerBatchBig == "" ){
    var hidRMQtyPerBatchBig = 0;
  }
  if( hidRMQtyPerBatchSmall == "" ){
    var hidRMQtyPerBatchSmall = 0;
  }

  if( txtMasterBatch == "" ){
    var txtMasterBatch = 0;
  }

  if( txtRecoveryOnprocess == "" ){
    var txtRecoveryOnprocess = 0;
  }
  if( txtRecoveryLeftOver == "" ){
    var txtRecoveryLeftOver = 0;
  }

  if( txtReturnedBatch == "" ){
    var txtReturnedBatch = 0;
  }
  if( txtReturnedBatchSmall == "" ){
    var txtReturnedBatchSmall = 0;
  }

  if ( hidBigBatch == "" && hidRMQtyPerBatchBig == "" ){
    var BigBatchTotal = 0;
  }else{

    var BigBatch = parseFloat(hidBigBatch) * parseFloat(hidRMQtyPerBatchBig);

    if ( txtReturnedBatch > 0 && txtReturnedBatch <= hidBigBatch ){
      var BigBatchReturned = parseFloat(txtReturnedBatch) * parseFloat(hidRMQtyPerBatchBig);
    }else{
      var BigBatchReturned = 0;
    }

    var BigBatchTotal = parseFloat(BigBatch) - parseFloat(BigBatchReturned);

  }

  if ( hidSmallBatch == "" && hidRMQtyPerBatchSmall == "" ){
    var SmallBatchTotal = 0;
  }else{

    var SmallBatch = parseFloat(hidSmallBatch) * parseFloat(hidRMQtyPerBatchSmall);

    if ( txtReturnedBatchSmall > 0 && txtReturnedBatchSmall <= hidSmallBatch ){
      var SmallBatchReturned = parseFloat(txtReturnedBatchSmall) * parseFloat(hidRMQtyPerBatchSmall);
    }else{
      var SmallBatchReturned = 0;
    }

    var SmallBatchTotal = parseFloat(SmallBatch) - parseFloat(SmallBatchReturned);

  }

  var Input = (parseFloat(BigBatchTotal) + parseFloat(SmallBatchTotal)) + parseFloat(txtMasterBatch) + parseFloat(txtRecoveryOnprocess) + parseFloat(txtRecoveryLeftOver);

  var MaterialYield = ((parseFloat(hidGoodOutputEPM) + parseFloat(hidGoodUnderpackEPM)) / parseFloat(Input)) * 100;

  document.getElementById("tdMaterialYield").innerHTML = "<label class='constant_total'>" + MaterialYield.toFixed(5) + "</label>";
  document.getElementById("hidMaterialYield").value = MaterialYield.toFixed(5);

}

function editLotProdDate(index){
  var Edit = document.getElementById("Edit"+index);
  var txtProductionDateI = document.getElementById("txtProductionDateI"+index);
  var txtLotNumberI = document.getElementById("txtLotNumberI"+index);

  if ( Edit.checked == true ){
    txtProductionDateI.readOnly = false
    txtLotNumberI.readOnly = false
  }else{
    txtProductionDateI.readOnly = true
    txtLotNumberI.readOnly = true
  }
}

function showAddOutput(){
  var LotNumberLength =  document.getElementsByName("txtLotNumberI[]").length;
  var hidFGID =  document.getElementById("hidFGID").value;
  var sltIssuedFormula =  document.getElementById("sltIssuedFormula").value;
  var LotNumbers = new Array();

    // LotNumberLength = ( LotNumberLength == 0 ? 5 : LotNumberLength );


  for ( var i = 0; i < LotNumberLength; i++ ){
    LotNumbers[i] = document.getElementById("txtLotNumberI"+i).value;
  }

  var LotNumbersImplode = LotNumbers.join();
    // alert(LotNumbersImplode);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("hidOutput").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/material_balance_additional_output.php?FGID="+hidFGID+"&LotNumbersImplode="+LotNumbersImplode+"&sltIssuedFormula="+sltIssuedFormula,true);
    xmlhttp.send();
}