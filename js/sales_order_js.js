function enableCloseSOQty(index)
{

    chkCloseSOItem = document.getElementById('chkCloseSOItem'+index);
    txtCloseQuantity = document.getElementById('txtCloseQuantity'+index);
    txtCloseReason = document.getElementById('txtCloseReason'+index);

    if ( chkCloseSOItem.checked == true ){
    	txtCloseQuantity.readOnly = false;
		txtCloseReason.readOnly = false;
    }else{
    	txtCloseQuantity.readOnly = true;
		txtCloseReason.readOnly = true;
    }
  
}