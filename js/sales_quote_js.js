function showFGSalesQuote()
{

  var sltYear =  document.getElementById("sltYear").value;
  var sltMonth =  document.getElementById("sltMonth").value;
  var sltCustomer =  document.getElementById("sltCustomer").value;
  var hidQuoteID =  document.getElementById("hidQuoteID").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("tbQuote").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/sales_quote_quotation_items.php?sltYear="+sltYear+"&sltMonth="+sltMonth+"&sltCustomer="+sltCustomer+"&hidQuoteID="+hidQuoteID,true);
    xmlhttp.send();
}

function showCurrMonthQuote(index)
{

  var hidLastMonthQuote =  document.getElementById("hidLastMonthQuote"+index);
  var txtQuoteDiff =  document.getElementById("txtQuoteDiff"+index);

  if ( hidLastMonthQuote.value !== "" ){
    var LastMonthQuote = hidLastMonthQuote.value;
  }else{
    var LastMonthQuote = 0;
  }

  if ( txtQuoteDiff.value ){
    var QuoteDiff = txtQuoteDiff.value;
  }else{
    var QuoteDiff = 0;
  }

  var CurrMonthQuote = parseFloat(LastMonthQuote) + parseFloat(QuoteDiff);

  if ( QuoteDiff != 0 ){
    document.getElementById("hidCurrMonthQuote"+index).value = CurrMonthQuote.toFixed(4);
    document.getElementById("tdCurrMonthQuote"+index).innerHTML = CurrMonthQuote.toFixed(4);
  }else{
    document.getElementById("hidCurrMonthQuote"+index).value = null;
    document.getElementById("tdCurrMonthQuote"+index).innerHTML = null;
  }
  // alert(LastMonthQuote);
  
}

function showCurrMonthQuoteRevised(index)
{

  var hidLastMonthQuote =  document.getElementById("hidLastMonthQuote"+index);
  var txtQuoteDiff =  document.getElementById("txtQuoteDiff"+index);
  var txtQuoteDiffRevised =  document.getElementById("txtQuoteDiffRevised"+index);

  if ( hidLastMonthQuote.value !== "" ){
    var LastMonthQuote = hidLastMonthQuote.value;
  }else{
    var LastMonthQuote = 0;
  }
  if ( txtQuoteDiff.value !== "" ){
    var QuoteDiff = txtQuoteDiff.value;
  }else{
    var QuoteDiff = 0;
  }

  if ( txtQuoteDiffRevised.value ){
    var QuoteDiffRevised = txtQuoteDiffRevised.value;
  }else{
    var QuoteDiffRevised = 0;
  }

  var CurrMonthQuoteRevised = ( parseFloat(LastMonthQuote) + parseFloat(QuoteDiff) ) - parseFloat(QuoteDiffRevised);

  // if ( QuoteDiff == 0 ){
  //   document.getElementById("tdCurrMonthQuoteRevised"+index).innerHTML = "* Accomplish quote difference first.";
  // }else 
  if ( QuoteDiffRevised != 0 ){
    document.getElementById("hidCurrMonthQuoteRevised"+index).value = CurrMonthQuoteRevised.toFixed(4);
    document.getElementById("tdCurrMonthQuoteRevised"+index).innerHTML = CurrMonthQuoteRevised.toFixed(4);
  }else{
    document.getElementById("hidCurrMonthQuoteRevised"+index).value = null;
    document.getElementById("tdCurrMonthQuoteRevised"+index).innerHTML = null;
  }

  // alert(LastMonthQuote);
  
}