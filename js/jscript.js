!function(e,t,n){"use strict";!function o(e,t,n){function a(s,l){if(!t[s]){if(!e[s]){var i="function"==typeof require&&require;if(!l&&i)return i(s,!0);if(r)return r(s,!0);var u=new Error("Cannot find module '"+s+"'");throw u.code="MODULE_NOT_FOUND",u}var c=t[s]={exports:{}};e[s][0].call(c.exports,function(t){var n=e[s][1][t];return a(n?n:t)},c,c.exports,o,e,t,n)}return t[s].exports}for(var r="function"==typeof require&&require,s=0;s<n.length;s++)a(n[s]);return a}({1:[function(o){var a,r,s,l,i=function(e){return e&&e.__esModule?e:{"default":e}},u=o("./modules/handle-dom"),c=o("./modules/utils"),d=o("./modules/handle-swal-dom"),f=o("./modules/handle-click"),p=o("./modules/handle-key"),m=i(p),v=o("./modules/default-params"),y=i(v),h=o("./modules/set-params"),g=i(h);s=l=function(){function o(e){var t=s;return t[e]===n?y["default"][e]:t[e]}var s=arguments[0];if(u.addClass(t.body,"stop-scrolling"),d.resetInput(),s===n)return c.logStr("SweetAlert expects at least 1 attribute!"),!1;var i=c.extend({},y["default"]);switch(typeof s){case"string":i.title=s,i.text=arguments[1]||"",i.type=arguments[2]||"";break;case"object":if(s.title===n)return c.logStr('Missing "title" argument!'),!1;i.title=s.title;for(var p in y["default"])i[p]=o(p);i.confirmButtonText=i.showCancelButton?"Confirm":y["default"].confirmButtonText,i.confirmButtonText=o("confirmButtonText"),i.doneFunction=arguments[1]||null;break;default:return c.logStr('Unexpected type of argument! Expected "string" or "object", got '+typeof s),!1}g["default"](i),d.fixVerticalPosition(),d.openModal(arguments[1]);for(var v=d.getModal(),h=v.querySelectorAll("button"),b=["onclick","onmouseover","onmouseout","onmousedown","onmouseup","onfocus"],w=function(e){return f.handleButton(e,i,v)},C=0;C<h.length;C++)for(var S=0;S<b.length;S++){var x=b[S];h[C][x]=w}d.getOverlay().onclick=w,a=e.onkeydown;var k=function(e){return m["default"](e,i,v)};e.onkeydown=k,e.onfocus=function(){setTimeout(function(){r!==n&&(r.focus(),r=n)},0)},l.enableButtons()},s.setDefaults=l.setDefaults=function(e){if(!e)throw new Error("userParams is required");if("object"!=typeof e)throw new Error("userParams has to be a object");c.extend(y["default"],e)},s.close=l.close=function(){var o=d.getModal();u.fadeOut(d.getOverlay(),5),u.fadeOut(o,5),u.removeClass(o,"showSweetAlert"),u.addClass(o,"hideSweetAlert"),u.removeClass(o,"visible");var s=o.querySelector(".sa-icon.sa-success");u.removeClass(s,"animate"),u.removeClass(s.querySelector(".sa-tip"),"animateSuccessTip"),u.removeClass(s.querySelector(".sa-long"),"animateSuccessLong");var l=o.querySelector(".sa-icon.sa-error");u.removeClass(l,"animateErrorIcon"),u.removeClass(l.querySelector(".sa-x-mark"),"animateXMark");var i=o.querySelector(".sa-icon.sa-warning");return u.removeClass(i,"pulseWarning"),u.removeClass(i.querySelector(".sa-body"),"pulseWarningIns"),u.removeClass(i.querySelector(".sa-dot"),"pulseWarningIns"),setTimeout(function(){var e=o.getAttribute("data-custom-class");u.removeClass(o,e)},300),u.removeClass(t.body,"stop-scrolling"),e.onkeydown=a,e.previousActiveElement&&e.previousActiveElement.focus(),r=n,clearTimeout(o.timeout),!0},s.showInputError=l.showInputError=function(e){var t=d.getModal(),n=t.querySelector(".sa-input-error");u.addClass(n,"show");var o=t.querySelector(".sa-error-container");u.addClass(o,"show"),o.querySelector("p").innerHTML=e,setTimeout(function(){s.enableButtons()},1),t.querySelector("input").focus()},s.resetInputError=l.resetInputError=function(e){if(e&&13===e.keyCode)return!1;var t=d.getModal(),n=t.querySelector(".sa-input-error");u.removeClass(n,"show");var o=t.querySelector(".sa-error-container");u.removeClass(o,"show")},s.disableButtons=l.disableButtons=function(){var e=d.getModal(),t=e.querySelector("button.confirm"),n=e.querySelector("button.cancel");t.disabled=!0,n.disabled=!0},s.enableButtons=l.enableButtons=function(){var e=d.getModal(),t=e.querySelector("button.confirm"),n=e.querySelector("button.cancel");t.disabled=!1,n.disabled=!1},"undefined"!=typeof e?e.sweetAlert=e.swal=s:c.logStr("SweetAlert is a frontend module!")},{"./modules/default-params":2,"./modules/handle-click":3,"./modules/handle-dom":4,"./modules/handle-key":5,"./modules/handle-swal-dom":6,"./modules/set-params":8,"./modules/utils":9}],2:[function(e,t,n){Object.defineProperty(n,"__esModule",{value:!0});var o={title:"",text:"",type:null,allowOutsideClick:!1,showConfirmButton:!0,showCancelButton:!1,closeOnConfirm:!0,closeOnCancel:!0,confirmButtonText:"OK",confirmButtonColor:"#8CD4F5",cancelButtonText:"Cancel",imageUrl:null,imageSize:null,timer:null,customClass:"",html:!1,animation:!0,allowEscapeKey:!0,inputType:"text",inputPlaceholder:"",inputValue:"",showLoaderOnConfirm:!1};n["default"]=o,t.exports=n["default"]},{}],3:[function(t,n,o){Object.defineProperty(o,"__esModule",{value:!0});var a=t("./utils"),r=(t("./handle-swal-dom"),t("./handle-dom")),s=function(t,n,o){function s(e){m&&n.confirmButtonColor&&(p.style.backgroundColor=e)}var u,c,d,f=t||e.event,p=f.target||f.srcElement,m=-1!==p.className.indexOf("confirm"),v=-1!==p.className.indexOf("sweet-overlay"),y=r.hasClass(o,"visible"),h=n.doneFunction&&"true"===o.getAttribute("data-has-done-function");switch(m&&n.confirmButtonColor&&(u=n.confirmButtonColor,c=a.colorLuminance(u,-.04),d=a.colorLuminance(u,-.14)),f.type){case"mouseover":s(c);break;case"mouseout":s(u);break;case"mousedown":s(d);break;case"mouseup":s(c);break;case"focus":var g=o.querySelector("button.confirm"),b=o.querySelector("button.cancel");m?b.style.boxShadow="none":g.style.boxShadow="none";break;case"click":var w=o===p,C=r.isDescendant(o,p);if(!w&&!C&&y&&!n.allowOutsideClick)break;m&&h&&y?l(o,n):h&&y||v?i(o,n):r.isDescendant(o,p)&&"BUTTON"===p.tagName&&sweetAlert.close()}},l=function(e,t){var n=!0;r.hasClass(e,"show-input")&&(n=e.querySelector("input").value,n||(n="")),t.doneFunction(n),t.closeOnConfirm&&sweetAlert.close(),t.showLoaderOnConfirm&&sweetAlert.disableButtons()},i=function(e,t){var n=String(t.doneFunction).replace(/\s/g,""),o="function("===n.substring(0,9)&&")"!==n.substring(9,10);o&&t.doneFunction(!1),t.closeOnCancel&&sweetAlert.close()};o["default"]={handleButton:s,handleConfirm:l,handleCancel:i},n.exports=o["default"]},{"./handle-dom":4,"./handle-swal-dom":6,"./utils":9}],4:[function(n,o,a){Object.defineProperty(a,"__esModule",{value:!0});var r=function(e,t){return new RegExp(" "+t+" ").test(" "+e.className+" ")},s=function(e,t){r(e,t)||(e.className+=" "+t)},l=function(e,t){var n=" "+e.className.replace(/[\t\r\n]/g," ")+" ";if(r(e,t)){for(;n.indexOf(" "+t+" ")>=0;)n=n.replace(" "+t+" "," ");e.className=n.replace(/^\s+|\s+$/g,"")}},i=function(e){var n=t.createElement("div");return n.appendChild(t.createTextNode(e)),n.innerHTML},u=function(e){e.style.opacity="",e.style.display="block"},c=function(e){if(e&&!e.length)return u(e);for(var t=0;t<e.length;++t)u(e[t])},d=function(e){e.style.opacity="",e.style.display="none"},f=function(e){if(e&&!e.length)return d(e);for(var t=0;t<e.length;++t)d(e[t])},p=function(e,t){for(var n=t.parentNode;null!==n;){if(n===e)return!0;n=n.parentNode}return!1},m=function(e){e.style.left="-9999px",e.style.display="block";var t,n=e.clientHeight;return t="undefined"!=typeof getComputedStyle?parseInt(getComputedStyle(e).getPropertyValue("padding-top"),10):parseInt(e.currentStyle.padding),e.style.left="",e.style.display="none","-"+parseInt((n+t)/2)+"px"},v=function(e,t){if(+e.style.opacity<1){t=t||16,e.style.opacity=0,e.style.display="block";var n=+new Date,o=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){e.style.opacity=+e.style.opacity+(new Date-n)/100,n=+new Date,+e.style.opacity<1&&setTimeout(o,t)});o()}e.style.display="block"},y=function(e,t){t=t||16,e.style.opacity=1;var n=+new Date,o=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){e.style.opacity=+e.style.opacity-(new Date-n)/100,n=+new Date,+e.style.opacity>0?setTimeout(o,t):e.style.display="none"});o()},h=function(n){if("function"==typeof MouseEvent){var o=new MouseEvent("click",{view:e,bubbles:!1,cancelable:!0});n.dispatchEvent(o)}else if(t.createEvent){var a=t.createEvent("MouseEvents");a.initEvent("click",!1,!1),n.dispatchEvent(a)}else t.createEventObject?n.fireEvent("onclick"):"function"==typeof n.onclick&&n.onclick()},g=function(t){"function"==typeof t.stopPropagation?(t.stopPropagation(),t.preventDefault()):e.event&&e.event.hasOwnProperty("cancelBubble")&&(e.event.cancelBubble=!0)};a.hasClass=r,a.addClass=s,a.removeClass=l,a.escapeHtml=i,a._show=u,a.show=c,a._hide=d,a.hide=f,a.isDescendant=p,a.getTopMargin=m,a.fadeIn=v,a.fadeOut=y,a.fireClick=h,a.stopEventPropagation=g},{}],5:[function(t,o,a){Object.defineProperty(a,"__esModule",{value:!0});var r=t("./handle-dom"),s=t("./handle-swal-dom"),l=function(t,o,a){var l=t||e.event,i=l.keyCode||l.which,u=a.querySelector("button.confirm"),c=a.querySelector("button.cancel"),d=a.querySelectorAll("button[tabindex]");if(-1!==[9,13,32,27].indexOf(i)){for(var f=l.target||l.srcElement,p=-1,m=0;m<d.length;m++)if(f===d[m]){p=m;break}9===i?(f=-1===p?u:p===d.length-1?d[0]:d[p+1],r.stopEventPropagation(l),f.focus(),o.confirmButtonColor&&s.setFocusStyle(f,o.confirmButtonColor)):13===i?("INPUT"===f.tagName&&(f=u,u.focus()),f=-1===p?u:n):27===i&&o.allowEscapeKey===!0?(f=c,r.fireClick(f,l)):f=n}};a["default"]=l,o.exports=a["default"]},{"./handle-dom":4,"./handle-swal-dom":6}],6:[function(n,o,a){var r=function(e){return e&&e.__esModule?e:{"default":e}};Object.defineProperty(a,"__esModule",{value:!0});var s=n("./utils"),l=n("./handle-dom"),i=n("./default-params"),u=r(i),c=n("./injected-html"),d=r(c),f=".sweet-alert",p=".sweet-overlay",m=function(){var e=t.createElement("div");for(e.innerHTML=d["default"];e.firstChild;)t.body.appendChild(e.firstChild)},v=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){var e=t.querySelector(f);return e||(m(),e=v()),e}),y=function(){var e=v();return e?e.querySelector("input"):void 0},h=function(){return t.querySelector(p)},g=function(e,t){var n=s.hexToRgb(t);e.style.boxShadow="0 0 2px rgba("+n+", 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)"},b=function(n){var o=v();l.fadeIn(h(),10),l.show(o),l.addClass(o,"showSweetAlert"),l.removeClass(o,"hideSweetAlert"),e.previousActiveElement=t.activeElement;var a=o.querySelector("button.confirm");a.focus(),setTimeout(function(){l.addClass(o,"visible")},500);var r=o.getAttribute("data-timer");if("null"!==r&&""!==r){var s=n;o.timeout=setTimeout(function(){var e=(s||null)&&"true"===o.getAttribute("data-has-done-function");e?s(null):sweetAlert.close()},r)}},w=function(){var e=v(),t=y();l.removeClass(e,"show-input"),t.value=u["default"].inputValue,t.setAttribute("type",u["default"].inputType),t.setAttribute("placeholder",u["default"].inputPlaceholder),C()},C=function(e){if(e&&13===e.keyCode)return!1;var t=v(),n=t.querySelector(".sa-input-error");l.removeClass(n,"show");var o=t.querySelector(".sa-error-container");l.removeClass(o,"show")},S=function(){var e=v();e.style.marginTop=l.getTopMargin(v())};a.sweetAlertInitialize=m,a.getModal=v,a.getOverlay=h,a.getInput=y,a.setFocusStyle=g,a.openModal=b,a.resetInput=w,a.resetInputError=C,a.fixVerticalPosition=S},{"./default-params":2,"./handle-dom":4,"./injected-html":7,"./utils":9}],7:[function(e,t,n){Object.defineProperty(n,"__esModule",{value:!0});var o='<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert"><div class="sa-icon sa-error">\n      <span class="sa-x-mark">\n        <span class="sa-line sa-left"></span>\n        <span class="sa-line sa-right"></span>\n      </span>\n    </div><div class="sa-icon sa-warning">\n      <span class="sa-body"></span>\n      <span class="sa-dot"></span>\n    </div><div class="sa-icon sa-info"></div><div class="sa-icon sa-success">\n      <span class="sa-line sa-tip"></span>\n      <span class="sa-line sa-long"></span>\n\n      <div class="sa-placeholder"></div>\n      <div class="sa-fix"></div>\n    </div><div class="sa-icon sa-custom"></div><h2>Title</h2>\n    <p>Text</p>\n    <fieldset>\n      <input type="text" tabIndex="3" />\n      <div class="sa-input-error"></div>\n    </fieldset><div class="sa-error-container">\n      <div class="icon">!</div>\n      <p>Not valid!</p>\n    </div><div class="sa-button-container">\n      <button class="cancel" tabIndex="2">Cancel</button>\n      <div class="sa-confirm-button-container">\n        <button class="confirm" tabIndex="1">OK</button><div class="la-ball-fall">\n          <div></div>\n          <div></div>\n          <div></div>\n        </div>\n      </div>\n    </div></div>';n["default"]=o,t.exports=n["default"]},{}],8:[function(e,t,o){Object.defineProperty(o,"__esModule",{value:!0});var a=e("./utils"),r=e("./handle-swal-dom"),s=e("./handle-dom"),l=["error","warning","info","success","input","prompt"],i=function(e){var t=r.getModal(),o=t.querySelector("h2"),i=t.querySelector("p"),u=t.querySelector("button.cancel"),c=t.querySelector("button.confirm");if(o.innerHTML=e.html?e.title:s.escapeHtml(e.title).split("\n").join("<br>"),i.innerHTML=e.html?e.text:s.escapeHtml(e.text||"").split("\n").join("<br>"),e.text&&s.show(i),e.customClass)s.addClass(t,e.customClass),t.setAttribute("data-custom-class",e.customClass);else{var d=t.getAttribute("data-custom-class");s.removeClass(t,d),t.setAttribute("data-custom-class","")}if(s.hide(t.querySelectorAll(".sa-icon")),e.type&&!a.isIE8()){var f=function(){for(var o=!1,a=0;a<l.length;a++)if(e.type===l[a]){o=!0;break}if(!o)return logStr("Unknown alert type: "+e.type),{v:!1};var i=["success","error","warning","info"],u=n;-1!==i.indexOf(e.type)&&(u=t.querySelector(".sa-icon.sa-"+e.type),s.show(u));var c=r.getInput();switch(e.type){case"success":s.addClass(u,"animate"),s.addClass(u.querySelector(".sa-tip"),"animateSuccessTip"),s.addClass(u.querySelector(".sa-long"),"animateSuccessLong");break;case"error":s.addClass(u,"animateErrorIcon"),s.addClass(u.querySelector(".sa-x-mark"),"animateXMark");break;case"warning":s.addClass(u,"pulseWarning"),s.addClass(u.querySelector(".sa-body"),"pulseWarningIns"),s.addClass(u.querySelector(".sa-dot"),"pulseWarningIns");break;case"input":case"prompt":c.setAttribute("type",e.inputType),c.value=e.inputValue,c.setAttribute("placeholder",e.inputPlaceholder),s.addClass(t,"show-input"),setTimeout(function(){c.focus(),c.addEventListener("keyup",swal.resetInputError)},400)}}();if("object"==typeof f)return f.v}if(e.imageUrl){var p=t.querySelector(".sa-icon.sa-custom");p.style.backgroundImage="url("+e.imageUrl+")",s.show(p);var m=80,v=80;if(e.imageSize){var y=e.imageSize.toString().split("x"),h=y[0],g=y[1];h&&g?(m=h,v=g):logStr("Parameter imageSize expects value with format WIDTHxHEIGHT, got "+e.imageSize)}p.setAttribute("style",p.getAttribute("style")+"width:"+m+"px; height:"+v+"px")}t.setAttribute("data-has-cancel-button",e.showCancelButton),e.showCancelButton?u.style.display="inline-block":s.hide(u),t.setAttribute("data-has-confirm-button",e.showConfirmButton),e.showConfirmButton?c.style.display="inline-block":s.hide(c),e.cancelButtonText&&(u.innerHTML=s.escapeHtml(e.cancelButtonText)),e.confirmButtonText&&(c.innerHTML=s.escapeHtml(e.confirmButtonText)),e.confirmButtonColor&&(c.style.backgroundColor=e.confirmButtonColor,c.style.borderLeftColor=e.confirmLoadingButtonColor,c.style.borderRightColor=e.confirmLoadingButtonColor,r.setFocusStyle(c,e.confirmButtonColor)),t.setAttribute("data-allow-outside-click",e.allowOutsideClick);var b=e.doneFunction?!0:!1;t.setAttribute("data-has-done-function",b),e.animation?"string"==typeof e.animation?t.setAttribute("data-animation",e.animation):t.setAttribute("data-animation","pop"):t.setAttribute("data-animation","none"),t.setAttribute("data-timer",e.timer)};o["default"]=i,t.exports=o["default"]},{"./handle-dom":4,"./handle-swal-dom":6,"./utils":9}],9:[function(t,n,o){Object.defineProperty(o,"__esModule",{value:!0});var a=function(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e},r=function(e){var t=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);return t?parseInt(t[1],16)+", "+parseInt(t[2],16)+", "+parseInt(t[3],16):null},s=function(){return e.attachEvent&&!e.addEventListener},l=function(t){e.console&&e.console.log("SweetAlert: "+t)},i=function(e,t){e=String(e).replace(/[^0-9a-f]/gi,""),e.length<6&&(e=e[0]+e[0]+e[1]+e[1]+e[2]+e[2]),t=t||0;var n,o,a="#";for(o=0;3>o;o++)n=parseInt(e.substr(2*o,2),16),n=Math.round(Math.min(Math.max(0,n+n*t),255)).toString(16),a+=("00"+n).substr(n.length);return a};o.extend=a,o.hexToRgb=r,o.isIE8=s,o.logStr=l,o.colorLuminance=i},{}]},{},[1]),"function"==typeof define&&define.amd?define(function(){return sweetAlert}):"undefined"!=typeof module&&module.exports&&(module.exports=sweetAlert)}(window,document);
function defaultValue(){
  document.getElementById("txtOther").readOnly=true;
  document.getElementById("txtCapexNo").readOnly=true;
  document.getElementById("txtCapexName").readOnly=true;
  document.getElementById("txtOtherDiv").readOnly=true;
}

function changetextbox()
{
    if (document.getElementById("chkCapex").checked == true) {
        document.getElementById("txtCapexNo").readOnly=false;
        document.getElementById("txtCapexName").readOnly=false;
    } else {
        document.getElementById("txtCapexNo").readOnly=true;
        document.getElementById("txtCapexName").readOnly=true;
        document.getElementById("txtCapexNo").value='';
        document.getElementById("txtCapexName").value='';
    }
    
    if (document.getElementById("chkOther").checked == true) {
        document.getElementById("txtOther").readOnly=false;
    } else {
        document.getElementById("txtOther").readOnly=true;
        document.getElementById("txtOther").value='';
    }
}

function textOther(){
  var r = document.getElementsByName("radDivision");
  var length = r.length;

  for (var i = 0; i < length; i++) {
      if (r[i].checked) {
        if (r[i].value == 'Others') {
          document.getElementById("txtOtherDiv").readOnly=false;
        } else {
          document.getElementById("txtOtherDiv").readOnly=true;
          document.getElementById("txtOtherDiv").value='';
        }
        // alert(r[i].value);
      }
  }
}

function showSupplies(str, supply)
{
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("supplyDD1").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD2").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD3").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD4").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD5").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD6").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD7").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD8").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD9").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD10").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/supply_dropdown.php?id="+str+"&supply="+supply,true);
    xmlhttp.send();
}
function showPREItem()
{
    var str = document.getElementById('sltSupplyType').value;

    var radDivision = document.getElementsByName("radDivision");
    var countDivision = radDivision.length;

    for ( var i = 0; i < countDivision; i++){
      if ( radDivision[i].checked ){
        if ( radDivision[i].value == 'Compounds' ){
          var Division = 'Compounds';
        }else if ( radDivision[i].value == 'Pipes' ){
          var Division = 'Pipes';
        }else if ( radDivision[i].value == 'Corporate' ){
          var Division = 'Corporate';
        }else if ( radDivision[i].value == 'PPR' ){
          var Division = 'PPR';
        }else if ( radDivision[i].value == 'Others' ){
          var Division = 'Others';
        }
      }
    }
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("supplyDD1").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD2").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD3").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD4").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD5").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD6").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD7").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD8").innerHTML=xmlhttp.responseText;
        }
      }//+"&supply="+supply
    // alert(Division);
    xmlhttp.open("GET","include/pre_item_dropdown.php?id="+str+"&Division="+Division,true);
    xmlhttp.send();
}

function showPONum(id)
{
    var s = document.getElementById('s');
    var r = document.getElementById('r');
    var hidPREIDs = document.getElementById('hidPREIDs').value;

    // alert(hidPREIDs);

    var radDivision = document.getElementsByName("radDivision");
    var countDivision = radDivision.length;

    for ( var i = 0; i < countDivision; i++){
      if ( radDivision[i].checked ){
        if ( radDivision[i].value == 'Compounds' ){
          var Division = 'Compounds';
        }else if ( radDivision[i].value == 'Pipes' ){
          var Division = 'Pipes';
        }else if ( radDivision[i].value == 'Corporate' ){
          var Division = 'Corporate';
        }else if ( radDivision[i].value == 'PPR' ){
          var Division = 'PPR';
        }else if ( radDivision[i].value == 'Others' ){
          var Division = 'Others';
        }
      }
    }

   if ( s.checked == true )
    {
      var item_type = s.value;
    }
    else if ( r.checked == true )
    {
      var item_type = r.value;
    }

    // alert(item_type);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltPRE").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/po_number_dropdown.php?item_type=" + item_type + "&poId=" + id + "&Division=" + Division + "&hidPREIDs=" + hidPREIDs,true);
    xmlhttp.send();

}

function sethidPREIDs(){
  document.getElementById("hidPREIDs").value = null;
}

function showPOItem(id)
{
    var hidPREIDs = document.getElementById('hidPREIDs').value;
    var preIds = document.getElementById('sltPRE');
    var s = document.getElementById('s');
    var r = document.getElementById('r');
    var r = document.getElementById('r');

    var radDivision = document.getElementsByName("radDivision");
    var countDivision = radDivision.length;

    for ( var i = 0; i < countDivision; i++){
      if ( radDivision[i].checked ){
        if ( radDivision[i].value == 'Compounds' ){
          var Division = 'Compounds';
        }else if ( radDivision[i].value == 'Pipes' ){
          var Division = 'Pipes';
        }else if ( radDivision[i].value == 'Corporate' ){
          var Division = 'Corporate';
        }else if ( radDivision[i].value == 'PPR' ){
          var Division = 'PPR';
        }
      }
    }

    if ( s.checked == true )
    {
      var item_type = s.value;
    }
    else if ( r.checked == true )
    {
      var item_type = r.value;
    }

    var selectPRE = new Array();
    var i;
    var count = 0;
    for (i=0; i<preIds.options.length; i++) {
      if (preIds.options[i].selected) {
        selectPRE[count] = preIds.options[i].value;
        count++;
      }
    }
      // var implodedPreIds = selectPRE.join(',');

      if ( hidPREIDs == null || hidPREIDs == "" ) {
        var implodedPreIds = selectPRE.join(',');
      }else{
        var implodedPreIds = hidPREIDs;
      }

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("preItems").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/po_item_dropdown.php?implodedPreIds=" + implodedPreIds + "&poId=" + id + "&item_type=" + item_type + "&Division=" + Division,true);
    xmlhttp.send();

}

function showDept(str, dept)
{
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltIssTo").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/dept_dropdown.php?deptid="+str+"&dept="+dept,true);
    xmlhttp.send();
}
function showUser(str, user) // ################ add division
{
    // var radDivision = document.getElementById("radDivision").value;
    // var Division = radDivision.toLowerCase();
    var r = document.getElementsByName("radDivision");
    var length = r.length;

    for (var i = 0; i < length; i++) {
      if (r[i].checked) {
        if ( r[i].value == 'Compounds' ){
          var Division = 'compounds';
        }else if ( r[i].value == 'Pipes' ){
          var Division = 'pipes';
        }else if ( r[i].value == 'Corporate' ){
          var Division = 'corporate';
        }else if ( r[i].value == 'PPR' ){
          var Division = 'ppr';
        }else if ( r[i].value == 'Others' ){
          var Division = 'Others';
        }
      }
    }
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltIssToUser").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/user_dropdown.php?userid="+str+"&user="+user+"&Division="+Division,true);
    xmlhttp.send();
}

function showFormulaType(str, fg_type, bat_id)
{
  // var sltActivity = document.getElementById('sltActivity').value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltFormulaType").innerHTML=xmlhttp.responseText;
        }
      }
    // xmlhttp.open("GET","include/formula_type_dropdown.php?fg_id="+str+"&fg_type="+fg_type+"&sltActivity="+sltActivity+"&bat_id="+bat_id,true);
    xmlhttp.open("GET","include/formula_type_dropdown.php?fg_id="+str+"&fg_type="+fg_type+"&bat_id="+bat_id,true);
    xmlhttp.send();
}

function showFormula(str, trial)
{
  var NRM = document.getElementById('NRM');
  var JRD = document.getElementById('JRD');
  var sltActivity = document.getElementById('sltActivity').value;
// alert(sltActivity);
  if ( NRM.checked == true ){
    NRM = 1;
  }else{
    NRM = 2;
  }

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("standard_formula").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/standard_formula.php?formula_type="+str+"&trial="+trial+"&sltActivity="+sltActivity,true);
    xmlhttp.send();
}

function autoMultiply(){
  var multiplier = document.getElementById("txtMultiplier");
  var phr = document.getElementById("txtNPHR");

  if ( document.getElementById('txtNPHR').disabled == true ){
    var result = 0 * phr.value;
  }else{
    var result = multiplier.value * phr.value;
  }
  
  if(!isNaN(result)){
      document.getElementById('txtNQuantity').value = result.toFixed(4);
  }//end of 1st if

  var i
  var phr_count = document.getElementsByName('txtPhr[]').length;
  var results = new Array();
  var phrs = new Array();
  var qty = new Array();

  for (i = 0; i < phr_count; i++) {
    phrs[i] = document.getElementById('txtPhr'+i).value;

    if ( document.getElementById('txtPhr'+i).disabled == true ){
      results[i] = 0 * parseFloat(phrs[i]);
    }else{
      results[i] = parseFloat(multiplier.value) * parseFloat(phrs[i]);
    }

    if(!isNaN(results[i])){
        document.getElementById('txtQty'+i).value = results[i].toFixed(4);
    }//end of 2nd if

  }//end of for loop

}//end of functin autoMultiply()

function autoSum(){
  var qty_count = document.getElementsByName('txtQty[]').length;
  var nqty = document.getElementById('txtNQuantity').value;
  var qty = new Array();
  var total = 0;

  for (i = 0; i < qty_count; i++) {
    qty[i] = document.getElementById('txtQty'+i).value;
    total = parseFloat(total) + parseFloat(qty[i]);
  }//end of for loop

  total = parseFloat(total) + parseFloat(nqty);
  total = parseFloat(total);

  if(!isNaN(total)){
      document.getElementById('txtTotalQty').value = total.toFixed(4);
  }//end of if

}//end of functin autoSum()

function exclude(){
  var chk_count = document.getElementsByName('chkExclude[]').length;
  var chk_val = new Array();
  var phr_val = new Array();
  var total = document.getElementById('txtTotalQty').value;
  var sltActivity = document.getElementById('sltActivity').value;

  for (i = 0; i < chk_count; i++) {
    chk_val[i] = document.getElementById('chkExclude'+i);
    phr_val[i] = document.getElementById('hidPhr'+i).value;

    if ( chk_val[i].checked == true ){
      if ( sltActivity != 5 ){
        document.getElementById('txtRM'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtPhr'+i).value = 0.0000;
      }
      else{
        // document.getElementById('txtRM'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtPhr'+i).readOnly = true;
        document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtQty'+i).readOnly = true;
        document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtPhr'+i).value = 0.0000;
      }
    }//end of if
    else{
      if ( sltActivity != 5 ){
        document.getElementById('txtRM'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtPhr'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtQty'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtPhr'+i).value = 0;
        document.getElementById('txtPhr'+i).value = phr_val[i];
      }else{
        // document.getElementById('txtRM'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtPhr'+i).readOnly = false;
        document.getElementById('txtPhr'+i).style.cssText = 'background: #ffffff;';
        document.getElementById('txtQty'+i).readOnly = true;
        document.getElementById('txtQty'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtPhr'+i).value = 0;
        document.getElementById('txtPhr'+i).value = phr_val[i];
      }
    }//end of else

  }//end of for loop

}//end of functin exclude()

function showCode(str)
{
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("code").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/code.php?code="+str,true);
    xmlhttp.send();
}

function changeCheckBox()
{
  var chkExclude = document.getElementsByName("chkExclude[]");
  var clength = chkExclude.length;
  var PreQual = document.getElementById("PreQual");
  var SamplePrep = document.getElementById("SamplePrep");

  if ( PreQual.checked == true ){
     for (var i = 0; i < clength; i++) {
      document.getElementById("chkExclude"+i).disabled=true;
      document.getElementById("chkExclude"+i).checked=false;
      // document.getElementById('txtRM'+i).disabled = false;
      // document.getElementById('txtPhr'+i).disabled = false;
      // document.getElementById('txtQty'+i).disabled = false;
      document.getElementById('txtRM'+i).readOnly = true;
      document.getElementById('txtPhr'+i).readOnly = true;
      document.getElementById('txtQty'+i).readOnly = true;
     }
     document.getElementById("txtNewRm").disabled=true;
     document.getElementById("txtNPHR").disabled=true;
     document.getElementById("txtNQuantity").disabled=true;
     document.getElementById("txtNQuantity").readOnly=false;
  }else if ( SamplePrep.checked == true ){
     for (var i = 0; i < clength; i++) {
      document.getElementById("chkExclude"+i).disabled=false;
     }
     document.getElementById("txtNewRm").disabled=false;
     document.getElementById("txtNPHR").disabled=false;
     document.getElementById("txtNQuantity").disabled=false;
     document.getElementById("txtNQuantity").readOnly=true;
  }

}

function changeStatus(){
  var chkStatus = document.getElementsByName("Status[]");
  var slength = chkStatus.length;
  var chk_val = new Array();
  // alert(slength);

  for ( var i = 0; i < slength; i++) {
    chk_val[i] = document.getElementById('chkStatus'+i);

    if ( chk_val[i].checked == true ){
      document.getElementById('txtDateConduct'+i).disabled = false;
      document.getElementById('txtConductBy'+i).disabled = false;
      document.getElementById('txtRemarks'+i).disabled = false;
      document.getElementById('btnUpdate'+i).disabled = false;
      // document.getElementById('btnCancel'+i).disabled = false;
      document.getElementById('hidScheduleId'+i).disabled = false;
      document.getElementById('hidEquipmentId'+i).disabled = false;
      document.getElementById('hidScheduleType'+i).disabled = false;
      document.getElementById('hidSchedule'+i).disabled = false;
      document.getElementById('hidCreatedAt'+i).disabled = false;
      document.getElementById('hidCreatedId'+i).disabled = false;
    }//end of if
    else{
      document.getElementById('txtDateConduct'+i).disabled = true;
      document.getElementById('txtConductBy'+i).disabled = true;
      document.getElementById('txtRemarks'+i).disabled = true;
      document.getElementById('btnUpdate'+i).disabled = true;
      // document.getElementById('btnCancel'+i).disabled = true;
      document.getElementById('hidScheduleId'+i).disabled = true;
      document.getElementById('hidEquipmentId'+i).disabled = true;
      document.getElementById('hidScheduleType'+i).disabled = true;
      document.getElementById('hidSchedule'+i).disabled = true;
      document.getElementById('hidCreatedAt'+i).disabled = true;
      document.getElementById('hidCreatedId'+i).disabled = true;
      document.getElementById('txtDateConduct'+i).value = '';
      document.getElementById('txtConductBy'+i).value = '';
      document.getElementById('txtRemarks'+i).value = '';
    }//end of else

  }//end of for loop
  
}

function showPoints()
{ 
    var id = document.getElementById("sltArea").value;
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("sltEntryPoint").innerHTML=xmlhttp.responseText;
            // document.getElementById("sltExitPoint").innerHTML=xmlhttp.responseText;
        }
      } //+"&ent_val="+ent_val
    xmlhttp.open("GET","include/entry_point_dropdown.php?id="+id,true);
    xmlhttp.send();
}

function disableOption(ent_val, ext_val)
{ 
    var id = document.getElementById("sltArea").value;

    // alert(ext_val);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("sltExitPoint").innerHTML=xmlhttp.responseText;
        }
      }//+ "&ext_val=" + ext_val
    xmlhttp.open("GET","include/exit_point_dropdown.php?ent_val=" + ent_val + "&id=" + id ,true);
    xmlhttp.send();
}

// function checkTime(){
//   var txtDeparture = document.getElementById('txtDeparture').value;
//   var txtArrival = document.getElementById('txtArrival').value;
//   var errorTime = document.getElementById('errorTime');

//   if (txtArrival <= txtDeparture) {
//     errorTime.value = 'Arrival time cannot be less than Departure time.';
//   }else{
//     errorTime.value = '';
//   }

// } // end function checkTime

// function checkReading(){
//   var txtKMReadingIn = document.getElementById('txtKMReadingIn').value;
//   var txtKMReadingOut = document.getElementById('txtKMReadingOut').value;
//   var errorKM = document.getElementById('errorKM');

//   if (txtKMReadingOut <= txtKMReadingIn) {
//     errorKM.value = 'KM Reading (Out) cannot be less than KM Reading (In).';
//   }else{
//     errorKM.value = '';
//   }

// } // end function checkReading

function showToll(matrix_id, index)
{ 

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            // for ( var i = 0; i < 8; i++){
            //   if ( index == i ){
               document.getElementById("txtTollfee"+index).innerHTML=xmlhttp.responseText;
            //   }
            // }
            // document.getElementById("sltExitPoint").innerHTML=xmlhttp.responseText;
        }
      }
      
      xmlhttp.open("GET","include/show_toll.php?matrix_id="+matrix_id+"&index="+index,true);
      xmlhttp.send();
}

function autoSummation(){
  var toll_count = document.getElementsByName('txtToll[]').length;
  var toll = new Array();
  var total = 0;

  for (i = 0; i < toll_count; i++) {
    toll[i] = document.getElementById('txtToll'+i).value;
    total = parseFloat(total) + parseFloat(toll[i]);
  }//end of for loop
  
  if(!isNaN(total)){
      document.getElementById('txtTotal').value = total.toFixed(2);
  }//end of if

}

function showMIRSNo()
{
    var date = document.getElementById('txtDate').value;
    // alert(id);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltMIRNo").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/mirno_dropdown.php?date="+date,true);
    xmlhttp.send();
}

function showRM(rm_type, index){
  var txtRM_type_id = document.getElementById('txtRM_type_id');

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("txtRM_id"+index).innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/rm_bat_dropdown.php?rm_type=" + rm_type + "&index=" + index,true);
    xmlhttp.send();
}

function showRMItem()
{
    var rm = document.getElementById('sltMIRNo').value;
    // alert(rm);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltRMItem").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/rm_dropdown.php?rm="+rm,true);
    xmlhttp.send();
}

function showDSQ(id)
{
    var date = document.getElementById('txtDate').value;
    // var hidLMRId = document.getElementById('hidLMRId').value;
    // alert(date);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltDSQ").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/dsq_dropdown.php?date="+date+"&id="+id,true);
    xmlhttp.send();
}

function showDSQa(id)
{
    var date = document.getElementById('txtDate').value;
    // var hidLMRId = document.getElementById('hidLMRId').value;
    // alert(date);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltDSQ").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/dsq_a_dropdown.php?date="+date+"&id="+id,true);
    xmlhttp.send();
}

function showTollPoints(vclass)
{
  // alert(vclass);
    var vlength = vclass.length;
    var vpos = vclass.indexOf('-') + 1;
    var v = vclass.substr(vpos, vlength);
    // alert(v);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltEntryExitPt0").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt1").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt2").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt3").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt4").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt5").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt6").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt7").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/entry_exit_dropdown.php?class=" + v,true);
    xmlhttp.send();

}

function include()
{
  var chk_count = document.getElementsByName('chkInclude[]').length;
  var chk_val = new Array();
  // var quantity = new Array();

  for (i = 0; i < chk_count; i++) {
    chk_val[i] = document.getElementById('chkInclude'+i);
    // quantity[i] = document.getElementById('hidQuantity'+i).value;

    if ( chk_val[i].checked == true ){
      document.getElementById('hidPREId'+i).disabled = false;
      document.getElementById('hidPREItemId'+i).disabled = false;
      document.getElementById('hidQuantity'+i).disabled = false;
      // document.getElementById('hidQty'+i).value = quantity[i];
      document.getElementById('hidUOMId'+i).disabled = false;
      document.getElementById('hidSupplyId'+i).disabled = false;
      document.getElementById('txtUnitPrice'+i).disabled = false;
      document.getElementById('txtTotalAmount'+i).disabled = false;
      document.getElementById('txtTotalAmount'+i).readOnly = true;
    }
     else{
      document.getElementById('hidPREId'+i).disabled = true;
      document.getElementById('hidPREItemId'+i).disabled = true;
      document.getElementById('hidQuantity'+i).disabled = true;
      // document.getElementById('hidQty'+i).value = 0.00;
      document.getElementById('hidUOMId'+i).disabled = true;
      document.getElementById('hidSupplyId'+i).disabled = true;
      document.getElementById('txtUnitPrice'+i).disabled = true;
      document.getElementById('txtTotalAmount'+i).disabled = true;
      document.getElementById('txtTotalAmount'+i).readOnly = false;
    }

  }//end of for loop

}//end of function 

function autoProduct()
{
  var qty_count = document.getElementsByName('hidQuantity[]').length;
  var quantity = new Array();
  // var qty = new Array();
  var unit_price = new Array();
  var product = new Array();

  for ( var i = 0; i < qty_count; i++ ) {

    unit_price[i] = document.getElementById('txtUnitPrice'+i).value;
    quantity[i] = document.getElementById('hidQuantity'+i).value;
    // qty[i] = document.getElementById('hidQty'+i).value;

    if ( document.getElementById('hidQuantity'+i).disabled == true ){
      product[i] = 0 * parseFloat(unit_price[i]);
    }else{
      product[i] = parseFloat(quantity[i]) * parseFloat(unit_price[i]);
    }

    if(!isNaN(product[i])){
        document.getElementById('txtTotalAmount'+i).value = product[i].toFixed(4);
    }//end of 2nd if

  }

}

function autoAdd()
{
  var amount_count = document.getElementsByName('txtTotalAmount[]').length;
  var amount = new Array();
  var sum = 0;

  for ( var i = 0; i < amount_count; i++ ) {

    amount[i] = +document.getElementById('txtTotalAmount'+i).value;

    sum = parseFloat(sum) + parseFloat(amount[i]);

  }//end of for loop

  if(!isNaN(sum)){

      document.getElementById('txtTotalPrice').value = sum.toFixed(4);

  }

}

function editable()
{
  if ( document.getElementById('chkEdit').checked == true )
  {
    document.getElementById('txtMixParam1').readOnly = false;
    document.getElementById('txtMixSequence1').readOnly = false;
    document.getElementById('txtMixParam2').readOnly = false;
    document.getElementById('txtMixSequence2').readOnly = false;
    document.getElementById('txtMixParam3').readOnly = false;
    document.getElementById('txtMixSequence3').readOnly = false;
    document.getElementById('txtMixParam4').readOnly = false;
    document.getElementById('tztMixSequence4').readOnly = false;
    document.getElementById('txtMixParam5').readOnly = false;
    document.getElementById('txtMixSequence5').readOnly = false;
  }
  else
  {
    document.getElementById('txtMixParam1').readOnly = true;
    document.getElementById('txtMixSequence1').readOnly = true;
    document.getElementById('txtMixParam2').readOnly = true;
    document.getElementById('txtMixSequence2').readOnly = true;
    document.getElementById('txtMixParam3').readOnly = true;
    document.getElementById('txtMixSequence3').readOnly = true;
    document.getElementById('txtMixParam4').readOnly = true;
    document.getElementById('tztMixSequence4').readOnly = true;
    document.getElementById('txtMixParam5').readOnly = true;
    document.getElementById('txtMixSequence5').readOnly = true;
  }

}


function showFG(fg, batID)
{
  var soft = document.getElementById('Soft');
  var rigid = document.getElementById('Rigid');
  var sltActivity = document.getElementById('sltActivity').value;
  var txtExtDarNo = document.getElementById('txtExtDarNo');
  var txtMixDarNo = document.getElementById('txtMixDarNo');
  var txtExtZ1 = document.getElementById('txtExtZ1');
  var txtMixParam1 = document.getElementById('txtMixParam1');
  var txtMixSequence1 = document.getElementById('txtMixSequence1');
  var txtExtZ2 = document.getElementById('txtExtZ2');
  var txtMixParm2 = document.getElementById('txtMixParm2');
  var txtMixSequence2 = document.getElementById('txtMixSequence2');
  var txtExtDH = document.getElementById('txtExtDH');
  var txtMixParam3 = document.getElementById('txtMixParam3');
  var txtMixSequence3 = document.getElementById('txtMixSequence3');
  var txtExtScrewSpeed = document.getElementById('txtExtScrewSpeed');
  var txtMixParam4 = document.getElementById('txtMixParam4');
  var tztMixSequence4 = document.getElementById('tztMixSequence4');
  var txtExtCutterSpeed = document.getElementById('txtExtCutterSpeed');
  var txtMixParam5 = document.getElementById('txtMixParam5');
  var txtSequence5 = document.getElementById('txtSequence5');

  if ( soft.checked == true )
  {
    txtExtDarNo.readOnly = false;
    txtExtZ1.readOnly = false;
    txtExtZ2.readOnly = false;
    txtExtDH.readOnly = false;
    txtExtScrewSpeed.readOnly = false;
    txtExtCutterSpeed.readOnly = false;
    var soft_pvc = 1;
  }
  else if ( rigid.checked == true )
  {
    txtExtDarNo.readOnly = true;
    txtExtZ1.readOnly = true;
    txtExtZ2.readOnly = true;
    txtExtDH.readOnly = true;
    txtExtScrewSpeed.readOnly = true;
    txtExtCutterSpeed.readOnly = true;
    var soft_pvc = 0;
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltFG").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/fg_dropdown.php?soft_pvc=" + soft_pvc + "&fg=" + fg + "&batID=" + batID + "&sltActivity=" + sltActivity,true);
    xmlhttp.send();

}

function showCOQ(fgid)
{
  var lotnumber = document.getElementById('sltLotNum').value;
  // alert(lotnumber);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("coqdd").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/coq_dropdown.php?fgid=" + fgid + "&lotnumber=" + lotnumber,true);
    xmlhttp.send();

}

function showPersonnel()
{
  var sltDSQ = document.getElementById('sltDSQ').value;
  var dpos = sltDSQ.indexOf('-');
  var dsq = sltDSQ.substr(0, dpos);
  // alert(dsq);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("hidPersonnel").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/personnel_dropdown.php?dsqid=" + dsq,true);
    xmlhttp.send();

}

function showMachine()
{
  var mixer = document.getElementById('M');
  var extruder = document.getElementById('E');

  if ( mixer.checked == true )
  {
    // alert(mixer.value);
    var machine_type = mixer.value;
  }
  else if ( extruder.checked == true )
  {
    // alert(extruder.value);
    var machine_type = extruder.value;
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("MachineLines").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/machine_dropdown.php?machine_type=" + machine_type, true);
    xmlhttp.send();

}

function showMBTypeCode()
{
  var mb_id = document.getElementById('sltMasterBatch').value;

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("mb_type_code").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/mb_type_code_dropdown.php?mb_id=" + mb_id, true);
    xmlhttp.send();

}

function showFGSales(sltItemType, fg_id, index)
{
  var hidSalesID = document.getElementById('hidSalesID');
  var sltProductLen = document.getElementsByName('sltProduct[]').length;
  // var sltItemType = document.getElementById('sltItemType').value;
  var sltCustomer = document.getElementById('sltCustomer').value;

// alert(sltItemType);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          // for ( var i = 0; i < sltProductLen; i++ ){
           document.getElementById("tdProduct"+index).innerHTML=xmlhttp.responseText;
          // }
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_sales.php?hidSalesID=" + hidSalesID + "&fg_id=" + fg_id + "&sltCustomer=" + sltCustomer + "&sltItemType=" + sltItemType + "&index=" + index, true);
    xmlhttp.send();

}

function showFGItemType(item_type)
{
  var hidSalesID = document.getElementById('hidSalesID');
  var sltItemTypeLen = document.getElementsByName('sltItemType[]').length;
  var sltCustomer = document.getElementById('sltCustomer').value;

// alert(sltItemType);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          for ( var i = 0; i < sltItemTypeLen; i++ ){
           document.getElementById("sltItemType"+i).innerHTML=xmlhttp.responseText;
          }
        }
      }
    xmlhttp.open("GET","include/fg_item_type.php?hidSalesID=" + hidSalesID + "&item_type=" + item_type, true);
    xmlhttp.send();

}

function showFGSales1(sltItemType, fg_id)
{
  var hidSalesID = document.getElementById('hidSalesID');
  var sltProductLen = document.getElementsByName('sltProduct[]').length;
  // var sltItemType = document.getElementById('sltItemType').value;
  var sltCustomer = document.getElementById('sltCustomer').value;

// alert(sltItemType);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          for ( var i = 0; i < sltProductLen; i++ ){
           document.getElementById("sltProduct"+i).innerHTML=xmlhttp.responseText;
          }
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_sales_1.php?hidSalesID=" + hidSalesID + "&fg_id=" + fg_id + "&sltCustomer=" + sltCustomer + "&sltItemType=" + sltItemType, true);
    xmlhttp.send();

}

function showExtraItems(tag)
{
  var hidSalesID = document.getElementById('hidSalesID');
  var sltProductLen = document.getElementsByName('sltProduct[]').length;
  var sltCustomer = document.getElementById('sltCustomer').value;
  var sltItemTypeLen = document.getElementsByName('sltItemType[]').length;

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("extraItem").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/show_extra_item.php?tag = " + tag, true);
    xmlhttp.send();

}



function closedBox(id, type, number, index){
    var closed_index = document.getElementById("closed"+index);
    if( closed_index.checked == true ){

      if( closed_index.disabled == false ){
          swal(
              {
                title: "Are you sure you want to \nclose " + type + " " + number + "?",   
                text: "Enter reason for closure:",   
                type: "input",   
                confirmButtonColor: "#DD6B55", 
                confirmButtonText: "YES", 
                showCancelButton: true,
                closeOnConfirm: false,   
                inputPlaceholder: "Enter reason",   
                showLoaderOnConfirm: true
              },
              function(inputValue){
                if (inputValue === false) {
                  document.getElementById("closed"+index).checked = false;
                  return false;      
                }

                if (inputValue.trim() === "") {     
                  swal.showInputError("You need to write something!");    
                  return false; 
                }else{
                  swal({title: "Processing ...", type: "info"});  
                  setTimeout(
                    function() {
                      window.location.assign('process_close_so.php?id=' + id + '&reason=' + inputValue);
                    }
                    , 2000
                  ); 
                }

              }

            );
      }

    }
}

function showFG_Forecast(fg_id, forecast_id)
{
  // alert(fg);
  var radLocal = document.getElementById('radLocal');
  var radExport = document.getElementById('radExport');
  var sltFGItem = document.getElementsByName('sltFGItem[]').length;

  if ( radLocal.checked == true ){
    fg_type = 1;
  }else if ( radExport.checked == true ){
    fg_type = 0;
  }

  // alert(fg_type);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          for ( var i = 0; i < sltFGItem; i++ ){
            document.getElementById("sltFGItem"+i).innerHTML=xmlhttp.responseText;
          }
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_forecast.php?fg_type=" + fg_type + "&fg_id=" + fg_id + "&forecast_id=" + forecast_id, true);
    xmlhttp.send();

}

function addExtraSpecs(count)
{
  // var hidSalesID = document.getElementById('hidSalesID');
  // var sltProductLen = document.getElementsByName('sltProduct[]').length;
  // var sltCustomer = document.getElementById('sltCustomer').value;
  // var sltItemTypeLen = document.getElementsByName('sltItemType[]').length;

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("AddSpecs").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/show_extra_specs.php?count=" + count, true);
    xmlhttp.send();

}

function showReceivedFrom(sltReceivedType, addInventoryID, receivedFromID)
{
  // alert(sltReceivedType);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltReceivedFrom").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/received_from_dropdown.php?received_type=" + sltReceivedType + "&receivedFromID=" + receivedFromID + "&addInventoryID=" + addInventoryID,true);
    xmlhttp.send();

}

function showAsset(addInventoryID, index)
{
  var sltAssetType = document.getElementById('sltAssetType'+index).value;
  // alert(sltAssetType);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltAsset"+index).innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset1").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset2").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset3").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset4").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset5").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset6").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset7").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset8").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset9").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/asset_dropdown.php?asset_type=" + sltAssetType + "&addInventoryID=" + addInventoryID,true);
    xmlhttp.send();

}

function showAsset2(addInventoryID, index)
{
  var sltAssetType = document.getElementById('sltAssetType'+index).value;
  // alert(sltAssetType);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltAsset"+index).innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/asset_dropdown.php?asset_type=" + sltAssetType + "&addInventoryID=" + addInventoryID,true);
    xmlhttp.send();

}

function showAssetSpecification(Asset, addInventoryID, index)
{
  var hidInventoryType = document.getElementById('hidInventoryType').value;
  // var sltAssetCondition = document.getElementById('sltAssetCondition'+index).value;
  // alert(hidInventoryType);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltAssetSpecification"+index).innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/asset_specification_dropdown.php?asset=" + Asset + "&addInventoryID=" + addInventoryID + "&inventory_type=" + hidInventoryType,true);
    xmlhttp.send();

}

function disablePTSNo(){

  var sltInventorySource = document.getElementById('sltInventorySource').value;

  if ( sltInventorySource == 'NonPTS' ){
    document.getElementById("txtPTSNo").readOnly= true;
  }else{
    document.getElementById("txtPTSNo").readOnly= false;
  }

}

function showReceivedType(addInventoryID, received_type){

  var sltInventorySource = document.getElementById("sltInventorySource").value;

  if ( sltInventorySource != 'Others' ){
    document.getElementById("txtOtherSource").readOnly= true;
  }else{
    document.getElementById("txtOtherSource").readOnly= false;
  }

    var xmlhttp; 
      if (window.XMLHttpRequest)
        {
          xmlhttp=new XMLHttpRequest();
        }
      else
        {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
      xmlhttp.onreadystatechange=function()
        {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
          {
            document.getElementById("sltReceivedType").innerHTML=xmlhttp.responseText;
          }
        }
      xmlhttp.open("GET","include/received_type_dropdown.php?sltInventorySource=" + sltInventorySource + "&received_type=" + received_type + "&addInventoryID=" + addInventoryID,true);
      xmlhttp.send();
}

function enableRefRR(){
  var sltInventorySource = document.getElementById("sltInventorySource").value;
  var pts_count = document.getElementsByName("txtRefRRNumber[]").length;

  for ( var i = 0; i < pts_count; i++ ){
    if ( sltInventorySource == "PRE" ){
      document.getElementById("txtRefRRNumber"+i).readOnly=false;
    } else{
      document.getElementById("txtRefRRNumber"+i).readOnly=true;
    }
  }
}

function showPTSReceiver(addInventoryID, userID){

  var sltToDepartment = document.getElementById("sltToDepartment").value;
  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");

  if ( Compounds.checked == true ){
    var division = 'compounds';
  }else if ( Pipes.checked == true ){
    var division = 'pipes';
  }else if ( Corporate.checked == true ){
    var division = 'corporate';
  }
  // alert(sltToDepartment);
    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltToUser").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/pts_receiver_dropdown.php?sltToDepartment=" + sltToDepartment + "&addInventoryID=" + addInventoryID + "&division=" + division + "&userID=" + userID,true);
      xmlhttp.send();

}

function showCCRFGDropdown(ccr_id, fg_id){

  var sltCustomer = document.getElementById("sltCustomer").value;
  // alert(sltCustomer);
    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltProduct").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/fg_dropdown_ccr.php?sltCustomer=" + sltCustomer + "&fg_id=" + fg_id,true);
      xmlhttp.send();

}

function enableOtherComplaint(){

  var sltComplaintType = document.getElementById('sltComplaintType').value;
  
  if ( sltComplaintType == 4 ){
    document.getElementById("txtOtherComplaintType").readOnly= false;
  }else{
    document.getElementById("txtOtherComplaintType").readOnly= true;
  }

}

function disabledInvalid(){

  var chkInvalid = document.getElementById('chkInvalid');
  
  if ( chkInvalid.checked == true ){
    document.getElementById("txtRootCause").readOnly= true;
    document.getElementById("TechnicalRoot").disabled= true;
    document.getElementById("PAMRoot").disabled= true;
    document.getElementById("WPDRoot").disabled= true;
    document.getElementById("Man").disabled= true;
    document.getElementById("Method").disabled= true;
    document.getElementById("Machine").disabled= true;
    document.getElementById("Material").disabled= true;
    document.getElementById("Measurement").disabled= true;
    document.getElementById("MotherNature").disabled= true;
    document.getElementById("txtCorrective").readOnly= true;
    document.getElementById("txtImplementCorrective").readOnly= true;
    document.getElementById("txtResponsibleCorrective").readOnly= true;
    document.getElementById("txtPreventive").readOnly= true;
    document.getElementById("txtImplementPreventive").readOnly= true;
    document.getElementById("txtResponsiblePreventive").readOnly= true;
    // alert('1');
  }else{
    document.getElementById("txtRootCause").readOnly= false;
    document.getElementById("TechnicalRoot").disabled= false;
    document.getElementById("PAMRoot").disabled= false;
    document.getElementById("WPDRoot").disabled= false;
    document.getElementById("Man").disabled= false;
    document.getElementById("Method").disabled= false;
    document.getElementById("Machine").disabled= false;
    document.getElementById("Material").disabled= false;
    document.getElementById("Measurement").disabled= false;
    document.getElementById("MotherNature").disabled= false;
    document.getElementById("txtCorrective").readOnly= false;
    document.getElementById("txtImplementCorrective").readOnly= false;
    document.getElementById("txtResponsibleCorrective").readOnly= false;
    document.getElementById("txtPreventive").readOnly= false;
    document.getElementById("txtImplementPreventive").readOnly= false;
    document.getElementById("txtResponsiblePreventive").readOnly= false;
  }

}

function showDepartment(id, department_id){

  var Cmpds = document.getElementById("Cmpds");
  var Pips = document.getElementById("Pips");
  var Corp = document.getElementById("Corp");
  var PPR = document.getElementById("PPR");

  if ( Cmpds.checked == true ){
    var division = 'compounds';
  }else if ( Pips.checked == true ){
    var division = 'pipes';
  }else if ( Corp.checked == true ){
    var division = 'corporate';
  }else if ( PPR.checked == true ){
    var division = 'ppr';
  }

  // alert( division );

    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltDept1").innerHTML=xmlhttp.responseText;
          document.getElementById("sltDept2").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();

}


function showCPIARIssuer(id, department_id, user_id) {

  var Compounds = document.getElementById("FromCompounds");
  var Pipes = document.getElementById("FromPipes");
  var Corporate = document.getElementById("FromCorporate");

  if ( Compounds.checked == true ){
    var division = 'compounds';
  }else if ( Pipes.checked == true ){
    var division = 'pipes';
  }else if ( Corporate.checked == true ){
    var division = 'corporate';
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltIssuerID").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/qms_user_dropdown.php?id=" + id+ "&department_id=" + department_id + "&user_id=" + user_id + "&division=" + division ,true);
    xmlhttp.send();
}

function showCPIARReceiver(id, department_id, user_id){

  var Compounds = document.getElementById("ToCompounds");
  var Pipes = document.getElementById("ToPipes");
  var Corporate = document.getElementById("ToCorporate");

  if ( Compounds.checked == true ){
    var division = 'compounds';
  }else if ( Pipes.checked == true ){
    var division = 'pipes';
  }else if ( Corporate.checked == true ){
    var division = 'corporate';
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltReceiverID").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/qms_user_dropdown.php?id=" + id+ "&department_id=" + department_id + "&user_id=" + user_id + "&division=" + division ,true);
    xmlhttp.send();
}

function showCPIARVerifier(id, department_id, user_id) {

  var Compounds = document.getElementById("ToCompounds");
  var Pipes = document.getElementById("ToPipes");
  var Corporate = document.getElementById("ToCorporate");

  if ( Compounds.checked == true ){
    var division = 'compounds';
  }else if ( Pipes.checked == true ){
    var division = 'pipes';
  }else if ( Corporate.checked == true ){
    var division = 'corporate';
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltAuditorID").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/qms_user_dropdown.php?id=" + id+ "&department_id=" + department_id + "&user_id=" + user_id + "&division=" + division ,true);
    xmlhttp.send();
}

function showToDepartment(id, department_id){

  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");
  var PPR = document.getElementById("PPR");

  if ( Compounds.checked == true ){
    var division = 'compounds';
  }else if ( Pipes.checked == true ){
    var division = 'pipes';
  }else if ( Corporate.checked == true ){
    var division = 'corporate';
  }else if ( PPR.checked == true ){
    var division = 'ppr';
  }

  // alert( division );

    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltToDepartment").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();

}

function showPREDepartment(id, department_id){

  var r = document.getElementsByName("radDivision");
  var length = r.length;

  for (var i = 0; i < length; i++) {
      if (r[i].checked) {
        if ( r[i].value == 'Compounds' ){
          var division = 'compounds';
        }else if ( r[i].value == 'Pipes' ){
          var division = 'pipes';
        }else if ( r[i].value == 'Corporate' ){
          var division = 'corporate';
        }else if ( r[i].value == 'PPR' ){
          var division = 'ppr';
        }else if ( r[i].value == 'Others' ){
          var division = 'Others';
        }
      }
  }
    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltDept").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();

}

function showFGSalesForecastAnnual(sltItemType, fg_id)
{
  var hidSalesID = document.getElementById('hidForecastID').value;
  var sltCustomer = document.getElementById('sltCustomer').value;
  // var len = document.getElementsByName("sltFGItem[]").length;

// alert(sltItemType);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          // for ( var i = 0; i < len; i++ ){
            document.getElementById("sltFGItem").innerHTML=xmlhttp.responseText;
          // }
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_forecast_annual.php?hidSalesID=" + hidSalesID + "&fg_id=" + fg_id + "&sltCustomer=" + sltCustomer + "&sltItemType=" + sltItemType, true);
    xmlhttp.send();

}

function showLCNumber(){

  var sltCustomer = document.getElementById('sltCustomer').value;

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          // for ( var i = 0; i < len; i++ ){
            document.getElementById("sltLCNumber").innerHTML=xmlhttp.responseText;
          // }
        }
      }
    xmlhttp.open("GET","include/lc_number_dropdown.php?sltCustomer=" + sltCustomer, true);
    xmlhttp.send();

}

function showIssDepartment(id, department_id) {

  var Compounds = document.getElementById("FromCompounds");
  var Pipes = document.getElementById("FromPipes");
  var Corporate = document.getElementById("FromCorporate");

  if ( Compounds.checked == true ){
    division = 'compounds';
  }else if ( Pipes.checked == true ){
    division = 'pipes';
  }else if ( Corporate.checked == true ){
    division = 'corporate';
  }

  // alert( division );

    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltIssueDeptID").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();
}

function showRecDepartment(id, department_id){

  var Compounds = document.getElementById("ToCompounds");
  var Pipes = document.getElementById("ToPipes");
  var Corporate = document.getElementById("ToCorporate");

  if ( Compounds.checked == true ){
    division = 'compounds';
  }else if ( Pipes.checked == true ){
    division = 'pipes';
  }else if ( Corporate.checked == true ){
    division = 'corporate';
  }

  // alert( division );

    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltReceiveDeptID").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();
}

function showNRMResults(){

  var sltNRM = document.getElementById("sltNRM").value;

    // alert(sltNRM);

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("NRMResults").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/nrm_results.php?sltNRM=" + sltNRM,true);
    xmlhttp.send();

}

function showJRDNumber()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      jrd = 0;
    }else if ( JRD.checked == true ){
      jrd = 1;
    }
    // alert('nrm '+ nrm + 'jrd '+ jrd );
    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltJRDNumber").innerHTML=xmlhttp.responseText;
      }
    }

        xmlhttp.open("GET","include/jrd_number_dropdown.php?jrd=" + jrd,true);
        xmlhttp.send();
}

function showNRMNumber()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      nrm = 1;
    }else if ( JRD.checked == true ){
      nrm = 0;
    }
    // alert('nrm '+ nrm + 'jrd '+ jrd );
    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltNRMNumber").innerHTML=xmlhttp.responseText;
      }
    }

        xmlhttp.open("GET","include/nrm_number_dropdown.php?nrm=" + nrm,true);
        xmlhttp.send();
}

function showRNDActivity()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      rnd_activity = NRM.value;
    }else if ( JRD.checked == true ){
      rnd_activity = JRD.value;
    }
    // alert(rnd_activity);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltActivity").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/activity_dropdown.php?rnd_activity=" + rnd_activity,true);
    xmlhttp.send();
}

function enableClassOthers(){
  var radClassOthers = document.getElementById('radClassOthers');
  var radClassResin = document.getElementById('radClassResin');
  var radClassStabilizer = document.getElementById('radClassStabilizer');
  var radClassFillers = document.getElementById('radClassFillers');
  var radClassPlasticizer = document.getElementById('radClassPlasticizer');
  var radClassLubricant = document.getElementById('radClassLubricant');
  var radClassColorant = document.getElementById('radClassColorant');

  if ( radClassOthers.checked == true ) {
    document.getElementById("txtOtherClass").readOnly = false;
  }else if ( radClassOthers.checked == false 
            || radClassResin.checked == true
            || radClassStabilizer.checked == true
            || radClassFillers.checked == true
            || radClassPlasticizer.checked == true
            || radClassLubricant.checked == true
            || radClassColorant.checked == true ){
    document.getElementById("txtOtherClass").readOnly = true;
  }
}

function enableSampleOthers(){
  var radSamplePowder = document.getElementById('radSamplePowder');
  var radSampleLiquid = document.getElementById('radSampleLiquid');
  var radSampleOthers = document.getElementById('radSampleOthers');
  var radSamplePellet = document.getElementById('radSamplePellet');

  if ( radSampleOthers.checked == true ) {
    document.getElementById("txtOtherSample").readOnly = false;
  }else if ( radSampleOthers.checked == false 
            || radSamplePowder.checked == true
            || radSampleLiquid.checked == true
            || radSamplePellet.checked == true){
    document.getElementById("txtOtherSample").readOnly = true;
  }
}

function enableOtherSupplier(){
  var otherSupplier = document.getElementById('otherSupplier');
  var optionLength = document.getElementById('sltSupplier').length;

  if ( otherSupplier.checked == true ) {
    document.getElementById("txtOtherSupplier").readOnly = false;
    for ( var i = 0; i < optionLength; i++ ){
      document.getElementById('optionSupplier'+i).disabled = true;
    }
    // alert(optionLength);
  }else if ( otherSupplier.checked == false ){
    document.getElementById("txtOtherSupplier").readOnly = true;
    for ( var i = 0; i < optionLength; i++ ){
      document.getElementById('optionSupplier'+i).disabled = false;
    }
  }
}

function disabledOptionFG(){
  var sltFGLen = document.getElementById('sltFG').length;
  var sltActivity = document.getElementById('sltActivity').value;

  for ( var i = 0; i < sltFGLen; i++ ){
    if ( sltActivity == 4 || sltActivity == 2 ){
      document.getElementById('optionFG'+i).disabled = true;
    }else{
      document.getElementById('optionFG'+i).disabled = false;
    }
  }

  // alert(sltFGLen);
}

/* ################# CREDIT AND COLLECTIONS ################# */
function enableTerms(){

  var sltTerms = document.getElementById("sltTerms").value;

  if ( sltTerms == 'COD'){
    document.getElementById("txtTerms").readOnly = true;
  }else{
    document.getElementById("txtTerms").readOnly = false;
  }

}

function enableCheck(){

  var sltTransType = document.getElementById("sltTransType").value;

  if ( sltTransType == 'PRC' || sltTransType == 'IPT'){
    document.getElementById("txtInvoiceDate").disabled = true;
    document.getElementById("txtCheckReturnedDate").disabled = false;

  }else{
    document.getElementById("txtInvoiceDate").disabled = false;
    document.getElementById("txtCheckReturnedDate").disabled = true;

  }

  for ( var i = 0; i < 10; i++ ){

    var txtInvoiceNumber = document.getElementById("txtInvoiceNumber"+i);
    var sltBankCodeLength = document.getElementById("sltBankCode"+i).length;
    // var txtChecknumber = document.getElementById("txtChecknumber"+i);

    // alert(sltBankCodeLength);

    if ( sltTransType == 'PRC' || sltTransType == 'IPT'){
      txtInvoiceNumber.disabled = true;

      if ( i == 0 ){
        document.getElementById("txtAmount"+i).disabled = false;
        document.getElementById("txtDiscount"+i).disabled = false;
      }else{
        document.getElementById("txtAmount"+i).disabled = true;
        document.getElementById("txtDiscount"+i).disabled = true;
      }

      document.getElementById("txtChecknumber0").disabled = false;

      for ( var y = 0; y < (sltBankCodeLength-1); y++ ){
        document.getElementById("optionBankCode0"+y).disabled = false;
      }

    }else{
      txtInvoiceNumber.disabled = false;
      document.getElementById("txtAmount"+i).disabled = false;
      document.getElementById("txtDiscount"+i).disabled = false;
      document.getElementById("txtChecknumber"+i).disabled = true;

      for ( var y = 0; y < (sltBankCodeLength-1); y++ ){
        document.getElementById("optionBankCode0"+y).disabled = true;
      }

    }

  }

}

function showInvoices( index, jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber"+index);
  var sltBankCode = document.getElementById("sltBankCode"+index).value;
  var txtChecknumber = document.getElementById("txtChecknumber"+index).value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdInvoiceNumber"+index).innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_invoices.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&index=" + index + "&jacket_id=" + jacket_id,true);
  xmlhttp.send();

}

function showAmounts( index, jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber"+index);
  var sltBankCode = document.getElementById("sltBankCode"+index).value;
  var txtChecknumber = document.getElementById("txtChecknumber"+index).value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdAmount"+index).innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_amounts.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&index="+ index + "&jacket_id=" + jacket_id ,true);
  xmlhttp.send();

}

function showDiscounts( index, jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber"+index);
  var sltBankCode = document.getElementById("sltBankCode"+index).value;
  var txtChecknumber = document.getElementById("txtChecknumber"+index).value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdDiscount"+index).innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_discounts.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&index=" + index + "&jacket_id=" + jacket_id,true);
  xmlhttp.send();

}

function showInvoiceNumber(sltInvoiceNum){

  var sltCustomer = document.getElementById("sltCustomer").value;
  var hidMemoId = document.getElementById("hidMemoId").value;
  var hidProcessType = document.getElementById("hidProcessType").value;
  // var sltInvoiceNum = document.getElementById("sltInvoiceNum").value;
  // var hidDebitId = document.getElementById("hidDebitId").value;

    // alert(hidDebitId);

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltInvoiceNum").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/invoice_number_dropdown.php?sltCustomer=" + sltCustomer + "&sltInvoiceNum=" + sltInvoiceNum + "&hidMemoId=" + hidMemoId + "&hidProcessType=" + hidProcessType,true);
    xmlhttp.send();

}

function showCreditPaymentItems(){

  var sltCustomer = document.getElementById("sltCustomer").value;
  var hidCreditId = document.getElementById("hidCreditId").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("tbleCreditPaymentItems").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/credit_payment_items_dropdown.php?sltCustomer=" + sltCustomer + "&hidCreditId=" + hidCreditId,true);
    xmlhttp.send();

}

function enableCreditItems(index){

  var chkIncludeInvoice = document.getElementById("chkIncludeInvoice"+index);
  var hidInvoiceNumber = document.getElementById("hidInvoiceNumber"+index);
  var txtAmount = document.getElementById("txtAmount"+index);
  var txtDiscount = document.getElementById("txtDiscount"+index);
  var hidCreditAmount = document.getElementById("hidCreditAmount"+index);
  var hidDebitAmount = document.getElementById("hidDebitAmount"+index);
  var hidARAmount = document.getElementById("hidARAmount"+index);
  var hidPaidAmount = document.getElementById("hidARAmount"+index);
  var txtBalance = document.getElementById("txtBalance"+index);

  if ( chkIncludeInvoice.checked == true ){
    hidInvoiceNumber.disabled = false;
    txtAmount.readOnly = false;
    hidCreditAmount.disabled = false;
    hidDebitAmount.disabled = false;
    txtDiscount.readOnly = false;
    hidARAmount.disabled = false;
    hidPaidAmount.disabled = false;
    // txtBalance.readOnly = true;
    // txtBalance.disabled = false;
  }else{
    hidInvoiceNumber.disabled = true;
    txtAmount.readOnly = true;
    hidCreditAmount.disabled = true;
    hidDebitAmount.disabled = true;
    txtDiscount.readOnly = true;
    hidARAmount.disabled = true;
    hidPaidAmount.disabled = true;
    // txtBalance.readOnly = false;
    // txtBalance.disabled = true;
  }

}

function valueForFirstField(checkAmount){

  document.getElementById("txtBalance0").value = checkAmount;
  var InvoiceCount = document.getElementsByName("hidInvoiceNumber[]").length;

  for ( var i = 0; i < InvoiceCount; i++){
    document.getElementById("Payment"+i).value = 0;
  }

}

function computeBalance(index){
  var chkIncludeInvoice = document.getElementById("chkIncludeInvoice"+index);
  var hidARAmount = document.getElementById("hidARAmount"+index).value;
  var hidPaidAmount = document.getElementById("hidPaidAmount"+index).value;
  var hidCreditAmount = document.getElementById("hidCreditAmount"+index).value;
  var hidDebitAmount = document.getElementById("hidDebitAmount"+index).value;
  var txtDiscount = document.getElementById("txtDiscount"+index).value;
  var txtBalance = document.getElementById("txtBalance"+index);

  if ( txtDiscount == '' || txtDiscount == null ){
    txtDiscount = 0;
  }

  if ( chkIncludeInvoice.checked == true ){
    var Balance = parseFloat(hidARAmount) - parseFloat(hidPaidAmount) - parseFloat(hidCreditAmount) + parseFloat(hidDebitAmount) - parseFloat(txtDiscount);

    if ( !isNaN(Balance) ){
      txtBalance.value = parseFloat(Balance).toFixed(2);
      // document.getElementById("txtAmount"+index).value = parseFloat(Balance);
      // .toString().replace(/\B(?=(?=\d*\.)(\d{3})+(?!\d))/g, ',');
    }
  }else{
    var Balance = 0;
    txtBalance.value = '';
    document.getElementById("txtAmount"+index).value='';
  }

}

function computeTotalPayment(){
  var AmountLength = document.getElementsByClassName("sample").length;
  // alert(AmountLength);
  var Payments = new Array();
  var TotalPayment = 0;

  for ( var i = 0; i < AmountLength; i++ ){
    Payments[i] = document.getElementById("txtAmount"+i).value;

    if ( Payments[i] == '' || Payments[i] == null ){
      Payments[i] = 0;
    }

    TotalPayment = parseFloat(Payments[i]) + parseFloat(TotalPayment);

  }

  if ( !isNaN(TotalPayment) ){
    var txtTotalPayment = document.getElementById("txtTotalPayment");
    txtTotalPayment.value = parseFloat(TotalPayment).toFixed(2);
    // .toString().replace(/\B(?=(?=\d*\.)(\d{3})+(?!\d))/g, ',');
  } 

  var txtCheckAmount = document.getElementById("txtCheckAmount").value;

  if ( txtCheckAmount == '' || txtCheckAmount == null ){

    document.getElementById("lblErrorMessage").innerHTML = '* Check amount can\'t be blank.';
    document.getElementById("tdErrorMessage").innerHTML = '* Check amount can\'t be blank.';
    document.getElementById("hidErrorMessage").value = '* Check amount can\'t be blank.';

  }else if ( parseFloat(txtTotalPayment.value).toFixed(2) > parseFloat(txtCheckAmount) ){

    document.getElementById("lblErrorMessage").innerHTML = '* Total payment can\'t be greater than check amount.';
    document.getElementById("hidErrorMessage").value = '* Total payment can\'t be greater than check amount.';

  }else{

    document.getElementById("lblErrorMessage").innerHTML = '';
    document.getElementById("hidErrorMessage").value = '';
    document.getElementById("tdErrorMessage").innerHTML = '';

  }

}

// function validate(form) {
//   var invoice_amount = document.getElementById("txtInvoiceNo").value;
//   var amount = parseFloat(document.getElementById("txtAmount").value);

//   var invoice_length = invoice_amount.length;
//   var invoice_position = invoice_amount.indexOf('-') + 1;
//   var balance = parseFloat(invoice_amount.substr(invoice_position, invoice_length));

//   // alert(amount);

//     if( amount > balance ) {
//       // alert(balance);
//         return confirm("The payment amount is greater than the invoice balance. Are you sure you want to post the payment?");
//         // return false;
//     }

// }

// function disabledOptionBankCode(){
//   var sltFGLen = document.getElementById('sltFG').length;
//   var sltActivity = document.getElementById('sltActivity').value;

//   for ( var i = 0; i < sltFGLen; i++ ){
//     if ( sltActivity == 4 || sltActivity == 2 ){
//       document.getElementById('optionFG'+i).disabled = true;
//     }else{
//       document.getElementById('optionFG'+i).disabled = false;
//     }
//   }

//   // alert(sltFGLen);
// }

/* ################# CREDIT AND COLLECTIONS ################# */

/* #############  RESIN CALCULATOR  ############ */

function showMixerCapacity()
{
  var mixer_id = document.getElementById("sltMixer").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("tdMixerCapacity").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/mixer_capacity.php?mixer_id="+mixer_id,true);
    xmlhttp.send();
}

function showFormulaPHR(formula_type_id)
{
  // var formula_type_id = document.getElementById("sltFormulaType").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("tble10").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/formula_phr.php?formula_type_id="+formula_type_id,true);
    xmlhttp.send();
}


function showRMComputation(){

  var formula_length = document.getElementsByName('hidFormulaPHR[]').length;
  var hidResinDosage0 = document.getElementById('hidResinDosage0').value;
  var hidFormulaPHR0 = document.getElementById('hidFormulaPHR0').value;
  var totalPHR = 0;
  var totalDosage = 0;

  for (var i = 0; i < (formula_length); i++) {
    var hidFormulaPHR = document.getElementById('hidFormulaPHR'+i).value;
    var hidResinDosage = parseFloat(document.getElementById('hidResinDosage'+i).value);

    totalPHR = parseFloat(totalPHR) + parseFloat(hidFormulaPHR);

    if ( i != 0 ){
      rm_dosage = parseFloat(hidFormulaPHR) / parseFloat(hidFormulaPHR0) * parseFloat(hidResinDosage0);

      if(!isNaN(rm_dosage)){
        document.getElementById("hidResinDosage"+i).value = rm_dosage.toFixed(5);
      }
      // alert(rm_dosage);

    }


      var hid_rm_dosage = parseFloat(hidFormulaPHR) / parseFloat(hidFormulaPHR0) * parseFloat(hidResinDosage0);
      if(!isNaN(hid_rm_dosage)){
        document.getElementById("ResinDosage"+i).value = hid_rm_dosage.toFixed(5);
      }
      totalDosage = parseFloat(totalDosage) + parseFloat(hid_rm_dosage);

  }//end of for loop
    if(!isNaN(totalPHR)){
      document.getElementById("tdTotalPHR").innerHTML = parseFloat(totalPHR).toFixed(5);
    }
    if(!isNaN(totalPHR)){
      document.getElementById("tdTotalCompute").innerHTML = parseFloat(totalDosage).toFixed(5);
    }

}
/* #############  RESIN CALCULATOR  ############ */
function checkDuplicatePRE(){
  var txtPRENo = document.getElementById("txtPRENo").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("tdPRENo").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/pre_duplicate_error.php?txtPRENo="+txtPRENo,true);
  xmlhttp.send();
}
function checkDuplicatePO(){
  var txtPONum = document.getElementById("txtPONum").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("tdPONo").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/po_duplicate_error.php?txtPONum="+txtPONum,true);
  xmlhttp.send();
}

function cancelBox(id, pre_number){
  swal(
      {
        title: "Are you sure you want to \ncancel PRE# " + pre_number + "?",   
        text: "Enter reason for closure:",   
        type: "input",   
        confirmButtonColor: "#DD6B55", 
        confirmButtonText: "YES", 
        showCancelButton: true,
        closeOnConfirm: false,   
        inputPlaceholder: "Enter reason",   
        showLoaderOnConfirm: true
      },
      function(inputValue){
        if (inputValue === false) {
        	return false;      
        }

        if (inputValue.trim() === "") {     
          swal.showInputError("You need to write something!");    
          return false; 
        }else{
          swal({title: "Processing ...", type: "info"});  
          setTimeout(
            function() {
              window.location.assign('process_cancel_pre.php?id=' + id + '&reason=' + inputValue);
            }
            , 2000
          ); 
        }

      }

    );
}

function closePOBox(id, po_number){
  swal(
      {
        title: "Are you sure you want to \nclose PO# " + po_number + "?",   
        text: "Enter reason for closure:",   
        type: "input",   
        confirmButtonColor: "#DD6B55", 
        confirmButtonText: "YES", 
        showCancelButton: true,
        closeOnConfirm: false,   
        inputPlaceholder: "Enter reason",   
        showLoaderOnConfirm: true
      },
      function(inputValue){
        if (inputValue === false) {
        	return false;      
        }

        if (inputValue.trim() === "") {     
          swal.showInputError("You need to write something!");    
          return false; 
        }else{
          swal({title: "Processing ...", type: "info"});  
          setTimeout(
            function() {
               window.location.assign('process_close_po.php?id=' + id + '&reason=' + inputValue);
            }
            , 3000
          ); 
        }

      }

    );
}

function cancelPOBox(id, po_number){
  // var reason = prompt("Are you sure you want to Cancel PO?\n\nReason:",'');

  // if ( reason == null || reason == '' ){
  //   alert ("Reason is mandatory.\n\nCancellation of PO is not completed.");
  // }else if ( reason != null ){
  //   window.location.assign('process_cancel_po.php?id=' + id + '&reason=' + reason);
  // }
  swal(
      {
        title: "Are you sure you want to \ncancel PO# " + po_number + "?",   
        text: "Enter reason for closure:",   
        type: "input",   
        confirmButtonColor: "#DD6B55", 
        confirmButtonText: "YES", 
        showCancelButton: true,
        closeOnConfirm: false,   
        inputPlaceholder: "Enter reason",   
        showLoaderOnConfirm: true
      },
      function(inputValue){
        if (inputValue === false) {
        	return false;      
        }

        if (inputValue.trim() === "") {     
          swal.showInputError("You need to write something!");    
          return false; 
        }else{
          swal({title: "Processing ...", type: "info"});  
          setTimeout(
            function() {
               window.location.assign('process_cancel_po.php?id=' + id + '&reason=' + inputValue);
            }
            , 3000
          ); 
        }

      }

    );
}

function showPOSupplier(supplier_id){
  var radDivision = document.getElementsByName("radDivision");
  var countDivision = radDivision.length;

  for ( var i = 0; i < countDivision; i++){
    if ( radDivision[i].checked ){
      if ( radDivision[i].value == 'Compounds' ){
        var Division = 'Compounds';
      }else if ( radDivision[i].value == 'Pipes' ){
        var Division = 'Pipes';
      }else if ( radDivision[i].value == 'Corporate' ){
        var Division = 'Corporate';
      }else if ( radDivision[i].value == 'PPR' ){
        var Division = 'PPR';
      }else if ( radDivision[i].value == 'Others' ){
        var Division = 'Others';
      }
    }
  }

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("sltSupplier").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/supplier_dropdown.php?Division=" + Division + "&supplier_id=" + supplier_id,true);
  xmlhttp.send();

}

function showRMType(type_id){

  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");
  var PPR = document.getElementById("PPR");

  if ( Compounds.checked == true ){
    Compounds = 1;
  }else{
    Compounds = 0;
  }
  if ( Pipes.checked == true ){
    Pipes = 1;
  }else{
    Pipes = 0;
  }
  if ( Corporate.checked == true ){
    Corporate = 1;
  }else{
    Corporate = 0;
  }
  if ( PPR.checked == true ){
    PPR = 1;
  }else{
    PPR = 0;
  }

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("sltRMType").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/rm_type_dropdown.php?Compounds=" + Compounds + "&Pipes=" + Pipes + "&Corporate=" + Corporate + "&PPR=" + PPR + "&type_id=" + type_id,true);
  xmlhttp.send();

}

function showDepartmentNew(id, department_id){

  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");
  var PPR = document.getElementById("PPR");

  if ( Compounds.checked == true ){
    Compounds = 1;
  }else{
    Compounds = 0;
  }

  if ( Pipes.checked == true ){
    Pipes = 1;
  }else{
    Pipes = 0;
  }

  if ( Corporate.checked == true ){
    Corporate = 1;
  }else{
    Corporate = 0;
  }

  if ( PPR.checked == true ){
    PPR = 1;
  }else{
    PPR = 0;
  }

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("sltDept1").innerHTML=xmlhttp.responseText;
      document.getElementById("sltDept2").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/department_dropdown_new.php?id=" + id + "&department_id=" + department_id + "&Compounds=" + Compounds + "&Pipes=" + Pipes + "&Corporate=" + Corporate + "&PPR=" + PPR,true);
  xmlhttp.send();

}

function showRoles(){

  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");
  var PPR = document.getElementById("PPR");
  var dept1 = document.getElementById("sltDept1").value;
  var dept2 = document.getElementById("sltDept2").value;

  if ( Compounds.checked == true ){
    Compounds = 1;
  }else{
    Compounds = 0;
  }

  if ( Pipes.checked == true ){
    Pipes = 1;
  }else{
    Pipes = 0;
  }

  if ( Corporate.checked == true ){
    Corporate = 1;
  }else{
    Corporate = 0;
  }

  if ( PPR.checked == true ){
    PPR = 1;
  }else{
    PPR = 0;
  }

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdRoles").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/roles_dropdown.php?dept1=" + dept1 + "&dept2=" + dept2 + "&Compounds=" + Compounds + "&Pipes=" + Pipes + "&Corporate=" + Corporate + "&PPR=" + PPR,true);
  xmlhttp.send();

}

function showAddForecastItems(count){
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("tbAddForecastItems").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/show_extra_forecast_item.php?count=" + count, true);
    xmlhttp.send();
}

function showForecastProduct(index){
  var item_type = document.getElementById("hidItemType"+index).value;
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("hidFGID"+index).innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_per_item_type.php?item_type=" + item_type, true);
    xmlhttp.send();
}







// <script type="text/javascript">
// jQuery(window).load(function() {
 
//     $("#nav > li > a").click(function (e) { // binding onclick
//         if ($(this).parent().hasClass('selected')) {
//             $("#nav .selected div div").slideUp(100); // hiding popups
//             $("#nav .selected").removeClass("selected");
//         } else {
//             $("#nav .selected div div").slideUp(100); // hiding popups
//             $("#nav .selected").removeClass("selected");
 
//             if ($(this).next(".subs").length) {
//                 $(this).parent().addClass("selected"); // display popup
//                 $(this).next(".subs").children().slideDown(200);
//             }
//         }
//         e.stopPropagation();
//     });
 
//     $("body").click(function () { // binding onclick to body
//         $("#nav .selected div div").slideUp(100); // hiding popups
//         $("#nav .selected").removeClass("selected");
//     });
 
// });
// </script>