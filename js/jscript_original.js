function defaultValue(){
  document.getElementById("txtOther").readOnly=true;
  document.getElementById("txtCapexNo").readOnly=true;
  document.getElementById("txtCapexName").readOnly=true;
  document.getElementById("txtOtherDiv").readOnly=true;
}

function changetextbox()
{
    if (document.getElementById("chkCapex").checked == true) {
        document.getElementById("txtCapexNo").readOnly=false;
        document.getElementById("txtCapexName").readOnly=false;
    } else {
        document.getElementById("txtCapexNo").readOnly=true;
        document.getElementById("txtCapexName").readOnly=true;
        document.getElementById("txtCapexNo").value='';
        document.getElementById("txtCapexName").value='';
    }
    
    if (document.getElementById("chkOther").checked == true) {
        document.getElementById("txtOther").readOnly=false;
    } else {
        document.getElementById("txtOther").readOnly=true;
        document.getElementById("txtOther").value='';
    }
}

function textOther(){
  var r = document.getElementsByName("radDivision");
  var length = r.length;

  for (var i = 0; i < length; i++) {
      if (r[i].checked) {
        if (r[i].value == 'Others') {
          document.getElementById("txtOtherDiv").readOnly=false;
        } else {
          document.getElementById("txtOtherDiv").readOnly=true;
          document.getElementById("txtOtherDiv").value='';
        }
        // alert(r[i].value);
      }
  }
}

function showSupplies(str, supply)
{
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("supplyDD1").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD2").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD3").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD4").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD5").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD6").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD7").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD8").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD9").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD10").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/supply_dropdown.php?id="+str+"&supply="+supply,true);
    xmlhttp.send();
}
function showPREItem()
{
    var str = document.getElementById('sltSupplyType').value;

    var radDivision = document.getElementsByName("radDivision");
    var countDivision = radDivision.length;

    for ( var i = 0; i < countDivision; i++){
      if ( radDivision[i].checked ){
        if ( radDivision[i].value == 'Compounds' ){
          var Division = 'Compounds';
        }else if ( radDivision[i].value == 'Pipes' ){
          var Division = 'Pipes';
        }else if ( radDivision[i].value == 'Corporate' ){
          var Division = 'Corporate';
        }else if ( radDivision[i].value == 'PPR' ){
          var Division = 'PPR';
        }else if ( radDivision[i].value == 'Others' ){
          var Division = 'Others';
        }
      }
    }
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("supplyDD1").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD2").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD3").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD4").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD5").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD6").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD7").innerHTML=xmlhttp.responseText;
            document.getElementById("supplyDD8").innerHTML=xmlhttp.responseText;
        }
      }//+"&supply="+supply
    // alert(Division);
    xmlhttp.open("GET","include/pre_item_dropdown.php?id="+str+"&Division="+Division,true);
    xmlhttp.send();
}

function showPONum(id)
{
    var s = document.getElementById('s');
    var r = document.getElementById('r');
    var hidPREIDs = document.getElementById('hidPREIDs').value;

    // alert(hidPREIDs);

    var radDivision = document.getElementsByName("radDivision");
    var countDivision = radDivision.length;

    for ( var i = 0; i < countDivision; i++){
      if ( radDivision[i].checked ){
        if ( radDivision[i].value == 'Compounds' ){
          var Division = 'Compounds';
        }else if ( radDivision[i].value == 'Pipes' ){
          var Division = 'Pipes';
        }else if ( radDivision[i].value == 'Corporate' ){
          var Division = 'Corporate';
        }else if ( radDivision[i].value == 'PPR' ){
          var Division = 'PPR';
        }else if ( radDivision[i].value == 'Others' ){
          var Division = 'Others';
        }
      }
    }

   if ( s.checked == true )
    {
      var item_type = s.value;
    }
    else if ( r.checked == true )
    {
      var item_type = r.value;
    }

    // alert(item_type);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltPRE").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/po_number_dropdown.php?item_type=" + item_type + "&poId=" + id + "&Division=" + Division + "&hidPREIDs=" + hidPREIDs,true);
    xmlhttp.send();

}

function sethidPREIDs(){
  document.getElementById("hidPREIDs").value = null;
}

function showPOItem(id)
{
    var hidPREIDs = document.getElementById('hidPREIDs').value;
    var preIds = document.getElementById('sltPRE');
    var s = document.getElementById('s');
    var r = document.getElementById('r');
    var r = document.getElementById('r');

    var radDivision = document.getElementsByName("radDivision");
    var countDivision = radDivision.length;

    for ( var i = 0; i < countDivision; i++){
      if ( radDivision[i].checked ){
        if ( radDivision[i].value == 'Compounds' ){
          var Division = 'Compounds';
        }else if ( radDivision[i].value == 'Pipes' ){
          var Division = 'Pipes';
        }else if ( radDivision[i].value == 'Corporate' ){
          var Division = 'Corporate';
        }else if ( radDivision[i].value == 'PPR' ){
          var Division = 'PPR';
        }
      }
    }

    if ( s.checked == true )
    {
      var item_type = s.value;
    }
    else if ( r.checked == true )
    {
      var item_type = r.value;
    }

    var selectPRE = new Array();
    var i;
    var count = 0;
    for (i=0; i<preIds.options.length; i++) {
      if (preIds.options[i].selected) {
        selectPRE[count] = preIds.options[i].value;
        count++;
      }
    }
      // var implodedPreIds = selectPRE.join(',');

      if ( hidPREIDs == null || hidPREIDs == "" ) {
        var implodedPreIds = selectPRE.join(',');
      }else{
        var implodedPreIds = hidPREIDs;
      }

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("preItems").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/po_item_dropdown.php?implodedPreIds=" + implodedPreIds + "&poId=" + id + "&item_type=" + item_type + "&Division=" + Division,true);
    xmlhttp.send();

}

function showDept(str, dept)
{
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltIssTo").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/dept_dropdown.php?deptid="+str+"&dept="+dept,true);
    xmlhttp.send();
}
function showUser(str, user) // ################ add division
{
    // var radDivision = document.getElementById("radDivision").value;
    // var Division = radDivision.toLowerCase();
    var r = document.getElementsByName("radDivision");
    var length = r.length;

    for (var i = 0; i < length; i++) {
      if (r[i].checked) {
        if ( r[i].value == 'Compounds' ){
          var Division = 'compounds';
        }else if ( r[i].value == 'Pipes' ){
          var Division = 'pipes';
        }else if ( r[i].value == 'Corporate' ){
          var Division = 'corporate';
        }else if ( r[i].value == 'PPR' ){
          var Division = 'ppr';
        }else if ( r[i].value == 'Others' ){
          var Division = 'Others';
        }
      }
    }
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltIssToUser").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/user_dropdown.php?userid="+str+"&user="+user+"&Division="+Division,true);
    xmlhttp.send();
}

function showFormulaType(str, fg_type, bat_id)
{
  // var sltActivity = document.getElementById('sltActivity').value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltFormulaType").innerHTML=xmlhttp.responseText;
        }
      }
    // xmlhttp.open("GET","include/formula_type_dropdown.php?fg_id="+str+"&fg_type="+fg_type+"&sltActivity="+sltActivity+"&bat_id="+bat_id,true);
    xmlhttp.open("GET","include/formula_type_dropdown.php?fg_id="+str+"&fg_type="+fg_type+"&bat_id="+bat_id,true);
    xmlhttp.send();
}

function showFormula(str, trial)
{
  var NRM = document.getElementById('NRM');
  var JRD = document.getElementById('JRD');
  var sltActivity = document.getElementById('sltActivity').value;
// alert(sltActivity);
  if ( NRM.checked == true ){
    NRM = 1;
  }else{
    NRM = 2;
  }

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("standard_formula").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/standard_formula.php?formula_type="+str+"&trial="+trial+"&sltActivity="+sltActivity,true);
    xmlhttp.send();
}

function autoMultiply(){
  var multiplier = document.getElementById("txtMultiplier");
  var phr = document.getElementById("txtNPHR");

  if ( document.getElementById('txtNPHR').disabled == true ){
    var result = 0 * phr.value;
  }else{
    var result = multiplier.value * phr.value;
  }
  
  if(!isNaN(result)){
      document.getElementById('txtNQuantity').value = result.toFixed(4);
  }//end of 1st if

  var i
  var phr_count = document.getElementsByName('txtPhr[]').length;
  var results = new Array();
  var phrs = new Array();
  var qty = new Array();

  for (i = 0; i < phr_count; i++) {
    phrs[i] = document.getElementById('txtPhr'+i).value;

    if ( document.getElementById('txtPhr'+i).disabled == true ){
      results[i] = 0 * parseFloat(phrs[i]);
    }else{
      results[i] = parseFloat(multiplier.value) * parseFloat(phrs[i]);
    }

    if(!isNaN(results[i])){
        document.getElementById('txtQty'+i).value = results[i].toFixed(4);
    }//end of 2nd if

  }//end of for loop

}//end of functin autoMultiply()

function autoSum(){
  var qty_count = document.getElementsByName('txtQty[]').length;
  var nqty = document.getElementById('txtNQuantity').value;
  var qty = new Array();
  var total = 0;

  for (i = 0; i < qty_count; i++) {
    qty[i] = document.getElementById('txtQty'+i).value;
    total = parseFloat(total) + parseFloat(qty[i]);
  }//end of for loop

  total = parseFloat(total) + parseFloat(nqty);
  total = parseFloat(total);

  if(!isNaN(total)){
      document.getElementById('txtTotalQty').value = total.toFixed(4);
  }//end of if

}//end of functin autoSum()

function exclude(){
  var chk_count = document.getElementsByName('chkExclude[]').length;
  var chk_val = new Array();
  var phr_val = new Array();
  var total = document.getElementById('txtTotalQty').value;
  var sltActivity = document.getElementById('sltActivity').value;

  for (i = 0; i < chk_count; i++) {
    chk_val[i] = document.getElementById('chkExclude'+i);
    phr_val[i] = document.getElementById('hidPhr'+i).value;

    if ( chk_val[i].checked == true ){
      if ( sltActivity != 5 ){
        document.getElementById('txtRM'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtPhr'+i).value = 0.0000;
      }
      else{
        // document.getElementById('txtRM'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtPhr'+i).readOnly = true;
        document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtQty'+i).readOnly = true;
        document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;border:2px #ede4c9;border-style: groove;resize: none;';
        document.getElementById('txtPhr'+i).value = 0.0000;
      }
    }//end of if
    else{
      if ( sltActivity != 5 ){
        document.getElementById('txtRM'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtPhr'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtQty'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtPhr'+i).value = 0;
        document.getElementById('txtPhr'+i).value = phr_val[i];
      }else{
        // document.getElementById('txtRM'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtPhr'+i).readOnly = false;
        document.getElementById('txtPhr'+i).style.cssText = 'background: #ffffff;';
        document.getElementById('txtQty'+i).readOnly = true;
        document.getElementById('txtQty'+i).style.cssText = 'background: -webkit-linear-gradient(#f0f0f0, #e1e1e1);background: -moz-linear-gradient(#f0f0f0, #e1e1e1);background: -ms-linear-gradient(#f0f0f0, #e1e1e1);color:#1e1e1e !important;border:2px #e1e1e1;border-style: ridge;';
        document.getElementById('txtPhr'+i).value = 0;
        document.getElementById('txtPhr'+i).value = phr_val[i];
      }
    }//end of else

  }//end of for loop

}//end of functin exclude()

function showCode(str)
{
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("code").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/code.php?code="+str,true);
    xmlhttp.send();
}

function changeCheckBox()
{
  var chkExclude = document.getElementsByName("chkExclude[]");
  var clength = chkExclude.length;
  var PreQual = document.getElementById("PreQual");
  var SamplePrep = document.getElementById("SamplePrep");

  if ( PreQual.checked == true ){
     for (var i = 0; i < clength; i++) {
      document.getElementById("chkExclude"+i).disabled=true;
      document.getElementById("chkExclude"+i).checked=false;
      // document.getElementById('txtRM'+i).disabled = false;
      // document.getElementById('txtPhr'+i).disabled = false;
      // document.getElementById('txtQty'+i).disabled = false;
      document.getElementById('txtRM'+i).readOnly = true;
      document.getElementById('txtPhr'+i).readOnly = true;
      document.getElementById('txtQty'+i).readOnly = true;
     }
     document.getElementById("txtNewRm").disabled=true;
     document.getElementById("txtNPHR").disabled=true;
     document.getElementById("txtNQuantity").disabled=true;
     document.getElementById("txtNQuantity").readOnly=false;
  }else if ( SamplePrep.checked == true ){
     for (var i = 0; i < clength; i++) {
      document.getElementById("chkExclude"+i).disabled=false;
     }
     document.getElementById("txtNewRm").disabled=false;
     document.getElementById("txtNPHR").disabled=false;
     document.getElementById("txtNQuantity").disabled=false;
     document.getElementById("txtNQuantity").readOnly=true;
  }

}

function changeStatus(){
  var chkStatus = document.getElementsByName("Status[]");
  var slength = chkStatus.length;
  var chk_val = new Array();
  // alert(slength);

  for ( var i = 0; i < slength; i++) {
    chk_val[i] = document.getElementById('chkStatus'+i);

    if ( chk_val[i].checked == true ){
      document.getElementById('txtDateConduct'+i).disabled = false;
      document.getElementById('txtConductBy'+i).disabled = false;
      document.getElementById('txtRemarks'+i).disabled = false;
      document.getElementById('btnUpdate'+i).disabled = false;
      // document.getElementById('btnCancel'+i).disabled = false;
      document.getElementById('hidScheduleId'+i).disabled = false;
      document.getElementById('hidEquipmentId'+i).disabled = false;
      document.getElementById('hidScheduleType'+i).disabled = false;
      document.getElementById('hidSchedule'+i).disabled = false;
      document.getElementById('hidCreatedAt'+i).disabled = false;
      document.getElementById('hidCreatedId'+i).disabled = false;
    }//end of if
    else{
      document.getElementById('txtDateConduct'+i).disabled = true;
      document.getElementById('txtConductBy'+i).disabled = true;
      document.getElementById('txtRemarks'+i).disabled = true;
      document.getElementById('btnUpdate'+i).disabled = true;
      // document.getElementById('btnCancel'+i).disabled = true;
      document.getElementById('hidScheduleId'+i).disabled = true;
      document.getElementById('hidEquipmentId'+i).disabled = true;
      document.getElementById('hidScheduleType'+i).disabled = true;
      document.getElementById('hidSchedule'+i).disabled = true;
      document.getElementById('hidCreatedAt'+i).disabled = true;
      document.getElementById('hidCreatedId'+i).disabled = true;
      document.getElementById('txtDateConduct'+i).value = '';
      document.getElementById('txtConductBy'+i).value = '';
      document.getElementById('txtRemarks'+i).value = '';
    }//end of else

  }//end of for loop
  
}

function showPoints()
{ 
    var id = document.getElementById("sltArea").value;
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("sltEntryPoint").innerHTML=xmlhttp.responseText;
            // document.getElementById("sltExitPoint").innerHTML=xmlhttp.responseText;
        }
      } //+"&ent_val="+ent_val
    xmlhttp.open("GET","include/entry_point_dropdown.php?id="+id,true);
    xmlhttp.send();
}

function disableOption(ent_val, ext_val)
{ 
    var id = document.getElementById("sltArea").value;

    // alert(ext_val);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("sltExitPoint").innerHTML=xmlhttp.responseText;
        }
      }//+ "&ext_val=" + ext_val
    xmlhttp.open("GET","include/exit_point_dropdown.php?ent_val=" + ent_val + "&id=" + id ,true);
    xmlhttp.send();
}

// function checkTime(){
//   var txtDeparture = document.getElementById('txtDeparture').value;
//   var txtArrival = document.getElementById('txtArrival').value;
//   var errorTime = document.getElementById('errorTime');

//   if (txtArrival <= txtDeparture) {
//     errorTime.value = 'Arrival time cannot be less than Departure time.';
//   }else{
//     errorTime.value = '';
//   }

// } // end function checkTime

// function checkReading(){
//   var txtKMReadingIn = document.getElementById('txtKMReadingIn').value;
//   var txtKMReadingOut = document.getElementById('txtKMReadingOut').value;
//   var errorKM = document.getElementById('errorKM');

//   if (txtKMReadingOut <= txtKMReadingIn) {
//     errorKM.value = 'KM Reading (Out) cannot be less than KM Reading (In).';
//   }else{
//     errorKM.value = '';
//   }

// } // end function checkReading

function showToll(matrix_id, index)
{ 

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            // for ( var i = 0; i < 8; i++){
            //   if ( index == i ){
               document.getElementById("txtTollfee"+index).innerHTML=xmlhttp.responseText;
            //   }
            // }
            // document.getElementById("sltExitPoint").innerHTML=xmlhttp.responseText;
        }
      }
      
      xmlhttp.open("GET","include/show_toll.php?matrix_id="+matrix_id+"&index="+index,true);
      xmlhttp.send();
}

function autoSummation(){
  var toll_count = document.getElementsByName('txtToll[]').length;
  var toll = new Array();
  var total = 0;

  for (i = 0; i < toll_count; i++) {
    toll[i] = document.getElementById('txtToll'+i).value;
    total = parseFloat(total) + parseFloat(toll[i]);
  }//end of for loop
  
  if(!isNaN(total)){
      document.getElementById('txtTotal').value = total.toFixed(2);
  }//end of if

}

function showMIRSNo()
{
    var date = document.getElementById('txtDate').value;
    // alert(id);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltMIRNo").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/mirno_dropdown.php?date="+date,true);
    xmlhttp.send();
}

function showRM(rm_type, index){
  var txtRM_type_id = document.getElementById('txtRM_type_id');

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("txtRM_id"+index).innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/rm_bat_dropdown.php?rm_type=" + rm_type + "&index=" + index,true);
    xmlhttp.send();
}

function showRMItem()
{
    var rm = document.getElementById('sltMIRNo').value;
    // alert(rm);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltRMItem").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/rm_dropdown.php?rm="+rm,true);
    xmlhttp.send();
}

function showDSQ(id)
{
    var date = document.getElementById('txtDate').value;
    // var hidLMRId = document.getElementById('hidLMRId').value;
    // alert(date);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltDSQ").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/dsq_dropdown.php?date="+date+"&id="+id,true);
    xmlhttp.send();
}

function showDSQa(id)
{
    var date = document.getElementById('txtDate').value;
    // var hidLMRId = document.getElementById('hidLMRId').value;
    // alert(date);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltDSQ").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/dsq_a_dropdown.php?date="+date+"&id="+id,true);
    xmlhttp.send();
}

function showTollPoints(vclass)
{
  // alert(vclass);
    var vlength = vclass.length;
    var vpos = vclass.indexOf('-') + 1;
    var v = vclass.substr(vpos, vlength);
    // alert(v);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltEntryExitPt0").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt1").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt2").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt3").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt4").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt5").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt6").innerHTML=xmlhttp.responseText;
        document.getElementById("sltEntryExitPt7").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/entry_exit_dropdown.php?class=" + v,true);
    xmlhttp.send();

}

function include()
{
  var chk_count = document.getElementsByName('chkInclude[]').length;
  var chk_val = new Array();
  // var quantity = new Array();

  for (i = 0; i < chk_count; i++) {
    chk_val[i] = document.getElementById('chkInclude'+i);
    // quantity[i] = document.getElementById('hidQuantity'+i).value;

    if ( chk_val[i].checked == true ){
      document.getElementById('hidPREId'+i).disabled = false;
      document.getElementById('hidPREItemId'+i).disabled = false;
      document.getElementById('hidQuantity'+i).disabled = false;
      // document.getElementById('hidQty'+i).value = quantity[i];
      document.getElementById('hidUOMId'+i).disabled = false;
      document.getElementById('hidSupplyId'+i).disabled = false;
      document.getElementById('txtUnitPrice'+i).disabled = false;
      document.getElementById('txtTotalAmount'+i).disabled = false;
      document.getElementById('txtTotalAmount'+i).readOnly = true;
    }
     else{
      document.getElementById('hidPREId'+i).disabled = true;
      document.getElementById('hidPREItemId'+i).disabled = true;
      document.getElementById('hidQuantity'+i).disabled = true;
      // document.getElementById('hidQty'+i).value = 0.00;
      document.getElementById('hidUOMId'+i).disabled = true;
      document.getElementById('hidSupplyId'+i).disabled = true;
      document.getElementById('txtUnitPrice'+i).disabled = true;
      document.getElementById('txtTotalAmount'+i).disabled = true;
      document.getElementById('txtTotalAmount'+i).readOnly = false;
    }

  }//end of for loop

}//end of function 

function autoProduct()
{
  var qty_count = document.getElementsByName('hidQuantity[]').length;
  var quantity = new Array();
  // var qty = new Array();
  var unit_price = new Array();
  var product = new Array();

  for ( var i = 0; i < qty_count; i++ ) {

    unit_price[i] = document.getElementById('txtUnitPrice'+i).value;
    quantity[i] = document.getElementById('hidQuantity'+i).value;
    // qty[i] = document.getElementById('hidQty'+i).value;

    if ( document.getElementById('hidQuantity'+i).disabled == true ){
      product[i] = 0 * parseFloat(unit_price[i]);
    }else{
      product[i] = parseFloat(quantity[i]) * parseFloat(unit_price[i]);
    }

    if(!isNaN(product[i])){
        document.getElementById('txtTotalAmount'+i).value = product[i].toFixed(4);
    }//end of 2nd if

  }

}

function autoAdd()
{
  var amount_count = document.getElementsByName('txtTotalAmount[]').length;
  var amount = new Array();
  var sum = 0;

  for ( var i = 0; i < amount_count; i++ ) {

    amount[i] = +document.getElementById('txtTotalAmount'+i).value;

    sum = parseFloat(sum) + parseFloat(amount[i]);

  }//end of for loop

  if(!isNaN(sum)){

      document.getElementById('txtTotalPrice').value = sum.toFixed(4);

  }

}

function editable()
{
  if ( document.getElementById('chkEdit').checked == true )
  {
    document.getElementById('txtMixParam1').readOnly = false;
    document.getElementById('txtMixSequence1').readOnly = false;
    document.getElementById('txtMixParam2').readOnly = false;
    document.getElementById('txtMixSequence2').readOnly = false;
    document.getElementById('txtMixParam3').readOnly = false;
    document.getElementById('txtMixSequence3').readOnly = false;
    document.getElementById('txtMixParam4').readOnly = false;
    document.getElementById('tztMixSequence4').readOnly = false;
    document.getElementById('txtMixParam5').readOnly = false;
    document.getElementById('txtMixSequence5').readOnly = false;
  }
  else
  {
    document.getElementById('txtMixParam1').readOnly = true;
    document.getElementById('txtMixSequence1').readOnly = true;
    document.getElementById('txtMixParam2').readOnly = true;
    document.getElementById('txtMixSequence2').readOnly = true;
    document.getElementById('txtMixParam3').readOnly = true;
    document.getElementById('txtMixSequence3').readOnly = true;
    document.getElementById('txtMixParam4').readOnly = true;
    document.getElementById('tztMixSequence4').readOnly = true;
    document.getElementById('txtMixParam5').readOnly = true;
    document.getElementById('txtMixSequence5').readOnly = true;
  }

}


function showFG(fg, batID)
{
  var soft = document.getElementById('Soft');
  var rigid = document.getElementById('Rigid');
  var sltActivity = document.getElementById('sltActivity').value;
  var txtExtDarNo = document.getElementById('txtExtDarNo');
  var txtMixDarNo = document.getElementById('txtMixDarNo');
  var txtExtZ1 = document.getElementById('txtExtZ1');
  var txtMixParam1 = document.getElementById('txtMixParam1');
  var txtMixSequence1 = document.getElementById('txtMixSequence1');
  var txtExtZ2 = document.getElementById('txtExtZ2');
  var txtMixParm2 = document.getElementById('txtMixParm2');
  var txtMixSequence2 = document.getElementById('txtMixSequence2');
  var txtExtDH = document.getElementById('txtExtDH');
  var txtMixParam3 = document.getElementById('txtMixParam3');
  var txtMixSequence3 = document.getElementById('txtMixSequence3');
  var txtExtScrewSpeed = document.getElementById('txtExtScrewSpeed');
  var txtMixParam4 = document.getElementById('txtMixParam4');
  var tztMixSequence4 = document.getElementById('tztMixSequence4');
  var txtExtCutterSpeed = document.getElementById('txtExtCutterSpeed');
  var txtMixParam5 = document.getElementById('txtMixParam5');
  var txtSequence5 = document.getElementById('txtSequence5');

  if ( soft.checked == true )
  {
    txtExtDarNo.readOnly = false;
    txtExtZ1.readOnly = false;
    txtExtZ2.readOnly = false;
    txtExtDH.readOnly = false;
    txtExtScrewSpeed.readOnly = false;
    txtExtCutterSpeed.readOnly = false;
    var soft_pvc = 1;
  }
  else if ( rigid.checked == true )
  {
    txtExtDarNo.readOnly = true;
    txtExtZ1.readOnly = true;
    txtExtZ2.readOnly = true;
    txtExtDH.readOnly = true;
    txtExtScrewSpeed.readOnly = true;
    txtExtCutterSpeed.readOnly = true;
    var soft_pvc = 0;
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltFG").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/fg_dropdown.php?soft_pvc=" + soft_pvc + "&fg=" + fg + "&batID=" + batID + "&sltActivity=" + sltActivity,true);
    xmlhttp.send();

}

function showCOQ(fgid)
{
  var lotnumber = document.getElementById('sltLotNum').value;
  // alert(lotnumber);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("coqdd").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/coq_dropdown.php?fgid=" + fgid + "&lotnumber=" + lotnumber,true);
    xmlhttp.send();

}

function showPersonnel()
{
  var sltDSQ = document.getElementById('sltDSQ').value;
  var dpos = sltDSQ.indexOf('-');
  var dsq = sltDSQ.substr(0, dpos);
  // alert(dsq);

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("hidPersonnel").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/personnel_dropdown.php?dsqid=" + dsq,true);
    xmlhttp.send();

}

function showMachine()
{
  var mixer = document.getElementById('M');
  var extruder = document.getElementById('E');

  if ( mixer.checked == true )
  {
    // alert(mixer.value);
    var machine_type = mixer.value;
  }
  else if ( extruder.checked == true )
  {
    // alert(extruder.value);
    var machine_type = extruder.value;
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("MachineLines").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/machine_dropdown.php?machine_type=" + machine_type, true);
    xmlhttp.send();

}

function showMBTypeCode()
{
  var mb_id = document.getElementById('sltMasterBatch').value;

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("mb_type_code").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/mb_type_code_dropdown.php?mb_id=" + mb_id, true);
    xmlhttp.send();

}

function showFGSales(sltItemType, fg_id, index)
{
  var hidSalesID = document.getElementById('hidSalesID');
  var sltProductLen = document.getElementsByName('sltProduct[]').length;
  // var sltItemType = document.getElementById('sltItemType').value;
  var sltCustomer = document.getElementById('sltCustomer').value;

// alert(sltItemType);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          // for ( var i = 0; i < sltProductLen; i++ ){
           document.getElementById("tdProduct"+index).innerHTML=xmlhttp.responseText;
          // }
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_sales.php?hidSalesID=" + hidSalesID + "&fg_id=" + fg_id + "&sltCustomer=" + sltCustomer + "&sltItemType=" + sltItemType + "&index=" + index, true);
    xmlhttp.send();

}

function showFGItemType(item_type)
{
  var hidSalesID = document.getElementById('hidSalesID');
  var sltItemTypeLen = document.getElementsByName('sltItemType[]').length;
  var sltCustomer = document.getElementById('sltCustomer').value;

// alert(sltItemType);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          for ( var i = 0; i < sltItemTypeLen; i++ ){
           document.getElementById("sltItemType"+i).innerHTML=xmlhttp.responseText;
          }
        }
      }
    xmlhttp.open("GET","include/fg_item_type.php?hidSalesID=" + hidSalesID + "&item_type=" + item_type, true);
    xmlhttp.send();

}

function showFGSales1(sltItemType, fg_id)
{
  var hidSalesID = document.getElementById('hidSalesID');
  var sltProductLen = document.getElementsByName('sltProduct[]').length;
  // var sltItemType = document.getElementById('sltItemType').value;
  var sltCustomer = document.getElementById('sltCustomer').value;

// alert(sltItemType);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          for ( var i = 0; i < sltProductLen; i++ ){
           document.getElementById("sltProduct"+i).innerHTML=xmlhttp.responseText;
          }
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_sales_1.php?hidSalesID=" + hidSalesID + "&fg_id=" + fg_id + "&sltCustomer=" + sltCustomer + "&sltItemType=" + sltItemType, true);
    xmlhttp.send();

}

function showExtraItems(tag)
{
  var hidSalesID = document.getElementById('hidSalesID');
  var sltProductLen = document.getElementsByName('sltProduct[]').length;
  var sltCustomer = document.getElementById('sltCustomer').value;
  var sltItemTypeLen = document.getElementsByName('sltItemType[]').length;

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("extraItem").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/show_extra_item.php?tag = " + tag, true);
    xmlhttp.send();

}



function closedBox(id, type, number){
    var closed = document.getElementsByName('closed[]').length;
    var array_closed = new Array();
    // alert(closed);
    for ( var i = 0; i < closed; i++ ) {

      array_closed[i] = document.getElementById('closed'+i);

        if ( array_closed[i].checked == true ){

          if ( array_closed[i].disabled == false ) {

            var reason = prompt("Are you sure you want to close " + type+ " " + number + "?\n\nReason:",'');

            if ( reason == null || reason == '' ){
              alert ("Reason is mandatory.\n\n Closing not complete.");
              array_closed[i].checked  = false;
            }else if ( reason != null ){
              window.location.assign('process_close_so.php?id=' + id + '&reason=' + reason);
              // alert ( 'yes' );
            }
            
          }

        }
    }
}

function showFG_Forecast(fg_id, forecast_id)
{
  // alert(fg);
  var radLocal = document.getElementById('radLocal');
  var radExport = document.getElementById('radExport');
  var sltFGItem = document.getElementsByName('sltFGItem[]').length;

  if ( radLocal.checked == true ){
    fg_type = 1;
  }else if ( radExport.checked == true ){
    fg_type = 0;
  }

  // alert(fg_type);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          for ( var i = 0; i < sltFGItem; i++ ){
            document.getElementById("sltFGItem"+i).innerHTML=xmlhttp.responseText;
          }
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_forecast.php?fg_type=" + fg_type + "&fg_id=" + fg_id + "&forecast_id=" + forecast_id, true);
    xmlhttp.send();

}

function addExtraSpecs(count)
{
  // var hidSalesID = document.getElementById('hidSalesID');
  // var sltProductLen = document.getElementsByName('sltProduct[]').length;
  // var sltCustomer = document.getElementById('sltCustomer').value;
  // var sltItemTypeLen = document.getElementsByName('sltItemType[]').length;

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("AddSpecs").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/show_extra_specs.php?count=" + count, true);
    xmlhttp.send();

}

function showReceivedFrom(sltReceivedType, addInventoryID, receivedFromID)
{
  // alert(sltReceivedType);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltReceivedFrom").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/received_from_dropdown.php?received_type=" + sltReceivedType + "&receivedFromID=" + receivedFromID + "&addInventoryID=" + addInventoryID,true);
    xmlhttp.send();

}

function showAsset(addInventoryID, index)
{
  var sltAssetType = document.getElementById('sltAssetType'+index).value;
  // alert(sltAssetType);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltAsset"+index).innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset1").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset2").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset3").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset4").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset5").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset6").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset7").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset8").innerHTML=xmlhttp.responseText;
          // document.getElementById("sltAsset9").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/asset_dropdown.php?asset_type=" + sltAssetType + "&addInventoryID=" + addInventoryID,true);
    xmlhttp.send();

}

function showAsset2(addInventoryID, index)
{
  var sltAssetType = document.getElementById('sltAssetType'+index).value;
  // alert(sltAssetType);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltAsset"+index).innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/asset_dropdown.php?asset_type=" + sltAssetType + "&addInventoryID=" + addInventoryID,true);
    xmlhttp.send();

}

function showAssetSpecification(Asset, addInventoryID, index)
{
  var hidInventoryType = document.getElementById('hidInventoryType').value;
  // var sltAssetCondition = document.getElementById('sltAssetCondition'+index).value;
  // alert(hidInventoryType);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
    else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltAssetSpecification"+index).innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/asset_specification_dropdown.php?asset=" + Asset + "&addInventoryID=" + addInventoryID + "&inventory_type=" + hidInventoryType,true);
    xmlhttp.send();

}

function disablePTSNo(){

  var sltInventorySource = document.getElementById('sltInventorySource').value;

  if ( sltInventorySource == 'NonPTS' ){
    document.getElementById("txtPTSNo").readOnly= true;
  }else{
    document.getElementById("txtPTSNo").readOnly= false;
  }

}

function showReceivedType(addInventoryID, received_type){

  var sltInventorySource = document.getElementById("sltInventorySource").value;

  if ( sltInventorySource != 'Others' ){
    document.getElementById("txtOtherSource").readOnly= true;
  }else{
    document.getElementById("txtOtherSource").readOnly= false;
  }

    var xmlhttp; 
      if (window.XMLHttpRequest)
        {
          xmlhttp=new XMLHttpRequest();
        }
      else
        {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
      xmlhttp.onreadystatechange=function()
        {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
          {
            document.getElementById("sltReceivedType").innerHTML=xmlhttp.responseText;
          }
        }
      xmlhttp.open("GET","include/received_type_dropdown.php?sltInventorySource=" + sltInventorySource + "&received_type=" + received_type + "&addInventoryID=" + addInventoryID,true);
      xmlhttp.send();
}

function enableRefRR(){
  var sltInventorySource = document.getElementById("sltInventorySource").value;
  var pts_count = document.getElementsByName("txtRefRRNumber[]").length;

  for ( var i = 0; i < pts_count; i++ ){
    if ( sltInventorySource == "PRE" ){
      document.getElementById("txtRefRRNumber"+i).readOnly=false;
    } else{
      document.getElementById("txtRefRRNumber"+i).readOnly=true;
    }
  }
}

function showPTSReceiver(addInventoryID, userID){

  var sltToDepartment = document.getElementById("sltToDepartment").value;
  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");

  if ( Compounds.checked == true ){
    division = 'compounds';
  }else if ( Pipes.checked == true ){
    division = 'pipes';
  }else if ( Corporate.checked == true ){
    division = 'corporate';
  }
  // alert(sltToDepartment);
    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltToUser").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/pts_receiver_dropdown.php?sltToDepartment=" + sltToDepartment + "&addInventoryID=" + addInventoryID + "&division=" + division + "&userID=" + userID,true);
      xmlhttp.send();

}

function showCCRFGDropdown(ccr_id, fg_id){

  var sltCustomer = document.getElementById("sltCustomer").value;
  // alert(sltCustomer);
    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltProduct").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/fg_dropdown_ccr.php?sltCustomer=" + sltCustomer + "&fg_id=" + fg_id,true);
      xmlhttp.send();

}

function enableOtherComplaint(){

  var sltComplaintType = document.getElementById('sltComplaintType').value;
  
  if ( sltComplaintType == 4 ){
    document.getElementById("txtOtherComplaintType").readOnly= false;
  }else{
    document.getElementById("txtOtherComplaintType").readOnly= true;
  }

}

function disabledInvalid(){

  var chkInvalid = document.getElementById('chkInvalid');
  
  if ( chkInvalid.checked == true ){
    document.getElementById("txtRootCause").readOnly= true;
    document.getElementById("TechnicalRoot").disabled= true;
    document.getElementById("PAMRoot").disabled= true;
    document.getElementById("WPDRoot").disabled= true;
    document.getElementById("Man").disabled= true;
    document.getElementById("Method").disabled= true;
    document.getElementById("Machine").disabled= true;
    document.getElementById("Material").disabled= true;
    document.getElementById("Measurement").disabled= true;
    document.getElementById("MotherNature").disabled= true;
    document.getElementById("txtCorrective").readOnly= true;
    document.getElementById("txtImplementCorrective").readOnly= true;
    document.getElementById("txtResponsibleCorrective").readOnly= true;
    document.getElementById("txtPreventive").readOnly= true;
    document.getElementById("txtImplementPreventive").readOnly= true;
    document.getElementById("txtResponsiblePreventive").readOnly= true;
    // alert('1');
  }else{
    document.getElementById("txtRootCause").readOnly= false;
    document.getElementById("TechnicalRoot").disabled= false;
    document.getElementById("PAMRoot").disabled= false;
    document.getElementById("WPDRoot").disabled= false;
    document.getElementById("Man").disabled= false;
    document.getElementById("Method").disabled= false;
    document.getElementById("Machine").disabled= false;
    document.getElementById("Material").disabled= false;
    document.getElementById("Measurement").disabled= false;
    document.getElementById("MotherNature").disabled= false;
    document.getElementById("txtCorrective").readOnly= false;
    document.getElementById("txtImplementCorrective").readOnly= false;
    document.getElementById("txtResponsibleCorrective").readOnly= false;
    document.getElementById("txtPreventive").readOnly= false;
    document.getElementById("txtImplementPreventive").readOnly= false;
    document.getElementById("txtResponsiblePreventive").readOnly= false;
  }

}

function showDepartment(id, department_id){

  var Cmpds = document.getElementById("Cmpds");
  var Pips = document.getElementById("Pips");
  var Corp = document.getElementById("Corp");
  var PPR = document.getElementById("PPR");

  if ( Cmpds.checked == true ){
    division = 'compounds';
  }else if ( Pips.checked == true ){
    division = 'pipes';
  }else if ( Corp.checked == true ){
    division = 'corporate';
  }else if ( PPR.checked == true ){
    division = 'ppr';
  }

  // alert( division );

    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltDept1").innerHTML=xmlhttp.responseText;
          document.getElementById("sltDept2").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();

}


function showCPIARIssuer(id, department_id, user_id) {

  var Compounds = document.getElementById("FromCompounds");
  var Pipes = document.getElementById("FromPipes");
  var Corporate = document.getElementById("FromCorporate");

  if ( Compounds.checked == true ){
    division = 'compounds';
  }else if ( Pipes.checked == true ){
    division = 'pipes';
  }else if ( Corporate.checked == true ){
    division = 'corporate';
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltIssuerID").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/qms_user_dropdown.php?id=" + id+ "&department_id=" + department_id + "&user_id=" + user_id + "&division=" + division ,true);
    xmlhttp.send();
}

function showCPIARReceiver(id, department_id, user_id){

  var Compounds = document.getElementById("ToCompounds");
  var Pipes = document.getElementById("ToPipes");
  var Corporate = document.getElementById("ToCorporate");

  if ( Compounds.checked == true ){
    division = 'compounds';
  }else if ( Pipes.checked == true ){
    division = 'pipes';
  }else if ( Corporate.checked == true ){
    division = 'corporate';
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltReceiverID").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/qms_user_dropdown.php?id=" + id+ "&department_id=" + department_id + "&user_id=" + user_id + "&division=" + division ,true);
    xmlhttp.send();
}

function showCPIARVerifier(id, department_id, user_id) {

  var Compounds = document.getElementById("ToCompounds");
  var Pipes = document.getElementById("ToPipes");
  var Corporate = document.getElementById("ToCorporate");

  if ( Compounds.checked == true ){
    division = 'compounds';
  }else if ( Pipes.checked == true ){
    division = 'pipes';
  }else if ( Corporate.checked == true ){
    division = 'corporate';
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltAuditorID").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/qms_user_dropdown.php?id=" + id+ "&department_id=" + department_id + "&user_id=" + user_id + "&division=" + division ,true);
    xmlhttp.send();
}

function showToDepartment(id, department_id){

  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");
  var PPR = document.getElementById("PPR");

  if ( Compounds.checked == true ){
    var division = 'compounds';
  }else if ( Pipes.checked == true ){
    var division = 'pipes';
  }else if ( Corporate.checked == true ){
    var division = 'corporate';
  }else if ( PPR.checked == true ){
    var division = 'ppr';
  }

  // alert( division );

    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltToDepartment").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();

}

function showPREDepartment(id, department_id){

  var r = document.getElementsByName("radDivision");
  var length = r.length;

  for (var i = 0; i < length; i++) {
      if (r[i].checked) {
        if ( r[i].value == 'Compounds' ){
          var division = 'compounds';
        }else if ( r[i].value == 'Pipes' ){
          var division = 'pipes';
        }else if ( r[i].value == 'Corporate' ){
          var division = 'corporate';
        }else if ( r[i].value == 'PPR' ){
          var division = 'ppr';
        }else if ( r[i].value == 'Others' ){
          var division = 'Others';
        }
      }
  }
    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltDept").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();

}

function showFGSalesForecastAnnual(sltItemType, fg_id)
{
  var hidSalesID = document.getElementById('hidForecastID').value;
  var sltCustomer = document.getElementById('sltCustomer').value;
  // var len = document.getElementsByName("sltFGItem[]").length;

// alert(sltItemType);
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          // for ( var i = 0; i < len; i++ ){
            document.getElementById("sltFGItem").innerHTML=xmlhttp.responseText;
          // }
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_forecast_annual.php?hidSalesID=" + hidSalesID + "&fg_id=" + fg_id + "&sltCustomer=" + sltCustomer + "&sltItemType=" + sltItemType, true);
    xmlhttp.send();

}

function showLCNumber(){

  var sltCustomer = document.getElementById('sltCustomer').value;

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          // for ( var i = 0; i < len; i++ ){
            document.getElementById("sltLCNumber").innerHTML=xmlhttp.responseText;
          // }
        }
      }
    xmlhttp.open("GET","include/lc_number_dropdown.php?sltCustomer=" + sltCustomer, true);
    xmlhttp.send();

}

function showIssDepartment(id, department_id) {

  var Compounds = document.getElementById("FromCompounds");
  var Pipes = document.getElementById("FromPipes");
  var Corporate = document.getElementById("FromCorporate");

  if ( Compounds.checked == true ){
    division = 'compounds';
  }else if ( Pipes.checked == true ){
    division = 'pipes';
  }else if ( Corporate.checked == true ){
    division = 'corporate';
  }

  // alert( division );

    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltIssueDeptID").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();
}

function showRecDepartment(id, department_id){

  var Compounds = document.getElementById("ToCompounds");
  var Pipes = document.getElementById("ToPipes");
  var Corporate = document.getElementById("ToCorporate");

  if ( Compounds.checked == true ){
    division = 'compounds';
  }else if ( Pipes.checked == true ){
    division = 'pipes';
  }else if ( Corporate.checked == true ){
    division = 'corporate';
  }

  // alert( division );

    var xmlhttp; 
      if (window.XMLHttpRequest)
      {
        xmlhttp=new XMLHttpRequest();
      }
      else
      {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltReceiveDeptID").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET","include/department_dropdown.php?id=" + id + "&division=" + division + "&department_id=" + department_id,true);
      xmlhttp.send();
}

function showNRMResults(){

  var sltNRM = document.getElementById("sltNRM").value;

    // alert(sltNRM);

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("NRMResults").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/nrm_results.php?sltNRM=" + sltNRM,true);
    xmlhttp.send();

}

function showJRDNumber()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      jrd = 0;
    }else if ( JRD.checked == true ){
      jrd = 1;
    }
    // alert('nrm '+ nrm + 'jrd '+ jrd );
    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltJRDNumber").innerHTML=xmlhttp.responseText;
      }
    }

        xmlhttp.open("GET","include/jrd_number_dropdown.php?jrd=" + jrd,true);
        xmlhttp.send();
}

function showNRMNumber()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      nrm = 1;
    }else if ( JRD.checked == true ){
      nrm = 0;
    }
    // alert('nrm '+ nrm + 'jrd '+ jrd );
    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltNRMNumber").innerHTML=xmlhttp.responseText;
      }
    }

        xmlhttp.open("GET","include/nrm_number_dropdown.php?nrm=" + nrm,true);
        xmlhttp.send();
}

function showRNDActivity()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      rnd_activity = NRM.value;
    }else if ( JRD.checked == true ){
      rnd_activity = JRD.value;
    }
    // alert(rnd_activity);
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltActivity").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/activity_dropdown.php?rnd_activity=" + rnd_activity,true);
    xmlhttp.send();
}

function enableClassOthers(){
  var radClassOthers = document.getElementById('radClassOthers');
  var radClassResin = document.getElementById('radClassResin');
  var radClassStabilizer = document.getElementById('radClassStabilizer');
  var radClassFillers = document.getElementById('radClassFillers');
  var radClassPlasticizer = document.getElementById('radClassPlasticizer');
  var radClassLubricant = document.getElementById('radClassLubricant');
  var radClassColorant = document.getElementById('radClassColorant');

  if ( radClassOthers.checked == true ) {
    document.getElementById("txtOtherClass").readOnly = false;
  }else if ( radClassOthers.checked == false 
            || radClassResin.checked == true
            || radClassStabilizer.checked == true
            || radClassFillers.checked == true
            || radClassPlasticizer.checked == true
            || radClassLubricant.checked == true
            || radClassColorant.checked == true ){
    document.getElementById("txtOtherClass").readOnly = true;
  }
}

function enableSampleOthers(){
  var radSamplePowder = document.getElementById('radSamplePowder');
  var radSampleLiquid = document.getElementById('radSampleLiquid');
  var radSampleOthers = document.getElementById('radSampleOthers');
  var radSamplePellet = document.getElementById('radSamplePellet');

  if ( radSampleOthers.checked == true ) {
    document.getElementById("txtOtherSample").readOnly = false;
  }else if ( radSampleOthers.checked == false 
            || radSamplePowder.checked == true
            || radSampleLiquid.checked == true
            || radSamplePellet.checked == true){
    document.getElementById("txtOtherSample").readOnly = true;
  }
}

function enableOtherSupplier(){
  var otherSupplier = document.getElementById('otherSupplier');
  var optionLength = document.getElementById('sltSupplier').length;

  if ( otherSupplier.checked == true ) {
    document.getElementById("txtOtherSupplier").readOnly = false;
    for ( var i = 0; i < optionLength; i++ ){
      document.getElementById('optionSupplier'+i).disabled = true;
    }
    // alert(optionLength);
  }else if ( otherSupplier.checked == false ){
    document.getElementById("txtOtherSupplier").readOnly = true;
    for ( var i = 0; i < optionLength; i++ ){
      document.getElementById('optionSupplier'+i).disabled = false;
    }
  }
}

function disabledOptionFG(){
  var sltFGLen = document.getElementById('sltFG').length;
  var sltActivity = document.getElementById('sltActivity').value;

  for ( var i = 0; i < sltFGLen; i++ ){
    if ( sltActivity == 4 || sltActivity == 2 ){
      document.getElementById('optionFG'+i).disabled = true;
    }else{
      document.getElementById('optionFG'+i).disabled = false;
    }
  }

  // alert(sltFGLen);
}

/* ################# CREDIT AND COLLECTIONS ################# */
function enableTerms(){

  var sltTerms = document.getElementById("sltTerms").value;

  if ( sltTerms == 'COD'){
    document.getElementById("txtTerms").readOnly = true;
  }else{
    document.getElementById("txtTerms").readOnly = false;
  }

}

function enableCheck(){

  var sltTransType = document.getElementById("sltTransType").value;

  if ( sltTransType == 'PRC' || sltTransType == 'IPT'){
    document.getElementById("txtInvoiceDate").disabled = true;
    document.getElementById("txtCheckReturnedDate").disabled = false;

  }else{
    document.getElementById("txtInvoiceDate").disabled = false;
    document.getElementById("txtCheckReturnedDate").disabled = true;

  }

  for ( var i = 0; i < 10; i++ ){

    var txtInvoiceNumber = document.getElementById("txtInvoiceNumber"+i);
    var sltBankCodeLength = document.getElementById("sltBankCode"+i).length;
    // var txtChecknumber = document.getElementById("txtChecknumber"+i);

    // alert(sltBankCodeLength);

    if ( sltTransType == 'PRC' || sltTransType == 'IPT'){
      txtInvoiceNumber.disabled = true;

      if ( i == 0 ){
        document.getElementById("txtAmount"+i).disabled = false;
        document.getElementById("txtDiscount"+i).disabled = false;
      }else{
        document.getElementById("txtAmount"+i).disabled = true;
        document.getElementById("txtDiscount"+i).disabled = true;
      }

      document.getElementById("txtChecknumber0").disabled = false;

      for ( var y = 0; y < (sltBankCodeLength-1); y++ ){
        document.getElementById("optionBankCode0"+y).disabled = false;
      }

    }else{
      txtInvoiceNumber.disabled = false;
      document.getElementById("txtAmount"+i).disabled = false;
      document.getElementById("txtDiscount"+i).disabled = false;
      document.getElementById("txtChecknumber"+i).disabled = true;

      for ( var y = 0; y < (sltBankCodeLength-1); y++ ){
        document.getElementById("optionBankCode0"+y).disabled = true;
      }

    }

  }

}

function showInvoices( index, jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber"+index);
  var sltBankCode = document.getElementById("sltBankCode"+index).value;
  var txtChecknumber = document.getElementById("txtChecknumber"+index).value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdInvoiceNumber"+index).innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_invoices.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&index=" + index + "&jacket_id=" + jacket_id,true);
  xmlhttp.send();

}

function showAmounts( index, jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber"+index);
  var sltBankCode = document.getElementById("sltBankCode"+index).value;
  var txtChecknumber = document.getElementById("txtChecknumber"+index).value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdAmount"+index).innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_amounts.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&index="+ index + "&jacket_id=" + jacket_id ,true);
  xmlhttp.send();

}

function showDiscounts( index, jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber"+index);
  var sltBankCode = document.getElementById("sltBankCode"+index).value;
  var txtChecknumber = document.getElementById("txtChecknumber"+index).value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdDiscount"+index).innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_discounts.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&index=" + index + "&jacket_id=" + jacket_id,true);
  xmlhttp.send();

}

function showInvoiceNumber(sltInvoiceNum){

  var sltCustomer = document.getElementById("sltCustomer").value;
  var hidMemoId = document.getElementById("hidMemoId").value;
  var hidProcessType = document.getElementById("hidProcessType").value;
  // var sltInvoiceNum = document.getElementById("sltInvoiceNum").value;
  // var hidDebitId = document.getElementById("hidDebitId").value;

    // alert(hidDebitId);

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltInvoiceNum").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/invoice_number_dropdown.php?sltCustomer=" + sltCustomer + "&sltInvoiceNum=" + sltInvoiceNum + "&hidMemoId=" + hidMemoId + "&hidProcessType=" + hidProcessType,true);
    xmlhttp.send();

}

function showCreditPaymentItems(){

  var sltCustomer = document.getElementById("sltCustomer").value;
  var hidCreditId = document.getElementById("hidCreditId").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("tbleCreditPaymentItems").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/credit_payment_items_dropdown.php?sltCustomer=" + sltCustomer + "&hidCreditId=" + hidCreditId,true);
    xmlhttp.send();

}

function enableCreditItems(index){

  var chkIncludeInvoice = document.getElementById("chkIncludeInvoice"+index);
  var hidInvoiceNumber = document.getElementById("hidInvoiceNumber"+index);
  var txtAmount = document.getElementById("txtAmount"+index);
  var txtDiscount = document.getElementById("txtDiscount"+index);
  var hidCreditAmount = document.getElementById("hidCreditAmount"+index);
  var hidDebitAmount = document.getElementById("hidDebitAmount"+index);
  var hidARAmount = document.getElementById("hidARAmount"+index);
  var hidPaidAmount = document.getElementById("hidARAmount"+index);
  var txtBalance = document.getElementById("txtBalance"+index);

  if ( chkIncludeInvoice.checked == true ){
    hidInvoiceNumber.disabled = false;
    txtAmount.readOnly = false;
    hidCreditAmount.disabled = false;
    hidDebitAmount.disabled = false;
    txtDiscount.readOnly = false;
    hidARAmount.disabled = false;
    hidPaidAmount.disabled = false;
    // txtBalance.readOnly = true;
    // txtBalance.disabled = false;
  }else{
    hidInvoiceNumber.disabled = true;
    txtAmount.readOnly = true;
    hidCreditAmount.disabled = true;
    hidDebitAmount.disabled = true;
    txtDiscount.readOnly = true;
    hidARAmount.disabled = true;
    hidPaidAmount.disabled = true;
    // txtBalance.readOnly = false;
    // txtBalance.disabled = true;
  }

}

function valueForFirstField(checkAmount){

  document.getElementById("txtBalance0").value = checkAmount;
  var InvoiceCount = document.getElementsByName("hidInvoiceNumber[]").length;

  for ( var i = 0; i < InvoiceCount; i++){
    document.getElementById("Payment"+i).value = 0;
  }

}

function computeBalance(index){
  var chkIncludeInvoice = document.getElementById("chkIncludeInvoice"+index);
  var hidARAmount = document.getElementById("hidARAmount"+index).value;
  var hidPaidAmount = document.getElementById("hidPaidAmount"+index).value;
  var hidCreditAmount = document.getElementById("hidCreditAmount"+index).value;
  var hidDebitAmount = document.getElementById("hidDebitAmount"+index).value;
  var txtDiscount = document.getElementById("txtDiscount"+index).value;
  var txtBalance = document.getElementById("txtBalance"+index);

  if ( txtDiscount == '' || txtDiscount == null ){
    txtDiscount = 0;
  }

  if ( chkIncludeInvoice.checked == true ){
    var Balance = parseFloat(hidARAmount) - parseFloat(hidPaidAmount) - parseFloat(hidCreditAmount) + parseFloat(hidDebitAmount) - parseFloat(txtDiscount);

    if ( !isNaN(Balance) ){
      txtBalance.value = parseFloat(Balance).toFixed(2);
      // document.getElementById("txtAmount"+index).value = parseFloat(Balance);
      // .toString().replace(/\B(?=(?=\d*\.)(\d{3})+(?!\d))/g, ',');
    }
  }else{
    var Balance = 0;
    txtBalance.value = '';
    document.getElementById("txtAmount"+index).value='';
  }

}

function computeTotalPayment(){
  var AmountLength = document.getElementsByName("txtAmount[]").length;
  var Payments = new Array();
  var TotalPayment = 0;

  for ( var i = 0; i < AmountLength; i++ ){
    Payments[i] = document.getElementById("txtAmount"+i).value;

    if ( Payments[i] == '' || Payments[i] == null ){
      Payments[i] = 0;
    }

    TotalPayment = parseFloat(Payments[i]) + parseFloat(TotalPayment);

  }

  if ( !isNaN(TotalPayment) ){
    var txtTotalPayment = document.getElementById("txtTotalPayment");
    txtTotalPayment.value = parseFloat(TotalPayment).toFixed(2);
    // .toString().replace(/\B(?=(?=\d*\.)(\d{3})+(?!\d))/g, ',');
  } 

  var txtCheckAmount = document.getElementById("txtCheckAmount").value;

  if ( txtCheckAmount == '' || txtCheckAmount == null ){

    document.getElementById("lblErrorMessage").innerHTML = '* Check amount can\'t be blank.';
    document.getElementById("tdErrorMessage").innerHTML = '* Check amount can\'t be blank.';
    document.getElementById("hidErrorMessage").value = '* Check amount can\'t be blank.';

  }else if ( parseFloat(txtTotalPayment.value).toFixed(2) > parseFloat(txtCheckAmount) ){

    document.getElementById("lblErrorMessage").innerHTML = '* Total payment can\'t be greater than check amount.';
    document.getElementById("hidErrorMessage").value = '* Total payment can\'t be greater than check amount.';

  }else{

    document.getElementById("lblErrorMessage").innerHTML = '';
    document.getElementById("hidErrorMessage").value = '';
    document.getElementById("tdErrorMessage").innerHTML = '';

  }

}

// function validate(form) {
//   var invoice_amount = document.getElementById("txtInvoiceNo").value;
//   var amount = parseFloat(document.getElementById("txtAmount").value);

//   var invoice_length = invoice_amount.length;
//   var invoice_position = invoice_amount.indexOf('-') + 1;
//   var balance = parseFloat(invoice_amount.substr(invoice_position, invoice_length));

//   // alert(amount);

//     if( amount > balance ) {
//       // alert(balance);
//         return confirm("The payment amount is greater than the invoice balance. Are you sure you want to post the payment?");
//         // return false;
//     }

// }

// function disabledOptionBankCode(){
//   var sltFGLen = document.getElementById('sltFG').length;
//   var sltActivity = document.getElementById('sltActivity').value;

//   for ( var i = 0; i < sltFGLen; i++ ){
//     if ( sltActivity == 4 || sltActivity == 2 ){
//       document.getElementById('optionFG'+i).disabled = true;
//     }else{
//       document.getElementById('optionFG'+i).disabled = false;
//     }
//   }

//   // alert(sltFGLen);
// }

/* ################# CREDIT AND COLLECTIONS ################# */

/* #############  RESIN CALCULATOR  ############ */

function showMixerCapacity()
{
  var mixer_id = document.getElementById("sltMixer").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("tdMixerCapacity").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/mixer_capacity.php?mixer_id="+mixer_id,true);
    xmlhttp.send();
}

function showFormulaPHR(formula_type_id)
{
  // var formula_type_id = document.getElementById("sltFormulaType").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("tble10").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/formula_phr.php?formula_type_id="+formula_type_id,true);
    xmlhttp.send();
}


function showRMComputation(){

  var formula_length = document.getElementsByName('hidFormulaPHR[]').length;
  var hidResinDosage0 = document.getElementById('hidResinDosage0').value;
  var hidFormulaPHR0 = document.getElementById('hidFormulaPHR0').value;
  var totalPHR = 0;
  var totalDosage = 0;

  for (var i = 0; i < (formula_length); i++) {
    var hidFormulaPHR = document.getElementById('hidFormulaPHR'+i).value;
    var hidResinDosage = parseFloat(document.getElementById('hidResinDosage'+i).value);

    totalPHR = parseFloat(totalPHR) + parseFloat(hidFormulaPHR);

    if ( i != 0 ){
      rm_dosage = parseFloat(hidFormulaPHR) / parseFloat(hidFormulaPHR0) * parseFloat(hidResinDosage0);

      if(!isNaN(rm_dosage)){
        document.getElementById("hidResinDosage"+i).value = rm_dosage.toFixed(5);
      }
      // alert(rm_dosage);

    }


      var hid_rm_dosage = parseFloat(hidFormulaPHR) / parseFloat(hidFormulaPHR0) * parseFloat(hidResinDosage0);
      if(!isNaN(hid_rm_dosage)){
        document.getElementById("ResinDosage"+i).value = hid_rm_dosage.toFixed(5);
      }
      totalDosage = parseFloat(totalDosage) + parseFloat(hid_rm_dosage);

  }//end of for loop
    if(!isNaN(totalPHR)){
      document.getElementById("tdTotalPHR").innerHTML = parseFloat(totalPHR).toFixed(5);
    }
    if(!isNaN(totalPHR)){
      document.getElementById("tdTotalCompute").innerHTML = parseFloat(totalDosage).toFixed(5);
    }

}
/* #############  RESIN CALCULATOR  ############ */
function checkDuplicatePRE(){
  var txtPRENo = document.getElementById("txtPRENo").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("tdPRENo").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/pre_duplicate_error.php?txtPRENo="+txtPRENo,true);
  xmlhttp.send();
}
function checkDuplicatePO(){
  var txtPONum = document.getElementById("txtPONum").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("tdPONo").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/po_duplicate_error.php?txtPONum="+txtPONum,true);
  xmlhttp.send();
}

function cancelBox(id){
  var reason = prompt("Are you sure you want to Cancel PRE?\n\nReason:",'');

  if ( reason == null || reason == '' ){
    alert ("Reason is mandatory.\n\nCancellation not complete.");
  }else if ( reason != null ){
    window.location.assign('process_cancel_pre.php?id=' + id + '&reason=' + reason);
  }
}

function closePOBox(id){
  var reason = prompt("Are you sure you want to Close PO?\n\nReason:",'');

  if ( reason == null || reason == '' ){
    alert ("Reason is mandatory.\n\nClosing of PO is not completed.");
  }else if ( reason != null ){
    window.location.assign('process_close_po.php?id=' + id + '&reason=' + reason);
  }
}

function cancelPOBox(id){
  var reason = prompt("Are you sure you want to Cancel PO?\n\nReason:",'');

  if ( reason == null || reason == '' ){
    alert ("Reason is mandatory.\n\nCancellation of PO is not completed.");
  }else if ( reason != null ){
    window.location.assign('process_cancel_po.php?id=' + id + '&reason=' + reason);
  }
}

function showPOSupplier(supplier_id){
  var radDivision = document.getElementsByName("radDivision");
  var countDivision = radDivision.length;

  for ( var i = 0; i < countDivision; i++){
    if ( radDivision[i].checked ){
      if ( radDivision[i].value == 'Compounds' ){
        var Division = 'Compounds';
      }else if ( radDivision[i].value == 'Pipes' ){
        var Division = 'Pipes';
      }else if ( radDivision[i].value == 'Corporate' ){
        var Division = 'Corporate';
      }else if ( radDivision[i].value == 'PPR' ){
        var Division = 'PPR';
      }else if ( radDivision[i].value == 'Others' ){
        var Division = 'Others';
      }
    }
  }

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("sltSupplier").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/supplier_dropdown.php?Division=" + Division + "&supplier_id=" + supplier_id,true);
  xmlhttp.send();

}

function showRMType(type_id){

  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");
  var PPR = document.getElementById("PPR");

  if ( Compounds.checked == true ){
    Compounds = 1;
  }else{
    Compounds = 0;
  }
  if ( Pipes.checked == true ){
    Pipes = 1;
  }else{
    Pipes = 0;
  }
  if ( Corporate.checked == true ){
    Corporate = 1;
  }else{
    Corporate = 0;
  }
  if ( PPR.checked == true ){
    PPR = 1;
  }else{
    PPR = 0;
  }

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("sltRMType").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/rm_type_dropdown.php?Compounds=" + Compounds + "&Pipes=" + Pipes + "&Corporate=" + Corporate + "&PPR=" + PPR + "&type_id=" + type_id,true);
  xmlhttp.send();

}

function showDepartmentNew(id, department_id){

  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");
  var PPR = document.getElementById("PPR");

  if ( Compounds.checked == true ){
    Compounds = 1;
  }else{
    Compounds = 0;
  }

  if ( Pipes.checked == true ){
    Pipes = 1;
  }else{
    Pipes = 0;
  }

  if ( Corporate.checked == true ){
    Corporate = 1;
  }else{
    Corporate = 0;
  }

  if ( PPR.checked == true ){
    PPR = 1;
  }else{
    PPR = 0;
  }

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("sltDept1").innerHTML=xmlhttp.responseText;
      document.getElementById("sltDept2").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/department_dropdown_new.php?id=" + id + "&department_id=" + department_id + "&Compounds=" + Compounds + "&Pipes=" + Pipes + "&Corporate=" + Corporate + "&PPR=" + PPR,true);
  xmlhttp.send();

}

function showRoles(){

  var Compounds = document.getElementById("Compounds");
  var Pipes = document.getElementById("Pipes");
  var Corporate = document.getElementById("Corporate");
  var PPR = document.getElementById("PPR");
  var dept1 = document.getElementById("sltDept1").value;
  var dept2 = document.getElementById("sltDept2").value;

  if ( Compounds.checked == true ){
    Compounds = 1;
  }else{
    Compounds = 0;
  }

  if ( Pipes.checked == true ){
    Pipes = 1;
  }else{
    Pipes = 0;
  }

  if ( Corporate.checked == true ){
    Corporate = 1;
  }else{
    Corporate = 0;
  }

  if ( PPR.checked == true ){
    PPR = 1;
  }else{
    PPR = 0;
  }

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdRoles").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/roles_dropdown.php?dept1=" + dept1 + "&dept2=" + dept2 + "&Compounds=" + Compounds + "&Pipes=" + Pipes + "&Corporate=" + Corporate + "&PPR=" + PPR,true);
  xmlhttp.send();

}

function confirmStatus(id, hidIndex){

  // var hidIndex = document.getElementById("hidIndex").value;
  var chkDone = document.getElementById("chkDone"+hidIndex);

  if ( chkDone.checked == true){
    var remarks = prompt("Update status?\n\nAction Taken:",'');

    if ( remarks == null || remarks == '' ){
      alert ("Action taken can't be blank.\n\nUpdate is not completed.");
      chkDone.checked = false;
    }else if ( remarks != null ){
      window.location.assign('process_update_ticket_status.php?id=' + id + '&remarks=' + remarks);
    }
  }
}


function showAddForecastItems(count){
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("tbAddForecastItems").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/show_extra_forecast_item.php?count=" + count, true);
    xmlhttp.send();
}

function showForecastProduct(index){
  var item_type = document.getElementById("hidItemType"+index).value;
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("hidFGID"+index).innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/fg_dropdown_per_item_type.php?item_type=" + item_type, true);
    xmlhttp.send();
}





// <script type="text/javascript">
// jQuery(window).load(function() {
 
//     $("#nav > li > a").click(function (e) { // binding onclick
//         if ($(this).parent().hasClass('selected')) {
//             $("#nav .selected div div").slideUp(100); // hiding popups
//             $("#nav .selected").removeClass("selected");
//         } else {
//             $("#nav .selected div div").slideUp(100); // hiding popups
//             $("#nav .selected").removeClass("selected");
 
//             if ($(this).next(".subs").length) {
//                 $(this).parent().addClass("selected"); // display popup
//                 $(this).next(".subs").children().slideDown(200);
//             }
//         }
//         e.stopPropagation();
//     });
 
//     $("body").click(function () { // binding onclick to body
//         $("#nav .selected div div").slideUp(100); // hiding popups
//         $("#nav .selected").removeClass("selected");
//     });
 
// });
// </script>