function showFormulaType(str, fg_type, bat_id)
{
    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltFormulaType").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/formula_type_dropdown.php?fg_id="+str+"&fg_type="+fg_type+"&bat_id="+bat_id,true);
    xmlhttp.send();
}

function showFormula(str, trial)
{
  var sltActivity = document.getElementById('sltActivity').value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("standard_formula").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/standard_formula.php?formula_type="+str+"&trial="+trial+"&sltActivity="+sltActivity,true);
    xmlhttp.send();
}

function showFG(fg, batID)
{
  var soft = document.getElementById('Soft');
  var rigid = document.getElementById('Rigid');
  var sltActivity = document.getElementById('sltActivity').value;
  var txtExtDarNo = document.getElementById('txtExtDarNo');
  var txtMixDarNo = document.getElementById('txtMixDarNo');
  var txtExtZ1 = document.getElementById('txtExtZ1');
  var txtMixParam1 = document.getElementById('txtMixParam1');
  var txtMixSequence1 = document.getElementById('txtMixSequence1');
  var txtExtZ2 = document.getElementById('txtExtZ2');
  var txtMixParm2 = document.getElementById('txtMixParm2');
  var txtMixSequence2 = document.getElementById('txtMixSequence2');
  var txtExtDH = document.getElementById('txtExtDH');
  var txtMixParam3 = document.getElementById('txtMixParam3');
  var txtMixSequence3 = document.getElementById('txtMixSequence3');
  var txtExtScrewSpeed = document.getElementById('txtExtScrewSpeed');
  var txtMixParam4 = document.getElementById('txtMixParam4');
  var txtMixSequence4 = document.getElementById('txtMixSequence4');
  var txtExtCutterSpeed = document.getElementById('txtExtCutterSpeed');
  var txtMixParam5 = document.getElementById('txtMixParam5');
  var txtSequence5 = document.getElementById('txtSequence5');

  if ( soft.checked == true )
  {
    txtExtDarNo.readOnly = false;
    txtExtZ1.readOnly = false;
    txtExtZ2.readOnly = false;
    txtExtDH.readOnly = false;
    txtExtScrewSpeed.readOnly = false;
    txtExtCutterSpeed.readOnly = false;
    var soft_pvc = 1;
  }
  else if ( rigid.checked == true )
  {
    txtExtDarNo.readOnly = true;
    txtExtZ1.readOnly = true;
    txtExtZ2.readOnly = true;
    txtExtDH.readOnly = true;
    txtExtScrewSpeed.readOnly = true;
    txtExtCutterSpeed.readOnly = true;
    var soft_pvc = 0;
  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltFG").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/fg_dropdown.php?soft_pvc=" + soft_pvc + "&fg=" + fg + "&batID=" + batID + "&sltActivity=" + sltActivity,true);
    xmlhttp.send();

}

function exclude(){
  var chk_count = document.getElementsByName('chkExclude[]').length;
  var chk_val = new Array();
  var phr_val = new Array();
  var total = document.getElementById('txtTotalQty').value;
  var sltActivity = document.getElementById('sltActivity').value;

  for (i = 0; i < chk_count; i++) {
    chk_val[i] = document.getElementById('chkExclude'+i);
    phr_val[i] = document.getElementById('hidPhr'+i).value;

    if ( chk_val[i].checked == true ){
      if ( sltActivity != 5 ){
        document.getElementById('txtRM'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtPhr'+i).value = 0.0000;
      }
      else{
        document.getElementById('txtPhr'+i).readOnly = true;
        document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtQty'+i).readOnly = true;
        document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0; color:#decf9c !important;';
        document.getElementById('txtPhr'+i).value = 0.0000;
      }
    }//end of if
    else{
      if ( sltActivity != 5 ){
        document.getElementById('txtRM'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
        document.getElementById('txtPhr'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
        document.getElementById('txtQty'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
        document.getElementById('txtPhr'+i).value = 0;
        document.getElementById('txtPhr'+i).value = phr_val[i];
      }else{
        document.getElementById('txtPhr'+i).readOnly = false;
        document.getElementById('txtPhr'+i).style.cssText = 'background: #ffffff;';
        document.getElementById('txtQty'+i).readOnly = true;
        document.getElementById('txtQty'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
        document.getElementById('txtPhr'+i).value = 0;
        document.getElementById('txtPhr'+i).value = phr_val[i];
      }
    }//end of else

  }//end of for loop

}//end of functin exclude()

function excludeModify(){
  var chk_count = document.getElementsByName('chkExclude[]').length;
  var chk_val = new Array();
  var phr_val = new Array();
  var total = document.getElementById('txtTotalQty').value;
  var hidActivity = document.getElementById('hidActivity').value;

  for (i = 0; i < chk_count; i++) {
    chk_val[i] = document.getElementById('chkExclude'+i);
    phr_val[i] = document.getElementById('hidPhr'+i).value;

    if ( chk_val[i].checked == true ){
      document.getElementById('txtRM_id'+i).disabled = true;
      document.getElementById('txtRM_id'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;';
      document.getElementById('txtPhr'+i).readOnly = true;
      document.getElementById('txtPhr'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;';
      document.getElementById('txtQty'+i).readOnly = true;
      document.getElementById('txtQty'+i).style.cssText = 'background: #f5efe0;color:#decf9c !important;';
      document.getElementById('txtPhr'+i).value = 0.0000;
    }//end of if
    else{
      document.getElementById('txtRM_id'+i).disabled = false;
      document.getElementById('txtRM_id'+i).style.cssText = 'background: none;';
      document.getElementById('txtPhr'+i).readOnly = false;
      document.getElementById('txtPhr'+i).style.cssText = 'background: #ffffff;';
      document.getElementById('txtQty'+i).readOnly = true;
      document.getElementById('txtQty'+i).style.cssText = 'background: #e1e1e1; color:#1e1e1e !important;';
      document.getElementById('txtPhr'+i).value = 0;
      document.getElementById('txtPhr'+i).value = phr_val[i];
    }//end of else

  }//end of for loop

}//end of functin excludeModify()

function enableRM_ID(){
  var rm_id_length = document.getElementsByName('txtRM_id[]').length;

  for ( var i = 0; i < rm_id_length; i++ ){
    document.getElementById('txtRM_id'+i).disabled = false;
  }

}

function autoMultiply(){
  var multiplier = document.getElementById("txtMultiplier");
  var phr = document.getElementById("txtNPHR");

  if ( document.getElementById('txtNPHR').disabled == true ){
    var result = 0 * phr.value;
  }else{
    var result = multiplier.value * phr.value;
  }
  
  if(!isNaN(result)){
      document.getElementById('txtNQuantity').value = result.toFixed(5);
  }//end of 1st if

  var i
  var phr_count = document.getElementsByName('txtPhr[]').length;
  var results = new Array();
  var phrs = new Array();
  var qty = new Array();

  for (i = 0; i < phr_count; i++) {
    phrs[i] = document.getElementById('txtPhr'+i).value;

    if ( document.getElementById('txtPhr'+i).disabled == true ){
      results[i] = 0 * parseFloat(phrs[i]);
    }else{
      results[i] = parseFloat(multiplier.value) * parseFloat(phrs[i]);
    }

    if(!isNaN(results[i])){
        document.getElementById('txtQty'+i).value = results[i].toFixed(5);
    }//end of 2nd if

  }//end of for loop

}//end of functin autoMultiply()

function autoSum(){
  var qty_count = document.getElementsByName('txtQty[]').length;
  var nqty = document.getElementById('txtNQuantity').value;
  var qty = new Array();
  var total = 0;

  for (i = 0; i < qty_count; i++) {
    qty[i] = document.getElementById('txtQty'+i).value;
    total = parseFloat(total) + parseFloat(qty[i]);
  }//end of for loop

  total = parseFloat(total) + parseFloat(nqty);
  total = parseFloat(total);

  if(!isNaN(total)){
      document.getElementById('txtTotalQty').value = total.toFixed(5);
  }//end of if

}//end of functin autoSum()

function changeCheckBox()
{
  var chkExclude = document.getElementsByName("chkExclude[]");
  var clength = chkExclude.length;
  var PreQual = document.getElementById("PreQual");
  var SamplePrep = document.getElementById("SamplePrep");

  if ( PreQual.checked == true ){
     for (var i = 0; i < clength; i++) {
      document.getElementById("chkExclude"+i).disabled=true;
      document.getElementById("chkExclude"+i).checked=false;
      document.getElementById('txtRM'+i).readOnly = true;
      document.getElementById('txtPhr'+i).readOnly = true;
      document.getElementById('txtQty'+i).readOnly = true;
     }
     document.getElementById("txtNewRm").disabled=true;
     document.getElementById("txtNPHR").disabled=true;
     document.getElementById("txtNQuantity").disabled=true;
     document.getElementById("txtNQuantity").readOnly=false;
  }else if ( SamplePrep.checked == true ){
     for (var i = 0; i < clength; i++) {
      document.getElementById("chkExclude"+i).disabled=false;
     }
     document.getElementById("txtNewRm").disabled=false;
     document.getElementById("txtNPHR").disabled=false;
     document.getElementById("txtNQuantity").disabled=false;
     document.getElementById("txtNQuantity").readOnly=true;
  }

}

function editable()
{
  if ( document.getElementById('chkEdit').checked == true )
  {
    document.getElementById('txtMixParam1').readOnly = false;
    document.getElementById('txtMixSequence1').readOnly = false;
    document.getElementById('txtMixParam2').readOnly = false;
    document.getElementById('txtMixSequence2').readOnly = false;
    document.getElementById('txtMixParam3').readOnly = false;
    document.getElementById('txtMixSequence3').readOnly = false;
    document.getElementById('txtMixParam4').readOnly = false;
    document.getElementById('txtMixSequence4').readOnly = false;
    document.getElementById('txtMixParam5').readOnly = false;
    document.getElementById('txtMixSequence5').readOnly = false;
  }
  else
  {
    document.getElementById('txtMixParam1').readOnly = true;
    document.getElementById('txtMixSequence1').readOnly = true;
    document.getElementById('txtMixParam2').readOnly = true;
    document.getElementById('txtMixSequence2').readOnly = true;
    document.getElementById('txtMixParam3').readOnly = true;
    document.getElementById('txtMixSequence3').readOnly = true;
    document.getElementById('txtMixParam4').readOnly = true;
    document.getElementById('txtMixSequence4').readOnly = true;
    document.getElementById('txtMixParam5').readOnly = true;
    document.getElementById('txtMixSequence5').readOnly = true;
  }

}

function enableExtrusion(){

  var soft = document.getElementById('Soft');
  var rigid = document.getElementById('Rigid');

  if ( soft.checked == true )
  {
    txtExtDarNo.readOnly = false;
    txtExtZ1.readOnly = false;
    txtExtZ2.readOnly = false;
    txtExtDH.readOnly = false;
    txtExtScrewSpeed.readOnly = false;
    txtExtCutterSpeed.readOnly = false;
    var soft_pvc = 1;
  }
  else if ( rigid.checked == true )
  {
    txtExtDarNo.readOnly = true;
    txtExtZ1.readOnly = true;
    txtExtZ2.readOnly = true;
    txtExtDH.readOnly = true;
    txtExtScrewSpeed.readOnly = true;
    txtExtCutterSpeed.readOnly = true;
    var soft_pvc = 0;
  }
  
}

function disabledOptionFG(){
  var sltFGLen = document.getElementById('sltFG').length;
  var sltActivity = document.getElementById('sltActivity').value;

  for ( var i = 0; i < sltFGLen; i++ ){
    if ( sltActivity == 4 || sltActivity == 2 ){
      document.getElementById('optionFG'+i).disabled = true;
    }else{
      document.getElementById('optionFG'+i).disabled = false;
    }
  }

}

function enableOtherSupplier(){
  var otherSupplier = document.getElementById('otherSupplier');
  var optionLength = document.getElementById('sltSupplier').length;

  if ( otherSupplier.checked == true ) {
    document.getElementById("txtOtherSupplier").readOnly = false;
    for ( var i = 0; i < optionLength; i++ ){
      document.getElementById('optionSupplier'+i).disabled = true;
    }
    // alert(optionLength);
  }else if ( otherSupplier.checked == false ){
    document.getElementById("txtOtherSupplier").readOnly = true;
    for ( var i = 0; i < optionLength; i++ ){
      document.getElementById('optionSupplier'+i).disabled = false;
    }
  }
}

function enableClassOthers(){
  var radClassOthers = document.getElementById('radClassOthers');
  var radClassResin = document.getElementById('radClassResin');
  var radClassStabilizer = document.getElementById('radClassStabilizer');
  var radClassFillers = document.getElementById('radClassFillers');
  var radClassPlasticizer = document.getElementById('radClassPlasticizer');
  var radClassLubricant = document.getElementById('radClassLubricant');
  var radClassColorant = document.getElementById('radClassColorant');

  if ( radClassOthers.checked == true ) {
    document.getElementById("txtOtherClass").readOnly = false;
  }else if ( radClassOthers.checked == false 
            || radClassResin.checked == true
            || radClassStabilizer.checked == true
            || radClassFillers.checked == true
            || radClassPlasticizer.checked == true
            || radClassLubricant.checked == true
            || radClassColorant.checked == true ){
    document.getElementById("txtOtherClass").readOnly = true;
  }
}

function enableSampleOthers(){
  var radSamplePowder = document.getElementById('radSamplePowder');
  var radSampleLiquid = document.getElementById('radSampleLiquid');
  var radSampleOthers = document.getElementById('radSampleOthers');
  var radSamplePellet = document.getElementById('radSamplePellet');

  if ( radSampleOthers.checked == true ) {
    document.getElementById("txtOtherSample").readOnly = false;
  }else if ( radSampleOthers.checked == false 
            || radSamplePowder.checked == true
            || radSampleLiquid.checked == true
            || radSamplePellet.checked == true){
    document.getElementById("txtOtherSample").readOnly = true;
  }
}

function showRNDActivity()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      rnd_activity = NRM.value;
    }else if ( JRD.checked == true ){
      rnd_activity = JRD.value;
    }

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("sltActivity").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/activity_dropdown.php?rnd_activity=" + rnd_activity,true);
    xmlhttp.send();
}

function showNRMNumber()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      nrm = 1;
    }else if ( JRD.checked == true ){
      nrm = 0;
    }

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltNRMNumber").innerHTML=xmlhttp.responseText;
      }
    }

        xmlhttp.open("GET","include/nrm_number_dropdown.php?nrm=" + nrm,true);
        xmlhttp.send();
}

function showJRDNumber()
{
    var NRM = document.getElementById('NRM');
    var JRD = document.getElementById('JRD');

    if ( NRM.checked == true ){
      jrd = 0;
    }else if ( JRD.checked == true ){
      jrd = 1;
    }
    // alert('nrm '+ nrm + 'jrd '+ jrd );
    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sltJRDNumber").innerHTML=xmlhttp.responseText;
      }
    }

        xmlhttp.open("GET","include/jrd_number_dropdown.php?jrd=" + jrd,true);
        xmlhttp.send();
}

function showRM(rm_type, index){
  var txtRM_type_id = document.getElementById('txtRM_type_id');

  var xmlhttp; 
    if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
    else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("txtRM_id"+index).innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/rm_bat_dropdown.php?rm_type=" + rm_type + "&index=" + index,true);
    xmlhttp.send();
}

function showNRMResults(){

  var sltNRM = document.getElementById("sltNRM").value;

    // alert(sltNRM);

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("NRMResults").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/nrm_results.php?sltNRM=" + sltNRM,true);
    xmlhttp.send();

}
function disableNewCustomer(){
  var chkNewCustomer = document.getElementById("chkNewCustomer");
  var txtNewCustomer = document.getElementById("txtNewCustomer");
  var sltExistingCustomer = document.getElementById("sltExistingCustomer").length;
  var i = 0;

  if ( chkNewCustomer.checked == true ){
    txtNewCustomer.disabled = false;
    for ( i = 0; i < sltExistingCustomer; i++ ){
      document.getElementById("optionCustomer"+i).disabled = true;
    }
  }else{
    txtNewCustomer.disabled = true;
    for ( i = 0; i < sltExistingCustomer; i++ ){
      document.getElementById("optionCustomer"+i).disabled = false;
    }

  }
}
function showCustomerReq(JRDId){
  var radRequestType = document.getElementsByName("radRequestType")
  
  for ( var i = 0; i < radRequestType.length; i++ ){

    if ( radRequestType[i].checked == true ){
      var RequestType = document.getElementsByName("radRequestType")[i].value;
    }

  }

  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           document.getElementById("tbodyCustomerReq").innerHTML=xmlhttp.responseText;
        }
      }
    if ( RequestType == 'Development' ){
      xmlhttp.open("GET","include/product_development.php?JRDId=" + JRDId, true);
      xmlhttp.send();
    }else if ( RequestType == 'Modification' ){
      xmlhttp.open("GET","include/product_modification.php?JRDId=" + JRDId, true);
      xmlhttp.send();
    }else if ( RequestType == 'Evaluation' ){
      xmlhttp.open("GET","include/product_evaluation.php?JRDId=" + JRDId, true);
      xmlhttp.send();
    }

}
function enableOtherReason(){
  var Others = document.getElementById("Others");

  if ( Others.checked == true ){
    document.getElementById("txtOtherReason").disabled = false;
  }else{
    document.getElementById("txtOtherReason").disabled = true;
  }
}
function enableOtherExtrusionType(){
  var radExtrusionType = document.getElementsByName("radExtrusionType");

  for ( var i = 0; i < radExtrusionType.length; i++ ){

    if ( radExtrusionType[i].checked == true ){
      var ExtrusionType = document.getElementsByName("radExtrusionType")[i].value;
    }

  }

  if ( ExtrusionType == 'OtherExtrusionType' ){
    document.getElementById("txtOtherExtrusionType").disabled = false;
  }else{
    document.getElementById("txtOtherExtrusionType").disabled = true;
  }
}
function enableOtherThermalRate(){
  var radThermalRate = document.getElementsByName("radThermalRate");
  
  for ( var i = 0; i < radThermalRate.length; i++ ){

    if ( radThermalRate[i].checked == true ){
      var ThermalRate = document.getElementsByName("radThermalRate")[i].value;
    }

  }

  if ( ThermalRate == 'OtherThermalRate' ){
    document.getElementById("txtOtherThermalRate").disabled = false;
  }else{
    document.getElementById("txtOtherThermalRate").disabled = true;
  }
}
function enableOtherMoldingType(){
  var radMoldingType = document.getElementsByName("radMoldingType");
  
  for ( var i = 0; i < radMoldingType.length; i++ ){

    if ( radMoldingType[i].checked == true ){
      var ThermalRate = document.getElementsByName("radMoldingType")[i].value;
    }

  }

  if ( ThermalRate == 'OtherMoldingType' ){
    document.getElementById("txtOtherMoldingType").disabled = false;
  }else{
    document.getElementById("txtOtherMoldingType").disabled = true;
  }
}
function enableOtherReq(){
  var OtherReq = document.getElementById("OtherReq");

  if ( OtherReq.checked == true ){
    document.getElementById("txtOtherReq").disabled = false;
  }else{
    document.getElementById("txtOtherReq").disabled = true;
  }

}
function enableSubCategories(){
  var chkExtrusion = document.getElementById("chkExtrusion");
  var chkMolding = document.getElementById("chkMolding");
  var chkOtherActivity = document.getElementById("chkOtherActivity");

  if ( chkExtrusion.checked == true ){
    document.getElementById("Jacket").disabled = false;
    document.getElementById("Insulation").disabled = false;
    document.getElementById("OtherExtrusionType").disabled = false;
    document.getElementById("60C").disabled = false;
    document.getElementById("75C").disabled = false;
    document.getElementById("90C").disabled = false;
    document.getElementById("105C").disabled = false;
    document.getElementById("OtherThermalRate").disabled = false;
  }else{
    document.getElementById("Jacket").disabled = true;
    document.getElementById("Insulation").disabled = true;
    document.getElementById("OtherExtrusionType").disabled = true;
    document.getElementById("txtOtherExtrusionType").disabled = true;
    document.getElementById("60C").disabled = true;
    document.getElementById("75C").disabled = true;
    document.getElementById("90C").disabled = true;
    document.getElementById("105C").disabled = true;
    document.getElementById("OtherThermalRate").disabled = true;
    document.getElementById("txtOtherThermalRate").disabled = true;
  }

  if ( chkMolding.checked == true ){
    document.getElementById("30").disabled = false;
    document.getElementById("55").disabled = false;
    document.getElementById("Tube").disabled = false;
    document.getElementById("40").disabled = false;
    document.getElementById("Film").disabled = false;
    document.getElementById("OtherMoldingType").disabled = false;
    document.getElementById("45").disabled = false;
    document.getElementById("Bottle").disabled = false;
  }else{
    document.getElementById("30").disabled = true;
    document.getElementById("55").disabled = true;
    document.getElementById("Tube").disabled = true;
    document.getElementById("40").disabled = true;
    document.getElementById("Film").disabled = true;
    document.getElementById("OtherMoldingType").disabled = true;
    document.getElementById("45").disabled = true;
    document.getElementById("Bottle").disabled = true;
  }

  if ( chkOtherActivity.checked == true ){
    document.getElementById("txtOtherActivity").disabled = false;
  }else{
    document.getElementById("txtOtherActivity").disabled = true;
  }

}

function showBatchTicketTrialsM(){

  var sltFormulaType = document.getElementById("sltFormulaType").value;
  
  var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("standard_formula").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/standard_formula.php?formula_type="+sltFormulaType+"&trial=1"+"&sltActivity=5",true);
    xmlhttp.send();

}

function showJRDDataNoReplicate(){

  var sltJRDNumber = document.getElementById("sltJRDNumber").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("tbJRDData").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/jrd_data_noreplicate_dropdown.php?sltJRDNumber=" + sltJRDNumber,true);
  xmlhttp.send();

}

function showFGtoEvaluate(){

  var sltFGEvaluate = document.getElementById("sltFGEvaluate").value;

  var product_id = sltFGEvaluate.substring(0, sltFGEvaluate.indexOf('-'));
  var product_name = sltFGEvaluate.substring(sltFGEvaluate.indexOf('-')+1);

  document.getElementById("hidProductID").value = product_id;
  document.getElementById("hidProductName").value = product_name;

}

function showJRDData(){

  var sltJRDNumber = document.getElementById("sltJRDNumber").value;
  var hidBatchTicketId = document.getElementById("hidBatchTicketId").value;
  var hidBatchTicketTrialId = document.getElementById("hidBatchTicketTrialId").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
    {
    xmlhttp=new XMLHttpRequest();
    }
  else
    {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
    {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("tbJRDData").innerHTML=xmlhttp.responseText;
      }
    }
  xmlhttp.open("GET","include/jrd_data_dropdown.php?sltJRDNumber=" + sltJRDNumber + "&hidBatchTicketId=" + hidBatchTicketId + "&hidBatchTicketTrialId=" + hidBatchTicketTrialId,true);
  xmlhttp.send();

}

function showJRDResults(){

  var sltJRD = document.getElementById("sltJRD").value;

    var xmlhttp; 
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("JRDResults").innerHTML=xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET","include/jrd_results.php?sltJRD=" + sltJRD,true);
    xmlhttp.send();

}
function cancelBox(id){
      var reason = prompt("Are you sure you want to Cancel Trial?\n\nReason:",'');

      if ( reason == null || reason == '' ){
        alert ("Reason is mandatory.\n\nCancellation not complete.");
      }else if ( reason != null ){
        window.location.assign('process_cancel_trial.php?id=' + id + '&reason=' + reason);
      }
    }