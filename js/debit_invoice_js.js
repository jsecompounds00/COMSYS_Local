function changeTransactionType()
{

   var sltTransType = document.getElementById("sltTransType").value;
   var hidDebitId = document.getElementById("hidDebitId").value;
   var txtInvoiceDate = document.getElementById("txtInvoiceDate");
   var txtCheckReturnedDate = document.getElementById("txtCheckReturnedDate");

   if ( sltTransType == "IAR" ){
        txtCheckReturnedDate.disabled = true;
        txtInvoiceDate.disabled = false;

        var xmlhttp; 
        if (window.XMLHttpRequest)
          {
          xmlhttp=new XMLHttpRequest();
          }
        else
          {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
          {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            document.getElementById("tbDebitInvoiceItems").innerHTML=xmlhttp.responseText;
            }
          }
        xmlhttp.open("GET","include/debit_invoice_items.php?hidDebitId="+hidDebitId,true);
        xmlhttp.send();
   }else{
        txtInvoiceDate.disabled = true;
        txtCheckReturnedDate.disabled = false;

        var xmlhttp; 
        if (window.XMLHttpRequest)
          {
          xmlhttp=new XMLHttpRequest();
          }
        else
          {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
          {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            document.getElementById("tbDebitInvoiceItems").innerHTML=xmlhttp.responseText;
            }
          }
        xmlhttp.open("GET","include/debit_pmt_reject_items.php?hidDebitId="+hidDebitId,true);
        xmlhttp.send();
   }
  
}

function showInvoices( jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber");
  var sltBankCode = document.getElementById("sltBankCode").value;
  var txtChecknumber = document.getElementById("txtChecknumber").value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdInvoiceNumber").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_invoices.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&jacket_id=" + jacket_id,true);
  xmlhttp.send();

}

function showAmounts( jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber");
  var sltBankCode = document.getElementById("sltBankCode").value;
  var txtChecknumber = document.getElementById("txtChecknumber").value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdAmount").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_amounts.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&jacket_id=" + jacket_id ,true);
  xmlhttp.send();

}

function showDiscounts( jacket_id ){

  // var tdInvoiceNumber = document.getElementById("tdInvoiceNumber");
  var sltBankCode = document.getElementById("sltBankCode").value;
  var txtChecknumber = document.getElementById("txtChecknumber").value;
  var sltCustomer = document.getElementById("sltCustomer").value;

  var xmlhttp; 
  if (window.XMLHttpRequest)
  {
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("tdDiscount").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","include/check_related_discounts.php?sltBankCode=" + sltBankCode + "&txtChecknumber=" + txtChecknumber + "&sltCustomer=" + sltCustomer + "&jacket_id=" + jacket_id,true);
  xmlhttp.send();

}