
function cancelBox(id, eId){
    var reason = prompt("Are you sure you want to Cancel Schedule?\n\nReason:",'');

    if ( reason == null || reason == '' ){
        alert ("Reason is mandatory.\n\nCancellation not completed.");
    }else if ( reason != null ){
        window.location.assign('process_cancel_schedule.php?schedule_id=' + id + '&reason=' + reason + '&equipment_Id=' + eId);
    }
}
