function showPRENumList()
{
    var hidCanvassID = document.getElementById('hidCanvassID').value;
    var hidPREIDs = document.getElementById('hidPREIDs').value;

    var radDivision = document.getElementsByName("radDivision");
    var countDivision = radDivision.length;

    for ( var i = 0; i < countDivision; i++){
      if ( radDivision[i].checked ){
        if ( radDivision[i].value == 'Compounds' ){
          var Division = 'Compounds';
        }else if ( radDivision[i].value == 'Pipes' ){
          var Division = 'Pipes';
        }else if ( radDivision[i].value == 'Corporate' ){
          var Division = 'Corporate';
        }else if ( radDivision[i].value == 'PPR' ){
          var Division = 'PPR';
        }
      }
    }

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("sltPRE").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/canvass_pre_number_dropdown.php?hidCanvassID=" + hidCanvassID + "&Division=" + Division + "&hidPREIDs=" + hidPREIDs,true);
    xmlhttp.send();

}

function sethidPREIDs(){
  document.getElementById("hidPREIDs").value = null;
}

function showPREDetails()
{
    var hidCanvassID = document.getElementById('hidCanvassID').value;
    var hidPREIDs = document.getElementById('hidPREIDs').value;

    var preIds = document.getElementById('sltPRE');

    var radDivision = document.getElementsByName("radDivision");
    var countDivision = radDivision.length;

    for ( var i = 0; i < countDivision; i++){
      if ( radDivision[i].checked ){
        if ( radDivision[i].value == 'Compounds' ){
          var Division = 'Compounds';
        }else if ( radDivision[i].value == 'Pipes' ){
          var Division = 'Pipes';
        }else if ( radDivision[i].value == 'Corporate' ){
          var Division = 'Corporate';
        }else if ( radDivision[i].value == 'PPR' ){
          var Division = 'PPR';
        }
      }
    }

    var selectPRE = new Array();
    var i;
    var count = 0;
    for (i=0; i < preIds.options.length; i++) {
      if (preIds.options[i].selected) {
        selectPRE[count] = preIds.options[i].value;
        count++;
      }
    }

      if ( hidPREIDs == null || hidPREIDs == "" ) {
        var implodedPreIds = selectPRE.join(',');
      }else{
        var implodedPreIds = hidPREIDs;
      }

    var xmlhttp; 
    if (window.XMLHttpRequest)
      {
      xmlhttp=new XMLHttpRequest();
      }
    else
      {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById("tbCanvassItems").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","include/canvass_pre_details.php?implodedPreIds=" + implodedPreIds + "&hidCanvassID=" + hidCanvassID + "&Division=" + Division,true);
    xmlhttp.send();

}

function includePREDetail(index){

    var chkInclude = document.getElementById("chkInclude" + index);
    
    if ( chkInclude.checked == true ){

        for ( var x = 0; x < 4; x++ ){

            document.getElementById("txtCanvassDetails" + index + x).readOnly = false;
            document.getElementById("chkAvailability" + index + x).disabled = false;

        }

    }else{

        for ( var x = 0; x < 4; x++ ){

            document.getElementById("txtCanvassDetails" + index + x).readOnly = true;
            document.getElementById("chkAvailability" + index + x).checked = false;
            document.getElementById("chkAvailability" + index + x).disabled = true;

        }

    }
}

function enableSupplier(x){

    var chkNewSupplierMark = document.getElementById("chkNewSupplierMark" + x);
    var ctr = document.getElementById("hidCounterID").value;
    // alert(x);
    if ( chkNewSupplierMark.checked == true ){
        document.getElementById("sltSupplier" + x).disabled = true;
        if (ctr <= x){ document.getElementById("sltSupplier" + x).value = 0; }
        document.getElementById("txtNewSupplier" + x).readOnly = false;
    }else{
        document.getElementById("sltSupplier" + x).disabled = false;
        document.getElementById("txtNewSupplier" + x).readOnly = true;
        if (ctr <= x){ document.getElementById("txtNewSupplier" + x).value = ""; }
    }
}

function enableSupplierMarking(x){

    var sltSupplier = document.getElementById("sltSupplier"+x);
    var txtNewSupplier = document.getElementById("txtNewSupplier"+x);
    var chkNewSupplierMark = document.getElementById("chkNewSupplierMark"+x);
    var hidCanvassID = document.getElementById("hidCanvassID");
    var ctr = document.getElementById("hidCounterID").value;

    if ( chkNewSupplierMark.checked == false ){
      
        if ( sltSupplier.value != 0 ){
          document.getElementById("SupplierMarking"+x).disabled = false;
        }else{
          if (ctr <= x){ document.getElementById("SupplierMarking"+x).disabled = true; }
        }

    }else if ( chkNewSupplierMark.checked == true ){
      
        if ( txtNewSupplier.value != "" ){
          document.getElementById("SupplierMarking"+x).disabled = false;
        }else{
          if (ctr <= x){ document.getElementById("SupplierMarking"+x).disabled = true; }
        }

    }

}

function enableMultipleSelect(){
  document.getElementById("sltPRE").disabled = false;
}
