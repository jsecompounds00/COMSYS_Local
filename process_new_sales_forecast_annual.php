<?php
########## Start session
	session_start();
    ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_forecast_annual.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidForecastID = $_POST['hidForecastID'];
		$txtYear = $_POST['txtYear'];
		$txtForex = $_POST['txtForex'];
		$sltCustomer = $_POST['sltCustomer'];
		$txtRemarks = $_POST['txtRemarks'];

		// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		
		// $txtRemarks = str_replace('\\', '', $txtRemarks);

########## Input Validations
		if ( !is_numeric( $txtYear ) ){
			$errmsg_arr[] = '* Invalid forecast year.';
			$errflag = true;
		}
		if ( $txtYear == '' && !is_numeric( $txtYear ) ){
			$errmsg_arr[] = '* Invalid value of forex.';
			$errflag = true;
		}
		if ( !( $sltCustomer ) ){
			$errmsg_arr[] = '* Select customer.';
			$errflag = true;
		}
		if ( $txtForex == '' ){
			$txtForex = NULL;
		}

		if($hidForecastID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidForecastID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

########## Input Validations (Rated Capacity)
	
		foreach ($_POST['hidForecastItemID'] as $ForecastItemID) {
			$ForecastItemID = array();
			$ForecastItemID = $_POST['hidForecastItemID'];
		}
		foreach ($_POST['hidFGID'] as $FGID) {
			$FGID = array();
			$FGID = $_POST['hidFGID'];
		}
		foreach ($_POST['hidItemType'] as $ItemType) {
			$ItemType = array();
			$ItemType = $_POST['hidItemType'];
		}
		foreach ($_POST['txtPrice'] as $Price) {
			$Price = array();
			$Price = $_POST['txtPrice'];
		}
		foreach ($_POST['txtJanuary'] as $January) {
			$January = array();
			$January = $_POST['txtJanuary'];
		}
		foreach ($_POST['txtFebruary'] as $February) {
			$February = array();
			$February = $_POST['txtFebruary'];
		}
		foreach ($_POST['txtMarch'] as $March) {
			$March = array();
			$March = $_POST['txtMarch'];
		}
		foreach ($_POST['txtApril'] as $April) {
			$April = array();
			$April = $_POST['txtApril'];
		}
		foreach ($_POST['txtMay'] as $May) {
			$May = array();
			$May = $_POST['txtMay'];
		}
		foreach ($_POST['txtJune'] as $June) {
			$June = array();
			$June = $_POST['txtJune'];
		}
		foreach ($_POST['txtJuly'] as $July) {
			$July = array();
			$July = $_POST['txtJuly'];
		}
		foreach ($_POST['txtAugust'] as $August) {
			$August = array();
			$August = $_POST['txtAugust'];
		}
		foreach ($_POST['txtSeptember'] as $September) {
			$September = array();
			$September = $_POST['txtSeptember'];
		}
		foreach ($_POST['txtOctober'] as $October) {
			$October = array();
			$October = $_POST['txtOctober'];
		}
		foreach ($_POST['txtNovember'] as $November) {
			$November = array();
			$November = $_POST['txtNovember'];
		}
		foreach ($_POST['txtDecember'] as $December) {
			$December = array();
			$December = $_POST['txtDecember'];
		}

		$ctr = count($FGID);
		$i = 0;

		// do{
		// 	if ( $Price[$i] == '' ){
		// 		$Price[$i] = NULL;
		// 	}
		// }while ($i < $ctr);

		foreach ($FGID as $key_1 => $value) {

			if ( $Price[$key_1] == '' ){
				$Price[$key_1] = NULL;
			}

			if ( $January[$key_1] == '' ){
				$January[$key_1] = NULL;
			}

			if ( $February[$key_1] == '' ){
				$February[$key_1] = NULL;
			}

			if ( $March[$key_1] == '' ){
				$March[$key_1] = NULL;
			}

			if ( $April[$key_1] == '' ){
				$April[$key_1] = NULL;
			}

			if ( $May[$key_1] == '' ){
				$May[$key_1] = NULL;
			}

			if ( $June[$key_1] == '' ){
				$June[$key_1] = NULL;
			}

			if ( $July[$key_1] == '' ){
				$July[$key_1] = NULL;
			}

			if ( $August[$key_1] == '' ){
				$August[$key_1] = NULL;
			}

			if ( $September[$key_1] == '' ){
				$September[$key_1] = NULL;
			}

			if ( $October[$key_1] == '' ){
				$October[$key_1] = NULL;
			}

			if ( $November[$key_1] == '' ){
				$November[$key_1] = NULL;
			}

			if ( $December[$key_1] == '' ){
				$December[$key_1] = NULL;
			}

		}

############# SESSION, keeping last input value
		// $_SESSION['counter1'] = $ctr;
		// $_SESSION['txtYear'] = $txtYear;
		// $_SESSION['radFGType'] = $radFGType;

		// foreach ($_POST['sltFGItem'] as $key => $prodValue) {
		// 	$_SESSION['sltFGItem'][$key] = $prodValue;
		// 	$_SESSION['txtQuantity'][$key] = $Quantity[$key];
		// }

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_sales_forecast_annual.php?id=$hidForecastID");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Sales_Forecast_Annual_CRU(?, ?, ?, ?, ?, ?, ?, ?, ? )");
	mysqli_stmt_bind_param($qry, 'iidisssii', $hidForecastID, $txtYear, $txtForex, $sltCustomer, $txtRemarks, $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_forecast_annual.php'.'</td><td>'.$processError.' near line 147.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidForecastID ) {
				$ForecastID = $hidForecastID;
			}else{
				$ForecastID = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['hidFGID'] as $key => $temValue)
			{	
				// if (!$hidForecastID) {
				// 	$ForecastItemID = 0;
				// }

				if ($temValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Sales_Forecast_Annual_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ? )");
					mysqli_stmt_bind_param($qryItems, 'iiiddddddddddddds', $ForecastItemID[$key], $ForecastID, $FGID[$key], $Price[$key]
																		, $January[$key], $February[$key], $March[$key], $April[$key]
																		, $May[$key], $June[$key], $July[$key], $August[$key]
																		, $September[$key], $October[$key], $November[$key], $December[$key]
																		, $ItemType[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_forecast_annual.php'.'</td><td>'.$processError1.' near line 172.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidForecastID )
							$_SESSION['SUCCESS']  = 'Sales forecast successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Sales forecast successfully added.';
						header("location: sales_forecast_annual.php?page=".$_SESSION['page_a']."&fg=".$_SESSION['fg_a']."&year=".$_SESSION['year_a']."&customer=".$_SESSION['customer_a']);
					}				
				}
			}
		}
	}	
############# Committing in Database

		require("include/database_close.php");

	}
?>