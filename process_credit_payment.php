<?php
############# Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_credit_payment.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitize the POST values
		$hidCreditId = $_POST['hidCreditId'];
		$hidProcessType = $_POST['hidProcessType'];
		$sltCustomer = $_POST['sltCustomer'];
		$hidTransType = $_POST['hidTransType'];
		$txtCheckDate = $_POST['txtCheckDate'];
		$txtCheckNo = $_POST['txtCheckNo'];
		$sltBankCode = $_POST['sltBankCode'];
		$txtCheckAmount = $_POST['txtCheckAmount'];
		$hidErrorMessage = $_POST['hidErrorMessage'];
		$txtInvoiceDate = NULL;
		$txtCheckReturnedDate = NULL;
		$hidJacketType = $_POST['hidJacketType'];
	
		// foreach ($_POST['chkIncludeInvoice'] as $IncludeInvoice) {
		// 	$IncludeInvoice = array();
		// 	$IncludeInvoice = $_POST['chkIncludeInvoice'];
		// }
		// foreach ($_POST['hidInvoiceNumber'] as $InvoiceNumber) {
		// 	$InvoiceNumber = array();
		// 	$InvoiceNumber = $_POST['hidInvoiceNumber'];
		// }
		// foreach ($_POST['txtAmount'] as $Amount) {
		// 	$Amount = array();
		// 	$Amount = $_POST['txtAmount'];
		// }
		// foreach ($_POST['txtDiscount'] as $Discount) {
		// 	$Discount = array();
		// 	$Discount = $_POST['txtDiscount'];
		// }
		// foreach ($_POST['hidCreditItemID'] as $CreditItemID) {
		// 	$CreditItemID = array();
		// 	$CreditItemID = $_POST['hidCreditItemID'];
		// }
		// foreach ($_POST['refInvoiceNumber'] as $refInvoiceNumber) {
		// 	$refInvoiceNumber = array();
		// 	$refInvoiceNumber = $_POST['refInvoiceNumber'];
		// }

		$ctr = count($InvoiceNumber);
		$counter = count($refInvoiceNumber);

		$i = 0;

############# Input Validations

		if ( !$sltCustomer ){
			$errmsg_arr[] = "* Select customer.";
			$errflag = true;
		}
		$valCheckDate = validateDate($txtCheckDate, "Y-m-d");
		if ( $valCheckDate != 1 ){
			$errmsg_arr[] = "* Invalid check date.";
			$errflag = true;
		}
		if ( !$sltBankCode ){
			$errmsg_arr[] = "* Select bank code.";
			$errflag = true;
		}
		if ( empty( $txtCheckNo ) ){
			$errmsg_arr[] = "* Check number can't be blank.";
			$errflag = true;
		}
		if ( !is_numeric( $txtCheckAmount ) ){
			$errmsg_arr[] = "* Check amount must be numeric.";
			$errflag = true;
		}

		if ( $sltCustomer ){
			// if ( $ctr == 0 ){
			// 	$errmsg_arr[] = "* Transaction is invalid. Please choose at least (1) one transaction";
			// 	$errflag = true;
			// }else{
			// 	do{
			// 		if ( isset($_POST["chkIncludeInvoice"][$i]) ){
			// 			if ( !is_numeric( $Amount[$i] ) ){
			// 				$errmsg_arr[] = "* Invalid value for Payment A/R for invoice number ".$InvoiceNumber[$i].".";
			// 				$errflag = true;
			// 			}
			// 			if ( $Discount[$i] != "" && !is_numeric( $Discount[$i] ) ){
			// 				$errmsg_arr[] = "* Invalid value of discount for invoice number ".$InvoiceNumber[$i].".";
			// 				$errflag = true;
			// 			}
			// 			if ( empty( $Discount[$i] ) ){
			// 				$Discount[$i] = NULL;
			// 			}
			// 		}
			// 		$i++;
			// 	}while ( $i < $ctr );
			// }
			$IncludeInvoice = $_POST['chkIncludeInvoice'];
			$InvoiceNumber = $_POST['hidInvoiceNumber'];
			$Amount = $_POST['txtAmount'];
			$Discount = $_POST['txtDiscount'];
			$CreditItemID = $_POST['hidCreditItemID'];
			$refInvoiceNumber = $_POST['refInvoiceNumber'];

			foreach ($IncludeInvoice as $key_1 => $value) {
				// if ( isset($_POST['chkIncludeInvoice'][$key_1]) ){
					echo $Amount[$key_1];
					if ( !is_numeric( $Amount[$key_1] ) ){
						$errmsg_arr[] = "* Invalid value for Payment A/R for invoice number ".$InvoiceNumber[$key_1].".";
						$errflag = true;
					}
					if ( $Discount[$key_1] != "" && !is_numeric( $Discount[$key_1] ) ){
						$errmsg_arr[] = "* Invalid value of discount for invoice number ".$InvoiceNumber[$key_1].".";
						$errflag = true;
					}
					if ( empty( $Discount[$key_1] ) ){
						$Discount[$i] = NULL;
					}
				// }else{echo "1";}
			}
		}

		if ( !empty($hidErrorMessage) ){
			$errmsg_arr[] = $hidErrorMessage;
			$errflag = true;
		}
		

		if($hidCreditId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidCreditId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# SESSION

		$_SESSION['SESS_CJCP_Customer'] = $sltCustomer;
		$_SESSION['SESS_CJCP_CheckDate'] = $txtCheckDate;
		$_SESSION['SESS_CJCP_CheckNo'] = $txtCheckNo;
		$_SESSION['SESS_CJCP_BankCode'] = $sltBankCode;
		$_SESSION['SESS_CJCP_CheckAmount'] = $txtCheckAmount;
		$_SESSION['SESS_CJCP_Counter'] = $ctr;

		foreach ($IncludeInvoice as $key_2 => $value) {
			$_SESSION['SESS_CJCP_Amount'][$key_2 ] = $Amount[$key_2];
			$_SESSION['SESS_CJCP_Discount'][$key_2 ] = $Discount[$key_2];
		}

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			if ( $hidCreditId ){
				header("Location: credit_payment.php?id=".$hidCreditId."&type=".$hidJacketType);
			}else{
				header("Location: credit_payment.php?id=".$hidCreditId);
			}
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isissssssiid', $hidCreditId, $hidProcessType, $sltCustomer, $hidTransType
											 , $txtInvoiceDate, $txtCheckDate, $txtCheckReturnedDate
											 , $createdAt, $updatedAt, $createdId, $updatedId, $txtCheckAmount);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_credit_payment.php'.'</td><td>'.$processError.' near line 180.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidCreditId ) {
				$CreditId = $hidCreditId;
			}else{
				$CreditId = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['hidInvoiceNumber'] as $key => $itemValue)
			{	
				if (!$hidCreditId) {
					$CreditItemID = 0;
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_Customer_Jacket_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iisisddss', $CreditItemID[$key], $CreditId, $InvoiceNumber[$key]
															   , $sltBankCode, $txtCheckNo, $Amount[$key]
															   , $Discount[$key], $createdAt, $updatedAt );
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_credit_payment.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidCreditId )
							$_SESSION['SUCCESS']  = 'Transaction successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'Transaction successfully added.';
						header("location: customer_jacket.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database	
		require("include/database_close.php");
	}
?>
	