<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_monthly_kw_rate.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidPriceID = $_POST['hidPriceID'];
		$sltMonth = clean($_POST['sltMonth']);
		$sltYear = clean($_POST['sltYear']);
		$txtPrice = clean($_POST['txtPrice']);

		//Input Validations
		if ( !($sltMonth) ){
			$errmsg_arr[] = '* Invalid month.';
			$errflag = true;
		}
		if ( !($sltYear) ){
			$errmsg_arr[] = '* Invalid year.';
			$errflag = true;
		}
		if (!is_numeric($txtPrice)){
			$errmsg_arr[] = '* Invalid price.';
			$errflag = true;
		}

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_monthly_kw_rate.php?id=$hidPriceID");
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Monthly_KW_Rate_CRU(?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isid', $hidPriceID, $sltMonth, $sltYear, $txtPrice);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_monthly_kw_rate.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidPriceID)
				$_SESSION['SUCCESS']  = 'Successfully updated price.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new price.';
			//echo $_SESSION['SUCCESS'];
			header("location:monthly_kw_rate.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	