<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_credit_terms.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidCreditId = $_POST['hidCreditId'];
		// $sltCustomer = $_POST['sltCustomer'];
		$sltContractNum = $_POST['sltContractNum'];
		$txtLCNum = $_POST['txtLCNum'];
		$txtDate = $_POST['txtDate'];
		$txtLCQuantity = $_POST['txtLCQuantity'];
		$sltCustomer = 0;
########## Input Validations
		// if ( !$sltCustomer ){
		// 	$errmsg_arr[] = '* Select customer.';
		// 	$errflag = true;
		// }
		if ( $txtLCNum == '' ){
			$errmsg_arr[] = '* LC number cannot be blank.';
			$errflag = true;
		}
		$valtxtDate = validateDate($txtDate, 'Y-m-d');
		if ( $valtxtDate != 1 ){
			$errmsg_arr[] = '* Invalid LC date.';
			$errflag = true;
		}
		if ( $txtLCQuantity == '' ){
			$errmsg_arr[] = '* LC quantity cannot be blank.';
			$errflag = true;
		}
		if ( !is_numeric($txtLCQuantity) ){
			$errmsg_arr[] = '* LC quantity must be numeric.';
			$errflag = true;
		}

		if($hidCreditId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidCreditId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];
	
############# SESSION, keeping last input value
		$hidTag = $_POST['hidTag'];

		$_SESSION['sltContractNum'] = $sltContractNum;
		$_SESSION['txtLCNum'] = $txtLCNum;
		$_SESSION['txtDate'] = $txtDate;
		$_SESSION['txtLCQuantity'] = $txtLCQuantity;

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_credit_terms.php?id=$hidCreditId");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_Credit_Terms_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iissdssiii', $hidCreditId, $sltContractNum, $txtLCNum, $txtDate, $txtLCQuantity
										   , $createdAt, $updatedAt, $createdId, $updatedId, $sltCustomer);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_credit_terms.php'.'</td><td>'.$processError.' near line 90.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{

		if ( !empty($processError1) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_credit_terms.php'.'</td><td>'.$processError1.' near line 95.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			if ( $hidCreditId )
				$_SESSION['SUCCESS']  = "LC# $txtLCNum is successfully updated.";
			else
				$_SESSION['SUCCESS']  = "LC# $txtLCNum is successfully added.";
			header("location: credit_terms.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}	

	}	
############# Committing in Database

		require("include/database_close.php");

	}
?>