<html>
	<head>
		<title>Batch Ticket - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/init_unset_values/batch_ticket_nrm_unset_value.php");
			require("/include/init_unset_values/batch_ticket_jrd_unset_value.php");
			require("/include/init_unset_values/batch_ticket_testing_unset_value.php");
			
			if( $_SESSION['bat_home'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];	
		?>

		<div class="wrapper">

			<span> <h3> Batch Ticket </h3> </span>

			<div class="search_box">
				<form method="get" action="batch_ticket.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_forms">
						<tr>
							<td> Batch Ticket Date: </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>"> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
							<td> Batch Ticket No.: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<!-- <td>
								<select>
									<option value="ALL">ALL</option>
									<option value="NRM">NRM</option>
									<option value="JRD">JRD</option>
								</select>
							</td> -->
							<td> <input type="submit" value="Search"> </td>
							<td> <input type="button" name="btnAddBatchTicket" value="New Batch Ticket" onclick="location.href='batch_ticket_for.php?id=0&tid=0'"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Batch_Ticket_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'ss', $qsone, $search);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "batch_ticket.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Batch_Ticket_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssii', $qsone, $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='10'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
						    <th>Type</th>
						    <th>Ref #</th>
						    <th>Activity</th>
						    <th>Batch Ticket No.</th>
						    <th>Batch Ticket Date</th>
						    <th>Material</th>
						    <th>FG Item</th>
						    <th>New Product</th>
						    <th>No. of Trials</th>
						    <th></th>
						</tr>
							<?php
								while( $row = mysqli_fetch_assoc( $result ) )
								{
									$id	= $row['id'];
									$ReferenceNo	= $row['ReferenceNo'];
									$ReqType = $row['ReqType'];
									$activity = $row['activity'];
									$FGName = $row['FGName'];
									$batch_ticket_no = $row['batch_ticket_no'];
									$batch_ticket_date = $row['batch_ticket_date'];
									$activity_id = $row['activity_id'];
									$no_of_trials = $row['no_of_trials'];
									$NewMaterial = $row['NewMaterial'];
									$NewFGName = $row['NewFGName'];
							?>
									<tr>
										<td> <?php echo $ReqType; ?> </td>
										<td> <?php echo $ReferenceNo; ?> </td>
										<td> <?php echo $activity; ?> </td>
										<td> <?php echo $batch_ticket_no; ?> </td>
										<td> <?php echo $batch_ticket_date; ?> </td>
										<td> <?php echo $NewMaterial; ?> </td>
										<td> <?php echo $FGName; ?> </td>
										<td> <?php echo $NewFGName; ?> </td>
										<td> <?php echo $no_of_trials; ?> </td>
										<td>
											<?php
												if ( $ReqType == 'NRM' ){
											?>		<input type='button' value='Edit Batch Ticket' onclick="location.href='new_batch_ticket_nrm.php?id=<?php echo $row['id'];?>&tid=0'">
											<?php
												}elseif ( $ReqType == 'JRD' ){
											?>		<input type='button' value='Edit Batch Ticket' onclick="location.href='new_batch_ticket_jrd.php?id=<?php echo $row['id'];?>&tid=0'">
											<?php
												}
											?>
										</td>
									</tr>
							<?php
								}
							?>
						<tr>
							<td colspan='10'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>

			<?php
				}
			?>
					
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>