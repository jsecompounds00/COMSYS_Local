<?php
############# Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_pre.php'.'</td><td>'.$error.' near line 31.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidPreId = $_POST['hidPreId'];
		$txtPRENo = clean($_POST['txtPRENo']);
		$txtPREDate = clean($_POST['txtPREDate']);
		$txtDateRec = clean($_POST['txtDateRec']);
		$txtDateNeed = clean($_POST['txtDateNeed']);
		$txtDateApproved = clean($_POST['txtDateApproved']);
		$radDivision = clean($_POST['radDivision']);
		$sltDept = clean($_POST['sltDept']);
		$sltIssToUser = clean($_POST['sltIssToUser']);
		$sltSupplyType = clean($_POST['sltSupplyType']);
		$txtRemarks = clean($_POST['txtRemarks']);
		$txtCancel_Reason = '';
		$tag_as_canceled = 0; // not cancel; 1 if cancel
		$date_cancelled = '0000-00-00 00:00:00'; // not cancel
		$hidErrorDuplicate = clean($_POST['hidErrorDuplicate']);

############# Input Validation
		if ( $hidErrorDuplicate == 1 ){
			$errmsg_arr[] = '* PRE number already exists..';
			$errflag = true;
		}
		if ( $txtPRENo=='' || !is_numeric($txtPRENo)){
			$errmsg_arr[] = '* PRE Number is invalid.';
			$errflag = true;
		}
		$valPREDate = validateDate($txtPREDate, 'Y-m-d');
		if ( $valPREDate != 1 ){
			$errmsg_arr[] = '* Invalid PRE Date.';
			$errflag = true;
		}
		$valDateRec = validateDate($txtDateRec, 'Y-m-d');
		if ( $txtDateRec != '' && $valDateRec != 1 ){
			$errmsg_arr[] = '* Invalid Date of Receipt.';
			$errflag = true;
		}elseif ( $txtDateRec == '' ){
			$txtDateRec = NULL;
		}
		$valDateNeed = validateDate($txtDateNeed, 'Y-m-d');
		if ( $valDateNeed != 1 ){
			$errmsg_arr[] = '* Invalid Date Needed.';
			$errflag = true;
		}
		$valDateApproved = validateDate($txtDateApproved, 'Y-m-d');
		if ( $txtDateApproved != '' && $valDateApproved != 1 ){
			$errmsg_arr[] = '* Invalid Date Approved.';
			$errflag = true;
		}elseif ( $txtDateApproved == '' ){
			$txtDateApproved = NULL;
		}
	############# Validation on Radio Buttons (Division Portion)
		if ( isset( $radDivision ) && $radDivision == 'Pipes' ){
			$radDivision = 'Pipes';
		}elseif ( isset( $radDivision ) && $radDivision == 'Compounds' ){
			$radDivision = 'Compounds';
		}elseif ( isset( $radDivision ) && $radDivision == 'Corporate' ){
			$radDivision = 'Corporate';
		}elseif ( isset( $radDivision ) && $radDivision == 'PPR' ){
			$radDivision = 'PPR';
		}elseif ( isset( $radDivision ) && $radDivision == 'Others' ){
			$radDivision = 'Others';
			$txtOtherDiv = clean($_POST['txtOtherDiv']);
			if ( $txtOtherDiv == '' || is_numeric( $txtOtherDiv) ){
				$errmsg_arr[] = '* Division is invalid.';
				$errflag = true;
			}
		}else{
			$errmsg_arr[] = '* Division is required.';
			$errflag = true;
		}
		// echo $radDivision;
	############# Validation on Select Dropdown (Department and Requisitioner Portion)
		if ( !$sltDept ){
			$errmsg_arr[] = '* Department is missing.';
			$errflag = true;
		}
		if ( $radDivision != 'Others' && !$sltIssToUser ){
			$errmsg_arr[] = '* Requisitioner is missing.';
			$errflag = true;
		}
	############# Validation on Checkboxes (Purpose Portion)
		if ( isset( $_POST['chkStock'] ) ){
			$chkStock = 1;
		}else{
			$chkStock = 0;
		}
		if ( isset( $_POST['chkDirec'] ) ){
			$chkDirec = 1;
		}else{
			$chkDirec = 0;
		}
		if ( isset( $_POST['chkCapex'] ) ){
			$chkCapex = 1;
			$txtCapexNo = clean($_POST['txtCapexNo']);
			$txtCapexName = clean($_POST['txtCapexName']);
			if ( $txtCapexNo == '' ){ //|| !is_numeric( $txtCapexNo)
				$errmsg_arr[] = '* Capex Number is invalid.';
				$errflag = true;
			}
			if ( $txtCapexName == '' || is_numeric( $txtCapexName) ){
				$errmsg_arr[] = '* Capex Name is invalid.';
				$errflag = true;
			}
		}else{
			$chkCapex = 0;
			$txtCapexNo = '';
			$txtCapexName = '';
		}
		if ( isset( $_POST['chkOther'] ) ){
			$chkOther = 1;
			$txtOther = clean($_POST['txtOther']);
			if ( $txtOther == '' || is_numeric( $txtOther) ){
				$errmsg_arr[] = '* Please state the purpose.';
				$errflag = true;
			}
		}else{
			$chkOther = 0;
			$txtOther = '';
		}

		if ( !$sltSupplyType ){
			$errmsg_arr[] = '* Supply Type is missing.';
			$errflag = true;
		}

		if($hidPreId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
			$updatedAt = date('Y-m-d H:i:s');
		if($hidPreId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
			$updatedId = $_SESSION['SESS_USER_ID'];
	############# Validation on PRE Items
	
		// foreach ($_POST['supplyDD'] as $supply) {
		// 	$supply = array();
		// 	$supply = $_POST['supplyDD'];
		// }
		// foreach ($_POST['txtQuantity'] as $quantity) {
		// 	$quantity = array();
		// 	$quantity = $_POST['txtQuantity'];
		// }
		// foreach ($_POST['txtUom'] as $uom) {
		// 	$uom = array();
		// 	$uom = $_POST['txtUom'];
		// }
		// foreach ($_POST['txtComments'] as $comments) {
		// 	$comments = array();
		// 	$comments = $_POST['txtComments'];
		// }
		// foreach ($_POST['hidItemNo'] as $itemNo) {
		// 	$itemNo = array();
		// 	$itemNo = $_POST['hidItemNo'];
		// }
		// foreach ($_POST['hidpreTransItemId'] as $hidpreTransItemId) {
		// 	$hidpreTransItemId = array();
		// 	$hidpreTransItemId = $_POST['hidpreTransItemId'];
		// }
		// foreach ($_POST['chkDelete'] as $delete) {
		// 	$delete = array();
			
		// 	if ( $hidPreId ){
		// 		$delete = $_POST['chkDelete'];
		// 	}else{
		// 		$delete = 0;
		// 	}
		// }
		// foreach ($_POST['chkPCF'] as $chkPCF) {
		// 	$chkPCF = array();
		// 	$chkPCF = $_POST['chkPCF'];
		// }
		// if(!empty($_POST['chkDelete']))
		// {
		// 	$deleteId = array();
		// 	foreach($_POST['chkDelete'] as $key => $id){
		// 		$deleteId[] = $itemNo[$key];
		// 	}
		// 	$delete = implode(', ', $deleteId);
		// 	echo "
		// 	<script>
		// 		var confirmDelete = confirm('Are you sure you want to delete Item No. $delete?');
		// 		if (confirmDelete==false)
		// 	 	{
		// 	  		window.location.assign('new_pre.php?id=$hidPreId');
		// 	  	}
		// 	</script>
		// 	";
		// }

		// $i=1;

		// do{

		// 	if ( $supply[$i] == 0 ){
		// 		if ($i == 1){
		// 			$errmsg_arr[] = '* Supply item is missing. (line '.$i.')';
		// 			$errflag = true;
		// 		}
		// 		else{
		// 			if ( $quantity[$i] != '' || $uom[$i] || $comments[$i] != '' ){
		// 				$errmsg_arr[] = '* Supply item is missing. (line '.$i.')';
		// 				$errflag = true;
		// 			}
		// 		}
		// 	}else{
		// 		$supp[$i] = substr($supply[$i], 0, strpos($supply[$i], ':'));
		// 		$itemType[$i] = substr($supply[$i], (strpos($supply[$i], ':')+1));
		// 		if ( $supply[$i-1] == 0 ){
		// 			if ( $i !=1 ){
		// 				$errmsg_arr[] = '* Skip Line is not allowed';
		// 				$errflag = true;
		// 			}
		// 		}if ( $quantity[$i] == '' || !is_numeric($quantity[$i]) ){
		// 			$errmsg_arr[] = '* Invalid quantity. (line '.$i.')';
		// 			$errflag = true;
		// 		}
		// 		if ( !$uom[$i] ){
		// 			$errmsg_arr[] = '* Invalid UOM. (line '.$i.')';
		// 			$errflag = true;
		// 		}
		// 	}
		// 	if ( $hidPreId && $delete[$i] && $comments[$i] == '' ){
		// 		$errmsg_arr[] = '* Comment is required for canceled item. (line '.$i.')';
		// 		$errflag = true;
		// 	}elseif ( !$delete[$i] ){
		// 		$delete[$i] = 0;
		// 	}
		// 	if ( $chkPCF[$i] ){
		// 		$PCF[$i] = 1;
		// 	}elseif ( !$delete[$i] ){
		// 		$PCF[$i] = 0;
		// 	}

		// 	$i++;
		// }while ($i<=8);


		$supply = $_POST['supplyDD'];
		$quantity = $_POST['txtQuantity'];
		$uom = $_POST['txtUom'];
		$comments = $_POST['txtComments'];
		$itemNo = $_POST['hidItemNo'];
		$hidpreTransItemId = $_POST['hidpreTransItemId'];
		$delete = $_POST['chkDelete'];
		$chkPCF = $_POST['chkPCF'];

		foreach ($supply as $key_1 => $value) {
			$supp[$key_1] = substr($supply[$key_1], 0, strpos($supply[$key_1], ':'));
			$itemType[$key_1] = substr($supply[$key_1], (strpos($supply[$key_1], ':')+1));

			if ( !$value ){
				if ( $quantity[$key_1] != '' || $uom[$key_1] || $comments[$key_1] != '' ){
					$errmsg_arr[] = '* Supply item is missing. (line '.$key_1.')';
					$errflag = true;
				}
			}else{

				if ( $quantity[$key_1] == '' || !is_numeric($quantity[$key_1]) ){
					$errmsg_arr[] = '* Invalid quantity. (line '.$key_1.')';
					$errflag = true;
				}
				if ( !$uom[$key_1] ){
					$errmsg_arr[] = '* Invalid UOM. (line '.$key_1.')';
					$errflag = true;
				}
				if ( $hidPreId && $delete[$key_1] && $comments[$key_1] == '' ){
					$errmsg_arr[] = '* Comment is required for canceled item. (line '.$key_1.')';
					$errflag = true;
				}elseif ( !$delete[$key_1] ){
					$delete[$key_1] = 0;
				}
				
				if ( $chkPCF[$key_1] ){
					$PCF[$key_1] = 1;
				}elseif ( !$delete[$key_1] ){
					$PCF[$key_1] = 0;
				}
			}
		}

############# Input Validation
	$txtComments = str_replace('\\r\\n', '<br>', $txtComments);

	$txtComments = str_replace('\\', '', $txtComments);

	$txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);

	$txtRemarks = str_replace('\\', '', $txtRemarks);

############# SESSION, keeping last input value
		$_SESSION['txtPRENo'] = $txtPRENo;
		$_SESSION['txtPREDate'] = $txtPREDate;
		$_SESSION['txtDateRec'] = $txtDateRec;
		$_SESSION['txtDateNeed'] = $txtDateNeed;
		$_SESSION['txtDateApproved'] = $txtDateApproved;
		$_SESSION['chkStock'] = $chkStock;
		$_SESSION['chkDirec'] = $chkDirec;
		$_SESSION['chkCapex'] = $chkCapex;
		$_SESSION['txtCapexNo'] = $txtCapexNo;
		$_SESSION['txtCapexName'] = $txtCapexName;
		$_SESSION['chkOther'] = $chkOther;
		$_SESSION['txtOther'] = $txtOther;
		$_SESSION['sltDept'] = $sltDept;
		$_SESSION['sltSupplyType'] = $sltSupplyType;
		$_SESSION['sltIssToUser'] = $sltIssToUser;
		$_SESSION['txtRemarks'] = $txtRemarks;
		$_SESSION['txtOtherDiv'] = $txtOtherDiv;
		$_SESSION['radDivision'] = $radDivision;


############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_PRE.$hidPreId);
			exit();
		}
// foreach ($_POST as $key => $value) {
// 	echo $key.' - '.$value;
// }
############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_PRE_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isssssssiiiiissississssii', $hidPreId, $txtPRENo, $txtPREDate, $txtDateNeed, $txtDateRec, 
									$txtDateApproved, $radDivision, $txtOtherDiv, $sltDept, $sltIssToUser, $chkStock, $chkDirec, 
									$chkCapex, $txtCapexNo, $txtCapexName, $chkOther, $txtOther, $txtRemarks, 
									$tag_as_canceled, $date_cancelled, $txtCancel_Reason, $createdAt, $updatedAt, $createdId, 
									$updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_pre.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidPreId ) {
				$preTransId = $hidPreId;
			}else{
				$preTransId = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['supplyDD'] as $key => $itemValue)
			{	
				if (!$hidPreId) {
					$hidpreTransItemId = 0;
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_PRE_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiidiiisssiiisi', $hidpreTransItemId[$key], $preTransId, $itemNo[$key],
																$quantity[$key], $uom[$key], $sltSupplyType, 
																$supply[$key], $comments[$key], $createdAt, 
																$updatedAt, $createdId, $updatedId, $delete[$key]
																, $itemType[$key], $PCF[$key]);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_pre.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidPreId )
							$_SESSION["SUCCESS"]  = "PRE# $txtPRENo is successfully updated.";
						else
							$_SESSION["SUCCESS"]  = "PRE# $txtPRENo is successfully added.";
						header("location: pre_monitoring.php?page=".$_SESSION["page"]."&item=".$_SESSION["item"]."&pre_number=".$_SESSION["pre_number"]."&pre_date=".$_SESSION["pre_date"]);
					}				
				}
			}
		}
	}	
############# Committing in Database		


	}
			require("include/database_close.php");
?>