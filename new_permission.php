<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_permission.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$permitId = $_GET['id'];

				if($permitId)
				{ 
					$qry = mysqli_prepare( $db,  "CALL sp_Permission_Query( ? )");
					mysqli_stmt_bind_param($qry, 'i', $permitId );
					$qry->execute();	
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_permission.php'.'</td><td>'.$processError.' near line 33.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$module = htmlspecialchars($row['module']);
							$permission = htmlspecialchars($row['permission']);
						}
						$db->next_result();
						$result->close();
					}

			############ .............
					$qryPI = "SELECT id from comsys.permissions";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_permission.php'.'</td><td>'.$processErrorPI.' near line 89.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

			############ .............
					if( !in_array($permitId, $id, TRUE) ){	//
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_permission.php</td><td>The user tries to edit a non-existing permission_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}					

					echo "<title>Permit - Edit</title>";
				}
				else
				{
				
					echo "<title>Permission - Add</title>";
				}

			}
		?>
	</head>
	<body>

		<form method="post" action="process_new_permission.php">
			<?php
				require("/include/header.php");
				require("/include/init_unset_values/permission_init_value.php");

				if ( $permitId ){
					if( $_SESSION['edit_permission'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
					}
				}else{
					if( $_SESSION['add_permission'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
					}
				}
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $permitId ? "Edit ".$permission : "New Permission" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>
							Module:
						</td>
						<td>
							<input type='text' name='txtModule' value="<?php echo ( $permitId ? $module : $initPERModule );?>">
						</td>
					</tr>
					<tr>
						<td>
							Permission name:
						</td>
						<td>
							<input type='text' name='txtPermission' value="<?php echo ( $permitId ? $permission : $initPERPermission );?>">
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveUser" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='permission.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidPermitid' value="<?php echo $permitId;?>">
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>