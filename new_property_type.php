<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_property_type.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$propertyTypeId = $_GET['id'];

				if($propertyTypeId)
				{ 
					echo "<title>Asset Type - Edit</title>";

					$qry = mysqli_prepare( $db, "CALL sp_PropertyType_Query(?)" );
					mysqli_stmt_bind_param( $qry, 'i', $propertyTypeId );
					$qry->execute();
					$result = mysqli_stmt_get_result( $qry );
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_type.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$propertyId = $row['id'];
							$propertyType = htmlspecialchars($row['property_type']);
							$db_compounds = $row['compounds'];
							$db_pipes = $row['pipes'];
							$db_corporate = $row['corporate'];
							$db_ppr = $row['ppr'];
							$active = $row['active'];
							$comments = htmlspecialchars($row['comments']);
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
						$db->next_result();
						$result->close();
					}

		############ .............
					$qryPI = "SELECT id from comsys.property_types";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_type.php'.'</td><td>'.$processErrorPI.' near line 62.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($propertyTypeId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_property_type.php</td><td>The user tries to edit a non-existing property_type_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					
				}
				else
				{
					echo "<title>Asset Type - Add</title>";

				}
			}
			
		?>
	</head>
	<body>

		<form method='post' action='process_new_property_type.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if( $propertyTypeId ){
					if( $_SESSION['property_type_edit'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['add_propertytype'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">
			
				<span> <h3> <?php echo ( $propertyTypeId ? "Edit ".$propertyType : "New Property Type" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Asset Type:</td>
						<td>
							<input type="text" name="txtNewPropertyType" value="<?php echo ( $propertyTypeId ? $propertyType : "" );?>">
						</td>
					</tr>
					<tr>
						<td>Applicable To:</td>
						<td colspan="3">
							<input type="checkbox" name="chkCmpds" id="chkCmpds" 
								<?php echo ( $propertyTypeId ? ( $db_compounds ? "checked" : "" ) : "" );?>>
								<label for="chkCmpds">Compounds</label>

							<input type="checkbox" name="chkPips" id="chkPips"
								<?php echo ( $propertyTypeId ? ( $db_pipes ? "checked" : "" ) : "" );?>>
								<label for="chkPips">Pipes</label>

							<input type="checkbox" name="chkCorp" id="chkCorp"
								<?php echo ( $propertyTypeId ? ( $db_corporate ? "checked" : "" ) : "" );?>>
								<label for="chkCorp">Corporate</label>

							<input type="checkbox" name="chkPPR" id="chkPPR"
								<?php echo ( $propertyTypeId ? ( $db_ppr ? "checked" : "" ) : "" );?>>
								<label for="chkPPR">PPR</label>							
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type="checkbox" name="chkActive" <?php echo ( $propertyTypeId ? ( $active ? "checked" : "" ) : ( $Active ? "checked" : "" ) );?>>
						</td>
					</tr>
					<tr>
						<td valign="top">Comments:</td>
						<td>
							<textarea name="txtComments"><?php
								if ( $propertyTypeId ){
									echo $comments;
								}else{
									echo $Comments;
								}
							?></textarea>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSavePropertyType" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='property_type.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidPropertyTypeId" value="<?php echo $propertyTypeId;?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $propertyTypeId ? $createdAt : date("Y-m-d H:i:s") );?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $propertyTypeId ? $createdId : $_SESSION["SESS_USER_ID"] );?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
