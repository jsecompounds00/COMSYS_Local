<html>
	<head>
		<?php
			require("/include/database_connect.php");
			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_user.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$userId = $_GET['id'];

				if($userId)
				{ 
					
					$qry = mysqli_prepare($db, "CALL sp_User_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $userId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_user.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							// $userId = $row['user_id'];
							$fname = htmlspecialchars($row['fname']);
							$lname = htmlspecialchars($row['lname']);
							$email = htmlspecialchars($row['email']);
							$username = htmlspecialchars($row['username']);
							$role_id = '0,'.$row['role_id'];
							$role = array();
							$role = explode(',', $role_id);
							$dept1 = $row['department_id_1'];
							$dept2 = $row['department_id_2'];
							$active = $row['active'];
							$pass = $row['userpass'];
							$userpass = convert_uudecode($pass);
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
							$db_compounds = $row['compounds'];
							$db_pipes = $row['pipes'];
							$db_corporate = $row['corporate'];
							$db_ppr = $row['ppr'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT user_id from comsys.login";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_user.php'.'</td><td>'.$processErrorPI.' near line 65.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['user_id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($userId, $id, TRUE) ){	//
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_user.php</td><td>The user tries to edit a non-existing user_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
						
					echo "<title>User - Edit</title>";
					
				}
				else
				{

					echo "<title>User - Add</title>";

				}
			}
		?>
		<script src="js/jscript.js"></script> 
	</head>
	<body <?php if ( !$userId ){ ?>onload="showRoles(), showDepartmentNew(0,0)" <?php }?>>

		<form method="post" action="process_new_user.php">

			<?php
				require '/include/header.php';
				require("/include/init_unset_values/user_init_value.php");

				if ( $userId ){
					if( $_SESSION['edit_user'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{	
					if( $_SESSION['add_user'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}				
				}

			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $userId ? "Edit ".$fname." ".$lname : "New User" ); ?> </h3> </span>

				<?php

					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {

						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);

					}

				?>

				<table class="parent_tables_form">
					<tr>
					    <td>First Name:</td>
					    <td>
					    	<input type="<?php echo ( $userId? ( $userId < 1000 ? "hidden" : "text" ) : "text" ); ?>" name="txtFirstName" value="<?php echo ( $userId  ? $fname : $initUSRFirstName ); ?>">
					    	<?php echo ( $userId? ( $userId < 1000 ? "<b>".$fname."</b>" : "" ) : "" ); ?>
					    </td>
					</tr>
					<tr>
					    <td>Last Name:</td>
					    <td>
					    	<input type="<?php echo ( $userId? ( $userId < 1000 ? "hidden" : "text" ) : "text" ); ?>" name="txtLastName" value="<?php echo ( $userId  ? $lname : $initUSRLastName ); ?>">
					    	<?php echo ( $userId? ( $userId < 1000 ? "<b>".$lname."</b>" : "" ) : "" ); ?>
					    </td>
					</tr>
					<tr>
					    <td>Email Address:</td>
					    <td>
					    	<input type="text" name="txtEmail" value="<?php echo ( $userId ? $email : "" );?>">
					    	<label class="instruction">(user@example.com)</label>
					    </td>
					</tr>
					<tr>
						<td> <?php echo ( $userId ? "" : "Username:" ); ?> </td>
						<td> 
							<input type="<?php echo ( $userId? "hidden" : "text" ); ?>" name="txtUserName" value="<?php echo ( $userId? $username : $initUSRUserName ); ?>">
						</td>
					</tr>
					<tr>
					    <td>Password:</td>
					    <td <?php echo ( $userId ? "colspan=3" : "" ); ?>>
					    	<input type="password" name="txtUserpass">
					    	<?php if($userId) ?> <label class="instruction"> (Leave blank if you do not want to change the password)</label>
					    	<input type="hidden" name="txthidUserpass" value="<?php echo $userpass;?>">
					    </td>
					</tr>
					<tr>
					    <td>Confirm Password:</td>
					    <td>
					    	<input type="password" name="txtConfUserpass">
					    </td>
					</tr>
					<tr>
						<td> Applicable to: </td>
						<td>
							<input type="checkbox" name="chkCompounds" id="Compounds" value="Compounds" 
								onchange="showRoles(), showDepartmentNew(0,0)" 
								<?php echo ( $userId ? ( $db_compounds ? "checked" : "" ) : ( $initUSRCompounds ? "checked" : "" ) ); ?> >
								<label for="Compounds"> Compounds</label>

							<input type="checkbox" name="chkPipes" id="Pipes" value="Pipes" 
								onchange="showRoles(), showDepartmentNew(0,0)" 
								<?php echo ( $userId ? ( $db_pipes ? "checked" : "" ) : ( $initUSRPipes ? "checked" : "" ) ); ?> >
								<label for="Pipes"> Pipes</label>

							<input type="checkbox" name="chkCorporate" id="Corporate" value="Corporate" 
								onchange="showRoles(), showDepartmentNew(0,0)" 
								<?php echo ( $userId ? ( $db_corporate ? "checked" : "" ) : ( $initUSRCorporate ? "checked" : "" ) ); ?> >
								<label for="Corporate"> Corporate</label>

							<input type="checkbox" name="chkPPR" id="PPR" value="PPR" 
								onchange="showRoles(), showDepartmentNew(0,0)" 
								<?php echo ( $userId ? ( $db_ppr ? "checked" : "" ) : ( $initUSRPPR ? "checked" : "" ) ); ?> >
								<label for="PPR"> PPR</label>
						</td>
					</tr>
					<tr>
						<td> Department: </td>
						<td> 
							<select name="sltDept1" id="sltDept1" onchange="showRoles()">
								<?php
						    		if($userId)
									{
										$qryD = mysqli_prepare($db, "CALL sp_Department_Dropdown(1, ?, ?, ?, ?)");
										mysqli_stmt_bind_param($qryD, "iiii", $db_compounds, $db_pipes, $db_corporate, $db_ppr);
										$qryD->execute();
										$resultD = mysqli_stmt_get_result($qryD);
							    		$processError2 = mysqli_error($db);

										if(!empty($processError2))
										{
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_user.php"."</td><td>".$processError2." near line 177.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultD))
											{
												
												$deptID = $row["id"];
												$deptCode = $row["code"];

												if ($deptID == $dept1 && $deptID != 0){
													echo "<option value='".$deptID."' selected>".$deptCode."</option>";
												}else {
													echo "<option value='".$deptID."'>".$deptCode."</option>";
												}
											}
											$db->next_result();
											$resultD->close();
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>  </td>
						<td> 
							<select name="sltDept2" id="sltDept2" onchange="showRoles()">
								<?php
						    		if($userId)
									{
										$qryD = mysqli_prepare($db, "CALL sp_Department_Dropdown(1, ?, ?, ?, ?)");
										mysqli_stmt_bind_param($qryD, "iiii", $db_compounds, $db_pipes, $db_corporate, $db_ppr);
										$qryD->execute();
										$resultD = mysqli_stmt_get_result($qryD);
							    		$processError2 = mysqli_error($db);

										if(!empty($processError2))
										{
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_user.php"."</td><td>".$processError2." near line 177.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultD))
											{
												
												$deptID = $row["id"];
												$deptCode = $row["code"];

												if ($deptID == $dept2 && $deptID != 0){
													echo "<option value='".$deptID."' selected>".$deptCode."</option>";
												}else {
													echo "<option value='".$deptID."'>".$deptCode."</option>";
												}
											}
											$db->next_result();
											$resultD->close();
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr height="20"></tr>
					<tr valign="top">
						<td> Role: </td>
						<td id="tdRoles"> 
							<?php
								if ( $userId ){
									$qryR = mysqli_prepare($db, "CALL sp_Role_Dropdown(?, ?, ?, ?, ?, ?)");
									mysqli_stmt_bind_param($qryR, "iiiiii", $dept1, $dept2, $db_compounds, $db_pipes, $db_corporate, $db_ppr);
									$qryR->execute();
									$resultR = mysqli_stmt_get_result($qryR);
						    		$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_user.php"."</td><td>".$processError1." near line 126.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultR))
										{
											$roleID = array();
											$roleID[] = $row["id"];
											$roleName = $row["name"];

											foreach($roleID as $key => $itemValue) {
												if(array_search($itemValue, $role)){
													echo"<input type='checkbox' name='chkRole[]' value='".$itemValue."' checked> <label> ".$roleName." </label>
														 <br>
														";
												}
												else
													echo"<input type='checkbox' name='chkRole[]' value='".$itemValue."'> <label> ".$roleName." </label>
														 <br>
														";
											}
										}
										$db->next_result();
										$resultR->close();
									}
								}
					    	?>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type="checkbox" name="chkActive" <?php echo ( $userId ? ( $active ? "checked" : "" ) : ( $initUSRActive ? "checked" : "" ) ); ?>>
						</td>
					</tr>
					<tr class="align_bottom">
						<td >
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveUser" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='user.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidUserid" value="<?php echo $userId; ?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $userId ? $createdAt : date("Y-m-d H:i:s") ); ?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $userId ? $createdId : 0 ); ?>">
						</td>
					</tr>
				</table>

			</div>

		</form>
		<footer>
			<?php	
				require("include/database_close.php");
			?>
		</footer>
	</body>
</html>