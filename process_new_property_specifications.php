<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_property_specifications.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidPropertyID = $_POST['hidPropertyID'];

############# Input Validation
		// foreach ($_POST['txtSpecifications'] as $Specs) {
		// 	$Specs = array();
		// 	$Specs = $_POST['txtSpecifications'];
		// }
		// $ctr = count($Specs);

		foreach ($_POST['txtSpecifications'] as $key => $specsValue) {
			$SpecificationID[$key] = $_POST['hidSpecificationID'][$key];
			$Specifications[$key] = $_POST['txtSpecifications'][$key];
			$Good[$key] = $_POST['chkGood'][$key];
			$Damaged[$key] = $_POST['chkDamaged'][$key];

			if ( isset($Good[$key]) && $Good[$key] == 'Good' ){
				$Good[$key] = 1;
			}else{
				$Good[$key] = 0;
			}//end of if ( isset($Condition[$key]) && $Condition[$key] == 'Good' )

			if ( isset($Damaged[$key]) && $Damaged[$key] == 'Damaged' ){
				$Damaged[$key] = 1;
			}else{
				$Damaged[$key] = 0;
			}//end of if ( isset($Condition[$key]) && $Condition[$key] == 'Damaged' )

			if ( $SpecificationID[$key] ){
				$CreatedAt[$key] = $_POST['hidCreatedAt'][$key];
				$CreatedID[$key] = $_POST['hidCreatedID'][$key];
				$updatedAt[$key] = date('Y-m-d H:i:s');
				$updatedId[$key] = $_SESSION['SESS_USER_ID'];
			}else{
				$CreatedAt[$key] = date('Y-m-d H:i:s');
				$CreatedID[$key] = $_SESSION['SESS_USER_ID'];
				$updatedAt[$key] = NULL;
				$updatedId[$key] = NULL;
			}//end of if ( $SpecificationID[$key] ) else

############# If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("Location: new_property_specifications.php?id=$hidPropertyID");
				exit();
			}//end of if($errflag)

############# Commiting to Database		
			if ( $Specifications[$key] != '' ){
				$qry = mysqli_prepare($db, "CALL sp_Company_Property_Specifications_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ? )");
				mysqli_stmt_bind_param($qry, 'iisiissii', $SpecificationID[$key], $hidPropertyID, $Specifications[$key], $Good[$key]
														, $Damaged[$key], $CreatedAt[$key], $updatedAt[$key], $CreatedID[$key]
														, $updatedId[$key]);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry); 
				$processError = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_property_specifications.php'.'</td><td>'.$processError.' near line 116.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{ 
					if( $hidPropertyID ){
						$_SESSION['SUCCESS']  = 'Successfully updated asset specifications.';
					}else{
						$_SESSION['SUCCESS']  = 'Successfully added asset specifications.';
					}
					header("location:property_transfer.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
				}//end of if(!empty($processError) else

			}// end of if ( $Specifications[$key] != '' )

		} // end of foreach ($_POST['txtSpecifications'] as $key => $specsValue)

		require("include/database_close.php");

	}
?>