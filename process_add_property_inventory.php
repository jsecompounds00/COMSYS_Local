<?php
	##########Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	##########Array to store validation errors
	$errmsg_arr = array();
 
	##########Validation error flag
	$errflag = false;
 
	##########Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_add_property_inventory.php'.'</td><td>'.$error.' near line 30.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$hidAddInventoryID = $_POST['hidAddInventoryID'];
		$sltInventorySource = $_POST['sltInventorySource'];
		$txtOtherSource = $_POST['txtOtherSource'];
		$txtReferenceNo = $_POST['txtReferenceNo'];
		$txtReferenceDate = $_POST['txtReferenceDate'];
		// $sltAssetType = $_POST['sltAssetType']);	
		$sltReceivedType = $_POST['sltReceivedType'];
		$sltReceivedFrom = $_POST['sltReceivedFrom'];
		// $txtPurpose = $_POST['txtPurpose'];
		$hidInventoryType = $_POST['hidInventoryType'];


		if($hidAddInventoryID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
			$updatedAt = date('Y-m-d H:i:s');
		if($hidAddInventoryID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
			$updatedId = $_SESSION['SESS_USER_ID'];

		foreach ($_POST['hidAddInventoryItemID'] as $AddInventoryItemID) {
			$AddInventoryItemID = array();
			$AddInventoryItemID = $_POST['hidAddInventoryItemID'];
		}
		foreach ($_POST['sltAssetCondition'] as $AssetCondition) {
			$AssetCondition = array();
			$AssetCondition = $_POST['sltAssetCondition'];
		}
		foreach ($_POST['sltAsset'] as $Asset) {
			$Asset = array();
			$Asset = $_POST['sltAsset'];
		}
		foreach ($_POST['sltAssetSpecification'] as $AssetSpecification) {
			$AssetSpecification = array();
			$AssetSpecification = $_POST['sltAssetSpecification'];
		}
		foreach ($_POST['txtQuantityReceived'] as $QuantityReceived) {
			$QuantityReceived = array();
			$QuantityReceived = $_POST['txtQuantityReceived'];
		}
		foreach ($_POST['txtUnitPrice'] as $UnitPrice) {
			$UnitPrice = array();
			$UnitPrice = $_POST['txtUnitPrice'];
		}
		foreach ($_POST['txtRemarks'] as $Remarks) {
			$Remarks = array();
			$Remarks = $_POST['txtRemarks'];
		}
		foreach ($_POST['sltAssetType'] as $AssetType) {
			$AssetType = array();
			$AssetType = $_POST['sltAssetType'];
		}
		foreach ($_POST['txtRefPTSNumber'] as $RefPTSNumber) {
			$RefPTSNumber = array();
			$RefPTSNumber = $_POST['txtRefPTSNumber'];
		}
		foreach ($_POST['txtRefRRNumber'] as $RefRRNumber) {
			$RefRRNumber = array();
			$RefRRNumber = $_POST['txtRefRRNumber'];
		}
		// foreach ($_POST['chkDelete'] as $delete) {
		// 	$delete = array();
			
		// 	if ( $hidSupplyTransId ){
		// 		$delete = $_POST['chkDelete'];
		// 	}else{
		// 		$delete = 0;
		// 	}
		// }

		##############---------- INPUT VALIDATION
		if ( !$sltInventorySource ){
			$errmsg_arr[] = '* Select inventory source.';
			$errflag = true;
		}elseif($sltInventorySource == 'Others' && $txtOtherSource == ''){
			$errmsg_arr[] = '* Please specify inventory source.';
			$errflag = true;
		}elseif($sltInventorySource != 'Others' && $txtOtherSource != ''){
			$txtOtherSource = '';
		}

		// if ( $txtReferenceNo == '' ){
		// 	$errmsg_arr[] = '* Reference number cannot be blank.';
		// 	$errflag = true;
		// }

		$valReferenceDate = validateDate($txtReferenceDate, 'Y-m-d');
		if ( $valReferenceDate != 1 ){
			$errmsg_arr[] = '* Invalid reference date.';
			$errflag = true;
		}

		// if ( !$sltAssetType ){
		// 	$errmsg_arr[] = '* Select asset type.';
		// 	$errflag = true;
		// }
		if ( !$sltReceivedType && $sltInventorySource != 'Others' ){
			$errmsg_arr[] = '* Select received type.';
			$errflag = true;
		}
		if ( !$sltReceivedFrom && $sltInventorySource != 'Others' ){
			$errmsg_arr[] = '* Select received from.';
			$errflag = true;
		}

		$i = 0;

		do{
			if ( !$AssetType[$i] ){
				if ($i == 0){
					$errmsg_arr[] = '* Select asset type (line '.($i+1).')';
					$errflag = true;
				}
				else{
					if ( $Asset[$i] || $AssetCondition[$i] || $sltAssetSpecification || $QuantityReceived[$i] != '' || $UnitPrice[$i] != '' ){
						$errmsg_arr[] = '* Select asset type. (line '.($i+1).')';
						$errflag = true;
					}
				}
			}else{
				if ( !$Asset[$i] ){
					if ($i == 0){
						$errmsg_arr[] = '* Select asset (line '.($i+1).')';
						$errflag = true;
					}
					else{
						if ( $AssetCondition[$i] || $sltAssetSpecification || $QuantityReceived[$i] != '' || $UnitPrice[$i] != '' ){
							$errmsg_arr[] = '* Select asset. (line '.($i+1).')';
							$errflag = true;
						}
					}
				}else{
					if ( $Asset[$i-1] == 0 ){
						if ( $i !=0 ){
							$errmsg_arr[] = '* Skip Line is not allowed';
							$errflag = true;
						}
					}
					if ( !$AssetCondition[$i] ){
						$errmsg_arr[] = '* Select asset condition. (line '.($i+1).')';
						$errflag = true;
					}
					if ( !$AssetSpecification[$i] ){
						$errmsg_arr[] = '* Select asset specification. (line '.($i+1).')';
						$errflag = true;
					}
					if ( !is_numeric($QuantityReceived[$i]) ){
						$errmsg_arr[] = '* Invalid quantity. (line '.($i+1).')';
						$errflag = true;
					}
					if ( $UnitPrice[$i] != '' && !is_numeric($UnitPrice[$i]) ){
						$errmsg_arr[] = '* Invalid unit price. (line '.($i+1).')';
						$errflag = true;
					}
				}
			}
			$i++;

		}while ($i < 10);

		##############---------- INPUT VALIDATION

		$counter2 = 10;

		$_SESSION['sltInventorySource'] = $sltInventorySource;
		$_SESSION['txtReferenceNo'] = htmlspecialchars($txtReferenceNo);
		$_SESSION['txtReferenceDate'] = htmlspecialchars($txtReferenceDate);
		// $_SESSION['sltAssetType'] = $sltAssetType;
		$_SESSION['sltReceivedType'] = $sltReceivedType;
		$_SESSION['sltReceivedFrom'] = $sltReceivedFrom;
		// $_SESSION['txtPurpose'] = $txtPurpose;

		for ( $i = 0; $i < $counter2; $i++ ) {
			$_SESSION['AssetType'][$i] = $AssetType[$i];
			$_SESSION['sltAsset'][$i] = $Asset[$i];
			$_SESSION['sltAssetSpecification'][$i] = $AssetSpecification[$i];
			$_SESSION['txtQuantityReceived'][$i] = htmlspecialchars($QuantityReceived[$i]);
			$_SESSION['txtUnitPrice'][$i] = htmlspecialchars($UnitPrice[$i]);
			$_SESSION['txtRemarks'][$i] = htmlspecialchars($Remarks[$i]);
			$_SESSION['txtRefPTSNumber'][$i] = htmlspecialchars($RefPTSNumber[$i]);
		}

		##########If there are input validations, redirect back to the login form
		if($errflag) 
		{
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:add_property_inventory.php?id=".$hidAddInventoryID);
			exit();
		} 

		############# Committing in Database
			$qry = mysqli_prepare($db, "CALL sp_Property_Transfer_Trans_Add_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			mysqli_stmt_bind_param($qry, 'issssssissii', $hidAddInventoryID, $hidInventoryType, $sltInventorySource, $txtOtherSource
														, $txtReferenceNo, $txtReferenceDate, $sltReceivedType
														, $sltReceivedFrom, $createdAt, $updatedAt, $createdId, $updatedId);
			$qry->execute();
			$result = mysqli_stmt_get_result($qry); //return results of query
			$processError = mysqli_error($db);

			if ( !empty($processError) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_add_property_inventory.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

				while($row = mysqli_fetch_assoc($qryLastId))
				{
					if ( $hidAddInventoryID ) {
						$AddInventoryID = $hidAddInventoryID;
					}else{
						$AddInventoryID = $row['LAST_INSERT_ID()'];
					}	
				}
				
				if(mysqli_affected_rows($db) > 0 ) 
				{
					foreach($_POST['sltAsset'] as $key => $itemValue)
					{	
						if (!$hidAddInventoryID) {
							$AddInventoryItemID = 0;
						}

						if ($itemValue)
						{
							$qryItems = mysqli_prepare($db, "CALL sp_Property_Transfer_Trans_Items_Add_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
							mysqli_stmt_bind_param($qryItems, 'iiiiddssiss', $AddInventoryItemID[$key], $AddInventoryID, $Asset[$key]
																	   , $AssetSpecification[$key], $QuantityReceived[$key]
																	   , $UnitPrice[$key], $Remarks[$key], $AssetCondition[$key], $AssetType[$key]
																	   , $RefPTSNumber[$key], $RefRRNumber[$key]);
							$qryItems->execute();
							$result = mysqli_stmt_get_result($qryItems); //return results of query
							$processError1 = mysqli_error($db);

							if ( !empty($processError1) ){
								error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_add_property_inventory.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
								header("location: error_message.html");
							}else{
								if ( $hidAddInventoryID )
									$_SESSION['SUCCESS']  = 'Transaction successfully updated.';
								else
									$_SESSION['SUCCESS']  = 'Transaction successfully added.';
								header("location: property_transfer.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
							}				
						}
					}
				}
			}	
		############# Committing in Database		

		require("include/database_close.php"); 
	}
?>