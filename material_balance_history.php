<html>
	<head>
		<title> Material Balance - History</title>
		<?php
			require("include/database_connect.php");

			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone='';

			$FGID = $_GET['id'];

			$qryCPH = mysqli_prepare($db, "CALL sp_Material_Balance_History(?, NULL, NULL)");
			mysqli_stmt_bind_param($qryCPH, 'i', $FGID);
			$qryCPH->execute();
			$resultCPH = mysqli_stmt_get_result($qryCPH);
			$total_results = mysqli_num_rows($resultCPH); //return number of rows of result

			$db->next_result();
			$resultCPH->close();

			$targetpage = "material_balance_history.php"; 	//your file name  (the name of this file)
			require("include/paginate_hist.php");

			$qry = mysqli_prepare($db, "CALL sp_Material_Balance_History(?, ?, ?)");
			mysqli_stmt_bind_param($qry, 'iii', $FGID, $start, $end);
			$qry->execute();
			$result = mysqli_stmt_get_result($qry);

			$processError = mysqli_error($db);
			
			if(!empty($processError))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_history.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
					$MaterialBalanceID = array();
					$Customer = array();
					$LotNumber = array();
					$ProductionStart = array();
					$ProductionEnd = array();
					$TotalProductiveMins = array();
					$RequiredOutput = array();
					$GoodOutput = array();
					$Underpack = array();
					$TotalScrap = array();
					$MachineRate = array();
					$MachinePerformance = array();
					$MaterialYield = array();
					$TotalInput = array();
				while ( $row = mysqli_fetch_assoc( $result ) ){
					$FGItem = $row["FGItem"];

					$MaterialBalanceID[] = $row["MaterialBalanceID"];
					$Customer[] = $row["Customer"];
					$LotNumber[] = $row["LotNumber"];
					$ProductionStart[] = $row["ProductionStart"];
					$ProductionEnd[] = $row["ProductionEnd"];
					$TotalProductiveMins[] = $row["TotalProductiveMins"];
					$RequiredOutput[] = $row["RequiredOutput"];
					$GoodOutput[] = $row["GoodOutput"];
					$Underpack[] = $row["Underpack"];
					$TotalScrap[] = $row["TotalScrap"];
					$MachineRate[] = $row["MachineRate"];
					$MachinePerformance[] = $row["MachinePerformance"];
					$MaterialYield[] = $row["MaterialYield"];
					$TotalInput[] = $row["TotalInput"];
				}
				$db->next_result();
				$result->close();
			}

			############ .............

			$qryPI = "SELECT id from comsys.finished_goods";
			$resultPI = mysqli_query($db, $qryPI); 
			$processErrorPI = mysqli_error($db);

			if ( !empty($processErrorPI) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_history.php'.'</td><td>'.$processErrorPI.' near line 57.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
					$id = array();
				while($row = mysqli_fetch_assoc($resultPI)){
					$id[] = $row['id'];
				}
			}
			$db->next_result();
			$resultPI->close();

			############ .............
			if( !in_array($FGID, $id, TRUE) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_history.php</td><td>The user tries to view transaction history of a non-existing finished good id.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		?>
	</head>
	<body>

		<?php
			require '/include/header.php';

			// if( $_SESSION['material_balance_history'] == false) 
			// {
			// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }
		?>

		<input type='hidden' name='page' value=$page>

		<div class="wrapper">
			
			<span> <h2> Production History for <?php echo $FGItem;?> </h2> </span>

			<a href='pam_analysis.php?page=1&search=&qsone=' class='back'><img src='images/back.png' height="20" name='txtBack'> Back</a>

			<table class="home_pages">
				<tr>
					<td colspan='14'>
						<?php echo $pagination;?>
					</td>
				</tr>
				<tr>
					<th> Customer </th>
					<th> Lot# </th>
					<th colspan="2"> Production Date & Time </th>
					<th> Total <br> Productive Mins. </th>
					<th> Required <br> Output </th>
					<th> Total <br> Input </th>
					<th> Good <br> Output </th>
					<th> Underpack </th>
					<th> Total <br> Scrap </th>
					<th> M/C <br> Rate </th>
					<th> M/C <br> Performance, % </th>
					<th> Material <br> Yield, % </th>
					<th></th>
				</tr>
				<?php
					foreach ($MaterialBalanceID as $key => $MatlBalID) {
				?>
						<tr>
							<td> <?php echo $Customer[$key]; ?> </td>
							<td> <?php echo $LotNumber[$key]; ?> </td>
							<td> <?php echo $ProductionStart[$key]; ?> </td>
							<td> <?php echo $ProductionEnd[$key]; ?> </td>
							<td> <?php echo $TotalProductiveMins[$key]; ?> </td>
							<td> <?php echo number_format((float)($RequiredOutput[$key]), 2, '.', ','); ?> </td>
							<td> <?php echo number_format((float)($TotalInput[$key]), 5, '.', ','); ?> </td>
							<td> <?php echo number_format((float)($GoodOutput[$key]), 2, '.', ','); ?> </td>
							<td> <?php echo $Underpack[$key]; ?> </td>
							<td> <?php echo $TotalScrap[$key]; ?> </td>
							<td> <?php echo $MachineRate[$key]; ?> </td>
							<td> <?php echo $MachinePerformance[$key]; ?> </td>
							<td> <?php echo $MaterialYield[$key]; ?> </td>
							<td>
								<input type="button" value="Edit" onclick="location.href='new_material_balance_computation.php?fg_id=<?php echo $FGID;?>&mbc_id=<?php echo $MaterialBalanceID[$key];?>'">
							</td>
						</tr>
				<?php
					}
				?>
				<tr>
					<td colspan='14'>
						<?php echo $pagination;?>
					</td>
				</tr>
			</table>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>