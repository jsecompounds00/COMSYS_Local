 <?php

$sltToDepartment = intval($_GET['sltToDepartment']);
$addInventoryID = intval($_GET['addInventoryID']);
$userID = intval($_GET['userID']);
$division = strval($_GET['division']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pts_receiver_dropdown.php'.'</td><td>'.$error.' near line 13.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qry = mysqli_prepare( $db, "CALL sp_User_Dropdown(?, 0, 1, ?)" );
		mysqli_stmt_bind_param( $qry, 'is', $sltToDepartment, $division );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pts_receiver_dropdown.php'.'</td><td>'.$processError.' near line 24.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
				echo "<option></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				
				$user_id = $row['user_id'];
				$IssuedTo = $row['User'];
				
				if ( $userID == $user_id ){
					echo "<option value='".$user_id."' selected>".$IssuedTo."</option>";
				}else{
					echo "<option value='".$user_id."'>".$IssuedTo."</option>";
				}
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>