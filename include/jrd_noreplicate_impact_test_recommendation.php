<?php

	require("database_connect.php");

	$qryIT = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Impact_Test_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qryIT, 'is', $TypeID, $Type);
	$qryIT->execute();
	$resultIT = mysqli_stmt_get_result($qryIT);
	$processErrorIT = mysqli_error($db);

?>
	
	<table class="results_child_tables_form">
		<?php
			if ( !empty($processErrorIT) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_impact_test_recommendation.php'.'</td><td>'.$processErrorIT.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultIT)){
					$ITJRDTestID = $row['ITJRDTestID'];
					$ITJRDFGSoft = $row['ITJRDFGSoft'];

					$ITClientsProduct = $row['ITClientsProduct']; 
					$ITCACCProduct = $row['ITCACCProduct']; 

					$ITClientsDropWeight = $row['ITClientsDropWeight']; 
					$ITCACCDropWeight = $row['ITCACCDropWeight']; 

					$ITClientsDropHeight = $row['ITClientsDropHeight']; 
					$ITCACCDropHeight = $row['ITCACCDropHeight']; 

					$ITClientsNoOfSample = $row['ITClientsNoOfSample']; 
					$ITCACCNoOfSample = $row['ITCACCNoOfSample']; 

					$ITClientsNoOfPassed = $row['ITClientsNoOfPassed']; 
					$ITCACCNoOfPassed = $row['ITCACCNoOfPassed']; 
		?>
					<tr>
						<th></th>
						<th> <?php echo $ITClientsProduct; ?> </th>
						<th> <?php echo $ITCACCProduct; ?> </th>
					</tr>
				<!-- ###################### Weight of Drop	 -->	
					<tr>
						<td> Weight of Drop </td>
						<td> <?php echo $ITClientsDropWeight; ?> </td>
						<td> <?php echo $ITCACCDropWeight; ?> </td>
					</tr>
				<!-- ###################### Height of Drop	 -->	
					<tr>
						<td> Height of Drop </td>
						<td> <?php echo $ITClientsDropHeight; ?> </td>
						<td> <?php echo $ITCACCDropHeight; ?> </td>
					</tr>
				<!-- ###################### Actual	 -->	
					<tr>
						<td> Actual </td>
						<td> <?php echo $ITClientsNoOfSample.'/'.$ITClientsNoOfPassed; ?> </td>
						<td> <?php echo $ITCACCNoOfSample.'/'.$ITCACCNoOfPassed; ?> </td>
					</tr>
		<?php
				}
				$db->next_result();
				$resultIT->close();
			}
		?>
	</table>
	<br>
<?php	

	require("database_close.php");
?>