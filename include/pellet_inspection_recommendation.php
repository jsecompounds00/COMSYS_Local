<?php

	require("database_connect.php");

	$qryPI = mysqli_prepare($db, "CALL sp_BAT_Pellet_Inspection_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qryPI, 'is', $TypeID, $Type);
	$qryPI->execute();
	$resultPI = mysqli_stmt_get_result($qryPI);
	$processErrorPI = mysqli_error($db);

		if ( !empty($processErrorPI) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pellet_inspection_recommendation.php'.'</td><td>'.$processErrorPI.' near line 12.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultPI)){
				$PItrial_no = $row['PItrial_no'];
				$PIFGName = $row['PIFGName'];

				$PITrial1 = $row['PITrial1']; $PITrial2 = $row['PITrial2']; $PITrial3 = $row['PITrial3']; $PITrial4 = $row['PITrial4']; $PITrial5 = $row['PITrial5'];
				$PITrial6 = $row['PITrial6']; $PITrial7 = $row['PITrial7']; $PITrial8 = $row['PITrial8']; $PITrial9 = $row['PITrial9']; $PITrial10 = $row['PITrial10'];
				$PITrial11 = $row['PITrial11']; $PITrial12 = $row['PITrial12']; $PITrial13 = $row['PITrial13']; $PITrial14 = $row['PITrial14']; $PITrial15 = $row['PITrial15'];
				$PITrial16 = $row['PITrial16']; $PITrial17 = $row['PITrial17']; $PITrial18 = $row['PITrial18']; $PITrial19 = $row['PITrial19']; $PITrial20 = $row['PITrial20'];

				$ClarityColor1 = $row['ClarityColor1']; $ClarityColor2 = $row['ClarityColor2']; $ClarityColor3 = $row['ClarityColor3']; $ClarityColor4 = $row['ClarityColor4']; $ClarityColor5 = $row['ClarityColor5'];
				$ClarityColor6 = $row['ClarityColor6']; $ClarityColor7 = $row['ClarityColor7']; $ClarityColor8 = $row['ClarityColor8']; $ClarityColor9 = $row['ClarityColor9']; $ClarityColor10 = $row['ClarityColor10'];
				$ClarityColor11 = $row['ClarityColor11']; $ClarityColor12 = $row['ClarityColor12']; $ClarityColor13 = $row['ClarityColor13']; $ClarityColor14 = $row['ClarityColor14']; $ClarityColor15 = $row['ClarityColor15'];
				$ClarityColor16 = $row['ClarityColor16']; $ClarityColor17 = $row['ClarityColor17']; $ClarityColor18 = $row['ClarityColor18']; $ClarityColor19 = $row['ClarityColor19']; $ClarityColor20 = $row['ClarityColor20'];
?>
				<table class='results_child_tables_form'>
					<tr>
						<td colspan='<?php echo ($PItrial_no+1); ?>'> <?php  echo $PIFGName; ?> </td>
					</tr>
					<tr>
						<th></th>
						<?php
							if ( $PITrial1 <= $PItrial_no &&$PITrial1!= '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial1; ?> </th>
						<?php		
							}

							if ( $PITrial2 <= $PItrial_no && $PITrial2 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial2; ?> </th>
						<?php		
							}

							if ( $PITrial3 <= $PItrial_no && $PITrial3 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial3; ?> </th>
						<?php		
							}

							if ( $PITrial4 <= $PItrial_no && $PITrial4 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial4; ?> </th>
						<?php		
							}

							if ( $PITrial5 <= $PItrial_no && $PITrial5 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial5; ?> </th>
						<?php		
							}

							if ( $PITrial6 <= $PItrial_no && $PITrial6 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial6; ?> </th>
						<?php		
							}

							if ( $PITrial7 <= $PItrial_no && $PITrial7 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial7; ?> </th>
						<?php		
							}

							if ( $PITrial8 <= $PItrial_no && $PITrial8 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial8; ?> </th>
						<?php		
							}

							if ( $PITrial9 <= $PItrial_no && $PITrial9 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial9; ?> </th>
						<?php		
							}

							if ( $PITrial10 <= $PItrial_no && $PITrial10 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial10; ?> </th>
						<?php		
							}

							if ( $PITrial11 <= $PItrial_no && $PITrial11 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial11; ?> </th>
						<?php		
							}

							if ( $PITrial12 <= $PItrial_no && $PITrial12 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial12; ?> </th>
						<?php		
							}

							if ( $PITrial13 <= $PItrial_no && $PITrial13 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial13; ?> </th>
						<?php		
							}

							if ( $PITrial14 <= $PItrial_no && $PITrial14 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial14; ?> </th>
						<?php		
							}

							if ( $PITrial15 <= $PItrial_no && $PITrial15 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial15; ?> </th>
						<?php		
							}

							if ( $PITrial16 <= $PItrial_no && $PITrial16 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial16; ?> </th>
						<?php		
							}

							if ( $PITrial17 <= $PItrial_no && $PITrial17 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial17; ?> </th>
						<?php		
							}

							if ( $PITrial18 <= $PItrial_no && $PITrial18 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial18; ?> </th>
						<?php		
							}

							if ( $PITrial19 <= $PItrial_no && $PITrial19 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial19; ?> </th>
						<?php		
							}

							if ( $PITrial20 <= $PItrial_no && $PITrial20 != '-' ){
						?>
								<th> <?php echo 'Trial '.$PITrial20; ?> </th>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Clarity / Color Conform	 -->	
					<tr>
						<td> Clarity / Color Conform </td>
						<?php
							if ( $PITrial1 <= $PItrial_no && $PITrial1 != '-' ){
						?>
								<td> <?php echo $ClarityColor1; ?> </td>
						<?php		
							}

							if ( $PITrial2 <= $PItrial_no && $PITrial2 != '-' ){
						?>
								<td> <?php echo $ClarityColor2; ?> </td>
						<?php		
							}

							if ( $PITrial3 <= $PItrial_no && $PITrial3 != '-' ){
						?>
								<td> <?php echo $ClarityColor3; ?> </td>
						<?php		
							}

							if ( $PITrial4 <= $PItrial_no && $PITrial4 != '-' ){
						?>
								<td> <?php echo $ClarityColor4; ?> </td>
						<?php		
							}

							if ( $PITrial5 <= $PItrial_no && $PITrial5 != '-' ){
						?>
								<td> <?php echo $ClarityColor5; ?> </td>
						<?php		
							}

							if ( $PITrial6 <= $PItrial_no && $PITrial6 != '-' ){
						?>
								<td> <?php echo $ClarityColor6; ?> </td>
						<?php		
							}

							if ( $PITrial7 <= $PItrial_no && $PITrial7 != '-' ){
						?>
								<td> <?php echo $ClarityColor7; ?> </td>
						<?php		
							}

							if ( $PITrial8 <= $PItrial_no && $PITrial8 != '-' ){
						?>
								<td> <?php echo $ClarityColor8; ?> </td>
						<?php		
							}

							if ( $PITrial9 <= $PItrial_no && $PITrial9 != '-' ){
						?>
								<td> <?php echo $ClarityColor9; ?> </td>
						<?php		
							}

							if ( $PITrial10 <= $PItrial_no && $PITrial10 != '-' ){
						?>
								<td> <?php echo $ClarityColor10; ?> </td>
						<?php		
							}

							if ( $PITrial11 <= $PItrial_no && $PITrial11 != '-' ){
						?>
								<td> <?php echo $ClarityColor11; ?> </td>
						<?php		
							}

							if ( $PITrial12 <= $PItrial_no && $PITrial12 != '-' ){
						?>
								<td> <?php echo $ClarityColor12; ?> </td>
						<?php		
							}

							if ( $PITrial13 <= $PItrial_no && $PITrial13 != '-' ){
						?>
								<td> <?php echo $ClarityColor13; ?> </td>
						<?php		
							}

							if ( $PITrial14 <= $PItrial_no && $PITrial14 != '-' ){
						?>
								<td> <?php echo $ClarityColor14; ?> </td>
						<?php		
							}

							if ( $PITrial15 <= $PItrial_no && $PITrial15 != '-' ){
						?>
								<td> <?php echo $ClarityColor15; ?> </td>
						<?php		
							}

							if ( $PITrial16 <= $PItrial_no && $PITrial16 != '-' ){
						?>
								<td> <?php echo $ClarityColor16; ?> </td>
						<?php		
							}

							if ( $PITrial17 <= $PItrial_no && $PITrial17 != '-' ){
						?>
								<td> <?php echo $ClarityColor17; ?> </td>
						<?php		
							}

							if ( $PITrial18 <= $PItrial_no && $PITrial18 != '-' ){
						?>
								<td> <?php echo $ClarityColor18; ?> </td>
						<?php		
							}

							if ( $PITrial19 <= $PItrial_no && $PITrial19 != '-' ){
						?>
								<td> <?php echo $ClarityColor19; ?> </td>
						<?php		
							}

							if ( $PITrial20 <= $PItrial_no && $PITrial20 != '-' ){
						?>
								<td> <?php echo $ClarityColor20; ?> </td>
						<?php		
							}
						?>
					</tr>
				</table>
		<?php
				}
				$db->next_result();
				$resultPI->close();
			}

	require("database_close.php");
?>