<?php

$requisitioner_id = intval($_GET['requisitioner_id']);
$approver_dept_id = intval($_GET['approver_dept_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>job_order_approver.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_Job_Order_Superior_Query(?,?)");
		mysqli_stmt_bind_param($qry, 'ii', $requisitioner_id, $approver_dept_id);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>job_order_approver.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			$i = 0;
			while($row = mysqli_fetch_assoc($result))
			{
				$TempParentUserID = $row['TempParentUserID'];
				$row_num = $row['row_num'];
				$ParentEmail = $row['ParentEmail'];
				$ParentName = $row['ParentName'];
				$PropertyDisabled = $row['PropertyDisabled'];
				$PropertyChecked = $row['PropertyChecked'];
				$PropertyEmailDisabled = $row['PropertyEmailDisabled'];
				
?>
				<input type="radio" name="radSuperior" 
					id="<?php echo $TempParentUserID;?>" 
					value="<?php echo $TempParentUserID;?>" 
					<?php echo $PropertyChecked;?>
					onchange="enableEmail(<?php echo $i;?>)">
				<label for="<?php echo $TempParentUserID;?>"> <?php echo $ParentName;?> ( <?php echo $ParentEmail;?> ) </label>
				<input type="hidden" name="hidEmail[]" id="hidEmail<?php echo $i;?>" value="<?php echo $ParentEmail;?>" <?php echo $PropertyEmailDisabled;?>>
				<br>
<?php
				$i++;
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>