<?php
	$sltJRDNumber = intval($_GET["sltJRDNumber"]);
	$BatchTicketID = intval($_GET["hidBatchTicketId"]);
	$BatchTicketTrialID = intval($_GET["hidBatchTicketTrialId"]);
	// $db_BatchTicketTrial = intval($_GET["db_BatchTicketTrial"]);

	session_start();
	require("/database_connect.php");
	require("/init_unset_values/batch_ticket_jrd_init_value.php");

	if($errno)
	{
		$error = mysqli_connect_error();
		error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>jrd_data_dropdown.php"."</td><td>".$error." near line 13.</td></tr>", 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		if ( $sltJRDNumber != 0 ){
			require("/queries_new_batch_ticket_jrd.php");

			$qry = mysqli_prepare($db, "CALL sp_JRD_Data_Query(?)");
			mysqli_stmt_bind_param($qry, "i", $sltJRDNumber);
			$qry -> execute();
			$result = mysqli_stmt_get_result($qry);
			$processError = mysqli_error($db);

			if ($processError){
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>jrd_data_dropdown.php"."</td><td>".$processError." near line 20.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{	
				$formula_type = array();
				$formula_id = array();
				while($row = mysqli_fetch_assoc($result))
				{
					$date_requested = $row["date_requested"];
					$date_needed = $row["date_needed"];
					$type_of_request = $row["type_of_request"];
					$tag_eval_replicate = $row["tag_eval_replicate"];
					$finished_goods = $row["finished_goods"];
					$mod_eval_finished_good_id = $row["mod_eval_finished_good_id"];
					$soft_pvc = $row["soft_pvc"];
					$tag_dev_extrusion = $row["tag_dev_extrusion"];
					$dev_extrusion_type = $row["dev_extrusion_type"];
					$dev_extrusion_type_others = $row["dev_extrusion_type_others"];
					$dev_thermal_rate = $row["dev_thermal_rate"];
					$dev_thermal_rate_others = $row["dev_thermal_rate_others"];
					$tag_dev_molding = $row["tag_dev_molding"];
					$dev_molding_type = $row["dev_molding_type"];
					$dev_molding_type_others = $row["dev_molding_type_others"];
					$tag_dev_others = $row["tag_dev_others"];
					$dev_others = $row["dev_others"];
					$eval_finished_good = $row["eval_finished_good"];
					$formula_type[] = $row["formula_type"];
					$formula_id[] = $row["formula_id"];
				}
				$db->next_result();
				$result->close();
	?>	
				<table class='trials_parent_tables_form' onmouseover="enableExtrusion()">	
					<tr>
						<td> Activity: </td>
						<td> 
							<b> 
								Product <?php echo $type_of_request;?> 
								<?php echo ( $tag_eval_replicate ? "w/ replication" : "" ); ?>
								<input type="hidden" name="hidActivity"  id="hidActivity" value="<?php echo ( $type_of_request == "Development" ? 4 : ( $type_of_request == "Modification" ? 5 : ( $type_of_request == "Evaluation" && $tag_eval_replicate != 1 ? 6 : ( $type_of_request == "Evaluation" && $tag_eval_replicate == 1 ? 7 : 0 ) ) ) ); ?>">
							</b> 
						</td>
						<td> FG Type: </td>
						<td>
							<?php 
								if ( $type_of_request == "Development" ){
							?>
									<input type='radio' name='radFGType' id='Rigid' value='Rigid' onchange='enableExtrusion()' <?php echo ( $BatchTicketID ? ( $db_FGSoft == 1 ? "disabled" : "checked" ) : ( $initFGSoft_JRD == "Rigid" ? "checked" : "" ) ) ?>>
										<label for='Rigid'> <b> RIGID </b> </label>
										
									<input type='radio' name='radFGType' id='Soft' value='Soft' onchange='enableExtrusion()' <?php echo ( $BatchTicketID ? ( $db_FGSoft == 1 ? "checked" : "disabled" ) : ( $initFGSoft_JRD == "Soft" ? "checked" : "" ) ) ?>>
										<label for='Soft'> <b> SOFT </b> </label>
							<?php		
								}elseif ( ( $type_of_request == "Modification" ) || ( $type_of_request == "Evaluation" && $tag_eval_replicate == 1 ) ){
							?>
								<input type='radio' name='radFGType' id='Rigid' value='Rigid' onchange='enableExtrusion()' <?php echo ( $soft_pvc == 1 ? "disabled" : "checked" ); ?>>
									<label for='Rigid'> <b> RIGID </b> </label>
									
								<input type='radio' name='radFGType' id='Soft' value='Soft' onchange='enableExtrusion()' <?php echo ( $soft_pvc == 1 ? "checked" : "disabled" ); ?>>
									<label for='Soft'> <b> SOFT </b> </label>
							<?php
								}
							?>
						</td>
					</tr>
					<tr valign="top">
						<td>
							<?php 
								echo ( $type_of_request == "Modification" ? "Item to be Modified:" : "New Product Name:" ); 
							?>
						</td>
						<td>
							<b>
								<?php 
									if ( $type_of_request == "Development" || $type_of_request == "Evaluation" ){
								?>		
										<input type="text" name="txtNewFGName"  value="<?php echo ( $BatchTicketID ? $db_NewFinishedGood : $initNewFGName_JRD ); ?>">
								<?php
									}elseif ( $type_of_request == "Modification" ){
										echo $finished_goods;
								?>		
										<input type="hidden" name="hidFGID" value="<?php echo $mod_eval_finished_good_id; ?>">
								<?php
									}
								?> 
							</b>
						</td>
						<td>
							<?php 
								echo ( $type_of_request == "Modification" ? "Formula Type:" : ($type_of_request == "Evaluation" ? "" : ($type_of_request == "Development" ? "Product Requirements:" : "" ) ));
							 ?>
						</td>
						<td>
							<b>
								<?php 
									if ( $type_of_request == "Development" ){
										if ( $tag_dev_extrusion == 1 ){
								?>
											Extrusion -> 
											<?php 
												echo ( $dev_extrusion_type == 'OtherExtrusionType' ? $dev_extrusion_type_others : $dev_extrusion_type );
												echo ( !is_null($dev_extrusion_type) && !is_null($dev_thermal_rate) ? ", " : "" );
												echo ( $dev_thermal_rate == 'OtherThermalRate' ? $dev_thermal_rate_others : $dev_thermal_rate );
											?>
											<br>
									<?php			
										}
										if ( $tag_dev_molding == 1 ){
									?>
											Molding -> 
											<?php 
												echo ( $dev_molding_type == 'OtherMoldingType' ? $dev_molding_type_others : $dev_molding_type ); 
											?>
											<br>
									<?php			
										}
										if ( $tag_dev_others == 1 ){
									?>
											Other Requirement: -> 
											<?php 
												echo $dev_others; 
											?>
											<br>
									<?php			
										}
									?>
							</b>

							<?php
								}else{
									if( ($type_of_request == 'Modification') ){  
							?>
										<select name="sltFormulaType" id="sltFormulaType" onchange="showBatchTicketTrialsM()">
											<option></option>
											<?php
												foreach ($formula_type as $key => $formula_type_value) { //( $formula_id[$key] == $db_FormulaTypeID ? "" : "hidden" ) : ""
													if( $BatchTicketID ){
														if ( $BatchTicketTrialID ){
															if ( $formula_id[$key] == $db_FormulaTypeID ){
																echo "<option value=".$formula_id[$key]." selected>".$formula_type_value.'<br>';
															}else{
																echo "<option value=".$formula_id[$key]." hidden>".$formula_type_value.'<br>';
															}
														}else{
															if ( $formula_id[$key] == $db_FormulaTypeID ){
																echo "<option value=".$formula_id[$key]." >".$formula_type_value.'<br>';
															}else{
																echo "<option value=".$formula_id[$key]." hidden>".$formula_type_value.'<br>';
															}
														}
													}else{
														echo "<option value=".$formula_id[$key]." >".$formula_type_value.'<br>';
													}
												}
											?>
										</select>
							<?php 
									}
								} 
							?>
						</td>
					</tr>
					<?php 
						if ( $type_of_request == "Modification" ){ 
					?>
							<tr>
								<td> New Product Name </td>
								<td> <input type="text" name="txtNewFGName"  value="<?php echo ( $BatchTicketID ? $db_NewFinishedGood : $initNewFGName_JRD ); ?>">
							</tr>
					<?php 
						} 

						if ( ( $type_of_request == "Modification" ) || ( $type_of_request == "Development" ) || ( $type_of_request == "Evaluation" && $tag_eval_replicate == 1 ) )
						{
					?>
								<tr>
									<td>Trial No.:</td>
									<td>
										<input type="hidden" name="txtTrialNo" id="txtTrialNo"  value="<?php echo $db_BatchTicketTrial; ?>">
										<b> <?php echo $db_BatchTicketTrial; ?> </b>
									</td>
								</tr>
								<tr>
									<td>Lot No.:</td>
									<td>
										<input type="text" name="txtLotNo" id="txtLotNo" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_lot_no : $initLotNo_JRD ); ?>">
									</td>
									<td>Trial Date:</td>
									<td>
										<input type="text" name="txtTrialDate" id="txtTrialDate" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_trial_date : $initTrialDate_JRD ); ?>">
										<img src="js/cal.gif" onclick="javascript:NewCssCal('txtTrialDate')" style="cursor:pointer" name="picker" />
									</td>
								</tr>
							</table>
							<?php
								require("/tec_batch_ticket_trials.php"); 

								if ( $BatchTicketID && $BatchTicketTrialID ){
							?>	
									<table class='trials_child_tables_form'>
										<tr>
											<th colspan='2'>Raw Materials</th>
											<th>PHR</th>
											<th>Quantity</th>
										</tr>
										<?php
											$count = count($db_rm_id);
										?>							
										<tbody>
										<?php
											for ( $i=0 ; $i<$count ; $i++ ){
										?>
												<tr>
													<td colspan='2'>
														<input <?php echo ($db_phr[$i] == 0 ? "class='excluded'" : ""); ?> type='text' name='txtRM[]' id='txtRM<?php echo $i;?>' value='<?php echo $db_rm[$i]; ?>' readonly >
														<input type='hidden' name='txtRM_type_id[]' value='<?php echo $db_raw_material_type_id[$i]; ?>'>
														<input type='hidden' name='txtRM_id[]' value='<?php echo $db_rm_id[$i]; ?>'>
														<input type='hidden' name='hidBatchTicketTrialItemId[]' value='<?php echo $db_trialItemId[$i]; ?>'>
													</td>
													<td>
														<input <?php echo ($db_phr[$i] == 0 ? "class='excluded'" : ""); ?> type='text' name='txtPhr[]' id='txtPhr<?php echo $i;?>' value='<?php echo $db_phr[$i]; ?>' readonly  onkeyup='autoMultiply(), autoSum()'>
													</td>
													<td>
														<input <?php echo ($db_phr[$i] == 0 ? "class='excluded'" : ""); ?> type='text' name='txtQty[]' id='txtQty<?php echo $i;?>' value='<?php echo $db_qty[$i]; ?>' readonly >
													</td>
												</tr>
										<?php
											}
										?>
										</tbody>
										<tr id='total' class="spacing border_top">
											<td colspan='3' align="right"><b>TOTAL</b></td>
											<td>
												<input type='text' name='txtTotalQty' id='txtTotalQty' readonly value=''>
											</td>
										</tr>
											<input type='hidden' name='txtNRM'>
									  		<input type='hidden' name='txtNRm_type_id'>
									  		<input type='hidden' name='txtNRm_id'>
									  		<input type='hidden' name='hidNBatchTicketTrialItemId'>
											<input type='hidden' name='txtNPHR' id='txtNPHR' value="0">
											<input type='hidden' name='txtNQuantity' id='txtNQuantity' value="0">
									</table>
<?php
							require("/tec_additional_tests.php");
						}else{
							if ( ( $type_of_request == "Development" ) || ( $type_of_request == "Evaluation" && $tag_eval_replicate == 1 ) )
							{
								require("/tec_formula_development_evaluation_new_sample.php");
								require("/tec_additional_tests.php");
							}
							elseif ( $type_of_request == "Modification" )
							{
								echo "<table class='trials_child_tables_form' id='standard_formula'>";
								echo "</table>";
								require("/tec_additional_tests.php");
							}
						}
					}
					elseif ( $type_of_request == "Evaluation" && $tag_eval_replicate != 1 )
					{
							echo "</table>";
							require("/tec_additional_tests.php"); 
					}
			}
		}
	}

	require("/init_unset_values/batch_ticket_jrd_unset_value.php");
	require("/database_close.php"); 
?>