<?php

$Compounds = intval($_GET['Compounds']);
$Pipes = intval($_GET['Pipes']);
$Corporate = intval($_GET['Corporate']);
$PPR = intval($_GET['PPR']);
$type_id = intval($_GET['type_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>rm_type_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_RMType_Dropdown_New(?, ?, ?, ?, 0)");
		mysqli_stmt_bind_param($qry, 'iiii', $Compounds, $Pipes, $Corporate, $PPR);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>rm_type_dropdown.php'.'</td><td>'.$processError.' near line 26.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{	
			while($row = mysqli_fetch_assoc($result))
			{ 
				$rm_type_id = $row['rm_type_id'];
				$code = $row['code'];
				
				if ( $type_id == $rm_type_id )
					echo "<option value='".$rm_type_id."' selected>".$code."</option>";
				else 
					echo "<option value='".$rm_type_id."'>".$code."</option>";
				
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>