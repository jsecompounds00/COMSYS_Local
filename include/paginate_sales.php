<?php

	/* Setup vars for query. */

	$adjacents = 3; // How many adjacent pages should be shown on each side?
	$limit = 20; 		

	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;
	$end = $start + $limit - 1;								//if no page var is given, set start to 0

	/* Setup page vars for display. */
	if ($page == 0) 
		$page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_results/$limit);		//lastpage is = total results/ items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1

	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	$so_no = ($_GET['so_no'] ? $_GET['so_no'] : "");
	$so_date = ($_GET['so_date'] ? $_GET['so_date'] : NULL);
	$so_type = ($_GET['so_type'] ? $_GET['so_type'] : "");

	/*if($page <= 1)
	{
		$pagination.= "<a class='disabled'>previous</a>";
		$pagination.= "<a class='current'>$page</a>";
		$pagination.= "<a class='disabled'>next</a>";
	}*/

	if($lastpage >= 1)
	{	
		$pagination .= "<div class='pagination' align='center'>";

		//previous button
		if ($page > 1) 
			$pagination.= "<a href='$targetpage?page=$prev&so_no=$so_no&so_date=$so_date&so_type=$so_type'>previous</a>";
		else
			$pagination.= "<a class='disabled'>previous</a>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up. In this case less than 13 pages
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<a class='current'>$counter</a>";
				else
					$pagination.= "<a href='$targetpage?page=$counter&so_no=$so_no&so_date=$so_date&so_type=$so_type'>$counter</a>";					
			}
		} 
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<a class='current'>$counter</a>";
					else
						$pagination.= "<a href='$targetpage?page=$counter&so_no=$so_no&so_date=$so_date&so_type=$so_type'>$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href='$targetpage?page=$lpm1&so_no=$so_no&so_date=$so_date&so_type=$so_type'>$lpm1</a>";
				$pagination.= "<a href='$targetpage?page=$lastpage&so_no=$so_no&so_date=$so_date&so_type=$so_type'>$lastpage</a>";		
			} 
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href='$targetpage?page=1&so_no=$so_no&so_date=$so_date&so_type=$so_type'>1</a>";
				$pagination.= "<a href='$targetpage?page=2&so_no=$so_no&so_date=$so_date&so_type=$so_type'>2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<a class='current'>$counter</a>";
					else
						$pagination.= "<a href='$targetpage?page=$counter&so_no=$so_no&so_date=$so_date&so_type=$so_type'>$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href='$targetpage?page=$lpm1>$lpm1&so_no=$so_no&so_date=$so_date&so_type=$so_type'</a>";
				$pagination.= "<a href='$targetpage?page=$lastpage&so_no=$so_no&so_date=$so_date&so_type=$so_type'>$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href='$targetpage?page=1&so_no=$so_no&so_date=$so_date&so_type=$so_type'>1</a>";
				$pagination.= "<a href='$targetpage?page=2&so_no=$so_no&so_date=$so_date&so_type=$so_type'>2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<a class='current'>$counter</a>";
					else
						$pagination.= "<a href='$targetpage?page=$counter&so_no=$so_no&so_date=$so_date&so_type=$so_type'>$counter</a>";					
				}
			} 
		}

		
	
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href='$targetpage?page=$next&so_no=$so_no&so_date=$so_date&so_type=$so_type'>next</a>";
		else
			$pagination.= "<a class='disabled'>next</a>";
		$pagination.= "</div>\n";	
	}
	
	//echo $pagination;

?>