<?php
	require("/database_connect.php");	

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_complete_testing.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qryCT = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Complete_Testing_Query( ?, ?, ? )");
		mysqli_stmt_bind_param($qryCT, 'isi', $JRDNoReplicateID, $product_for_evaluation, $FGID);
		$qryCT->execute();
		$resultCT = mysqli_stmt_get_result($qryCT);
		$processErrorCT = mysqli_error($db);

		if ( !empty($processErrorCT) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_complete_testing.php'.'</td><td>'.$processErrorCT.' near line 12.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultCT)){
				$specific_gravity = $row['specific_gravity'];
				$hardness_a = $row['hardness_a'];
				$hardness_d = $row['hardness_d'];
				$volume_resistivity = $row['volume_resistivity'];
				$unaged_tensile = $row['unaged_tensile'];
				$unaged_elongation = $row['unaged_elongation'];
				$ovenaged_tensile = $row['ovenaged_tensile'];
				$ovenaged_elongation = $row['ovenaged_elongation'];
				$oilaged_tensile = $row['oilaged_tensile'];
				$oilaged_elongation = $row['oilaged_elongation'];
				$CTcreated_at = $row['created_at'];
				$CTcreated_id = $row['created_id'];
			}
			$db->next_result();
			$resultCT->close();
		}
?>
	
		<table class="results_child_tables_form">
			<col width="250"></col>
			<tr></tr>
			<input type="hidden" name="hidCompleteTestingID" value="<?php echo $complete_testing_id; ?>">
			<tr>
				<td>Specific Gravity:</td>
				<td colspan="4">
					<input type="text" name="txtSpecificGravity" value="<?php echo ( $complete_testing_id ? $specific_gravity : $initNRSpecificGravity );?>">
				</td>
			</tr>
			<?php
				if ( $fg_soft == 1 ){
			?>
					<tr>
						<td>H - Shore A:</td>
						<td colspan="4">
							<input type="text" name="txtHardnessA" value="<?php echo ( $complete_testing_id ? $hardness_a : $initNRHardnessA );?>">
						</td>
					</tr>
					<tr>
						<td>Volume Resistivity:</td>
						<td colspan="4">
							<input type="text" name="txtVolumeResistivity" value="<?php echo ( $complete_testing_id ? $volume_resistivity : $initNRVolumeResistivity );?>">
							<label class="instruction"> (e.g. 1.23E12) </label>
						</td>
					</tr>
			<?php
				}else{
			?>
					<tr>
						<td>H - Shore D:</td>
						<td>
							<input type="text" name="txtHardnessD" value="<?php echo ( $complete_testing_id ? $hardness_d : $initNRHardnessD );?>">
						</td>
					</tr>
			<?php
				}
			if ( $fg_soft == 1 ){
		?>
				<tr>
					<th></th>
					<th>
						Unaged Properties
					</th>
					<?php
						if ( $oven_aging == 1 ){
					?>
							<th>
								Oven Aged
							</th>
					<?php
						}

						if ( $oil_aging == 1 ){
					?>
							<th>
								Oil Aged
							</th>
					<?php
						}
					?>
				</tr>
				<tr>
					<td> Tensile Strength:</td>
					<td>
						<input type="text" name="txtUnagedTS" value="<?php echo ( $complete_testing_id ? $unaged_tensile : $initNRUnagedTS );?>">
					</td>
					<?php
						if ( $oven_aging == 1 ){
					?>
							<td>
								<input type="text" name="txtOvenTS" value="<?php echo ( $complete_testing_id ? $ovenaged_tensile : $initNROvenTS );?>">
							</td>
					<?php
						}

						if ( $oil_aging == 1 ){
					?>
							<td>
								<input type="text" name="txtOilTS" value="<?php echo ( $complete_testing_id ? $oilaged_tensile : $initNROilTS );?>">
							</td>
					<?php
						}
					?>
				</tr>
				<tr>
					<td>Elongation:</td>
					<td>
						<input type="text" name="txtUnagedE" value="<?php echo ( $complete_testing_id ? $unaged_elongation : $initNRUnagedE );?>">
					<?php
						if ( $oven_aging == 1 ){
					?>
							<td>
								<input type="text" name="txtOvenE" value="<?php echo ( $complete_testing_id ? $ovenaged_elongation : $initNROvenE );?>">
							</td>
					<?php
						}

						if ( $oil_aging == 1 ){
					?>
							<td>
								<input type="text" name="txtOilE" value="<?php echo ( $complete_testing_id ? $oilaged_elongation : $initNROilE );?>">
							</td>
					<?php
						}
					?>
				</tr>
	<?php
			}
	?>
			<input type='hidden' name='hidCTCreatedAt' value='<?php echo ( $complete_testing_id ? $CTcreated_at : date('Y-m-d H:i:s') );?>'>
			<input type='hidden' name='hidCTCreatedId' value='<?php echo ( $complete_testing_id ? $CTcreated_id : 0 );?>'>
		</table>
<?php	 
	}
		require("database_close.php"); 
?>