<?php
	session_start();
	require("/database_connect.php");
	require("/init_unset_values/jrd_init_value.php");
	$JRDId = intval($_GET["JRDId"]);

	$qry = mysqli_prepare($db, "CALL sp_JRD_Query(?)");
	mysqli_stmt_bind_param($qry, 'i', $JRDId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>product_modification.php'.'</td><td>'.$processError.' near line 13.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		while($row = mysqli_fetch_assoc($result))
		{
			$mod_eval_finished_good_id = $row['mod_eval_finished_good_id'];
			$tag_mod_reason_cost = $row['tag_mod_reason_cost'];
			$tag_mod_reason_process_improve = $row['tag_mod_reason_process_improve'];
			$tag_mod_reason_property_enhance = $row['tag_mod_reason_property_enhance'];
			$tag_mod_reason_legal_require = $row['tag_mod_reason_legal_require'];
			$tag_mod_reason_others = $row['tag_mod_reason_others'];
			$mod_reason_others = $row['mod_reason_others'];
		}
	}
	$db->next_result();
	$result->close();
?>

<table class='child_tables_form' onmouseover="enableOtherReason()">
	<tr valign="top">
		<td> 
			Item to be Modified 
		</td>
		<td class="border_right">	
			<select name='sltFGItem'>
				<?php
					$qryFG = "CALL sp_FG_Calcu_Dropdown()";
					$resultFG = mysqli_query($db, $qryFG);
					$processErrorFG = mysqli_error($db);

					if ( !empty($processErrorFG) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>product_evaluation.php'.'</td><td>'.$processErrorFG.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						echo "<option></option>";
						while ( $row = mysqli_fetch_assoc( $resultFG ) ){
							if ( $JRDId ){
								if ( $mod_eval_finished_good_id == $row["id"] ){
									echo " <option value='".$row['id']."' selected> ".$row['name']." </option> ";
								}else{
									echo " <option value='".$row['id']."'> ".$row['name']." </option> ";
								}
							}else{
								if ( $initFGItem == $row["id"] ){
									echo " <option value='".$row['id']."' selected> ".$row['name']." </option> ";
								}else{
									echo " <option value='".$row['id']."'> ".$row['name']." </option> ";
								}
							}
						}
						$db->next_result();
						$resultFG->close();
					}

				?>
			</select>
		</td>
		<td>
			Reason for Modification:
		</td>
		<td>
			<dt> 
				<input type='checkbox' name='chkCost' id='Cost' value='1' <?php echo ( $JRDId ? ( $tag_mod_reason_cost ? "checked" : "" ) : ( $initTagCost ? "checked" : "" ) );?>>
					<label for='Cost'>Cost </label> 
				</dt>
			<dt>
			 	<input type='checkbox' name='chkProcessabilityImprove' id='Processability' value='1'<?php echo ( $JRDId ? ( $tag_mod_reason_process_improve ? "checked" : "" ) : ( $initTagProcessabilityImprove ? "checked" : "" ) );?>>
			 	<label for='Processability'>Processability Improvement </label> 
			 </dt>
			<dt>
				<input type='checkbox' name='chkPropertyEnhance' id='Property' value='1'<?php echo ( $JRDId ? ( $tag_mod_reason_property_enhance ? "checked" : "" ) : ( $initTagPropertyEnhance ? "checked" : "" ) );?>>
				<label for='Property'>Property Enhancement </label> 
			</dt>
			<dt>
			 	<input type='checkbox' name='chkLegalReq' id='Legal' value='1'<?php echo ( $JRDId ? ( $tag_mod_reason_legal_require ? "checked" : "" ) : ( $initTagLegalReq ? "checked" : "" ) );?>>
			 	<label for='Legal'>Legal Requirement </label> 
			 </dt>
			<dt>
			 	<input type='checkbox' name='chkOthers' id='Others' value='1' onchange='enableOtherReason()'<?php echo ( $JRDId ? ( $tag_mod_reason_others ? "checked" : "" ) : ( $initTagOtherReason ? "checked" : "" ) );?>>
			 	<label for='Others'>Others </label> 
			 </dt>
			<dd>
				<input type='text' name='txtOtherReason' id='txtOtherReason' disabled value="<?php echo( $JRDId ? $mod_reason_others : $initOtherReason );?>"> 
			</dd>
		</td>
	</tr>
</table>

<?php
	require("/init_unset_values/jrd_unset_value.php");
	require("/database_close.php");
?>