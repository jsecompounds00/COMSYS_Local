<?php

$id = intval($_GET['id']);
$department_id = intval($_GET['department_id']);
$Compounds = intval($_GET['Compounds']);
$Pipes = intval($_GET['Pipes']);
$Corporate = intval($_GET['Corporate']);
$PPR = intval($_GET['PPR']);

require("database_connect.php");

if(!empty($errno))
{
	$error = mysqli_connect_error();
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>department_dropdown_new.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}
else
{

	$active = 1;

	$qry = mysqli_prepare($db, "CALL sp_Department_Dropdown(?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iiiii', $active, $Compounds, $Pipes, $Corporate, $PPR);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query

	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>department_dropdown_new.php'.'</td><td>'.$processError.' near line 33.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{ 
		while( $row = mysqli_fetch_assoc( $result ) ){
			$deptID = $row['id'];
			$deptCode = $row['code'];

			if ( $department_id == $deptID && $deptID != 0 )
				echo "<option value='".$deptID."' selected>".$deptCode."</option>";
			else echo "<option value='".$deptID."'>".$deptCode."</option>";
		}

		$db->next_result();
		$result->close(); 
	}


}
require("database_close.php");
?> 