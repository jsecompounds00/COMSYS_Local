<?php

$asset = intval($_GET['asset']);
$inventory_type = strval($_GET['inventory_type']);
// $sltAssetCondition = strval($_GET['sltAssetCondition']);
$addInventoryID = intval($_GET['addInventoryID']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>asset_dropdown.php'.'</td><td>'.$error.' near line 13.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qry = mysqli_prepare( $db,  "CALL sp_Asset_Specification_Dropdown( ?, ?, 0 )");
		mysqli_stmt_bind_param( $qry, "is", $asset, $inventory_type );
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
		$count = mysqli_num_rows($result);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>asset_dropdown.php'.'</td><td>'.$processError.' near line 24.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
				if ( $count ){		
					echo "<option value='0'></option>";
				}else{
					if ( $asset ){
						echo "<option value='0'>No available stock.</option>";	
					}else{
						echo "<option value='0'></option>";
					}
				}
			while($row = mysqli_fetch_assoc($result))
			{
				
				$SpecificationID = $row['SpecificationID'];
				$Specifications = $row['Specifications'];
				$Balance = $row['Balance'];

				echo "<option value=".$SpecificationID."-".$Balance.">".$Specifications."</option>";
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>