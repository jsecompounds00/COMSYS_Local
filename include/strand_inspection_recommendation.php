<?php

	require("database_connect.php");

	$qrySI = mysqli_prepare($db, "CALL sp_BAT_Strand_Inspection_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qrySI, 'is', $TypeID, $Type);
	$qrySI->execute();
	$resultSI = mysqli_stmt_get_result($qrySI);
	$processErrorSI = mysqli_error($db);

		if ( !empty($processErrorSI) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>strand_inspection_recommendation.php'.'</td><td>'.$processErrorSI.' near line 12.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultSI)){
				$SItrial_no = $row['SItrial_no'];
				$SIFGName = $row['SIFGName'];

				$SITrial1 = $row['SITrial1']; $SITrial2 = $row['SITrial2']; $SITrial3 = $row['SITrial3']; $SITrial4 = $row['SITrial4']; $SITrial5 = $row['SITrial5'];
				$SITrial6 = $row['SITrial6']; $SITrial7 = $row['SITrial7']; $SITrial8 = $row['SITrial8']; $SITrial9 = $row['SITrial9']; $SITrial10 = $row['SITrial10'];
				$SITrial11 = $row['SITrial11']; $SITrial12 = $row['SITrial12']; $SITrial13 = $row['SITrial13']; $SITrial14 = $row['SITrial14']; $SITrial15 = $row['SITrial15'];
				$SITrial16 = $row['SITrial16']; $SITrial17 = $row['SITrial17']; $SITrial18 = $row['SITrial18']; $SITrial19 = $row['SITrial19']; $SITrial20 = $row['SITrial20'];

				$Fisheye_Blisters1 = $row['Fisheye_Blisters1']; $Fisheye_Blisters2 = $row['Fisheye_Blisters2']; $Fisheye_Blisters3 = $row['Fisheye_Blisters3']; $Fisheye_Blisters4 = $row['Fisheye_Blisters4']; $Fisheye_Blisters5 = $row['Fisheye_Blisters5'];
				$Fisheye_Blisters6 = $row['Fisheye_Blisters6']; $Fisheye_Blisters7 = $row['Fisheye_Blisters7']; $Fisheye_Blisters8 = $row['Fisheye_Blisters8']; $Fisheye_Blisters9 = $row['Fisheye_Blisters9']; $Fisheye_Blisters10 = $row['Fisheye_Blisters10'];
				$Fisheye_Blisters11 = $row['Fisheye_Blisters11']; $Fisheye_Blisters12 = $row['Fisheye_Blisters12']; $Fisheye_Blisters13 = $row['Fisheye_Blisters13']; $Fisheye_Blisters14 = $row['Fisheye_Blisters14']; $Fisheye_Blisters15 = $row['Fisheye_Blisters15'];
				$Fisheye_Blisters16 = $row['Fisheye_Blisters16']; $Fisheye_Blisters17 = $row['Fisheye_Blisters17']; $Fisheye_Blisters18 = $row['Fisheye_Blisters18']; $Fisheye_Blisters19 = $row['Fisheye_Blisters19']; $Fisheye_Blisters20 = $row['Fisheye_Blisters20'];

				$Pinhole1 = $row['Pinhole1']; $Pinhole2 = $row['Pinhole2']; $Pinhole3 = $row['Pinhole3']; $Pinhole4 = $row['Pinhole4']; $Pinhole5 = $row['Pinhole5'];
				$Pinhole6 = $row['Pinhole6']; $Pinhole7 = $row['Pinhole7']; $Pinhole8 = $row['Pinhole8']; $Pinhole9 = $row['Pinhole9']; $Pinhole10 = $row['Pinhole10'];
				$Pinhole11 = $row['Pinhole11']; $Pinhole12 = $row['Pinhole12']; $Pinhole13 = $row['Pinhole13']; $Pinhole14 = $row['Pinhole14']; $Pinhole15 = $row['Pinhole15'];
				$Pinhole16 = $row['Pinhole16']; $Pinhole17 = $row['Pinhole17']; $Pinhole18 = $row['Pinhole18']; $Pinhole19 = $row['Pinhole19']; $Pinhole20 = $row['Pinhole20'];

				$ColorConform1 = $row['ColorConform1']; $ColorConform2 = $row['ColorConform2']; $ColorConform3 = $row['ColorConform3']; $ColorConform4 = $row['ColorConform4']; $ColorConform5 = $row['ColorConform5'];
				$ColorConform6 = $row['ColorConform6']; $ColorConform7 = $row['ColorConform7']; $ColorConform8 = $row['ColorConform8']; $ColorConform9 = $row['ColorConform9']; $ColorConform10 = $row['ColorConform10'];
				$ColorConform11 = $row['ColorConform11']; $ColorConform12 = $row['ColorConform12']; $ColorConform13 = $row['ColorConform13']; $ColorConform14 = $row['ColorConform14']; $ColorConform15 = $row['ColorConform15'];
				$ColorConform16 = $row['ColorConform16']; $ColorConform17 = $row['ColorConform17']; $ColorConform18 = $row['ColorConform18']; $ColorConform19 = $row['ColorConform19']; $ColorConform20 = $row['ColorConform20'];

				$Porous1 = $row['Porous1']; $Porous2 = $row['Porous2']; $Porous3 = $row['Porous3']; $Porous4 = $row['Porous4']; $Porous5 = $row['Porous5'];
				$Porous6 = $row['Porous6']; $Porous7 = $row['Porous7']; $Porous8 = $row['Porous8']; $Porous9 = $row['Porous9']; $Porous10 = $row['Porous10'];
				$Porous11 = $row['Porous11']; $Porous12 = $row['Porous12']; $Porous13 = $row['Porous13']; $Porous14 = $row['Porous14']; $Porous15 = $row['Porous15'];
				$Porous16 = $row['Porous16']; $Porous17 = $row['Porous17']; $Porous18 = $row['Porous18']; $Porous19 = $row['Porous19']; $Porous20 = $row['Porous20'];

				$FCPresent1 = $row['FCPresent1']; $FCPresent2 = $row['FCPresent2']; $FCPresent3 = $row['FCPresent3']; $FCPresent4 = $row['FCPresent4']; $FCPresent5 = $row['FCPresent5'];
				$FCPresent6 = $row['FCPresent6']; $FCPresent7 = $row['FCPresent7']; $FCPresent8 = $row['FCPresent8']; $FCPresent9 = $row['FCPresent9']; $FCPresent10 = $row['FCPresent10'];
				$FCPresent11 = $row['FCPresent11']; $FCPresent12 = $row['FCPresent12']; $FCPresent13 = $row['FCPresent13']; $FCPresent14 = $row['FCPresent14']; $FCPresent15 = $row['FCPresent15'];
				$FCPresent16 = $row['FCPresent16']; $FCPresent17 = $row['FCPresent17']; $FCPresent18 = $row['FCPresent18']; $FCPresent19 = $row['FCPresent19']; $FCPresent20 = $row['FCPresent20'];

				$OverallEvaluation1 = $row['OverallEvaluation1']; $OverallEvaluation2 = $row['OverallEvaluation2']; $OverallEvaluation3 = $row['OverallEvaluation3']; $OverallEvaluation4 = $row['OverallEvaluation4']; $OverallEvaluation5 = $row['OverallEvaluation5'];
				$OverallEvaluation6 = $row['OverallEvaluation6']; $OverallEvaluation7 = $row['OverallEvaluation7']; $OverallEvaluation8 = $row['OverallEvaluation8']; $OverallEvaluation9 = $row['OverallEvaluation9']; $OverallEvaluation10 = $row['OverallEvaluation10'];
				$OverallEvaluation11 = $row['OverallEvaluation11']; $OverallEvaluation12 = $row['OverallEvaluation12']; $OverallEvaluation13 = $row['OverallEvaluation13']; $OverallEvaluation14 = $row['OverallEvaluation14']; $OverallEvaluation15 = $row['OverallEvaluation15'];
				$OverallEvaluation16 = $row['OverallEvaluation16']; $OverallEvaluation17 = $row['OverallEvaluation17']; $OverallEvaluation18 = $row['OverallEvaluation18']; $OverallEvaluation19 = $row['OverallEvaluation19']; $OverallEvaluation20 = $row['OverallEvaluation20'];
		?>
				<table class="results_child_tables_form">
						<tr>
							<td colspan='<?php echo ($SItrial_no+1); ?>'> <?php  echo $SIFGName; ?> </td>
						</tr>
						<tr>
							<th></th>
							<?php
								if ( $SITrial1 <= $SItrial_no &&$SITrial1!= '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial1; ?> </th>
							<?php		
								}

								if ( $SITrial2 <= $SItrial_no && $SITrial2 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial2; ?> </th>
							<?php		
								}

								if ( $SITrial3 <= $SItrial_no && $SITrial3 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial3; ?> </th>
							<?php		
								}

								if ( $SITrial4 <= $SItrial_no && $SITrial4 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial4; ?> </th>
							<?php		
								}

								if ( $SITrial5 <= $SItrial_no && $SITrial5 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial5; ?> </th>
							<?php		
								}

								if ( $SITrial6 <= $SItrial_no && $SITrial6 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial6; ?> </th>
							<?php		
								}

								if ( $SITrial7 <= $SItrial_no && $SITrial7 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial7; ?> </th>
							<?php		
								}

								if ( $SITrial8 <= $SItrial_no && $SITrial8 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial8; ?> </th>
							<?php		
								}

								if ( $SITrial9 <= $SItrial_no && $SITrial9 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial9; ?> </th>
							<?php		
								}

								if ( $SITrial10 <= $SItrial_no && $SITrial10 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial10; ?> </th>
							<?php		
								}

								if ( $SITrial11 <= $SItrial_no && $SITrial11 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial11; ?> </th>
							<?php		
								}

								if ( $SITrial12 <= $SItrial_no && $SITrial12 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial12; ?> </th>
							<?php		
								}

								if ( $SITrial13 <= $SItrial_no && $SITrial13 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial13; ?> </th>
							<?php		
								}

								if ( $SITrial14 <= $SItrial_no && $SITrial14 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial14; ?> </th>
							<?php		
								}

								if ( $SITrial15 <= $SItrial_no && $SITrial15 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial15; ?> </th>
							<?php		
								}

								if ( $SITrial16 <= $SItrial_no && $SITrial16 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial16; ?> </th>
							<?php		
								}

								if ( $SITrial17 <= $SItrial_no && $SITrial17 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial17; ?> </th>
							<?php		
								}

								if ( $SITrial18 <= $SItrial_no && $SITrial18 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial18; ?> </th>
							<?php		
								}

								if ( $SITrial19 <= $SItrial_no && $SITrial19 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial19; ?> </th>
							<?php		
								}

								if ( $SITrial20 <= $SItrial_no && $SITrial20 != '-' ){
							?>
									<th> <?php echo 'Trial '.$SITrial20; ?> </th>
							<?php		
								}
							?>
						</tr>
				<!-- ###################### Fish Eyes / Blisters	 -->	
						<tr>
							<td> Fish Eyes / Blisters </td>
							<?php
								if ( $SITrial1 <= $SItrial_no && $SITrial1 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters1; ?> </td>
							<?php		
								}

								if ( $SITrial2 <= $SItrial_no && $SITrial2 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters2; ?> </td>
							<?php		
								}

								if ( $SITrial3 <= $SItrial_no && $SITrial3 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters3; ?> </td>
							<?php		
								}

								if ( $SITrial4 <= $SItrial_no && $SITrial4 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters4; ?> </td>
							<?php		
								}

								if ( $SITrial5 <= $SItrial_no && $SITrial5 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters5; ?> </td>
							<?php		
								}

								if ( $SITrial6 <= $SItrial_no && $SITrial6 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters6; ?> </td>
							<?php		
								}

								if ( $SITrial7 <= $SItrial_no && $SITrial7 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters7; ?> </td>
							<?php		
								}

								if ( $SITrial8 <= $SItrial_no && $SITrial8 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters8; ?> </td>
							<?php		
								}

								if ( $SITrial9 <= $SItrial_no && $SITrial9 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters9; ?> </td>
							<?php		
								}

								if ( $SITrial10 <= $SItrial_no && $SITrial10 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters10; ?> </td>
							<?php		
								}

								if ( $SITrial11 <= $SItrial_no && $SITrial11 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters11; ?> </td>
							<?php		
								}

								if ( $SITrial12 <= $SItrial_no && $SITrial12 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters12; ?> </td>
							<?php		
								}

								if ( $SITrial13 <= $SItrial_no && $SITrial13 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters13; ?> </td>
							<?php		
								}

								if ( $SITrial14 <= $SItrial_no && $SITrial14 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters14; ?> </td>
							<?php		
								}

								if ( $SITrial15 <= $SItrial_no && $SITrial15 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters15; ?> </td>
							<?php		
								}

								if ( $SITrial16 <= $SItrial_no && $SITrial16 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters16; ?> </td>
							<?php		
								}

								if ( $SITrial17 <= $SItrial_no && $SITrial17 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters17; ?> </td>
							<?php		
								}

								if ( $SITrial18 <= $SItrial_no && $SITrial18 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters18; ?> </td>
							<?php		
								}

								if ( $SITrial19 <= $SItrial_no && $SITrial19 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters19; ?> </td>
							<?php		
								}

								if ( $SITrial20 <= $SItrial_no && $SITrial20 != '-' ){
							?>
									<td> <?php echo $Fisheye_Blisters20; ?> </td>
							<?php		
								}
							?>
						</tr>
					<!-- ###################### Pin Holes	 -->	
						<tr>
							<td> Pin Holes </td>
							<?php
								if ( $SITrial1 <= $SItrial_no && $SITrial1 != '-' ){
							?>
									<td> <?php echo $Pinhole1; ?> </td>
							<?php		
								}

								if ( $SITrial2 <= $SItrial_no && $SITrial2 != '-' ){
							?>
									<td> <?php echo $Pinhole2; ?> </td>
							<?php		
								}

								if ( $SITrial3 <= $SItrial_no && $SITrial3 != '-' ){
							?>
									<td> <?php echo $Pinhole3; ?> </td>
							<?php		
								}

								if ( $SITrial4 <= $SItrial_no && $SITrial4 != '-' ){
							?>
									<td> <?php echo $Pinhole4; ?> </td>
							<?php		
								}

								if ( $SITrial5 <= $SItrial_no && $SITrial5 != '-' ){
							?>
									<td> <?php echo $Pinhole5; ?> </td>
							<?php		
								}

								if ( $SITrial6 <= $SItrial_no && $SITrial6 != '-' ){
							?>
									<td> <?php echo $Pinhole6; ?> </td>
							<?php		
								}

								if ( $SITrial7 <= $SItrial_no && $SITrial7 != '-' ){
							?>
									<td> <?php echo $Pinhole7; ?> </td>
							<?php		
								}

								if ( $SITrial8 <= $SItrial_no && $SITrial8 != '-' ){
							?>
									<td> <?php echo $Pinhole8; ?> </td>
							<?php		
								}

								if ( $SITrial9 <= $SItrial_no && $SITrial9 != '-' ){
							?>
									<td> <?php echo $Pinhole9; ?> </td>
							<?php		
								}

								if ( $SITrial10 <= $SItrial_no && $SITrial10 != '-' ){
							?>
									<td> <?php echo $Pinhole10; ?> </td>
							<?php		
								}

								if ( $SITrial11 <= $SItrial_no && $SITrial11 != '-' ){
							?>
									<td> <?php echo $Pinhole11; ?> </td>
							<?php		
								}

								if ( $SITrial12 <= $SItrial_no && $SITrial12 != '-' ){
							?>
									<td> <?php echo $Pinhole12; ?> </td>
							<?php		
								}

								if ( $SITrial13 <= $SItrial_no && $SITrial13 != '-' ){
							?>
									<td> <?php echo $Pinhole13; ?> </td>
							<?php		
								}

								if ( $SITrial14 <= $SItrial_no && $SITrial14 != '-' ){
							?>
									<td> <?php echo $Pinhole14; ?> </td>
							<?php		
								}

								if ( $SITrial15 <= $SItrial_no && $SITrial15 != '-' ){
							?>
									<td> <?php echo $Pinhole15; ?> </td>
							<?php		
								}

								if ( $SITrial16 <= $SItrial_no && $SITrial16 != '-' ){
							?>
									<td> <?php echo $Pinhole16; ?> </td>
							<?php		
								}

								if ( $SITrial17 <= $SItrial_no && $SITrial17 != '-' ){
							?>
									<td> <?php echo $Pinhole17; ?> </td>
							<?php		
								}

								if ( $SITrial18 <= $SItrial_no && $SITrial18 != '-' ){
							?>
									<td> <?php echo $Pinhole18; ?> </td>
							<?php		
								}

								if ( $SITrial19 <= $SItrial_no && $SITrial19 != '-' ){
							?>
									<td> <?php echo $Pinhole19; ?> </td>
							<?php		
								}

								if ( $SITrial20 <= $SItrial_no && $SITrial20 != '-' ){
							?>
									<td> <?php echo $Pinhole20; ?> </td>
							<?php		
								}
							?>
						</tr>
					<!-- ###################### Color Conform	 -->	
						<tr>
							<td> Color Conform </td>
							<?php
								if ( $SITrial1 <= $SItrial_no && $SITrial1 != '-' ){
							?>
									<td> <?php echo $ColorConform1; ?> </td>
							<?php		
								}

								if ( $SITrial2 <= $SItrial_no && $SITrial2 != '-' ){
							?>
									<td> <?php echo $ColorConform2; ?> </td>
							<?php		
								}

								if ( $SITrial3 <= $SItrial_no && $SITrial3 != '-' ){
							?>
									<td> <?php echo $ColorConform3; ?> </td>
							<?php		
								}

								if ( $SITrial4 <= $SItrial_no && $SITrial4 != '-' ){
							?>
									<td> <?php echo $ColorConform4; ?> </td>
							<?php		
								}

								if ( $SITrial5 <= $SItrial_no && $SITrial5 != '-' ){
							?>
									<td> <?php echo $ColorConform5; ?> </td>
							<?php		
								}

								if ( $SITrial6 <= $SItrial_no && $SITrial6 != '-' ){
							?>
									<td> <?php echo $ColorConform6; ?> </td>
							<?php		
								}

								if ( $SITrial7 <= $SItrial_no && $SITrial7 != '-' ){
							?>
									<td> <?php echo $ColorConform7; ?> </td>
							<?php		
								}

								if ( $SITrial8 <= $SItrial_no && $SITrial8 != '-' ){
							?>
									<td> <?php echo $ColorConform8; ?> </td>
							<?php		
								}

								if ( $SITrial9 <= $SItrial_no && $SITrial9 != '-' ){
							?>
									<td> <?php echo $ColorConform9; ?> </td>
							<?php		
								}

								if ( $SITrial10 <= $SItrial_no && $SITrial10 != '-' ){
							?>
									<td> <?php echo $ColorConform10; ?> </td>
							<?php		
								}

								if ( $SITrial11 <= $SItrial_no && $SITrial11 != '-' ){
							?>
									<td> <?php echo $ColorConform11; ?> </td>
							<?php		
								}

								if ( $SITrial12 <= $SItrial_no && $SITrial12 != '-' ){
							?>
									<td> <?php echo $ColorConform12; ?> </td>
							<?php		
								}

								if ( $SITrial13 <= $SItrial_no && $SITrial13 != '-' ){
							?>
									<td> <?php echo $ColorConform13; ?> </td>
							<?php		
								}

								if ( $SITrial14 <= $SItrial_no && $SITrial14 != '-' ){
							?>
									<td> <?php echo $ColorConform14; ?> </td>
							<?php		
								}

								if ( $SITrial15 <= $SItrial_no && $SITrial15 != '-' ){
							?>
									<td> <?php echo $ColorConform15; ?> </td>
							<?php		
								}

								if ( $SITrial16 <= $SItrial_no && $SITrial16 != '-' ){
							?>
									<td> <?php echo $ColorConform16; ?> </td>
							<?php		
								}

								if ( $SITrial17 <= $SItrial_no && $SITrial17 != '-' ){
							?>
									<td> <?php echo $ColorConform17; ?> </td>
							<?php		
								}

								if ( $SITrial18 <= $SItrial_no && $SITrial18 != '-' ){
							?>
									<td> <?php echo $ColorConform18; ?> </td>
							<?php		
								}

								if ( $SITrial19 <= $SItrial_no && $SITrial19 != '-' ){
							?>
									<td> <?php echo $ColorConform19; ?> </td>
							<?php		
								}

								if ( $SITrial20 <= $SItrial_no && $SITrial20 != '-' ){
							?>
									<td> <?php echo $ColorConform20; ?> </td>
							<?php		
								}
							?>
						</tr>
					<!-- ###################### Porous	 -->	
						<tr>
							<td> Porous </td>
							<?php
								if ( $SITrial1 <= $SItrial_no && $SITrial1 != '-' ){
							?>
									<td> <?php echo $Porous1; ?> </td>
							<?php		
								}

								if ( $SITrial2 <= $SItrial_no && $SITrial2 != '-' ){
							?>
									<td> <?php echo $Porous2; ?> </td>
							<?php		
								}

								if ( $SITrial3 <= $SItrial_no && $SITrial3 != '-' ){
							?>
									<td> <?php echo $Porous3; ?> </td>
							<?php		
								}

								if ( $SITrial4 <= $SItrial_no && $SITrial4 != '-' ){
							?>
									<td> <?php echo $Porous4; ?> </td>
							<?php		
								}

								if ( $SITrial5 <= $SItrial_no && $SITrial5 != '-' ){
							?>
									<td> <?php echo $Porous5; ?> </td>
							<?php		
								}

								if ( $SITrial6 <= $SItrial_no && $SITrial6 != '-' ){
							?>
									<td> <?php echo $Porous6; ?> </td>
							<?php		
								}

								if ( $SITrial7 <= $SItrial_no && $SITrial7 != '-' ){
							?>
									<td> <?php echo $Porous7; ?> </td>
							<?php		
								}

								if ( $SITrial8 <= $SItrial_no && $SITrial8 != '-' ){
							?>
									<td> <?php echo $Porous8; ?> </td>
							<?php		
								}

								if ( $SITrial9 <= $SItrial_no && $SITrial9 != '-' ){
							?>
									<td> <?php echo $Porous9; ?> </td>
							<?php		
								}

								if ( $SITrial10 <= $SItrial_no && $SITrial10 != '-' ){
							?>
									<td> <?php echo $Porous10; ?> </td>
							<?php		
								}

								if ( $SITrial11 <= $SItrial_no && $SITrial11 != '-' ){
							?>
									<td> <?php echo $Porous11; ?> </td>
							<?php		
								}

								if ( $SITrial12 <= $SItrial_no && $SITrial12 != '-' ){
							?>
									<td> <?php echo $Porous12; ?> </td>
							<?php		
								}

								if ( $SITrial13 <= $SItrial_no && $SITrial13 != '-' ){
							?>
									<td> <?php echo $Porous13; ?> </td>
							<?php		
								}

								if ( $SITrial14 <= $SItrial_no && $SITrial14 != '-' ){
							?>
									<td> <?php echo $Porous14; ?> </td>
							<?php		
								}

								if ( $SITrial15 <= $SItrial_no && $SITrial15 != '-' ){
							?>
									<td> <?php echo $Porous15; ?> </td>
							<?php		
								}

								if ( $SITrial16 <= $SItrial_no && $SITrial16 != '-' ){
							?>
									<td> <?php echo $Porous16; ?> </td>
							<?php		
								}

								if ( $SITrial17 <= $SItrial_no && $SITrial17 != '-' ){
							?>
									<td> <?php echo $Porous17; ?> </td>
							<?php		
								}

								if ( $SITrial18 <= $SItrial_no && $SITrial18 != '-' ){
							?>
									<td> <?php echo $Porous18; ?> </td>
							<?php		
								}

								if ( $SITrial19 <= $SItrial_no && $SITrial19 != '-' ){
							?>
									<td> <?php echo $Porous19; ?> </td>
							<?php		
								}

								if ( $SITrial20 <= $SItrial_no && $SITrial20 != '-' ){
							?>
									<td> <?php echo $Porous20; ?> </td>
							<?php		
								}
							?>
						</tr>
					<!-- ###################### FC Present	 -->	
						<tr>
							<td> FC Present </td>
							<?php
								if ( $SITrial1 <= $SItrial_no && $SITrial1 != '-' ){
							?>
									<td> <?php echo $FCPresent1; ?> </td>
							<?php		
								}

								if ( $SITrial2 <= $SItrial_no && $SITrial2 != '-' ){
							?>
									<td> <?php echo $FCPresent2; ?> </td>
							<?php		
								}

								if ( $SITrial3 <= $SItrial_no && $SITrial3 != '-' ){
							?>
									<td> <?php echo $FCPresent3; ?> </td>
							<?php		
								}

								if ( $SITrial4 <= $SItrial_no && $SITrial4 != '-' ){
							?>
									<td> <?php echo $FCPresent4; ?> </td>
							<?php		
								}

								if ( $SITrial5 <= $SItrial_no && $SITrial5 != '-' ){
							?>
									<td> <?php echo $FCPresent5; ?> </td>
							<?php		
								}

								if ( $SITrial6 <= $SItrial_no && $SITrial6 != '-' ){
							?>
									<td> <?php echo $FCPresent6; ?> </td>
							<?php		
								}

								if ( $SITrial7 <= $SItrial_no && $SITrial7 != '-' ){
							?>
									<td> <?php echo $FCPresent7; ?> </td>
							<?php		
								}

								if ( $SITrial8 <= $SItrial_no && $SITrial8 != '-' ){
							?>
									<td> <?php echo $FCPresent8; ?> </td>
							<?php		
								}

								if ( $SITrial9 <= $SItrial_no && $SITrial9 != '-' ){
							?>
									<td> <?php echo $FCPresent9; ?> </td>
							<?php		
								}

								if ( $SITrial10 <= $SItrial_no && $SITrial10 != '-' ){
							?>
									<td> <?php echo $FCPresent10; ?> </td>
							<?php		
								}

								if ( $SITrial11 <= $SItrial_no && $SITrial11 != '-' ){
							?>
									<td> <?php echo $FCPresent11; ?> </td>
							<?php		
								}

								if ( $SITrial12 <= $SItrial_no && $SITrial12 != '-' ){
							?>
									<td> <?php echo $FCPresent12; ?> </td>
							<?php		
								}

								if ( $SITrial13 <= $SItrial_no && $SITrial13 != '-' ){
							?>
									<td> <?php echo $FCPresent13; ?> </td>
							<?php		
								}

								if ( $SITrial14 <= $SItrial_no && $SITrial14 != '-' ){
							?>
									<td> <?php echo $FCPresent14; ?> </td>
							<?php		
								}

								if ( $SITrial15 <= $SItrial_no && $SITrial15 != '-' ){
							?>
									<td> <?php echo $FCPresent15; ?> </td>
							<?php		
								}

								if ( $SITrial16 <= $SItrial_no && $SITrial16 != '-' ){
							?>
									<td> <?php echo $FCPresent16; ?> </td>
							<?php		
								}

								if ( $SITrial17 <= $SItrial_no && $SITrial17 != '-' ){
							?>
									<td> <?php echo $FCPresent17; ?> </td>
							<?php		
								}

								if ( $SITrial18 <= $SItrial_no && $SITrial18 != '-' ){
							?>
									<td> <?php echo $FCPresent18; ?> </td>
							<?php		
								}

								if ( $SITrial19 <= $SItrial_no && $SITrial19 != '-' ){
							?>
									<td> <?php echo $FCPresent19; ?> </td>
							<?php		
								}

								if ( $SITrial20 <= $SItrial_no && $SITrial20 != '-' ){
							?>
									<td> <?php echo $FCPresent20; ?> </td>
							<?php		
								}
							?>
						</tr>
					<!-- ###################### Overall Evaluation Remark	 -->	
						<tr>
							<td> Overall Evaluation Remark </td>
							<?php
								if ( $SITrial1 <= $SItrial_no && $SITrial1 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation1; ?> </td>
							<?php		
								}

								if ( $SITrial2 <= $SItrial_no && $SITrial2 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation2; ?> </td>
							<?php		
								}

								if ( $SITrial3 <= $SItrial_no && $SITrial3 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation3; ?> </td>
							<?php		
								}

								if ( $SITrial4 <= $SItrial_no && $SITrial4 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation4; ?> </td>
							<?php		
								}

								if ( $SITrial5 <= $SItrial_no && $SITrial5 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation5; ?> </td>
							<?php		
								}

								if ( $SITrial6 <= $SItrial_no && $SITrial6 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation6; ?> </td>
							<?php		
								}

								if ( $SITrial7 <= $SItrial_no && $SITrial7 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation7; ?> </td>
							<?php		
								}

								if ( $SITrial8 <= $SItrial_no && $SITrial8 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation8; ?> </td>
							<?php		
								}

								if ( $SITrial9 <= $SItrial_no && $SITrial9 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation9; ?> </td>
							<?php		
								}

								if ( $SITrial10 <= $SItrial_no && $SITrial10 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation10; ?> </td>
							<?php		
								}

								if ( $SITrial11 <= $SItrial_no && $SITrial11 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation11; ?> </td>
							<?php		
								}

								if ( $SITrial12 <= $SItrial_no && $SITrial12 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation12; ?> </td>
							<?php		
								}

								if ( $SITrial13 <= $SItrial_no && $SITrial13 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation13; ?> </td>
							<?php		
								}

								if ( $SITrial14 <= $SItrial_no && $SITrial14 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation14; ?> </td>
							<?php		
								}

								if ( $SITrial15 <= $SItrial_no && $SITrial15 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation15; ?> </td>
							<?php		
								}

								if ( $SITrial16 <= $SItrial_no && $SITrial16 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation16; ?> </td>
							<?php		
								}

								if ( $SITrial17 <= $SItrial_no && $SITrial17 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation17; ?> </td>
							<?php		
								}

								if ( $SITrial18 <= $SItrial_no && $SITrial18 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation18; ?> </td>
							<?php		
								}

								if ( $SITrial19 <= $SItrial_no && $SITrial19 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation19; ?> </td>
							<?php		
								}

								if ( $SITrial20 <= $SItrial_no && $SITrial20 != '-' ){
							?>
									<td> <?php echo $OverallEvaluation20; ?> </td>
							<?php		
								}
							?>
						</tr>
					</table>
		<?php
				}
				$db->next_result();
				$resultSI->close();
			}

	require("database_close.php");
?>