<?php

$date = strval($_GET['date']);
$id = intval($_GET['id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dsq_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = "CALL sp_DSQ_a_Dropdown('$date', 0)";
		$result = mysqli_query($db, $qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dsq_dropdown.php'.'</td><td>'.$processError.' near line 21.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{	
			if (!$id)
				echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$dsq_id = $row['DSQId'];
				$control_no = $row['ControlNumber'];
				$delivery_date = $row['DeliveryDate'];
				$delivery_time = $row['DeliveryTime'];
				$truck_no = $row['Trucknumber'];
				$class = $row['Class'];
				
				if ($id)
				{
					if ( $id == $dsq_id )
						echo "<option value=".$dsq_id."-".$class." selected>".$control_no." : ".$truck_no." : ".$delivery_time."</option>";
				}
				else
				{
					echo "<option value=".$dsq_id."-".$class.">".$control_no." : ".$truck_no." : ".$delivery_time."</option>";
				}
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>