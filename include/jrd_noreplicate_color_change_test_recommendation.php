<?php

	require("database_connect.php");

	$qryCCT = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Color_Change_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qryCCT, 'is', $TypeID, $Type);
	$qryCCT->execute();
	$resultCCT = mysqli_stmt_get_result($qryCCT);
	$processErrorCCT = mysqli_error($db);
?>
	
	<table class="results_child_tables_form">
		<?php
			if ( !empty($processErrorCCT) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_color_change_test_recommendation.php'.'</td><td>'.$processErrorCCT.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultCCT)){
					$CCTJRDTestID = $row['CCTJRDTestID'];
					$CCTJRDFGSoft = $row['CCTJRDFGSoft'];

					$CCTClientsProduct = $row['CCTClientsProduct']; 
					$CCTCACCProduct = $row['CCTCACCProduct']; 

					$CCTClientsColorConformance = $row['CCTClientsColorConformance']; 
					$CCTCACCColorConformance = $row['CCTCACCColorConformance']; 

					$CCTClientsHeatStability = $row['CCTClientsHeatStability']; 
					$CCTCACCHeatStability = $row['CCTCACCHeatStability']; 
		?>
					<tr>
						<th></th>
						<th> <?php echo $CCTClientsProduct; ?> </th>
						<th> <?php echo $CCTCACCProduct; ?> </th>
					</tr>
				<!-- ###################### Color Conformance	 -->	
					<tr>
						<td> Color Conformance </td>
						<td> <?php echo $CCTClientsColorConformance; ?> </td>
						<td> <?php echo $CCTCACCColorConformance; ?> </td>
					</tr>
				<!-- ###################### Heat Stability	 -->	
					<tr>
						<td> Heat Stability </td>
						<td> <?php echo $CCTClientsHeatStability; ?> </td>
						<td> <?php echo $CCTCACCHeatStability; ?> </td>
					</tr>
		<?php
				}
				$db->next_result();
				$resultCCT->close();
			}
		?>
	</table>
	<br>
<?php	

	require("database_close.php");
?>