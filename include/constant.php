<?php //PG for page, AC for action/process

	//MODULE: SUPPLY TYPE
	DEFINE ("PG_SUPPLY_TYPE_HOME", "supply_type.php?page=1&search=&qsone="); 
	DEFINE ("PG_NEW_SUPPLY_TYPE", "new_supply_type.php?id=");
	DEFINE ("AC_PROCESS_SUPPLY_TYPE", "process_new_supply_type.php");

	//MODULE: SUPPLIES
	DEFINE ("PG_SUPPLIES_HOME", "supplies.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_SUPPLY", "new_supply.php?id=");
	DEFINE ("PG_ADD_SUPPLY_STOCK", "add_supply_stock.php?id=");
	DEFINE ("PG_DEDUCT_SUPPLY_STOCK", "deduct_supply_stock.php?id=");
	DEFINE ("PG_SUPPLY_HISTORY", "supply_history.php?page=1&id=");
	DEFINE ("AC_PROCESS_SUPPLY", "process_new_supply.php");
	DEFINE ("AC_PROCESS_DEDUCT_SUPPLY", "process_deduct_supply_stock.php");
	DEFINE ("AC_PROCESS_ADD_SUPPLY", "process_add_supply_stock.php");

	//MODULE: UOM
	DEFINE ("PG_UOM_HOME", "uom.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_UOM", "new_uom.php?id=");
	DEFINE ("AC_PROCESS_UOM", "process_new_uom.php");
	
	//MODULE: USER
	DEFINE ("PG_USER_HOME", "user.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_USER", "new_user.php?id=");
	DEFINE ("AC_PROCESS_USER", "process_new_user.php");
	
	//MODULE: ROLE
	DEFINE ("PG_ROLE_HOME", "role.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_ROLE", "new_role.php?id=");
	DEFINE ("AC_PROCESS_ROLE", "process_new_role.php");
	
	//MODULE: DEPARTMENT
	DEFINE ("PG_DEPARTMENT_HOME", "department.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_DEPARTMENT", "new_department.php?id=");
	DEFINE ("AC_PROCESS_DEPARTMENT", "process_new_department.php");
	
	//MODULE: SUPPLIER
	DEFINE ("PG_SUPPLIER_HOME", "supplier.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_SUPPLIER", "new_supplier.php?id=");
	DEFINE ("AC_PROCESS_SUPPLIER", "process_new_supplier.php");
	
	//MODULE: CUSTOMER
	DEFINE ("PG_CUSTOMER_HOME", "customer.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_CUSTOMER", "new_customer.php?id=");
	DEFINE ("AC_PROCESS_CUSTOMER", "process_new_customer.php");
	
	//MODULE: PERMISSIONS
	DEFINE ("PG_PERMISSION_HOME", "permission.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_PERMISSION", "new_permission.php?id=");
	DEFINE ("AC_PROCESS_PERMISSION", "process_new_permission.php");
	
	//MODULE: DATA MIGRATION
	DEFINE ("PG_MIGRATION_HOME", "data_migration.php");
	DEFINE ("AC_PROCESS_MIGRATION", "process_migration.php");
	
	//MODULE: EQUIPMENT
	DEFINE ("PG_EQUIPMENT_HOME", "equipment.php?page=1&name_text=&code_text=&type_text=");
	DEFINE ("PG_NEW_EQUIPMENT", "new_equipment.php?id=");
	DEFINE ("AC_PROCESS_EQUIPMENT", "process_new_equipment.php");
	
	//MODULE: LOCATION
	DEFINE ("PG_LOCATION_HOME", "location.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_LOCATION", "new_location.php?id=");
	DEFINE ("AC_PROCESS_LOCATION", "process_new_location.php");
	
	//MODULE: EQUIPMENT_STATUS
	DEFINE ("PG_EQUIPMENT_STATUS_HOME", "equipment_status.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_EQUIPMENT_STATUS", "new_equipment_status.php?id=");
	DEFINE ("AC_PROCESS_EQUIPMENT_STATUS", "process_new_equipment_status.php");
	
	//MODULE: EQUIPMENT_MEASURE
	DEFINE ("PG_EQUIPMENT_MEASURE_HOME", "equipment_measure.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_EQUIPMENT_MEASURE", "new_equipment_measure.php?id=");
	DEFINE ("AC_PROCESS_EQUIPMENT_MEASURE", "process_new_equipment_measure.php");
	
	//MODULE: CALIBRATION
	DEFINE ("PG_CALIBRATION_HOME", "calibration.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_CALIBRATION", "new_calibration.php?id=");
	DEFINE ("AC_PROCESS_CALIBRATION", "process_new_calibration.php");
	
	//MODULE: VALIDATION
	DEFINE ("PG_VALIDATION_HOME", "validation.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_VALIDATION", "new_validation.php?id=");
	DEFINE ("AC_PROCESS_VALIDATION", "process_new_validation.php");
	
	//MODULE: NRM
	DEFINE ("PG_NRM_HOME", "nrm.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_NRM", "new_nrm.php?id=");
	DEFINE ("AC_PROCESS_NRM", "process_new_nrm.php");
	
	//MODULE: PRE
	DEFINE ("PG_PRE_HOME", "pre_monitoring.php?page=1&item=&pre_number=&pre_date=");
	DEFINE ("PG_NEW_PRE", "new_pre.php?id=");
	DEFINE ("AC_PROCESS_PRE", "process_new_pre.php");
	
	//MODULE: PO
	DEFINE ("PG_PO_HOME", "po_monitoring.php?page=1&po_date=&po_number=&pre_number=");
	DEFINE ("PG_NEW_PO", "new_po.php?id=");
	DEFINE ("AC_PROCESS_PO", "process_new_po.php");

	//MODULE: EXPENSE
	DEFINE ("PG_EXPENSE_HOME", "expense_code.php?page=1&search=&qsone=");
	DEFINE ("PG_NEW_EXPENSE", "new_expense_code.php?id=");
	DEFINE ("AC_PROCESS_EXPENSE", "process_new_expense_code.php");
	DEFINE ("PG_NEW_PARTICULARS", "new_expense_particular.php?id=");

?>
