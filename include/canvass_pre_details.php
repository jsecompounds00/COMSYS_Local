<tr>
	<th rowspan="2"> Include? </th>
	<th rowspan="2"> Quantity </th>
	<th rowspan="2"> PRE Number </th>
	<th rowspan="2"> Description </th>
	<th colspan="4"> Last Purchase Information </th>
	<?php
		session_start();
		ob_start();  

		$preIds = strval($_GET['implodedPreIds']);
		$hidCanvassID = intval($_GET['hidCanvassID']);
		$Division = strval($_GET['Division']);

		$preId = array();
		$preId = explode(',', $preIds);
		$preId = array_unique($preId);

		require("database_connect.php");
		require("/init_unset_values/canvass_init_value.php");

		$qryEdit = mysqli_prepare($db, "CALL sp_Canvass_Supplier_Query(?)");
		mysqli_stmt_bind_param($qryEdit, 'i', $hidCanvassID);
		$qryEdit->execute();
		$resultEdit = mysqli_stmt_get_result($qryEdit);

		$processErrorEdit = mysqli_error($db);

		if(!empty($processErrorEdit))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvass_per_details.php'.'</td><td>'.$processErrorEdit.' near line 35.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			$canvass_supplier_id = array();
			$new_supplier = array();
			$delivery_terms = array();
			$payment_days = array();
			$payment_terms = array();
			$contact_person = array();
			$telephone_number = array();
			$fax_number = array();
			while($row = mysqli_fetch_assoc($resultEdit))
			{
				$canvass_supplier_id[] = $row['canvass_supplier_id'];
				$supplier_id[] = $row['supplier_id'];
				$new_supplier[] = htmlspecialchars($row['new_supplier']);
				$delivery_terms[] = $row['delivery_terms'];
				$payment_days[] = $row['payment_days'];
				$payment_terms[] = $row['payment_terms'];
				$contact_person[] = htmlspecialchars($row['contact_person']);
				$telephone_number[] = htmlspecialchars($row['telephone_number']);
				$fax_number[] = htmlspecialchars($row['fax_number']);

				$remarks = htmlspecialchars($row['remarks']);
			}
			$db->next_result();
			$resultEdit->close();

			$ctr = count($canvass_supplier_id);
		}

		for ( $y = 0; $y < 4; $y++ ) { 
	?>
			<th rowspan="2" class="align_left">
				
				<input type="hidden" id="hidCounterID" value="<?php echo $ctr;?>">

				<input type="hidden" name="hidCanvassSuppliersID[<?php echo $y;?>]" id="hidCanvassSuppliersID<?php echo $y;?>"
					value="<?php echo ( $ctr > $y ? $canvass_supplier_id[$y] : NULL );?>">

				<input type="hidden" name="SupplierMarking[<?php echo $y;?>]" id="SupplierMarking<?php echo $y;?>" 
					value="<?php echo ($y+1);?>"
					<?php echo ( $ctr > $y ? "" : ( !$initCANVASSSupplier[$y] && $initCANVASSNewsupplier[$y] == "" ? "disabled" : "" ) );?>>

				<select name="sltSupplier[<?php echo $y;?>]" id="sltSupplier<?php echo $y;?>" onchange="enableSupplierMarking('<?php echo $y;?>')">
					<?php
						$qryG = mysqli_prepare($db, "CALL sp_PO_Supplier_Group_Dropdown( ? )");
						mysqli_stmt_bind_param($qryG, 's', $Division);
						$qryG->execute();
						$resultG = mysqli_stmt_get_result($qryG); //return results of query
						$processErrorG = mysqli_error($db);

						if(!empty($processErrorG))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvass_pre_details.php'.'</td><td>'.$processErrorG.' near line 24.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							$applicable_to = array();
							while($row = mysqli_fetch_assoc($resultG))
							{
								$applicable_to[] = $row["applicable_to"];

							}

							$db->next_result();
							$resultG->close();

						}
						if ( $ctr <= $y || is_null($supplier_id[$y]) )
							echo "<option value='0'></option>";
						foreach ($applicable_to as $applicable_to_key => $applicable_to_value) {
							echo "<optgroup label='".$applicable_to_value."'>";
								$qry = mysqli_prepare($db, "CALL sp_PO_Supplier_Dropdown(?, ?)");
								mysqli_stmt_bind_param($qry, 'ss', $Division, $applicable_to_value);
								$qry->execute();
								$result = mysqli_stmt_get_result($qry);
								$processError = mysqli_error($db);

								if(!empty($processError))
								{
									error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvass_pre_details.php'.'</td><td>'.$processError.' near line 24.</td></tr>', 3, "errors.php");
									header("location: error_message.html");
								}
								else
								{
									while($row = mysqli_fetch_assoc($result))
									{
										$id = $row["id"];
										$name = $row["name"];

										if ( $ctr > $y ){
											if ( $supplier_id[$y] == $id )
												echo "	<option value='".$id."' selected>".$name."</option>";
											else echo "	<option value='".$id."'>".$name."</option>";
										}else{
											if ( $initCANVASSSupplier[$y] == $id )
												echo "	<option value='".$id."' selected>".$name."</option>";
											else echo "	<option value='".$id."'>".$name."</option>";	
										}

									}
									$db->next_result();
									$result->close();
								}
							echo "</optgroup>";
						}
					?>
				</select>
				
				<br>
				
				<input type="checkbox" name="chkNewSupplierMark[<?php echo $y;?>]" id="chkNewSupplierMark<?php echo $y;?>" 
					onchange="enableSupplier('<?php echo $y;?>'), enableSupplierMarking('<?php echo $y;?>')" 
					<?php echo ( $ctr > $y ? ($new_supplier[$y] != "" ? "checked" : "") : ( $initCANVASSNewsupplier[$y] != "" ? "checked" : "" ) );?>>
				
				<label for="chkNewSupplierMark<?php echo $y;?>"> New Supplier </label>
				
				<br>
				
				<input type="text" name="txtNewsupplier[<?php echo $y;?>]" id="txtNewSupplier<?php echo $y;?>" 
					<?php echo ( $ctr > $y ? ($new_supplier[$y] != "" ? "" : "readOnly") : ($initCANVASSNewsupplier[$y] != "" ? "" : "readOnly") );?> 
					onblur="enableSupplierMarking('<?php echo $y;?>')" onkeyup="enableSupplierMarking('<?php echo $y;?>')" 
					value="<?php echo ( $ctr > $y ? $new_supplier[$y] : $initCANVASSNewsupplier[$y] );?>" >
			</th>
	<?php 
		}	
	?>
</tr>
<tr>
	<th> Date </th>
	<th> Supplier </th>
	<th> Qty Ordered </th>
	<th> Unit Price </th>
</tr>
<?php
	$CanvassPREItemID = array();
	$CanvassPRENumber = array();
	$CanvassItemType = array();
	$CanvassItemID = array();
	$CanvassDescription = array();
	$CanvassQuantity = array();
	$POItemID = array();
	$PODate = array();
	$Supplier = array();
	$Quantity = array();
	$UnitPrice = array();
	foreach ($preId as $preId_key => $value) {

		$qry = mysqli_prepare($db, "CALL sp_Canvass_PRE_Details_Dropdown(?, ?)");
		mysqli_stmt_bind_param($qry, 'ii', $value, $hidCanvassID);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvass_pre_details.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else	
		{
			while($row = mysqli_fetch_assoc($result))
			{	
				$CanvassPREItemID[] = $row['CanvassPREItemID'];
				$CanvassPRENumber[] = $row['CanvassPRENumber'];
				$CanvassItemType[] = $row['CanvassItemType'];
				$CanvassItemID[] = $row['CanvassItemID'];
				$CanvassDescription[] = $row['CanvassDescription'];
				$CanvassQuantity[] = $row['CanvassQuantity'];
				$POItemID[] = $row['POItemID'];
				$PODate[] = $row['PODate'];
				$Supplier[] = $row['Supplier'];
				$Quantity[] = $row['Quantity'];
				$UnitPrice[] = $row['UnitPrice'];
			}
		}
		$db->next_result();
		$result->close();

	}

	$qryItem = mysqli_prepare($db, "CALL sp_Canvass_Item_Query(?)");
	mysqli_stmt_bind_param($qryItem, 'i', $hidCanvassID);
	$qryItem->execute();
	$resultItem = mysqli_stmt_get_result($qryItem);

	$processErrorItem = mysqli_error($db);

	if(!empty($processErrorItem))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvass_per_details.php'.'</td><td>'.$processErrorItem.' near line 35.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$canvass_item_id = array();
		$item_id = array();
		$item_type = array();
		$quantity = array();
		$pre_item_id = array();
		$last_purchase_item_id = array();	

		$CanvassItemID1 = array();
		$CanvassItemID2 = array();
		$CanvassItemID3 = array();
		$CanvassItemID4 = array();

		$CanvassDetails1 = array();
		$CanvassDetails2 = array();
		$CanvassDetails3 = array();
		$CanvassDetails4 = array();

		$Availability1 = array();
		$Availability2 = array();
		$Availability3 = array();
		$Availability4 = array();
		while($row = mysqli_fetch_assoc($resultItem))
		{
			$item_id[] = $row['item_id'];
			$item_type[] = $row['item_type'];
			$quantity[] = $row['quantity'];
			$pre_item_id[] = $row['pre_item_id'];
			$last_purchase_item_id[] = $row['last_purchase_item_id'];

			$CanvassItemID1[] = $row['CanvassItemID1'];
			$CanvassItemID2[] = $row['CanvassItemID2'];
			$CanvassItemID3[] = $row['CanvassItemID3'];
			$CanvassItemID4[] = $row['CanvassItemID4'];

			$CanvassDetails1[] = $row['CanvassDetails1'];
			$CanvassDetails2[] = $row['CanvassDetails2'];
			$CanvassDetails3[] = $row['CanvassDetails3'];
			$CanvassDetails4[] = $row['CanvassDetails4'];

			$Availability1[] = $row['Availability1'];
			$Availability2[] = $row['Availability2'];
			$Availability3[] = $row['Availability3'];
			$Availability4[] = $row['Availability4'];
		}
		$db->next_result();
		$resultItem->close();
	}
	
	$ctr2 = count($canvass_item_id);
	$canvass_item_count = 0;

	foreach ($CanvassPREItemID as $key => $value) {
?>
		<tr valign="top">
			<td>
				<input type="checkbox" name="chkInclude[<?php echo $key;?>]" id="chkInclude<?php echo $key;?>" 
					onchange="includePREDetail('<?php echo $key;?>')" 
					<?php echo ( $hidCanvassID ? "checked" : "" );?>>
			</td>

			<td> 
				<?php echo $CanvassQuantity[$key]; ?> 
				<input type="hidden" name="hidQuantity[<?php echo $key;?>]" id="hidQuantity<?php echo $key;?>" 
					value="<?php echo $CanvassQuantity[$key]; ?> ">
			</td>

			<td> 
				<?php echo $CanvassPRENumber[$key]; ?> 
				<input type="hidden" name="hidPREItemID[<?php echo $key;?>]" id="hidPREItemID<?php echo $key;?>" value="<?php echo $CanvassPREItemID[$key]; ?> ">
				<input type="hidden" name="hidItemID[<?php echo $key;?>]" id="hidItemID<?php echo $key;?>" value="<?php echo $CanvassItemID[$key]; ?> ">
				<input type="hidden" name="hidItemType[<?php echo $key;?>]" id="hidItemType<?php echo $key;?>" value="<?php echo $CanvassItemType[$key]; ?> ">
			</td>

			<td> <?php echo $CanvassDescription[$key]; ?> </td>

			<td> 
				<?php echo $PODate[$key]; ?> 
				<input type="hidden" name="hidLastTransInfo[<?php echo $key;?>]" id="hidLastPurchaseInfo<?php echo $key;?>" value="<?php echo $POItemID[$key]; ?> ">
			</td>

			<td> <?php echo $Supplier[$key]; ?> </td>

			<td> <?php echo $Quantity[$key]; ?> </td>

			<td> <?php echo $UnitPrice[$key]; ?> </td>

			<?php
				for ( $x = 0; $x < 4; $x++ ){
			?>		
					<td>
						<?php 
							if ( $hidCanvassID ){
								if ( $x == 0 ){
						?>			
									<input type="hidden" name="hidCanvassItemDetailsID[<?php echo $key;?>][<?php echo $x;?>]" id="hidCanvassItemDetailsID<?php echo $key.$x;?>" value="<?php echo $CanvassItemID1[$key];?>">
						<?php
								}elseif( $x == 1 ){
						?>			
									<input type="hidden" name="hidCanvassItemDetailsID[<?php echo $key;?>][<?php echo $x;?>]" id="hidCanvassItemDetailsID<?php echo $key.$x;?>" value="<?php echo $CanvassItemID2[$key];?>">
						<?php
								}elseif( $x == 2 ){
						?>			
									<input type="hidden" name="hidCanvassItemDetailsID[<?php echo $key;?>][<?php echo $x;?>]" id="hidCanvassItemDetailsID<?php echo $key.$x;?>" value="<?php echo $CanvassItemID3[$key];?>">
						<?php
								}elseif( $x == 3 ){
						?>			
									<input type="hidden" name="hidCanvassItemDetailsID[<?php echo $key;?>][<?php echo $x;?>]" id="hidCanvassItemDetailsID<?php echo $key.$x;?>" value="<?php echo $CanvassItemID4[$key];?>">
						<?php
								}
							}else{
						?>
								<input type="hidden" name="hidCanvassItemDetailsID[<?php echo $key;?>][<?php echo $x;?>]" id="hidCanvassItemDetailsID<?php echo $key.$x;?>">
						<?php
							}
						?>

						<textarea style="resize:vertical;" name="txtCanvassDetails[<?php echo $key;?>][<?php echo $x;?>]" id="txtCanvassDetails<?php echo $key.$x;?>" <?php echo ( $hidCanvassID ? "" : "readOnly" );?>><?php
							if ( $hidCanvassID ){
								$CanvassDetails1_array = explode("<br>", $CanvassDetails1[$key]);
								$CanvassDetails2_array = explode("<br>", $CanvassDetails2[$key]);
								$CanvassDetails3_array = explode("<br>", $CanvassDetails3[$key]);
								$CanvassDetails4_array = explode("<br>", $CanvassDetails4[$key]);

								if( $x == 0 ){
									foreach ($CanvassDetails1_array as $CanvassDetails1key => $CanvassDetails1_value) {
										echo $CanvassDetails1_value."\n";
									}
								}elseif( $x == 1 ){
									foreach ($CanvassDetails2_array as $CanvassDetails2key => $CanvassDetails2_value) {
										echo $CanvassDetails2_value."\n";
									}
								}elseif( $x == 2 ){
									foreach ($CanvassDetails3_array as $CanvassDetails3key => $CanvassDetails3_value) {
										echo $CanvassDetails3_value."\n";
									}
								}elseif( $x == 3 ){
									foreach ($CanvassDetails4_array as $CanvassDetails4key => $CanvassDetails4_value) {
										echo $CanvassDetails4_value."\n";
									}
								}
							}else{
								if ( $initCANVASSIncludeCount == 0 ){
									$initCANVASSCanvassDetails[$key][$x] = NULL;
								}

								$initCANVASSCanvassDetails_array = explode("<br>", $initCANVASSCanvassDetails[$key][$x]);

								foreach ($initCANVASSCanvassDetails_array as $initCANVASSCanvassDetailskey => $initCANVASSCanvassDetails_value) {
									echo $initCANVASSCanvassDetails_value."\n";
								}
							}
						?></textarea>

						<br><br>
						<?php 
							if ( $hidCanvassID ){
								if ( $x == 0){
						?>			
									<input type="checkbox" name="chkAvailability[<?php echo $key;?>][<?php echo $x;?>]" id="chkAvailability<?php echo $key.$x;?>" <?php echo ($Availability1[$key] ? "checked" : "");?> > 
						<?php
								}elseif( $x == 1 ){
						?>			
									<input type="checkbox" name="chkAvailability[<?php echo $key;?>][<?php echo $x;?>]" id="chkAvailability<?php echo $key.$x;?>" <?php echo ($Availability2[$key] ? "checked" : "");?>> 
						<?php
								}elseif( $x == 2 ){
						?>			
									<input type="checkbox" name="chkAvailability[<?php echo $key;?>][<?php echo $x;?>]" id="chkAvailability<?php echo $key.$x;?>" <?php echo ($Availability3[$key] ? "checked" : "");?>> 
						<?php
								}elseif( $x == 3 ){
						?>			
									<input type="checkbox" name="chkAvailability[<?php echo $key;?>][<?php echo $x;?>]" id="chkAvailability<?php echo $key.$x;?>" <?php echo ($Availability4[$key] ? "checked" : "");?>> 
						<?php
								}
							}else{
						?>
								<input type="checkbox" name="chkAvailability[<?php echo $key;?>][<?php echo $x;?>]" id="chkAvailability<?php echo $key.$x;?>" disabled > 
						<?php
							}
						?>
						<label for="chkAvailability<?php echo $key.$x;?>"> On Stock </label>

					</td>
			<?php
				}
			?>
		</tr>
<?php 
	}
?>

<tr>
	<td colspan="4" rowspan="5" valign="top">
		Remarks: 
		<br><br>
		<textarea name="txtRemarks" class="noresize"><?php
			if( $hidCanvassID ){
				$remarks_array = explode("<br>", $remarks);

				foreach ($remarks_array as $remarkskey => $remarks_value) {
					echo $remarks_value."\n";
				}
			}else{
				$initCANVASSRemarks_array = explode("<br>", $initCANVASSRemarks);

				foreach ($initCANVASSRemarks_array as $initCANVASSRemarkskey => $initCANVASSRemarks_value) {
					echo $initCANVASSRemarks_value."\n";
				}
			}
		?></textarea>
		
	</td>
<!-- ###################################### -->
	<td colspan="4" align="right"> Delivery Terms </td>
	<?php
		for ( $a = 0; $a < 4; $a++ ) { 
	?>
			<td>
				<select name="txtDeliveryTerms[<?php echo $a;?>]" id="txtDeliveryTerms<?php echo $a;?>">
					<option>  </option>

					<option value="Delivery" 
						<?php echo ( $ctr > $a ? ( $delivery_terms[$a] == "Delivery" ? "selected" : "" ) : ( $initCANVASSDeliveryTerms[$a] == "Delivery" ? "selected" : "" ) );?> >
							Delivery 
					</option>

					<option value="Pickup" 
						<?php echo ( $ctr > $a ? ( $delivery_terms[$a] == "Pickup" ? "selected" : "" ) : ( $initCANVASSDeliveryTerms[$a] == "Pickup" ? "selected" : "" ) );?> > 
							ickup 
					</option>
				</select>
			</td>
	<?php
		}	
	?>
</tr>
<!-- ###################################### -->
<tr>
	<td colspan="4" align="right"> Payment Terms </td>
	<?php
		for ( $b = 0; $b < 4; $b++ ) {
	?>
			<td>
				<input type="text" size="3" name="txtPaymentDays[<?php echo $b;?>]" id="txtPaymentDays<?php echo $b;?>" value="<?php echo ( $ctr > $b ? $payment_days[$b] : $initCANVASSPaymentDays[$b] ); ?>"> days
				
				<select name="txtPaymentTerms[<?php echo $b;?>]" id="txtPaymentTerms<?php echo $b;?>">
					<option>  </option>

					<option value="PDC" 
						<?php echo ( $ctr > $b ? ( $payment_terms[$b] == "PDC" ? "selected" : "" ) : ( $initCANVASSPaymentTerms[$b] == "PDC" ? "selected" : "" ) );?>> 
						PDC 
					</option>
					
					<option value="COD" 
						<?php echo ( $ctr > $b ? ( $payment_terms[$b] == "COD" ? "selected" : "" ) : ( $initCANVASSPaymentTerms[$b] == "COD" ? "selected" : "" ) );?>> 
						COD 
					</option>
				</select>
			</td>
	<?php
		}	
	?>
</tr>
<!-- ###################################### -->
<tr>
	<td colspan="4" align="right"> Contact Person </td>
	<?php
		for ( $c = 0; $c < 4; $c++ ) {
	?>
			<td>
				<input type="text" name="txtContactPerson[<?php echo $c;?>]" id="txtContactPerson<?php echo $c;?>" 
					value="<?php echo ( $ctr > $c ? $contact_person[$c] : $initCANVASSContactPerson[$c] ); ?>">
			</td>
	<?php
		}	
	?>
</tr>
<!-- ###################################### -->
<tr>
	<td colspan="4" align="right"> Telephone Number </td>
<?php
	for ( $d = 0; $d < 4; $d++ ) {
?>
		<td>
			<input type="text" name="txtTelephoneNum[<?php echo $d;?>]" id="txtTelephoneNum<?php echo $d;?>" 
				value="<?php echo ( $ctr > $d ? $telephone_number[$d] : $initCANVASSTelephoneNum[$d] ); ?>">
		</td>
<?php
	}	
?>
</tr>
<!-- ###################################### -->
<tr>
	<td colspan="4" align="right"> Fax Number </td>
<?php
	for ( $e = 0; $e < 4; $e++ ) {
?>
		<td>
			<input type="text" name="txtFaxNum[<?php echo $e;?>]" id="txtFaxNum<?php echo $e;?>" value="<?php echo ( $ctr > $e ? $fax_number[$e] : $initCANVASSFaxNum[$e] ); ?>">
		</td>
<?php
	}	
?>
</tr>

<?php
	require("/init_unset_values/canvass_unset_value.php");
	require("database_close.php");
?>