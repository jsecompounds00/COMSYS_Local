<?php

	require("database_connect.php");

	$qryPI = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Pellet_Inspection_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qryPI, 'is', $TypeID, $Type);
	$qryPI->execute();
	$resultPI = mysqli_stmt_get_result($qryPI);
	$processErrorPI = mysqli_error($db);

?>
	
	<table class="results_child_tables_form">
		<?php
			if ( !empty($processErrorPI) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_pellet_inspection_recommendation.php'.'</td><td>'.$processErrorPI.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultPI)){
					$PIJRDTestID = $row['PIJRDTestID'];
					$PIJRDFGSoft = $row['PIJRDFGSoft'];

					$PIClientsProduct = $row['PIClientsProduct']; 
					$PICACCProduct = $row['PICACCProduct']; 

					$PIClientsClarityColor = $row['PIClientsClarityColor']; 
					$PICACCClarityColor = $row['PICACCClarityColor']; 
		?>
					<tr>
						<th></th>
						<th> <?php echo $PIClientsProduct; ?> </th>
						<th> <?php echo $PICACCProduct; ?> </th>
					</tr>
				<!-- ###################### Clarity / Color Conform	 -->	
					<tr>
						<td> Clarity / Color Conform </td>
						<td> <?php echo $PIClientsClarityColor; ?> </td>
						<td> <?php echo $PICACCClarityColor; ?> </td>
					</tr>
		<?php
				}
				$db->next_result();
				$resultPI->close();
			}
		?>
	</table>
	<br>
<?php

	require("database_close.php");
?>