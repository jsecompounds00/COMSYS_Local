<!-- <br><br><br> -->
<?php
	session_start();
	require("/database_connect.php");
	require("/init_unset_values/jrd_init_value.php");
	$sltJRDNumber = intval($_GET["sltJRDNumber"]);

	if($errno)
	{
		$error = mysqli_connect_error();
		error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>jrd_data_noreplicate_dropdown.php"."</td><td>".$error." near line 13.</td></tr>", 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		if ( $sltJRDNumber != 0 ){
			$qry = mysqli_prepare($db, "CALL sp_JRD_Data_NoReplicate_Query(?)");
			mysqli_stmt_bind_param($qry, "i", $sltJRDNumber);
			$qry -> execute();
			$result = mysqli_stmt_get_result($qry);
			$processError = mysqli_error($db);

			if ($processError){
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>jrd_data_noreplicate_dropdown.php"."</td><td>".$processError." near line 20.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{	
				while($row = mysqli_fetch_assoc($result))
				{
					$date_requested = $row["date_requested"];
					$date_needed = $row["date_needed"];
					$type_of_request = $row["type_of_request"];
					$tag_eval_replicate = $row["tag_eval_replicate"];
					$finished_goods = $row["finished_goods"];
					$mod_eval_finished_good_id = $row["mod_eval_finished_good_id"];
					$JRDFormulaTypeID = $row["JRDFormulaTypeID"];
					$soft_pvc = $row["soft_pvc"];
					$eval_finished_good = $row["eval_finished_good"];
					$JRDNoReplicateID = $row["JRDNoReplicateID"];
					$ter_number = $row["ter_number"];
					$dynamic_milling = $row["dynamic_milling"];
					$oil_aging = $row["oil_aging"];
					$oven_aging = $row["oven_aging"];
					$strand_inspection = $row["strand_inspection"];
					$pellet_inspection = $row["pellet_inspection"];
					$impact_test = $row["impact_test"];
					$static_heating = $row["static_heating"];
					$color_change_test = $row["color_change_test"];
					$water_immersion = $row["water_immersion"];
					$cold_inspection = $row["cold_inspection"];
					$remarks = $row["remarks"];
					$remarks_array = explode("<br>", $remarks);
					$hidCreatedAt = $row["created_at"];
					$hidCreatedId = $row["created_id"];
				}
				$db->next_result();
				$result->close();
	?>			
				<table class="parent_tables_form">	
					<tr></tr>
					<tr>
						<td>TER No.:</td>
						<td>
							<input type='text' name='txtTERNo' value="<?php echo ( $JRDNoReplicateID ? $ter_number : $initTERNumber );?>">
						</td>
					</tr>
					<tr>
						<td> Activity: </td>
						<td> 
							<b> 
								Product Evaluation without Replication
								<input type="hidden" name="txtActivity" value="6">
							</b> 
						</td>
						<td> FG Type: </td>
						<td>
							<b> <?php echo ( $soft_pvc == 1 ? "SOFT" : "RIGID" ); ?> </b>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo ( $type_of_request == "Modification" ? "Item to be modified:" : ($type_of_request == "Evaluation" ? "Product for comparison:" : "New Product Name:" )); ?>
						</td>
						<td>
							<b> <?php  echo $finished_goods; ?>  </b>
							<input type="hidden" name="hidProductforComparisonID" value="<?php echo $mod_eval_finished_good_id;?>">
						</td>
						<td>
							Formula Type:
						</td>
						<td>
							<select name="sltFormulaType">
								<option></option>
								<?php
									$qryFT = mysqli_prepare($db, "CALL sp_FormulaType_Dropdown(?)");
									mysqli_stmt_bind_param($qryFT, "i", $mod_eval_finished_good_id);
									$qryFT -> execute();
									$resultFT = mysqli_stmt_get_result($qryFT);
									$processErrorFT = mysqli_error($db);

									if ($processErrorFT){
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>jrd_data_noreplicate_dropdown.php"."</td><td>".$processErrorFT." near line 20.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{	
										while($row = mysqli_fetch_assoc($resultFT))
										{
											$formula_type_id = $row["id"];
											$formula_type = $row["formula_type"];

											if ( $formula_type_id == $JRDFormulaTypeID )
												echo "<option value=".$formula_type_id." selected>".$formula_type."</option>";
											elseif ( $initFormulaTypeID == $formula_type_id )
												echo "<option value=".$formula_type_id." selected>".$formula_type."</option>";
											else
												echo "<option value=".$formula_type_id.">".$formula_type."</option>";
										}
										$db->next_result();
										$resultFT->close();
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td> Product for Evaluation: </td>
						<td>
							<b> <?php echo $eval_finished_good; ?> </b>
							<input type="hidden" name="hidProductforEvaluation" value="<?php echo $eval_finished_good;?>">
						</td>
					</tr>
						<input type='hidden' name='hidNoReplicateJRDID' id='hidNoReplicateJRDID' value="<?php echo $JRDNoReplicateID;?>">
						<input type='hidden' name='hidCreatedAt' value='<?php echo ( $JRDNoReplicateID ? $hidCreatedAt : date('Y-m-d H:i:s') );?>'>
						<input type='hidden' name='hidCreatedId' value='<?php echo ( $JRDNoReplicateID ? $hidCreatedId : $_SESSION['SESS_USER_ID'] ); ?>'>
				</table>

				<!-- <div id='trial'> -->
					<table class="child_tables_form	">
						<tr>
							<th colspan="5">Additional Tests:</th>
						</tr>
						<tr>
							<td>
								<input type='checkbox' name='chkDynamicMilling' id='DM' <?php echo ( $dynamic_milling ? "checked" : ( $initTagDynamicMilling ? "checked" : "" ) ); ?>>
									<label for='DM'>Dynamic Milling</label>
							</td>
							<td>
								<input type='checkbox' name='chkOilAging' id='OIA' <?php echo ( $oil_aging ? "checked" : ( $initTagOilAging ? "checked" : "" ) ); ?>>
									<label for='OIA'>Oil Aging</label>
							</td>
							<td>
								<input type='checkbox' name='chkOvenAging' id='OVA' <?php echo ( $oven_aging ? "checked" : ( $initTagOvenAging ? "checked" : "" ) ); ?>>
									<label for='OVA'>Oven Aging</label>
							</td>
							<td>
								<input type='checkbox' name='chkStrandInspect' id='Strand' <?php echo ( $strand_inspection ? "checked" : ( $initTagStrandInspect ? "checked" : "" ) ); ?>>
									<label for='Strand'>Strands Inspection</label>
							</td>
							<td>
								<input type='checkbox' name='chkPelletInspect' id='Pellet' <?php echo ( $pellet_inspection ? "checked" : ( $initPelletInspect ? "checked" : "" ) ); ?>>
									<label for='Pellet'>Pellet Inspection</label>
							</td>
						</tr>
						<tr>
							<td>
								<input type='checkbox' name='chkImpactTest' id='IT' <?php echo ( $impact_test ? "checked" : ( $initTagImpactTest ? "checked" : "" ) ); ?>>
									<label for='IT'>Impact Test</label>
							</td>
							<td>
								<input type='checkbox' name='chkStaticHeating' id='SH' <?php echo ( $static_heating ? "checked" : ( $initTagStaticHeating ? "checked" : "" ) ); ?>>
									<label for='SH'>Static Heating</label>
							</td>
							<td>
								<input type='checkbox' name='chkColorChange' id='ColorChange' <?php echo ( $color_change_test ? "checked" : ( $initTagColorChange ? "checked" : "" ) ); ?>>
									<label for='ColorChange'>Color Change Test</label>
							</td>
							<td>
								<input type='checkbox' name='chkWaterImmersion' id='WI' <?php echo ( $water_immersion ? "checked" : ( $initTagWaterImmersion ? "checked" : "" ) ); ?>>
									<label for='WI'>Water Immersion</label>
							</td>
							<td>
								<input type='checkbox' name='chkColdTesting' id='CI' <?php echo ( $cold_inspection ? "checked" : "" ); ?>>
									<label for='CI'>Cold Bend Test</label>
							</td>
						</tr>
					</table>
					<table class="comments_buttons">
						<tr>
							<td valign='top'>Remarks:</td>
							<td colspan=3>
								<textarea name='txtRemarks' class="paragraph"><?php
									if ( $remarks != "" ){
										echo $remarks;
									}else{
										echo $initRemarks;
									}
								?></textarea>
							</td>
						</tr> 
					</table>
				<!-- </div> -->
<?php
			}
		}
	}

	require("/init_unset_values/jrd_unset_value.php");
	require("/database_close.php"); 
?>