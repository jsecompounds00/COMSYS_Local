<?php

	// unset($_SESSION["page"]);
	// unset($_SESSION["search"]);
	// unset($_SESSION["qsone"]);

	######################### JRD #########################
	
	unset($_SESSION['txtJRDNumber']);
	unset($_SESSION['txtRequestedDate']);
	unset($_SESSION['txtNeededDate']);
	unset($_SESSION['sltExistingCustomer']);
	unset($_SESSION['chkNewCustomer']);
	unset($_SESSION['txtNewCustomer']);
	unset($_SESSION['txtContactPerson']);
	unset($_SESSION['radRequestType']);

	unset($_SESSION['chkExtrusion']);
	unset($_SESSION['radExtrusionType']);
	unset($_SESSION['txtOtherExtrusionType']);
	unset($_SESSION['radThermalRate']);
	unset($_SESSION['txtOtherThermalRate']);
	unset($_SESSION['chkMolding']);
	unset($_SESSION['radMoldingType']);
	unset($_SESSION['txtOtherMoldingType']);
	unset($_SESSION['chkOtherActivity']);
	unset($_SESSION['txtOtherActivity']);
	unset($_SESSION['chkUL']);
	unset($_SESSION['chkNone']);
	unset($_SESSION['chkPNS']);
	unset($_SESSION['chkOtherReq']);
	unset($_SESSION['txtOtherReq']);

	unset($_SESSION['chkCost']);
	unset($_SESSION['chkProcessabilityImprove']);
	unset($_SESSION['chkPropertyEnhance']);
	unset($_SESSION['chkLegalReq']);
	unset($_SESSION['chkOthers']);
	unset($_SESSION['txtOtherReason']);

	unset($_SESSION['txtCompetitorsProduct']);
	unset($_SESSION['txtCompetitorsName']);
	unset($_SESSION['chkNeedsReplication']);

	unset($_SESSION['sltFGItem']);

	unset($_SESSION['sltJRDNumber']);

	unset($_SESSION['txtTERNo']);
	unset($_SESSION['sltFormulaType']);
	unset($_SESSION['chkDynamicMilling']);
	unset($_SESSION['chkOilAging']);
	unset($_SESSION['chkOvenAging']);
	unset($_SESSION['chkStrandInspect']);
	unset($_SESSION['chkPelletInspect']);
	unset($_SESSION['chkImpactTest']);
	unset($_SESSION['chkStaticHeating']);
	unset($_SESSION['chkColorChange']);
	unset($_SESSION['chkWaterImmersion']);
	unset($_SESSION['txtRemarks']);

	unset($_SESSION['txtSpecificGravity']);
	unset($_SESSION['txtHardnessA']);
	unset($_SESSION['txtVolumeResistivity']);
	unset($_SESSION['txtHardnessD']);
	unset($_SESSION['txtUnagedTS']);
	unset($_SESSION['txtOvenTS']);
	unset($_SESSION['txtOilTS']);
	unset($_SESSION['txtUnagedE']);
	unset($_SESSION['txtOvenE']);
	unset($_SESSION['txtOilE']);

	unset($_SESSION['txtSampling']);
	unset($_SESSION['radDMColor']);
	unset($_SESSION['radDMHeat']);
	unset($_SESSION['radDMProcess']);
	unset($_SESSION['chkColorConform']);
	unset($_SESSION['chkPorous']);
	unset($_SESSION['chkFCPresent']);

	unset($_SESSION['chkClarityColorConform']);

	unset($_SESSION['txtDropWeight']);
	unset($_SESSION['txtDropHeight']);
	unset($_SESSION['txtSamplesNo']);
	unset($_SESSION['txtPassedNo']);

	unset($_SESSION['radSHColor']);
	unset($_SESSION['radSHHeat']);
	
	unset($_SESSION['radCCTColor']);
	unset($_SESSION['radCCTHeat']);

	unset($_SESSION['NR_txtCIResult1']);
	unset($_SESSION['NR_txtCIResult2']);
	unset($_SESSION['NR_txtCIResult3']);
	unset($_SESSION['NR_txtCICracked1']);
	unset($_SESSION['NR_txtCICracked2']);
	unset($_SESSION['NR_txtCICracked3']);

	######################### JRD CLIENT RESPONSE

	unset($_SESSION['sltJRD']);
	unset($_SESSION['txtRecommendation']);
	unset($_SESSION['txtTestObjective']);
	unset($_SESSION['txtIntroduction']);
	unset($_SESSION['txtMethodology']);
?>