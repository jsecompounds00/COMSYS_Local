<?php

	######################### DEBIT INVOICE #########################

	$initCJDICustomer = NULL;
	$initCJDITransType = NULL;
	$initCJDIInvoiceDate = NULL;
	$initCJDICheckDate = NULL;
	$initCJDICheckReturnedDate = NULL;
	$initCJDICheckEncode = NULL;
	$initCJDICheckAmount = NULL;
	$initCJDIkCode = NULL;
	$initCJDIcknumber = NULL;

	for ( $y = 0; $y <= 10; $y++ ){
		$initCJDIInvoiceNumber[$y] = NULL;
		$initCJDIAmount[$y] = NULL;
		$initCJDIDiscount[$y] = NULL;
	}

	if (isset($_SESSION['SESS_CJDI_Customer'])){
		$initCJDICustomer = $_SESSION['SESS_CJDI_Customer'];
		unset($_SESSION['SESS_CJDI_Customer']);
	}

	if (isset($_SESSION['SESS_CJDI_TransType'])){
		$initCJDITransType = $_SESSION['SESS_CJDI_TransType'];
		unset($_SESSION['SESS_CJDI_TransType']);
	}
	if (isset($_SESSION['SESS_CJDI_InvoiceDate'])){
		$initCJDIInvoiceDate = htmlspecialchars($_SESSION['SESS_CJDI_InvoiceDate']);
		unset($_SESSION['SESS_CJDI_InvoiceDate']);
	}
	if (isset($_SESSION['SESS_CJDI_CheckDate'])){
		$initCJDICheckDate = htmlspecialchars($_SESSION['SESS_CJDI_CheckDate']);
		unset($_SESSION['SESS_CJDI_CheckDate']);
	}
	if (isset($_SESSION['SESS_CJDI_CheckReturnedDate'])){
		$initCJDICheckReturnedDate = htmlspecialchars($_SESSION['SESS_CJDI_CheckReturnedDate']);
		unset($_SESSION['SESS_CJDI_CheckReturnedDate']);
	}
	if (isset($_SESSION['SESS_CJDI_CheckEncode'])){
		$initCJDICheckEncode = $_SESSION['SESS_CJDI_CheckEncode'];
		unset($_SESSION['SESS_CJDI_CheckEncode']);
	}
	if (isset($_SESSION['SESS_CJDI_CheckAmount'])){
		$initCJDICheckAmount = htmlspecialchars($_SESSION['SESS_CJDI_CheckAmount']);
		unset($_SESSION['SESS_CJDI_CheckAmount']);
	}
	if (isset($_SESSION['SESS_CJDI_kCode'])){
		$initCJDIkCode = htmlspecialchars($_SESSION['SESS_CJDI_kCode']);
		// unset($_SESSION['SESS_CJDI_kCode']);
	}
	if (isset($_SESSION['SESS_CJDI_cknumber'])){
		$initCJDIcknumber = htmlspecialchars($_SESSION['SESS_CJDI_cknumber']);
		// unset($_SESSION['SESS_CJDI_cknumber']);
	}

	for ( $x = 0; $x <= 10; $x++ ){
		if (isset($_SESSION['SESS_CJDI_InvoiceNumber'][$x])){
			$initCJDIInvoiceNumber[$x] = htmlspecialchars($_SESSION['SESS_CJDI_InvoiceNumber'][$x]);
			// unset($_SESSION['SESS_CJDI_InvoiceNumber'][$x]);
		}
		if (isset($_SESSION['SESS_CJDI_Amount'][$x])){
			$initCJDIAmount[$x] = htmlspecialchars($_SESSION['SESS_CJDI_Amount'][$x]);
			// unset($_SESSION['SESS_CJDI_Amount'][$x]);
		}
		if (isset($_SESSION['SESS_CJDI_Discount'][$x])){
			$initCJDIDiscount[$x] = htmlspecialchars($_SESSION['SESS_CJDI_Discount'][$x]);
			// unset($_SESSION['SESS_CJDI_Discount'][$x]);
		}
	}

?>