<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### CPIAR VERIFICATION #########################

	unset($_SESSION["SESS_CPM_CPRNo"]);
	unset($_SESSION["SESS_CPM_CPRDate"]);
	unset($_SESSION["SESS_CPM_DueDate"]);
	unset($_SESSION["SESS_CPM_IssueDeptID"]);
	unset($_SESSION["SESS_CPM_IssuerID"]);
	unset($_SESSION["SESS_CPM_ReceiveDeptID"]);
	unset($_SESSION["SESS_CPM_ReceiverID"]);
	unset($_SESSION["SESS_CPM_Occurrence"]);
	unset($_SESSION["SESS_CPM_Criticality"]);
	unset($_SESSION["SESS_CPM_Source"]);
	unset($_SESSION["SESS_CPM_Clause"]);
	unset($_SESSION["SESS_CPM_Details"]);
	unset($_SESSION["SESS_CPM_MemoDate"]);
	unset($_SESSION["SESS_CPM_FinalDueDate"]);
	unset($_SESSION["SESS_CPM_ReceiveDateDCC"]);
	unset($_SESSION["SESS_CPM_RootCause"]);
	unset($_SESSION["SESS_CPM_Containment"]);
	unset($_SESSION["SESS_CPM_ImplementContainment"]);
	unset($_SESSION["SESS_CPM_Action"]);
	unset($_SESSION["SESS_CPM_CorrectPrevent"]);
	unset($_SESSION["SESS_CPM_ImplementCorrectPrevent"]);
	unset($_SESSION["SESS_CPM_Man"]);
	unset($_SESSION["SESS_CPM_Method"]);
	unset($_SESSION["SESS_CPM_Machine"]);
	unset($_SESSION["SESS_CPM_Material"]);
	unset($_SESSION["SESS_CPM_Measurement"]);
	unset($_SESSION["SESS_CPM_MotherNature"]);
	unset($_SESSION["SESS_CPM_Remarks"]);
	unset($_SESSION["SESS_CPM_FromDivision"]);
	unset($_SESSION["SESS_CPM_ToDivision"]);

?>