<?php

	##############  ############## 

	$initBMDBudgetYear = NULL;
	$initBMDDepartment = NULL;
	$initBMDGLCode = NULL;
	$initBMDParticulars = NULL;
	$initBMDUOM = NULL;
	$initBMDUnitPrice = NULL;
	$initBMDJAN = NULL;
	$initBMDFEB = NULL;
	$initBMDMAR = NULL;
	$initBMDAPR = NULL;
	$initBMDMAY = NULL;
	$initBMDJUN = NULL;
	$initBMDJUL = NULL;
	$initBMDAUG = NULL;
	$initBMDSEP = NULL;
	$initBMDOCT = NULL;
	$initBMDNOV = NULL;
	$initBMDDEC = NULL;
	$initBMDADD = NULL;
	$initBMDRemarks = NULL;

	if (isset($_SESSION['SESS_BMD_BudgetYear'])){
		$initBMDBudgetYear = htmlspecialchars($_SESSION['SESS_BMD_BudgetYear']); 
		unset($_SESSION['SESS_BMD_BudgetYear']);
	}
	if (isset($_SESSION['SESS_BMD_Department'])){
		$initBMDDepartment = htmlspecialchars($_SESSION['SESS_BMD_Department']); 
		unset($_SESSION['SESS_BMD_Department']);
	}
	if (isset($_SESSION['SESS_BMD_GLCode'])){
		$initBMDGLCode = $_SESSION['SESS_BMD_GLCode']; 
		unset($_SESSION['SESS_BMD_GLCode']);
	}
	if (isset($_SESSION['SESS_BMD_Particulars'])){
		$initBMDParticulars = $_SESSION['SESS_BMD_Particulars']; 
		unset($_SESSION['SESS_BMD_Particulars']);
	}
	if (isset($_SESSION['SESS_BMD_UOM'])){
		$initBMDUOM = $_SESSION['SESS_BMD_UOM']; 
		unset($_SESSION['SESS_BMD_UOM']);
	}
	if (isset($_SESSION['SESS_BMD_UnitPrice'])){
		$initBMDUnitPrice = $_SESSION['SESS_BMD_UnitPrice']; 
		unset($_SESSION['SESS_BMD_UnitPrice']);
	}
	if (isset($_SESSION['SESS_BMD_JAN'])){
		$initBMDJAN = $_SESSION['SESS_BMD_JAN']; 
		unset($_SESSION['SESS_BMD_JAN']);
	}
	if (isset($_SESSION['SESS_BMD_FEB'])){
		$initBMDFEB = $_SESSION['SESS_BMD_FEB']; 
		unset($_SESSION['SESS_BMD_FEB']);
	}
	if (isset($_SESSION['SESS_BMD_MAR'])){
		$initBMDMAR = $_SESSION['SESS_BMD_MAR']; 
		unset($_SESSION['SESS_BMD_MAR']);
	}
	if (isset($_SESSION['SESS_BMD_APR'])){
		$initBMDAPR = $_SESSION['SESS_BMD_APR']; 
		unset($_SESSION['SESS_BMD_APR']);
	}
	if (isset($_SESSION['SESS_BMD_MAY'])){
		$initBMDMAY = $_SESSION['SESS_BMD_MAY']; 
		unset($_SESSION['SESS_BMD_MAY']);
	}
	if (isset($_SESSION['SESS_BMD_JUN'])){
		$initBMDJUN = $_SESSION['SESS_BMD_JUN']; 
		unset($_SESSION['SESS_BMD_JUN']);
	}
	if (isset($_SESSION['SESS_BMD_JUL'])){
		$initBMDJUL = $_SESSION['SESS_BMD_JUL']; 
		unset($_SESSION['SESS_BMD_JUL']);
	}
	if (isset($_SESSION['SESS_BMD_AUG'])){
		$initBMDAUG = $_SESSION['SESS_BMD_AUG']; 
		unset($_SESSION['SESS_BMD_AUG']);
	}
	if (isset($_SESSION['SESS_BMD_SEP'])){
		$initBMDSEP = $_SESSION['SESS_BMD_SEP']; 
		unset($_SESSION['SESS_BMD_SEP']);
	}
	if (isset($_SESSION['SESS_BMD_OCT'])){
		$initBMDOCT = $_SESSION['SESS_BMD_OCT']; 
		unset($_SESSION['SESS_BMD_OCT']);
	}
	if (isset($_SESSION['SESS_BMD_NOV'])){
		$initBMDNOV = $_SESSION['SESS_BMD_NOV']; 
		unset($_SESSION['SESS_BMD_NOV']);
	}
	if (isset($_SESSION['SESS_BMD_DEC'])){
		$initBMDDEC = $_SESSION['SESS_BMD_DEC']; 
		unset($_SESSION['SESS_BMD_DEC']);
	}
	if (isset($_SESSION['SESS_BMD_ADD'])){
		$initBMDADD = $_SESSION['SESS_BMD_ADD']; 
		unset($_SESSION['SESS_BMD_ADD']);
	}
	if (isset($_SESSION['SESS_BMD_Remarks'])){
		$initBMDRemarks = $_SESSION['SESS_BMD_Remarks']; 
		unset($_SESSION['SESS_BMD_Remarks']);
	}

?>