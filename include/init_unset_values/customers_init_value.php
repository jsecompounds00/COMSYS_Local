<?php

	############## CUSTOMERS ############## 

	$initCUSCustType = NULL;
	$initCUSTermsDays = NULL;
	$initCUSTerms = NULL;
	$initCUSCreditLimitUnit = NULL;
	$initCUSCreditLimit = NULL;
	$initCUSAgent = NULL;
	$initCUSAddressStreet = NULL;
	$initCUSAddressBaranggay = NULL;
	$initCUSAddressCityProvince = NULL;
	$initCUSZipCode = NULL;
	$initCUSTitle = NULL;
	$initCUSContactFName = NULL;
	$initCUSContactLName = NULL;
	$initCUSContactNumber = NULL;
	$initCUSGracePeriod = NULL;

	if (isset($_SESSION['SESS_CUS_CustType'])){
		$initCUSCustType = $_SESSION['SESS_CUS_CustType']; 
		unset($_SESSION['SESS_CUS_CustType']);
	}
	if (isset($_SESSION['SESS_CUS_TermsDays'])){
		$initCUSTermsDays = $_SESSION['SESS_CUS_TermsDays']; 
		unset($_SESSION['SESS_CUS_TermsDays']);
	}
	if (isset($_SESSION['SESS_CUS_Terms'])){
		$initCUSTerms = $_SESSION['SESS_CUS_Terms']; 
		unset($_SESSION['SESS_CUS_Terms']);
	}
	if (isset($_SESSION['SESS_CUS_CreditLimitUnit'])){
		$initCUSCreditLimitUnit = $_SESSION['SESS_CUS_CreditLimitUnit']; 
		unset($_SESSION['SESS_CUS_CreditLimitUnit']);
	}
	if (isset($_SESSION['SESS_CUS_CreditLimit'])){
		$initCUSCreditLimit = $_SESSION['SESS_CUS_CreditLimit']; 
		unset($_SESSION['SESS_CUS_CreditLimit']);
	}
	if (isset($_SESSION['SESS_CUS_Agent'])){
		$initCUSAgent = $_SESSION['SESS_CUS_Agent']; 
		unset($_SESSION['SESS_CUS_Agent']);
	}
	if (isset($_SESSION['SESS_CUS_AddressStreet'])){
		$initCUSAddressStreet = htmlspecialchars($_SESSION['SESS_CUS_AddressStreet']); 
		unset($_SESSION['SESS_CUS_AddressStreet']);
	}
	if (isset($_SESSION['SESS_CUS_AddressBaranggay'])){
		$initCUSAddressBaranggay = htmlspecialchars($_SESSION['SESS_CUS_AddressBaranggay']); 
		unset($_SESSION['SESS_CUS_AddressBaranggay']);
	}
	if (isset($_SESSION['SESS_CUS_AddressCityProvince'])){
		$initCUSAddressCityProvince = htmlspecialchars($_SESSION['SESS_CUS_AddressCityProvince']); 
		unset($_SESSION['SESS_CUS_AddressCityProvince']);
	}
	if (isset($_SESSION['SESS_CUS_ZipCode'])){
		$initCUSZipCode = $_SESSION['SESS_CUS_ZipCode']; 
		unset($_SESSION['SESS_CUS_ZipCode']);
	}
	if (isset($_SESSION['SESS_CUS_Title'])){
		$initCUSTitle = $_SESSION['SESS_CUS_Title']; 
		unset($_SESSION['SESS_CUS_Title']);
	}
	if (isset($_SESSION['SESS_CUS_ContactFName'])){
		$initCUSContactFName = htmlspecialchars($_SESSION['SESS_CUS_ContactFName']); 
		unset($_SESSION['SESS_CUS_ContactFName']);
	}
	if (isset($_SESSION['SESS_CUS_ContactLName'])){
		$initCUSContactLName = htmlspecialchars($_SESSION['SESS_CUS_ContactLName']); 
		unset($_SESSION['SESS_CUS_ContactLName']);
	}
	if (isset($_SESSION['SESS_CUS_ContactNumber'])){
		$initCUSContactNumber = htmlspecialchars($_SESSION['SESS_CUS_ContactNumber']); 
		unset($_SESSION['SESS_CUS_ContactNumber']);
	}
	if (isset($_SESSION['SESS_CUS_GracePeriod'])){
		$initCUSGracePeriod = $_SESSION['SESS_CUS_GracePeriod']; 
		unset($_SESSION['SESS_CUS_GracePeriod']);
	}

?>