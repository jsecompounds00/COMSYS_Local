<?php

	############## FG CATEGORY ############## 

	$initFGSPECSFromSpecificGravity = NULL;
	$initFGSPECSToSpecificGravity = NULL;
	$initFGSPECSFromHardnessA = NULL;
	$initFGSPECSToHardnessA = NULL;
	$initFGSPECSFromHardnessD = NULL;
	$initFGSPECSToHardnessD = NULL;
	$initFGSPECSVolumeResistivity = NULL;
	$initFGSPECSUnagedTS = NULL;
	$initFGSPECSOvenTS = NULL;
	$initFGSPECSOilTS = NULL;
	$initFGSPECSUnagedE = NULL;
	$initFGSPECSOvenE = NULL;
	$initFGSPECSOilE = NULL;

	if (isset($_SESSION['SESS_FGSPECS_FromSpecificGravity'])){
		$initFGSPECSFromSpecificGravity = htmlspecialchars($_SESSION['SESS_FGSPECS_FromSpecificGravity']); 
		unset($_SESSION['SESS_FGSPECS_FromSpecificGravity']);
	}
	if (isset($_SESSION['SESS_FGSPECS_ToSpecificGravity'])){
		$initFGSPECSToSpecificGravity = htmlspecialchars($_SESSION['SESS_FGSPECS_ToSpecificGravity']); 
		unset($_SESSION['SESS_FGSPECS_ToSpecificGravity']);
	}
	if (isset($_SESSION['SESS_FGSPECS_FromHardnessA'])){
		$initFGSPECSFromHardnessA = htmlspecialchars($_SESSION['SESS_FGSPECS_FromHardnessA']); 
		unset($_SESSION['SESS_FGSPECS_FromHardnessA']);
	}
	if (isset($_SESSION['SESS_FGSPECS_ToHardnessA'])){
		$initFGSPECSToHardnessA = htmlspecialchars($_SESSION['SESS_FGSPECS_ToHardnessA']); 
		unset($_SESSION['SESS_FGSPECS_ToHardnessA']);
	}
	if (isset($_SESSION['SESS_FGSPECS_FromHardnessD'])){
		$initFGSPECSFromHardnessD = htmlspecialchars($_SESSION['SESS_FGSPECS_FromHardnessD']); 
		unset($_SESSION['SESS_FGSPECS_FromHardnessD']);
	}
	if (isset($_SESSION['SESS_FGSPECS_ToHardnessD'])){
		$initFGSPECSToHardnessD = htmlspecialchars($_SESSION['SESS_FGSPECS_ToHardnessD']); 
		unset($_SESSION['SESS_FGSPECS_ToHardnessD']);
	}
	if (isset($_SESSION['SESS_FGSPECS_VolumeResistivity'])){
		$initFGSPECSVolumeResistivity = htmlspecialchars($_SESSION['SESS_FGSPECS_VolumeResistivity']); 
		unset($_SESSION['SESS_FGSPECS_VolumeResistivity']);
	}
	if (isset($_SESSION['SESS_FGSPECS_UnagedTS'])){
		$initFGSPECSUnagedTS = htmlspecialchars($_SESSION['SESS_FGSPECS_UnagedTS']); 
		unset($_SESSION['SESS_FGSPECS_UnagedTS']);
	}
	if (isset($_SESSION['SESS_FGSPECS_OvenTS'])){
		$initFGSPECSOvenTS = htmlspecialchars($_SESSION['SESS_FGSPECS_OvenTS']); 
		unset($_SESSION['SESS_FGSPECS_OvenTS']);
	}
	if (isset($_SESSION['SESS_FGSPECS_OilTS'])){
		$initFGSPECSOilTS = htmlspecialchars($_SESSION['SESS_FGSPECS_OilTS']); 
		unset($_SESSION['SESS_FGSPECS_OilTS']);
	}
	if (isset($_SESSION['SESS_FGSPECS_UnagedE'])){
		$initFGSPECSUnagedE = htmlspecialchars($_SESSION['SESS_FGSPECS_UnagedE']); 
		unset($_SESSION['SESS_FGSPECS_UnagedE']);
	}
	if (isset($_SESSION['SESS_FGSPECS_OvenE'])){
		$initFGSPECSOvenE = htmlspecialchars($_SESSION['SESS_FGSPECS_OvenE']); 
		unset($_SESSION['SESS_FGSPECS_OvenE']);
	}
	if (isset($_SESSION['SESS_FGSPECS_OilE'])){
		$initFGSPECSOilE = htmlspecialchars($_SESSION['SESS_FGSPECS_OilE']); 
		unset($_SESSION['SESS_FGSPECS_OilE']);
	}

?>