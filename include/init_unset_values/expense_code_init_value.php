<?php

	############## EXPENSE CODE ############## 

	$initEXPCode = NULL;
	$initEXPDescription = NULL;
	$initEXPActive = NULL;
	$initEXPDepartment = NULL;
	$initEXPMiscellaneous = NULL;

	if (isset($_SESSION['SESS_EXP_Code'])){
		$initEXPCode = htmlspecialchars($_SESSION['SESS_EXP_Code']); 
		unset($_SESSION['SESS_EXP_Code']);
	}
	if (isset($_SESSION['SESS_EXP_Description'])){
		$initEXPDescription = htmlspecialchars($_SESSION['SESS_EXP_Description']); 
		unset($_SESSION['SESS_EXP_Description']);
	}
	if (isset($_SESSION['SESS_EXP_Active'])){
		$initEXPActive = $_SESSION['SESS_EXP_Active']; 
		unset($_SESSION['SESS_EXP_Active']);
	}
	if (isset($_SESSION['SESS_EXP_Department'])){
		$initEXPDepartment = $_SESSION['SESS_EXP_Department']; 
		unset($_SESSION['SESS_EXP_Department']);
	}
	if (isset($_SESSION['SESS_EXP_Miscellaneous'])){
		$initEXPMiscellaneous = $_SESSION['SESS_EXP_Miscellaneous']; 
		unset($_SESSION['SESS_EXP_Miscellaneous']);
	}

?>