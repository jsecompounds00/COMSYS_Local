<?php

	############## PERMISSION ############## 

	$initPERPermission = NULL;
	$initPERModule = NULL;

	if (isset($_SESSION['SESS_PER_Permission'])){
		$initPERPermission = htmlspecialchars($_SESSION['SESS_PER_Permission']); 
		unset($_SESSION['SESS_PER_Permission']);
	}
	if (isset($_SESSION['SESS_PER_Module'])){
		$initPERModule = htmlspecialchars($_SESSION['SESS_PER_Module']); 
		unset($_SESSION['SESS_PER_Module']);
	}

?>