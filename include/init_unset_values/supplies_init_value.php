<?php

	############## SUPPLIES ############## 

	$initSUPSupplyTypeId = NULL;
	$initSUPSupply = NULL;
	$initSUPDescription = NULL;
	$initSUPComments = NULL;
	$initSUPUom = NULL;
	$initSUPCmpds = NULL;
	$initSUPPips = NULL;
	$initSUPCorp = NULL;
	$initSUPPPR = NULL;
	$initSUPActive = NULL;
	$initSUPmisc = NULL;

	if (isset($_SESSION['SESS_SUP_SupplyTypeId'])){
		$initSUPSupplyTypeId = $_SESSION['SESS_SUP_SupplyTypeId']; 
		unset($_SESSION['SESS_SUP_SupplyTypeId']);
	}
	if (isset($_SESSION['SESS_SUP_Supply'])){
		$initSUPSupply = htmlspecialchars($_SESSION['SESS_SUP_Supply']); 
		unset($_SESSION['SESS_SUP_Supply']);
	}
	if (isset($_SESSION['SESS_SUP_Description'])){
		$initSUPDescription = htmlspecialchars($_SESSION['SESS_SUP_Description']); 
		unset($_SESSION['SESS_SUP_Description']);
	}
	if (isset($_SESSION['SESS_SUP_Comments'])){
		$initSUPComments = htmlspecialchars($_SESSION['SESS_SUP_Comments']); 
		unset($_SESSION['SESS_SUP_Comments']);
	}
	if (isset($_SESSION['SESS_SUP_Uom'])){
		$initSUPUom = $_SESSION['SESS_SUP_Uom']; 
		unset($_SESSION['SESS_SUP_Uom']);
	}
	if (isset($_SESSION['SESS_SUP_Cmpds'])){
		$initSUPCmpds = $_SESSION['SESS_SUP_Cmpds']; 
		unset($_SESSION['SESS_SUP_Cmpds']);
	}
	if (isset($_SESSION['SESS_SUP_Pips'])){
		$initSUPPips = $_SESSION['SESS_SUP_Pips']; 
		unset($_SESSION['SESS_SUP_Pips']);
	}
	if (isset($_SESSION['SESS_SUP_Corp'])){
		$initSUPCorp = $_SESSION['SESS_SUP_Corp']; 
		unset($_SESSION['SESS_SUP_Corp']);
	}
	if (isset($_SESSION['SESS_SUP_PPR'])){
		$initSUPPPR = $_SESSION['SESS_SUP_PPR']; 
		unset($_SESSION['SESS_SUP_PPR']);
	}
	if (isset($_SESSION['SESS_SUP_Active'])){
		$initSUPActive = $_SESSION['SESS_SUP_Active']; 
		unset($_SESSION['SESS_SUP_Active']);
	}
	if (isset($_SESSION['SESS_SUP_Miscellaneous'])){
		$initSUPmisc = $_SESSION['SESS_SUP_Miscellaneous']; 
		unset($_SESSION['SESS_SUP_Miscellaneous']);
	}

?>