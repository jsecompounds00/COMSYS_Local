<?php

	unset($_SESSION['page']);
	unset($_SESSION['search']);
	unset($_SESSION['qsone']);
	
	############## CUSTOMERS ############## 

	unset($_SESSION['SESS_CUS_CustType']);
	unset($_SESSION['SESS_CUS_TermsDays']);
	unset($_SESSION['SESS_CUS_Terms']);
	unset($_SESSION['SESS_CUS_CreditLimitUnit']);
	unset($_SESSION['SESS_CUS_CreditLimit']);
	unset($_SESSION['SESS_CUS_Agent']);
	unset($_SESSION['SESS_CUS_AddressStreet']);
	unset($_SESSION['SESS_CUS_AddressBaranggay']);
	unset($_SESSION['SESS_CUS_AddressCityProvince']);
	unset($_SESSION['SESS_CUS_ZipCode']);
	unset($_SESSION['SESS_CUS_Title']);
	unset($_SESSION['SESS_CUS_ContactFName']);
	unset($_SESSION['SESS_CUS_ContactLName']);
	unset($_SESSION['SESS_CUS_ContactNumber']);
	unset($_SESSION['SESS_CUS_GracePeriod']);

?>