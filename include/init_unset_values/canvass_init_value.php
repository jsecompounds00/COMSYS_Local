<?php

	######################### CANVASS #########################

	$initCANVASSDate = NULL;
	$initCANVASSDivision = NULL;
	$initCANVASSRemarks = NULL;
	$initCANVASSPRE = NULL;
	$initCANVASSIncludeCount = 0;

	for( $x = 0; $x < 4; $x++ ){
		$initCANVASSSupplier[$x] = NULL;
		$initCANVASSNewsupplier[$x] = NULL;
		$initCANVASSDeliveryTerms[$x] = NULL;
		$initCANVASSPaymentDays[$x] = NULL;
		$initCANVASSPaymentTerms[$x] = NULL;
		$initCANVASSContactPerson[$x] = NULL;
		$initCANVASSTelephoneNum[$x] = NULL;
		$initCANVASSFaxNum[$x] = NULL;
	}

	for ( $x1 = 0; $x1 < $initCANVASSIncludeCount; $x1++ ) {

		$initCANVASSInclude[$x1] = 0;

		for ( $y1 = 0; $y1 < 4; $y1++ ){

			$initCANVASSAvailability[$x1][$y1] = 0;
			$initCANVASSCanvassDetails[$x1][$y1] = NULL;

		}

	}

	
	if (isset($_SESSION['SESS_CANVASS_Date'])){
		$initCANVASSDate = htmlspecialchars($_SESSION['SESS_CANVASS_Date']);
		unset($_SESSION['SESS_CANVASS_Date']);
	}
	
	if (isset($_SESSION['SESS_CANVASS_Division'])){
		$initCANVASSDivision = $_SESSION['SESS_CANVASS_Division'];
		unset($_SESSION['SESS_CANVASS_Division']);
	}
	
	if (isset($_SESSION['SESS_CANVASS_Remarks'])){
		$initCANVASSRemarks = htmlspecialchars($_SESSION['SESS_CANVASS_Remarks']);
		// unset($_SESSION['SESS_CANVASS_Remarks']);
	}
	
	if (isset($_SESSION['SESS_CANVASS_PRE'])){
		$initCANVASSPRE = $_SESSION['SESS_CANVASS_PRE'];
		unset($_SESSION['SESS_CANVASS_PRE']);
	}


	for ( $i = 0; $i < 4; $i++ ){
		if ( isset( $_SESSION['SESS_CANVASS_Supplier'][$i] ) ){
			$initCANVASSSupplier[$i] = $_SESSION['SESS_CANVASS_Supplier'][$i];
		}
		if ( isset( $_SESSION['SESS_CANVASS_Newsupplier'][$i] ) ){
			$initCANVASSNewsupplier[$i] = htmlspecialchars($_SESSION['SESS_CANVASS_Newsupplier'][$i]);
		}
		if ( isset( $_SESSION['SESS_CANVASS_DeliveryTerms'][$i] ) ){
			$initCANVASSDeliveryTerms[$i] = htmlspecialchars($_SESSION['SESS_CANVASS_DeliveryTerms'][$i]);
		}
		if ( isset( $_SESSION['SESS_CANVASS_PaymentDays'][$i] ) ){
			$initCANVASSPaymentDays[$i] = htmlspecialchars($_SESSION['SESS_CANVASS_PaymentDays'][$i]);
		}
		if ( isset( $_SESSION['SESS_CANVASS_PaymentTerms'][$i] ) ){
			$initCANVASSPaymentTerms[$i] = htmlspecialchars($_SESSION['SESS_CANVASS_PaymentTerms'][$i]);
		}
		if ( isset( $_SESSION['SESS_CANVASS_ContactPerson'][$i] ) ){
			$initCANVASSContactPerson[$i] = htmlspecialchars($_SESSION['SESS_CANVASS_ContactPerson'][$i]);
		}
		if ( isset( $_SESSION['SESS_CANVASS_TelephoneNum'][$i] ) ){
			$initCANVASSTelephoneNum[$i] = htmlspecialchars($_SESSION['SESS_CANVASS_TelephoneNum'][$i]);
		}
		if ( isset( $_SESSION['SESS_CANVASS_FaxNum'][$i] ) ){
			$initCANVASSFaxNum[$i] = htmlspecialchars($_SESSION['SESS_CANVASS_FaxNum'][$i]);
		}
	}

	if ( isset( $_SESSION["SESS_CANVASS_IncludeCount"] ) ){  
		$initCANVASSIncludeCount = $_SESSION["SESS_CANVASS_IncludeCount"];
	}

	for ( $sess_key = 0; $sess_key < $initCANVASSIncludeCount; $sess_key++ ) {

		if ( isset( $_SESSION['SESS_CANVASS_Include'][$sess_key] ) ){
			$initCANVASSInclude[$sess_key] = $_SESSION["SESS_CANVASS_Include"][$sess_key];
		}

		for ( $yy = 0; $yy < 4; $yy++ ){

			$initCANVASSAvailability[$sess_key][$yy] = $_SESSION["SESS_CANVASS_Availability"][$sess_key][$yy];
			$initCANVASSCanvassDetails[$sess_key][$yy] = htmlspecialchars($_SESSION["SESS_CANVASS_CanvassDetails"][$sess_key][$yy]);

		}

	}

?>