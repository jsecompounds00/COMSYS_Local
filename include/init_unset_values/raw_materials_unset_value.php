<?php

	unset($_SESSION['page']);
	unset($_SESSION['name_text']);
	unset($_SESSION['code_text']);
	unset($_SESSION['type_text']);
	
	##############  ############## 

	unset($_SESSION['SESS_RMS_Compounds']);
	unset($_SESSION['SESS_RMS_Pipes']);
	unset($_SESSION['SESS_RMS_Corporate']);
	unset($_SESSION['SESS_RMS_PPR']);
	unset($_SESSION['SESS_RMS_RMType']);
	unset($_SESSION['SESS_RMS_RMCode']);
	unset($_SESSION['SESS_RMS_RMName']);
	unset($_SESSION['SESS_RMS_Description']);
	unset($_SESSION['SESS_RMS_UOM']);
	unset($_SESSION['SESS_RMS_Local']);
	unset($_SESSION['SESS_RMS_Active']);
	unset($_SESSION['SESS_RMS_Packaging']);
	unset($_SESSION['SESS_RMS_AveMonthlyUsage']);

?>