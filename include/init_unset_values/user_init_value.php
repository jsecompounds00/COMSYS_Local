<?php

	############## SUPPLY TYPE ############## 

	$initUSRFirstName = NULL;
	$initUSRLastName = NULL;
	$initUSRUserName = NULL;
	$initUSRActive = NULL;
	$initUSRCompounds = NULL;
	$initUSRPipes = NULL;
	$initUSRCorporate = NULL;
	$initUSRPPR = NULL;

	if (isset($_SESSION['SESS_USR_FirstName'])){
		$initUSRFirstName = htmlspecialchars($_SESSION['SESS_USR_FirstName']); 
		unset($_SESSION['SESS_USR_FirstName']);
	}
	if (isset($_SESSION['SESS_USR_LastName'])){
		$initUSRLastName = htmlspecialchars($_SESSION['SESS_USR_LastName']); 
		unset($_SESSION['SESS_USR_LastName']);
	}
	if (isset($_SESSION['SESS_USR_UserName'])){
		$initUSRUserName = htmlspecialchars($_SESSION['SESS_USR_UserName']); 
		unset($_SESSION['SESS_USR_UserName']);
	}
	if (isset($_SESSION['SESS_USR_Active'])){
		$initUSRActive = $_SESSION['SESS_USR_Active']; 
		unset($_SESSION['SESS_USR_Active']);
	}
	if (isset($_SESSION['SESS_USR_Compounds'])){
		$initUSRCompounds = $_SESSION['SESS_USR_Compounds']; 
		unset($_SESSION['SESS_USR_Compounds']);
	}
	if (isset($_SESSION['SESS_USR_Pipes'])){
		$initUSRPipes = $_SESSION['SESS_USR_Pipes']; 
		unset($_SESSION['SESS_USR_Pipes']);
	}
	if (isset($_SESSION['SESS_USR_Corporate'])){
		$initUSRCorporate = $_SESSION['SESS_USR_Corporate']; 
		unset($_SESSION['SESS_USR_Corporate']);
	}
	if (isset($_SESSION['SESS_USR_PPR'])){
		$initUSRPPR = $_SESSION['SESS_USR_PPR']; 
		unset($_SESSION['SESS_USR_PPR']);
	}

?>