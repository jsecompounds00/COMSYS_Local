<?php

	##############  ############## 

	$initRMSCompounds = NULL;
	$initRMSPipes = NULL;
	$initRMSCorporate = NULL;
	$initRMSPPR = NULL;
	$initRMSRMType = NULL;
	$initRMSRMCode = NULL;
	$initRMSRMName = NULL;
	$initRMSDescription = NULL;
	$initRMSUOM = NULL;
	$initRMSLocal = NULL;
	$initRMSActive = NULL;
	$initRMSPackaging = NULL;
	$initRMSAveMonthlyUsage = NULL;

	if (isset($_SESSION['SESS_RMS_Compounds'])){
		$initRMSCompounds = $_SESSION['SESS_RMS_Compounds']; 
		unset($_SESSION['SESS_RMS_Compounds']);
	}
	if (isset($_SESSION['SESS_RMS_Pipes'])){
		$initRMSPipes = $_SESSION['SESS_RMS_Pipes']; 
		unset($_SESSION['SESS_RMS_Pipes']);
	}
	if (isset($_SESSION['SESS_RMS_Corporate'])){
		$initRMSCorporate = $_SESSION['SESS_RMS_Corporate']; 
		unset($_SESSION['SESS_RMS_Corporate']);
	}
	if (isset($_SESSION['SESS_RMS_PPR'])){
		$initRMSPPR = $_SESSION['SESS_RMS_PPR']; 
		unset($_SESSION['SESS_RMS_PPR']);
	}
	if (isset($_SESSION['SESS_RMS_RMType'])){
		$initRMSRMType = $_SESSION['SESS_RMS_RMType']; 
		unset($_SESSION['SESS_RMS_RMType']);
	}
	if (isset($_SESSION['SESS_RMS_RMCode'])){
		$initRMSRMCode = htmlspecialchars($_SESSION['SESS_RMS_RMCode']); 
		unset($_SESSION['SESS_RMS_RMCode']);
	}
	if (isset($_SESSION['SESS_RMS_RMName'])){
		$initRMSRMName = htmlspecialchars($_SESSION['SESS_RMS_RMName']); 
		unset($_SESSION['SESS_RMS_RMName']);
	}
	if (isset($_SESSION['SESS_RMS_Description'])){
		$initRMSDescription = htmlspecialchars($_SESSION['SESS_RMS_Description']); 
		unset($_SESSION['SESS_RMS_Description']);
	}
	if (isset($_SESSION['SESS_RMS_UOM'])){
		$initRMSUOM = $_SESSION['SESS_RMS_UOM']; 
		unset($_SESSION['SESS_RMS_UOM']);
	}
	if (isset($_SESSION['SESS_RMS_Local'])){
		$initRMSLocal = $_SESSION['SESS_RMS_Local']; 
		unset($_SESSION['SESS_RMS_Local']);
	}
	if (isset($_SESSION['SESS_RMS_Active'])){
		$initRMSActive = $_SESSION['SESS_RMS_Active']; 
		unset($_SESSION['SESS_RMS_Active']);
	}
	if (isset($_SESSION['SESS_RMS_Packaging'])){
		$initRMSPackaging = htmlspecialchars($_SESSION['SESS_RMS_Packaging']); 
		unset($_SESSION['SESS_RMS_Packaging']);
	}
	if (isset($_SESSION['SESS_RMS_AveMonthlyUsage'])){
		$initRMSAveMonthlyUsage = htmlspecialchars($_SESSION['SESS_RMS_AveMonthlyUsage']); 
		unset($_SESSION['SESS_RMS_AveMonthlyUsage']);
	}

?>