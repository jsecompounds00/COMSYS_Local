<?php

	######################### JRD #########################

	$initJRDNumber = NULL;
	$initDateRequested = NULL;
	$initDateNeedeed = NULL;
	$initCustomerID = NULL;
	$initTagNewCustomer = NULL;
	$initNewCustomer = NULL;
	$initContactPerson = NULL;
	$initRequestType = NULL;
	
	if (isset($_SESSION['txtJRDNumber'])){
		$initJRDNumber = htmlspecialchars($_SESSION['txtJRDNumber']);
		unset($_SESSION['txtJRDNumber']);
	}
	
	if (isset($_SESSION['txtRequestedDate'])){
		$initDateRequested = htmlspecialchars($_SESSION['txtRequestedDate']);
		unset($_SESSION['txtRequestedDate']);
	}
	
	if (isset($_SESSION['txtNeededDate'])){
		$initDateNeedeed = htmlspecialchars($_SESSION['txtNeededDate']);
		unset($_SESSION['txtNeededDate']);
	}
	
	if (isset($_SESSION['sltExistingCustomer'])){
		$initCustomerID = $_SESSION['sltExistingCustomer'];
		unset($_SESSION['sltExistingCustomer']);
	}
	
	if (isset($_SESSION['chkNewCustomer'])){
		$initTagNewCustomer = $_SESSION['chkNewCustomer'];
		unset($_SESSION['chkNewCustomer']);
	}
	
	if (isset($_SESSION['txtNewCustomer'])){
		$initNewCustomer = htmlspecialchars($_SESSION['txtNewCustomer']);
		unset($_SESSION['txtNewCustomer']);
	}
	
	if (isset($_SESSION['txtContactPerson'])){
		$initContactPerson = htmlspecialchars($_SESSION['txtContactPerson']);
		unset($_SESSION['txtContactPerson']);
	}
	
	if (isset($_SESSION['radRequestType'])){
		$initRequestType = $_SESSION['radRequestType'];
		unset($_SESSION['radRequestType']);
	}

	######################### PRODUCT DEVELOPMENT

	$initTagExtrusion = NULL;
	$initExtrusionType = NULL;
	$initOtherExtrusionType = NULL;
	$initThermalRate = NULL;
	$initOtherThermalRate = NULL;
	$initTagMolding = NULL;
	$initMoldingType = NULL;
	$initOtherMoldingType = NULL;
	$initTagOtherActivity = NULL;
	$initOtherActivity = NULL;
	$initTagUL = NULL;
	$initTagNone = NULL;
	$initTagPNS = NULL;
	$initTagOtherRequirement = NULL;
	$initOtherRequirement = NULL;
	
	if (isset($_SESSION['chkExtrusion'])){
		$initTagExtrusion = $_SESSION['chkExtrusion'];
		// unset($_SESSION['chkExtrusion']);
	}
	
	if (isset($_SESSION['radExtrusionType'])){
		$initExtrusionType = $_SESSION['radExtrusionType'];
		// unset($_SESSION['radExtrusionType']);
	}
	
	if (isset($_SESSION['txtOtherExtrusionType'])){
		$initOtherExtrusionType = htmlspecialchars($_SESSION['txtOtherExtrusionType']);
		// unset($_SESSION['txtOtherExtrusionType']);
	}
	
	if (isset($_SESSION['radThermalRate'])){
		$initThermalRate = $_SESSION['radThermalRate'];
		// unset($_SESSION['radThermalRate']);
	}
	
	if (isset($_SESSION['txtOtherThermalRate'])){
		$initOtherThermalRate = htmlspecialchars($_SESSION['txtOtherThermalRate']);
		// unset($_SESSION['txtOtherThermalRate']);
	}
	
	if (isset($_SESSION['chkMolding'])){
		$initTagMolding = $_SESSION['chkMolding'];
		// unset($_SESSION['chkMolding']);
	}
	
	if (isset($_SESSION['radMoldingType'])){
		$initMoldingType = $_SESSION['radMoldingType'];
		// unset($_SESSION['radMoldingType']);
	}
	
	if (isset($_SESSION['txtOtherMoldingType'])){
		$initOtherMoldingType = htmlspecialchars($_SESSION['txtOtherMoldingType']);
		// unset($_SESSION['txtOtherMoldingType']);
	}
	
	if (isset($_SESSION['chkOtherActivity'])){
		$initTagOtherActivity = $_SESSION['chkOtherActivity'];
		// unset($_SESSION['chkOtherActivity']);
	}
	
	if (isset($_SESSION['txtOtherActivity'])){
		$initOtherActivity = htmlspecialchars($_SESSION['txtOtherActivity']);
		// unset($_SESSION['txtOtherActivity']);
	}
	
	if (isset($_SESSION['chkUL'])){
		$initTagUL = $_SESSION['chkUL'];
		// unset($_SESSION['chkUL']);
	}
	
	if (isset($_SESSION['chkNone'])){
		$initTagNone = $_SESSION['chkNone'];
		// unset($_SESSION['chkNone']);
	}
	
	if (isset($_SESSION['chkPNS'])){
		$initTagPNS = $_SESSION['chkPNS'];
		// unset($_SESSION['chkPNS']);
	}
	
	if (isset($_SESSION['chkOtherReq'])){
		$initTagOtherRequirement = $_SESSION['chkOtherReq'];
		// unset($_SESSION['chkOtherReq']);
	}
	
	if (isset($_SESSION['txtOtherReq'])){
		$initOtherRequirement = htmlspecialchars($_SESSION['txtOtherReq']);
		// unset($_SESSION['txtOtherReq']);
	}

	######################### PRODUCT DEVELOPMENT

	$initTagCost = NULL;
	$initTagProcessabilityImprove = NULL;
	$initTagPropertyEnhance = NULL;
	$initTagLegalReq = NULL;
	$initTagOtherReason = NULL;
	$initOtherReason = NULL;
	
	if (isset($_SESSION['chkCost'])){
		$initTagCost = $_SESSION['chkCost'];
		// unset($_SESSION['chkCost']);
	}
	
	if (isset($_SESSION['chkProcessabilityImprove'])){
		$initTagProcessabilityImprove = $_SESSION['chkProcessabilityImprove'];
		// unset($_SESSION['chkProcessabilityImprove']);
	}
	
	if (isset($_SESSION['chkPropertyEnhance'])){
		$initTagPropertyEnhance = $_SESSION['chkPropertyEnhance'];
		// unset($_SESSION['chkPropertyEnhance']);
	}
	
	if (isset($_SESSION['chkLegalReq'])){
		$initTagLegalReq = $_SESSION['chkLegalReq'];
		// unset($_SESSION['chkLegalReq']);
	}
	
	if (isset($_SESSION['chkOthers'])){
		$initTagOtherReason = $_SESSION['chkOthers'];
		// unset($_SESSION['chkOthers']);
	}
	
	if (isset($_SESSION['txtOtherReason'])){
		$initOtherReason = htmlspecialchars($_SESSION['txtOtherReason']);
		// unset($_SESSION['txtOtherReason']);
	}

	######################### PRODUCT EVALUATION

	$initCompetitorsProduct = NULL;
	$initCompetitorsName = NULL;
	$initTagReplicate = NULL;
	
	if (isset($_SESSION['txtCompetitorsProduct'])){
		$initCompetitorsProduct = htmlspecialchars($_SESSION['txtCompetitorsProduct']);
		// unset($_SESSION['txtCompetitorsProduct']);
	}
	
	if (isset($_SESSION['txtCompetitorsName'])){
		$initCompetitorsName = htmlspecialchars($_SESSION['txtCompetitorsName']);
		// unset($_SESSION['txtCompetitorsName']);
	}
	
	if (isset($_SESSION['chkNeedsReplication'])){
		$initTagReplicate = $_SESSION['chkNeedsReplication'];
		// unset($_SESSION['chkNeedsReplication']);
	}

	######################### PRODUCT MODIFICATION AND EVALUATION

	$initFGItem = NULL;
	
	if (isset($_SESSION['sltFGItem'])){
		$initFGItem = $_SESSION['sltFGItem'];
		// unset($_SESSION['sltFGItem']);
	}

	######################### TEST EVALUATION WITHOUT REPLICATION (new_jrd_test_evaluation_noreplicate.php)

	$initJRDNumberID = NULL;
	
	if (isset($_SESSION['sltJRDNumber'])){
		$initJRDNumberID = $_SESSION['sltJRDNumber'];
		unset($_SESSION['sltJRDNumber']);
	}

	######################### (include/jrd_data_noreplicate_dropdown.php)

	$initTERNumber = NULL;
	$initFormulaTypeID = NULL;
	$initTagDynamicMilling = NULL;
	$initTagOilAging = NULL;
	$initTagOvenAging = NULL;
	$initTagStrandInspect = NULL;
	$initPelletInspect = NULL;
	$initTagImpactTest = NULL;
	$initTagStaticHeating = NULL;
	$initTagColorChange = NULL;
	$initTagWaterImmersion = NULL;
	$initRemarks = NULL;
	
	if (isset($_SESSION['txtTERNo'])){
		$initTERNumber = htmlspecialchars($_SESSION['txtTERNo']);
		// unset($_SESSION['txtTERNo']);
	}
	
	if (isset($_SESSION['sltFormulaType'])){
		$initFormulaTypeID = $_SESSION['sltFormulaType'];
		// unset($_SESSION['sltFormulaType']);
	}
	
	if (isset($_SESSION['chkDynamicMilling'])){
		$initTagDynamicMilling = $_SESSION['chkDynamicMilling'];
		// unset($_SESSION['chkDynamicMilling']);
	}
	
	if (isset($_SESSION['chkOilAging'])){
		$initTagOilAging = $_SESSION['chkOilAging'];
		// unset($_SESSION['chkOilAging']);
	}
	
	if (isset($_SESSION['chkOvenAging'])){
		$initTagOvenAging = $_SESSION['chkOvenAging'];
		// unset($_SESSION['chkOvenAging']);
	}
	
	if (isset($_SESSION['chkStrandInspect'])){
		$initTagStrandInspect = $_SESSION['chkStrandInspect'];
		// unset($_SESSION['chkStrandInspect']);
	}
	
	if (isset($_SESSION['chkPelletInspect'])){
		$initPelletInspect = $_SESSION['chkPelletInspect'];
		// unset($_SESSION['chkPelletInspect']);
	}
	
	if (isset($_SESSION['chkImpactTest'])){
		$initTagImpactTest = $_SESSION['chkImpactTest'];
		// unset($_SESSION['chkImpactTest']);
	}
	
	if (isset($_SESSION['chkStaticHeating'])){
		$initTagStaticHeating = $_SESSION['chkStaticHeating'];
		// unset($_SESSION['chkStaticHeating']);
	}
	
	if (isset($_SESSION['chkColorChange'])){
		$initTagColorChange = $_SESSION['chkColorChange'];
		// unset($_SESSION['chkColorChange']);
	}
	
	if (isset($_SESSION['chkWaterImmersion'])){
		$initTagWaterImmersion = $_SESSION['chkWaterImmersion'];
		// unset($_SESSION['chkWaterImmersion']);
	}
	
	if (isset($_SESSION['txtRemarks'])){
		$initRemarks = htmlspecialchars($_SESSION['txtRemarks']);
		// unset($_SESSION['txtRemarks']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_complete_testing.php)

	$initNRSpecificGravity = NULL;
	$initNRHardnessA = NULL;
	$initNRVolumeResistivity = NULL;
	$initNRHardnessD = NULL;
	$initNRUnagedTS = NULL;
	$initNROvenTS = NULL;
	$initNROilTS = NULL;
	$initNRUnagedE = NULL;
	$initNROvenE = NULL;
	$initNROilE = NULL;
	
	if (isset($_SESSION['txtSpecificGravity'])){
		$initNRSpecificGravity = htmlspecialchars($_SESSION['txtSpecificGravity']);
		unset($_SESSION['txtSpecificGravity']);
	}
	
	if (isset($_SESSION['txtHardnessA'])){
		$initNRHardnessA = htmlspecialchars($_SESSION['txtHardnessA']);
		unset($_SESSION['txtHardnessA']);
	}
	
	if (isset($_SESSION['txtVolumeResistivity'])){
		$initNRVolumeResistivity = htmlspecialchars($_SESSION['txtVolumeResistivity']);
		unset($_SESSION['txtVolumeResistivity']);
	}
	
	if (isset($_SESSION['txtHardnessD'])){
		$initNRHardnessD = htmlspecialchars($_SESSION['txtHardnessD']);
		unset($_SESSION['txtHardnessD']);
	}
	
	if (isset($_SESSION['txtUnagedTS'])){
		$initNRUnagedTS = htmlspecialchars($_SESSION['txtUnagedTS']);
		unset($_SESSION['txtUnagedTS']);
	}
	
	if (isset($_SESSION['txtOvenTS'])){
		$initNROvenTS = htmlspecialchars($_SESSION['txtOvenTS']);
		unset($_SESSION['txtOvenTS']);
	}
	
	if (isset($_SESSION['txtOilTS'])){
		$initNROilTS = htmlspecialchars($_SESSION['txtOilTS']);
		unset($_SESSION['txtOilTS']);
	}
	
	if (isset($_SESSION['txtUnagedE'])){
		$initNRUnagedE = htmlspecialchars($_SESSION['txtUnagedE']);
		unset($_SESSION['txtUnagedE']);
	}
	
	if (isset($_SESSION['txtOvenE'])){
		$initNROvenE = htmlspecialchars($_SESSION['txtOvenE']);
		unset($_SESSION['txtOvenE']);
	}
	
	if (isset($_SESSION['txtOilE'])){
		$initNROilE = htmlspecialchars($_SESSION['txtOilE']);
		unset($_SESSION['txtOilE']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_dynamic_milling.php)

	$initNRDMSampling = NULL;
	$initNRDMColor= "Good";
	$initNRDMHeat = "Good";
	$initNRDMProcess = "Good";

	if (isset($_SESSION['txtSampling'])){
		$initNRDMSampling = htmlspecialchars($_SESSION['txtSampling']);
		unset($_SESSION['txtSampling']);
	}

	if (isset($_SESSION['radDMColor'])){
		$initNRDMColor= $_SESSION['radDMColor'];
		unset($_SESSION['radDMColor']);
	}

	if (isset($_SESSION['radDMHeat'])){
		$initNRDMHeat = $_SESSION['radDMHeat'];
		unset($_SESSION['radDMHeat']);
	}

	if (isset($_SESSION['radDMProcess'])){
		$initNRDMProcess = $_SESSION['radDMProcess'];
		unset($_SESSION['radDMProcess']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_strand_inspection.php)

	$initNRSIFisheyeEvaluation = NULL;
	$initNRSIFisheyeClass = NULL;
	$initNRSIPinholeEvaluation = NULL;
	$initNRSIColorConform = NULL;
	$initNRSIPorous = NULL;
	$initNRSIFCPresent = NULL;
	$initNRSIOverallRemarks = NULL;

	if (isset($_SESSION['sltFisheyeEvaluation'])){
		$initNRSIFisheyeEvaluation = $_SESSION['sltFisheyeEvaluation'];
		unset($_SESSION['sltFisheyeEvaluation']);
	}

	if (isset($_SESSION['sltFisheyeClass'])){
		$initNRSIFisheyeClass = $_SESSION['sltFisheyeClass'];
		unset($_SESSION['sltFisheyeClass']);
	}

	if (isset($_SESSION['sltPinholeEvaluation'])){
		$initNRSIPinholeEvaluation = $_SESSION['sltPinholeEvaluation'];
		unset($_SESSION['sltPinholeEvaluation']);
	}

	if (isset($_SESSION['chkColorConform'])){
		$initNRSIColorConform = $_SESSION['chkColorConform'];
		unset($_SESSION['chkColorConform']);
	}

	if (isset($_SESSION['chkPorous'])){
		$initNRSIPorous = $_SESSION['chkPorous'];
		unset($_SESSION['chkPorous']);
	}

	if (isset($_SESSION['chkFCPresent'])){
		$initNRSIFCPresent = $_SESSION['chkFCPresent'];
		unset($_SESSION['chkFCPresent']);
	}

	if (isset($_SESSION['sltOverallRemarks'])){
		$initNRSIOverallRemarks = $_SESSION['sltOverallRemarks'];
		unset($_SESSION['sltOverallRemarks']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_pellet_inspection.php)

	$initNRPIClarityColorConform = NULL;

	if (isset($_SESSION['chkClarityColorConform'])){
		$initNRPIClarityColorConform = $_SESSION['chkClarityColorConform'];
		unset($_SESSION['chkClarityColorConform']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_impact_test.php)

	$initNRITDropWeight = NULL;
	$initNRITDropHeight = NULL;
	$initNRITSamplesNo = NULL;
	$initNRITPassedNo = NULL;

	if (isset($_SESSION['txtDropWeight'])){
		$initNRITDropWeight = htmlspecialchars($_SESSION['txtDropWeight']);
		unset($_SESSION['txtDropWeight']);
	}

	if (isset($_SESSION['txtDropHeight'])){
		$initNRITDropHeight = htmlspecialchars($_SESSION['txtDropHeight']);
		unset($_SESSION['txtDropHeight']);
	}

	if (isset($_SESSION['txtSamplesNo'])){
		$initNRITSamplesNo = htmlspecialchars($_SESSION['txtSamplesNo']);
		unset($_SESSION['txtSamplesNo']);
	}

	if (isset($_SESSION['txtPassedNo'])){
		$initNRITPassedNo = htmlspecialchars($_SESSION['txtPassedNo']);
		unset($_SESSION['txtPassedNo']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_static_heating.php)

	$initNRSHColor = "Good";
	$initNdSHHeat = "Good";

	if (isset($_SESSION['radSHColor'])){
		$initNRSHColor = $_SESSION['radSHColor'];
		unset($_SESSION['radSHColor']);
	}

	if (isset($_SESSION['radSHHeat'])){
		$initNdSHHeat = $_SESSION['radSHHeat'];
		unset($_SESSION['radSHHeat']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_color_change_test.php)

	$initNRCCTColor = "Good";
	$initNdCCTHeat = "Good";

	if (isset($_SESSION['radCCTColor'])){
		$initNRCCTColor = $_SESSION['radCCTColor'];
		unset($_SESSION['radCCTColor']);
	}

	if (isset($_SESSION['radCCTHeat'])){
		$initNdCCTHeat = $_SESSION['radCCTHeat'];
		unset($_SESSION['radCCTHeat']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_cold_inspection.php)

	$initNRCIResult1 = "P";
	$initNRCIResult2 = "P";
	$initNRCIResult3 = "P";
	$initNRCICracked1 = NULL;
	$initNRCICracked2 = NULL;
	$initNRCICracked3 = NULL;

	if (isset($_SESSION['NR_txtCIResult1'])){
		$initNRCIResult1 = htmlspecialchars($_SESSION['NR_txtCIResult1']);
		unset($_SESSION['NR_txtCIResult1']);
	}

	if (isset($_SESSION['NR_txtCIResult2'])){
		$initNRCIResult2 = htmlspecialchars($_SESSION['NR_txtCIResult2']);
		unset($_SESSION['NR_txtCIResult2']);
	}

	if (isset($_SESSION['NR_txtCIResult3'])){
		$initNRCIResult3 = htmlspecialchars($_SESSION['NR_txtCIResult3']);
		unset($_SESSION['NR_txtCIResult3']);
	}

	if (isset($_SESSION['NR_txtCICracked1'])){
		$initNRCICracked1 = htmlspecialchars($_SESSION['NR_txtCICracked1']);
		unset($_SESSION['NR_txtCICracked1']);
	}

	if (isset($_SESSION['NR_txtCICracked2'])){
		$initNRCICracked2 = htmlspecialchars($_SESSION['NR_txtCICracked2']);
		unset($_SESSION['NR_txtCICracked2']);
	}

	if (isset($_SESSION['NR_txtCICracked3'])){
		$initNRCICracked3 = htmlspecialchars($_SESSION['NR_txtCICracked3']);
		unset($_SESSION['NR_txtCICracked3']);
	}

	######################### JRD TESTING WITHOUT REPLICATION (include/jrd_noreplicate_water_immersion.php)

	$initNRWIImmersionStartDate = NULL;
	$initNRWIImmersionEndDate = NULL;
	$initNRWIDay0 = NULL;
	$initNRWIDay1 = NULL;
	$initNRWIDay2 = NULL;
	$initNRWIDay3 = NULL;
	$initNRWIDay4 = NULL;
	$initNRWIDay5 = NULL;
	$initNRWIDay6 = NULL;
	$initNRWIDay7 = NULL;
	$initNRWIDay8 = NULL;
	$initNRWIDay9 = NULL;
	$initNRWIDay10 = NULL;
	$initNRWIDay11 = NULL;
	$initNRWIDay12 = NULL;
	$initNRWIDay13 = NULL;
	$initNRWIDay14 = NULL;

	if (isset($_SESSION['txtImmersionStartDate'])){
		$initNRWIImmersionStartDate = htmlspecialchars($_SESSION['txtImmersionStartDate']);
		unset($_SESSION['txtImmersionStartDate']);
	}

	if (isset($_SESSION['txtImmersionEndDate'])){
		$initNRWIImmersionEndDate = htmlspecialchars($_SESSION['txtImmersionEndDate']);
		unset($_SESSION['txtImmersionEndDate']);
	}

	if (isset($_SESSION['txtDay0'])){
		$initNRWIDay0 = htmlspecialchars($_SESSION['txtDay0']);
		unset($_SESSION['txtDay0']);
	}

	if (isset($_SESSION['txtDay1'])){
		$initNRWIDay1 = htmlspecialchars($_SESSION['txtDay1']);
		unset($_SESSION['txtDay1']);
	}

	if (isset($_SESSION['txtDay2'])){
		$initNRWIDay2 = htmlspecialchars($_SESSION['txtDay2']);
		unset($_SESSION['txtDay2']);
	}

	if (isset($_SESSION['txtDay3'])){
		$initNRWIDay3 = htmlspecialchars($_SESSION['txtDay3']);
		unset($_SESSION['txtDay3']);
	}

	if (isset($_SESSION['txtDay4'])){
		$initNRWIDay4 = htmlspecialchars($_SESSION['txtDay4']);
		unset($_SESSION['txtDay4']);
	}

	if (isset($_SESSION['txtDay5'])){
		$initNRWIDay5 = htmlspecialchars($_SESSION['txtDay5']);
		unset($_SESSION['txtDay5']);
	}

	if (isset($_SESSION['txtDay6'])){
		$initNRWIDay6 = htmlspecialchars($_SESSION['txtDay6']);
		unset($_SESSION['txtDay6']);
	}

	if (isset($_SESSION['txtDay7'])){
		$initNRWIDay7 = htmlspecialchars($_SESSION['txtDay7']);
		unset($_SESSION['txtDay7']);
	}

	if (isset($_SESSION['txtDay8'])){
		$initNRWIDay8 = htmlspecialchars($_SESSION['txtDay8']);
		unset($_SESSION['txtDay8']);
	}

	if (isset($_SESSION['txtDay9'])){
		$initNRWIDay9 = htmlspecialchars($_SESSION['txtDay9']);
		unset($_SESSION['txtDay9']);
	}

	if (isset($_SESSION['txtDay10'])){
		$initNRWIDay10 = htmlspecialchars($_SESSION['txtDay10']);
		unset($_SESSION['txtDay10']);
	}

	if (isset($_SESSION['txtDay11'])){
		$initNRWIDay11 = htmlspecialchars($_SESSION['txtDay11']);
		unset($_SESSION['txtDay11']);
	}

	if (isset($_SESSION['txtDay12'])){
		$initNRWIDay12 = htmlspecialchars($_SESSION['txtDay12']);
		unset($_SESSION['txtDay12']);
	}

	if (isset($_SESSION['txtDay13'])){
		$initNRWIDay13 = htmlspecialchars($_SESSION['txtDay13']);
		unset($_SESSION['txtDay13']);
	}

	if (isset($_SESSION['txtDay14'])){
		$initNRWIDay14 = htmlspecialchars($_SESSION['txtDay14']);
		unset($_SESSION['txtDay14']);
	}

	######################### JRD CLIENT RESPONSE

	$initJRDID = NULL;
	$initRecommendation = NULL;
	$initTestObjective = NULL;
	$initIntroduction = NULL;
	$initMethodology = NULL;

	if (isset($_SESSION['sltJRD'])){
		$initJRDID = $_SESSION['sltJRD'];
		unset($_SESSION['sltJRD']);
	}

	if (isset($_SESSION['txtRecommendation'])){
		$initRecommendation = htmlspecialchars($_SESSION['txtRecommendation']);
		// unset($_SESSION['txtRecommendation']);
	}

	if (isset($_SESSION['txtTestObjective'])){
		$initTestObjective = htmlspecialchars($_SESSION['txtTestObjective']);
		// unset($_SESSION['txtTestObjective']);
	}

	if (isset($_SESSION['txtIntroduction'])){
		$initIntroduction = htmlspecialchars($_SESSION['txtIntroduction']);
		// unset($_SESSION['txtIntroduction']);
	}

	if (isset($_SESSION['txtMethodology'])){
		$initMethodology = htmlspecialchars($_SESSION['txtMethodology']);
		// unset($_SESSION['txtIntroduction']);
	}

?>