<?php

	######################### MOISTURE CONTENT #########################

	$initMCDate = NULL;
	$initMCMIRNo = NULL;
	$initMCRMItem = NULL;
	$initMCRMIRNo = NULL;
	$initMCPRMNo = NULL;
	$initMCRemarks = NULL;

	for ( $ii = 0; $ii < 10; $ii++ ) {
		$initMCLotNumber[$ii] = NULL;
		$initMCPercentMC[$ii] = NULL;
		$initMCBulkDensity[$ii] = NULL;
	}

	if (isset($_SESSION['SESS_MC_Date'])){
		$initMCDate = htmlspecialchars($_SESSION['SESS_MC_Date']);
		unset($_SESSION['SESS_MC_Date']);
	}
	if (isset($_SESSION['SESS_MC_MIRNo'])){
		$initMCMIRNo = $_SESSION['SESS_MC_MIRNo'];
		// unset($_SESSION['SESS_MC_MIRNo']);
	}
	if (isset($_SESSION['SESS_MC_RMItem'])){
		$initMCRMItem = $_SESSION['SESS_MC_RMItem'];
		// unset($_SESSION['SESS_MC_RMItem']);
	}
	if (isset($_SESSION['SESS_MC_RMIRNo'])){
		$initMCRMIRNo = htmlspecialchars($_SESSION['SESS_MC_RMIRNo']);
		unset($_SESSION['SESS_MC_RMIRNo']);
	}
	if (isset($_SESSION['SESS_MC_PRMNo'])){
		$initMCPRMNo = htmlspecialchars($_SESSION['SESS_MC_PRMNo']);
		unset($_SESSION['SESS_MC_PRMNo']);
	}
	if (isset($_SESSION['SESS_MC_Remarks'])){
		$initMCRemarks = htmlspecialchars($_SESSION['SESS_MC_Remarks']);
		unset($_SESSION['SESS_MC_Remarks']);
	}

	for ( $i = 0; $i < 10; $i++ ) {
		if ( isset( $_SESSION['SESS_MC_LotNumber'][$i] ) ){
			$initMCLotNumber[$i] = htmlspecialchars($_SESSION['SESS_MC_LotNumber'][$i]);
			unset($_SESSION['SESS_MC_LotNumber'][$i]);
		}
		if ( isset( $_SESSION['SESS_MC_PercentMC'][$i] ) ){
			$initMCPercentMC[$i] = htmlspecialchars($_SESSION['SESS_MC_PercentMC'][$i]);
			unset($_SESSION['SESS_MC_PercentMC'][$i]);
		}
		if ( isset( $_SESSION['SESS_MC_BulkDensity'][$i] ) ){
			$initMCBulkDensity[$i] = htmlspecialchars($_SESSION['SESS_MC_BulkDensity'][$i]);
			unset($_SESSION['SESS_MC_BulkDensity'][$i]);
		}
	}


?>