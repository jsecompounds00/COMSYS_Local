<?php

	// unset($_SESSION["page"]);
	// unset($_SESSION["search"]);
	// unset($_SESSION["qsone"]);

	######################### BATCH TICKET - NRM #########################
	unset($_SESSION["NRM_error_indicator"]);
	
	unset($_SESSION['NRM_sltActivity']);
	unset($_SESSION['NRM_sltNRMNumber']);
	unset($_SESSION['NRM_radSoft']);
	unset($_SESSION['NRM_txtBatchTicketDate']);
	unset($_SESSION['NRM_sltFG']);
	unset($_SESSION['NRM_txtTERNo']);
	unset($_SESSION['NRM_sltFormulaType']);
	unset($_SESSION['NRM_txtLotNo']);
	unset($_SESSION['NRM_txtTrialDate']);
	unset($_SESSION['NRM_txtExtDarNo']);
	unset($_SESSION['NRM_txtMixDarNo']);
	unset($_SESSION['NRM_txtExtZ1']);
	unset($_SESSION['NRM_txtMixParam1']);
	unset($_SESSION['NRM_txtMixSequence1']);
	unset($_SESSION['NRM_txtExtZ2']);
	unset($_SESSION['NRM_txtMixParam2']);
	unset($_SESSION['NRM_txtMixSequence2']);
	unset($_SESSION['NRM_txtExtDH']);
	unset($_SESSION['NRM_txtMixParam3']);
	unset($_SESSION['NRM_txtMixSequence3']);
	unset($_SESSION['NRM_txtExtScrewSpeed']);
	unset($_SESSION['NRM_txtMixParam4']);
	unset($_SESSION['NRM_tztMixSequence4']);
	unset($_SESSION['NRM_txtExtCutterSpeed']);
	unset($_SESSION['NRM_txtMixParam5']);
	unset($_SESSION['NRM_txtMixSequence5']);
	unset($_SESSION['NRM_txtMultiplier']);

	unset($_SESSION['NRM_chkDynamicMilling']);
	unset($_SESSION['NRM_chkOilAging']);
	unset($_SESSION['NRM_chkOvenAging']);
	unset($_SESSION['NRM_chkStrandInspect']);
	unset($_SESSION['NRM_chkPelletInspect']);
	unset($_SESSION['NRM_chkImpactTest']);
	unset($_SESSION['NRM_chkStaticHeating']);
	unset($_SESSION['NRM_chkColorChange']);
	unset($_SESSION['NRM_chkWaterImmersion']);
	unset($_SESSION['NRM_chkColdTesting']);

	unset($_SESSION['NRM_txtRemarks']);

	unset($_SESSION['NRM_txtNRm_id']);
	unset($_SESSION['NRM_txtNPHR']);
	unset($_SESSION['NRM_txtNQuantity']);
?>