<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### MATERIAL BALANCE #########################

		unset($_SESSION['SESS_MBC_IssuedFormula']);
		unset($_SESSION['SESS_MBC_Customer']);
		unset($_SESSION['SESS_MBC_PlannedShutdown']);
		unset($_SESSION['SESS_MBC_UnplannedShutdown']);
		unset($_SESSION['SESS_MBC_RequiredOutput']);
		unset($_SESSION['SESS_MBC_MasterBatch']);
		unset($_SESSION['SESS_MBC_MaterialIncorporation']);
		unset($_SESSION['SESS_MBC_LabSamples']);
		unset($_SESSION['SESS_MBC_UnplanLabSamples']);
		unset($_SESSION['SESS_MBC_OnholdPellet']);
		unset($_SESSION['SESS_MBC_OnholdPowder']);
		unset($_SESSION['SESS_MBC_ScreenChange']);
		unset($_SESSION['SESS_MBC_ChangeGrade']);
		unset($_SESSION['SESS_MBC_EndRun']);
		unset($_SESSION['SESS_MBC_FloorSweepingPellet']);
		unset($_SESSION['SESS_MBC_FloorSweepingPowder']);
		unset($_SESSION['SESS_MBC_ScrapedCooling']);
		unset($_SESSION['SESS_MBC_ScrapedMixer']);
		unset($_SESSION['SESS_MBC_StartUp']);
		unset($_SESSION['SESS_MBC_MixedPellets']);
		unset($_SESSION['SESS_MBC_UnplannedScrap']);
		unset($_SESSION['SESS_MBC_ReturnedBatch']);
		unset($_SESSION['SESS_MBC_Remarks']);
		unset($_SESSION['SESS_MBC_HeatUp']);
		unset($_SESSION['SESS_MBC_IdealSpeed']);
		unset($_SESSION['SESS_MBC_FromBagEPM']);
		unset($_SESSION['SESS_MBC_ThruBagEPM']);
		unset($_SESSION['SESS_MBC_UnderpackEPM']);
		unset($_SESSION['SESS_MBC_ReturnedBatchSmall']);
		unset($_SESSION['SESS_MBC_TOSAct']);
		unset($_SESSION['SESS_MBC_VarInOut']);
		unset($_SESSION['SESS_MBC_MaterialYield']);
		unset($_SESSION['SESS_MBC_RecoveryOnprocess']);
		unset($_SESSION['SESS_MBC_RecoveryLeftOver']);


		for ( $i = 0; $i < 10; $i++ ){
			unset($_SESSION['SESS_MBC_ProductionStart'][$i]);
			unset($_SESSION['SESS_MBC_ProductionEnd'][$i]);
			unset($_SESSION['SESS_MBC_TimeConsumed'][$i]);
			unset($_SESSION['SESS_MBC_FromBagEPM'][$i]);
			unset($_SESSION['SESS_MBC_ThruBagEPM'][$i]);
			unset($_SESSION['SESS_MBC_UnderpackEPM'][$i]);
		}

?>