<?php

	######################### BATCH TICKET - NRM #########################

	$session_error_indicator_NRM = 0;

	$initActivityID_NRM = NULL;
	$initNRMNumberID_NRM = NULL;
	$initTagSofPVC_NRM = NULL;
	$initBatchTicketDate_NRM = NULL;
	$initFGID_NRM = NULL;
	$initTERNumber_NRM = NULL;
	$initFormulaTypeID_NRM = NULL;
	$initLotNumber_NRM = NULL;
	$initTrialDate_NRM = NULL;
	$initExtDarNo_NRM = NULL;
	$initMixDarno_NRM = NULL;
	$initExtZ1_NRM = NULL;
	$initMixParam1_NRM = NULL;
	$initMixSequence1_NRM = NULL;
	$initExtZ2_NRM = NULL;
	$initMixParam2_NRM = NULL;
	$initMixSequence2_NRM = NULL;
	$initExtDH_NRM = NULL;
	$initMixParam3_NRM = NULL;
	$initMixSequence3_NRM = NULL;
	$initExtScrewSpeed_NRM = NULL;
	$initMixParam4_NRM = NULL;
	$initMixSequence4_NRM = NULL;
	$initExtCutterSpeed_NRM = NULL;
	$initMixParam5_NRM = NULL;
	$initMixSequence5_NRM = NULL;
	$initMultiplier_NRM = NULL;
	$initDynamicMilling_NRM = NULL;
	$initOilAging_NRM = NULL;
	$initOvenAging_NRM = NULL;
	$initStrandInspect_NRM = NULL;
	$initPelletInspect_NRM = NULL;
	$initImpactTest_NRM = NULL;
	$initStaticHeating_NRM = NULL;
	$initColorChange_NRM = NULL;
	$initWaterImmersion_NRM = NULL;
	$initColdTesting_NRM = NULL;
	$initRemarks_NRM = NULL;
	$initNRm_id_NRM = NULL;
	$initNPHR_NRM = NULL;
	$initNQuantity_NRM = NULL;
	
	if (isset($_SESSION['NRM_error_indicator'])){
		$session_error_indicator_NRM = $_SESSION['NRM_error_indicator'];
		unset($_SESSION['NRM_error_indicator']);
	}
	
	if (isset($_SESSION['NRM_sltActivity'])){
		$initActivityID_NRM = $_SESSION['NRM_sltActivity'];
		unset($_SESSION['NRM_sltActivity']);
	}
	
	if (isset($_SESSION['NRM_sltNRMNumber'])){
		$initNRMNumberID_NRM = $_SESSION['NRM_sltNRMNumber'];
		unset($_SESSION['NRM_sltNRMNumber']);
	}
	
	if (isset($_SESSION['NRM_radSoft'])){
		$initTagSofPVC_NRM = $_SESSION['NRM_radSoft'];
		unset($_SESSION['NRM_radSoft']);
	}
	
	if (isset($_SESSION['NRM_txtBatchTicketDate'])){
		$initBatchTicketDate_NRM = htmlspecialchars($_SESSION['NRM_txtBatchTicketDate']);
		unset($_SESSION['NRM_txtBatchTicketDate']);
	}
	
	if (isset($_SESSION['NRM_sltFG'])){
		$initFGID_NRM = $_SESSION['NRM_sltFG'];
		unset($_SESSION['NRM_sltFG']);
	}
	
	if (isset($_SESSION['NRM_txtTERNo'])){
		$initTERNumber_NRM = htmlspecialchars($_SESSION['NRM_txtTERNo']);
		unset($_SESSION['NRM_txtTERNo']);
	}
	
	if (isset($_SESSION['NRM_sltFormulaType'])){
		$initFormulaTypeID_NRM = $_SESSION['NRM_sltFormulaType'];
		unset($_SESSION['NRM_sltFormulaType']);
	}
	
	if (isset($_SESSION['NRM_txtLotNo'])){
		$initLotNumber_NRM = htmlspecialchars($_SESSION['NRM_txtLotNo']);
		unset($_SESSION['NRM_txtLotNo']);
	}
	
	if (isset($_SESSION['NRM_txtTrialDate'])){
		$initTrialDate_NRM = htmlspecialchars($_SESSION['NRM_txtTrialDate']);
		unset($_SESSION['NRM_txtTrialDate']);
	}
	
	if (isset($_SESSION['NRM_txtExtDarNo'])){
		$initExtDarNo_NRM = htmlspecialchars($_SESSION['NRM_txtExtDarNo']);
		unset($_SESSION['NRM_txtExtDarNo']);
	}
	
	if (isset($_SESSION['NRM_txtMixDarNo'])){
		$initMixDarno_NRM = htmlspecialchars($_SESSION['NRM_txtMixDarNo']);
		unset($_SESSION['NRM_txtMixDarNo']);
	}
	
	if (isset($_SESSION['NRM_txtExtZ1'])){
		$initExtZ1_NRM = htmlspecialchars($_SESSION['NRM_txtExtZ1']);
		unset($_SESSION['NRM_txtExtZ1']);
	}
	
	if (isset($_SESSION['NRM_txtMixParam1'])){
		$initMixParam1_NRM = htmlspecialchars($_SESSION['NRM_txtMixParam1']);
		unset($_SESSION['NRM_txtMixParam1']);
	}
	
	if (isset($_SESSION['NRM_txtMixSequence1'])){
		$initMixSequence1_NRM = htmlspecialchars($_SESSION['NRM_txtMixSequence1']);
		unset($_SESSION['NRM_txtMixSequence1']);
	}
	
	if (isset($_SESSION['NRM_txtExtZ2'])){
		$initExtZ2_NRM = htmlspecialchars($_SESSION['NRM_txtExtZ2']);
		unset($_SESSION['NRM_txtExtZ2']);
	}
	
	if (isset($_SESSION['NRM_txtMixParam2'])){
		$initMixParam2_NRM = htmlspecialchars($_SESSION['NRM_txtMixParam2']);
		unset($_SESSION['NRM_txtMixParam2']);
	}
	
	if (isset($_SESSION['NRM_txtMixSequence2'])){
		$initMixSequence2_NRM = htmlspecialchars($_SESSION['NRM_txtMixSequence2']);
		unset($_SESSION['NRM_txtMixSequence2']);
	}
	
	if (isset($_SESSION['NRM_txtExtDH'])){
		$initExtDH_NRM = htmlspecialchars($_SESSION['NRM_txtExtDH']);
		unset($_SESSION['NRM_txtExtDH']);
	}
	
	if (isset($_SESSION['NRM_txtMixParam3'])){
		$initMixParam3_NRM = htmlspecialchars($_SESSION['NRM_txtMixParam3']);
		unset($_SESSION['NRM_txtMixParam3']);
	}
	
	if (isset($_SESSION['NRM_txtMixSequence3'])){
		$initMixSequence3_NRM = htmlspecialchars($_SESSION['NRM_txtMixSequence3']);
		unset($_SESSION['NRM_txtMixSequence3']);
	}
	
	if (isset($_SESSION['NRM_txtExtScrewSpeed'])){
		$initExtScrewSpeed_NRM = htmlspecialchars($_SESSION['NRM_txtExtScrewSpeed']);
		unset($_SESSION['NRM_txtExtScrewSpeed']);
	}
	
	if (isset($_SESSION['NRM_txtMixParam4'])){
		$initMixParam4_NRM = htmlspecialchars($_SESSION['NRM_txtMixParam4']);
		unset($_SESSION['NRM_txtMixParam4']);
	}
	
	if (isset($_SESSION['NRM_tztMixSequence4'])){
		$initMixSequence4_NRM = htmlspecialchars($_SESSION['NRM_tztMixSequence4']);
		unset($_SESSION['NRM_tztMixSequence4']);
	}
	
	if (isset($_SESSION['NRM_txtExtCutterSpeed'])){
		$initExtCutterSpeed_NRM = htmlspecialchars($_SESSION['NRM_txtExtCutterSpeed']);
		unset($_SESSION['NRM_txtExtCutterSpeed']);
	}
	
	if (isset($_SESSION['NRM_txtMixParam5'])){
		$initMixParam5_NRM = htmlspecialchars($_SESSION['NRM_txtMixParam5']);
		unset($_SESSION['NRM_txtMixParam5']);
	}
	
	if (isset($_SESSION['NRM_txtMixSequence5'])){
		$initMixSequence5_NRM = htmlspecialchars($_SESSION['NRM_txtMixSequence5']);
		unset($_SESSION['NRM_txtMixSequence5']);
	}
	
	if (isset($_SESSION['NRM_txtMultiplier'])){
		$initMultiplier_NRM = htmlspecialchars($_SESSION['NRM_txtMultiplier']);
		unset($_SESSION['NRM_txtMultiplier']);
	}
	
	if (isset($_SESSION['NRM_chkDynamicMilling'])){
		$initDynamicMilling_NRM = $_SESSION['NRM_chkDynamicMilling'];
		unset($_SESSION['NRM_chkDynamicMilling']);
	}
	
	if (isset($_SESSION['NRM_chkOilAging'])){
		$initOilAging_NRM = $_SESSION['NRM_chkOilAging'];
		unset($_SESSION['NRM_chkOilAging']);
	}
	
	if (isset($_SESSION['NRM_chkOvenAging'])){
		$initOvenAging_NRM = $_SESSION['NRM_chkOvenAging'];
		unset($_SESSION['NRM_chkOvenAging']);
	}
	
	if (isset($_SESSION['NRM_chkStrandInspect'])){
		$initStrandInspect_NRM = $_SESSION['NRM_chkStrandInspect'];
		unset($_SESSION['NRM_chkStrandInspect']);
	}
	
	if (isset($_SESSION['NRM_chkPelletInspect'])){
		$initPelletInspect_NRM = $_SESSION['NRM_chkPelletInspect'];
		unset($_SESSION['NRM_chkPelletInspect']);
	}
	
	if (isset($_SESSION['NRM_chkImpactTest'])){
		$initImpactTest_NRM = $_SESSION['NRM_chkImpactTest'];
		unset($_SESSION['NRM_chkImpactTest']);
	}
	
	if (isset($_SESSION['NRM_chkStaticHeating'])){
		$initStaticHeating_NRM = $_SESSION['NRM_chkStaticHeating'];
		unset($_SESSION['NRM_chkStaticHeating']);
	}
	
	if (isset($_SESSION['NRM_chkColorChange'])){
		$initColorChange_NRM = $_SESSION['NRM_chkColorChange'];
		unset($_SESSION['NRM_chkColorChange']);
	}
	
	if (isset($_SESSION['NRM_chkWaterImmersion'])){
		$initWaterImmersion_NRM = $_SESSION['NRM_chkWaterImmersion'];
		unset($_SESSION['NRM_chkWaterImmersion']);
	}
	
	if (isset($_SESSION['NRM_chkColdTesting'])){
		$initColdTesting_NRM = $_SESSION['NRM_chkColdTesting'];
		unset($_SESSION['NRM_chkColdTesting']);
	}
	
	if (isset($_SESSION['NRM_txtRemarks'])){
		$initRemarks_NRM = htmlspecialchars($_SESSION['NRM_txtRemarks']);
		unset($_SESSION['NRM_txtRemarks']);
	}
	
	if (isset($_SESSION['NRM_txtNRm_id'])){
		$initNRm_id_NRM = htmlspecialchars($_SESSION['NRM_txtNRm_id']);
		// unset($_SESSION['NRM_txtNRm_id']);
	}
	
	if (isset($_SESSION['NRM_txtNPHR'])){
		$initNPHR_NRM = htmlspecialchars($_SESSION['NRM_txtNPHR']);
		// unset($_SESSION['NRM_txtNPHR']);
	}
	
	if (isset($_SESSION['NRM_txtNQuantity'])){
		$initNQuantity_NRM = htmlspecialchars($_SESSION['NRM_txtNQuantity']);
		unset($_SESSION['NRM_txtNQuantity']);
	}

?>