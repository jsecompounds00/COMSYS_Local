<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### MOISTURE CONTENT #########################
	unset($_SESSION['SESS_MC_Date']);
	unset($_SESSION['SESS_MC_MIRNo']);
	unset($_SESSION['SESS_MC_RMItem']);
	unset($_SESSION['SESS_MC_RMIRNo']);
	unset($_SESSION['SESS_MC_PRMNo']);
	unset($_SESSION['SESS_MC_Remarks']);

	for ( $i = 0; $i < 10; $i++ ) {
		unset($_SESSION['SESS_MC_LotNumber'][$i]);
		unset($_SESSION['SESS_MC_PercentMC'][$i]);
		unset($_SESSION['SESS_MC_BulkDensity'][$i]);
	}

?>