<?php

	######################### CPIAR VERIFICATION #########################

	$initCCVVerifyDate = NULL;
	$initCCVDivision = NULL;
	$initCCVDeptID = NULL;
	$initCCVAuditorID = NULL;
	$initCCVVerification = NULL;
	$initCCVStatus = NULL;
	$initCCVMonth = NULL;
	$initCCVYear = NULL;

	if (isset($_SESSION['SESS_CCV_VerifyDate'])){
		$initCCVVerifyDate = htmlspecialchars($_SESSION['SESS_CCV_VerifyDate']);
		unset($_SESSION['SESS_CCV_VerifyDate']);
	}
	if (isset($_SESSION['SESS_CCV_Division'])){
		$initCCVDivision = $_SESSION['SESS_CCV_Division'];
		unset($_SESSION['SESS_CCV_Division']);
	}
	if (isset($_SESSION['SESS_CCV_DeptID'])){
		$initCCVDeptID = $_SESSION['SESS_CCV_DeptID'];
		unset($_SESSION['SESS_CCV_DeptID']);
	}
	if (isset($_SESSION['SESS_CCV_AuditorID'])){
		$initCCVAuditorID = $_SESSION['SESS_CCV_AuditorID'];
		unset($_SESSION['SESS_CCV_AuditorID']);
	}
	if (isset($_SESSION['SESS_CCV_Verification'])){
		$initCCVVerification = htmlspecialchars($_SESSION['SESS_CCV_Verification']);
		unset($_SESSION['SESS_CCV_Verification']);
	}
	if (isset($_SESSION['SESS_CCV_Status'])){
		$initCCVStatus = $_SESSION['SESS_CCV_Status'];
		unset($_SESSION['SESS_CCV_Status']);
	}
	if (isset($_SESSION['SESS_CCV_Month'])){
		$initCCVMonth = $_SESSION['SESS_CCV_Month'];
		unset($_SESSION['SESS_CCV_Month']);
	}
	if (isset($_SESSION['SESS_CCV_Year'])){
		$initCCVYear = $_SESSION['SESS_CCV_Year'];
		unset($_SESSION['SESS_CCV_Year']);
	}


?>