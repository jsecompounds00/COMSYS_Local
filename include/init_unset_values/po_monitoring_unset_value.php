<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### PO MONITORING #########################

	unset($_SESSION['SESS_PO_PRE']);
	unset($_SESSION['SESS_PO_PONum']);
	unset($_SESSION['SESS_PO_DeliveryDate']);
	unset($_SESSION['SESS_PO_PODate']);
	unset($_SESSION['SESS_PO_Supplier']);
	unset($_SESSION['SESS_PO_Remarks']);
	unset($_SESSION['SESS_PO_ItemType']);
	unset($_SESSION['SESS_PO_Division']);

?>