<?php

	############## RAW MATERIAL TYPE ############## 

	$initRMTNewSupplyType = NULL;
	$initRMTCompounds = NULL;
	$initRMTPipes = NULL;
	$initRMTCorporate = NULL;
	$initRMTPPR = NULL;
	$initRMTRMDescription = NULL;

	if (isset($_SESSION['SESS_RMT_NewSupplyType'])){
		$initRMTNewSupplyType = htmlspecialchars($_SESSION['SESS_RMT_NewSupplyType']); 
		unset($_SESSION['SESS_RMT_NewSupplyType']);
	}
	if (isset($_SESSION['SESS_RMT_Compounds'])){
		$initRMTCompounds = $_SESSION['SESS_RMT_Compounds']; 
		unset($_SESSION['SESS_RMT_Compounds']);
	}
	if (isset($_SESSION['SESS_RMT_Pipes'])){
		$initRMTPipes = $_SESSION['SESS_RMT_Pipes']; 
		unset($_SESSION['SESS_RMT_Pipes']);
	}
	if (isset($_SESSION['SESS_RMT_Corporate'])){
		$initRMTCorporate = $_SESSION['SESS_RMT_Corporate']; 
		unset($_SESSION['SESS_RMT_Corporate']);
	}
	if (isset($_SESSION['SESS_RMT_PPR'])){
		$initRMTPPR = $_SESSION['SESS_RMT_PPR']; 
		unset($_SESSION['SESS_RMT_PPR']);
	}
	if (isset($_SESSION['SESS_RMT_RMDescription'])){
		$initRMTRMDescription = htmlspecialchars($_SESSION['SESS_RMT_RMDescription']); 
		unset($_SESSION['SESS_RMT_RMDescription']);
	}

?>