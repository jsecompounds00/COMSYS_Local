<?php

	######################### CREDIT PAYMENT #########################

	$initCJCPCustomer = NULL;
	$initCJCPCheckDate = NULL;
	$initCJCPCheckNo = NULL;
	$initCJCPBankCode = NULL;
	$initCJCPCheckAmount = NULL;
	$initCJCPCounter = 100;

	for ( $y = 0; $y <= $initCJCPCounter; $y++ ){
		$initCJCPAmount[$y] = NULL;
		$initCJCPDiscount[$y] = NULL;
	}

	if (isset($_SESSION['SESS_CJCP_Customer'])){
		$initCJCPCustomer = $_SESSION['SESS_CJCP_Customer'];
		unset($_SESSION['SESS_CJCP_Customer']);
	}

	if (isset($_SESSION['SESS_CJCP_CheckDate'])){
		$initCJCPCheckDate = htmlspecialchars($_SESSION['SESS_CJCP_CheckDate']);
		unset($_SESSION['SESS_CJCP_CheckDate']);
	}
	if (isset($_SESSION['SESS_CJCP_CheckNo'])){
		$initCJCPCheckNo = htmlspecialchars($_SESSION['SESS_CJCP_CheckNo']);
		unset($_SESSION['SESS_CJCP_CheckNo']);
	}
	if (isset($_SESSION['SESS_CJCP_BankCode'])){
		$initCJCPBankCode = $_SESSION['SESS_CJCP_BankCode'];
		unset($_SESSION['SESS_CJCP_BankCode']);
	}
	if (isset($_SESSION['SESS_CJCP_CheckAmount'])){
		$initCJCPCheckAmount = htmlspecialchars($_SESSION['SESS_CJCP_CheckAmount']);
		unset($_SESSION['SESS_CJCP_CheckAmount']);
	}
	if (isset($_SESSION['SESS_CJCP_Counter'])){
		$initCJCPCounter = $_SESSION['SESS_CJCP_Counter'];
		// unset($_SESSION['SESS_CJCP_Counter']);
	}

	for ( $x = 0; $x <= $initCJCPCounter; $x++ ){
		if (isset($_SESSION['SESS_CJCP_Amount'][$x])){
			$initCJCPAmount[$x] = htmlspecialchars($_SESSION['SESS_CJCP_Amount'][$x]);
			// unset($_SESSION['SESS_CJCP_Amount'][$x]);
		}
		if (isset($_SESSION['SESS_CJCP_Discount'][$x])){
			$initCJCPDiscount[$x] = htmlspecialchars($_SESSION['SESS_CJCP_Discount'][$x]);
			// unset($_SESSION['SESS_CJCP_Discount'][$x]);
		}
	}

?>