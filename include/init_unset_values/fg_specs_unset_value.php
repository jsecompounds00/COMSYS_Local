<?php

	unset($_SESSION['page']);
	unset($_SESSION['search']);
	unset($_SESSION['qsone']);
	
	############## FG CATEGORY ############## 

	unset($_SESSION['SESS_FGSPECS_FromSpecificGravity']);
	unset($_SESSION['SESS_FGSPECS_ToSpecificGravity']);
	unset($_SESSION['SESS_FGSPECS_FromHardnessA']);
	unset($_SESSION['SESS_FGSPECS_ToHardnessA']);
	unset($_SESSION['SESS_FGSPECS_FromHardnessD']);
	unset($_SESSION['SESS_FGSPECS_ToHardnessD']);
	unset($_SESSION['SESS_FGSPECS_VolumeResistivity']);
	unset($_SESSION['SESS_FGSPECS_UnagedTS']);
	unset($_SESSION['SESS_FGSPECS_OvenTS']);
	unset($_SESSION['SESS_FGSPECS_OilTS']);
	unset($_SESSION['SESS_FGSPECS_UnagedE']);
	unset($_SESSION['SESS_FGSPECS_OvenE']);
	unset($_SESSION['SESS_FGSPECS_OilE']);

?>