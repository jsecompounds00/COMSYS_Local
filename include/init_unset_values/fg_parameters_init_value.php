<?php

	############## FG CATEGORY ############## 

	$initFGCFGZ1 = NULL;
	$initFGCFGZ2 = NULL;
	$initFGCFGDH = NULL;
	$initFGCFGCS = NULL;
	$initFGCFGSS = NULL;

	if (isset($_SESSION['SESS_FGC_FGZ1'])){
		$initFGCFGZ1 = htmlspecialchars($_SESSION['SESS_FGC_FGZ1']); 
		unset($_SESSION['SESS_FGC_FGZ1']);
	}
	if (isset($_SESSION['SESS_FGC_FGZ2'])){
		$initFGCFGZ2 = htmlspecialchars($_SESSION['SESS_FGC_FGZ2']); 
		unset($_SESSION['SESS_FGC_FGZ2']);
	}
	if (isset($_SESSION['SESS_FGC_FGDH'])){
		$initFGCFGDH = htmlspecialchars($_SESSION['SESS_FGC_FGDH']); 
		unset($_SESSION['SESS_FGC_FGDH']);
	}
	if (isset($_SESSION['SESS_FGC_FGCS'])){
		$initFGCFGCS = htmlspecialchars($_SESSION['SESS_FGC_FGCS']); 
		unset($_SESSION['SESS_FGC_FGCS']);
	}
	if (isset($_SESSION['SESS_FGC_FGSS'])){
		$initFGCFGSS = htmlspecialchars($_SESSION['SESS_FGC_FGSS']); 
		unset($_SESSION['SESS_FGC_FGSS']);
	}

?>