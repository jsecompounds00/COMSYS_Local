<?php

	############## DEPARTMENTS ############## 

	$initDEPCode = NULL;
	$initDEPDepartment = NULL;
	$initDEPActive = NULL;
	$initDEPCmpds = NULL;
	$initDEPPips = NULL;
	$initDEPCorp = NULL;
	$initDEPPPR = NULL;
	$initDEPBudget = NULL;
	$initDEPAccountCode = NULL;

	if (isset($_SESSION['SESS_DEP_Code'])){
		$initDEPCode = htmlspecialchars($_SESSION['SESS_DEP_Code']); 
		unset($_SESSION['SESS_DEP_Code']);
	}
	if (isset($_SESSION['SESS_DEP_Department'])){
		$initDEPDepartment = htmlspecialchars($_SESSION['SESS_DEP_Department']); 
		unset($_SESSION['SESS_DEP_Department']);
	}
	if (isset($_SESSION['SESS_DEP_Active'])){
		$initDEPActive = $_SESSION['SESS_DEP_Active']; 
		unset($_SESSION['SESS_DEP_Active']);
	}
	if (isset($_SESSION['SESS_DEP_Cmpds'])){
		$initDEPCmpds = $_SESSION['SESS_DEP_Cmpds']; 
		unset($_SESSION['SESS_DEP_Cmpds']);
	}
	if (isset($_SESSION['SESS_DEP_Pips'])){
		$initDEPPips = $_SESSION['SESS_DEP_Pips']; 
		unset($_SESSION['SESS_DEP_Pips']);
	}
	if (isset($_SESSION['SESS_DEP_Corp'])){
		$initDEPCorp = $_SESSION['SESS_DEP_Corp']; 
		unset($_SESSION['SESS_DEP_Corp']);
	}
	if (isset($_SESSION['SESS_DEP_PPR'])){
		$initDEPPPR = $_SESSION['SESS_DEP_PPR']; 
		unset($_SESSION['SESS_DEP_PPR']);
	}
	if (isset($_SESSION['SESS_DEP_Budget'])){
		$initDEPBudget = $_SESSION['SESS_DEP_Budget']; 
		unset($_SESSION['SESS_DEP_Budget']);
	}
	if (isset($_SESSION['SESS_DEP_AcctCode'])){
		$initDEPAccountCode = $_SESSION['SESS_DEP_AcctCode']; 
		unset($_SESSION['SESS_DEP_AcctCode']);
	}

?>