<?php

	######################### BATCH TICKET TESTING #########################

	######################### include/complete_testing.php #########################

	$initSpecificGravity_BT = NULL;
	$initHardnessA_BT = NULL;
	$initVolumeResistivity_BT = NULL;
	$initHardnessD_BT = NULL;
	$initUnagedTS_BT = NULL;
	$initOvenTS_BT = NULL;
	$initOilTS_BT = NULL;
	$initUnagedE_BT = NULL;
	$initOvenE_BT = NULL;
	$initOilE_BT = NULL;

	if (isset($_SESSION['BT_txtSpecificGravity'])){
		$initSpecificGravity_BT = htmlspecialchars($_SESSION['BT_txtSpecificGravity']);
		unset($_SESSION['BT_txtSpecificGravity']);
	}

	if (isset($_SESSION['BT_txtHardnessA'])){
		$initHardnessA_BT = htmlspecialchars($_SESSION['BT_txtHardnessA']);
		unset($_SESSION['BT_txtHardnessA']);
	}

	if (isset($_SESSION['BT_txtVolumeResistivity'])){
		$initVolumeResistivity_BT = htmlspecialchars($_SESSION['BT_txtVolumeResistivity']);
		unset($_SESSION['BT_txtVolumeResistivity']);
	}

	if (isset($_SESSION['BT_txtHardnessD'])){
		$initHardnessD_BT = htmlspecialchars($_SESSION['BT_txtHardnessD']);
		unset($_SESSION['BT_txtHardnessD']);
	}

	if (isset($_SESSION['BT_txtUnagedTS'])){
		$initUnagedTS_BT = htmlspecialchars($_SESSION['BT_txtUnagedTS']);
		unset($_SESSION['BT_txtUnagedTS']);
	}

	if (isset($_SESSION['BT_txtOvenTS'])){
		$initOvenTS_BT = htmlspecialchars($_SESSION['BT_txtOvenTS']);
		unset($_SESSION['BT_txtOvenTS']);
	}

	if (isset($_SESSION['BT_txtOilTS'])){
		$initOilTS_BT = htmlspecialchars($_SESSION['BT_txtOilTS']);
		unset($_SESSION['BT_txtOilTS']);
	}

	if (isset($_SESSION['BT_txtUnagedE'])){
		$initUnagedE_BT = htmlspecialchars($_SESSION['BT_txtUnagedE']);
		unset($_SESSION['BT_txtUnagedE']);
	}

	if (isset($_SESSION['BT_txtOvenE'])){
		$initOvenE_BT = htmlspecialchars($_SESSION['BT_txtOvenE']);
		unset($_SESSION['BT_txtOvenE']);
	}

	if (isset($_SESSION['BT_txtOilE'])){
		$initOilE_BT = htmlspecialchars($_SESSION['BT_txtOilE']);
		unset($_SESSION['BT_txtOilE']);
	}

	######################### include/dynamic_milling.php #########################

	$initSampling_BT = NULL;
	$initDMColor_BT = "Good";
	$initDMHeat_BT = "Good";
	$initDMProcess_BT = "Good";

	if (isset($_SESSION['BT_txtSampling'])){
		$initSampling_BT = htmlspecialchars($_SESSION['BT_txtSampling']);
		unset($_SESSION['BT_txtSampling']);
	}

	if (isset($_SESSION['BT_radDMColor'])){
		$initDMColor_BT = $_SESSION['BT_radDMColor'];
		unset($_SESSION['BT_radDMColor']);
	}

	if (isset($_SESSION['BT_radDMHeat'])){
		$initDMHeat_BT = $_SESSION['BT_radDMHeat'];
		unset($_SESSION['BT_radDMHeat']);
	}

	if (isset($_SESSION['BT_radDMProcess'])){
		$initDMProcess_BT = $_SESSION['BT_radDMProcess'];
		unset($_SESSION['BT_radDMProcess']);
	}

	######################### include/strand_inspection.php #########################

	$initFisheyeEvaluation_BT = NULL;
	$initFisheyeClass_BT = NULL;
	$initOverallRemarks_BT = NULL;
	$initPinholeEvaluation_BT = NULL;
	$initColorConform_BT = NULL;
	$initPorous_BT = NULL;
	$initFCPresent_BT = NULL;

	if (isset($_SESSION['BT_sltFisheyeEvaluation'])){
		$initFisheyeEvaluation_BT = $_SESSION['BT_sltFisheyeEvaluation'];
		unset($_SESSION['BT_sltFisheyeEvaluation']);
	}

	if (isset($_SESSION['BT_sltFisheyeClass'])){
		$initFisheyeClass_BT = $_SESSION['BT_sltFisheyeClass'];
		unset($_SESSION['BT_sltFisheyeClass']);
	}

	if (isset($_SESSION['BT_sltOverallRemarks'])){
		$initOverallRemarks_BT = $_SESSION['BT_sltOverallRemarks'];
		unset($_SESSION['BT_sltOverallRemarks']);
	}

	if (isset($_SESSION['BT_sltPinholeEvaluation'])){
		$initPinholeEvaluation_BT = $_SESSION['BT_sltPinholeEvaluation'];
		unset($_SESSION['BT_sltPinholeEvaluation']);
	}

	if (isset($_SESSION['BT_chkColorConform'])){
		$initColorConform_BT = $_SESSION['BT_chkColorConform'];
		unset($_SESSION['BT_chkColorConform']);
	}

	if (isset($_SESSION['BT_chkPorous'])){
		$initPorous_BT = $_SESSION['BT_chkPorous'];
		unset($_SESSION['BT_chkPorous']);
	}

	if (isset($_SESSION['BT_chkFCPresent'])){
		$initFCPresent_BT = $_SESSION['BT_chkFCPresent'];
		unset($_SESSION['BT_chkFCPresent']);
	}

	######################### include/pellet_inspection.php #########################

	$initClarityColorConform_BT = NULL;

	if (isset($_SESSION['BT_chkClarityColorConform'])){
		$initClarityColorConform_BT = $_SESSION['BT_chkClarityColorConform'];
		unset($_SESSION['BT_chkClarityColorConform']);
	}

	######################### include/impact_test.php #########################

	$initDropWeight_BT = NULL;
	$initDropHeight_BT = NULL;
	$initSamplesNo_BT = NULL;
	$initPassedNo_BT = NULL;

	if (isset($_SESSION['BT_txtDropWeight'])){
		$initDropWeight_BT = htmlspecialchars($_SESSION['BT_txtDropWeight']);
		unset($_SESSION['BT_txtDropWeight']);
	}

	if (isset($_SESSION['BT_txtDropHeight'])){
		$initDropHeight_BT = htmlspecialchars($_SESSION['BT_txtDropHeight']);
		unset($_SESSION['BT_txtDropHeight']);
	}

	if (isset($_SESSION['BT_txtSamplesNo'])){
		$initSamplesNo_BT = htmlspecialchars($_SESSION['BT_txtSamplesNo']);
		unset($_SESSION['BT_txtSamplesNo']);
	}

	if (isset($_SESSION['BT_txtPassedNo'])){
		$initPassedNo_BT = htmlspecialchars($_SESSION['BT_txtPassedNo']);
		unset($_SESSION['BT_txtPassedNo']);
	}

	######################### include/pellet_inspection.php #########################

	$initSHColor_BT = "Good";
	$initSHHeat_BT = "Good";

	if (isset($_SESSION['BT_radSHColor'])){
		$initSHColor_BT = $_SESSION['BT_radSHColor'];
		unset($_SESSION['BT_radSHColor']);
	}

	if (isset($_SESSION['BT_radSHHeat'])){
		$initSHHeat_BT = $_SESSION['BT_radSHHeat'];
		unset($_SESSION['BT_radSHHeat']);
	}

	######################### include/color_change_test.php #########################

	$initCCTColor_BT = "Good";
	$initCCTHeat_BT = "Good";

	if (isset($_SESSION['BT_radCCTColor'])){
		$initCCTColor_BT = $_SESSION['BT_radCCTColor'];
		unset($_SESSION['BT_radCCTColor']);
	}

	if (isset($_SESSION['BT_radCCTHeat'])){
		$initCCTHeat_BT = $_SESSION['BT_radCCTHeat'];
		unset($_SESSION['BT_radCCTHeat']);
	}

	######################### include/water_immersion.php #########################

	$initImmersionStartDate_BT = NULL;
	$initImmersionEndDate_BT = NULL;
	$initDay0_BT = NULL;
	$initDay1_BT = NULL;
	$initDay2_BT = NULL;
	$initDay3_BT = NULL;
	$initDay4_BT = NULL;
	$initDay5_BT = NULL;
	$initDay6_BT = NULL;
	$initDay7_BT = NULL;
	$initDay8_BT = NULL;
	$initDay9_BT = NULL;
	$initDay10_BT = NULL;
	$initDay11_BT = NULL;
	$initDay12_BT = NULL;
	$initDay13_BT = NULL;
	$initDay14_BT = NULL;

	if (isset($_SESSION['BT_txtImmersionStartDate'])){
		$initImmersionStartDate_BT = htmlspecialchars($_SESSION['BT_txtImmersionStartDate']);
		unset($_SESSION['BT_txtImmersionStartDate']);
	}

	if (isset($_SESSION['BT_txtImmersionEndDate'])){
		$initImmersionEndDate_BT = htmlspecialchars($_SESSION['BT_txtImmersionEndDate']);
		unset($_SESSION['BT_txtImmersionEndDate']);
	}

	if (isset($_SESSION['BT_txtDay0'])){
		$initDay0_BT = htmlspecialchars($_SESSION['BT_txtDay0']);
		unset($_SESSION['BT_txtDay0']);
	}

	if (isset($_SESSION['BT_txtDay1'])){
		$initDay1_BT = htmlspecialchars($_SESSION['BT_txtDay1']);
		unset($_SESSION['BT_txtDay1']);
	}

	if (isset($_SESSION['BT_txtDay2'])){
		$initDay2_BT = htmlspecialchars($_SESSION['BT_txtDay2']);
		unset($_SESSION['BT_txtDay2']);
	}

	if (isset($_SESSION['BT_txtDay3'])){
		$initDay3_BT = htmlspecialchars($_SESSION['BT_txtDay3']);
		unset($_SESSION['BT_txtDay3']);
	}

	if (isset($_SESSION['BT_txtDay4'])){
		$initDay4_BT = htmlspecialchars($_SESSION['BT_txtDay4']);
		unset($_SESSION['BT_txtDay4']);
	}

	if (isset($_SESSION['BT_txtDay5'])){
		$initDay5_BT = htmlspecialchars($_SESSION['BT_txtDay5']);
		unset($_SESSION['BT_txtDay5']);
	}

	if (isset($_SESSION['BT_txtDay6'])){
		$initDay6_BT = htmlspecialchars($_SESSION['BT_txtDay6']);
		unset($_SESSION['BT_txtDay6']);
	}

	if (isset($_SESSION['BT_txtDay7'])){
		$initDay7_BT = htmlspecialchars($_SESSION['BT_txtDay7']);
		unset($_SESSION['BT_txtDay7']);
	}

	if (isset($_SESSION['BT_txtDay8'])){
		$initDay8_BT = htmlspecialchars($_SESSION['BT_txtDay8']);
		unset($_SESSION['BT_txtDay8']);
	}

	if (isset($_SESSION['BT_txtDay9'])){
		$initDay9_BT = htmlspecialchars($_SESSION['BT_txtDay9']);
		unset($_SESSION['BT_txtDay9']);
	}

	if (isset($_SESSION['BT_txtDay10'])){
		$initDay10_BT = htmlspecialchars($_SESSION['BT_txtDay10']);
		unset($_SESSION['BT_txtDay10']);
	}

	if (isset($_SESSION['BT_txtDay11'])){
		$initDay11_BT = htmlspecialchars($_SESSION['BT_txtDay11']);
		unset($_SESSION['BT_txtDay11']);
	}

	if (isset($_SESSION['BT_txtDay12'])){
		$initDay12_BT = htmlspecialchars($_SESSION['BT_txtDay12']);
		unset($_SESSION['BT_txtDay12']);
	}

	if (isset($_SESSION['BT_txtDay13'])){
		$initDay13_BT = htmlspecialchars($_SESSION['BT_txtDay13']);
		unset($_SESSION['BT_txtDay13']);
	}

	if (isset($_SESSION['BT_txtDay14'])){
		$initDay14_BT = htmlspecialchars($_SESSION['BT_txtDay14']);
		unset($_SESSION['BT_txtDay14']);
	}

	######################### include/cold_inspection.php #########################

	$initCIResult1_BT = "P";
	$initCIResult2_BT = "P";
	$initCIResult3_BT = "P";
	$initCICracked1_BT = NULL;
	$initCICracked2_BT = NULL;
	$initCICracked3_BT = NULL;

	if (isset($_SESSION['BT_txtCIResult1'])){
		$initCIResult1_BT = htmlspecialchars($_SESSION['BT_txtCIResult1']);
		unset($_SESSION['BT_txtCIResult1']);
	}

	if (isset($_SESSION['BT_txtCIResult2'])){
		$initCIResult2_BT = htmlspecialchars($_SESSION['BT_txtCIResult2']);
		unset($_SESSION['BT_txtCIResult2']);
	}

	if (isset($_SESSION['BT_txtCIResult3'])){
		$initCIResult3_BT = htmlspecialchars($_SESSION['BT_txtCIResult3']);
		unset($_SESSION['BT_txtCIResult3']);
	}

	if (isset($_SESSION['BT_txtCICracked1'])){
		$initCICracked1_BT = htmlspecialchars($_SESSION['BT_txtCICracked1']);
		unset($_SESSION['BT_txtCICracked1']);
	}

	if (isset($_SESSION['BT_txtCICracked2'])){
		$initCICracked2_BT = htmlspecialchars($_SESSION['BT_txtCICracked2']);
		unset($_SESSION['BT_txtCICracked2']);
	}

	if (isset($_SESSION['BT_txtCICracked3'])){
		$initCICracked3_BT = htmlspecialchars($_SESSION['BT_txtCICracked3']);
		unset($_SESSION['BT_txtCICracked3']);
	}


?>