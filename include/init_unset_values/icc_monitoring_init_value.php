<?php

	######################### ICC MONITORING #########################

	$initICMICCNo = NULL;
	$initICMICCDate = NULL;
	$initICMDueDate = NULL;
	$initICMIssueDeptID = NULL;
	$initICMReceiveDeptID = NULL;
	$initICMNatureOfComplaint = NULL;
	$initICMRootCause = NULL;
	$initICMContainmentAction = NULL;
	$initICMContainmentImplement = NULL;
	$initICMContainmentResponsible = NULL;
	$initICMCorrectiveAction = NULL;
	$initICMCorrectiveImplementation = NULL;
	$initICMCorrectiveResponsible = NULL;
	$initICMMemoDate = NULL;
	$initICMFinalDueDate = NULL;
	$initICMReceiveDateDCC = NULL;
	$initICMMonth = NULL;
	$initICMYear = NULL;
	$initICMRemarks = NULL;
	$initICMFromDivision = NULL;
	$initICMToDivision = NULL;
	$initICMMan = NULL;
	$initICMMethod = NULL;
	$initICMMachine = NULL;
	$initICMMaterial = NULL;
	$initICMMeasurement = NULL;
	$initICMMotherNature = NULL;

	if (isset($_SESSION['SESS_ICM_ICCNo'])){
		$initICMICCNo = htmlspecialchars($_SESSION['SESS_ICM_ICCNo']);
		unset($_SESSION['SESS_ICM_ICCNo']);
	}
	if (isset($_SESSION['SESS_ICM_ICCDate'])){
		$initICMICCDate = htmlspecialchars($_SESSION['SESS_ICM_ICCDate']);
		unset($_SESSION['SESS_ICM_ICCDate']);
	}
	if (isset($_SESSION['SESS_ICM_DueDate'])){
		$initICMDueDate = htmlspecialchars($_SESSION['SESS_ICM_DueDate']);
		unset($_SESSION['SESS_ICM_DueDate']);
	}
	if (isset($_SESSION['SESS_ICM_IssueDeptID'])){
		$initICMIssueDeptID = $_SESSION['SESS_ICM_IssueDeptID'];
		unset($_SESSION['SESS_ICM_IssueDeptID']);
	}
	if (isset($_SESSION['SESS_ICM_ReceiveDeptID'])){
		$initICMReceiveDeptID = $_SESSION['SESS_ICM_ReceiveDeptID'];
		unset($_SESSION['SESS_ICM_ReceiveDeptID']);
	}
	if (isset($_SESSION['SESS_ICM_NatureOfComplaint'])){
		$initICMNatureOfComplaint = htmlspecialchars($_SESSION['SESS_ICM_NatureOfComplaint']);
		unset($_SESSION['SESS_ICM_NatureOfComplaint']);
	}
	if (isset($_SESSION['SESS_ICM_RootCause'])){
		$initICMRootCause = htmlspecialchars($_SESSION['SESS_ICM_RootCause']);
		unset($_SESSION['SESS_ICM_RootCause']);
	}
	if (isset($_SESSION['SESS_ICM_ContainmentAction'])){
		$initICMContainmentAction = htmlspecialchars($_SESSION['SESS_ICM_ContainmentAction']);
		unset($_SESSION['SESS_ICM_ContainmentAction']);
	}
	if (isset($_SESSION['SESS_ICM_ContainmentImplement'])){
		$initICMContainmentImplement = htmlspecialchars($_SESSION['SESS_ICM_ContainmentImplement']);
		unset($_SESSION['SESS_ICM_ContainmentImplement']);
	}
	if (isset($_SESSION['SESS_ICM_ContainmentResponsible'])){
		$initICMContainmentResponsible = htmlspecialchars($_SESSION['SESS_ICM_ContainmentResponsible']);
		unset($_SESSION['SESS_ICM_ContainmentResponsible']);
	}
	if (isset($_SESSION['SESS_ICM_CorrectiveAction'])){
		$initICMCorrectiveAction = htmlspecialchars($_SESSION['SESS_ICM_CorrectiveAction']);
		unset($_SESSION['SESS_ICM_CorrectiveAction']);
	}
	if (isset($_SESSION['SESS_ICM_CorrectiveImplementation'])){
		$initICMCorrectiveImplementation = htmlspecialchars($_SESSION['SESS_ICM_CorrectiveImplementation']);
		unset($_SESSION['SESS_ICM_CorrectiveImplementation']);
	}
	if (isset($_SESSION['SESS_ICM_CorrectiveResponsible'])){
		$initICMCorrectiveResponsible = htmlspecialchars($_SESSION['SESS_ICM_CorrectiveResponsible']);
		unset($_SESSION['SESS_ICM_CorrectiveResponsible']);
	}
	if (isset($_SESSION['SESS_ICM_MemoDate'])){
		$initICMMemoDate = htmlspecialchars($_SESSION['SESS_ICM_MemoDate']);
		unset($_SESSION['SESS_ICM_MemoDate']);
	}
	if (isset($_SESSION['SESS_ICM_FinalDueDate'])){
		$initICMFinalDueDate = htmlspecialchars($_SESSION['SESS_ICM_FinalDueDate']);
		unset($_SESSION['SESS_ICM_FinalDueDate']);
	}
	if (isset($_SESSION['SESS_ICM_ReceiveDateDCC'])){
		$initICMReceiveDateDCC = htmlspecialchars($_SESSION['SESS_ICM_ReceiveDateDCC']);
		unset($_SESSION['SESS_ICM_ReceiveDateDCC']);
	}
	if (isset($_SESSION['SESS_ICM_Month'])){
		$initICMMonth = $_SESSION['SESS_ICM_Month'];
		unset($_SESSION['SESS_ICM_Month']);
	}
	if (isset($_SESSION['SESS_ICM_Year'])){
		$initICMYear = $_SESSION['SESS_ICM_Year'];
		unset($_SESSION['SESS_ICM_Year']);
	}
	if (isset($_SESSION['SESS_ICM_Remarks'])){
		$initICMRemarks = htmlspecialchars($_SESSION['SESS_ICM_Remarks']);
		unset($_SESSION['SESS_ICM_Remarks']);
	}
	if (isset($_SESSION['SESS_ICM_FromDivision'])){
		$initICMFromDivision = $_SESSION['SESS_ICM_FromDivision'];
		unset($_SESSION['SESS_ICM_FromDivision']);
	}
	if (isset($_SESSION['SESS_ICM_ToDivision'])){
		$initICMToDivision = $_SESSION['SESS_ICM_ToDivision'];
		unset($_SESSION['SESS_ICM_ToDivision']);
	}
	if (isset($_SESSION['SESS_ICM_Man'])){
		$initICMMan = $_SESSION['SESS_ICM_Man'];
		unset($_SESSION['SESS_ICM_Man']);
	}
	if (isset($_SESSION['SESS_ICM_Method'])){
		$initICMMethod = $_SESSION['SESS_ICM_Method'];
		unset($_SESSION['SESS_ICM_Method']);
	}
	if (isset($_SESSION['SESS_ICM_Machine'])){
		$initICMMachine = $_SESSION['SESS_ICM_Machine'];
		unset($_SESSION['SESS_ICM_Machine']);
	}
	if (isset($_SESSION['SESS_ICM_Material'])){
		$initICMMaterial = $_SESSION['SESS_ICM_Material'];
		unset($_SESSION['SESS_ICM_Material']);
	}
	if (isset($_SESSION['SESS_ICM_Measurement'])){
		$initICMMeasurement = $_SESSION['SESS_ICM_Measurement'];
		unset($_SESSION['SESS_ICM_Measurement']);
	}
	if (isset($_SESSION['SESS_ICM_MotherNature'])){
		$initICMMotherNature = $_SESSION['SESS_ICM_MotherNature'];
		unset($_SESSION['SESS_ICM_MotherNature']);
	}


?>