<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### CANVASS #########################

		unset($_SESSION['SESS_CANVASS_Date']);
		unset($_SESSION['SESS_CANVASS_Division']);
		unset($_SESSION['SESS_CANVASS_Remarks']);
		unset($_SESSION['SESS_CANVASS_PRE']);

		for ( $i = 0; $i < 4; $i++ ){
			unset($_SESSION['SESS_CANVASS_Supplier'][$i]);
			unset($_SESSION['SESS_CANVASS_Newsupplier'][$i]);
			unset($_SESSION['SESS_CANVASS_DeliveryTerms'][$i]);
			unset($_SESSION['SESS_CANVASS_PaymentDays'][$i]);
			unset($_SESSION['SESS_CANVASS_PaymentTerms'][$i]);
			unset($_SESSION['SESS_CANVASS_ContactPerson'][$i]);
			unset($_SESSION['SESS_CANVASS_TelephoneNum'][$i]);
			unset($_SESSION['SESS_CANVASS_FaxNum'][$i]);
		}

		if ( !isset($_SESSION["SESS_CANVASS_IncludeCount"]) ){
			$_SESSION["SESS_CANVASS_IncludeCount"] = 0;
		}

		for ( $sess_key = 0; $sess_key < $_SESSION["SESS_CANVASS_IncludeCount"]; $sess_key++ ) {

			unset($_SESSION["SESS_CANVASS_Include"][$sess_key]);

			for ( $yy = 0; $yy < 4; $yy++ ){

				unset($_SESSION["SESS_CANVASS_Availability"][$sess_key][$yy]);
				unset($_SESSION["SESS_CANVASS_CanvassDetails"][$sess_key][$yy]);

			}

		}

		unset($_SESSION["SESS_CANVASS_IncludeCount"]);
?>