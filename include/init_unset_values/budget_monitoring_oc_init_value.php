<?php

	##############  ############## 

	$initBMOBudgetYear = NULL;
	$initBMODepartment = NULL;
	$initBMOMiscType = NULL;
	$initBMOMiscellaneous = NULL;
	$initBMOUOM = NULL;
	$initBMOUnitPrice = NULL;
	$initBMOJAN = NULL;
	$initBMOFEB = NULL;
	$initBMOMAR = NULL;
	$initBMOAPR = NULL;
	$initBMOMAY = NULL;
	$initBMOJUN = NULL;
	$initBMOJUL = NULL;
	$initBMOAUG = NULL;
	$initBMOSEP = NULL;
	$initBMOOCT = NULL;
	$initBMONOV = NULL;
	$initBMODEC = NULL;
	$initBMOADD = NULL;
	$initBMORemarks = NULL;

	if (isset($_SESSION['SESS_BMO_BudgetYear'])){
		$initBMOBudgetYear = htmlspecialchars($_SESSION['SESS_BMO_BudgetYear']); 
		unset($_SESSION['SESS_BMO_BudgetYear']);
	}
	if (isset($_SESSION['SESS_BMO_Department'])){
		$initBMODepartment = htmlspecialchars($_SESSION['SESS_BMO_Department']); 
		unset($_SESSION['SESS_BMO_Department']);
	}
	if (isset($_SESSION['SESS_BMO_MiscType'])){
		$initBMOMiscType = $_SESSION['SESS_BMO_MiscType']; 
		unset($_SESSION['SESS_BMO_MiscType']);
	}
	if (isset($_SESSION['SESS_BMO_Miscellaneous'])){
		$initBMOMiscellaneous = $_SESSION['SESS_BMO_Miscellaneous']; 
		unset($_SESSION['SESS_BMO_Miscellaneous']);
	}
	if (isset($_SESSION['SESS_BMO_UOM'])){
		$initBMOUOM = $_SESSION['SESS_BMO_UOM']; 
		unset($_SESSION['SESS_BMO_UOM']);
	}
	if (isset($_SESSION['SESS_BMO_UnitPrice'])){
		$initBMOUnitPrice = $_SESSION['SESS_BMO_UnitPrice']; 
		unset($_SESSION['SESS_BMO_UnitPrice']);
	}
	if (isset($_SESSION['SESS_BMO_JAN'])){
		$initBMOJAN = $_SESSION['SESS_BMO_JAN']; 
		unset($_SESSION['SESS_BMO_JAN']);
	}
	if (isset($_SESSION['SESS_BMO_FEB'])){
		$initBMOFEB = $_SESSION['SESS_BMO_FEB']; 
		unset($_SESSION['SESS_BMO_FEB']);
	}
	if (isset($_SESSION['SESS_BMO_MAR'])){
		$initBMOMAR = $_SESSION['SESS_BMO_MAR']; 
		unset($_SESSION['SESS_BMO_MAR']);
	}
	if (isset($_SESSION['SESS_BMO_APR'])){
		$initBMOAPR = $_SESSION['SESS_BMO_APR']; 
		unset($_SESSION['SESS_BMO_APR']);
	}
	if (isset($_SESSION['SESS_BMO_MAY'])){
		$initBMOMAY = $_SESSION['SESS_BMO_MAY']; 
		unset($_SESSION['SESS_BMO_MAY']);
	}
	if (isset($_SESSION['SESS_BMO_JUN'])){
		$initBMOJUN = $_SESSION['SESS_BMO_JUN']; 
		unset($_SESSION['SESS_BMO_JUN']);
	}
	if (isset($_SESSION['SESS_BMO_JUL'])){
		$initBMOJUL = $_SESSION['SESS_BMO_JUL']; 
		unset($_SESSION['SESS_BMO_JUL']);
	}
	if (isset($_SESSION['SESS_BMO_AUG'])){
		$initBMOAUG = $_SESSION['SESS_BMO_AUG']; 
		unset($_SESSION['SESS_BMO_AUG']);
	}
	if (isset($_SESSION['SESS_BMO_SEP'])){
		$initBMOSEP = $_SESSION['SESS_BMO_SEP']; 
		unset($_SESSION['SESS_BMO_SEP']);
	}
	if (isset($_SESSION['SESS_BMO_OCT'])){
		$initBMOOCT = $_SESSION['SESS_BMO_OCT']; 
		unset($_SESSION['SESS_BMO_OCT']);
	}
	if (isset($_SESSION['SESS_BMO_NOV'])){
		$initBMONOV = $_SESSION['SESS_BMO_NOV']; 
		unset($_SESSION['SESS_BMO_NOV']);
	}
	if (isset($_SESSION['SESS_BMO_DEC'])){
		$initBMODEC = $_SESSION['SESS_BMO_DEC']; 
		unset($_SESSION['SESS_BMO_DEC']);
	}
	if (isset($_SESSION['SESS_BMO_ADD'])){
		$initBMOADD = $_SESSION['SESS_BMO_ADD']; 
		unset($_SESSION['SESS_BMO_ADD']);
	}
	if (isset($_SESSION['SESS_BMO_Remarks'])){
		$initBMORemarks = $_SESSION['SESS_BMO_Remarks']; 
		unset($_SESSION['SESS_BMO_Remarks']);
	}

?>