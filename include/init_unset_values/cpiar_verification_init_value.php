<?php

	######################### CPIAR VERIFICATION #########################

	$initCPVVerifyDate = NULL;
	$initCPVDivision = NULL;
	$initCPVDeptID = NULL;
	$initCPVAuditorID = NULL;
	$initCPVVerification = NULL;
	$initCPVStatus = NULL;
	$initCPVMonth = NULL;
	$initCPVYear = NULL;

	if (isset($_SESSION['SESS_CPV_VerifyDate'])){
		$initCPVVerifyDate = $_SESSION['SESS_CPV_VerifyDate'];
		unset($_SESSION['SESS_CPV_VerifyDate']);
	}
	if (isset($_SESSION['SESS_CPV_Division'])){
		$initCPVDivision = $_SESSION['SESS_CPV_Division'];
		unset($_SESSION['SESS_CPV_Division']);
	}
	if (isset($_SESSION['SESS_CPV_DeptID'])){
		$initCPVDeptID = $_SESSION['SESS_CPV_DeptID'];
		unset($_SESSION['SESS_CPV_DeptID']);
	}
	if (isset($_SESSION['SESS_CPV_AuditorID'])){
		$initCPVAuditorID = $_SESSION['SESS_CPV_AuditorID'];
		unset($_SESSION['SESS_CPV_AuditorID']);
	}
	if (isset($_SESSION['SESS_CPV_Verification'])){
		$initCPVVerification = $_SESSION['SESS_CPV_Verification'];
		unset($_SESSION['SESS_CPV_Verification']);
	}
	if (isset($_SESSION['SESS_CPV_Status'])){
		$initCPVStatus = $_SESSION['SESS_CPV_Status'];
		unset($_SESSION['SESS_CPV_Status']);
	}
	if (isset($_SESSION['SESS_CPV_Month'])){
		$initCPVMonth = $_SESSION['SESS_CPV_Month'];
		unset($_SESSION['SESS_CPV_Month']);
	}
	if (isset($_SESSION['SESS_CPV_Year'])){
		$initCPVYear = $_SESSION['SESS_CPV_Year'];
		unset($_SESSION['SESS_CPV_Year']);
	}


?>