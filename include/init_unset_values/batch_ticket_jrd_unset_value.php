<?php

	// unset($_SESSION["page"]);
	// unset($_SESSION["search"]);
	// unset($_SESSION["qsone"]);

	######################### BATCH TICKET - NRM #########################
	unset($_SESSION["JRD_error_indicator"]);
	
	unset($_SESSION['JRD_sltJRDNumber']);
	unset($_SESSION['JRD_txtTERNo']);
	unset($_SESSION['JRD_txtBatchTicketDate']);	
	unset($_SESSION['JRD_radFGType']);
	unset($_SESSION['JRD_txtNewFGName']);
	unset($_SESSION['JRD_sltFormulaType']);

	unset($_SESSION['JRD_txtLotNo']);
	unset($_SESSION['JRD_txtTrialDate']);
	unset($_SESSION['JRD_txtExtDarNo']);
	unset($_SESSION['JRD_txtExtZ1']);
	unset($_SESSION['JRD_txtExtZ2']);
	unset($_SESSION['JRD_txtExtDH']);
	unset($_SESSION['JRD_txtExtScrewSpeed']);
	unset($_SESSION['JRD_txtExtCutterSpeed']);
	unset($_SESSION['JRD_txtMixDarNo']);
	unset($_SESSION['JRD_txtMixParam1']);
	unset($_SESSION['JRD_txtMixSequence1']);
	unset($_SESSION['JRD_txtMixParam2']);
	unset($_SESSION['JRD_txtMixSequence2']);
	unset($_SESSION['JRD_txtMixParam3']);
	unset($_SESSION['JRD_txtMixSequence3']);
	unset($_SESSION['JRD_txtMixParam4']);
	unset($_SESSION['JRD_txtMixSequence4']);
	unset($_SESSION['JRD_txtMixParam5']);
	unset($_SESSION['JRD_txtMixSequence5']);
	unset($_SESSION['JRD_txtMultiplier']);

	unset($_SESSION['JRD_chkDynamicMilling']);
	unset($_SESSION['JRD_chkOilAging']);
	unset($_SESSION['JRD_chkOvenAging']);
	unset($_SESSION['JRD_chkStrandInspect']);
	unset($_SESSION['JRD_chkPelletInspect']);
	unset($_SESSION['JRD_chkImpactTest']);
	unset($_SESSION['JRD_chkStaticHeating']);
	unset($_SESSION['JRD_chkColorChange']);
	unset($_SESSION['JRD_chkWaterImmersion']);
	unset($_SESSION['JRD_chkColdTesting']);
?>