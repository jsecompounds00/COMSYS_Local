<?php

	############## ROLE ############## 

	$initRLEName = NULL;
	$initRLECompounds = NULL;
	$initRLEPipes = NULL;
	$initRLECorporate = NULL;
	$initRLEPPR = NULL;

	if (isset($_SESSION['SESS_RLE_Name'])){
		$initRLEName = htmlspecialchars($_SESSION['SESS_RLE_Name']); 
		unset($_SESSION['SESS_RLE_Name']);
	}
	if (isset($_SESSION['SESS_RLE_Compounds'])){
		$initRLECompounds = $_SESSION['SESS_RLE_Compounds']; 
		unset($_SESSION['SESS_RLE_Compounds']);
	}
	if (isset($_SESSION['SESS_RLE_Pipes'])){
		$initRLEPipes = $_SESSION['SESS_RLE_Pipes']; 
		unset($_SESSION['SESS_RLE_Pipes']);
	}
	if (isset($_SESSION['SESS_RLE_Corporate'])){
		$initRLECorporate = $_SESSION['SESS_RLE_Corporate']; 
		unset($_SESSION['SESS_RLE_Corporate']);
	}
	if (isset($_SESSION['SESS_RLE_PPR'])){
		$initRLEPPR = $_SESSION['SESS_RLE_PPR']; 
		unset($_SESSION['SESS_RLE_PPR']);
	}
?>