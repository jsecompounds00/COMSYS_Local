<?php

	######################### ICC VERIFICATION #########################

	$initICVVerifyDate = NULL;
	$initICVVerification = NULL;
	$initICVStatus = NULL;
	$initICVMonth = NULL;
	$initICVYear = NULL;
	$initICVDivision = NULL;

	if (isset($_SESSION['SESS_ICV_VerifyDate'])){
		$initICVVerifyDate = htmlspecialchars($_SESSION['SESS_ICV_VerifyDate']);
		unset($_SESSION['SESS_ICV_VerifyDate']);
	}
	if (isset($_SESSION['SESS_ICV_Verification'])){
		$initICVVerification = htmlspecialchars($_SESSION['SESS_ICV_Verification']);
		unset($_SESSION['SESS_ICV_Verification']);
	}
	if (isset($_SESSION['SESS_ICV_Status'])){
		$initICVStatus = $_SESSION['SESS_ICV_Status'];
		unset($_SESSION['SESS_ICV_Status']);
	}
	if (isset($_SESSION['SESS_ICV_Month'])){
		$initICVMonth = $_SESSION['SESS_ICV_Month'];
		unset($_SESSION['SESS_ICV_Month']);
	}
	if (isset($_SESSION['SESS_ICV_Year'])){
		$initICVYear = $_SESSION['SESS_ICV_Year'];
		unset($_SESSION['SESS_ICV_Year']);
	}
	if (isset($_SESSION['SESS_ICV_Division'])){
		$initICVDivision = $_SESSION['SESS_ICV_Division'];
		unset($_SESSION['SESS_ICV_Division']);
	}


?>