<?php

	######################### PO MONITORING #########################

	$initPOMCOUNT = NULL;
	$initPOMPREIds = NULL;
	$initPOMPONum = NULL;
	$initPOMDeliveryDate = NULL;
	$initPOMPODate = NULL;
	$initPOMSupplier = NULL;
	$initPOMRemarks = NULL;
	$initPOMItemType = NULL;
	$initPOMDivision = NULL;

	if (isset($_SESSION['SESS_PO_COUNT'])){
		$initPOMCOUNT = $_SESSION['SESS_PO_COUNT'];
		unset($_SESSION['SESS_PO_COUNT']);
	}

	if (isset($_SESSION['SESS_PO_PRE'])){
		$initPOMPREIds = $_SESSION['SESS_PO_PRE'];
		unset($_SESSION['SESS_PO_PRE']);
	}
	if (isset($_SESSION['SESS_PO_PONum'])){
		$initPOMPONum = htmlspecialchars($_SESSION['SESS_PO_PONum']);
		unset($_SESSION['SESS_PO_PONum']);
	}
	if (isset($_SESSION['SESS_PO_DeliveryDate'])){
		$initPOMDeliveryDate = htmlspecialchars($_SESSION['SESS_PO_DeliveryDate']);
		unset($_SESSION['SESS_PO_DeliveryDate']);
	}
	if (isset($_SESSION['SESS_PO_PODate'])){
		$initPOMPODate = htmlspecialchars($_SESSION['SESS_PO_PODate']);
		unset($_SESSION['SESS_PO_PODate']);
	}
	if (isset($_SESSION['SESS_PO_Supplier'])){
		$initPOMSupplier = $_SESSION['SESS_PO_Supplier'];
		unset($_SESSION['SESS_PO_Supplier']);
	}
	if (isset($_SESSION['SESS_PO_Remarks'])){
		$initPOMRemarks = htmlspecialchars($_SESSION['SESS_PO_Remarks']);
		unset($_SESSION['SESS_PO_Remarks']);
	}
	if (isset($_SESSION['SESS_PO_ItemType'])){
		$initPOMItemType = $_SESSION['SESS_PO_ItemType'];
		unset($_SESSION['SESS_PO_ItemType']);
	}
	if (isset($_SESSION['SESS_PO_Division'])){
		$initPOMDivision = $_SESSION['SESS_PO_Division'];
		unset($_SESSION['SESS_PO_Division']);
	}

?>