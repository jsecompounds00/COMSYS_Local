<?php

	######################### JRD #########################

	$initMBCIssuedFormula = NULL;
	$initMBCCustomer = NULL;
	$initMBCPlannedShutdown = NULL;
	$initMBCUnplannedShutdown = NULL;
	$initMBCRequiredOutput = NULL;
	$initMBCMasterBatch = NULL;
	$initMBCMaterialIncorporation = NULL;
	$initMBCLabSamples = NULL;
	$initMBCUnplanLabSamples = NULL;
	$initMBCOnholdPellet = NULL;
	$initMBCOnholdPowder = NULL;
	$initMBCScreenChange = NULL;
	$initMBCChangeGrade = NULL;
	$initMBCEndRun = NULL;
	$initMBCFloorSweepingPellet = NULL;
	$initMBCFloorSweepingPowder = NULL;
	$initMBCScrapedCooling = NULL;
	$initMBCScrapedMixer = NULL;
	$initMBCStartUp = NULL;
	$initMBCMixedPellets = NULL;
	$initMBCUnplannedScrap = NULL;
	$initMBCReturnedBatch = NULL;
	$initMBCRemarks = NULL;
	$initMBCHeatUp = NULL;
	$initMBCIdealSpeed = NULL;
	$initMBCFromBagEPM = NULL;
	$initMBCThruBagEPM = NULL;
	$initMBCUnderpackEPM = NULL;
	$initMBCReturnedBatchSmall = NULL;
	$initMBCTOSAct = NULL;
	$initMBCVarInOut = NULL;
	$initMBCMaterialYield = NULL;
	$initMBCRecoveryOnprocess = NULL;
	$initMBCRecoveryLeftOver = NULL;

	for( $x = 0; $x < 10; $x++ ){
		$initMBCProductionStart[$x] = NULL;
		$initMBCProductionEnd[$x] = NULL;
		$initMBCTimeConsumed[$x] = NULL;
		$initMBCFromBagEPM[$x] = NULL;
		$initMBCThruBagEPM[$x] = NULL;
		$initMBCUnderpackEPM[$x] = NULL;
	}
	
	if (isset($_SESSION['SESS_MBC_IssuedFormula'])){
		$initMBCIssuedFormula = $_SESSION['SESS_MBC_IssuedFormula'];
		unset($_SESSION['SESS_MBC_IssuedFormula']);
	}
	
	if (isset($_SESSION['SESS_MBC_Customer'])){
		$initMBCCustomer = $_SESSION['SESS_MBC_Customer'];
		unset($_SESSION['SESS_MBC_Customer']);
	}
	
	if (isset($_SESSION['SESS_MBC_PlannedShutdown'])){
		$initMBCPlannedShutdown = $_SESSION['SESS_MBC_PlannedShutdown'];
		unset($_SESSION['SESS_MBC_PlannedShutdown']);
	}
	
	if (isset($_SESSION['SESS_MBC_UnplannedShutdown'])){
		$initMBCUnplannedShutdown = $_SESSION['SESS_MBC_UnplannedShutdown'];
		unset($_SESSION['SESS_MBC_UnplannedShutdown']);
	}
	
	if (isset($_SESSION['SESS_MBC_RequiredOutput'])){
		$initMBCRequiredOutput = $_SESSION['SESS_MBC_RequiredOutput'];
		unset($_SESSION['SESS_MBC_RequiredOutput']);
	}
	
	if (isset($_SESSION['SESS_MBC_MasterBatch'])){
		$initMBCMasterBatch = $_SESSION['SESS_MBC_MasterBatch'];
		unset($_SESSION['SESS_MBC_MasterBatch']);
	}
	
	if (isset($_SESSION['SESS_MBC_MaterialIncorporation'])){
		$initMBCMaterialIncorporation = $_SESSION['SESS_MBC_MaterialIncorporation'];
		unset($_SESSION['SESS_MBC_MaterialIncorporation']);
	}
	
	if (isset($_SESSION['SESS_MBC_LabSamples'])){
		$initMBCLabSamples = $_SESSION['SESS_MBC_LabSamples'];
		unset($_SESSION['SESS_MBC_LabSamples']);
	}
	
	if (isset($_SESSION['SESS_MBC_UnplanLabSamples'])){
		$initMBCUnplanLabSamples = $_SESSION['SESS_MBC_UnplanLabSamples'];
		unset($_SESSION['SESS_MBC_UnplanLabSamples']);
	}
	
	if (isset($_SESSION['SESS_MBC_OnholdPellet'])){
		$initMBCOnholdPellet = $_SESSION['SESS_MBC_OnholdPellet'];
		unset($_SESSION['SESS_MBC_OnholdPellet']);
	}
	
	if (isset($_SESSION['SESS_MBC_OnholdPowder'])){
		$initMBCOnholdPowder = $_SESSION['SESS_MBC_OnholdPowder'];
		unset($_SESSION['SESS_MBC_OnholdPowder']);
	}
	
	if (isset($_SESSION['SESS_MBC_ScreenChange'])){
		$initMBCScreenChange = $_SESSION['SESS_MBC_ScreenChange'];
		unset($_SESSION['SESS_MBC_ScreenChange']);
	}
	
	if (isset($_SESSION['SESS_MBC_ChangeGrade'])){
		$initMBCChangeGrade = $_SESSION['SESS_MBC_ChangeGrade'];
		unset($_SESSION['SESS_MBC_ChangeGrade']);
	}
	
	if (isset($_SESSION['SESS_MBC_EndRun'])){
		$initMBCEndRun = $_SESSION['SESS_MBC_EndRun'];
		unset($_SESSION['SESS_MBC_EndRun']);
	}
	
	if (isset($_SESSION['SESS_MBC_FloorSweepingPellet'])){
		$initMBCFloorSweepingPellet = $_SESSION['SESS_MBC_FloorSweepingPellet'];
		unset($_SESSION['SESS_MBC_FloorSweepingPellet']);
	}
	
	if (isset($_SESSION['SESS_MBC_FloorSweepingPowder'])){
		$initMBCFloorSweepingPowder = $_SESSION['SESS_MBC_FloorSweepingPowder'];
		unset($_SESSION['SESS_MBC_FloorSweepingPowder']);
	}
	
	if (isset($_SESSION['SESS_MBC_ScrapedCooling'])){
		$initMBCScrapedCooling = $_SESSION['SESS_MBC_ScrapedCooling'];
		unset($_SESSION['SESS_MBC_ScrapedCooling']);
	}
	
	if (isset($_SESSION['SESS_MBC_ScrapedMixer'])){
		$initMBCScrapedMixer = $_SESSION['SESS_MBC_ScrapedMixer'];
		unset($_SESSION['SESS_MBC_ScrapedMixer']);
	}
	
	if (isset($_SESSION['SESS_MBC_StartUp'])){
		$initMBCStartUp = $_SESSION['SESS_MBC_StartUp'];
		unset($_SESSION['SESS_MBC_StartUp']);
	}
	
	if (isset($_SESSION['SESS_MBC_MixedPellets'])){
		$initMBCMixedPellets = $_SESSION['SESS_MBC_MixedPellets'];
		unset($_SESSION['SESS_MBC_MixedPellets']);
	}
	
	if (isset($_SESSION['SESS_MBC_UnplannedScrap'])){
		$initMBCUnplannedScrap = $_SESSION['SESS_MBC_UnplannedScrap'];
		unset($_SESSION['SESS_MBC_UnplannedScrap']);
	}

	if (isset($_SESSION['SESS_MBC_ReturnedBatch'])){
		$initMBCReturnedBatch = $_SESSION['SESS_MBC_ReturnedBatch'];
		unset($_SESSION['SESS_MBC_ReturnedBatch']);
	}

	if (isset($_SESSION['SESS_MBC_Remarks'])){
		$initMBCRemarks = $_SESSION['SESS_MBC_Remarks'];
		unset($_SESSION['SESS_MBC_Remarks']);
	}

	if (isset($_SESSION['SESS_MBC_HeatUp'])){
		$initMBCHeatUp = $_SESSION['SESS_MBC_HeatUp'];
		unset($_SESSION['SESS_MBC_HeatUp']);
	}

	if (isset($_SESSION['SESS_MBC_IdealSpeed'])){
		$initMBCIdealSpeed = $_SESSION['SESS_MBC_IdealSpeed'];
		unset($_SESSION['SESS_MBC_IdealSpeed']);
	}

	if (isset($_SESSION['SESS_MBC_FromBagEPM'])){
		$initMBCFromBagEPM = $_SESSION['SESS_MBC_FromBagEPM'];
		unset($_SESSION['SESS_MBC_FromBagEPM']);
	}

	if (isset($_SESSION['SESS_MBC_ThruBagEPM'])){
		$initMBCThruBagEPM = $_SESSION['SESS_MBC_ThruBagEPM'];
		unset($_SESSION['SESS_MBC_ThruBagEPM']);
	}

	if (isset($_SESSION['SESS_MBC_UnderpackEPM'])){
		$initMBCUnderpackEPM = $_SESSION['SESS_MBC_UnderpackEPM'];
		unset($_SESSION['SESS_MBC_UnderpackEPM']);
	}

	if (isset($_SESSION['SESS_MBC_ReturnedBatchSmall'])){
		$initMBCReturnedBatchSmall = $_SESSION['SESS_MBC_ReturnedBatchSmall'];
		unset($_SESSION['SESS_MBC_ReturnedBatchSmall']);
	}

	if (isset($_SESSION['SESS_MBC_TOSAct'])){
		$initMBCTOSAct = $_SESSION['SESS_MBC_TOSAct'];
		unset($_SESSION['SESS_MBC_TOSAct']);
	}

	if (isset($_SESSION['SESS_MBC_VarInOut'])){
		$initMBCVarInOut = $_SESSION['SESS_MBC_VarInOut'];
		unset($_SESSION['SESS_MBC_VarInOut']);
	}

	if (isset($_SESSION['SESS_MBC_MaterialYield'])){
		$initMBCMaterialYield = $_SESSION['SESS_MBC_MaterialYield'];
		unset($_SESSION['SESS_MBC_MaterialYield']);
	}

	if (isset($_SESSION['SESS_MBC_RecoveryOnprocess'])){
		$initMBCRecoveryOnprocess = $_SESSION['SESS_MBC_RecoveryOnprocess'];
		unset($_SESSION['SESS_MBC_RecoveryOnprocess']);
	}

	if (isset($_SESSION['SESS_MBC_RecoveryLeftOver'])){
		$initMBCRecoveryLeftOver = $_SESSION['SESS_MBC_RecoveryLeftOver'];
		unset($_SESSION['SESS_MBC_RecoveryLeftOver']);
	}


	for ( $i = 0; $i < 10; $i++ ){
		if ( isset( $_SESSION['SESS_MBC_ProductionStart'][$i] ) ){
			$initMBCProductionStart[$i] = $_SESSION['SESS_MBC_ProductionStart'][$i];
			// unset($_SESSION['SESS_MBC_ProductionStart'][$i]);
		}
		if ( isset( $_SESSION['SESS_MBC_ProductionEnd'][$i] ) ){
			$initMBCProductionEnd[$i] = $_SESSION['SESS_MBC_ProductionEnd'][$i];
			// unset($_SESSION['SESS_MBC_ProductionEnd'][$i]);
		}
		if ( isset( $_SESSION['SESS_MBC_TimeConsumed'][$i] ) ){
			$initMBCTimeConsumed[$i] = $_SESSION['SESS_MBC_TimeConsumed'][$i];
			// unset($_SESSION['SESS_MBC_TimeConsumed'][$i]);
		}
		if ( isset( $_SESSION['SESS_MBC_FromBagEPM'][$i] ) ){
			$initMBCFromBagEPM[$i] = $_SESSION['SESS_MBC_FromBagEPM'][$i];
			unset($_SESSION['SESS_MBC_FromBagEPM'][$i]);
		}
		if ( isset( $_SESSION['SESS_MBC_ThruBagEPM'][$i] ) ){
			$initMBCThruBagEPM[$i] = $_SESSION['SESS_MBC_ThruBagEPM'][$i];
			unset($_SESSION['SESS_MBC_ThruBagEPM'][$i]);
		}
		if ( isset( $_SESSION['SESS_MBC_UnderpackEPM'][$i] ) ){
			$initMBCUnderpackEPM[$i] = $_SESSION['SESS_MBC_UnderpackEPM'][$i];
			unset($_SESSION['SESS_MBC_UnderpackEPM'][$i]);
		}
	}

?>