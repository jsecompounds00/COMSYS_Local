<?php

	unset($_SESSION['page']);
	unset($_SESSION['search']);
	unset($_SESSION['qsone']);
	
	############## SUPPLIES ############## 

	unset($_SESSION['SESS_SUP_SupplyTypeId']);
	unset($_SESSION['SESS_SUP_Supply']);
	unset($_SESSION['SESS_SUP_Description']);
	unset($_SESSION['SESS_SUP_Comments']);
	unset($_SESSION['SESS_SUP_Uom']);
	unset($_SESSION['SESS_SUP_Cmpds']);
	unset($_SESSION['SESS_SUP_Pips']);
	unset($_SESSION['SESS_SUP_Corp']);
	unset($_SESSION['SESS_SUP_PPR']);
	unset($_SESSION['SESS_SUP_Active']);
	unset($_SESSION['SESS_SUP_Miscellaneous']);

?>