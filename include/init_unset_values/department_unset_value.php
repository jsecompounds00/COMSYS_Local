<?php

	unset($_SESSION['page']);
	unset($_SESSION['search']);
	unset($_SESSION['qsone']);
	
	############## DEPARTMENTS ############## 

	unset($_SESSION['SESS_DEP_Code']);
	unset($_SESSION['SESS_DEP_Department']);
	unset($_SESSION['SESS_DEP_Active']);
	unset($_SESSION['SESS_DEP_Cmpds']);
	unset($_SESSION['SESS_DEP_Pips']);
	unset($_SESSION['SESS_DEP_Corp']);
	unset($_SESSION['SESS_DEP_PPR']);
	unset($_SESSION['SESS_DEP_Budget']);
	unset($_SESSION['SESS_DEP_AcctCode']);

?>