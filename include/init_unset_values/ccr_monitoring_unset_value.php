<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### CCR MONITORING #########################

	unset($_SESSION['SESS_CCM_CCRNo']);
	unset($_SESSION['SESS_CCM_Invalid']);
	unset($_SESSION['SESS_CCM_Customer']);
	unset($_SESSION['SESS_CCM_CCRDate']);
	unset($_SESSION['SESS_CCM_ComplaintType']);
	unset($_SESSION['SESS_CCM_OtherComplaintType']);
	unset($_SESSION['SESS_CCM_LotNum']);
	unset($_SESSION['SESS_CCM_BagNum']);
	unset($_SESSION['SESS_CCM_Complaint']);
	unset($_SESSION['SESS_CCM_SalesContainment']);
	unset($_SESSION['SESS_CCM_TechnicalContainment']);
	unset($_SESSION['SESS_CCM_WPDContainment']);
	unset($_SESSION['SESS_CCM_Containment']);
	unset($_SESSION['SESS_CCM_ImplementContainment']);
	unset($_SESSION['SESS_CCM_ResponsibleContainment']);
	unset($_SESSION['SESS_CCM_TechnicalRootCause']);
	unset($_SESSION['SESS_CCM_PAMRootCause']);
	unset($_SESSION['SESS_CCM_WPDRootCause']);
	unset($_SESSION['SESS_CCM_Man']);
	unset($_SESSION['SESS_CCM_Method']);
	unset($_SESSION['SESS_CCM_Machine']);
	unset($_SESSION['SESS_CCM_Material']);
	unset($_SESSION['SESS_CCM_Measurement']);
	unset($_SESSION['SESS_CCM_MotherNature']);
	unset($_SESSION['SESS_CCM_RootCause']);
	unset($_SESSION['SESS_CCM_Corrective']);
	unset($_SESSION['SESS_CCM_ImplementCorrective']);
	unset($_SESSION['SESS_CCM_ResponsibleCorrective']);
	unset($_SESSION['SESS_CCM_Preventive']);
	unset($_SESSION['SESS_CCM_ImplementPreventive']);
	unset($_SESSION['SESS_CCM_ResponsiblePreventive']);
	unset($_SESSION['SESS_CCM_Remarks']);
	
?>