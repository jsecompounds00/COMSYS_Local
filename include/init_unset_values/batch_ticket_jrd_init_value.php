<?php

	######################### BATCH TICKET - JRD #########################

	$session_error_indicator_JRD = 0;

	$initJRDNumberID_JRD = NULL;
	$initTERNo_JRD = NULL;
	$initBatchTicketDate_JRD = NULL;
	$initFGSoft_JRD = NULL;
	$initNewFGName_JRD = NULL;
	$initFormulaTypeID_JRD = NULL;

	$initLotNo_JRD = NULL;
	$initTrialDate_JRD = NULL;
	$initExtDarNo_JRD = NULL;
	$initExtZ1_JRD = NULL;
	$initExtZ2_JRD = NULL;
	$initExtDH_JRD = NULL;
	$initExtScrewSpeed_JRD = NULL;
	$initExtCutterSpeed_JRD = NULL;
	$initMixDarNo_JRD = NULL;
	$initMixParam1_JRD = NULL;
	$initMixSequence1_JRD = NULL;
	$initMixParam2_JRD = NULL;
	$initMixSequence2_JRD = NULL;
	$initMixParam3_JRD = NULL;
	$initMixSequence3_JRD = NULL;
	$initMixParam4_JRD = NULL;
	$initMixSequence4_JRD = NULL;
	$initMixParam5_JRD = NULL;
	$initMixSequence5_JRD = NULL;
	$initMultiplier_JRD = NULL;

	$initDynamicMilling_JRD = NULL;
	$initOilAging_JRD = NULL;
	$initOvenAging_JRD = NULL;
	$initStrandInspect_JRD = NULL;
	$initPelletInspect_JRD = NULL;
	$initImpactTest_JRD = NULL;
	$initStaticHeating_JRD = NULL;
	$initColorChange_JRD = NULL;
	$initWaterImmersion_JRD = NULL;
	$initchkColdTesting_JRD = NULL;
	
	$initRemarks_JRD = NULL;
		
	if (isset($_SESSION['JRD_error_indicator'])){
		$session_error_indicator_JRD = $_SESSION['JRD_error_indicator'];
		unset($_SESSION['JRD_error_indicator']);
	}
	
	if (isset($_SESSION['JRD_sltJRDNumber'])){
		$initJRDNumberID_JRD = $_SESSION['JRD_sltJRDNumber'];
		unset($_SESSION['JRD_sltJRDNumber']);
	}
	
	if (isset($_SESSION['JRD_txtTERNo'])){
		$initTERNo_JRD = htmlspecialchars($_SESSION['JRD_txtTERNo']);
		unset($_SESSION['JRD_txtTERNo']);
	}
	
	if (isset($_SESSION['JRD_txtBatchTicketDate'])){
		$initBatchTicketDate_JRD = htmlspecialchars($_SESSION['JRD_txtBatchTicketDate']);
		unset($_SESSION['JRD_txtBatchTicketDate']);
	}
	
	if (isset($_SESSION['JRD_radFGType'])){
		$initFGSoft_JRD = $_SESSION['JRD_radFGType'];
		// unset($_SESSION['JRD_radFGType']);
	}
	
	if (isset($_SESSION['JRD_txtNewFGName'])){
		$initNewFGName_JRD = htmlspecialchars($_SESSION['JRD_txtNewFGName']);
		// unset($_SESSION['JRD_txtNewFGName']);
	}
	
	if (isset($_SESSION['JRD_sltFormulaType'])){
		$initFormulaTypeID_JRD = $_SESSION['JRD_sltFormulaType'];
		// unset($_SESSION['JRD_sltFormulaType']);
	}
	
	if (isset($_SESSION['JRD_txtLotNo'])){
		$initLotNo_JRD = htmlspecialchars($_SESSION['JRD_txtLotNo']);
		// unset($_SESSION['JRD_txtLotNo']);
	}
	
	if (isset($_SESSION['JRD_txtTrialDate'])){
		$initTrialDate_JRD = htmlspecialchars($_SESSION['JRD_txtTrialDate']);
		// unset($_SESSION['JRD_txtTrialDate']);
	}
	
	if (isset($_SESSION['JRD_txtExtDarNo'])){
		$initExtDarNo_JRD = htmlspecialchars($_SESSION['JRD_txtExtDarNo']);
		// unset($_SESSION['JRD_txtExtDarNo']);
	}
	
	if (isset($_SESSION['JRD_txtExtZ1'])){
		$initExtZ1_JRD = htmlspecialchars($_SESSION['JRD_txtExtZ1']);
		// unset($_SESSION['JRD_txtExtZ1']);
	}
	
	if (isset($_SESSION['JRD_txtExtZ2'])){
		$initExtZ2_JRD = htmlspecialchars($_SESSION['JRD_txtExtZ2']);
		// unset($_SESSION['JRD_txtExtZ2']);
	}
	
	if (isset($_SESSION['JRD_txtExtDH'])){
		$initExtDH_JRD = htmlspecialchars($_SESSION['JRD_txtExtDH']);
		// unset($_SESSION['JRD_txtExtDH']);
	}
	
	if (isset($_SESSION['JRD_txtExtScrewSpeed'])){
		$initExtScrewSpeed_JRD = htmlspecialchars($_SESSION['JRD_txtExtScrewSpeed']);
		// unset($_SESSION['JRD_txtExtScrewSpeed']);
	}
	
	if (isset($_SESSION['JRD_txtExtCutterSpeed'])){
		$initExtCutterSpeed_JRD = htmlspecialchars($_SESSION['JRD_txtExtCutterSpeed']);
		// unset($_SESSION['JRD_txtExtCutterSpeed']);
	}
	
	if (isset($_SESSION['JRD_txtMixDarNo'])){
		$initMixDarNo_JRD = htmlspecialchars($_SESSION['JRD_txtMixDarNo']);
		// unset($_SESSION['JRD_txtMixDarNo']);
	}
	
	if (isset($_SESSION['JRD_txtMixParam1'])){
		$initMixParam1_JRD = htmlspecialchars($_SESSION['JRD_txtMixParam1']);
		// unset($_SESSION['JRD_txtMixParam1']);
	}
	
	if (isset($_SESSION['JRD_txtMixSequence1'])){
		$initMixSequence1_JRD = htmlspecialchars($_SESSION['JRD_txtMixSequence1']);
		// unset($_SESSION['JRD_txtMixSequence1']);
	}
	
	if (isset($_SESSION['JRD_txtMixParam2'])){
		$initMixParam2_JRD = htmlspecialchars($_SESSION['JRD_txtMixParam2']);
		// unset($_SESSION['JRD_txtMixParam2']);
	}
	
	if (isset($_SESSION['JRD_txtMixSequence2'])){
		$initMixSequence2_JRD = htmlspecialchars($_SESSION['JRD_txtMixSequence2']);
		// unset($_SESSION['JRD_txtMixSequence2']);
	}
	
	if (isset($_SESSION['JRD_txtMixParam3'])){
		$initMixParam3_JRD = htmlspecialchars($_SESSION['JRD_txtMixParam3']);
		// unset($_SESSION['JRD_txtMixParam3']);
	}
	
	if (isset($_SESSION['JRD_txtMixSequence3'])){
		$initMixSequence3_JRD = htmlspecialchars($_SESSION['JRD_txtMixSequence3']);
		// unset($_SESSION['JRD_txtMixSequence3']);
	}
	
	if (isset($_SESSION['JRD_txtMixParam4'])){
		$initMixParam4_JRD = htmlspecialchars($_SESSION['JRD_txtMixParam4']);
		// unset($_SESSION['JRD_txtMixParam4']);
	}
	
	if (isset($_SESSION['JRD_txtMixSequence4'])){
		$initMixSequence4_JRD = htmlspecialchars($_SESSION['JRD_txtMixSequence4']);
		// unset($_SESSION['JRD_txtMixSequence4']);
	}
	
	if (isset($_SESSION['JRD_txtMixParam5'])){
		$initMixParam5_JRD = htmlspecialchars($_SESSION['JRD_txtMixParam5']);
		// unset($_SESSION['JRD_txtMixParam5']);
	}
	
	if (isset($_SESSION['JRD_txtMixSequence5'])){
		$initMixSequence5_JRD = htmlspecialchars($_SESSION['JRD_txtMixSequence5']);
		// unset($_SESSION['JRD_txtMixSequence5']);
	}
	
	if (isset($_SESSION['JRD_txtMultiplier'])){
		$initMultiplier_JRD = htmlspecialchars($_SESSION['JRD_txtMultiplier']);
		// unset($_SESSION['JRD_txtMultiplier']);
	}
	
	if (isset($_SESSION['JRD_chkDynamicMilling'])){
		$initDynamicMilling_JRD = $_SESSION['JRD_chkDynamicMilling'];
		// unset($_SESSION['JRD_chkDynamicMilling']);
	}
	
	if (isset($_SESSION['JRD_chkOilAging'])){
		$initOilAging_JRD = $_SESSION['JRD_chkOilAging'];
		// unset($_SESSION['JRD_chkOilAging']);
	}
	
	if (isset($_SESSION['JRD_chkOvenAging'])){
		$initOvenAging_JRD = $_SESSION['JRD_chkOvenAging'];
		// unset($_SESSION['JRD_chkOvenAging']);
	}
	
	if (isset($_SESSION['JRD_chkStrandInspect'])){
		$initStrandInspect_JRD = $_SESSION['JRD_chkStrandInspect'];
		// unset($_SESSION['JRD_chkStrandInspect']);
	}
	
	if (isset($_SESSION['JRD_chkPelletInspect'])){
		$initPelletInspect_JRD = $_SESSION['JRD_chkPelletInspect'];
		// unset($_SESSION['JRD_chkPelletInspect']);
	}
	
	if (isset($_SESSION['JRD_chkImpactTest'])){
		$initImpactTest_JRD = $_SESSION['JRD_chkImpactTest'];
		// unset($_SESSION['JRD_chkImpactTest']);
	}
	
	if (isset($_SESSION['JRD_chkStaticHeating'])){
		$initStaticHeating_JRD = $_SESSION['JRD_chkStaticHeating'];
		// unset($_SESSION['JRD_chkStaticHeating']);
	}
	
	if (isset($_SESSION['JRD_chkColorChange'])){
		$initColorChange_JRD = $_SESSION['JRD_chkColorChange'];
		// unset($_SESSION['JRD_chkColorChange']);
	}
	
	if (isset($_SESSION['JRD_chkWaterImmersion'])){
		$initWaterImmersion_JRD = $_SESSION['JRD_chkWaterImmersion'];
		// unset($_SESSION['JRD_chkWaterImmersion']);
	}
	
	if (isset($_SESSION['JRD_chkColdTesting'])){
		$initchkColdTesting_JRD = $_SESSION['JRD_chkColdTesting'];
		// unset($_SESSION['JRD_chkColdTesting']);
	}

	if (isset($_SESSION['JRD_txtRemarks'])){
		$initRemarks_JRD = htmlspecialchars($_SESSION['JRD_txtRemarks']);
		// unset($_SESSION['JRD_txtRemarks']);
	}

?>