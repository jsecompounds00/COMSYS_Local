<?php

	######################### CPIAR VERIFICATION #########################

	$initCPMCPRNo = NULL;
	$initCPMCPRDate = NULL;
	$initCPMDueDate = NULL;
	$initCPMIssueDeptID = NULL;
	$initCPMIssuerID = NULL;
	$initCPMReceiveDeptID = NULL;
	$initCPMReceiverID = NULL;
	$initCPMOccurrence = NULL;
	$initCPMCriticality = NULL;
	$initCPMSource = NULL;
	$initCPMClause = NULL;
	$initCPMDetails = NULL;
	$initCPMMemoDate = NULL;
	$initCPMFinalDueDate = NULL;
	$initCPMReceiveDateDCC = NULL;
	$initCPMRootCause = NULL;
	$initCPMContainment = NULL;
	$initCPMImplementContainment = NULL;
	$initCPMAction = NULL;
	$initCPMCorrectPrevent = NULL;
	$initCPMImplementCorrectPrevent = NULL;
	$initCPMMan = NULL;
	$initCPMMethod = NULL;
	$initCPMMachine = NULL;
	$initCPMMaterial = NULL;
	$initCPMMeasurement = NULL;
	$initCPMMotherNature = NULL;
	$initCPMRemarks = NULL;
	$initCPMFromDivision = NULL;
	$initCPMToDivision = NULL;

	if (isset($_SESSION["SESS_CPM_CPRNo"])){
		$initCPMCPRNo = $_SESSION["SESS_CPM_CPRNo"];
		unset($_SESSION["SESS_CPM_CPRNo"]);
	}
	if (isset($_SESSION["SESS_CPM_CPRDate"])){
		$initCPMCPRDate = $_SESSION["SESS_CPM_CPRDate"];
		unset($_SESSION["SESS_CPM_CPRDate"]);
	}
	if (isset($_SESSION["SESS_CPM_DueDate"])){
		$initCPMDueDate = $_SESSION["SESS_CPM_DueDate"];
		unset($_SESSION["SESS_CPM_DueDate"]);
	}
	if (isset($_SESSION["SESS_CPM_IssueDeptID"])){
		$initCPMIssueDeptID = $_SESSION["SESS_CPM_IssueDeptID"];
		unset($_SESSION["SESS_CPM_IssueDeptID"]);
	}
	if (isset($_SESSION["SESS_CPM_IssuerID"])){
		$initCPMIssuerID = $_SESSION["SESS_CPM_IssuerID"];
		unset($_SESSION["SESS_CPM_IssuerID"]);
	}
	if (isset($_SESSION["SESS_CPM_ReceiveDeptID"])){
		$initCPMReceiveDeptID = $_SESSION["SESS_CPM_ReceiveDeptID"];
		unset($_SESSION["SESS_CPM_ReceiveDeptID"]);
	}
	if (isset($_SESSION["SESS_CPM_ReceiverID"])){
		$initCPMReceiverID = $_SESSION["SESS_CPM_ReceiverID"];
		unset($_SESSION["SESS_CPM_ReceiverID"]);
	}
	if (isset($_SESSION["SESS_CPM_Occurrence"])){
		$initCPMOccurrence = $_SESSION["SESS_CPM_Occurrence"];
		unset($_SESSION["SESS_CPM_Occurrence"]);
	}
	if (isset($_SESSION["SESS_CPM_Criticality"])){
		$initCPMCriticality = $_SESSION["SESS_CPM_Criticality"];
		unset($_SESSION["SESS_CPM_Criticality"]);
	}
	if (isset($_SESSION["SESS_CPM_Source"])){
		$initCPMSource = $_SESSION["SESS_CPM_Source"];
		unset($_SESSION["SESS_CPM_Source"]);
	}
	if (isset($_SESSION["SESS_CPM_Clause"])){
		$initCPMClause = $_SESSION["SESS_CPM_Clause"];
		unset($_SESSION["SESS_CPM_Clause"]);
	}
	if (isset($_SESSION["SESS_CPM_Details"])){
		$initCPMDetails = $_SESSION["SESS_CPM_Details"];
		unset($_SESSION["SESS_CPM_Details"]);
	}
	if (isset($_SESSION["SESS_CPM_MemoDate"])){
		$initCPMMemoDate = $_SESSION["SESS_CPM_MemoDate"];
		unset($_SESSION["SESS_CPM_MemoDate"]);
	}
	if (isset($_SESSION["SESS_CPM_FinalDueDate"])){
		$initCPMFinalDueDate = $_SESSION["SESS_CPM_FinalDueDate"];
		unset($_SESSION["SESS_CPM_FinalDueDate"]);
	}
	if (isset($_SESSION["SESS_CPM_ReceiveDateDCC"])){
		$initCPMReceiveDateDCC = $_SESSION["SESS_CPM_ReceiveDateDCC"];
		unset($_SESSION["SESS_CPM_ReceiveDateDCC"]);
	}
	if (isset($_SESSION["SESS_CPM_RootCause"])){
		$initCPMRootCause = $_SESSION["SESS_CPM_RootCause"];
		unset($_SESSION["SESS_CPM_RootCause"]);
	}
	if (isset($_SESSION["SESS_CPM_Containment"])){
		$initCPMContainment = $_SESSION["SESS_CPM_Containment"];
		unset($_SESSION["SESS_CPM_Containment"]);
	}
	if (isset($_SESSION["SESS_CPM_ImplementContainment"])){
		$initCPMImplementContainment = $_SESSION["SESS_CPM_ImplementContainment"];
		unset($_SESSION["SESS_CPM_ImplementContainment"]);
	}
	if (isset($_SESSION["SESS_CPM_Action"])){
		$initCPMAction = $_SESSION["SESS_CPM_Action"];
		unset($_SESSION["SESS_CPM_Action"]);
	}
	if (isset($_SESSION["SESS_CPM_CorrectPrevent"])){
		$initCPMCorrectPrevent = $_SESSION["SESS_CPM_CorrectPrevent"];
		unset($_SESSION["SESS_CPM_CorrectPrevent"]);
	}
	if (isset($_SESSION["SESS_CPM_ImplementCorrectPrevent"])){
		$initCPMImplementCorrectPrevent = $_SESSION["SESS_CPM_ImplementCorrectPrevent"];
		unset($_SESSION["SESS_CPM_ImplementCorrectPrevent"]);
	}
	if (isset($_SESSION["SESS_CPM_Man"])){
		$initCPMMan = $_SESSION["SESS_CPM_Man"];
		unset($_SESSION["SESS_CPM_Man"]);
	}
	if (isset($_SESSION["SESS_CPM_Method"])){
		$initCPMMethod = $_SESSION["SESS_CPM_Method"];
		unset($_SESSION["SESS_CPM_Method"]);
	}
	if (isset($_SESSION["SESS_CPM_Machine"])){
		$initCPMMachine = $_SESSION["SESS_CPM_Machine"];
		unset($_SESSION["SESS_CPM_Machine"]);
	}
	if (isset($_SESSION["SESS_CPM_Material"])){
		$initCPMMaterial = $_SESSION["SESS_CPM_Material"];
		unset($_SESSION["SESS_CPM_Material"]);
	}
	if (isset($_SESSION["SESS_CPM_Measurement"])){
		$initCPMMeasurement = $_SESSION["SESS_CPM_Measurement"];
		unset($_SESSION["SESS_CPM_Measurement"]);
	}
	if (isset($_SESSION["SESS_CPM_MotherNature"])){
		$initCPMMotherNature = $_SESSION["SESS_CPM_MotherNature"];
		unset($_SESSION["SESS_CPM_MotherNature"]);
	}
	if (isset($_SESSION["SESS_CPM_Remarks"])){
		$initCPMRemarks = $_SESSION["SESS_CPM_Remarks"];
		unset($_SESSION["SESS_CPM_Remarks"]);
	}
	if (isset($_SESSION["SESS_CPM_FromDivision"])){
		$initCPMFromDivision = $_SESSION["SESS_CPM_FromDivision"];
		unset($_SESSION["SESS_CPM_FromDivision"]);
	}
	if (isset($_SESSION["SESS_CPM_ToDivision"])){
		$initCPMToDivision = $_SESSION["SESS_CPM_ToDivision"];
		unset($_SESSION["SESS_CPM_ToDivision"]);
	}


?>