<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### CPIAR VERIFICATION #########################

	unset($_SESSION['SESS_CPV_VerifyDate']);
	unset($_SESSION['SESS_CPV_Division']);
	unset($_SESSION['SESS_CPV_DeptID']);
	unset($_SESSION['SESS_CPV_AuditorID']);
	unset($_SESSION['SESS_CPV_Verification']);
	unset($_SESSION['SESS_CPV_Status']);
	unset($_SESSION['SESS_CPV_Month']);
	unset($_SESSION['SESS_CPV_Year']);

?>