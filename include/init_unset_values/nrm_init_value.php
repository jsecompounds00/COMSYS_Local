<?php

	######################### NRM #########################

	$initNRMNo = NULL;
	$initNRMDate = NULL;
	$initMaterialDescription = NULL;
	$initSupplier = NULL;
	$initTagOtherSupplier = NULL;
	$initOtherSupplier = NULL;
	$initClass = NULL;
	$initOtherClass = NULL;
	$initSample = NULL;
	$initOtherSample = NULL;
	$initTagMSDS = NULL;
	$initTagTDS = NULL;
	$initRemarks = NULL;
	
	if (isset($_SESSION['SESS_NRM_NRMNo'])){
		$initNRMNo = htmlspecialchars($_SESSION['SESS_NRM_NRMNo']);
		unset($_SESSION['SESS_NRM_NRMNo']);
	}
	
	if (isset($_SESSION['SESS_NRM_NRMDate'])){
		$initNRMDate = htmlspecialchars($_SESSION['SESS_NRM_NRMDate']);
		unset($_SESSION['SESS_NRM_NRMDate']);
	}
	
	if (isset($_SESSION['SESS_NRM_MaterialDescription'])){
		$initMaterialDescription = htmlspecialchars($_SESSION['SESS_NRM_MaterialDescription']);
		unset($_SESSION['SESS_NRM_MaterialDescription']);
	}
	
	if (isset($_SESSION['SESS_NRM_Supplier'])){
		$initSupplier = $_SESSION['SESS_NRM_Supplier'];
		unset($_SESSION['SESS_NRM_Supplier']);
	}
	
	if (isset($_SESSION['SESS_NRM_OtherSupplier'])){
		$initTagOtherSupplier = $_SESSION['SESS_NRM_OtherSupplier'];
		unset($_SESSION['SESS_NRM_OtherSupplier']);
	}
	
	if (isset($_SESSION['SESS_NRM_OtherSupplier'])){
		$initOtherSupplier = htmlspecialchars($_SESSION['SESS_NRM_OtherSupplier']);
		unset($_SESSION['SESS_NRM_OtherSupplier']);
	}
	
	if (isset($_SESSION['SESS_NRM_Class'])){
		$initClass = $_SESSION['SESS_NRM_Class'];
		unset($_SESSION['SESS_NRM_Class']);
	}
	
	if (isset($_SESSION['SESS_NRM_OtherClass'])){
		$initOtherClass = htmlspecialchars($_SESSION['SESS_NRM_OtherClass']);
		unset($_SESSION['SESS_NRM_OtherClass']);
	}
	
	if (isset($_SESSION['SESS_NRM_Sample'])){
		$initSample = $_SESSION['SESS_NRM_Sample'];
		unset($_SESSION['SESS_NRM_Sample']);
	}
	
	if (isset($_SESSION['SESS_NRM_OtherSample'])){
		$initOtherSample = htmlspecialchars($_SESSION['SESS_NRM_OtherSample']);
		unset($_SESSION['SESS_NRM_OtherSample']);
	}
	
	if (isset($_SESSION['SESS_NRM_MSDS'])){
		$initTagMSDS = $_SESSION['SESS_NRM_MSDS'];
		unset($_SESSION['SESS_NRM_MSDS']);
	}
	
	if (isset($_SESSION['SESS_NRM_TDS'])){
		$initTagTDS = $_SESSION['SESS_NRM_TDS'];
		unset($_SESSION['SESS_NRM_TDS']);
	}
	
	if (isset($_SESSION['SESS_NRM_Remarks'])){
		$initRemarks = htmlspecialchars($_SESSION['SESS_NRM_Remarks']);
		unset($_SESSION['SESS_NRM_Remarks']);
	}

	######################### NRM CLIENT RESPONSE

	$initNRMNRM = NULL;
	$initNRMRecommendation = NULL;
	$initNRMTestObjective = NULL;
	$initNRMIntroduction = NULL;
	$initNRMMethodology = NULL;

	if (isset($_SESSION['SESS_NRMR_NRM'])){
		$initNRMNRM = $_SESSION['SESS_NRMR_NRM'];
		unset($_SESSION['SESS_NRMR_NRM']);
	}

	if (isset($_SESSION['SESS_NRMR_Recommendation'])){
		$initNRMRecommendation = htmlspecialchars($_SESSION['SESS_NRMR_Recommendation']);
		// unset($_SESSION['SESS_NRMR_Recommendation']);
	}

	if (isset($_SESSION['SESS_NRMR_TestObjective'])){
		$initNRMTestObjective = htmlspecialchars($_SESSION['SESS_NRMR_TestObjective']);
		// unset($_SESSION['SESS_NRMR_TestObjective']);
	}

	if (isset($_SESSION['SESS_NRMR_Introduction'])){
		$initNRMIntroduction = htmlspecialchars($_SESSION['SESS_NRMR_Introduction']);
		// unset($_SESSION['SESS_NRMR_Introduction']);
	}

	if (isset($_SESSION['SESS_NRMR_Methodology'])){
		$initNRMMethodology = htmlspecialchars($_SESSION['SESS_NRMR_Methodology']);
		// unset($_SESSION['SESS_NRMR_Methodology']);
	}


?>