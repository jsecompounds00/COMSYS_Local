<?php

	// unset($_SESSION["page"]);
	// unset($_SESSION["search"]);
	// unset($_SESSION["qsone"]);

	######################### NRM #########################

	unset($_SESSION['SESS_NRM_NRMNo']);
	unset($_SESSION['SESS_NRM_NRMDate']);
	unset($_SESSION['SESS_NRM_MaterialDescription']);
	unset($_SESSION['SESS_NRM_Supplier']);
	unset($_SESSION['SESS_NRM_OtherSupplier']);
	unset($_SESSION['SESS_NRM_OtherSupplier']);
	unset($_SESSION['SESS_NRM_Class']);
	unset($_SESSION['SESS_NRM_OtherClass']);
	unset($_SESSION['SESS_NRM_Sample']);
	unset($_SESSION['SESS_NRM_OtherSample']);
	unset($_SESSION['SESS_NRM_MSDS']);
	unset($_SESSION['SESS_NRM_TDS']);
	unset($_SESSION['SESS_NRM_Remarks']);

	######################### NRM RECOMMENDATION

	unset($_SESSION['SESS_NRMR_NRM']);
	unset($_SESSION['SESS_NRMR_Recommendation']);
	unset($_SESSION['SESS_NRMR_TestObjective']);
	unset($_SESSION['SESS_NRMR_Introduction']);
	unset($_SESSION['SESS_NRMR_Methodology']);
?>