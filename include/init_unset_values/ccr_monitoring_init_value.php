<?php

	######################### CCR MONITORING #########################

	$initCCMCCRNo = NULL;
	$initCCMInvalid = NULL;
	$initCCMCustomer = NULL;
	$initCCMCCRDate = NULL;
	$initCCMComplaintType = NULL;
	$initCCMOtherComplaintType = NULL;
	$initCCMLotNum = NULL;
	$initCCMBagNum = NULL;
	$initCCMComplaint = NULL;
	$initCCMSalesContainment = NULL;
	$initCCMTechnicalContainment = NULL;
	$initCCMWPDContainment = NULL;
	$initCCMContainment = NULL;
	$initCCMImplementContainment = NULL;
	$initCCMResponsibleContainment = NULL;
	$initCCMTechnicalRootCause = NULL;
	$initCCMPAMRootCause = NULL;
	$initCCMWPDRootCause = NULL;
	$initCCMMan = NULL;
	$initCCMMethod = NULL;
	$initCCMMachine = NULL;
	$initCCMMaterial = NULL;
	$initCCMMeasurement = NULL;
	$initCCMMotherNature = NULL;
	$initCCMRootCause = NULL;
	$initCCMCorrective = NULL;
	$initCCMImplementCorrective = NULL;
	$initCCMResponsibleCorrective = NULL;
	$initCCMPreventive = NULL;
	$initCCMImplementPreventive = NULL;
	$initCCMResponsiblePreventive = NULL;
	$initCCMRemarks = NULL;

	if (isset($_SESSION['SESS_CCM_CCRNo'])){
		$initCCMCCRNo = htmlspecialchars($_SESSION['SESS_CCM_CCRNo']);
		unset($_SESSION['SESS_CCM_CCRNo']);
	}
	if (isset($_SESSION['SESS_CCM_Invalid'])){
		$initCCMInvalid = $_SESSION['SESS_CCM_Invalid'];
		unset($_SESSION['SESS_CCM_Invalid']);
	}
	if (isset($_SESSION['SESS_CCM_Customer'])){
		$initCCMCustomer = $_SESSION['SESS_CCM_Customer'];
		unset($_SESSION['SESS_CCM_Customer']);
	}
	if (isset($_SESSION['SESS_CCM_CCRDate'])){
		$initCCMCCRDate = htmlspecialchars($_SESSION['SESS_CCM_CCRDate']);
		unset($_SESSION['SESS_CCM_CCRDate']);
	}
	if (isset($_SESSION['SESS_CCM_ComplaintType'])){
		$initCCMComplaintType = $_SESSION['SESS_CCM_ComplaintType'];
		unset($_SESSION['SESS_CCM_ComplaintType']);
	}
	if (isset($_SESSION['SESS_CCM_OtherComplaintType'])){
		$initCCMOtherComplaintType = htmlspecialchars($_SESSION['SESS_CCM_OtherComplaintType']);
		unset($_SESSION['SESS_CCM_OtherComplaintType']);
	}
	if (isset($_SESSION['SESS_CCM_LotNum'])){
		$initCCMLotNum = htmlspecialchars($_SESSION['SESS_CCM_LotNum']);
		unset($_SESSION['SESS_CCM_LotNum']);
	}
	if (isset($_SESSION['SESS_CCM_BagNum'])){
		$initCCMBagNum = htmlspecialchars($_SESSION['SESS_CCM_BagNum']);
		unset($_SESSION['SESS_CCM_BagNum']);
	}
	if (isset($_SESSION['SESS_CCM_Complaint'])){
		$initCCMComplaint = htmlspecialchars($_SESSION['SESS_CCM_Complaint']);
		unset($_SESSION['SESS_CCM_Complaint']);
	}
	if (isset($_SESSION['SESS_CCM_SalesContainment'])){
		$initCCMSalesContainment = $_SESSION['SESS_CCM_SalesContainment'];
		unset($_SESSION['SESS_CCM_SalesContainment']);
	}
	if (isset($_SESSION['SESS_CCM_TechnicalContainment'])){
		$initCCMTechnicalContainment = $_SESSION['SESS_CCM_TechnicalContainment'];
		unset($_SESSION['SESS_CCM_TechnicalContainment']);
	}
	if (isset($_SESSION['SESS_CCM_WPDContainment'])){
		$initCCMWPDContainment = $_SESSION['SESS_CCM_WPDContainment'];
		unset($_SESSION['SESS_CCM_WPDContainment']);
	}
	if (isset($_SESSION['SESS_CCM_Containment'])){
		$initCCMContainment = htmlspecialchars($_SESSION['SESS_CCM_Containment']);
		unset($_SESSION['SESS_CCM_Containment']);
	}
	if (isset($_SESSION['SESS_CCM_ImplementContainment'])){
		$initCCMImplementContainment = htmlspecialchars($_SESSION['SESS_CCM_ImplementContainment']);
		unset($_SESSION['SESS_CCM_ImplementContainment']);
	}
	if (isset($_SESSION['SESS_CCM_ResponsibleContainment'])){
		$initCCMResponsibleContainment = htmlspecialchars($_SESSION['SESS_CCM_ResponsibleContainment']);
		unset($_SESSION['SESS_CCM_ResponsibleContainment']);
	}
	if (isset($_SESSION['SESS_CCM_TechnicalRootCause'])){
		$initCCMTechnicalRootCause = $_SESSION['SESS_CCM_TechnicalRootCause'];
		unset($_SESSION['SESS_CCM_TechnicalRootCause']);
	}
	if (isset($_SESSION['SESS_CCM_PAMRootCause'])){
		$initCCMPAMRootCause = $_SESSION['SESS_CCM_PAMRootCause'];
		unset($_SESSION['SESS_CCM_PAMRootCause']);
	}
	if (isset($_SESSION['SESS_CCM_WPDRootCause'])){
		$initCCMWPDRootCause = $_SESSION['SESS_CCM_WPDRootCause'];
		unset($_SESSION['SESS_CCM_WPDRootCause']);
	}
	if (isset($_SESSION['SESS_CCM_Man'])){
		$initCCMMan = $_SESSION['SESS_CCM_Man'];
		unset($_SESSION['SESS_CCM_Man']);
	}
	if (isset($_SESSION['SESS_CCM_Method'])){
		$initCCMMethod = $_SESSION['SESS_CCM_Method'];
		unset($_SESSION['SESS_CCM_Method']);
	}
	if (isset($_SESSION['SESS_CCM_Machine'])){
		$initCCMMachine = $_SESSION['SESS_CCM_Machine'];
		unset($_SESSION['SESS_CCM_Machine']);
	}
	if (isset($_SESSION['SESS_CCM_Material'])){
		$initCCMMaterial = $_SESSION['SESS_CCM_Material'];
		unset($_SESSION['SESS_CCM_Material']);
	}
	if (isset($_SESSION['SESS_CCM_Measurement'])){
		$initCCMMeasurement = $_SESSION['SESS_CCM_Measurement'];
		unset($_SESSION['SESS_CCM_Measurement']);
	}
	if (isset($_SESSION['SESS_CCM_MotherNature'])){
		$initCCMMotherNature = $_SESSION['SESS_CCM_MotherNature'];
		unset($_SESSION['SESS_CCM_MotherNature']);
	}
	if (isset($_SESSION['SESS_CCM_RootCause'])){
		$initCCMRootCause = htmlspecialchars($_SESSION['SESS_CCM_RootCause']);
		unset($_SESSION['SESS_CCM_RootCause']);
	}
	if (isset($_SESSION['SESS_CCM_Corrective'])){
		$initCCMCorrective = htmlspecialchars($_SESSION['SESS_CCM_Corrective']);
		unset($_SESSION['SESS_CCM_Corrective']);
	}
	if (isset($_SESSION['SESS_CCM_ImplementCorrective'])){
		$initCCMImplementCorrective = htmlspecialchars($_SESSION['SESS_CCM_ImplementCorrective']);
		unset($_SESSION['SESS_CCM_ImplementCorrective']);
	}
	if (isset($_SESSION['SESS_CCM_ResponsibleCorrective'])){
		$initCCMResponsibleCorrective = htmlspecialchars($_SESSION['SESS_CCM_ResponsibleCorrective']);
		unset($_SESSION['SESS_CCM_ResponsibleCorrective']);
	}
	if (isset($_SESSION['SESS_CCM_Preventive'])){
		$initCCMPreventive = htmlspecialchars($_SESSION['SESS_CCM_Preventive']);
		unset($_SESSION['SESS_CCM_Preventive']);
	}
	if (isset($_SESSION['SESS_CCM_ImplementPreventive'])){
		$initCCMImplementPreventive = htmlspecialchars($_SESSION['SESS_CCM_ImplementPreventive']);
		unset($_SESSION['SESS_CCM_ImplementPreventive']);
	}
	if (isset($_SESSION['SESS_CCM_ResponsiblePreventive'])){
		$initCCMResponsiblePreventive = htmlspecialchars($_SESSION['SESS_CCM_ResponsiblePreventive']);
		unset($_SESSION['SESS_CCM_ResponsiblePreventive']);
	}
	if (isset($_SESSION['SESS_CCM_Remarks'])){
		$initCCMRemarks = htmlspecialchars($_SESSION['SESS_CCM_Remarks']);
		unset($_SESSION['SESS_CCM_Remarks']);
	}


?>