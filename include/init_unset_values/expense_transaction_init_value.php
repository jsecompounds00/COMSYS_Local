<?php

	##############  ############## 

	$initBETTransactionDate = NULL;
	$initBETDepartment = NULL;
	$initBETGLCode = NULL;
	$initBETParticulars = NULL;
	$initBETUOM = NULL;
	$initBETQuantity = NULL;
	$initBETUnitPrice = NULL;
	$initBETComments = NULL;
	$initBETRemarks = NULL;

	if (isset($_SESSION['SESS_BET_TransactionDate'])){
		$initBETTransactionDate = htmlspecialchars($_SESSION['SESS_BET_TransactionDate']); 
		unset($_SESSION['SESS_BET_TransactionDate']);
	}
	if (isset($_SESSION['SESS_BET_Department'])){
		$initBETDepartment = htmlspecialchars($_SESSION['SESS_BET_Department']); 
		unset($_SESSION['SESS_BET_Department']);
	}
	if (isset($_SESSION['SESS_BET_GLCode'])){
		$initBETGLCode = $_SESSION['SESS_BET_GLCode']; 
		unset($_SESSION['SESS_BET_GLCode']);
	}
	if (isset($_SESSION['SESS_BET_Particulars'])){
		$initBETParticulars = $_SESSION['SESS_BET_Particulars']; 
		unset($_SESSION['SESS_BET_Particulars']);
	}
	if (isset($_SESSION['SESS_BET_UOM'])){
		$initBETUOM = $_SESSION['SESS_BET_UOM']; 
		unset($_SESSION['SESS_BET_UOM']);
	}
	if (isset($_SESSION['SESS_BET_Quantity'])){
		$initBETQuantity = $_SESSION['SESS_BET_Quantity']; 
		unset($_SESSION['SESS_BET_Quantity']);
	}
	if (isset($_SESSION['SESS_BET_UnitPrice'])){
		$initBETUnitPrice = $_SESSION['SESS_BET_UnitPrice']; 
		unset($_SESSION['SESS_BET_UnitPrice']);
	}
	if (isset($_SESSION['SESS_BET_Comments'])){
		$initBETComments = htmlspecialchars($_SESSION['SESS_BET_Comments']); 
		unset($_SESSION['SESS_BET_Comments']);
	}
	if (isset($_SESSION['SESS_BET_Remarks'])){
		$initBETRemarks = $_SESSION['SESS_BET_Remarks']; 
		unset($_SESSION['SESS_BET_Remarks']);
	}

?>