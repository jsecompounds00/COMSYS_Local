<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### ICC MONITORING #########################

	unset($_SESSION['SESS_ICM_ICCNo']);
	unset($_SESSION['SESS_ICM_ICCDate']);
	unset($_SESSION['SESS_ICM_DueDate']);
	unset($_SESSION['SESS_ICM_IssueDeptID']);
	unset($_SESSION['SESS_ICM_ReceiveDeptID']);
	unset($_SESSION['SESS_ICM_NatureOfComplaint']);
	unset($_SESSION['SESS_ICM_RootCause']);
	unset($_SESSION['SESS_ICM_ContainmentAction']);
	unset($_SESSION['SESS_ICM_ContainmentImplement']);
	unset($_SESSION['SESS_ICM_ContainmentResponsible']);
	unset($_SESSION['SESS_ICM_CorrectiveAction']);
	unset($_SESSION['SESS_ICM_CorrectiveImplementation']);
	unset($_SESSION['SESS_ICM_CorrectiveResponsible']);
	unset($_SESSION['SESS_ICM_MemoDate']);
	unset($_SESSION['SESS_ICM_FinalDueDate']);
	unset($_SESSION['SESS_ICM_ReceiveDateDCC']);
	unset($_SESSION['SESS_ICM_Month']);
	unset($_SESSION['SESS_ICM_Year']);
	unset($_SESSION['SESS_ICM_Remarks']);
	unset($_SESSION['SESS_ICM_FromDivision']);
	unset($_SESSION['SESS_ICM_ToDivision']);
	unset($_SESSION['SESS_ICM_Man']);
	unset($_SESSION['SESS_ICM_Method']);
	unset($_SESSION['SESS_ICM_Machine']);
	unset($_SESSION['SESS_ICM_Material']);
	unset($_SESSION['SESS_ICM_Measurement']);
	unset($_SESSION['SESS_ICM_MotherNature']);
	
?>