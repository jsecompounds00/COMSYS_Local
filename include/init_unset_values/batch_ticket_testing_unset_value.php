<?php

	// unset($_SESSION["page"]);
	// unset($_SESSION["search"]);
	// unset($_SESSION["qsone"]);

	######################### BATCH TICKET TESTING #########################

	unset($_SESSION['BT_txtSpecificGravity']);
	unset($_SESSION['BT_txtHardnessA']);
	unset($_SESSION['BT_txtVolumeResistivity']);
	unset($_SESSION['BT_txtHardnessD']);
	unset($_SESSION['BT_txtUnagedTS']);
	unset($_SESSION['BT_txtOvenTS']);
	unset($_SESSION['BT_txtOilTS']);
	unset($_SESSION['BT_txtUnagedE']);
	unset($_SESSION['BT_txtOvenE']);
	unset($_SESSION['BT_txtOilE']);

	unset($_SESSION['BT_txtSampling']);
	unset($_SESSION['BT_radDMColor']);
	unset($_SESSION['BT_radDMHeat']);
	unset($_SESSION['BT_radDMProcess']);
	
	unset($_SESSION['BT_sltFisheyeEvaluation']);
	unset($_SESSION['BT_sltFisheyeClass']);
	unset($_SESSION['BT_sltOverallRemarks']);
	unset($_SESSION['BT_sltPinholeEvaluation']);
	unset($_SESSION['BT_chkColorConform']);
	unset($_SESSION['BT_chkPorous']);
	unset($_SESSION['BT_chkFCPresent']);

	unset($_SESSION['BT_chkClarityColorConform']);

	unset($_SESSION['BT_txtDropWeight']);
	unset($_SESSION['BT_txtDropHeight']);
	unset($_SESSION['BT_txtSamplesNo']);
	unset($_SESSION['BT_txtPassedNo']);
	
	unset($_SESSION['BT_radSHColor']);
	unset($_SESSION['BT_radSHHeat']);

	unset($_SESSION['BT_radCCTColor']);
	unset($_SESSION['BT_radCCTHeat']);

	unset($_SESSION['BT_txtImmersionStartDate']);
	unset($_SESSION['BT_txtImmersionEndDate']);
	unset($_SESSION['BT_txtDay0']);
	unset($_SESSION['BT_txtDay1']);
	unset($_SESSION['BT_txtDay2']);
	unset($_SESSION['BT_txtDay3']);
	unset($_SESSION['BT_txtDay4']);
	unset($_SESSION['BT_txtDay5']);
	unset($_SESSION['BT_txtDay6']);
	unset($_SESSION['BT_txtDay7']);
	unset($_SESSION['BT_txtDay8']);
	unset($_SESSION['BT_txtDay9']);
	unset($_SESSION['BT_txtDay10']);
	unset($_SESSION['BT_txtDay11']);
	unset($_SESSION['BT_txtDay12']);
	unset($_SESSION['BT_txtDay13']);
	unset($_SESSION['BT_txtDay14']);

	unset($_SESSION['BT_txtCIResult1']);
	unset($_SESSION['BT_txtCIResult2']);
	unset($_SESSION['BT_txtCIResult3']);
	unset($_SESSION['BT_txtCICracked1']);
	unset($_SESSION['BT_txtCICracked2']);
	unset($_SESSION['BT_txtCICracked3']);

?>