<?php

	############## SUPPLY TYPE ############## 

	$initSPTNewSupplyType = NULL;
	$initSPTCmpds = NULL;
	$initSPTPips = NULL;
	$initSPTCorp = NULL;
	$initSPTActive = NULL;
	$initSPTComments = NULL;

	if (isset($_SESSION['SESS_SPT_NewSupplyType'])){
		$initSPTNewSupplyType = htmlspecialchars($_SESSION['SESS_SPT_NewSupplyType']); 
		unset($_SESSION['SESS_SPT_NewSupplyType']);
	}
	if (isset($_SESSION['SESS_SPT_Cmpds'])){
		$initSPTCmpds = $_SESSION['SESS_SPT_Cmpds']; 
		unset($_SESSION['SESS_SPT_Cmpds']);
	}
	if (isset($_SESSION['SESS_SPT_Pips'])){
		$initSPTPips = $_SESSION['SESS_SPT_Pips']; 
		unset($_SESSION['SESS_SPT_Pips']);
	}
	if (isset($_SESSION['SESS_SPT_Corp'])){
		$initSPTCorp = $_SESSION['SESS_SPT_Corp']; 
		unset($_SESSION['SESS_SPT_Corp']);
	}
	if (isset($_SESSION['SESS_SPT_Active'])){
		$initSPTActive = $_SESSION['SESS_SPT_Active']; 
		unset($_SESSION['SESS_SPT_Active']);
	}
	if (isset($_SESSION['SESS_SPT_Comments'])){
		$initSPTComments = htmlspecialchars($_SESSION['SESS_SPT_Comments']); 
		unset($_SESSION['SESS_SPT_Comments']);
	}

?>