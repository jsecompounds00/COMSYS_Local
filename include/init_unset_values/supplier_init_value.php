<?php

	############## SUPPLIER ############## 

	$initSPRSupplier = NULL;
	$initSPRRm = NULL;
	$initSPRFg = NULL;
	$initSPRSupplies = NULL;
	$initSPRActive = NULL;
	$initSPRCmpds = NULL;
	$initSPRPips = NULL;
	$initSPRCorp = NULL;
	$initSPRPPR = NULL;

	if (isset($_SESSION['SESS_SPR_Supplier'])){
		$initSPRSupplier = htmlspecialchars($_SESSION['SESS_SPR_Supplier']); 
		unset($_SESSION['SESS_SPR_Supplier']);
	}
	if (isset($_SESSION['SESS_SPR_Rm'])){
		$initSPRRm = $_SESSION['SESS_SPR_Rm']; 
		unset($_SESSION['SESS_SPR_Rm']);
	}
	if (isset($_SESSION['SESS_SPR_Fg'])){
		$initSPRFg = $_SESSION['SESS_SPR_Fg']; 
		unset($_SESSION['SESS_SPR_Fg']);
	}
	if (isset($_SESSION['SESS_SPR_Supplies'])){
		$initSPRSupplies = $_SESSION['SESS_SPR_Supplies']; 
		unset($_SESSION['SESS_SPR_Supplies']);
	}
	if (isset($_SESSION['SESS_SPR_Active'])){
		$initSPRActive = $_SESSION['SESS_SPR_Active']; 
		unset($_SESSION['SESS_SPR_Active']);
	}
	if (isset($_SESSION['SESS_SPR_Cmpds'])){
		$initSPRCmpds = $_SESSION['SESS_SPR_Cmpds']; 
		unset($_SESSION['SESS_SPR_Cmpds']);
	}
	if (isset($_SESSION['SESS_SPR_Pips'])){
		$initSPRPips = $_SESSION['SESS_SPR_Pips']; 
		unset($_SESSION['SESS_SPR_Pips']);
	}
	if (isset($_SESSION['SESS_SPR_Corp'])){
		$initSPRCorp = $_SESSION['SESS_SPR_Corp']; 
		unset($_SESSION['SESS_SPR_Corp']);
	}
	if (isset($_SESSION['SESS_SPR_PPR'])){
		$initSPRPPR = $_SESSION['SESS_SPR_PPR']; 
		unset($_SESSION['SESS_SPR_PPR']);
	}

?>