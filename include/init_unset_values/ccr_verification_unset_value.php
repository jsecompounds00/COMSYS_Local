<?php

	unset($_SESSION["page"]);
	unset($_SESSION["search"]);
	unset($_SESSION["qsone"]);

	######################### CPIAR VERIFICATION #########################

	unset($_SESSION['SESS_CCV_VerifyDate']);
	unset($_SESSION['SESS_CCV_Division']);
	unset($_SESSION['SESS_CCV_DeptID']);
	unset($_SESSION['SESS_CCV_AuditorID']);
	unset($_SESSION['SESS_CCV_Verification']);
	unset($_SESSION['SESS_CCV_Status']);
	unset($_SESSION['SESS_CCV_Month']);
	unset($_SESSION['SESS_CCV_Year']);

?>