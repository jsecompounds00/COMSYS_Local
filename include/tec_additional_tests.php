
<table class="trials_child_tables_form">
	<tr>
		<th colspan="6">Additional Tests:</th>
	</tr>
	<tr>
		<td>
			<input type='checkbox' name='chkDynamicMilling' id='DM' <?php echo ( $BatchTicketID ? ( $db_DynamicMilling ? "checked" : "" ) : ( $initDynamicMilling_JRD ? "checked" : "" ) ); ?>>
				<label for='DM'>Dynamic Milling</label>
		</td>
		<td>
			<input type='checkbox' name='chkOilAging' id='OIA' <?php echo ( $BatchTicketID ? ( $db_OilAging ? "checked" : "" ) : ( $initOilAging_JRD ? "checked" : "" ) ); ?>>
				<label for='OIA'>Oil Aging</label>
		</td>
		<td>
			<input type='checkbox' name='chkOvenAging' id='OVA' <?php echo ( $BatchTicketID ? ( $db_OvenAging ? "checked" : "" ) : ( $initOvenAging_JRD ? "checked" : "" ) ); ?>>
				<label for='OVA'>Oven Aging</label>
		</td>
		<td>
			<input type='checkbox' name='chkStrandInspect' id='Strand' <?php echo ( $BatchTicketID ? ( $db_StrandInspection ? "checked" : "" ) : ( $initStrandInspect_JRD ? "checked" : "" ) ); ?>>
				<label for='Strand'>Strands Inspection</label>
		</td>
		<td>
			<input type='checkbox' name='chkPelletInspect' id='Pellet' <?php echo ( $BatchTicketID ? ( $db_PelletInspection ? "checked" : "" ) : ( $initPelletInspect_JRD ? "checked" : "" ) ); ?>>
				<label for='Pellet'>Pellet Inspection</label>
		</td>
	</tr>
	<tr>
		<td>
			<input type='checkbox' name='chkImpactTest' id='IT' <?php echo ( $BatchTicketID ? ( $db_ImpactTest ? "checked" : "" ) : ( $initImpactTest_JRD ? "checked" : "" ) ); ?>>
				<label for='IT'>Impact Test</label>
		</td>
		<td>
			<input type='checkbox' name='chkStaticHeating' id='SH' <?php echo ( $BatchTicketID ? ( $db_StaticHeating ? "checked" : "" ) : ( $initStaticHeating_JRD ? "checked" : "" ) ); ?>>
				<label for='SH'>Static Heating</label>
		</td>
		<td>
			<input type='checkbox' name='chkColorChange' id='ColorChange' <?php echo ( $BatchTicketID ? ( $db_ColorChangeTest ? "checked" : "" ) : ( $initColorChange_JRD ? "checked" : "" ) ); ?>>
				<label for='ColorChange'>Color Change Test</label>
		</td>
		<td>
			<input type='checkbox' name='chkWaterImmersion' id='WI' <?php echo ( $BatchTicketID ? ( $db_WaterImmersion ? "checked" : "" ) : ( $initWaterImmersion_JRD ? "checked" : "" ) ); ?>>
				<label for='WI'>Water Immersion</label>
		</td>
		<td>
			<input type='checkbox' name='chkColdTesting' id='CI' <?php echo ( $BatchTicketID ? ( $db_ColdInspection ? "checked" : "" ) : ( $initchkColdTesting_JRD ? "checked" : "" ) ); ?>>
				<label for='CI'>Cold Bend Test</label>
		</td>
	</tr>
</table>
<table class="comments_buttons">
	<tr>
		<td valign='top'>Remarks:</td>
		<td colspan=3>
			<textarea name='txtRemarks' class="paragraph"><?php
				if ( $BatchTicketID && $BatchTicketTrialID ){
					echo $remarks;
				}else{
					echo $initRemarks_JRD;
				}
			?></textarea>
		</td>
	</tr> 
</table>