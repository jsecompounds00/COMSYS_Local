<?php
// ########## ........
// 		$qryI = "SELECT id from comsys.nrm";
// 		$resultI = mysqli_query($db, $qryI);
// 		$processErrorI = mysqli_error($db);
// 		if(!empty($processErrorI))
// 		{
// 			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processErrorI.' near line 25.</td></tr>', 3, "errors.php");
// 			header("location: error_message.html");
// 		}else{
// 			$id = array();
// 			while($row = mysqli_fetch_assoc($resultI)){
// 				$id[] = $row['id'];
// 			}
// 			$db->next_result();
// 			$resultI->close();
// 		}

// 		$qryTN = "SELECT trial_no from comsys.nrm_trials WHERE nrm_id = $BatchTicketID";
// 		$resultTN = mysqli_query($db, $qryTN);
// 		$processErrorTN = mysqli_error($db);
// 		if(!empty($processErrorT))
// 		{
// 			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processErrorTN.' near line 25.</td></tr>', 3, "errors.php");
// 			header("location: error_message.html");
// 		}else{
// 			$tn = array();
// 			while($row = mysqli_fetch_assoc($resultTN)){
// 				$tn[] = $row['trial_no'];
// 			}
// 			$db->next_result();
// 			$resultTN->close();
// 		}
// ########## ........
########## Getting last inserted batch ticket no.
		$qryB = mysqli_prepare($db, "CALL sp_Batch_Ticket_Num( ? )");
		mysqli_stmt_bind_param($qryB, 'i', $BatchTicketID);
		$qryB->execute();
		$resultB = mysqli_stmt_get_result($qryB); 
		$processError1 = mysqli_error($db);

		if(!empty($processError1))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processError1.' near line 42.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultB))
			{
				$bat_num = $row['db_BatchTicketNumber'];
				
			}
			$db->next_result();
			$resultB->close();
		}
########## Getting last inserted batch ticket no.
########## Getting data from nrm table per $BatchTicketID
		$qryN = mysqli_prepare($db, "CALL sp_Batch_Ticket_Query( ? )");
		mysqli_stmt_bind_param($qryN, 'i', $BatchTicketID);
		$qryN->execute();
		$resultN = mysqli_stmt_get_result($qryN); 
		$processErrorN = mysqli_error($db);

		if ( !empty($processErrorN) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processErrorN.' near line 78.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultN)){
				$request_type = $row['request_type'];
				$activity_id = $row['activity_id'];
				$batch_ticket_no = $row['batch_ticket_no'];
				$batch_ticket_date = $row['batch_ticket_date'];
				$fg_soft = $row['fg_soft'];
				$finished_good_id = $row['finished_good_id'];
				$formula_type_id = $row['formula_type_id'];
				$hidBatCreatedAt = $row['created_at'];
				$hidBatCreatedID = $row['created_id'];
				$dynamic_milling = $row['dynamic_milling'];
				$oil_aging = $row['oil_aging'];
				$oven_aging = $row['oven_aging'];
				$strand_inspection = $row['strand_inspection'];
				$pellet_inspection = $row['pellet_inspection'];
				$impact_test = $row['impact_test'];
				$static_heating = $row['static_heating'];
				$color_change_test = $row['color_change_test'];
				$water_immersion = $row['water_immersion'];
				$cold_inspection = $row['cold_inspection'];
			}
			$db->next_result();
			$resultN->close();
		}

########## Getting data from nrm table per $BatchTicketID

########## Getting last inserted trial no. per nrm_id
		$qryT = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_Num( ?, ? )");
		mysqli_stmt_bind_param($qryT, 'ii', $BatchTicketID, $BatchTicketTrialID);
		$qryT->execute();
		$resultT = mysqli_stmt_get_result($qryT); 
		$processError2 = mysqli_error($db);

		if(!empty($processError2))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processError2.' near line 118.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultT))
			{
				$db_BatchTicketTrial = $row['db_BatchTicketTrial'];
			}
			$db->next_result();
			$resultT->close();
		}
########## Getting last inserted trial no. per nrm_id

########## Getting data from nrm table per $BatchTicketID
		// echo $BatchTicketID.' '.$nrmTrialId;
		$qryBT = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_Query( ?, ? )");
		mysqli_stmt_bind_param($qryBT, 'ii', $BatchTicketID, $BatchTicketTrialID);
		$qryBT->execute();
		$resultBT = mysqli_stmt_get_result($qryBT); 
		$processErrorBT = mysqli_error($db);

		if(!empty($processErrorBT))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processErrorBT.' near line 148.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			// $trialId = array();
			$trialItemId = array();
			$BTraw_material_type_id = array();
			$BTrm_id = array();
			$BTphr = array();
			$BTqty = array();
			while($row = mysqli_fetch_assoc($resultBT))
			{
				$trial_id = $row['trial_id'];
				$BTnrm_id = $row['nrm_id'];
				$BTjrd_id = $row['jrd_id'];
				$BTter_no = htmlspecialchars($row['ter_no']);
				$BTtrial_no = $row['trial_no'];
				$BTlot_no = $row['lot_no'];
				$BTtrial_date = $row['trial_date'];
				$BText_dar_no = $row['ext_dar_no'];
				$BText_dh = $row['ext_dh'];
				$BText_z1 = $row['ext_z1'];
				$BText_z2 = $row['ext_z2'];
				$BText_screw_speed = $row['ext_screw_speed'];
				$BText_cutter_speed = $row['ext_cutter_speed'];
				$BTmix_dar_no = $row['mix_dar_no'];
				$BTmix_parameter1 = $row['mix_parameter1'];
				$BTmix_parameter2 = $row['mix_parameter2'];
				$BTmix_parameter3 = $row['mix_parameter3'];
				$BTmix_parameter4 = $row['mix_parameter4'];
				$BTmix_parameter5 = $row['mix_parameter5'];
				$BTmix_sequence1 = $row['mix_sequence1'];
				$BTmix_sequence2 = $row['mix_sequence2'];
				$BTmix_sequence3 = $row['mix_sequence3'];
				$BTmix_sequence4 = $row['mix_sequence4'];
				$BTmix_sequence5 = $row['mix_sequence5'];
				$BTmultiplier = $row['multiplier'];
				$BTremarks = htmlspecialchars($row['remarks']);
				// $remarks_array = explode('<br>', $BTremarks);
				$bat_trial_created_at = $row['created_at'];
				$bat_trial_created_id = $row['created_id'];

				$trialItemId[] = $row['trial_item_id'];	

				$BTraw_material_type_id[] = $row['raw_material_type_id'];	

				$BTrm_id[] = $row['raw_material_id'];	

				$BTrm[] = htmlspecialchars($row['raw_material']);	

				$BTphr[] = $row['phr'];	

				$BTqty[] = $row['quantity'];	
			}
			$db->next_result();
			$resultBT->close();
		}

########## Getting data for Test Evaluation
		// $qryTER = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_for_TestingIDs_Query( ? )");
		// mysqli_stmt_bind_param($qryTER, 'i', $bat_trial_id);
		// $qryTER->execute();
		// $resultTER = mysqli_stmt_get_result($qryTER);
		// $processErrorTER = mysqli_error($db);

		// if ( !empty($processErrorTER) ){
		// 	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processErrorTER.' near line 212.</td></tr>', 3, "errors.php");
		// 	header("location: error_message.html");
		// }else{
		// 	while($row = mysqli_fetch_assoc($resultTER)){
		// 		$complete_testing_id = $row['complete_testing_id'];
		// 		$dynamic_milling_id = $row['dynamic_milling_id'];
		// 		$water_immersion_id = $row['water_immersion_id'];
		// 		$strand_inspect_id = $row['strand_inspect_id'];
		// 		$pellet_inspect_id = $row['pellet_inspect_id'];
		// 		$impact_test_id = $row['impact_test_id'];
		// 		$static_heat_id = $row['static_heat_id'];
		// 		$color_change_id = $row['color_change_id'];
		// 	}
		// 	$db->next_result();
		// 	$resultTER->close();
		// }

?>