<?php

$customer_id = intval($_GET['sltCustomer']);
$sltInvoiceNum = strval($_GET['sltInvoiceNum']);
$hidMemoId = intval($_GET['hidMemoId']);
$hidProcessType = strval($_GET['hidProcessType']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>invoice_number_dropdown.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		if ( $hidProcessType == 'sub' ){
			$qryM = mysqli_prepare($db, "CALL sp_Credit_Memo_Query( ? )");
		}else{
			$qryM = mysqli_prepare($db, "CALL sp_Debit_Memo_Query( ? )");
		}
			mysqli_stmt_bind_param($qryM, 'i', $hidMemoId);
			$qryM->execute();
			$resultM = mysqli_stmt_get_result($qryM);
			$processErrorM = mysqli_error($db);

			if ( !empty($processErrorM) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>invoice_number_dropdown.php'.'</td><td>'.$processErrorM.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultM)){
					$amount = $row['amount'];
				}
				$db->next_result();
				$resultM->close();
			}


		$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_Dropdown(?, ?)");
		mysqli_stmt_bind_param($qry, 'ii', $customer_id, $hidMemoId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>invoice_number_dropdown.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$invoice_number = $row['invoice_number'];
				$InvBalance = $row['InvBalance'];
				
				if ( $sltInvoiceNum == $invoice_number){
					if ( $hidProcessType == 'sub' ){
						echo "<option value='".$invoice_number."-".($InvBalance+$amount)."'selected>".$invoice_number." - ".number_format((float)($InvBalance+$amount), 2, '.', ',')." A/R</option>";
					}elseif ( $hidProcessType == 'add' ){
						echo "<option value='".$invoice_number."-".($InvBalance-$amount)."'selected>".$invoice_number." - ".number_format((float)($InvBalance-$amount), 2, '.', ',')." A/R</option>";
					}
					
				}else{
					echo "<option value='".$invoice_number."-".$InvBalance."'>".$invoice_number." - ".number_format((float)($InvBalance), 2, '.', ',')." A/R</option>";
				}
				
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>