<colgroup> <col width="200px"></col> </colgroup>
<tr class="spacing">
	<td><b>Rated Capacity</b></td>
</tr>
<?php

$machine_type = strval($_GET['machine_type']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>machine_dropdown.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		if( $machine_type == 'M' ){
			$qry = "CALL sp_Mixer_Dropdown()";
		}elseif( $machine_type == 'E' ){
			$qry = "CALL sp_Extruder_Dropdown()";
		}

		$result = mysqli_query($db, $qry);
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>machine_dropdown.php'.'</td><td>'.$processError.' near line 26.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result)){
?>
				<tr>
					<td>
						<?php echo $row['name'];?>
						<input type='hidden' name='hidLineComponentCapacityID[]' value='0'>
						<input type='hidden' name='hidMachineID[]' value='<?php echo $row['id'];?>'>
					</td>
					<td>
						<input type='text' name='txtRatedCapacity[]' value=''>
					</td>
				</tr>
<?php
			}
			$db->next_result();
			$result->close();
		}
	}

	require("database_close.php");
?>