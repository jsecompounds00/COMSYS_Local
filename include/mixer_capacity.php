<?php
	$mixer_id = intval($_GET['mixer_id']);

	require("database_connect.php");

	$qry = mysqli_prepare($db, "CALL sp_Mixers_Query( ? )");
	mysqli_stmt_bind_param($qry, 'i', $mixer_id);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);

	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_mixers.php'.'</td><td>'.$processError.' near line 37.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		while($row = mysqli_fetch_assoc($result))
		{
			$capacity = $row['capacity'];

			echo '<b>'.$capacity.'</b>';
		}
	}
	$db->next_result();
	$result->close();

	require("database_close.php");
?>