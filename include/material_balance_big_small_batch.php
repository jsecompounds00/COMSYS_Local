<?php

$batch_id = strval($_GET['batch_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_big_small_batch.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_Issued_Formula_Details_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 'i', $batch_id);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_big_small_batch.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result))
			{
				$RMBigQty = $row["RMBigQty"];
				$BigBatch = $row["BigBatch"];
				$RMSmallQty = $row["RMSmallQty"];
				$SmallBatch = $row["SmallBatch"];
				$RMQtyPerBatchBig = $row["RMQtyPerBatchBig"];
				$RMQtyPerBatchSmall = $row["RMQtyPerBatchSmall"];
			}
			$db->next_result();
			$result->close();

			if ( $batch_id != 0 ){
?>
				<tr>
					<td colspan="3" class="border_right"> <b> Big Batch: </b> </td>

					<td colspan="3"> <b> Small Batch: </b> </td>
				</tr>

				<tr>
					<td> <dd> No. of Batches: </dd> </td>

					<td colspan="2" class="border_right"> 
						<dd> 
							<label class="constant"> <?php echo $BigBatch; ?> </b> 
							<input type="hidden" id="hidBigBatch" value="<?php echo ( is_null( $BigBatch ) ? 0 : $BigBatch );?>">
						</dd> 
					</td>

					<td> <dd> No. of Batches: </dd> </td>

					<td colspan="2"> 
						<dd> 
							<label class="constant"> <?php echo $SmallBatch; ?> </b> 
							<input type="hidden" id="hidSmallBatch" value="<?php echo ( is_null( $SmallBatch ) ? 0 : $SmallBatch );?>">
						</dd> 
					</td>
				</tr>

				<tr>
					<td class="border_bottom"> <dd> Batch Size (kgs): </dd> </td>

					<td class="border_bottom"> 
						<dd> 
							<label class="constant"> <?php echo $RMQtyPerBatchBig; ?> </b> 
							<input type="hidden" id="hidRMQtyPerBatchBig" value="<?php echo ( is_null( $RMQtyPerBatchBig ) ? 0 : $RMQtyPerBatchBig );?>">
						</dd> 
					</td>

					<td class="border_right border_bottom"> <dd> <label class="constant_total"> = <?php echo $RMBigQty; ?> </label> </dd> </td>

					<td class="border_bottom"> <dd> Batch Size (kgs): </dd> </td>

					<td colspan="2" class="border_bottom"> 
						<dd> 
							<label class="constant"> <?php echo $RMQtyPerBatchSmall; ?> </b> 
							<input type="hidden" id="hidRMQtyPerBatchSmall" value="<?php echo ( is_null( $RMQtyPerBatchSmall ) ? 0 : $RMQtyPerBatchSmall );?>">
						</dd> 
					</td>
				</tr>
<?php
			}
		}
	}
	require("database_close.php");
?>