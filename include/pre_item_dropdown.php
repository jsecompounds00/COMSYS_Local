<?php

	$id = intval($_GET['id']);
	$Division = strval($_GET['Division']);
	
	require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pre_item_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		// if ( $id == 99 ) {
		// 	$qry = mysqli_prepare($db, "CALL sp_RawMaterials_Dropdown(?)");
		// 	mysqli_stmt_bind_param($qry, 's', $Division);
		// }else{
		// 	$qry = mysqli_prepare($db, "CALL sp_Suppliesa_Dropdown(?, ?)");
		// 	mysqli_stmt_bind_param($qry, 'is', $id, $Division);
		// }
		if ( $id == 99 ) {
			if ( $Division == "PPR" ){ //|| $Division == "Pipes" 
				$qry = mysqli_prepare($db, "CALL sp_RawMaterials_PICSYS_Dropdown(?)");
				mysqli_stmt_bind_param($qry, 's', $Division);
			}else{
				$qry = mysqli_prepare($db, "CALL sp_RawMaterials_Dropdown(?)");
				mysqli_stmt_bind_param($qry, 's', $Division);
			}
		}else{
			$qry = mysqli_prepare($db, "CALL sp_Suppliesa_Dropdown(?, ?)");
			mysqli_stmt_bind_param($qry, 'is', $id, $Division);
		}
		
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pre_item_dropdown.php'.'</td><td>'.$processError.' near line 26.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$supplyId = $row['id'];
				$supply = $row['name'];

				if ( $id == 99 ) {
					echo "<option value='".$supplyId.":r'>".$supply."</option>";
				}else{
					echo "<option value='".$supplyId.":s'>".$supply."</option>";
				}
				
				
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?> 