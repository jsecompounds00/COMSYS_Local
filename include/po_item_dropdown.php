<?php
	$preIds = strval($_GET['implodedPreIds']);
	$poId = intval($_GET['poId']);
	$item_type = strval($_GET['item_type']);
	$Division = strval($_GET['Division']);
	
	$length = strlen($preIds); 

	$preId = array();
	$preId = explode(',', $preIds);
	$preId = array_unique($preId);

	require("database_connect.php");

	$i=0;

	if ( $length != 0 ){
		foreach ($preId as $key => $value) {
			
			if ( $poId ){
				if ( $item_type == 'r' ){
					if ( $Division == "PPR" ){ // || $Division == "Pipes"
						$qry = mysqli_prepare($db, "CALL sp_POItemB_RM_PICSYS_Dropdown(?, ?, 0, 1)");
						mysqli_stmt_bind_param( $qry, 'ii', $poId, $value );
					}else{
						$qry = mysqli_prepare($db, "CALL sp_POItemB_RM_Dropdown(?, ?, 0, 1)");
						mysqli_stmt_bind_param( $qry, 'ii', $poId, $value );
					}
				}elseif ( $item_type == 's' ){
					$qry = mysqli_prepare($db, "CALL sp_POItemB_Dropdown(?, ?, 0, 1)");
					mysqli_stmt_bind_param( $qry, 'ii', $poId, $value );
				}
			}else{
				if ( $item_type == 'r' ){
					if ( $Division == "PPR" ){ // || $Division == "Pipes"
						$qry = mysqli_prepare($db, "CALL sp_POItem_RM_PICSYS_Dropdown(?, 0, 0)");
						mysqli_stmt_bind_param( $qry, 'i', $value );
					}else{
						$qry = mysqli_prepare($db, "CALL sp_POItem_RM_Dropdown(?, 0, 0)");
						mysqli_stmt_bind_param( $qry, 'i', $value );
					}
				}elseif ( $item_type == 's' ){
					$qry = mysqli_prepare($db, "CALL sp_POItem_Dropdown(?, 0, 0)");
					mysqli_stmt_bind_param( $qry, 'i', $value );
				}
			}

			$qry->execute();
			$result = mysqli_stmt_get_result( $qry );
			$processError = mysqli_error($db);

			if ($processError){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>po_item_dropdown.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{


				while($row = mysqli_fetch_assoc($result))
				{
					$PREId = $row['PREId'];
					$PREItemId = $row['PREItemId'];
					$PRENumber = $row['PRENumber'];
					$Quantity = $row['Quantity'];
					$UOM = $row['UOM'];
					$ItemDescription = $row['ItemDescription'];
					$UOMId = $row['UOMId'];
					$SupplyId = $row['SupplyId'];
					if ( $poId ){
						$ReasonCancelled = $row['ReasonCancelled'];
						$UnitPrice = $row['UnitPrice'];
						$POItemId = $row['POItemId'];
						$ManualServed = $row['ManualServed'];
						$TagCancelled = $row['TagCancelled'];
					}

					if ( $poId ){
?>
						<tr>
							<?php
								if ( $TagCancelled ){
							?>
									<td>
										<input type='checkbox' checked disabled>
										<input type='hidden' name='chkCanceled[<?php echo $i;?>]' id='chkCanceled<?php echo $i;?>' value='1'>
									</td>
									<?php
										if ( $ManualServed ){
									?>
											<td>
												<input type='checkbox' name='chkServed[<?php echo $i;?>]' id='chkServed<?php echo $i;?>' value='1' checked disabled>
											</td>
									<?php
										}else{
									?>
											<td>
												<input type='checkbox' name='chkServed[<?php echo $i;?>]' id='chkServed<?php echo $i;?>' value='1' disabled>
											</td>
							<?php
										}
								}
								else{
							?>
									<td>
										<input type='checkbox' name='chkCanceled[<?php echo $i;?>]' id='chkCanceled<?php echo $i;?>' value='1'>
									</td>
									<?php
										if ( $ManualServed ){
									?>
											<td>
												<input type='checkbox' name='chkServed[<?php echo $i;?>]' id='chkServed<?php echo $i;?>' value='1' checked> 
											</td>
									<?php
										}else{
									?>
											<td>
												<input type='checkbox' name='chkServed[<?php echo $i;?>]' id='chkServed<?php echo $i;?>' value='1'>
											</td>
							<?php
										}
								}
							?>
							<td>
								<input type='hidden' name='hidPOItemId[]' id='hidPOItemId<?php echo $i;?>' value='<?php echo $POItemId;?>'>
								<input type='hidden' name='hidPREId[]' id='hidPREId<?php echo $i;?>' value='<?php echo $PREId;?>'>
								<input type='hidden' name='hidPREItemId[]' id='hidPREItemId<?php echo $i;?>' value='<?php echo $PREItemId;?>'>
								<?php echo $PRENumber;?>
							</td>

							<td>
								<input type='checkbox' name='chkInclude[]' id='chkInclude<?php echo $i;?>' value='<?php echo $i;?>' 
									onchange='include(), autoProduct(), autoAdd()' checked disabled>
							</td>

							<td>
								<input type='text' name='hidQuantity[]' id='hidQuantity<?php echo $i;?>' value='<?php echo $Quantity;?>' 
									onkeyup='autoProduct(), autoAdd()' 
									onblur='autoProduct(), autoAdd()' 
									onchange='autoProduct(), autoAdd()'>
							</td>

							<td>
								<input type='hidden' name='hidUOMId[]' id='hidUOMId<?php echo $i;?>' value='<?php echo $UOMId;?>'>
								<?php echo $UOM;?>
							</td>

							<td>
								<input type='hidden' name='hidSupplyId[]' id='hidSupplyId<?php echo $i;?>' value='<?php echo $SupplyId;?>'>
								<?php echo $ItemDescription;?>
							</td>

							<td>
								<input type='text' name='txtUnitPrice[]' id='txtUnitPrice<?php echo $i;?>' value='<?php echo $UnitPrice;?>' 
									onkeyup='autoProduct(), autoAdd()' 
									onblur='autoProduct(), autoAdd()' 
									onchange='autoProduct(), autoAdd()'>
							</td>

							<td>
								<input type='text' name='txtTotalAmount[]' id='txtTotalAmount<?php echo $i;?>' readonly 
									value='<?php echo number_format((float)($Quantity*$UnitPrice), 4, '.', '');?>'>
							</td>

							<td>
								<input type='text' name='txtReason[]' id='txtReason<?php echo $i;?>' value='<?php echo $ReasonCancelled;?>'>
							</td>
						</tr>
				<?php
					}else{
				?>
						<tr>
							<td>
								<input type='hidden' name='hidPOItemId[]' id='hidPOItemId<?php echo $i;?>' value='0'>
								<input type='hidden' name='hidPREId[]' id='hidPREId<?php echo $i;?>' value='<?php echo $PREId;?>' disabled>
								<input type='hidden' name='hidPREItemId[]' id='hidPREItemId<?php echo $i;?>' value='<?php echo $PREItemId;?>' disabled>
								<?php echo $PRENumber;?>
							</td>

							<td>
								<input type='checkbox' name='chkInclude[]' id='chkInclude<?php echo $i;?>' value='<?php echo $i;?>' 
									onchange='include(), autoProduct(), autoAdd()'>
							</td>

							<td>
								<input type='text' name='hidQuantity[]' id='hidQuantity<?php echo $i;?>' value='<?php echo $Quantity;?>' disabled
									onkeyup='autoProduct(), autoAdd()' 
									onblur='autoProduct(), autoAdd()'
									onchange='autoProduct(), autoAdd()'>
							</td>

							<td>
								<input type='hidden' name='hidUOMId[]' id='hidUOMId<?php echo $i;?>' value='<?php echo $UOMId;?>' disabled>
								<?php echo $UOM;?>
							</td>

							<td>
								<input type='hidden' name='hidSupplyId[]' id='hidSupplyId<?php echo $i;?>' value='<?php echo $SupplyId;?>' disabled>
								<?php echo $ItemDescription;?>
							</td>

							<td>
								<input type='text' name='txtUnitPrice[]' id='txtUnitPrice<?php echo $i;?>' disabled 
									onkeyup='autoProduct(), autoAdd()' 
									onblur='autoProduct(), autoAdd()' 
									onchange='autoProduct(), autoAdd()'>
							</td>

							<td>
								<input type='text' name='txtTotalAmount[]' id='txtTotalAmount<?php echo $i;?>' disabled>
							</td>
						</tr>
<?php
					}

					$i++;
				} // while

				$db->next_result();
				$result->close();

			} // if process error

		} // foreach

	} // if length
?>
	<tr class='spacing'>
		<?php
			if ( $poId ){
		?>
				<td colspan='8' class='total_label'>
		<?php
			}else{
		?>
				<td colspan='6' class='total_label'>
		<?php
			}
		?>
					Total:
				</td>
		<td>
			<input type='text' name='txtTotalPrice' id='txtTotalPrice' readonly>
		</td>
	</tr>
<?php
	require("database_close.php");

?>