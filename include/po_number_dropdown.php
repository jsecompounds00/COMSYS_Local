<?php

	$item_type = strval($_GET['item_type']);
	$Division = strval($_GET['Division']);
	$poId = intval($_GET['poId']);
	$hidPREIDs = strval($_GET['hidPREIDs']);

	$preId_array = array();
	$preId_array = explode(',', $hidPREIDs);
	$preId_array = array_unique($preId_array);

	require("database_connect.php");

	$i=0;

	
	if (!empty($errno)){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>po_item_dropdown.php'.'</td><td>'.$error.' near line 14.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		if ( $poId ){
			$qryPRE = mysqli_prepare($db, "CALL sp_PRE_Dropdown( 1, ?, ? )");
		}else{
			$qryPRE = mysqli_prepare($db, "CALL sp_PRE_Dropdown( 0, ?, ? )");
		}
		mysqli_stmt_bind_param($qryPRE, 'ss', $item_type, $Division);
		$qryPRE->execute();
		$resultPRE = mysqli_stmt_get_result($qryPRE); //return results of query
		$processErrorPRE = mysqli_error($db);

		if ( !empty($processErrorPRE) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_po.php'.'</td><td>'.$processErrorPRE.' near line 22.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultPRE))
			{
				$db_preId = $row["preId"];
				$db_pre_number = $row["pre_number"];

				if ( is_null($hidPREIDs) ){
					echo "<option value='".$db_preId."'>".$db_pre_number."</option>";
				}else{
					if ( in_array( $db_preId, $preId_array ) ) {
						echo "<option value='".$db_preId."' selected>".$db_pre_number."</option>";
					}
					else{
						if ( !$poId ){
							echo "<option value='".$db_preId."'>".$db_pre_number."</option>";
						}
					}
				}
			}
			$db->next_result();
			$resultPRE->close();
		}
	}

	require("database_close.php");

?>