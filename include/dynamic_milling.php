<?php

	require("database_connect.php");

	$qryDM = mysqli_prepare($db, "CALL sp_Batch_Ticket_Dynamic_Milling_Query( ? )");
	mysqli_stmt_bind_param($qryDM, 'i', $TrialID);
	$qryDM->execute();
	$resultDM = mysqli_stmt_get_result($qryDM);
	$processErrorDM = mysqli_error($db);

	if ( !empty($processErrorDM) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dynamic_milling.php'.'</td><td>'.$processErrorDM.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultDM)){
			$dm_color_conformance = $row['color_conformance'];
			$dm_heat_stability = $row['heat_stability'];
			$dm_processability = $row['processability'];
			$DMcreated_at = $row['created_at'];
			$DMcreated_id = $row['created_id'];
			$sampling = $row['sampling'];
		}
		$db->next_result();
		$resultDM->close();
	}
?>
	
	<table class="results_child_tables_form">
		<col width="250"></col>
		<tr></tr>
		<input type='hidden' name='hidDynamicMillingID' value='<?php echo $dynamic_milling_id; ?>'>
		<tr>
			<td> Sampling: </td>
			<td>
				Every &nbsp;
				<input type="text" size="2" name="txtSampling" value='<?php echo ( $dynamic_milling_id ? $sampling : $initSampling_BT ); ?>'> &nbsp;
				minutes
			</td>
		</tr>
		<tr>
			<td>Color Conformance:</td>
			<td>
				<input type='radio' name='radDMColor' id='EDMColor' value='Excellent' <?php echo ( $dynamic_milling_id ? ( $dm_color_conformance == "Excellent" ? "checked" : "" ) : ( $initDMColor_BT == "Excellent" ? "checked" : "" ) );?>> 
					<label for='EDMColor'> Excellent </label>

				<input type='radio' name='radDMColor' id='GDMColor' value='Good' <?php echo ( $dynamic_milling_id ? ( $dm_color_conformance == "Good" ? "checked" : "" ) : ( $initDMColor_BT == "Good" ? "checked" : "" ) );?>> 
					<label for='GDMColor'> Good </label>

				<input type='radio' name='radDMColor' id='PDMColor' value='Poor' <?php echo ( $dynamic_milling_id ? ( $dm_color_conformance == "Poor" ? "checked" : "" ) : ( $initDMColor_BT == "Poor" ? "checked" : "" ) );?>> 
					<label for='PDMColor'> Poor </label>
			</td>
		</tr>
		<tr>
			<td>Heat Stability:</td>
			<td>
				<input type='radio' name='radDMHeat' id='EDMHeat' value='Excellent' <?php echo ( $dynamic_milling_id ? ( $dm_heat_stability == "Excellent" ? "checked" : "" ) : ( $initDMHeat_BT == "Excellent" ? "checked" : "" ) );?>> 
					<label for='EDMHeat'> Excellent </label>

				<input type='radio' name='radDMHeat' id='GDMHeat' value='Good' <?php echo ( $dynamic_milling_id ? ( $dm_heat_stability == "Good" ? "checked" : "" ) : ( $initDMHeat_BT == "Good" ? "checked" : "" ) );?>> 
					<label for='GDMHeat'> Good </label>

				<input type='radio' name='radDMHeat' id='PDMHeat' value='Poor' <?php echo ( $dynamic_milling_id ? ( $dm_heat_stability == "Poor" ? "checked" : "" ) : ( $initDMHeat_BT == "Poor" ? "checked" : "" ) );?>> 
					<label for='PDMHeat'> Poor </label>
			</td>
		</tr>
		<tr>
			<td>Processability:</td>
			<td>
				<input type='radio' name='radDMProcess' id='EDMProcess' value='Excellent' <?php echo ( $dynamic_milling_id ? ( $dm_processability == "Excellent" ? "checked" : "" ) : ( $initDMProcess_BT == "Excellent" ? "checked" : "" ) );?>> 
					<label for='EDMProcess'> Excellent </label>

				<input type='radio' name='radDMProcess' id='GDMProcess' value='Good' <?php echo ( $dynamic_milling_id ? ( $dm_processability == "Good" ? "checked" : "" ) : ( $initDMProcess_BT == "Good" ? "checked" : "" ) );?>> 
					<label for='GDMProcess'> Good </label>

				<input type='radio' name='radDMProcess' id='PDMProcess' value='Poor' <?php echo ( $dynamic_milling_id ? ( $dm_processability == "Poor" ? "checked" : "" ) : ( $initDMProcess_BT == "Poor" ? "checked" : "" ) );?>> 
					<label for='PDMProcess'> Poor </label>
			</td>
		</tr>
	</table>

<?php		
	
	if ( $dynamic_milling_id ){
		echo "<input type='hidden' name='hidDMCreatedAt' value='".$DMcreated_at."'>";
		echo "<input type='hidden' name='hidDMCreatedId' value='".$DMcreated_id."'>";	
	}else{
		echo "<input type='hidden' name='hidDMCreatedAt' value='".date('Y-m-d H:i:s')."'>";
		echo "<input type='hidden' name='hidDMCreatedId' value=''>";	
	}

	require("database_close.php");
?>