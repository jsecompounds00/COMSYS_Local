<?php

$id = intval($_GET['deptid']);
$dept_value = intval($_GET['dept']);
// echo $id;
require("database_connect.php");

if(!empty($errno))
{
	$error = mysqli_connect_error();
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dept_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}
else
{

	if ( $id==1 )
	{
		$qry = "SELECT * FROM departments ORDER BY code";
		$result = mysqli_query($db, $qry);
		$processError = mysqli_error($db);
	
		if ($processErrorf){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dept_dropdown.php'.'</td><td>'.$processErrorf.' near line 24.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "<option value='0'></option>";

			while($row = mysqli_fetch_assoc($result)){
				$dept_id = $row['id'];
				if ( $dept_value ){	
					if ( $dept_value==$dept_id )
					echo "<option value='".$dept_id."' selected>".$row['code']."</option>";
				else echo "<option value='".$dept_id."'>".$row['code']."</option>";
				}else{
					echo "<option value='".$dept_id."'>".$row['code']."</option>";
				}
			}
		}

		$db->next_result();
		$result->close();
	}elseif ( $id==2 )
	{
		echo "<option value=0></option>";
		if ( $dept_value ){
			echo "<option value=99999 selected>N/A</option>";
		}else{
			echo "<option value=99999>N/A</option>";
		}
	}
}
require("database_close.php");
?> 