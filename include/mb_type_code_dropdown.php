<?php

$mb_id = intval($_GET['mb_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>machine_dropdown.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		
		$qry = mysqli_prepare($db, "CALL sp_Master_Batch_Type_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 'i', $mb_id);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>machine_dropdown.php'.'</td><td>'.$processError.' near line 24.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result)){
				$MBType = $row['MBType'];
				$MBCode = $row['MBCode'];
				$MBTypeID = $row['MBTypeID'];

				echo $MBType.' '.$MBCode.': ';
				echo "<input type='hidden' name='hidMBTypeID' value='".$MBTypeID."'>";
			}
			$db->next_result();
			$result->close();
		}
	}

	require("database_close.php");
?>