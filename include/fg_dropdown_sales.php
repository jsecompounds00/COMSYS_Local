<?php

$hidSalesID = intval($_GET['hidSalesID']);
$fg_id = intval($_GET['fg_id']);
$sltCustomer = intval($_GET['sltCustomer']);
$sltItemType = strval($_GET['sltItemType']);
$index = intval($_GET['index']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown_sales.php'.'</td><td>'.$error.' near line 14.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qry = mysqli_prepare($db, "CALL sp_Sales_FG_Dropdown( ?, ? )");
		mysqli_stmt_bind_param($qry, 'si', $sltItemType, $sltCustomer);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown_sales.php'.'</td><td>'.$processError.' near line 25.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			$i = 0;
			echo "<select name='sltProduct[]' id='sltProduct$index'>";
			if(!$hidSalesID)
				echo "<option></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				
				$FgId = $row['FGID'];
				$Fg = $row['FGItem'];

				if($hidSalesID){
					if ($FgId == $fg_id){
						echo "<option value=".$FgId." selected>".$Fg."</option>";
					}
				}else{
					if ($FgId == $fg_id){
						echo "<option value=".$FgId." selected>".$Fg."</option>";
					}else {
						echo "<option value=".$FgId.">".$Fg."</option>";
					}
				}
				$i++;
			}
			echo "</select>";
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>