<?php

	require("database_connect.php");

	$qrySH = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Static_Heat_Query( ?, ?, ? )");
	mysqli_stmt_bind_param($qrySH, 'isi', $JRDNoReplicateID, $product_for_evaluation, $FGID);
	$qrySH->execute();
	$resultSH = mysqli_stmt_get_result($qrySH);
	$processErrorSH = mysqli_error($db);

	if ( !empty($processErrorSH) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_static_heating.php'.'</td><td>'.$processErrorSH.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultSH)){
			$sh_color_conformance = $row['color_conformance'];
			$sh_heat_stability = $row['heat_stability'];
			$SHcreated_at = $row['created_at'];
			$SHcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultSH->close();
	}

?>
	
	<table class='results_child_tables_form'>
		<col width='250'></col>
		<tr></tr>
		<tr>
			<td>Color Conformance:</td>
			<td>
				<input type='radio' name='radSHColor' id='ESHColor' value='Excellent' <?php echo ( $static_heat_id ? ( $sh_color_conformance == 'Excellent' ? "checked" : "" ) : ( $initNRSHColor == 'Excellent' ? "checked" : "" ) ); ?>> 
					<label for='ESHColor'> Excellent </label>

				<input type='radio' name='radSHColor' id='GSHColor' value='Good' <?php echo ( $static_heat_id ? ( $sh_color_conformance == 'Good' ? "checked" : "" ) : ( $initNRSHColor == 'Good' ? "checked" : "" ) ); ?>> 
					<label for='GSHColor'> Good </label>

				<input type='radio' name='radSHColor' id='PSHColor' value='Poor' <?php echo ( $static_heat_id ? ( $sh_color_conformance == 'Poor' ? "checked" : "" ) : ( $initNRSHColor == 'Poor' ? "checked" : "" ) ); ?>> 
					<label for='PSHColor'> Poor </label>
			</td>
		</tr>
		<tr>
			<td>Heat Stability:</td>
			<td>
				<input type='radio' name='radSHHeat' id='ESHHeat' value='Excellent' <?php echo ( $static_heat_id ? ( $sh_heat_stability == 'Excellent' ? "checked" : "" ) : ( $initNdSHHeat == 'Excellent' ? "checked" : "" ) ); ?>> 
					<label for='ESHHeat'> Excellent </label>

				<input type='radio' name='radSHHeat' id='GSHHeat' value='Good' <?php echo ( $static_heat_id ? ( $sh_heat_stability == 'Good' ? "checked" : "" ) : ( $initNdSHHeat == 'Good' ? "checked" : "" ) ); ?>> 
					<label for='GSHHeat'> Good </label>

				<input type='radio' name='radSHHeat' id='PSHHeat' value='Poor' <?php echo ( $static_heat_id ? ( $sh_heat_stability == 'Poor' ? "checked" : "" ) : ( $initNdSHHeat == 'Poor' ? "checked" : "" ) ); ?>> 
					<label for='PSHHeat'> Poor </label>
			</td>
		</tr>
		</tr>
		<input type='hidden' name='hidSHCreatedAt' value='<?php echo ( $static_heat_id ? $SHcreated_at : date('Y-m-d H:i:s') );?>'>
		<input type='hidden' name='hidSHCreatedId' value='<?php echo ( $static_heat_id ? $SHcreated_id : 0 );?>'>
	</table>
<?php	 require("database_close.php"); ?>