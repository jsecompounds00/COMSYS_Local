<?php

	require("database_connect.php");

	$qryCI = mysqli_prepare($db, "CALL sp_Batch_Ticket_Cold_Inspection_Query( ? )");
	mysqli_stmt_bind_param($qryCI, 'i', $TrialID);
	$qryCI->execute();
	$resultCI = mysqli_stmt_get_result($qryCI);
	$processErrorCI = mysqli_error($db);

	if ( !empty($processErrorCI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>cold_inspection.php'.'</td><td>'.$processErrorCI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultCI)){
			$ci_result_specimen1 = $row['result_specimen1'];
			$ci_result_specimen2 = $row['result_specimen2'];
			$ci_result_specimen3 = $row['result_specimen3'];
			$ci_crack_specimen1 = $row['crack_specimen1'];
			$ci_crack_specimen2 = $row['crack_specimen2'];
			$ci_crack_specimen3 = $row['crack_specimen3'];
			$CIcreated_at = $row['created_at'];
			$CIcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultCI->close();
	}

?>
	
	<table class="results_child_tables_form">
		<col width="250"></col>
		<tr></tr>
		<input type='hidden' name='hidColdInspectionID' value='<?php echo $cold_inspection_id; ?>'>
		<tr>
			<th></th>
			<th>Result</th>
			<th>Cracked At <i> (if failed) </i> </th>
		</tr>
		<tr>
			<td>Specimen 1</td>
			<td>
				<select name="txtCIResult1">
					<option value='P' <?php echo ( $cold_inspection_id ? ( $ci_result_specimen1 == "P" ? "selected" : "" ) : ( $initCIResult1_BT == "P" ? "selected" : "" ) );?>> No Crack </option>
					<option value='F' <?php echo ( $cold_inspection_id ? ( $ci_result_specimen1 == "F" ? "selected" : "" ) : ( $initCIResult1_BT == "F" ? "selected" : "" ) );?>> Failed </option>
				</select>
			</td>
			<td>
				<input type="text" name="txtCICracked1" value="<?php echo ( $cold_inspection_id ? $ci_crack_specimen1 : $initCICracked1_BT );?>">
			</td>
		</tr>
		<tr>
			<td>Specimen 2</td>
			<td>
				<select name="txtCIResult2">
					<option value='P' <?php echo ( $cold_inspection_id ? ( $ci_result_specimen2 == "P" ? "selected" : "" ) : ( $initCIResult2_BT == "P" ? "selected" : "" ) );?>> No Crack </option>
					<option value='F' <?php echo ( $cold_inspection_id ? ( $ci_result_specimen2 == "F" ? "selected" : "" ) : ( $initCIResult2_BT == "F" ? "selected" : "" ) );?>> Failed </option>
				</select>
			</td>
			<td>
				<input type="text" name="txtCICracked2" value="<?php echo ( $cold_inspection_id ? $ci_crack_specimen2 : $initCICracked2_BT );?>">
			</td>
		</tr>
		<tr>
			<td>Specimen 3</td>
			<td>
				<select name="txtCIResult3">
					<option value='P' <?php echo ( $cold_inspection_id ? ( $ci_result_specimen3 == "P" ? "selected" : "" ) : ( $initCIResult3_BT == "P" ? "selected" : "" ) );?>> No Crack </option>
					<option value='F' <?php echo ( $cold_inspection_id ? ( $ci_result_specimen3 == "F" ? "selected" : "" ) : ( $initCIResult3_BT == "F" ? "selected" : "" ) );?>> Failed </option>
				</select>
			</td>
			<td>
				<input type="text" name="txtCICracked3" value="<?php echo ( $cold_inspection_id ? $ci_crack_specimen3 : $initCICracked3_BT );?>">
			</td>
		</tr>
	</table>

<?php			
	
	if ( $cold_inspection_id ){
		echo "<input type='hidden' name='hidCICreatedAt' value='".$CIcreated_at."'>";
		echo "<input type='hidden' name='hidCICreatedId' value='".$CIcreated_id."'>";	
	}else{
		echo "<input type='hidden' name='hidCICreatedAt' value='".date('Y-m-d H:i:s')."'>";
		echo "<input type='hidden' name='hidCICreatedId' value=''>";	
	}

	require("database_close.php");
?>