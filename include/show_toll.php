<?php
$matrix_id = intval($_GET['matrix_id']);
$index = intval($_GET['index']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>show_toll.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = "SELECT toll_fee from toll_matrix where id=$matrix_id";
		$result = mysqli_query($db, $qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>show_toll.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result))
			{
				$toll_fee = $row['toll_fee'];

				echo "<input type='hidden' name='txtToll[]' id='txtToll".$index."' value='".$toll_fee."' readonly>";
				echo $toll_fee;
				
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");

?>