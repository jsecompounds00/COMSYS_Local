<?php
	
	$supplier_id = intval($_GET["supplier_id"]);
	$Division = strval($_GET["Division"]);

	require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>supplier_dropdown.php'.'</td><td>'.$error.' near line 13.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qryG = mysqli_prepare($db, "CALL sp_PO_Supplier_Group_Dropdown( ? )");
		mysqli_stmt_bind_param($qryG, 's', $Division);
		$qryG->execute();
		$resultG = mysqli_stmt_get_result($qryG); //return results of query
		$processErrorG = mysqli_error($db);

		if(!empty($processErrorG))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>supplier_dropdown.php'.'</td><td>'.$processErrorG.' near line 24.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			$applicable_to = array();
			while($row = mysqli_fetch_assoc($resultG))
			{
				$applicable_to[] = $row["applicable_to"];

			}

			$db->next_result();
			$resultG->close();

		}

			echo "<option></option>";
		foreach ($applicable_to as $key => $value) {
			echo "<optgroup label='".$value."'>";
				$qry = mysqli_prepare($db, "CALL sp_PO_Supplier_Dropdown(?, ?)");
				mysqli_stmt_bind_param($qry, 'ss', $Division, $value);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry);
				$processError = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>supplier_dropdown.php'.'</td><td>'.$processError.' near line 24.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					while($row = mysqli_fetch_assoc($result))
					{
						$id = $row["id"];
						$name = $row["name"];

						if ( $supplier_id == $id )
							echo "	<option value='".$id."' selected>".$name."</option>";
						else echo "	<option value='".$id."'>".$name."</option>";

					}
					$db->next_result();
					$result->close();
				}
			echo "</optgroup>";
		}

	}

	require("database_close.php");

?>