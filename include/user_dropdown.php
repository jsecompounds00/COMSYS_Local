<?php

$id = intval($_GET['userid']);
$user = intval($_GET['user']);
$Division = strval($_GET['Division']);
// echo $id;
// echo $user;
require("database_connect.php");

if(!empty($errno))
{
	$error = mysqli_connect_error();
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>user_dropdown.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}
else
{

	if ( $id==99999 ){
		
		$qry = "SELECT * FROM customers WHERE supplies=1 AND active=1 ORDER BY name";
		$result = mysqli_query($db, $qry);
		$processError = mysqli_error($db);

		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>user_dropdown.php'.'</td><td>'.$processError.' near line 25.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "<option value='0'></option>";

			while($row = mysqli_fetch_assoc($result)){
				$cust_id = $row['id'];
				if ( $user ){	
					if ( $user==$cust_id )
						echo "<option value='".$cust_id."' selected>".$row['name']."</option>";
					else echo "<option value='".$cust_id."'>".$row['name']."</option>";
				}else{
					echo "<option value='".$cust_id."'>".$row['name']."</option>";
				}
				// echo "<option value='".$cust_id."'>".$row['name']."</option>";
			}
		}
		

		$db->next_result();
		$result->close();
	}else{
		$qry = mysqli_prepare($db, "CALL sp_User_Dropdown(?, 0, 1, ?)");
		mysqli_stmt_bind_param($qry, 'is', $id, $Division);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);

		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>user_dropdown.php'.'</td><td>'.$processError.' near line 55.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "<option value='0'></option>";

			while($row = mysqli_fetch_assoc($result)){
				$user_id = $row['user_id'];
				if ( $user ){	
					if ( $user==$user_id )
						echo "<option value='".$user_id."' selected>".$row['User']."</option>";
					else echo "<option value='".$user_id."'>".$row['User']."</option>";
				}else{
					echo "<option value='".$user_id."'>".$row['User']."</option>";
				}
			}
			

			$db->next_result();
			$result->close();
		}
	}
}
require("database_close.php");
?> 