<?php

$id = intval($_GET['id']);
$department_id = intval($_GET['department_id']);
$division = strval($_GET['division']);

require("database_connect.php");

if(!empty($errno))
{
	$error = mysqli_connect_error();
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>department_dropdown.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}
else
{

	// if ( $id ){
	// 	$active = 2;
	// }else{
		$active = 1;
	// }s

	$qry = mysqli_prepare($db, "CALL sp_Department_Dropdown_New(?, ?)");
	mysqli_stmt_bind_param($qry, 'si', $division, $active);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query

	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>department_dropdown.php'.'</td><td>'.$processError.' near line 33.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{ 
			echo "<option></option>";
		while( $row = mysqli_fetch_assoc( $result ) ){
			$deptID = $row['id'];
			$deptCode = $row['code'];

			echo "<option value='".$deptID."'>".$deptCode."</option>";
		}

		$db->next_result();
		$result->close(); 
	}


}
require("database_close.php");
?> 