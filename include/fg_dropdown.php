<?php

$soft_pvc = intval($_GET['soft_pvc']);
$fg = intval($_GET['fg']);
$batID = intval($_GET['batID']);
$sltActivity = intval($_GET['sltActivity']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qry = mysqli_prepare($db, "CALL sp_FG_Dropdown(?, 0, 0)");
		mysqli_stmt_bind_param($qry, 'i', $soft_pvc);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown.php'.'</td><td>'.$processError.' near line 22.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			$i = 0;
			if(!$batID)
				echo "<option></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				
				$FgId = $row['id'];
				$Fg = $row['name'];

				if($batID){
					if ($FgId == $fg){
						echo "<option id='optionFG$i' value=".$FgId." selected>".$Fg."</option>";
					}
				}else{
					if ($FgId == $fg){
						if ( $sltActivity == 4 || $sltActivity == 2 )
							echo "<option id='optionFG$i' value=".$FgId." disabled>".$Fg."</option>";
						else
							echo "<option id='optionFG$i' value=".$FgId." selected>".$Fg."</option>";
					}else {
						if ( $sltActivity == 4 || $sltActivity == 2 )
							echo "<option id='optionFG$i' value=".$FgId." disabled>".$Fg."</option>";
						else
							echo "<option id='optionFG$i' value=".$FgId.">".$Fg."</option>";
					}
				}
				$i++;
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>