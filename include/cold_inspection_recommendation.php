<?php

	require("database_connect.php");

	$qryCI = mysqli_prepare($db, "CALL sp_BAT_Cold_Inspection_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qryCI, 'is', $TypeID, $Type);
	$qryCI->execute();
	$resultCI = mysqli_stmt_get_result($qryCI);
	$processErrorCI = mysqli_error($db);

	if ( !empty($processErrorCI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pellet_inspection_recommendation.php'.'</td><td>'.$processErrorCI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultCI)){
			$CItrial_no = $row['CItrial_no'];
			$CIFGName = $row['CIFGName'];

			$CITrial1 = $row['CITrial1']; $CITrial2 = $row['CITrial2']; $CITrial3 = $row['CITrial3']; $CITrial4 = $row['CITrial4']; $CITrial5 = $row['CITrial5'];
			$CITrial6 = $row['CITrial6']; $CITrial7 = $row['CITrial7']; $CITrial8 = $row['CITrial8']; $CITrial9 = $row['CITrial9']; $CITrial10 = $row['CITrial10'];
			$CITrial11 = $row['CITrial11']; $CITrial12 = $row['CITrial12']; $CITrial13 = $row['CITrial13']; $CITrial14 = $row['CITrial14']; $CITrial15 = $row['CITrial15'];
			$CITrial16 = $row['CITrial16']; $CITrial17 = $row['CITrial17']; $CITrial18 = $row['CITrial18']; $CITrial19 = $row['CITrial19']; $CITrial20 = $row['CITrial20'];

			$CISpecimen1_1 = $row['CISpecimen1_1']; $CISpecimen1_2 = $row['CISpecimen1_2']; $CISpecimen1_3 = $row['CISpecimen1_3']; $CISpecimen1_4 = $row['CISpecimen1_4']; $CISpecimen1_5 = $row['CISpecimen1_5'];
			$CISpecimen1_6 = $row['CISpecimen1_6']; $CISpecimen1_7 = $row['CISpecimen1_7']; $CISpecimen1_8 = $row['CISpecimen1_8']; $CISpecimen1_9 = $row['CISpecimen1_9']; $CISpecimen1_10 = $row['CISpecimen1_10'];
			$CISpecimen1_11 = $row['CISpecimen1_11']; $CISpecimen1_12 = $row['CISpecimen1_12']; $CISpecimen1_13 = $row['CISpecimen1_13']; $CISpecimen1_14 = $row['CISpecimen1_14']; $CISpecimen1_15 = $row['CISpecimen1_15'];
			$CISpecimen1_16 = $row['CISpecimen1_16']; $CISpecimen1_17 = $row['CISpecimen1_17']; $CISpecimen1_18 = $row['CISpecimen1_18']; $CISpecimen1_19 = $row['CISpecimen1_19']; $CISpecimen1_20 = $row['CISpecimen1_20'];

			$CISpecimen2_1 = $row['CISpecimen2_1']; $CISpecimen2_2 = $row['CISpecimen2_2']; $CISpecimen2_3 = $row['CISpecimen2_3']; $CISpecimen2_4 = $row['CISpecimen2_4']; $CISpecimen2_5 = $row['CISpecimen2_5'];
			$CISpecimen2_6 = $row['CISpecimen2_6']; $CISpecimen2_7 = $row['CISpecimen2_7']; $CISpecimen2_8 = $row['CISpecimen2_8']; $CISpecimen2_9 = $row['CISpecimen2_9']; $CISpecimen2_10 = $row['CISpecimen2_10'];
			$CISpecimen2_11 = $row['CISpecimen2_11']; $CISpecimen2_12 = $row['CISpecimen2_12']; $CISpecimen2_13 = $row['CISpecimen2_13']; $CISpecimen2_14 = $row['CISpecimen2_14']; $CISpecimen2_15 = $row['CISpecimen2_15'];
			$CISpecimen2_16 = $row['CISpecimen2_16']; $CISpecimen2_17 = $row['CISpecimen2_17']; $CISpecimen2_18 = $row['CISpecimen2_18']; $CISpecimen2_19 = $row['CISpecimen2_19']; $CISpecimen2_20 = $row['CISpecimen2_20'];

			$CISpecimen3_1 = $row['CISpecimen3_1']; $CISpecimen3_2 = $row['CISpecimen3_2']; $CISpecimen3_3 = $row['CISpecimen3_3']; $CISpecimen3_4 = $row['CISpecimen3_4']; $CISpecimen3_5 = $row['CISpecimen3_5'];
			$CISpecimen3_6 = $row['CISpecimen3_6']; $CISpecimen3_7 = $row['CISpecimen3_7']; $CISpecimen3_8 = $row['CISpecimen3_8']; $CISpecimen3_9 = $row['CISpecimen3_9']; $CISpecimen3_10 = $row['CISpecimen3_10'];
			$CISpecimen3_11 = $row['CISpecimen3_11']; $CISpecimen3_12 = $row['CISpecimen3_12']; $CISpecimen3_13 = $row['CISpecimen3_13']; $CISpecimen3_14 = $row['CISpecimen3_14']; $CISpecimen3_15 = $row['CISpecimen3_15'];
			$CISpecimen3_16 = $row['CISpecimen3_16']; $CISpecimen3_17 = $row['CISpecimen3_17']; $CISpecimen3_18 = $row['CISpecimen3_18']; $CISpecimen3_19 = $row['CISpecimen3_19']; $CISpecimen3_20 = $row['CISpecimen3_20'];
		?>
				<table class='results_child_tables_form'>
					<tr>
						<td colspan='<?php echo ($CItrial_no+1); ?>'> <?php  echo $CIFGName; ?> </td>
					</tr>
					<tr>
						<th></th>
						<?php
							if ( $CITrial1 <= $CItrial_no &&$CITrial1!= '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial1; ?> </th>
						<?php		
							}

							if ( $CITrial2 <= $CItrial_no && $CITrial2 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial2; ?> </th>
						<?php		
							}

							if ( $CITrial3 <= $CItrial_no && $CITrial3 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial3; ?> </th>
						<?php		
							}

							if ( $CITrial4 <= $CItrial_no && $CITrial4 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial4; ?> </th>
						<?php		
							}

							if ( $CITrial5 <= $CItrial_no && $CITrial5 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial5; ?> </th>
						<?php		
							}

							if ( $CITrial6 <= $CItrial_no && $CITrial6 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial6; ?> </th>
						<?php		
							}

							if ( $CITrial7 <= $CItrial_no && $CITrial7 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial7; ?> </th>
						<?php		
							}

							if ( $CITrial8 <= $CItrial_no && $CITrial8 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial8; ?> </th>
						<?php		
							}

							if ( $CITrial9 <= $CItrial_no && $CITrial9 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial9; ?> </th>
						<?php		
							}

							if ( $CITrial10 <= $CItrial_no && $CITrial10 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial10; ?> </th>
						<?php		
							}

							if ( $CITrial11 <= $CItrial_no && $CITrial11 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial11; ?> </th>
						<?php		
							}

							if ( $CITrial12 <= $CItrial_no && $CITrial12 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial12; ?> </th>
						<?php		
							}

							if ( $CITrial13 <= $CItrial_no && $CITrial13 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial13; ?> </th>
						<?php		
							}

							if ( $CITrial14 <= $CItrial_no && $CITrial14 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial14; ?> </th>
						<?php		
							}

							if ( $CITrial15 <= $CItrial_no && $CITrial15 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial15; ?> </th>
						<?php		
							}

							if ( $CITrial16 <= $CItrial_no && $CITrial16 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial16; ?> </th>
						<?php		
							}

							if ( $CITrial17 <= $CItrial_no && $CITrial17 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial17; ?> </th>
						<?php		
							}

							if ( $CITrial18 <= $CItrial_no && $CITrial18 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial18; ?> </th>
						<?php		
							}

							if ( $CITrial19 <= $CItrial_no && $CITrial19 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial19; ?> </th>
						<?php		
							}

							if ( $CITrial20 <= $CItrial_no && $CITrial20 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CITrial20; ?> </th>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### SPECIMEN 1	 -->	
					<tr>
						<td> Specimen 1 </td>
						<?php
							if ( $CITrial1 <= $CItrial_no && $CITrial1 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_1; ?> </td>
						<?php		
							}

							if ( $CITrial2 <= $CItrial_no && $CITrial2 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_2; ?> </td>
						<?php		
							}

							if ( $CITrial3 <= $CItrial_no && $CITrial3 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_3; ?> </td>
						<?php		
							}

							if ( $CITrial4 <= $CItrial_no && $CITrial4 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_4; ?> </td>
						<?php		
							}

							if ( $CITrial5 <= $CItrial_no && $CITrial5 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_5; ?> </td>
						<?php		
							}

							if ( $CITrial6 <= $CItrial_no && $CITrial6 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_6; ?> </td>
						<?php		
							}

							if ( $CITrial7 <= $CItrial_no && $CITrial7 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_7; ?> </td>
						<?php		
							}

							if ( $CITrial8 <= $CItrial_no && $CITrial8 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_8; ?> </td>
						<?php		
							}

							if ( $CITrial9 <= $CItrial_no && $CITrial9 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_9; ?> </td>
						<?php		
							}

							if ( $CITrial10 <= $CItrial_no && $CITrial10 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_10; ?> </td>
						<?php		
							}

							if ( $CITrial11 <= $CItrial_no && $CITrial11 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_11; ?> </td>
						<?php		
							}

							if ( $CITrial12 <= $CItrial_no && $CITrial12 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_12; ?> </td>
						<?php		
							}

							if ( $CITrial13 <= $CItrial_no && $CITrial13 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_13; ?> </td>
						<?php		
							}

							if ( $CITrial14 <= $CItrial_no && $CITrial14 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_14; ?> </td>
						<?php		
							}

							if ( $CITrial15 <= $CItrial_no && $CITrial15 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_15; ?> </td>
						<?php		
							}

							if ( $CITrial16 <= $CItrial_no && $CITrial16 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_16; ?> </td>
						<?php		
							}

							if ( $CITrial17 <= $CItrial_no && $CITrial17 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_17; ?> </td>
						<?php		
							}

							if ( $CITrial18 <= $CItrial_no && $CITrial18 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_18; ?> </td>
						<?php		
							}

							if ( $CITrial19 <= $CItrial_no && $CITrial19 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_19; ?> </td>
						<?php		
							}

							if ( $CITrial20 <= $CItrial_no && $CITrial20 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen1_20; ?> </td>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### SPECIMEN 2	 -->	
					<tr>
						<td> Specimen 2 </td>
						<?php
							if ( $CITrial1 <= $CItrial_no && $CITrial1 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_1; ?> </td>
						<?php		
							}

							if ( $CITrial2 <= $CItrial_no && $CITrial2 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_2; ?> </td>
						<?php		
							}

							if ( $CITrial3 <= $CItrial_no && $CITrial3 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_3; ?> </td>
						<?php		
							}

							if ( $CITrial4 <= $CItrial_no && $CITrial4 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_4; ?> </td>
						<?php		
							}

							if ( $CITrial5 <= $CItrial_no && $CITrial5 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_5; ?> </td>
						<?php		
							}

							if ( $CITrial6 <= $CItrial_no && $CITrial6 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_6; ?> </td>
						<?php		
							}

							if ( $CITrial7 <= $CItrial_no && $CITrial7 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_7; ?> </td>
						<?php		
							}

							if ( $CITrial8 <= $CItrial_no && $CITrial8 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_8; ?> </td>
						<?php		
							}

							if ( $CITrial9 <= $CItrial_no && $CITrial9 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_9; ?> </td>
						<?php		
							}

							if ( $CITrial10 <= $CItrial_no && $CITrial10 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_10; ?> </td>
						<?php		
							}

							if ( $CITrial11 <= $CItrial_no && $CITrial11 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_11; ?> </td>
						<?php		
							}

							if ( $CITrial12 <= $CItrial_no && $CITrial12 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_12; ?> </td>
						<?php		
							}

							if ( $CITrial13 <= $CItrial_no && $CITrial13 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_13; ?> </td>
						<?php		
							}

							if ( $CITrial14 <= $CItrial_no && $CITrial14 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_14; ?> </td>
						<?php		
							}

							if ( $CITrial15 <= $CItrial_no && $CITrial15 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_15; ?> </td>
						<?php		
							}

							if ( $CITrial16 <= $CItrial_no && $CITrial16 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_16; ?> </td>
						<?php		
							}

							if ( $CITrial17 <= $CItrial_no && $CITrial17 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_17; ?> </td>
						<?php		
							}

							if ( $CITrial18 <= $CItrial_no && $CITrial18 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_18; ?> </td>
						<?php		
							}

							if ( $CITrial19 <= $CItrial_no && $CITrial19 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_19; ?> </td>
						<?php		
							}

							if ( $CITrial20 <= $CItrial_no && $CITrial20 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen2_20; ?> </td>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### SPECIMEN 3	 -->	
					<tr>
						<td> Specimen 3 </td>
						<?php
							if ( $CITrial1 <= $CItrial_no && $CITrial1 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_1; ?> </td>
						<?php		
							}

							if ( $CITrial2 <= $CItrial_no && $CITrial2 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_2; ?> </td>
						<?php		
							}

							if ( $CITrial3 <= $CItrial_no && $CITrial3 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_3; ?> </td>
						<?php		
							}

							if ( $CITrial4 <= $CItrial_no && $CITrial4 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_4; ?> </td>
						<?php		
							}

							if ( $CITrial5 <= $CItrial_no && $CITrial5 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_5; ?> </td>
						<?php		
							}

							if ( $CITrial6 <= $CItrial_no && $CITrial6 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_6; ?> </td>
						<?php		
							}

							if ( $CITrial7 <= $CItrial_no && $CITrial7 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_7; ?> </td>
						<?php		
							}

							if ( $CITrial8 <= $CItrial_no && $CITrial8 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_8; ?> </td>
						<?php		
							}

							if ( $CITrial9 <= $CItrial_no && $CITrial9 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_9; ?> </td>
						<?php		
							}

							if ( $CITrial10 <= $CItrial_no && $CITrial10 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_10; ?> </td>
						<?php		
							}

							if ( $CITrial11 <= $CItrial_no && $CITrial11 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_11; ?> </td>
						<?php		
							}

							if ( $CITrial12 <= $CItrial_no && $CITrial12 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_12; ?> </td>
						<?php		
							}

							if ( $CITrial13 <= $CItrial_no && $CITrial13 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_13; ?> </td>
						<?php		
							}

							if ( $CITrial14 <= $CItrial_no && $CITrial14 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_14; ?> </td>
						<?php		
							}

							if ( $CITrial15 <= $CItrial_no && $CITrial15 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_15; ?> </td>
						<?php		
							}

							if ( $CITrial16 <= $CItrial_no && $CITrial16 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_16; ?> </td>
						<?php		
							}

							if ( $CITrial17 <= $CItrial_no && $CITrial17 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_17; ?> </td>
						<?php		
							}

							if ( $CITrial18 <= $CItrial_no && $CITrial18 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_18; ?> </td>
						<?php		
							}

							if ( $CITrial19 <= $CItrial_no && $CITrial19 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_19; ?> </td>
						<?php		
							}

							if ( $CITrial20 <= $CItrial_no && $CITrial20 != '-' ){
						?>
								<td class='align'> <?php echo $CISpecimen3_20; ?> </td>
						<?php		
							}
						?>
					</tr>
				</table>
	<?php
			}
			$db->next_result();
			$resultCI->close();
		}

	require("database_close.php");
?>