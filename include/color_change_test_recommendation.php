<?php

	require("database_connect.php");

	$qryCCT = mysqli_prepare($db, "CALL sp_BAT_Color_Change_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qryCCT, 'is', $TypeID, $Type);
	$qryCCT->execute();
	$resultCCT = mysqli_stmt_get_result($qryCCT);
	$processErrorCCT = mysqli_error($db);

	if ( !empty($processErrorCCT) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>color_change_test_recommendation.php'.'</td><td>'.$processErrorCCT.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultCCT)){
				$CCTtrial_no = $row['CCTtrial_no'];
				$CCTFGName = $row['CCTFGName'];

				$CCTTrial1 = $row['CCTTrial1']; $CCTTrial2 = $row['CCTTrial2']; $CCTTrial3 = $row['CCTTrial3']; $CCTTrial4 = $row['CCTTrial4']; $CCTTrial5 = $row['CCTTrial5'];
				$CCTTrial6 = $row['CCTTrial6']; $CCTTrial7 = $row['CCTTrial7']; $CCTTrial8 = $row['CCTTrial8']; $CCTTrial9 = $row['CCTTrial9']; $CCTTrial10 = $row['CCTTrial10'];
				$CCTTrial11 = $row['CCTTrial11']; $CCTTrial12 = $row['CCTTrial12']; $CCTTrial13 = $row['CCTTrial13']; $CCTTrial14 = $row['CCTTrial14']; $CCTTrial15 = $row['CCTTrial15'];
				$CCTTrial16 = $row['CCTTrial16']; $CCTTrial17 = $row['CCTTrial17']; $CCTTrial18 = $row['CCTTrial18']; $CCTTrial19 = $row['CCTTrial19']; $CCTTrial20 = $row['CCTTrial20'];

				$CCTColor1 = $row['CCTColor1']; $CCTColor2 = $row['CCTColor2']; $CCTColor3 = $row['CCTColor3']; $CCTColor4 = $row['CCTColor4']; $CCTColor5 = $row['CCTColor5'];
				$CCTColor6 = $row['CCTColor6']; $CCTColor7 = $row['CCTColor7']; $CCTColor8 = $row['CCTColor8']; $CCTColor9 = $row['CCTColor9']; $CCTColor10 = $row['CCTColor10'];
				$CCTColor11 = $row['CCTColor11']; $CCTColor12 = $row['CCTColor12']; $CCTColor13 = $row['CCTColor13']; $CCTColor14 = $row['CCTColor14']; $CCTColor15 = $row['CCTColor15'];
				$CCTColor16 = $row['CCTColor16']; $CCTColor17 = $row['CCTColor17']; $CCTColor18 = $row['CCTColor18']; $CCTColor19 = $row['CCTColor19']; $CCTColor20 = $row['CCTColor20'];

				$CCTHeat1 = $row['CCTHeat1']; $CCTHeat2 = $row['CCTHeat2']; $CCTHeat3 = $row['CCTHeat3']; $CCTHeat4 = $row['CCTHeat4']; $CCTHeat5 = $row['CCTHeat5'];
				$CCTHeat6 = $row['CCTHeat6']; $CCTHeat7 = $row['CCTHeat7']; $CCTHeat8 = $row['CCTHeat8']; $CCTHeat9 = $row['CCTHeat9']; $CCTHeat10 = $row['CCTHeat10'];
				$CCTHeat11 = $row['CCTHeat11']; $CCTHeat12 = $row['CCTHeat12']; $CCTHeat13 = $row['CCTHeat13']; $CCTHeat14 = $row['CCTHeat14']; $CCTHeat15 = $row['CCTHeat15'];
				$CCTHeat16 = $row['CCTHeat16']; $CCTHeat17 = $row['CCTHeat17']; $CCTHeat18 = $row['CCTHeat18']; $CCTHeat19 = $row['CCTHeat19']; $CCTHeat20 = $row['CCTHeat20'];
?>
				<table class='results_child_tables_form'>
					<tr>
						<td colspan='<?php echo ($CCTtrial_no+1); ?>'> <?php  echo $CCTFGName; ?> </td>
					</tr>
					<tr>
						<th></th>
						<?php
							if ( $CCTTrial1 <= $CCTtrial_no &&$CCTTrial1!= '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial1; ?> </th>
						<?php		
							}

							if ( $CCTTrial2 <= $CCTtrial_no && $CCTTrial2 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial2; ?> </th>
						<?php		
							}

							if ( $CCTTrial3 <= $CCTtrial_no && $CCTTrial3 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial3; ?> </th>
						<?php		
							}

							if ( $CCTTrial4 <= $CCTtrial_no && $CCTTrial4 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial4; ?> </th>
						<?php		
							}

							if ( $CCTTrial5 <= $CCTtrial_no && $CCTTrial5 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial5; ?> </th>
						<?php		
							}

							if ( $CCTTrial6 <= $CCTtrial_no && $CCTTrial6 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial6; ?> </th>
						<?php		
							}

							if ( $CCTTrial7 <= $CCTtrial_no && $CCTTrial7 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial7; ?> </th>
						<?php		
							}

							if ( $CCTTrial8 <= $CCTtrial_no && $CCTTrial8 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial8; ?> </th>
						<?php		
							}

							if ( $CCTTrial9 <= $CCTtrial_no && $CCTTrial9 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial9; ?> </th>
						<?php		
							}

							if ( $CCTTrial10 <= $CCTtrial_no && $CCTTrial10 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial10; ?> </th>
						<?php		
							}

							if ( $CCTTrial11 <= $CCTtrial_no && $CCTTrial11 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial11; ?> </th>
						<?php		
							}

							if ( $CCTTrial12 <= $CCTtrial_no && $CCTTrial12 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial12; ?> </th>
						<?php		
							}

							if ( $CCTTrial13 <= $CCTtrial_no && $CCTTrial13 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial13; ?> </th>
						<?php		
							}

							if ( $CCTTrial14 <= $CCTtrial_no && $CCTTrial14 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial14; ?> </th>
						<?php		
							}

							if ( $CCTTrial15 <= $CCTtrial_no && $CCTTrial15 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial15; ?> </th>
						<?php		
							}

							if ( $CCTTrial16 <= $CCTtrial_no && $CCTTrial16 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial16; ?> </th>
						<?php		
							}

							if ( $CCTTrial17 <= $CCTtrial_no && $CCTTrial17 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial17; ?> </th>
						<?php		
							}

							if ( $CCTTrial18 <= $CCTtrial_no && $CCTTrial18 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial18; ?> </th>
						<?php		
							}

							if ( $CCTTrial19 <= $CCTtrial_no && $CCTTrial19 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial19; ?> </th>
						<?php		
							}

							if ( $CCTTrial20 <= $CCTtrial_no && $CCTTrial20 != '-' ){
						?>
								<th> <?php echo 'Trial '.$CCTTrial20; ?> </th>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Color Conformance	 -->	
					<tr>
						<td> Color Conformance </td>
						<?php
							if ( $CCTTrial1 <= $CCTtrial_no && $CCTTrial1 != '-' ){
						?>
								<td> <?php echo $CCTColor1; ?> </td>
						<?php		
							}

							if ( $CCTTrial2 <= $CCTtrial_no && $CCTTrial2 != '-' ){
						?>
								<td> <?php echo $CCTColor2; ?> </td>
						<?php		
							}

							if ( $CCTTrial3 <= $CCTtrial_no && $CCTTrial3 != '-' ){
						?>
								<td> <?php echo $CCTColor3; ?> </td>
						<?php		
							}

							if ( $CCTTrial4 <= $CCTtrial_no && $CCTTrial4 != '-' ){
						?>
								<td> <?php echo $CCTColor4; ?> </td>
						<?php		
							}

							if ( $CCTTrial5 <= $CCTtrial_no && $CCTTrial5 != '-' ){
						?>
								<td> <?php echo $CCTColor5; ?> </td>
						<?php		
							}

							if ( $CCTTrial6 <= $CCTtrial_no && $CCTTrial6 != '-' ){
						?>
								<td> <?php echo $CCTColor6; ?> </td>
						<?php		
							}

							if ( $CCTTrial7 <= $CCTtrial_no && $CCTTrial7 != '-' ){
						?>
								<td> <?php echo $CCTColor7; ?> </td>
						<?php		
							}

							if ( $CCTTrial8 <= $CCTtrial_no && $CCTTrial8 != '-' ){
						?>
								<td> <?php echo $CCTColor8; ?> </td>
						<?php		
							}

							if ( $CCTTrial9 <= $CCTtrial_no && $CCTTrial9 != '-' ){
						?>
								<td> <?php echo $CCTColor9; ?> </td>
						<?php		
							}

							if ( $CCTTrial10 <= $CCTtrial_no && $CCTTrial10 != '-' ){
						?>
								<td> <?php echo $CCTColor10; ?> </td>
						<?php		
							}

							if ( $CCTTrial11 <= $CCTtrial_no && $CCTTrial11 != '-' ){
						?>
								<td> <?php echo $CCTColor11; ?> </td>
						<?php		
							}

							if ( $CCTTrial12 <= $CCTtrial_no && $CCTTrial12 != '-' ){
						?>
								<td> <?php echo $CCTColor12; ?> </td>
						<?php		
							}

							if ( $CCTTrial13 <= $CCTtrial_no && $CCTTrial13 != '-' ){
						?>
								<td> <?php echo $CCTColor13; ?> </td>
						<?php		
							}

							if ( $CCTTrial14 <= $CCTtrial_no && $CCTTrial14 != '-' ){
						?>
								<td> <?php echo $CCTColor14; ?> </td>
						<?php		
							}

							if ( $CCTTrial15 <= $CCTtrial_no && $CCTTrial15 != '-' ){
						?>
								<td> <?php echo $CCTColor15; ?> </td>
						<?php		
							}

							if ( $CCTTrial16 <= $CCTtrial_no && $CCTTrial16 != '-' ){
						?>
								<td> <?php echo $CCTColor16; ?> </td>
						<?php		
							}

							if ( $CCTTrial17 <= $CCTtrial_no && $CCTTrial17 != '-' ){
						?>
								<td> <?php echo $CCTColor17; ?> </td>
						<?php		
							}

							if ( $CCTTrial18 <= $CCTtrial_no && $CCTTrial18 != '-' ){
						?>
								<td> <?php echo $CCTColor18; ?> </td>
						<?php		
							}

							if ( $CCTTrial19 <= $CCTtrial_no && $CCTTrial19 != '-' ){
						?>
								<td> <?php echo $CCTColor19; ?> </td>
						<?php		
							}

							if ( $CCTTrial20 <= $CCTtrial_no && $CCTTrial20 != '-' ){
						?>
								<td> <?php echo $CCTColor20; ?> </td>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Heat Stability	 -->	
					<tr>
						<td> Heat Stability </td>
						<?php
							if ( $CCTTrial1 <= $CCTtrial_no && !is_null($CCTTrial1) ){
						?>
								<td> <?php echo $CCTHeat1; ?> </td>
						<?php		
							}

							if ( $CCTTrial2 <= $CCTtrial_no && $CCTTrial2 != '-' ){
						?>
								<td> <?php echo $CCTHeat2; ?> </td>
						<?php		
							}

							if ( $CCTTrial3 <= $CCTtrial_no && $CCTTrial3 != '-' ){
						?>
								<td> <?php echo $CCTHeat3; ?> </td>
						<?php		
							}

							if ( $CCTTrial4 <= $CCTtrial_no && $CCTTrial4 != '-' ){
						?>
								<td> <?php echo $CCTHeat4; ?> </td>
						<?php		
							}

							if ( $CCTTrial5 <= $CCTtrial_no && $CCTTrial5 != '-' ){
						?>
								<td> <?php echo $CCTHeat5; ?> </td>
						<?php		
							}

							if ( $CCTTrial6 <= $CCTtrial_no && $CCTTrial6 != '-' ){
						?>
								<td> <?php echo $CCTHeat6; ?> </td>
						<?php		
							}

							if ( $CCTTrial7 <= $CCTtrial_no && $CCTTrial7 != '-' ){
						?>
								<td> <?php echo $CCTHeat7; ?> </td>
						<?php		
							}

							if ( $CCTTrial8 <= $CCTtrial_no && $CCTTrial8 != '-' ){
						?>
								<td> <?php echo $CCTHeat8; ?> </td>
						<?php		
							}

							if ( $CCTTrial9 <= $CCTtrial_no && $CCTTrial9 != '-' ){
						?>
								<td> <?php echo $CCTHeat9; ?> </td>
						<?php		
							}

							if ( $CCTTrial10 <= $CCTtrial_no && $CCTTrial10 != '-' ){
						?>
								<td> <?php echo $CCTHeat10; ?> </td>
						<?php		
							}

							if ( $CCTTrial11 <= $CCTtrial_no && $CCTTrial11 != '-' ){
						?>
								<td> <?php echo $CCTHeat11; ?> </td>
						<?php		
							}

							if ( $CCTTrial12 <= $CCTtrial_no && $CCTTrial12 != '-' ){
						?>
								<td> <?php echo $CCTHeat12; ?> </td>
						<?php		
							}

							if ( $CCTTrial13 <= $CCTtrial_no && $CCTTrial13 != '-' ){
						?>
								<td> <?php echo $CCTHeat13; ?> </td>
						<?php		
							}

							if ( $CCTTrial14 <= $CCTtrial_no && $CCTTrial14 != '-' ){
						?>
								<td> <?php echo $CCTHeat14; ?> </td>
						<?php		
							}

							if ( $CCTTrial15 <= $CCTtrial_no && $CCTTrial15 != '-' ){
						?>
								<td> <?php echo $CCTHeat15; ?> </td>
						<?php		
							}

							if ( $CCTTrial16 <= $CCTtrial_no && $CCTTrial16 != '-' ){
						?>
								<td> <?php echo $CCTHeat16; ?> </td>
						<?php		
							}

							if ( $CCTTrial17 <= $CCTtrial_no && $CCTTrial17 != '-' ){
						?>
								<td> <?php echo $CCTHeat17; ?> </td>
						<?php		
							}

							if ( $CCTTrial18 <= $CCTtrial_no && $CCTTrial18 != '-' ){
						?>
								<td> <?php echo $CCTHeat18; ?> </td>
						<?php		
							}

							if ( $CCTTrial19 <= $CCTtrial_no && $CCTTrial19 != '-' ){
						?>
								<td> <?php echo $CCTHeat19; ?> </td>
						<?php		
							}

							if ( $CCTTrial20 <= $CCTtrial_no && $CCTTrial20 != '-' ){
						?>
								<td> <?php echo $CCTHeat20; ?> </td>
						<?php		
							}
						?>
					</tr>
				</table>
	<?php
			}
			$db->next_result();
			$resultCCT->close();
		}

	require("database_close.php");
?>