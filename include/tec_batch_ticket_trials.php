
	<table class="trials_child_tables_form">

		<colgroup> <col width="150px"></col><col></col><col width="150px"></col> </colgroup>

		<tr class="spacing">
			<td colspan="2"><b>EXTRUSION</b></td>

			<td rowspan="7"></td>

			<td><b>MIXING</b></td>
			<td>
				<input type="checkbox" name="chkEdit" id="chkEdit" onchange="editable()"> 
				<label for="chkEdit">Edit</label>
			</td>
		</tr>
		<tr>
			<td>DAR No.:</td>
			<td>
				<input type="text" name="txtExtDarNo" id="txtExtDarNo" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_ext_dar_no : $initExtDarNo_JRD ); ?>">
			</td>
			
			<td>DAR No.:</td>
			<td>
				<input type="text" name="txtMixDarNo" id="txtMixDarNo" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_dar_no : $initMixDarNo_JRD ); ?>">
			</td>
		</tr>
		<tr>
			<td>Z1:</td>
			<td>
				<input type="text" name="txtExtZ1" id="txtExtZ1" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_ext_z1 : $initExtZ1_JRD ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixParam1" id="txtMixParam1" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_parameter1 : ( $initMixParam1_JRD != NULL ? $initMixParam1_JRD : "0" ) ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixSequence1" id="txtMixSequence1" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_sequence1 : ( $initMixSequence1_JRD != NULL ? $initMixSequence1_JRD : "CMR + CSS" ) ); ?>" readOnly>
			</td>
		</tr>
		<tr>
			<td>Z2:</td>
			<td>
				<input type="text" name="txtExtZ2" id="txtExtZ2" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_ext_z2 : $initExtZ2_JRD ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixParam2" id="txtMixParam2" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_parameter2 : ( $initMixParam2_JRD != NULL ? $initMixParam2_JRD : "1" ) ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixSequence2" id="txtMixSequence2" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_sequence2 : ( $initMixSequence2_JRD != NULL ? $initMixSequence2_JRD : "CMP" ) ); ?>" readOnly>
			</td>
		</tr>
		<tr>
			<td>DH:</td>
			<td>
				<input type="text" name="txtExtDH" id="txtExtDH" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_ext_dh : $initExtDH_JRD ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixParam3" id="txtMixParam3" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_parameter3 : ( $initMixParam3_JRD != NULL ? $initMixParam3_JRD : "60C" ) ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixSequence3" id="txtMixSequence3" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_sequence3 : ( $initMixSequence3_JRD != NULL ? $initMixSequence3_JRD : "CC + CSL + COL" ) ); ?>" readOnly>
			</td>
		</tr>
		<tr>
			<td>Screw Speed:</td>
			<td>
				<input type="text" name="txtExtScrewSpeed" id="txtExtScrewSpeed" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_ext_screw_speed : $initExtScrewSpeed_JRD ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixParam4" id="txtMixParam4" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_parameter4 : ( $initMixParam4_JRD != NULL ? $initMixParam4_JRD : "80C" ) ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixSequence4" id="txtMixSequence4" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_sequence4 : ( $initMixSequence4_JRD != NULL ? $initMixSequence4_JRD : "Open H2O valve" ) ); ?>" readOnly>
			</td>
		</tr>
		<tr>
			<td>Cutter Speed:</td>
			<td>
				<input type="text" name="txtExtCutterSpeed" id="txtExtCutterSpeed" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_ext_cutter_speed : $initExtCutterSpeed_JRD ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixParam5" id="txtMixParam5" readOnly value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_parameter5 : ( $initMixParam5_JRD != NULL ? $initMixParam5_JRD : "D/C" ) ); ?>">
			</td>
			<td>
				<input type="text" name="txtMixSequence5" id="txtMixSequence5" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_mix_sequence5 : ( $initMixSequence5_JRD != NULL ? $initMixSequence5_JRD : "50C" ) ); ?>" readOnly>
			</td>
		</tr>
		<tr class="spacing">
			<td>Multiplier:</td>
			<td>
				<input type="text" name="txtMultiplier" id="txtMultiplier" size="5"  onblur="autoMultiply(), autoSum()" onkeyup="autoMultiply(), autoSum()" value="<?php echo ( $BatchTicketID && $BatchTicketTrialID ? $db_multiplier : $initMultiplier_JRD ); ?>">
			</td>
		</tr>
	</table>