<?php
	session_start();
	require("/database_connect.php");
	require("/init_unset_values/jrd_init_value.php");
	$JRDId = intval($_GET["JRDId"]);

	$qry = mysqli_prepare($db, "CALL sp_JRD_Query(?)");
	mysqli_stmt_bind_param($qry, 'i', $JRDId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>product_development.php'.'</td><td>'.$processError.' near line 13.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		while($row = mysqli_fetch_assoc($result))
		{
			$tag_dev_extrusion = $row['tag_dev_extrusion'];
			$dev_extrusion_type = $row['dev_extrusion_type'];
			$dev_extrusion_type_others = $row['dev_extrusion_type_others'];
			$dev_thermal_rate = $row['dev_thermal_rate'];
			$dev_thermal_rate_others = $row['dev_thermal_rate_others'];
			$tag_dev_molding = $row['tag_dev_molding'];
			$dev_molding_type = $row['dev_molding_type'];
			$dev_molding_type_others = $row['dev_molding_type_others'];
			$tag_dev_others = $row['tag_dev_others'];
			$dev_others = $row['dev_others'];
			// $dev_others_array = explode('<br>', $dev_others);
			$tag_dev_require_ul = $row['tag_dev_require_ul'];
			$tag_dev_require_pns = $row['tag_dev_require_pns'];
			$tag_dev_require_none = $row['tag_dev_require_none'];
			$tag_dev_require_others = $row['tag_dev_require_others'];
			$dev_require_others = $row['dev_require_others'];
		}
	}
	$db->next_result();
	$result->close();
?>

<table class='child_tables_form' onmouseover="enableSubCategories(), enableOtherExtrusionType(), enableOtherThermalRate(), enableSubCategories(), enableOtherMoldingType(), enableSubCategories(), enableOtherReq()">
	<colgroup>
		<col width="15"></col><col width="250"></col>
		<col width="15"></col><col width="250"></col><col width="250"></col>
		<col width="15"></col><col width="250"></col><col width="250"></col><col width="250"></col>
	</colgroup>
	<tr class="title_font">
		<!-- ############### EXTRUSION ############### -->
		<td colspan="5" class="border_right">
			<label for='chkExtrusion'> 
				<input type='checkbox' value='1' name='chkExtrusion' id='chkExtrusion' onchange='enableSubCategories()' <?php echo( $JRDId ? ( $tag_dev_extrusion ? "checked" : "" ) : ( $initTagExtrusion ? "checked" : "" ) );?>> 
				Extrusion 
			</label>
		</td>

		<!-- ############### MOLDING ############### -->
		<td colspan="4"> 
			<label for='chkMolding'> 
				<input type='checkbox' value='1' name='chkMolding' id='chkMolding' onchange='enableSubCategories()' <?php echo( $JRDId ? ( $tag_dev_molding ? "checked" : "" ) : ( $initTagMolding ? "checked" : "" ) );?>> 
				Molding 
			</label>
		</td>
	</tr>
	<tr class="title_font">
		<!-- ############### EXTRUSION ############### -->
		<td colspan="2">Type</td>
		<td colspan="3" class="border_right">Thermal Rating / Grade</td>

		<!-- ############### MOLDING ############### -->
		<td colspan="4">Class / Type</td>
	</tr>
	<tr>
		<!-- ############### EXTRUSION ############### -->
		<td rowspan="4"></td>
		<td>
			<label for='Jacket'> 
				<input type='radio' name='radExtrusionType' id='Jacket' value='Jacket' disabled onchange='enableOtherExtrusionType()' <?php echo( $JRDId ? ( $dev_extrusion_type == "Jacket" ? "checked" : "" ) : ( $initExtrusionType == "Jacket" ? "checked" : "" ) );?>> 
				Jacket 
			</label>
		</td>
		<td rowspan="4"></td>
		<td>
			<label for='60C'> <input type='radio' name='radThermalRate' id='60C' value='60C' disabled onchange='enableOtherThermalRate()' <?php echo( $JRDId ? ( $dev_thermal_rate == "60C" ? "checked" : "" ) : ( $initThermalRate == "60C" ? "checked" : "" ) );?>> 60C </label>
		</td>
		<td class="border_right">
			<label for='75C'> <input type='radio' name='radThermalRate' id='75C' value='75C' disabled onchange='enableOtherThermalRate()' <?php echo( $JRDId ? ( $dev_thermal_rate == "75C" ? "checked" : "" ) : ( $initThermalRate == "75C" ? "checked" : "" ) );?>> 75C </label>
		</td>

		<!-- ############### MOLDING ############### -->
		<td rowspan="3"></td>
		<td>
			<label for='30'> <input type='radio' name='radMoldingType' id='30' value='30' disabled onchange='enableOtherMoldingType()' <?php echo( $JRDId ? ( $dev_molding_type == "30" ? "checked" : "" ) : ( $initMoldingType == "30" ? "checked" : "" ) );?>> 30 </label>
		</td>
		<td>
			<label for='55'> <input type='radio' name='radMoldingType' id='55' value='55' disabled onchange='enableOtherMoldingType()' <?php echo( $JRDId ? ( $dev_molding_type == "55" ? "checked" : "" ) : ( $initMoldingType == "55" ? "checked" : "" ) );?>>55 </label>
		</td>
		<td>
			<label for='Tube'> <input type='radio' name='radMoldingType' id='Tube' value='Tube' disabled onchange='enableOtherMoldingType()' <?php echo( $JRDId ? ( $dev_molding_type == "Tube" ? "checked" : "" ) : ( $initMoldingType == "Tube" ? "checked" : "" ) );?>>Tube </label>
		</td>
	</tr>
	<tr>
		<!-- ############### EXTRUSION ############### -->
		<td>
			<label for='Insulation'> 
				<input type='radio' name='radExtrusionType' id='Insulation' value='Insulation' disabled onchange='enableOtherExtrusionType()' <?php echo( $JRDId ? ( $dev_extrusion_type == "Insulation" ? "checked" : "" ) : ( $initExtrusionType == "Insulation" ? "checked" : "" ) );?>>
				 Insulation 
			</label>
		</td>
		<td>
			<label for='90C'> <input type='radio' name='radThermalRate' id='90C' value='90C' disabled onchange='enableOtherThermalRate()' <?php echo( $JRDId ? ( $dev_thermal_rate == "90C" ? "checked" : "" ) : ( $initThermalRate == "90C" ? "checked" : "" ) );?>> 90C </label>
		</td>
		<td class="border_right">
			<label for='105C'> <input type='radio' name='radThermalRate' id='105C' value='105C' disabled onchange='enableOtherThermalRate()' <?php echo( $JRDId ? ( $dev_thermal_rate == "105C" ? "checked" : "" ) : ( $initThermalRate == "105C" ? "checked" : "" ) );?>> 105C </label>
		</td>

		<!-- ############### MOLDING ############### -->
		<td>
			<label for='40'> <input type='radio' name='radMoldingType' id='40' value='40' disabled onchange='enableOtherMoldingType()' <?php echo( $JRDId ? ( $dev_molding_type == "40" ? "checked" : "" ) : ( $initMoldingType == "40" ? "checked" : "" ) );?>> 40 </label>
		</td>
		<td>
			<label for='Film'> <input type='radio' name='radMoldingType' id='Film' value='Film' disabled onchange='enableOtherMoldingType()' <?php echo( $JRDId ? ( $dev_molding_type == "Film" ? "checked" : "" ) : ( $initMoldingType == "Film" ? "checked" : "" ) );?>>Film </label>
		</td>
		<td>
			<label for='OtherMoldingType'> <input type='radio' name='radMoldingType' id='OtherMoldingType' value='OtherMoldingType' disabled onchange='enableOtherMoldingType()' <?php echo( $JRDId ? ( $dev_molding_type == "OtherMoldingType" ? "checked" : "" ) : ( $initMoldingType == "OtherMoldingType" ? "checked" : "" ) );?>>Others </label>
		</td>
	</tr>
	<tr>
		<!-- ############### EXTRUSION ############### -->
		<td>
			<label for='OtherExtrusionType'> 
				<input type='radio' name='radExtrusionType' id='OtherExtrusionType' value='OtherExtrusionType' disabled onchange='enableOtherExtrusionType()' <?php echo( $JRDId ? ( $dev_extrusion_type == "OtherExtrusionType" ? "checked" : "" ) : ( $initExtrusionType == "OtherExtrusionType" ? "checked" : "" ) );?>> 
				Others 
			</label>
		</td>
		<td>
			<label for='OtherThermalRate'> 
				<input type='radio' name='radThermalRate' id='OtherThermalRate' value='OtherThermalRate' disabled onchange='enableOtherThermalRate()' <?php echo( $JRDId ? ( $dev_thermal_rate == "OtherThermalRate" ? "checked" : "" ) : ( $initThermalRate == "OtherThermalRate" ? "checked" : "" ) );?>> 
				Others 
			</label>
		</td>
		<td class="border_right" rowspan="2"> </td>

		<!-- ############### MOLDING ############### -->
		<td>
			<label for='45'> <input type='radio' name='radMoldingType' id='45' value='45' disabled onchange='enableOtherMoldingType()' <?php echo( $JRDId ? ( $dev_molding_type == "45" ? "checked" : "" ) : ( $initMoldingType == "45" ? "checked" : "" ) );?>> 45 </label>
		</td>
		<td>
			<label for='Bottle'> <input type='radio' name='radMoldingType' id='Bottle' value='Bottle' disabled onchange='enableOtherMoldingType()' <?php echo( $JRDId ? ( $dev_molding_type == "Bottle" ? "checked" : "" ) : ( $initMoldingType == "Bottle" ? "checked" : "" ) );?>>Bottle </label>
		</td>
		<td>
			<label> <input type='text' name='txtOtherMoldingType' id='txtOtherMoldingType' disabled value="<?php echo( $JRDId ? $dev_molding_type_others : $initOtherMoldingType );?>"> </label>
		</td>
	</tr>
	<tr>
		<!-- ############### EXTRUSION ############### -->
		<td>
			<input type='text' name='txtOtherExtrusionType' id='txtOtherExtrusionType' disabled onchange='enableOtherExtrusionType()' value="<?php echo( $JRDId ? $dev_extrusion_type_others : $initOtherExtrusionType );?>">
		</td>
		<td>
			<input type='text' name='txtOtherThermalRate' id='txtOtherThermalRate' disabled value="<?php echo( $JRDId ? $dev_thermal_rate_others : $initOtherThermalRate );?>">
		</td>

		<!-- ############### MOLDING ############### -->
	</tr>
	<tr class="title_font">
		<td class="border_right border_top" colspan="5" valign="bottom">
			<label for='chkOtherActivity'> 
				<input type='checkbox' value='1' name='chkOtherActivity' id='chkOtherActivity' onchange='enableSubCategories()' <?php echo( $JRDId ? ( $tag_dev_others ? "checked" : "" ) : ( $initTagOtherActivity ? "checked" : "" ) );?>> 
				Others 
			</label>
		</td>
		<td colspan="5" class="border_top">
			Compliance Requirements
		</td>
	</tr>
	<tr>
		<!-- ############### PRODUCT'S END USE ############### -->
		<td rowspan="3"></td>
		<td valign="top" rowspan="3">
			State product's end use: 
		</td>
		<td colspan='3' rowspan="3" class="border_right">
			<textarea name='txtOtherActivity' id='txtOtherActivity' disabled><?php 
				if( $JRDId ){
					echo $dev_others;
				}else{
					echo $initOtherActivity;
				}
			?></textarea> 
		</td>

		<!-- ############### COMPLIANCE REQUIREMENTS ############### -->
		<td rowspan="3"></td>
		<td>
			<label for='UL'> <input type='checkbox' value='1' name='chkUL' id='UL' onchange='enableOtherReq()' <?php echo( $JRDId ? ( $tag_dev_require_ul ? "checked" : "" ) : ( $initTagUL ? "checked" : "" ) );?>> UL </label>
		</td>
		<td>
			<label for='None'> <input type='checkbox' value='1' name='chkNone' id='None' onchange='enableOtherReq()' <?php echo( $JRDId ? ( $tag_dev_require_none ? "checked" : "" ) : ( $initTagNone ? "checked" : "" ) );?>> None </label>
		</td>
	</tr>
	<tr>
		<!-- ############### PRODUCT'S END USE ############### -->


		<!-- ############### COMPLIANCE REQUIREMENTS ############### -->
		<td>
			<label for='PNS'> <input type='checkbox' value='1' name='chkPNS' id='PNS' onchange='enableOtherReq()' <?php echo( $JRDId ? ( $tag_dev_require_pns ? "checked" : "" ) : ( $initTagPNS ? "checked" : "" ) );?>> PNS </label>
		</td>
		<td>
			<label for='OtherReq'> <input type='checkbox' value='1' name='chkOtherReq' id='OtherReq' onchange='enableOtherReq()' <?php echo( $JRDId ? ( $tag_dev_require_others ? "checked" : "" ) : ( $initTagOtherRequirement ? "checked" : "" ) );?>> Others </label>
		</td>
	</tr>
	<tr>
		<!-- ############### PRODUCT'S END USE ############### -->


		<!-- ############### COMPLIANCE REQUIREMENTS ############### -->
		<td>
			Others Specify: 
		</td>
		<td>
			<input type='text' name='txtOtherReq' id='txtOtherReq' disabled value="<?php echo( $JRDId ? $dev_require_others : $initOtherRequirement );?>">
		</td>
	</tr>
</table>		

<?php
	require("/init_unset_values/jrd_unset_value.php");
	require("/database_close.php");
?>