<?php
	$transDate = date('Y-m-d');
	$PREDate ='';
	$DateRec = '';
	$DateNeed = '';
	$DateNRM = date('Y-m-d');
	$PONum = '';
	$RefNum = '';
	$RefType = '';
	$SupplyTypeId = '0';
	$SupplierId = '0';
	$Comments = '';
	$SRNum = '';
	$MiscNum = '';
	$IssType = '0';
	$IssTo = '0';
	$IssToUser = 0;
	$NewSupplyType = '';
	$Cmpds = '0';
	$Pips = '0';
	$Corp = '0';
	$Active = '0';
	$UomCode = '';
	$Description = '';
	$Rm = '0';
	$Fg = '0';
	$Supplies = '0';
	$Username = '';
	$Email = '';
	$Fname = '';
	$Lname = '';
	$Dept = 0;
	$Department = '';
	$Code = '';
	$Supplier = '';
	$Name = '';
	$SupplyType = '0';
	$Supply = '';
	$Uom = '0';
	$Permission = '';
	$Module = '';
	$PRENo = '';
	$Stock = '0';
	$Direct = '0';
	$Capex = '0';
	$Other = '0';
	$OtherDiv = '';
	$CapexNo = '';
	$CapexName = '';
	$OtherPurpose = '';
	$Status = '0';
	$Equipment = '';
	$Measure = '0';
	$Model = '';
	$Location = '0';
	$TolerancePercent = '';
	$ToleranceValue = '';
	$InputRange = '';
	$OutputRange = '';
	$Frequency = '';
	$Remarks = '';
	$NRMNo ='';
	$FG ='0';
	$FormulaType ='0';
	$Date ='';
	$LotNo ='';
	$TrialDate = NULL;
	$DarNo ='';
	$DH ='';
	$Z1 ='';
	$Z2 ='';
	$ScrewSpeed ='';
	$CutterSpeed ='';
	$Multiplier ='';
	$NewRm ='';
	$NPHR ='';
	$NQuantity = '';
	$TotalQty = '';
	$NrmDate = date('Y-m-d');
	$TERNo = '';
	$SchedType = 0;
	$LMRNo = '';
	$iDSQ = '';
	$DepartureHr = '';
	$DepartureMin = '';
	$ArrivalHr = '';
	$ArrivalMin = '';
	$KMReadingIn = '';
	$KMReadingOut = '';
	$DieselConsumed = '';
	$initLMRUnitPrice = '';
	$DateApproved = NULL;
	$UnderloadRemarks = '';
	$SampleReplaceRemarks = NULL;
	$EngItem = 0;
	$RecordDate = '';
	#######
	$CPRNo = '';
	$CPRDate = NULL;
	$DueDate = NULL;
	$IssueDeptID = 0;
	$IssuerID = 0;
	$ReceiveDeptID = 0;
	$ReceiverID = 0;
	$Occurrence = 0;
	$Criticality = 0;
	$Source = 0;
	$Clause = '';
	$Details = '';
	$MemoDate = NULL;
	$FinalDueDate = NULL;
	$ReceiveDateDCC = NULL;
	$RootCause = '';
	$Containment = '';
	$ImplementContainment = '';
	$Action = 0;
	$CorrectPrevent = '';
	$ImplementCorrectPrevent = '';
	$Man = 0;
	$Method = 0;
	$Machine = 0;
	$Material = 0;
	$Measurement = 0;
	$MotherNature = 0;
	$Verification = '';
	###########
	$ICCNo = '';
	$ICCDate = NULL;
	$NatureOfComplaint = '';
	$ContainmentAction = '';
	$ContainmentImplement = '';
	$ContainmentResponsible = '';
	$CorrectiveAction = '';
	$CorrectiveImplementation = '';
	$CorrectiveResponsible = '';
	#####
	$counter = 0;
	$SOType = '';
	$Customer = 0;
	$SONumber = '';
	$SODate = NULL;
	$PONumber = '';
	$PODate = NULL;
	$Remarks = '';
	$Tag = 0;

	$Quantity[0] = ''; $Quantity[1] = ''; $Quantity[2] = ''; $Quantity[3] = ''; $Quantity[4] = '';
	$Quantity[5] = ''; $Quantity[6] = ''; $Quantity[7] = ''; $Quantity[8] = ''; $Quantity[9] = '';
	$Quantity[10] = ''; $Quantity[11] = ''; $Quantity[12] = ''; $Quantity[13] = ''; $Quantity[14] = '';
	$Quantity[15] = ''; $Quantity[16] = ''; $Quantity[17] = ''; $Quantity[18] = ''; $Quantity[19] = '';
	$Quantity[20] = ''; $Quantity[21] = ''; $Quantity[22] = ''; $Quantity[23] = ''; $Quantity[24] = '';
	$Quantity[25] = ''; $Quantity[26] = ''; $Quantity[27] = ''; $Quantity[28] = ''; $Quantity[29] = '';

	$UnitPrice[0] = ''; $UnitPrice[1] = ''; $UnitPrice[2] = ''; $UnitPrice[3] = ''; $UnitPrice[4] = '';
	$UnitPrice[5] = ''; $UnitPrice[6] = ''; $UnitPrice[7] = ''; $UnitPrice[8] = ''; $UnitPrice[9] = '';
	$UnitPrice[10] = ''; $UnitPrice[11] = ''; $UnitPrice[12] = ''; $UnitPrice[13] = ''; $UnitPrice[14] = '';
	$UnitPrice[15] = ''; $UnitPrice[16] = ''; $UnitPrice[17] = ''; $UnitPrice[18] = ''; $UnitPrice[19] = '';
	$UnitPrice[20] = ''; $UnitPrice[21] = ''; $UnitPrice[22] = ''; $UnitPrice[23] = ''; $UnitPrice[24] = '';
	$UnitPrice[25] = ''; $UnitPrice[26] = ''; $UnitPrice[27] = ''; $UnitPrice[28] = ''; $UnitPrice[29] = '';
	
	$RequiredDate[0] = ''; $RequiredDate[1] = ''; $RequiredDate[2] = ''; $RequiredDate[3] = ''; $RequiredDate[4] = '';
	$RequiredDate[5] = ''; $RequiredDate[6] = ''; $RequiredDate[7] = ''; $RequiredDate[8] = ''; $RequiredDate[9] = '';
	$RequiredDate[10] = ''; $RequiredDate[11] = ''; $RequiredDate[12] = ''; $RequiredDate[13] = ''; $RequiredDate[14] = '';
	$RequiredDate[15] = ''; $RequiredDate[16] = ''; $RequiredDate[17] = ''; $RequiredDate[18] = ''; $RequiredDate[19] = '';
	$RequiredDate[20] = ''; $RequiredDate[21] = ''; $RequiredDate[22] = ''; $RequiredDate[23] = ''; $RequiredDate[24] = '';
	$RequiredDate[25] = ''; $RequiredDate[26] = ''; $RequiredDate[27] = ''; $RequiredDate[28] = ''; $RequiredDate[29] = '';
	
###########
	$PropertyType = '0';
	$PropertyName = '';

###########
	$MonthYear = '';
	$FGType = 2;
	$counter1 = 0;
	$FGItem[0] = 0; $FGItem[1] = 0; $FGItem[2] = 0; $FGItem[3] = 0; $FGItem[4] = 0;
	$FGItem[5] = 0; $FGItem[6] = 0; $FGItem[7] = 0; $FGItem[8] = 0; $FGItem[9] = 0;
	$FGItem[10] = 0; $FGItem[11] = 0; $FGItem[12] = 0; $FGItem[13] = 0; $FGItem[14] = 0;
	$FGItem[15] = 0; $FGItem[16] = 0; $FGItem[17] = 0; $FGItem[18] = 0; $FGItem[19] = 0;

##########
	$counter2 = 0;
	$InventorySource = '0';
	$ReferenceNo = '';
	$ReferenceDate = NULL;
	// $AssetType = 0;
	$ReceivedType = '0'; 
	$ReceivedFrom = 0;
	$Purpose = '';
	$PTSNo = ''; 
	$ToDivision = '0'; 
	$ToDepartment = 0; 
	$FromUser = 0; 
	$ToUser = 0; 
	#####
	$NRMDate = NULL;
	$MaterialDescription = '';
	// $Supplier = 0;
	$OtherSupplierTxt = '';
	$OtherSupplierRad =  0;
	$Class = '';
	$OtherClass = '';
	$Sample = '';
	$OtherSample = '';
	$MSDS = 0;
	$TDS = 0;
	$Remarks = '';

	$QuantityIssued[0] = ''; $QuantityIssued[1] = ''; $QuantityIssued[2] = ''; $QuantityIssued[3] = ''; $QuantityIssued[4] = '';
	$QuantityIssued[5] = ''; $QuantityIssued[6] = ''; $QuantityIssued[7] = ''; $QuantityIssued[8] = ''; $QuantityIssued[9] = '';

	$QuantityReceived[0] = ''; $QuantityReceived[1] = ''; $QuantityReceived[2] = ''; $QuantityReceived[3] = ''; $QuantityReceived[4] = '';
	$QuantityReceived[5] = ''; $QuantityReceived[6] = ''; $QuantityReceived[7] = ''; $QuantityReceived[8] = ''; $QuantityReceived[9] = '';

	$UnitPrice[0] = ''; $UnitPrice[1] = ''; $UnitPrice[2] = ''; $UnitPrice[3] = ''; $UnitPrice[4] = '';
	$UnitPrice[5] = ''; $UnitPrice[6] = ''; $UnitPrice[7] = ''; $UnitPrice[8] = ''; $UnitPrice[9] = '';

	$Remarks1[0] = ''; $Remarks1[1] = ''; $Remarks1[2] = ''; $Remarks1[3] = ''; $Remarks1[4] = '';
	$Remarks1[5] = ''; $Remarks1[6] = ''; $Remarks1[7] = ''; $Remarks1[8] = ''; $Remarks1[9] = '';

	$AssetType[0] = ''; $AssetType[1] = ''; $AssetType[2] = ''; $AssetType[3] = ''; $AssetType[4] = '';
	$AssetType[5] = ''; $AssetType[6] = ''; $AssetType[7] = ''; $AssetType[8] = ''; $AssetType[9] = '';

	$RefPTSNumber[0] = ''; $RefPTSNumber[1] = ''; $RefPTSNumber[2] = ''; $RefPTSNumber[3] = ''; $RefPTSNumber[4] = '';
	$RefPTSNumber[5] = ''; $RefPTSNumber[6] = ''; $RefPTSNumber[7] = ''; $RefPTSNumber[8] = ''; $RefPTSNumber[9] = '';

	$RefRRNumber[0] = ''; $RefRRNumber[1] = ''; $RefRRNumber[2] = ''; $RefRRNumber[3] = ''; $RefRRNumber[4] = '';
	$RefRRNumber[5] = ''; $RefRRNumber[6] = ''; $RefRRNumber[7] = ''; $RefRRNumber[8] = ''; $RefRRNumber[9] = '';

##########
	$NRM = NULL;
	$Recommendation = '';
	$FromSpecificGravity = NULL;
	$ToSpecificGravity = NULL;
	$FromHardnessA = NULL;
	$ToHardnessA = NULL;
	$FromHardnessD = NULL;
	$ToHardnessD = NULL;
	$VolumeResistivity = NULL;
	$UnagedTS = NULL;
	$OvenTS = NULL;
	$OilTS = NULL;
	$UnagedE = NULL;
	$OvenE = NULL;
	$OilE = NULL;
		
###########
	$CustType = NULL;
	$TermsDays = NULL;
	$TermsType = NULL;
	$CreditLimitUnit = NULL;
	$CreditLimit = NULL;
	$Agent = NULL;
	$AddressStreet = NULL;
	$AddressBaranggay = NULL;
	$AddressCityProvince = NULL;
	$ZipCode = NULL;
############
	$Customer = NULL;
	$TransDate = NULL;
	$TransType = NULL;
	$InvoiceNo = NULL;
	$CheckNo = NULL;
	$Amount = NULL;
############
	$Compounds = 0;
	$Pipes = 0;
	$Corporate = 0;
	$PPR = 0;



	if (isset($_SESSION['transDate'])){
		$transDate = $_SESSION['transDate'];
		unset($_SESSION['transDate']);
	}
		
	if (isset($_SESSION['txtPONum'])){
		$PONum = $_SESSION['txtPONum'];
		unset($_SESSION['txtPONum']);
	}

	if (isset($_SESSION['txtRefNum'])){
		$RefNum = $_SESSION['txtRefNum'];
		unset($_SESSION['txtRefNum']);
	}
	
	if (isset($_SESSION['sltRefType'])){
		$RefType = $_SESSION['sltRefType'];
		unset($_SESSION['sltRefType']);
	}
	
	if (isset($_SESSION['sltSupplyTypeId'])){
		$SupplyTypeId = $_SESSION['sltSupplyTypeId'];
		unset($_SESSION['sltSupplyTypeId']);
	}
	
	if (isset($_SESSION['sltSupplierId'])){
		$SupplierId = $_SESSION['sltSupplierId'];
		unset($_SESSION['sltSupplierId']);
	}
	
	if (isset($_SESSION['txtComments'])){
		$Comments = $_SESSION['txtComments'];
		unset($_SESSION['txtComments']);
	}
	
	if (isset($_SESSION['txtSRNum'])){
		$SRNum = $_SESSION['txtSRNum'];
		unset($_SESSION['txtSRNum']);
	}
	
	if (isset($_SESSION['txtMiscNum'])){
		$MiscNum = $_SESSION['txtMiscNum'];
		unset($_SESSION['txtMiscNum']);
	}
	
	if (isset($_SESSION['sltIssType'])){
		$IssType = $_SESSION['sltIssType'];
		unset($_SESSION['sltIssType']);
	}
	
	if (isset($_SESSION['sltIssTo'])){
		$IssTo = $_SESSION['sltIssTo'];
		unset($_SESSION['sltIssTo']);
	}
	
	if (isset($_SESSION['sltIssToUser'])){
		$IssToUser = $_SESSION['sltIssToUser'];
		unset($_SESSION['sltIssToUser']);
	}
	
	if (isset($_SESSION['txtNewSupplyType'])){
		$NewSupplyType = $_SESSION['txtNewSupplyType'];
		unset($_SESSION['txtNewSupplyType']);
	}
	
	if (isset($_SESSION['chkCmpds'])){
		$Cmpds = $_SESSION['chkCmpds'];
		unset($_SESSION['chkCmpds']);
	}
	
	if (isset($_SESSION['chkPips'])){
		$Pips = $_SESSION['chkPips'];
		unset($_SESSION['chkPips']);
	}
	
	if (isset($_SESSION['chkCorp'])){
		$Corp = $_SESSION['chkCorp'];
		unset($_SESSION['chkCorp']);
	}
	
	if (isset($_SESSION['chkActive'])){
		$Active = $_SESSION['chkActive'];
		unset($_SESSION['chkActive']);
	}
	
	if (isset($_SESSION['txtUomCode'])){
		$UomCode = $_SESSION['txtUomCode'];
		unset($_SESSION['txtUomCode']);
	}
	
	if (isset($_SESSION['txtDescription'])){
		$Description = $_SESSION['txtDescription'];
		unset($_SESSION['txtDescription']);
	}
	
	if (isset($_SESSION['chkRm'])){
		$Rm = $_SESSION['chkRm'];
		unset($_SESSION['chkRm']);
	}
	
	if (isset($_SESSION['chkFg'])){
		$Fg = $_SESSION['chkFg'];
		unset($_SESSION['chkFg']);
	}
	
	if (isset($_SESSION['chkSupplies'])){
		$Supplies = $_SESSION['chkSupplies'];
		unset($_SESSION['chkSupplies']);
	}
	
	if (isset($_SESSION['txtUsername'])){
		$Username = $_SESSION['txtUsername'];
		unset($_SESSION['txtUsername']);
	}
	
	if (isset($_SESSION['txtEmail'])){
		$Email = $_SESSION['txtEmail'];
		unset($_SESSION['txtEmail']);
	}
	
	if (isset($_SESSION['txtFname'])){
		$Fname = $_SESSION['txtFname'];
		unset($_SESSION['txtFname']);
	}
	
	if (isset($_SESSION['txtLname'])){
		$Lname = $_SESSION['txtLname'];
		unset($_SESSION['txtLname']);
	}
	
	if (isset($_SESSION['sltDept'])){
		$Dept = $_SESSION['sltDept'];
		unset($_SESSION['sltDept']);
	}
	
	if (isset($_SESSION['txtDepartment'])){
		$Department = $_SESSION['txtDepartment'];
		unset($_SESSION['txtDepartment']);
	}
	
	if (isset($_SESSION['txtCode'])){
		$Code = $_SESSION['txtCode'];
		unset($_SESSION['txtCode']);
	}
	
	if (isset($_SESSION['txtSupplier'])){
		$Supplier = $_SESSION['txtSupplier'];
		unset($_SESSION['txtSupplier']);
	}
	
	if (isset($_SESSION['txtName'])){
		$Name = $_SESSION['txtName'];
		unset($_SESSION['txtName']);
	}
	
	if (isset($_SESSION['sltSupplyType'])){
		$SupplyType = $_SESSION['sltSupplyType'];
		unset($_SESSION['sltSupplyType']);
	}
	
	if (isset($_SESSION['txtSupply'])){
		$Supply = $_SESSION['txtSupply'];
		unset($_SESSION['txtSupply']);
	}
	
	if (isset($_SESSION['sltUom'])){
		$Uom = $_SESSION['sltUom'];
		unset($_SESSION['sltUom']);
	}
	
	if (isset($_SESSION['txtPermission'])){
		$Permission = $_SESSION['txtPermission'];
		unset($_SESSION['txtPermission']);
	}
	
	if (isset($_SESSION['txtModule'])){
		$Module = $_SESSION['txtModule'];
		unset($_SESSION['txtModule']);
	}
	
	if (isset($_SESSION['txtPRENo'])){
		$PRENo = $_SESSION['txtPRENo'];
		unset($_SESSION['txtPRENo']);
	}
	
	if (isset($_SESSION['txtPREDate'])){
		$PREDate = $_SESSION['txtPREDate'];
		unset($_SESSION['txtPREDate']);
	}
	
	if (isset($_SESSION['txtDateRec'])){
		$DateRec = $_SESSION['txtDateRec'];
		unset($_SESSION['txtDateRec']);
	}
	
	if (isset($_SESSION['txtDateNeed'])){
		$DateNeed = $_SESSION['txtDateNeed'];
		unset($_SESSION['txtDateNeed']);
	}
	
	if (isset($_SESSION['chkStock'])){
		$Stock = $_SESSION['chkStock'];
		unset($_SESSION['chkStock']);
	}
	
	if (isset($_SESSION['chkDirec'])){
		$Direct = $_SESSION['chkDirec'];
		unset($_SESSION['chkDirec']);
	}
	
	if (isset($_SESSION['chkCapex'])){
		$Capex = $_SESSION['chkCapex'];
		unset($_SESSION['chkCapex']);
	}
	
	if (isset($_SESSION['chkOther'])){
		$Other = $_SESSION['chkOther'];
		unset($_SESSION['chkOther']);
	}
	
	if (isset($_SESSION['txtOtherDiv'])){
		$OtherDiv = $_SESSION['txtOtherDiv'];
		unset($_SESSION['txtOtherDiv']);
	}
	
	if (isset($_SESSION['txtCapexNo'])){
		$CapexNo = $_SESSION['txtCapexNo'];
		unset($_SESSION['txtCapexNo']);
	}
	
	if (isset($_SESSION['txtCapexName'])){
		$CapexName = $_SESSION['txtCapexName'];
		unset($_SESSION['txtCapexName']);
	}
	
	if (isset($_SESSION['txtOther'])){
		$OtherPurpose = $_SESSION['txtOther'];
		unset($_SESSION['txtOther']);
	}
	
	if (isset($_SESSION['sltStatus'])){
		$Status = $_SESSION['sltStatus'];
		unset($_SESSION['sltStatus']);
	}
	
	if (isset($_SESSION['txtEquipment'])){
		$Equipment = $_SESSION['txtEquipment'];
		unset($_SESSION['txtEquipment']);
	}
	
	if (isset($_SESSION['sltMeasure'])){
		$Measure = $_SESSION['sltMeasure'];
		unset($_SESSION['sltMeasure']);
	}
	
	if (isset($_SESSION['txtModel'])){
		$Model = $_SESSION['txtModel'];
		unset($_SESSION['txtModel']);
	}
	
	if (isset($_SESSION['sltLocation'])){
		$Location = $_SESSION['sltLocation'];
		unset($_SESSION['sltLocation']);
	}
	
	if (isset($_SESSION['txtTolerancePercent'])){
		$TolerancePercent = $_SESSION['txtTolerancePercent'];
		unset($_SESSION['txtTolerancePercent']);
	}
	
	if (isset($_SESSION['txtToleranceValue'])){
		$ToleranceValue = $_SESSION['txtToleranceValue'];
		unset($_SESSION['txtToleranceValue']);
	}
	
	if (isset($_SESSION['txtInputRange'])){
		$InputRange = $_SESSION['txtInputRange'];
		unset($_SESSION['txtInputRange']);
	}
	
	if (isset($_SESSION['txtOutputRange'])){
		$OutputRange = $_SESSION['txtOutputRange'];
		unset($_SESSION['txtOutputRange']);
	}
	
	if (isset($_SESSION['sltFrequency'])){
		$Frequency = $_SESSION['sltFrequency'];
		unset($_SESSION['sltFrequency']);
	}
	
	if (isset($_SESSION['sltFrequency'])){
		$Frequency = $_SESSION['sltFrequency'];
		unset($_SESSION['sltFrequency']);
	}
	
	if (isset($_SESSION['txtRemarks'])){
		$Remarks = $_SESSION['txtRemarks'];
		unset($_SESSION['txtRemarks']);
	}
	
	if (isset($_SESSION['txtNRMNo'])){
		$NRMNo = $_SESSION['txtNRMNo'];
		unset($_SESSION['txtNRMNo']);
	}
	
	if (isset($_SESSION['sltFG'])){
		$FG = $_SESSION['sltFG'];
		unset($_SESSION['sltFG']);
	}
	
	if (isset($_SESSION['sltFormulaType'])){
		$FormulaType = $_SESSION['sltFormulaType'];
		unset($_SESSION['sltFormulaType']);
	}
	
	if (isset($_SESSION['txtDate'])){
		$Date = $_SESSION['txtDate'];
		unset($_SESSION['txtDate']);
	}
	
	if (isset($_SESSION['txtLotNo'])){
		$LotNo = $_SESSION['txtLotNo'];
		unset($_SESSION['txtLotNo']);
	}
	
	if (isset($_SESSION['txtTrialDate'])){
		$TrialDate = $_SESSION['txtTrialDate'];
		unset($_SESSION['txtTrialDate']);
	}
	
	if (isset($_SESSION['txtDarNo'])){
		$DarNo = $_SESSION['txtDarNo'];
		unset($_SESSION['txtDarNo']);
	}
	
	if (isset($_SESSION['txtDH'])){
		$DH = $_SESSION['txtDH'];
		unset($_SESSION['txtDH']);
	}
	
	if (isset($_SESSION['txtZ1'])){
		$Z1 = $_SESSION['txtZ1'];
		unset($_SESSION['txtZ1']);
	}
	
	if (isset($_SESSION['txtZ2'])){
		$Z2 = $_SESSION['txtZ2'];
		unset($_SESSION['txtZ2']);
	}
	
	if (isset($_SESSION['txtScrewSpeed'])){
		$ScrewSpeed = $_SESSION['txtScrewSpeed'];
		unset($_SESSION['txtScrewSpeed']);
	}
	
	if (isset($_SESSION['txtCutterSpeed'])){
		$CutterSpeed = $_SESSION['txtCutterSpeed'];
		unset($_SESSION['txtCutterSpeed']);
	}
	
	if (isset($_SESSION['txtMultiplier'])){
		$Multiplier = $_SESSION['txtMultiplier'];
		unset($_SESSION['txtMultiplier']);
	}
	
	if (isset($_SESSION['txtNewRm'])){
		$NewRm = $_SESSION['txtNewRm'];
		unset($_SESSION['txtNewRm']);
	}
	
	if (isset($_SESSION['txtNPHR'])){
		$NPHR = $_SESSION['txtNPHR'];
		unset($_SESSION['txtNPHR']);
	}
	
	if (isset($_SESSION['txtNQuantity'])){
		$NQuantity = $_SESSION['txtNQuantity'];
		unset($_SESSION['txtNQuantity']);
	}
	
	if (isset($_SESSION['txtTotalQty'])){
		$TotalQty = $_SESSION['txtTotalQty'];
		unset($_SESSION['txtTotalQty']);
	}
	
	if (isset($_SESSION['txtNrmDate'])){
		$NrmDate = $_SESSION['txtNrmDate'];
		unset($_SESSION['txtNrmDate']);
	}
	
	if (isset($_SESSION['txtTERNo'])){
		$TERNo = $_SESSION['txtTERNo'];
		unset($_SESSION['txtTERNo']);
	}
	
	if (isset($_SESSION['sltSchedType'])){
		$SchedType = $_SESSION['sltSchedType'];
		unset($_SESSION['sltSchedType']);
	}
######
	if (isset($_SESSION['txtLMRNo'])){
		$LMRNo = $_SESSION['txtLMRNo'];
		unset($_SESSION['txtLMRNo']);
	}
	
	if (isset($_SESSION['sltDSQ'])){
		$iDSQ = $_SESSION['sltDSQ'];
		unset($_SESSION['sltDSQ']);
	}
	
	if (isset($_SESSION['txtDepartureHr'])){
		$DepartureHr = $_SESSION['txtDepartureHr'];
		unset($_SESSION['txtDepartureHr']);
	}
	
	if (isset($_SESSION['txtDepartureMin'])){
		$DepartureMin = $_SESSION['txtDepartureMin'];
		unset($_SESSION['txtDepartureMin']);
	}
	
	if (isset($_SESSION['txtArrivalHr'])){
		$ArrivalHr = $_SESSION['txtArrivalHr'];
		unset($_SESSION['txtArrivalHr']);
	}
	
	if (isset($_SESSION['txtArrivalMin'])){
		$ArrivalMin = $_SESSION['txtArrivalMin'];
		unset($_SESSION['txtArrivalMin']);
	}
	
	if (isset($_SESSION['txtKMReadingIn'])){
		$KMReadingIn = $_SESSION['txtKMReadingIn'];
		unset($_SESSION['txtKMReadingIn']);
	}
	
	if (isset($_SESSION['txtKMReadingOut'])){
		$KMReadingOut = $_SESSION['txtKMReadingOut'];
		unset($_SESSION['txtKMReadingOut']);
	}
	
	if (isset($_SESSION['txtDieselConsumed'])){
		$DieselConsumed = $_SESSION['txtDieselConsumed'];
		unset($_SESSION['txtDieselConsumed']);
	}
	
	if (isset($_SESSION['SESS_LMR_UnitPrice'])){
		$initLMRUnitPrice = $_SESSION['SESS_LMR_UnitPrice'];
		unset($_SESSION['SESS_LMR_UnitPrice']);
	}
######
	if (isset($_SESSION['txtDateApproved'])){
		$DateApproved = $_SESSION['txtDateApproved'];
		unset($_SESSION['txtDateApproved']);
	}
######
	if (isset($_SESSION['txtUnderloadRemarks'])){
		$UnderloadRemarks = $_SESSION['txtUnderloadRemarks'];
		unset($_SESSION['txtUnderloadRemarks']);
	}
	if (isset($_SESSION['txtSampleReplaceRemarks'])){
		$SampleReplaceRemarks = $_SESSION['txtSampleReplaceRemarks'];
		unset($_SESSION['txtSampleReplaceRemarks']);
	}
######
	if (isset($_SESSION['sltEngItem'])){
		$EngItem = $_SESSION['sltEngItem'];
		unset($_SESSION['sltEngItem']);
	}
	if (isset($_SESSION['txtRecordDate'])){
		$RecordDate = $_SESSION['txtRecordDate'];
		unset($_SESSION['txtRecordDate']);
	}
######
	if (isset($_SESSION['txtCPRNo'])){
		$CPRNo = $_SESSION['txtCPRNo'];
		unset($_SESSION['txtCPRNo']);
	}
	if (isset($_SESSION['txtCPRDate'])){
		$CPRDate = $_SESSION['txtCPRDate'];
		unset($_SESSION['txtCPRDate']);
	}
	if (isset($_SESSION['txtDueDate'])){
		$DueDate = $_SESSION['txtDueDate'];
		unset($_SESSION['txtDueDate']);
	}
	if (isset($_SESSION['sltIssueDeptID'])){
		$IssueDeptID = $_SESSION['sltIssueDeptID'];
		unset($_SESSION['sltIssueDeptID']);
	}
	if (isset($_SESSION['sltIssuerID'])){
		$IssuerID = $_SESSION['sltIssuerID'];
		unset($_SESSION['sltIssuerID']);
	}
	if (isset($_SESSION['sltReceiveDeptID'])){
		$ReceiveDeptID = $_SESSION['sltReceiveDeptID'];
		unset($_SESSION['sltReceiveDeptID']);
	}
	if (isset($_SESSION['sltReceiverID'])){
		$ReceiverID = $_SESSION['sltReceiverID'];
		unset($_SESSION['sltReceiverID']);
	}
	if (isset($_SESSION['chkOccurrence'])){
		$Occurrence = $_SESSION['chkOccurrence'];
		unset($_SESSION['chkOccurrence']);
	}
	if (isset($_SESSION['chkCriticality'])){
		$Criticality = $_SESSION['chkCriticality'];
		unset($_SESSION['chkCriticality']);
	}
	if (isset($_SESSION['chkSource'])){
		$Source = $_SESSION['chkSource'];
		unset($_SESSION['chkSource']);
	}
	if (isset($_SESSION['txtClause'])){
		$Clause = $_SESSION['txtClause'];
		unset($_SESSION['txtClause']);
	}
	if (isset($_SESSION['txtDetails'])){
		$Details = $_SESSION['txtDetails'];
		unset($_SESSION['txtDetails']);
	}
	if (isset($_SESSION['txtMemoDate'])){
		$MemoDate = $_SESSION['txtMemoDate'];
		unset($_SESSION['txtMemoDate']);
	}
	if (isset($_SESSION['txtFinalDueDate'])){
		$FinalDueDate = $_SESSION['txtFinalDueDate'];
		unset($_SESSION['txtFinalDueDate']);
	}
	if (isset($_SESSION['txtReceiveDateDCC'])){
		$ReceiveDateDCC = $_SESSION['txtReceiveDateDCC'];
		unset($_SESSION['txtReceiveDateDCC']);
	}
	if (isset($_SESSION['txtRootCause'])){
		$RootCause = $_SESSION['txtRootCause'];
		unset($_SESSION['txtRootCause']);
	}
	if (isset($_SESSION['txtContainment'])){
		$Containment = $_SESSION['txtContainment'];
		unset($_SESSION['txtContainment']);
	}
	if (isset($_SESSION['txtImplementContainment'])){
		$ImplementContainment = $_SESSION['txtImplementContainment'];
		unset($_SESSION['txtImplementContainment']);
	}
	if (isset($_SESSION['chkAction'])){
		$Action = $_SESSION['chkAction'];
		unset($_SESSION['chkAction']);
	}
	if (isset($_SESSION['txtCorrectPrevent'])){
		$CorrectPrevent = $_SESSION['txtCorrectPrevent'];
		unset($_SESSION['txtCorrectPrevent']);
	}
	if (isset($_SESSION['txtImplementCorrectPrevent'])){
		$ImplementCorrectPrevent = $_SESSION['txtImplementCorrectPrevent'];
		unset($_SESSION['txtImplementCorrectPrevent']);
	}
	if (isset($_SESSION['chkMan'])){
		$Man = $_SESSION['chkMan'];
		unset($_SESSION['chkMan']);
	}
	if (isset($_SESSION['chkMethod'])){
		$Method = $_SESSION['chkMethod'];
		unset($_SESSION['chkMethod']);
	}
	if (isset($_SESSION['chkMachine'])){
		$Machine = $_SESSION['chkMachine'];
		unset($_SESSION['chkMachine']);
	}
	if (isset($_SESSION['chkMaterial'])){
		$Material = $_SESSION['chkMaterial'];
		unset($_SESSION['chkMaterial']);
	}
	if (isset($_SESSION['chkMeasurement'])){
		$Measurement = $_SESSION['chkMeasurement'];
		unset($_SESSION['chkMeasurement']);
	}
	if (isset($_SESSION['chkMotherNature'])){
		$MotherNature = $_SESSION['chkMotherNature'];
		unset($_SESSION['chkMotherNature']);
	}
	#############
	if (isset($_SESSION['txtICCNo'])){
		$ICCNo = $_SESSION['txtICCNo'];
		unset($_SESSION['txtICCNo']);
	}
	if (isset($_SESSION['txtICCDate'])){
		$ICCDate = $_SESSION['txtICCDate'];
		unset($_SESSION['txtICCDate']);
	}
	if (isset($_SESSION['txtNatureOfComplaint'])){
		$NatureOfComplaint = $_SESSION['txtNatureOfComplaint'];
		unset($_SESSION['txtNatureOfComplaint']);
	}
	if (isset($_SESSION['txtContainmentAction'])){
		$ContainmentAction = $_SESSION['txtContainmentAction'];
		unset($_SESSION['txtContainmentAction']);
	}
	if (isset($_SESSION['txtContainmentImplement'])){
		$ContainmentImplement = $_SESSION['txtContainmentImplement'];
		unset($_SESSION['txtContainmentImplement']);
	}
	if (isset($_SESSION['txtContainmentResponsible'])){
		$ContainmentResponsible = $_SESSION['txtContainmentResponsible'];
		unset($_SESSION['txtContainmentResponsible']);
	}
	if (isset($_SESSION['txtCorrectiveAction'])){
		$CorrectiveAction = $_SESSION['txtCorrectiveAction'];
		unset($_SESSION['txtCorrectiveAction']);
	}
	if (isset($_SESSION['txtCorrectiveImplementation'])){
		$CorrectiveImplementation = $_SESSION['txtCorrectiveImplementation'];
		unset($_SESSION['txtCorrectiveImplementation']);
	}
	if (isset($_SESSION['txtCorrectiveResponsible'])){
		$CorrectiveResponsible = $_SESSION['txtCorrectiveResponsible'];
		unset($_SESSION['txtCorrectiveResponsible']);
	}
#######
	if (isset($_SESSION['counter'])){
		$counter = $_SESSION['counter'];
		unset($_SESSION['counter']);
	}
	if (isset($_SESSION['sltSOType'])){
		$SOType = $_SESSION['sltSOType'];
		unset($_SESSION['sltSOType']);
	}
	if (isset($_SESSION['sltCustomer'])){
		$Customer = $_SESSION['sltCustomer'];
		unset($_SESSION['sltCustomer']);
	}
	if (isset($_SESSION['txtSONumber'])){
		$SONumber = $_SESSION['txtSONumber'];
		unset($_SESSION['txtSONumber']);
	}
	if (isset($_SESSION['txtSODate'])){
		$SODate = $_SESSION['txtSODate'];
		unset($_SESSION['txtSODate']);
	}
	if (isset($_SESSION['txtPONumber'])){
		$PONumber = $_SESSION['txtPONumber'];
		unset($_SESSION['txtPONumber']);
	}
	if (isset($_SESSION['txtPODate'])){
		$PODate = $_SESSION['txtPODate'];
		unset($_SESSION['txtPODate']);
	}
	if (isset($_SESSION['txtRemarks'])){
		$Remarks = $_SESSION['txtRemarks'];
		unset($_SESSION['txtRemarks']);
	}
	if (isset($_SESSION['hidTag'])){
		$Tag = $_SESSION['hidTag'];
		unset($_SESSION['hidTag']);
	}

	for ( $i = 0; $i < $counter; $i++ ){
		if ( isset( $_SESSION['sltProduct'][$i] ) ){
			$Product[$i] = $_SESSION['sltProduct'][$i];
			unset($_SESSION['sltProduct'][$i]);
		}
		if ( isset( $_SESSION['txtQuantity'][$i] ) ){
			$Quantity[$i] = $_SESSION['txtQuantity'][$i];
			unset($_SESSION['txtQuantity'][$i]);
		}
		if ( isset( $_SESSION['txtUnitPrice'][$i] ) ){
			$UnitPrice[$i] = $_SESSION['txtUnitPrice'][$i];
			unset($_SESSION['txtUnitPrice'][$i]);
		}
		if ( isset( $_SESSION['txtRequiredDate'][$i] ) ){
			$RequiredDate[$i] = $_SESSION['txtRequiredDate'][$i];
			unset($_SESSION['txtRequiredDate'][$i]);
		}
	}

#########

	if (isset($_SESSION['sltPropertyType'])){
		$PropertyType = $_SESSION['sltPropertyType'];
		unset($_SESSION['sltPropertyType']);
	}
	if (isset($_SESSION['txtPropertyName'])){
		$PropertyName = $_SESSION['txtPropertyName'];
		unset($_SESSION['txtPropertyName']);
	}

#########

	if (isset($_SESSION['txtMonthYear'])){
		$MonthYear = $_SESSION['txtMonthYear'];
		unset($_SESSION['txtMonthYear']);
	}
	if (isset($_SESSION['radFGType'])){
		$FGType = $_SESSION['radFGType'];
		unset($_SESSION['radFGType']);
	}
	if (isset($_SESSION['counter1'])){
		$counter1 = $_SESSION['counter1'];
		unset($_SESSION['counter1']);
	}

	for ( $i = 0; $i < $counter1; $i++ ){
		if ( isset( $_SESSION['sltFGItem'][$i] ) ){
			$FGItem[$i] = $_SESSION['sltFGItem'][$i];
			unset($_SESSION['sltFGItem'][$i]);
		}
		if ( isset( $_SESSION['txtQuantity'][$i] ) ){
			$Quantity[$i] = $_SESSION['txtQuantity'][$i];
			unset($_SESSION['txtQuantity'][$i]);
		}
	}

#########
	if (isset($_SESSION['counter2'])){
		$counter2 = $_SESSION['counter2'];
		unset($_SESSION['counter2']);
	}
	if (isset($_SESSION['sltInventorySource'])){
		$InventorySource = $_SESSION['sltInventorySource'];
		unset($_SESSION['sltInventorySource']);
	}
	if (isset($_SESSION['txtReferenceNo'])){
		$ReferenceNo = $_SESSION['txtReferenceNo'];
		unset($_SESSION['txtReferenceNo']);
	}
	if (isset($_SESSION['txtReferenceDate'])){
		$ReferenceDate = $_SESSION['txtReferenceDate'];
		unset($_SESSION['txtReferenceDate']);
	}
	// if (isset($_SESSION['sltAssetType'])){
	// 	$AssetType = $_SESSION['sltAssetType'];
	// 	unset($_SESSION['sltAssetType']);
	// }
	if (isset($_SESSION['sltReceivedType'])){
		$ReceivedType = $_SESSION['sltReceivedType'];
		unset($_SESSION['sltReceivedType']);
	}
	if (isset($_SESSION['sltReceivedFrom'])){
		$ReceivedFrom = $_SESSION['sltReceivedFrom'];
		unset($_SESSION['sltReceivedFrom']);
	}
	if (isset($_SESSION['txtPurpose'])){
		$Purpose = $_SESSION['txtPurpose'];
		unset($_SESSION['txtPurpose']);
	}
	if (isset($_SESSION['txtPTSNo'])){
		$PTSNo = $_SESSION['txtPTSNo'];
		unset($_SESSION['txtPTSNo']);
	}
	if (isset($_SESSION['radToDivision'])){
		$ToDivision = $_SESSION['radToDivision'];
		unset($_SESSION['radToDivision']);
	}
	if (isset($_SESSION['sltToDepartment'])){
		$ToDepartment = $_SESSION['sltToDepartment'];
		unset($_SESSION['sltToDepartment']);
	}
	if (isset($_SESSION['sltFromUser'])){
		$FromUser = $_SESSION['sltFromUser'];
		unset($_SESSION['sltFromUser']);
	}
	if (isset($_SESSION['sltToUser'])){
		$ToUser = $_SESSION['sltToUser'];
		unset($_SESSION['sltToUser']);
	}

	for ( $i = 0; $i < $counter2; $i++ ){
		if ( isset( $_SESSION['sltAsset'][$i] ) ){
			$Asset[$i] = $_SESSION['sltAsset'][$i];
			unset($_SESSION['sltAsset'][$i]);
		}
		if ( isset( $_SESSION['sltAssetSpecification'][$i] ) ){
			$AssetSpecification[$i] = $_SESSION['sltAssetSpecification'][$i];
			unset($_SESSION['sltAssetSpecification'][$i]);
		}
		if ( isset( $_SESSION['txtQuantityReceived'][$i] ) ){
			$QuantityReceived[$i] = $_SESSION['txtQuantityReceived'][$i];
			unset($_SESSION['txtQuantityReceived'][$i]);
		}
		if ( isset( $_SESSION['txtUnitPrice'][$i] ) ){
			$UnitPrice[$i] = $_SESSION['txtUnitPrice'][$i];
			unset($_SESSION['txtUnitPrice'][$i]);
		}
		if ( isset( $_SESSION['txtRemarks'][$i] ) ){
			$Remarks1[$i] = $_SESSION['txtRemarks'][$i];
			unset($_SESSION['txtRemarks'][$i]);
		}
		if ( isset( $_SESSION['txtQuantityIssued'][$i] ) ){
			$QuantityIssued[$i] = $_SESSION['txtQuantityIssued'][$i];
			unset($_SESSION['txtQuantityIssued'][$i]);
		}
		if ( isset( $_SESSION['sltAssetType'][$i] ) ){
			$AssetType[$i] = $_SESSION['sltAssetType'][$i];
			unset( $_SESSION['sltAssetType'][$i] );
		}
		if ( isset( $_SESSION['txtRefPTSNumber'][$i] ) ){
			$RefPTSNumber[$i] = $_SESSION['txtRefPTSNumber'][$i];
			unset($_SESSION['txtRefPTSNumber'][$i]);
		}
		if ( isset( $_SESSION['txtRefRRNumber'][$i] ) ){
			$RefRRNumber[$i] = $_SESSION['txtRefRRNumber'][$i];
			unset($_SESSION['txtRefRRNumber'][$i]);
		}
	}
######
	if (isset($_SESSION['sltNRM'])){
		$NRM = $_SESSION['sltNRM'];
		unset($_SESSION['sltNRM']);
	}
	if (isset($_SESSION['txtRecommendation'])){
		$Recommendation = $_SESSION['txtRecommendation'];
		unset($_SESSION['txtRecommendation']);
	}
	if (isset($_SESSION['txtFromSpecificGravity'])){
		$FromSpecificGravity = $_SESSION['txtFromSpecificGravity'];
		unset($_SESSION['txtFromSpecificGravity']);
	}
	if (isset($_SESSION['txtToSpecificGravity'])){
		$ToSpecificGravity = $_SESSION['txtToSpecificGravity'];
		unset($_SESSION['txtToSpecificGravity']);
	}
	if (isset($_SESSION['txtFromHardnessA'])){
		$FromHardnessA = $_SESSION['txtFromHardnessA'];
		unset($_SESSION['txtFromHardnessA']);
	}
	if (isset($_SESSION['txtToHardnessA'])){
		$ToHardnessA = $_SESSION['txtToHardnessA'];
		unset($_SESSION['txtToHardnessA']);
	}
	if (isset($_SESSION['txtFromHardnessD'])){
		$FromHardnessD = $_SESSION['txtFromHardnessD'];
		unset($_SESSION['txtFromHardnessD']);
	}
	if (isset($_SESSION['txtToHardnessD'])){
		$ToHardnessD = $_SESSION['txtToHardnessD'];
		unset($_SESSION['txtToHardnessD']);
	}
	if (isset($_SESSION['txtVolumeResistivity'])){
		$VolumeResistivity = $_SESSION['txtVolumeResistivity'];
		unset($_SESSION['txtVolumeResistivity']);
	}
	if (isset($_SESSION['txtUnagedTS'])){
		$UnagedTS = $_SESSION['txtUnagedTS'];
		unset($_SESSION['txtUnagedTS']);
	}
	if (isset($_SESSION['txtOvenTS'])){
		$OvenTS = $_SESSION['txtOvenTS'];
		unset($_SESSION['txtOvenTS']);
	}
	if (isset($_SESSION['txtOilTS'])){
		$OilTS = $_SESSION['txtOilTS'];
		unset($_SESSION['txtOilTS']);
	}
	if (isset($_SESSION['txtUnagedE'])){
		$UnagedE = $_SESSION['txtUnagedE'];
		unset($_SESSION['txtUnagedE']);
	}
	if (isset($_SESSION['txtOvenE'])){
		$OvenE = $_SESSION['txtOvenE'];
		unset($_SESSION['txtOvenE']);
	}
	if (isset($_SESSION['txtOilE'])){
		$OilE = $_SESSION['txtOilE'];
		unset($_SESSION['txtOilE']);
	}

######
	if (isset($_SESSION['txtNRMNo'])){
		$NRMNo = $_SESSION['txtNRMNo'];
		unset($_SESSION['txtNRMNo']);
	}
	if (isset($_SESSION['txtNRMDate'])){
		$NRMDate = $_SESSION['txtNRMDate'];
		unset($_SESSION['txtNRMDate']);
	}
	if (isset($_SESSION['txtMaterialDescription'])){
		$MaterialDescription = $_SESSION['txtMaterialDescription'];
		unset($_SESSION['txtMaterialDescription']);
	}
	if (isset($_SESSION['sltSupplier'])){
		$Supplier = $_SESSION['sltSupplier'];
		unset($_SESSION['sltSupplier']);
	}
	if (isset($_SESSION['radOtherSupplier'])){
		$OtherSupplierRad = $_SESSION['radOtherSupplier'];
		unset($_SESSION['radOtherSupplier']);
	}
	if (isset($_SESSION['txtOtherSupplier'])){
		$OtherSupplierTxt = $_SESSION['txtOtherSupplier'];
		unset($_SESSION['txtOtherSupplier']);
	}
	if (isset($_SESSION['radClass'])){
		$Class = $_SESSION['radClass'];
		unset($_SESSION['radClass']);
	}
	if (isset($_SESSION['txtOtherClass'])){
		$OtherClass = $_SESSION['txtOtherClass'];
		unset($_SESSION['txtOtherClass']);
	}
	if (isset($_SESSION['radSample'])){
		$Sample = $_SESSION['radSample'];
		unset($_SESSION['radSample']);
	}
	if (isset($_SESSION['txtOtherSample'])){
		$OtherSample = $_SESSION['txtOtherSample'];
		unset($_SESSION['txtOtherSample']);
	}
	if (isset($_SESSION['chkMSDS'])){
		$MSDS = $_SESSION['chkMSDS'];
		unset($_SESSION['chkMSDS']);
	}
	if (isset($_SESSION['chkTDS'])){
		$TDS = $_SESSION['chkTDS'];
		unset($_SESSION['chkTDS']);
	}
	if (isset($_SESSION['txtRemarks'])){
		$Remarks = $_SESSION['txtRemarks'];
		unset($_SESSION['txtRemarks']);
	}

#########
	if (isset($_SESSION['radCustType'])){
		$CustType = $_SESSION['radCustType'];
		unset($_SESSION['radCustType']);
	}
	if (isset($_SESSION['txtTerms'])){
		$TermsDays = $_SESSION['txtTerms'];
		unset($_SESSION['txtTerms']);
	}
	if (isset($_SESSION['sltTerms'])){
		$TermsType = $_SESSION['sltTerms'];
		unset($_SESSION['sltTerms']);
	}
	if (isset($_SESSION['txtCreditLimit'])){
		$CreditLimit = $_SESSION['txtCreditLimit'];
		unset($_SESSION['txtCreditLimit']);
	}
	if (isset($_SESSION['radCreditLimitUnit'])){
		$CreditLimitUnit = $_SESSION['radCreditLimitUnit'];
		unset($_SESSION['radCreditLimitUnit']);
	}
	if (isset($_SESSION['sltAgent'])){
		$Agent = $_SESSION['sltAgent'];
		unset($_SESSION['sltAgent']);
	}
	if (isset($_SESSION['txtAddressStreet'])){
		$AddressStreet = $_SESSION['txtAddressStreet'];
		unset($_SESSION['txtAddressStreet']);
	}
	if (isset($_SESSION['txtAddressBaranggay'])){
		$AddressBaranggay = $_SESSION['txtAddressBaranggay'];
		unset($_SESSION['txtAddressBaranggay']);
	}
	if (isset($_SESSION['txtAddressCityProvince'])){
		$AddressCityProvince = $_SESSION['txtAddressCityProvince'];
		unset($_SESSION['txtAddressCityProvince']);
	}
	if (isset($_SESSION['txtZipCode'])){
		$ZipCode = $_SESSION['txtZipCode'];
		unset($_SESSION['txtZipCode']);
	}
###########
	if (isset($_SESSION['sltCustomer'])){
		$Customer = $_SESSION['sltCustomer'];
		unset($_SESSION['sltCustomer']);
	}
	if (isset($_SESSION['txtTransDate'])){
		$TransDate = $_SESSION['txtTransDate'];
		unset($_SESSION['txtTransDate']);
	}
	if (isset($_SESSION['sltTransType'])){
		$TransType = $_SESSION['sltTransType'];
		unset($_SESSION['sltTransType']);
	}
	if (isset($_SESSION['txtInvoiceNo'])){
		$InvoiceNo = $_SESSION['txtInvoiceNo'];
		unset($_SESSION['txtInvoiceNo']);
	}
	if (isset($_SESSION['txtCheckNo'])){
		$CheckNo = $_SESSION['txtCheckNo'];
		unset($_SESSION['txtCheckNo']);
	}
	if (isset($_SESSION['txtAmount'])){
		$Amount = $_SESSION['txtAmount'];
		unset($_SESSION['txtAmount']);
	}

###########
	if (isset($_SESSION['chkCompounds'])){
		$Compounds = $_SESSION['chkCompounds'];
		unset($_SESSION['chkCompounds']);
	}
	if (isset($_SESSION['chkPipes'])){
		$Pipes = $_SESSION['chkPipes'];
		unset($_SESSION['chkPipes']);
	}
	if (isset($_SESSION['chkCorporate'])){
		$Corporate = $_SESSION['chkCorporate'];
		unset($_SESSION['chkCorporate']);
	}
	if (isset($_SESSION['chkPPR'])){
		$PPR = $_SESSION['chkPPR'];
		unset($_SESSION['chkPPR']);
	}
?>