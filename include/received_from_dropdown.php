<?php

$received_type = strval($_GET['received_type']);
$receivedFromID = intval($_GET['receivedFromID']);
$addInventoryID = intval($_GET['addInventoryID']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>received_from_dropdown.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qry = mysqli_prepare( $db, "CALL sp_Received_From_Dropdown(?)" );
		mysqli_stmt_bind_param( $qry, 's', $received_type );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>received_from_dropdown.php'.'</td><td>'.$processError.' near line 23.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
				echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				
				$IssuerID = $row['id'];
				$Issuer = $row['c_name'];

				if ( $receivedFromID == $IssuerID ){
					echo "<option value='".$IssuerID."' selected>".$Issuer."</option>";	
				}else{
					echo "<option value='".$IssuerID."'>".$Issuer."</option>";
				}
				
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>