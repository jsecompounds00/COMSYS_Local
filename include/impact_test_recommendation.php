<?php

	require("database_connect.php");

	$qryIT = mysqli_prepare($db, "CALL sp_BAT_Impact_Test_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qryIT, 'is', $TypeID, $Type);
	$qryIT->execute();
	$resultIT = mysqli_stmt_get_result($qryIT);
	$processErrorIT = mysqli_error($db);

		if ( !empty($processErrorIT) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>impact_test_recommendation.php'.'</td><td>'.$processErrorIT.' near line 12.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultIT)){
				$ITtrial_no = $row['ITtrial_no'];
				$ITFGName = $row['ITFGName'];

				$ITTrial1 = $row['ITTrial1']; $ITTrial2 = $row['ITTrial2']; $ITTrial3 = $row['ITTrial3']; $ITTrial4 = $row['ITTrial4']; $ITTrial5 = $row['ITTrial5'];
				$ITTrial6 = $row['ITTrial6']; $ITTrial7 = $row['ITTrial7']; $ITTrial8 = $row['ITTrial8']; $ITTrial9 = $row['ITTrial9']; $ITTrial10 = $row['ITTrial10'];
				$ITTrial11 = $row['ITTrial11']; $ITTrial12 = $row['ITTrial12']; $ITTrial13 = $row['ITTrial13']; $ITTrial14 = $row['ITTrial14']; $ITTrial15 = $row['ITTrial15'];
				$ITTrial16 = $row['ITTrial16']; $ITTrial17 = $row['ITTrial17']; $ITTrial18 = $row['ITTrial18']; $ITTrial19 = $row['ITTrial19']; $ITTrial20 = $row['ITTrial20'];

				$DropWeight1 = $row['DropWeight1']; $DropWeight2 = $row['DropWeight2']; $DropWeight3 = $row['DropWeight3']; $DropWeight4 = $row['DropWeight4']; $DropWeight5 = $row['DropWeight5'];
				$DropWeight6 = $row['DropWeight6']; $DropWeight7 = $row['DropWeight7']; $DropWeight8 = $row['DropWeight8']; $DropWeight9 = $row['DropWeight9']; $DropWeight10 = $row['DropWeight10'];
				$DropWeight11 = $row['DropWeight11']; $DropWeight12 = $row['DropWeight12']; $DropWeight13 = $row['DropWeight13']; $DropWeight14 = $row['DropWeight14']; $DropWeight15 = $row['DropWeight15'];
				$DropWeight16 = $row['DropWeight16']; $DropWeight17 = $row['DropWeight17']; $DropWeight18 = $row['DropWeight18']; $DropWeight19 = $row['DropWeight19']; $DropWeight20 = $row['DropWeight20'];

				$DropHeight1 = $row['DropHeight1']; $DropHeight2 = $row['DropHeight2']; $DropHeight3 = $row['DropHeight3']; $DropHeight4 = $row['DropHeight4']; $DropHeight5 = $row['DropHeight5'];
				$DropHeight6 = $row['DropHeight6']; $DropHeight7 = $row['DropHeight7']; $DropHeight8 = $row['DropHeight8']; $DropHeight9 = $row['DropHeight9']; $DropHeight10 = $row['DropHeight10'];
				$DropHeight11 = $row['DropHeight11']; $DropHeight12 = $row['DropHeight12']; $DropHeight13 = $row['DropHeight13']; $DropHeight14 = $row['DropHeight14']; $DropHeight15 = $row['DropHeight15'];
				$DropHeight16 = $row['DropHeight16']; $DropHeight17 = $row['DropHeight17']; $DropHeight18 = $row['DropHeight18']; $DropHeight19 = $row['DropHeight19']; $DropHeight20 = $row['DropHeight20'];

				$SampleNo1 = $row['SampleNo1']; $SampleNo2 = $row['SampleNo2']; $SampleNo3 = $row['SampleNo3']; $SampleNo4 = $row['SampleNo4']; $SampleNo5 = $row['SampleNo5'];
				$SampleNo6 = $row['SampleNo6']; $SampleNo7 = $row['SampleNo7']; $SampleNo8 = $row['SampleNo8']; $SampleNo9 = $row['SampleNo9']; $SampleNo10 = $row['SampleNo10'];
				$SampleNo11 = $row['SampleNo11']; $SampleNo12 = $row['SampleNo12']; $SampleNo13 = $row['SampleNo13']; $SampleNo14 = $row['SampleNo14']; $SampleNo15 = $row['SampleNo15'];
				$SampleNo16 = $row['SampleNo16']; $SampleNo17 = $row['SampleNo17']; $SampleNo18 = $row['SampleNo18']; $SampleNo19 = $row['SampleNo19']; $SampleNo20 = $row['SampleNo20'];

				$PassedNo1 = $row['PassedNo1']; $PassedNo2 = $row['PassedNo2']; $PassedNo3 = $row['PassedNo3']; $PassedNo4 = $row['PassedNo4']; $PassedNo5 = $row['PassedNo5'];
				$PassedNo6 = $row['PassedNo6']; $PassedNo7 = $row['PassedNo7']; $PassedNo8 = $row['PassedNo8']; $PassedNo9 = $row['PassedNo9']; $PassedNo10 = $row['PassedNo10'];
				$PassedNo11 = $row['PassedNo11']; $PassedNo12 = $row['PassedNo12']; $PassedNo13 = $row['PassedNo13']; $PassedNo14 = $row['PassedNo14']; $PassedNo15 = $row['PassedNo15'];
				$PassedNo16 = $row['PassedNo16']; $PassedNo17 = $row['PassedNo17']; $PassedNo18 = $row['PassedNo18']; $PassedNo19 = $row['PassedNo19']; $PassedNo20 = $row['PassedNo20'];
		?>
				<table class="results_child_tables_form">
					<tr>
						<td colspan='<?php echo ($ITtrial_no+1); ?>'> <?php  echo $ITFGName; ?> </td>
					</tr>
					<tr>
						<th></th>
						<?php
							if ( $ITTrial1 <= $ITtrial_no &&$ITTrial1!= '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial1; ?> </th>
						<?php		
							}

							if ( $ITTrial2 <= $ITtrial_no && $ITTrial2 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial2; ?> </th>
						<?php		
							}

							if ( $ITTrial3 <= $ITtrial_no && $ITTrial3 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial3; ?> </th>
						<?php		
							}

							if ( $ITTrial4 <= $ITtrial_no && $ITTrial4 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial4; ?> </th>
						<?php		
							}

							if ( $ITTrial5 <= $ITtrial_no && $ITTrial5 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial5; ?> </th>
						<?php		
							}

							if ( $ITTrial6 <= $ITtrial_no && $ITTrial6 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial6; ?> </th>
						<?php		
							}

							if ( $ITTrial7 <= $ITtrial_no && $ITTrial7 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial7; ?> </th>
						<?php		
							}

							if ( $ITTrial8 <= $ITtrial_no && $ITTrial8 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial8; ?> </th>
						<?php		
							}

							if ( $ITTrial9 <= $ITtrial_no && $ITTrial9 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial9; ?> </th>
						<?php		
							}

							if ( $ITTrial10 <= $ITtrial_no && $ITTrial10 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial10; ?> </th>
						<?php		
							}

							if ( $ITTrial11 <= $ITtrial_no && $ITTrial11 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial11; ?> </th>
						<?php		
							}

							if ( $ITTrial12 <= $ITtrial_no && $ITTrial12 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial12; ?> </th>
						<?php		
							}

							if ( $ITTrial13 <= $ITtrial_no && $ITTrial13 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial13; ?> </th>
						<?php		
							}

							if ( $ITTrial14 <= $ITtrial_no && $ITTrial14 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial14; ?> </th>
						<?php		
							}

							if ( $ITTrial15 <= $ITtrial_no && $ITTrial15 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial15; ?> </th>
						<?php		
							}

							if ( $ITTrial16 <= $ITtrial_no && $ITTrial16 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial16; ?> </th>
						<?php		
							}

							if ( $ITTrial17 <= $ITtrial_no && $ITTrial17 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial17; ?> </th>
						<?php		
							}

							if ( $ITTrial18 <= $ITtrial_no && $ITTrial18 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial18; ?> </th>
						<?php		
							}

							if ( $ITTrial19 <= $ITtrial_no && $ITTrial19 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial19; ?> </th>
						<?php		
							}

							if ( $ITTrial20 <= $ITtrial_no && $ITTrial20 != '-' ){
						?>
								<th> <?php echo 'Trial '.$ITTrial20; ?> </th>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Weight of Drop	 -->	
					<tr>
						<td> Weight of Drop </td>
						<?php
							if ( $ITTrial1 <= $ITtrial_no && $ITTrial1 != '-' ){
						?>
								<td> <?php echo $DropWeight1; ?> </td>
						<?php		
							}

							if ( $ITTrial2 <= $ITtrial_no && $ITTrial2 != '-' ){
						?>
								<td> <?php echo $DropWeight2; ?> </td>
						<?php		
							}

							if ( $ITTrial3 <= $ITtrial_no && $ITTrial3 != '-' ){
						?>
								<td> <?php echo $DropWeight3; ?> </td>
						<?php		
							}

							if ( $ITTrial4 <= $ITtrial_no && $ITTrial4 != '-' ){
						?>
								<td> <?php echo $DropWeight4; ?> </td>
						<?php		
							}

							if ( $ITTrial5 <= $ITtrial_no && $ITTrial5 != '-' ){
						?>
								<td> <?php echo $DropWeight5; ?> </td>
						<?php		
							}

							if ( $ITTrial6 <= $ITtrial_no && $ITTrial6 != '-' ){
						?>
								<td> <?php echo $DropWeight6; ?> </td>
						<?php		
							}

							if ( $ITTrial7 <= $ITtrial_no && $ITTrial7 != '-' ){
						?>
								<td> <?php echo $DropWeight7; ?> </td>
						<?php		
							}

							if ( $ITTrial8 <= $ITtrial_no && $ITTrial8 != '-' ){
						?>
								<td> <?php echo $DropWeight8; ?> </td>
						<?php		
							}

							if ( $ITTrial9 <= $ITtrial_no && $ITTrial9 != '-' ){
						?>
								<td> <?php echo $DropWeight9; ?> </td>
						<?php		
							}

							if ( $ITTrial10 <= $ITtrial_no && $ITTrial10 != '-' ){
						?>
								<td> <?php echo $DropWeight10; ?> </td>
						<?php		
							}

							if ( $ITTrial11 <= $ITtrial_no && $ITTrial11 != '-' ){
						?>
								<td> <?php echo $DropWeight11; ?> </td>
						<?php		
							}

							if ( $ITTrial12 <= $ITtrial_no && $ITTrial12 != '-' ){
						?>
								<td> <?php echo $DropWeight12; ?> </td>
						<?php		
							}

							if ( $ITTrial13 <= $ITtrial_no && $ITTrial13 != '-' ){
						?>
								<td> <?php echo $DropWeight13; ?> </td>
						<?php		
							}

							if ( $ITTrial14 <= $ITtrial_no && $ITTrial14 != '-' ){
						?>
								<td> <?php echo $DropWeight14; ?> </td>
						<?php		
							}

							if ( $ITTrial15 <= $ITtrial_no && $ITTrial15 != '-' ){
						?>
								<td> <?php echo $DropWeight15; ?> </td>
						<?php		
							}

							if ( $ITTrial16 <= $ITtrial_no && $ITTrial16 != '-' ){
						?>
								<td> <?php echo $DropWeight16; ?> </td>
						<?php		
							}

							if ( $ITTrial17 <= $ITtrial_no && $ITTrial17 != '-' ){
						?>
								<td> <?php echo $DropWeight17; ?> </td>
						<?php		
							}

							if ( $ITTrial18 <= $ITtrial_no && $ITTrial18 != '-' ){
						?>
								<td> <?php echo $DropWeight18; ?> </td>
						<?php		
							}

							if ( $ITTrial19 <= $ITtrial_no && $ITTrial19 != '-' ){
						?>
								<td> <?php echo $DropWeight19; ?> </td>
						<?php		
							}

							if ( $ITTrial20 <= $ITtrial_no && $ITTrial20 != '-' ){
						?>
								<td> <?php echo $DropWeight20; ?> </td>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Height of Drop	 -->	
					<tr>
						<td> Height of Drop </td>
						<?php
							if ( $ITTrial1 <= $ITtrial_no && $ITTrial1 != '-' ){
						?>
								<td> <?php echo $DropHeight1; ?> </td>
						<?php		
							}

							if ( $ITTrial2 <= $ITtrial_no && $ITTrial2 != '-' ){
						?>
								<td> <?php echo $DropHeight2; ?> </td>
						<?php		
							}

							if ( $ITTrial3 <= $ITtrial_no && $ITTrial3 != '-' ){
						?>
								<td> <?php echo $DropHeight3; ?> </td>
						<?php		
							}

							if ( $ITTrial4 <= $ITtrial_no && $ITTrial4 != '-' ){
						?>
								<td> <?php echo $DropHeight4; ?> </td>
						<?php		
							}

							if ( $ITTrial5 <= $ITtrial_no && $ITTrial5 != '-' ){
						?>
								<td> <?php echo $DropHeight5; ?> </td>
						<?php		
							}

							if ( $ITTrial6 <= $ITtrial_no && $ITTrial6 != '-' ){
						?>
								<td> <?php echo $DropHeight6; ?> </td>
						<?php		
							}

							if ( $ITTrial7 <= $ITtrial_no && $ITTrial7 != '-' ){
						?>
								<td> <?php echo $DropHeight7; ?> </td>
						<?php		
							}

							if ( $ITTrial8 <= $ITtrial_no && $ITTrial8 != '-' ){
						?>
								<td> <?php echo $DropHeight8; ?> </td>
						<?php		
							}

							if ( $ITTrial9 <= $ITtrial_no && $ITTrial9 != '-' ){
						?>
								<td> <?php echo $DropHeight9; ?> </td>
						<?php		
							}

							if ( $ITTrial10 <= $ITtrial_no && $ITTrial10 != '-' ){
						?>
								<td> <?php echo $DropHeight10; ?> </td>
						<?php		
							}

							if ( $ITTrial11 <= $ITtrial_no && $ITTrial11 != '-' ){
						?>
								<td> <?php echo $DropHeight11; ?> </td>
						<?php		
							}

							if ( $ITTrial12 <= $ITtrial_no && $ITTrial12 != '-' ){
						?>
								<td> <?php echo $DropHeight12; ?> </td>
						<?php		
							}

							if ( $ITTrial13 <= $ITtrial_no && $ITTrial13 != '-' ){
						?>
								<td> <?php echo $DropHeight13; ?> </td>
						<?php		
							}

							if ( $ITTrial14 <= $ITtrial_no && $ITTrial14 != '-' ){
						?>
								<td> <?php echo $DropHeight14; ?> </td>
						<?php		
							}

							if ( $ITTrial15 <= $ITtrial_no && $ITTrial15 != '-' ){
						?>
								<td> <?php echo $DropHeight15; ?> </td>
						<?php		
							}

							if ( $ITTrial16 <= $ITtrial_no && $ITTrial16 != '-' ){
						?>
								<td> <?php echo $DropHeight16; ?> </td>
						<?php		
							}

							if ( $ITTrial17 <= $ITtrial_no && $ITTrial17 != '-' ){
						?>
								<td> <?php echo $DropHeight17; ?> </td>
						<?php		
							}

							if ( $ITTrial18 <= $ITtrial_no && $ITTrial18 != '-' ){
						?>
								<td> <?php echo $DropHeight18; ?> </td>
						<?php		
							}

							if ( $ITTrial19 <= $ITtrial_no && $ITTrial19 != '-' ){
						?>
								<td> <?php echo $DropHeight19; ?> </td>
						<?php		
							}

							if ( $ITTrial20 <= $ITtrial_no && $ITTrial20 != '-' ){
						?>
								<td> <?php echo $DropHeight20; ?> </td>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Actual	 -->	
					<tr>
						<td> Actual </td>
						<?php
							if ( $ITTrial1 <= $ITtrial_no && $ITTrial1 != '-' ){
						?>
								<td> <?php echo $SampleNo1.'/'.$PassedNo1; ?> </td>
						<?php		
							}

							if ( $ITTrial2 <= $ITtrial_no && $ITTrial2 != '-' ){
						?>
								<td> <?php echo $SampleNo2.'/'.$PassedNo2; ?> </td>
						<?php		
							}

							if ( $ITTrial3 <= $ITtrial_no && $ITTrial3 != '-' ){
						?>
								<td> <?php echo $SampleNo3.'/'.$PassedNo3; ?> </td>
						<?php		
							}

							if ( $ITTrial4 <= $ITtrial_no && $ITTrial4 != '-' ){
						?>
								<td> <?php echo $SampleNo4.'/'.$PassedNo4; ?> </td>
						<?php		
							}

							if ( $ITTrial5 <= $ITtrial_no && $ITTrial5 != '-' ){
						?>
								<td> <?php echo $SampleNo5.'/'.$PassedNo5; ?> </td>
						<?php		
							}

							if ( $ITTrial6 <= $ITtrial_no && $ITTrial6 != '-' ){
						?>
								<td> <?php echo $SampleNo6.'/'.$PassedNo6; ?> </td>
						<?php		
							}

							if ( $ITTrial7 <= $ITtrial_no && $ITTrial7 != '-' ){
						?>
								<td> <?php echo $SampleNo7.'/'.$PassedNo7; ?> </td>
						<?php		
							}

							if ( $ITTrial8 <= $ITtrial_no && $ITTrial8 != '-' ){
						?>
								<td> <?php echo $SampleNo8.'/'.$PassedNo8; ?> </td>
						<?php		
							}

							if ( $ITTrial9 <= $ITtrial_no && $ITTrial9 != '-' ){
						?>
								<td> <?php echo $SampleNo9.'/'.$PassedNo9; ?> </td>
						<?php		
							}

							if ( $ITTrial10 <= $ITtrial_no && $ITTrial10 != '-' ){
						?>
								<td> <?php echo $SampleNo10.'/'.$PassedNo10; ?> </td>
						<?php		
							}

							if ( $ITTrial11 <= $ITtrial_no && $ITTrial11 != '-' ){
						?>
								<td> <?php echo $SampleNo11.'/'.$PassedNo11; ?> </td>
						<?php		
							}

							if ( $ITTrial12 <= $ITtrial_no && $ITTrial12 != '-' ){
						?>
								<td> <?php echo $SampleNo12.'/'.$PassedNo12; ?> </td>
						<?php		
							}

							if ( $ITTrial13 <= $ITtrial_no && $ITTrial13 != '-' ){
						?>
								<td> <?php echo $SampleNo13.'/'.$PassedNo13; ?> </td>
						<?php		
							}

							if ( $ITTrial14 <= $ITtrial_no && $ITTrial14 != '-' ){
						?>
								<td> <?php echo $SampleNo14.'/'.$PassedNo14; ?> </td>
						<?php		
							}

							if ( $ITTrial15 <= $ITtrial_no && $ITTrial15 != '-' ){
						?>
								<td> <?php echo $SampleNo15.'/'.$PassedNo15; ?> </td>
						<?php		
							}

							if ( $ITTrial16 <= $ITtrial_no && $ITTrial16 != '-' ){
						?>
								<td> <?php echo $SampleNo16.'/'.$PassedNo16; ?> </td>
						<?php		
							}

							if ( $ITTrial17 <= $ITtrial_no && $ITTrial17 != '-' ){
						?>
								<td> <?php echo $SampleNo17.'/'.$PassedNo17; ?> </td>
						<?php		
							}

							if ( $ITTrial18 <= $ITtrial_no && $ITTrial18 != '-' ){
						?>
								<td> <?php echo $SampleNo18.'/'.$PassedNo18; ?> </td>
						<?php		
							}

							if ( $ITTrial19 <= $ITtrial_no && $ITTrial19 != '-' ){
						?>
								<td> <?php echo $SampleNo19.'/'.$PassedNo19; ?> </td>
						<?php		
							}

							if ( $ITTrial20 <= $ITtrial_no && $ITTrial20 != '-' ){
						?>
								<td> <?php echo $SampleNo20.'/'.$PassedNo20; ?> </td>
						<?php		
							}
						?>
					</tr>
				</table>
	<?php
			}
			$db->next_result();
			$resultIT->close();
		}

	require("database_close.php");
?>