
<script src="js/jscript.js"></script>
<tr>
	<th>Raw Material</th>
	<th>PHR</th>
	<th>Computation</th>
</tr>
<?php

	$formula_type_id = intval($_GET["formula_type_id"]);


	require("database_connect.php");

	$qry = mysqli_prepare($db, "CALL sp_Formula_Dropdown( ? )");
	mysqli_stmt_bind_param($qry, 'i', $formula_type_id);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);

	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>formula_phr.php'.'</td><td>'.$processError.' near line 37.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$i = 0;
		while($row = mysqli_fetch_assoc($result))
		{
			$rm_type = $row["rm_type"];
			$rm_code = $row["rm_code"];
			$name = $row["name"];
			$phr = $row["phr"];
?>			
			<tr>
				<td> <?php echo $rm_type.' '.$rm_code.': '.$name;  ?> </td>
				<td> 
					<?php echo $phr;  ?> 
					<input type='hidden' name='hidFormulaPHR[]' id='hidFormulaPHR<?php echo $i;?>' value='<?php echo $phr; ?>'>
				</td>
				<td>
					<?php
						if ( $i == 0 ){
					?>
							<input type='text' name='hidResinDosage[]' id='hidResinDosage<?php echo $i;?>' onkeyup='showRMComputation()'>
							<input type='hidden' name='ResinDosage[]' id='ResinDosage<?php echo $i;?>'>
					<?php				
						}else{
					?>
							<input type='text' name='hidResinDosage[]' id='hidResinDosage<?php echo $i;?>' readOnly>
							<input type='hidden' name='ResinDosage[]' id='ResinDosage<?php echo $i;?>'>
					<?php
						}
					?>
				</td>
			</tr>
<?php
			$i++;
		}
?>			<tr class="spacing">
				<td class="total_label"><b>TOTAL</b></td>
				<td class="total_value"> <b id='tdTotalPHR'></b> </td>
				<td class="total_value"> <b id='tdTotalCompute'></b> </td>
			</tr>	
<?php
	}
	$db->next_result();
	$result->close();

	require("database_close.php");
?>