<?php

$fg_id = intval($_GET['fg_id']);
$fg_type = intval($_GET['fg_type']);
// $sltActivity = intval($_GET['sltActivity']);
$bat_id = intval($_GET['bat_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>formula_type_dropdown.php'.'</td><td>'.$error.' near line 13.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare( $db, "CALL sp_FormulaType_Dropdown(?)" );
		mysqli_stmt_bind_param( $qry, 'i', $fg_id );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>formula_type_dropdown.php'.'</td><td>'.$processError.' near line 23.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			$i = 0;
			if ( !$bat_id )
			echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$f_type_id = $row['id'];
				$formula_type = $row['formula_type'];
				
				if ( $bat_id ){
					if ($f_type_id == $fg_type){
						echo "<option value=".$f_type_id." selected>".$formula_type."</option>";
					}
				}else{
					if ($f_type_id == $fg_type){
						echo "<option value=".$f_type_id." selected>".$formula_type."</option>";
					}else {
						echo "<option id='optionFormulaType$i' value=".$f_type_id.">".$formula_type."</option>";
						
					}
				}

				$i++;
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>