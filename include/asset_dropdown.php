<?php

$asset_type = intval($_GET['asset_type']);
$addInventoryID = intval($_GET['addInventoryID']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>asset_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qry = mysqli_prepare($db, "CALL sp_Asset_Dropdown( ?, 1 )");
		mysqli_stmt_bind_param($qry, 's', $asset_type);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>asset_dropdown.php'.'</td><td>'.$processError.' near line 22.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
				echo "<option></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				
				$PrpertyID = $row['id'];
				$PropertyName = $row['property_name'];
				
					echo "<option value=".$PrpertyID.">".$PropertyName."</option>";
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>