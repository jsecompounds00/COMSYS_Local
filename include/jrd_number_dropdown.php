<?php

$jrd = intval($_GET['jrd']);

if ( $jrd )

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>activity_dropdown.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_JRD_Number_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 'i', $jrd);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>activity_dropdown.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$jrd_id = $row['jrd_id'];
				$jrd_number = $row['jrd_number'];
				
				echo "<option value=".$jrd_id.">".$jrd_number."</option>";
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>