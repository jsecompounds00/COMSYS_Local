<?php

$batch_id = strval($_GET['batch_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_lot_number.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_Issued_Formula_Details_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 'i', $batch_id);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_lot_number.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result))
			{
				echo "<font color='#2B5075'><b>".$row["lot_number"]."</b></font>";
				echo "<input type='hidden' name='txtLotNumber' id='txtLotNumber' value='".$row["lot_number"]."'>";
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>