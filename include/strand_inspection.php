<?php

	require("database_connect.php");

	$qrySI = mysqli_prepare($db, "CALL sp_Batch_Ticket_Strand_Inspection_Query( ? )");
	mysqli_stmt_bind_param($qrySI, 'i', $TrialID);
	$qrySI->execute();
	$resultSI = mysqli_stmt_get_result($qrySI);
	$processErrorSI = mysqli_error($db);

	if ( !empty($processErrorSI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>strand_inspection.php'.'</td><td>'.$processErrorSI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultSI)){
			$fisheye_blister_evaluation = $row['fisheye_blister_evaluation'];
			$fisheye_blister_classification = $row['fisheye_blister_classification'];
			$pinhole_evaluation = $row['pinhole_evaluation'];
			$SIcolor_conform = $row['color_conform'];
			$porous = $row['porous'];
			$fc_present = $row['fc_present'];
			$overall_evaluation = $row['overall_evaluation'];
			$SIcreated_at = $row['created_at'];
			$SIcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultSI->close();
	}

?>
	
	<table class="results_child_tables_form">
		<col width="250"></col>
		<tr></tr>
		<input type='hidden' name='hidStrandInspectionID' value='<?php echo $strand_inspect_id; ?>'>
		<tr>
			<td>Fish Eyes / Blisters:</td>
			<td>
				<select name='sltFisheyeEvaluation'>
					<?php
						$qry1 = "CALL sp_BAT_Evaluation_Fisheye_Pinhole_Dropdown()";
						$result1 = mysqli_query( $db, $qry1 );
						$processError1 = mysqli_error($db);

						if(!empty($processError1))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError1.' near line 27.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{ 
							while ( $row = mysqli_fetch_assoc( $result1 ) ){
								$evaluation_fisheye_id = $row['id'];
								$evaluation_fisheye = $row['evaluation'];

								if ( $strand_inspect_id ){
									if ( $fisheye_blister_evaluation == $evaluation_fisheye_id ){
										echo "<option value='".$evaluation_fisheye_id."' selected>".$evaluation_fisheye."</option>";
									}else{
										echo "<option value='".$evaluation_fisheye_id."'>".$evaluation_fisheye."</option>";
									}
								}else{
									if ( $initFisheyeEvaluation_BT == $evaluation_fisheye_id ){
										echo "<option value='".$evaluation_fisheye_id."' selected>".$evaluation_fisheye."</option>";
									}else{
										echo "<option value='".$evaluation_fisheye_id."'>".$evaluation_fisheye."</option>";
									}
								}

							}
							$db->next_result();
							$result1->close();
						}
					?>
				</select>

				<select name='sltFisheyeClass'>
					<?php
						$qry2 = "CALL sp_BAT_Class_Fisheye_Dropdown()";
						$result2 = mysqli_query( $db, $qry2 );
						$processError2 = mysqli_error($db);

						if(!empty($processError2))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError2.' near line 27.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{ 
							while ( $row = mysqli_fetch_assoc( $result2 ) ){
								$class_fisheye_id = $row['id'];
								$class_fisheye = $row['classification'];

								if ( $strand_inspect_id ){
									if ( $fisheye_blister_classification == $class_fisheye_id ){
										echo "<option value='".$class_fisheye_id."' selected>".$class_fisheye."</option>";
									}else{
										echo "<option value='".$class_fisheye_id."'>".$class_fisheye."</option>";
									}
								}else{
									if ( $initFisheyeClass_BT == $class_fisheye_id ){
										echo "<option value='".$class_fisheye_id."' selected>".$class_fisheye."</option>";
									}else{
										echo "<option value='".$class_fisheye_id."'>".$class_fisheye."</option>";
									}
								}

							}
							$db->next_result();
							$result2->close();
						}
					?>
				</select>
			</td>
			<td>Overall Evaluation Remarks:</td>
			<td>
				<select name='sltOverallRemarks'>
					<?php
						$qry4 = "CALL sp_BAT_Overall_Remarks_Dropdown()";
						$result4 = mysqli_query( $db, $qry4 );
						$processError4 = mysqli_error($db);

						if(!empty($processError4))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError4.' near line 47.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{ 
							while ( $row = mysqli_fetch_assoc( $result4 ) ){
								$overall_remarks_id = $row['id'];
								$overall_remarks = $row['remarks'];

								if ( $strand_inspect_id ){
									if ( $overall_evaluation == $overall_remarks_id ){
										echo "<option value='".$overall_remarks_id."' selected>".$overall_remarks."</option>";
									}else{
										echo "<option value='".$overall_remarks_id."'>".$overall_remarks."</option>";
									}
								}else{
									if ( $initOverallRemarks_BT == $overall_remarks_id ){
										echo "<option value='".$overall_remarks_id."' selected>".$overall_remarks."</option>";
									}else{
										echo "<option value='".$overall_remarks_id."'>".$overall_remarks."</option>";
									}
								}

							}
							$db->next_result();
							$result4->close();
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Pin Holes:</td>
			<td colspan="3">
				<select name='sltPinholeEvaluation'>
					<?php
						$qry3 = "CALL sp_BAT_Evaluation_Fisheye_Pinhole_Dropdown()";
						$result3 = mysqli_query( $db, $qry3 );
						$processError3 = mysqli_error($db);

						if(!empty($processError3))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError3.' near line 27.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{ 
							while ( $row = mysqli_fetch_assoc( $result3 ) ){
								$evaluation_pinhole_id = $row['id'];
								$evaluation_pinhole = $row['evaluation'];

								if ( $strand_inspect_id ){
									if ( $pinhole_evaluation == $evaluation_pinhole_id ){
										echo "<option value='".$evaluation_pinhole_id."' selected>".$evaluation_pinhole."</option>";
									}else{
										echo "<option value='".$evaluation_pinhole_id."'>".$evaluation_pinhole."</option>";
									}
								}else{
									if ( $initPinholeEvaluation_BT == $evaluation_pinhole_id ){
										echo "<option value='".$evaluation_pinhole_id."' selected>".$evaluation_pinhole."</option>";
									}else{
										echo "<option value='".$evaluation_pinhole_id."'>".$evaluation_pinhole."</option>";
									}
								}

							}
							$db->next_result();
							$result3->close();
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Color Conform:</td>
			<td colspan="3">
				<input type='checkbox' name='chkColorConform' <?php echo ( $strand_inspect_id ? ( $SIcolor_conform ? "checked" : "" ) : ( $initColorConform_BT ? "checked" : "" ) );?>> 
			</td>
		</tr>
		<tr>
			<td>Porous:</td>
			<td colspan="3">
				<input type='checkbox' name='chkPorous' <?php echo ( $strand_inspect_id ? ( $porous ? "checked" : "" ) : ( $initPorous_BT ? "checked" : "" ) );?>> 
			</td>
		</tr>
		<tr>
			<td>FC Present:</td>
			<td colspan="3">
				<input type='checkbox' name='chkFCPresent' <?php echo ( $strand_inspect_id ? ( $fc_present ? "checked" : "" ) : ( $initFCPresent_BT ? "checked" : "" ) );?>> 
			</td>
		</tr>
	</table>

<?php		
	
	if ( $strand_inspect_id ){
		echo "<input type='hidden' name='hidSICreatedAt' value='".$SIcreated_at."'>";
		echo "<input type='hidden' name='hidSICreatedId' value='".$SIcreated_id."'>";	
	}else{
		echo "<input type='hidden' name='hidSICreatedAt' value='".date('Y-m-d H:i:s')."'>";
		echo "<input type='hidden' name='hidSICreatedId' value=''>";	
	}

	require("database_close.php");
?>