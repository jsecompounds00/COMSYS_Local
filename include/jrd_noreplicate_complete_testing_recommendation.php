<?php

require("database_connect.php");
########## Getting data for Test Evaluation
$qryN = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Complete_Testing_Recommendation_Query( ?, ? )");
mysqli_stmt_bind_param($qryN, 'is', $TypeID, $Type);
$qryN->execute();
$resultN = mysqli_stmt_get_result($qryN); 
$processErrorN = mysqli_error($db);
?>
	<table class="results_child_tables_form">	
		<!-- ###################### Trial Numbers ###################### -->
		<?php
			if ( !empty($processErrorN) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_complete_testing_recommendation.php'.'</td><td>'.$processErrorN.' near line 78.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultN)){
					$CTJRDTestID = $row['CTJRDTestID'];
					$CTJRDFGSoft = $row['CTJRDFGSoft'];
					$CTJRDFGCategory = $row['CTJRDFGCategory'];
					$CTOilAging = $row['CTOilAging'];
					$CTOvenAging = $row['CTOvenAging'];
					$CTClientsProduct = $row['CTClientsProduct'];
					$CTCACCProduct = $row['CTCACCProduct'];
					$CTClientsSG = $row['CTClientsSG'];
					$CTCACCSG = $row['CTCACCSG'];
					$CTClientsHA = $row['CTClientsHA'];
					$CTCACCHA = $row['CTCACCHA'];
					$CTClientsHD = $row['CTClientsHD'];
					$CTCACCHD = $row['CTCACCHD'];
					$CTClientsVR = $row['CTClientsVR'];
					$CTCACCVR = $row['CTCACCVR'];
					$CTClientsUnageT = $row['CTClientsUnageT'];
					$CTCACCUnageT = $row['CTCACCUnageT'];
					$CTClientsUnageE = $row['CTClientsUnageE'];
					$CTCACCUnageE = $row['CTCACCUnageE'];
					$CTClientsOvenT = $row['CTClientsOvenT'];
					$CTCACCOvenT = $row['CTCACCOvenT'];
					$CTClientsOvenE = $row['CTClientsOvenE'];
					$CTCACCOvenE = $row['CTCACCOvenE'];
					$CTClientsOilT = $row['CTClientsOilT'];
					$CTCACCOilT = $row['CTCACCOilT'];
					$CTClientsOilE = $row['CTClientsOilE'];
					$CTCACCOilE = $row['CTCACCOilE'];
		?>
			<tr>
				<td><font color="white">.</font></td>
			</tr>
			<tr>
				<th></th>
				<th> <?php echo $CTClientsProduct; ?> </th>
				<th> <?php echo $CTCACCProduct; ?> </th>
			</tr>
			<!-- ###################### Specific Gravity	 -->	
			<tr>
				<td> Specific Gravity </td>
				<td> <?php echo $CTClientsSG; ?> </td>
				<td> <?php echo $CTCACCSG; ?> </td>
			</tr>
		<?php
			if ( $CTJRDFGSoft ){
		?>
				<!-- ###################### H-Shore A	 -->
					<tr>
						<td> H-Shore A </td>
						<td> <?php echo $CTClientsHA; ?> </td>
						<td> <?php echo $CTCACCHA; ?> </td>
					</tr>
				<!-- ###################### Volume Resistivity	 -->
					<tr>
						<td> Volume Resistivity </td>
						<td> <?php echo $CTClientsVR; ?> </td>
						<td> <?php echo $CTCACCVR; ?> </td>
					</tr>
				<!-- ###################### Unaged Properties	 -->
					<tr>
						<td colspan='3'> <strong> Unaged Properties </strong> </td>
					</tr>
				<!-- ###################### Unaged Tensile	 -->
					<tr>
						<td > > Tensile Strength </td>
						<td> <?php echo $CTClientsUnageT; ?> </td>
						<td> <?php echo $CTCACCUnageT; ?> </td>
					</tr>
				<!-- ###################### Unaged Elongation	 -->
					<tr>
						<td > > Elongation </td>
						<td> <?php echo $CTClientsUnageE; ?> </td>
						<td> <?php echo $CTCACCUnageE; ?> </td>
					</tr>
				<!-- ###################### AGE CONDITIONS	 -->
					<?php
						if ( $CTOvenAging || $CTOilAging ){
					?>	
							<tr>
								<th colspan="3"> <i> CONDITION </i> </th>
							</tr>
							<tr>
								<th colspan="3" >
									<b><i>
										<?php
											if ( $CTJRDFGCategory == "60" ){
										?>	
												100º ± 1º at 168 hrs
										<?php
											}elseif ( $CTJRDFGCategory == "75" ){
										?>	
												121º ± 1º at 168 hrs
										<?php
											}elseif ( $CTJRDFGCategory == "90" ){
										?>	
												136º ± 1º at 168 hrs
										<?php
											}
										?>
									</i></b>
								</th>
							</tr>
					<?php
						}
					?>

			<!-- ###################### Oven Aging	 -->
		<?php
			if ( $CTOvenAging ){
		?>
				<tr>
					<td colspan='3'> <strong> Oven Aging </strong> </td>
				</tr>
				<!-- ###################### Oven Aged Tensile	 -->
					<tr>
						<td > > %, Tensile Strength </td>
						<td> 
							<?php 
								if ( $CTClientsUnageT != 0 ){
									$PercOvenTS1 = (($CTClientsOvenT / $CTClientsUnageT) * 100); 
									echo number_format((float)($PercOvenTS1), 2, '.', '');
								}else{
									echo $CTClientsOvenT;
								}
							?> 
						</td>
						<td> 
							<?php 
								if ( $CTCACCUnageT != 0 ){
									$PercOvenTS2 = (($CTCACCOvenT / $CTCACCUnageT) * 100); 
									echo number_format((float)($PercOvenTS2), 2, '.', '');
								}else{
									echo $CTCACCOvenT;
								}
							?> 
						</td>
					</tr>
				<!-- ###################### Oven Aged Elongation	 -->
					<tr>
						<td > > %, Elongation </td>
						<td> 
							<?php 
								if ( $CTClientsUnageE != 0 ){
									$PercOvenE1 = (($CTClientsOvenE / $CTClientsUnageE) * 100); 
									echo number_format((float)($PercOvenE1), 2, '.', '');
								}else{
									echo $CTClientsOvenE;
								}
							?> 
						</td>
						<td> 
							<?php 
								if ( $CTCACCUnageE != 0 ){
									$PercOvenE2 = (($CTCACCOvenE / $CTCACCUnageE) * 100); 
									echo number_format((float)($PercOvenE2), 2, '.', '');
								}else{
									echo $CTCACCOvenE;
								}
							?> 
						</td>
					</tr>
		<?php
			}
		?>
			<!-- ###################### Oil Aging	 -->
		<?php
			if ( $CTOilAging ){
		?>
				<tr>
					<td colspan='3'> <strong> Oil Aging </strong> </td>
				</tr>
				<!-- ###################### Oil Aged Tensile	 -->
					<tr>
						<td > > %, Tensile Strength </td>
							<td> 
								<?php 
									if ( $CTClientsUnageT != 0 ){
										$PercOilTS1 = (($CTClientsOilT / $CTClientsUnageT) * 100); 
										echo number_format((float)($PercOilTS1), 2, '.', '');
									}else{
										echo $CTClientsOilT;
									}
								?> 
							</td>
							<td> 
								<?php 
									if ( $CTCACCUnageT != 0 ){
										$PercOilTS2 = (($CTCACCOilT / $CTCACCUnageT) * 100); 
										echo number_format((float)($PercOilTS2), 2, '.', '');
									}else{
										echo $CTCACCOilT;
									}
								?> 
							</td>
					</tr>
				<!-- ###################### Oil Aged Elongation	 -->
					<tr>
						<td > > %, Elongation </td>
							<td> 
								<?php 
									if ( $CTClientsUnageE != 0 ){
										$PercOilE1 = (($CTClientsOilE/ $CTClientsUnageE) * 100); 
										echo number_format((float)($PercOilE1), 2, '.', '');
									}else{
										echo $CTClientsOilE                                                                                                                                    ;
									}
								?> 
							</td>
							<td> 
								<?php 
									if ( $CTCACCUnageE != 0 ){
										$PercOilE2 = (($CTCACCOilE / $CTCACCUnageE) * 100); 
										echo number_format((float)($PercOilE2), 2, '.', '');
									}else{
										echo $CTCACCOilE                                                                                                                                     ;
									}
								?> 
							</td>
					</tr>
<?php
			}
		}else{
	?>
			<!-- ###################### H-Shore D	 -->
				<tr>
					<td> H-Shore D </td>
					<td> <?php echo $CTClientsHD; ?> </td>
					<td> <?php echo $CTCACCHD; ?> </td>
				</tr>
<?php		
		}
	}
	$db->next_result();
	$resultN->close();
}
?>
	</table>
	<br>
<?php	

	require("database_close.php");
?>