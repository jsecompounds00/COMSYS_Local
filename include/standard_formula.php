<?php

$formula_type = intval($_GET['formula_type']);
$trial = intval($_GET['trial']);
$sltActivity = intval($_GET['sltActivity']);
// echo $formula_type;
	session_start();
	require("/database_connect.php");
	require("/init_unset_values/batch_ticket_nrm_init_value.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>standard_formula.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else{
		$qryf = mysqli_prepare($db, "CALL sp_Formula_Dropdown(?)");
		mysqli_stmt_bind_param($qryf, "i", $formula_type);
		$qryf -> execute();
		$resultf = mysqli_stmt_get_result($qryf);
		$processErrorf = mysqli_error($db);

			$id = array();
			$phr = array();
			$rm_type_id = array();
			$rm_type = array();
			$rm_id = array();
			$name = array();
			$rm_code = array();
		while($row = mysqli_fetch_assoc($resultf))
		{
			$id[] = $row['id'];
			$phr[] = $row['phr'];
			$rm_type_id[] = $row['rm_type_id'];
			$rm_type[] = $row['rm_type'];
			$rm_id[] = $row['raw_materials_id'];
			$name[] = $row['name'];
			$rm_code[] = $row['rm_code'];
		}
			$count = count($id);
			$additional_lines = 15 - $count;
		$db->next_result();
		$resultf->close();

		if( $sltActivity != 5 )
		{
			if ($processErrorf){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>standard_formula.php'.'</td><td>'.$processErrorf.' near line 43.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
?>				<tr>
					<th>Exclude?</th>
					<th>Raw Materials</th>
					<th>PHR</th>
					<th>Quantity</th>
				</tr>
				<?php 		
					foreach ($id as $key => $formula_id) { 
				?>
						<tr>
							<td>
								<?php 				
									if ( $trial == 1 ){ 
								?>
										<input type='checkbox' name='chkExclude[]' id='chkExclude<?php echo $key;?>' value='<?php echo $key;?>' onchange='exclude(), autoMultiply(), autoSum()' disabled>
								<?php 
									}else{ 
								?>
										<input type='checkbox' name='chkExclude[]' id='chkExclude<?php echo $key;?>' value='<?php echo $key;?>' onchange='exclude(), autoMultiply(), autoSum()'>
								<?php 				
									} 
								?>
								<input type='hidden' name='hidBatchTicketTrialItemId[]' value='0'>
							</td>
							<td>
								<input type='text' name='txtRM[]' id='txtRM<?php echo $key;?>' value='<?php echo $rm_type[$key].' '.$rm_code[$key]." : ".$name[$key];?>' readonly>
								<input type='hidden' name='txtRM_type_id[]' value='<?php echo $rm_type_id[$key];?>'>
								<input type='hidden' name='txtRM_id[]' value='<?php echo $rm_id[$key];?>'>
							</td>
							<td>
								<input type='text' name='txtPhr[]' id='txtPhr<?php echo $key;?>' value='<?php echo $phr[$key];?>' readonly onkeyup='autoMultiply(), autoSum()'>
								<input type='hidden' name='hidPhr[]' id='hidPhr<?php echo $key;?>' value='<?php echo $phr[$key];?>'>
							</td>
							<td>
								<input type='text' name='txtQty[]' id='txtQty<?php echo $key;?>' value='' readonly>
							</td>
						</tr>
		<?php			
					}
			} 	
		?>
				<tr>
					<td> NRM
						<input type='hidden' name='txtNRm_type_id' value='1000'>
					</td>
					<td>
						<select name='txtNRm_id'>
						<?php
							$rm_type = 1000;

							$qry = mysqli_prepare($db, "CALL sp_RM_BAT_Dropdown(?)");
							mysqli_stmt_bind_param($qry, 'i', $rm_type);
							$qry->execute();
							$result = mysqli_stmt_get_result($qry);
							$processError = mysqli_error($db);
						
							if ($processError){
								error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>standard_formula.php'.'</td><td>'.$processError.' near line 100.</td></tr>', 3, "errors.php");
								header("location: error_message.html");
							}
							else
							{	
								echo "<option value='0'></option>";
								
								while($row = mysqli_fetch_assoc($result))
								{ 
									$id = $row['id'];
									$RMName = $row['RMName'];
									
									if ( $initNRm_id_NRM == $id ){
										echo "<option value='".$id."' selected>".$RMName."</option>";
									}else{
										echo "<option value='".$id."'>".$RMName."</option>";
									}

								}
								$db->next_result();
								$result->close();
							}
						?>
						</select>
						<input type='hidden' name='hidNBatchTicketTrialItemId' value='0'>
					</td>
					<td>
						<input type='text' name='txtNPHR' id='txtNPHR' onchange='autoMultiply(), autoSum()' onkeyup='autoMultiply(), autoSum()' value='<?php echo $initNPHR_NRM; ?>'>
					</td>
					<td>
						<input type='text' name='txtNQuantity' id='txtNQuantity' readonly value=''>
					</td>
				</tr>
				<tr id='total' class="spacing border_top">
					<td colspan='3' align='right' style='padding-right:20px;'><b>TOTAL</b></td>
					<td>
						<input type='text' name='txtTotalQty' id='txtTotalQty' readonly value=''>
					</td>
				</tr> 
<?php
		}
		elseif( $sltActivity == 5 )
		{		
			if ($processErrorf){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>standard_formula.php'.'</td><td>'.$processErrorf.' near line 141.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
?>				<tr>
					<th>Exclude?</th>
					<th>Raw Materials</th>
					<th>PHR</th>
					<th>Quantity</th>
				</tr>
<?php 			foreach ($id as $key => $formula_id) { ?>
					<tr>
						<td>
							<input type='checkbox' name='chkExclude[]' id='chkExclude<?php echo $key;?>' value='<?php echo $key;?>' onchange='excludeModify(), autoMultiply(), autoSum()'>
							<input type='hidden' name='hidBatchTicketTrialItemId[]' value='0'>
						</td>
						<td>
							<select name='txtRM_id[]' id='txtRM_id<?php echo $key;?>'>
								<?php
									$qryRMD = "CALL sp_BAT_RawMaterials_Dropdown()";
									$resultRMD = mysqli_query($db, $qryRMD);
									$processErrorRMD = mysqli_error($db);
								
									if ($processErrorRMD){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>standard_formula.php'.'</td><td>'.$processErrorRMD.' near line 172.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{	
										echo "<option value='0'></option>";
										
										while($row = mysqli_fetch_assoc($resultRMD))
										{ 
											$RMId = $row['id'];
											$RMName = $row['name'];
											
											if ( $rm_id[$key] == $RMId ){
												echo "<option value='".$RMId."' selected>".$RMName."</option>";
											}else{
												echo "<option value='".$RMId."'>".$RMName."</option>";
											}

										}
										$db->next_result();
										$resultRMD->close();
									}
								?>
							</select>
							<input type='hidden' name='txtRM_type_id[]' value='<?php echo $rm_type_id[$key];?>'>
						</td>
						<td>
							<input type='text' name='txtPhr[]' id='txtPhr<?php echo $key;?>' value='<?php echo $phr[$key];?>' onkeyup='autoMultiply(), autoSum()'>
							<input type='hidden' name='hidPhr[]' id='hidPhr<?php echo $key;?>' value='<?php echo $phr[$key];?>'>
						</td>
						<td>
							<input type='text' name='txtQty[]' id='txtQty<?php echo $key;?>' value='0' readonly>
						</td>
					</tr>
<?php			}
				for ( $i = $count; $i < ($count + $additional_lines); $i++ ){
?>			
					<tr>
						<td>
							<input type='checkbox' name='chkExclude[]' id='chkExclude<?php echo $i;?>' value='<?php echo $i;?>' onchange='excludeModify(), autoMultiply(), autoSum()'>
							<input type='hidden' name='hidBatchTicketTrialItemId[]' value='0'>
						</td>
						<td>
							<select name='txtRM_id[]' id='txtRM_id<?php echo $i;?>'>
								<?php
									$qryRMD = "CALL sp_BAT_RawMaterials_Dropdown()";
									$resultRMD = mysqli_query($db, $qryRMD);
									$processErrorRMD = mysqli_error($db);
								
									if ($processErrorRMD){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>standard_formula.php'.'</td><td>'.$processErrorRMD.' near line 172.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{	
										echo "<option value='0'></option>";
										
										while($row = mysqli_fetch_assoc($resultRMD))
										{ 
											$RMId = $row['id'];
											$RMName = $row['name'];
											
												echo "<option value='".$RMId."'>".$RMName."</option>";

										}
										$db->next_result();
										$resultRMD->close();
									}
								?>
							</select>
							<input type='hidden' name='txtRM_type_id[]' value=''>
						</td>
						<td>
							<input type='text' name='txtPhr[]' id='txtPhr<?php echo $i;?>' value='' onkeyup='autoMultiply(), autoSum()'>
							<input type='hidden' name='hidPhr[]' id='hidPhr<?php echo $i;?>' value=''>
						</td>
						<td>
							<input type='text' name='txtQty[]' id='txtQty<?php echo $i;?>' value='0' readonly>
						</td>
					</tr>
<?php			} 
			}
?>			
				<tr>
					<td>
						<input type='hidden' name='txtNRm_type_id' value='1000'>
						<input type='hidden' name='txtNRm_id' value='0'>
						<input type='hidden' name='hidNBatchTicketTrialItemId' value='0'>
						<input type='hidden' name='txtNPHR' id='txtNPHR' onchange='autoMultiply(), autoSum()' onkeyup='autoMultiply(), autoSum()' value='0'>
						<input type='hidden' name='txtNQuantity' id='txtNQuantity' readonly value='0'>
					</td>
				</tr>
				<tr id='total' class="spacing border_top">
					<td colspan='3' align='right' style='padding-right:20px;'><b>TOTAL</b></td>
					<td>
						<input type='text' name='txtTotalQty' id='txtTotalQty' readonly value=''>
					</td>
				</tr> 
<?php
		}
	}
	require("/init_unset_values/batch_ticket_nrm_unset_value.php");
	require("database_close.php");
?>