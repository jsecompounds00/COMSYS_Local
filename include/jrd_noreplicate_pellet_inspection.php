<?php

	require("database_connect.php");

	$qryPI = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Pellet_Inspection_Query( ?, ?, ? )");
	mysqli_stmt_bind_param($qryPI, 'isi', $JRDNoReplicateID, $product_for_evaluation, $FGID);
	$qryPI->execute();
	$resultPI = mysqli_stmt_get_result($qryPI);
	$processErrorPI = mysqli_error($db);

	if ( !empty($processErrorPI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_pellet_inspection.php'.'</td><td>'.$processErrorPI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultPI)){
			$clarity_color = $row['clarity_color'];
			$PIcreated_at = $row['created_at'];
			$PIcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultPI->close();
	}

?>
	
	<table class='results_child_tables_form'>
		<col width='250'></col>
		<tr></tr>
		<tr>
			<td >Clarity / Color Conform:</td>
			<td>
				<input type='checkbox' name='chkClarityColorConform' <?php echo ( $pellet_inspect_id ? ( $clarity_color ? "checked" : "" ) : ( $initNRPIClarityColorConform ? "checked" : "" ) ) ?>>
			</td>
		</tr>
		<input type='hidden' name='hidSICreatedAt' value='<?php echo ( $pellet_inspect_id ? $PIcreated_at : date('Y-m-d H:i:s') );?>'>
		<input type='hidden' name='hidSICreatedId' value='<?php echo ( $pellet_inspect_id ? $PIcreated_id : 0 );?>'>
	</table>
<?php	 require("database_close.php"); ?>