<?php
	$BankCodeID = intval($_GET["sltBankCode"]);
	$CheckNumber = strval($_GET["txtChecknumber"]);
	$CustomerID = intval($_GET["sltCustomer"]);
	$JacketID = intval($_GET["jacket_id"]);
	
	require ("database_connect.php");

	$qry = mysqli_prepare($db, "CALL sp_Check_Related_Info_Dropdown(?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isii', $BankCodeID, $CheckNumber, $CustomerID, $JacketID);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>check_related_invoices.php'.'</td><td>'.$processError.' near line 16.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{ 
		while( $row = mysqli_fetch_assoc( $result ) ){
			echo number_format((float)($row['discount']), 2, '.', ',').'<br>';
			echo "<input type='hidden' name='txtDiscount[]' id='txtDiscount' value='".$row['discount']."'>";
		}
	}

	$db->next_result();
	$result->close(); 

	require("database_close.php");

?>