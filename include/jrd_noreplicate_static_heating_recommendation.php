<?php

	require("database_connect.php");

	$qrySH = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Static_Heat_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qrySH, 'is', $TypeID, $Type);
	$qrySH->execute();
	$resultSH = mysqli_stmt_get_result($qrySH);
	$processErrorSH = mysqli_error($db);

?>
	
	<table class="results_child_tables_form">
		<?php
			if ( !empty($processErrorSH) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_static_heating_recommendation.php'.'</td><td>'.$processErrorSH.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultSH)){
					$SHJRDTestID = $row['SHJRDTestID'];
					$SHJRDFGSoft = $row['SHJRDFGSoft'];

					$SHClientsProduct = $row['SHClientsProduct']; 
					$SHCACCProduct = $row['SHCACCProduct']; 

					$SHClientsColorConformance = $row['SHClientsColorConformance']; 
					$SHCACCColorConformance = $row['SHCACCColorConformance']; 

					$SHClientsHeatStability = $row['SHClientsHeatStability']; 
					$SHCACCHeatStability = $row['SHCACCHeatStability']; 
		?>
					<tr>
						<th></th>
						<th> <?php echo $SHClientsProduct; ?> </th>
						<th> <?php echo $SHCACCProduct; ?> </th>
					</tr>
				<!-- ###################### Color Conformance	 -->	
					<tr>
						<td> Color Conformance </td>
						<td> <?php echo $SHClientsColorConformance; ?> </td>
						<td> <?php echo $SHCACCColorConformance; ?> </td>
					</tr>
				<!-- ###################### Heat Stability	 -->	
					<tr>
						<td> Heat Stability </td>
						<td> <?php echo $SHClientsHeatStability; ?> </td>
						<td> <?php echo $SHCACCHeatStability; ?> </td>
					</tr>
		<?php
				}
				$db->next_result();
				$resultSH->close();
			}
		?>
	</table>
	<br>
<?php	

	require("database_close.php");
?>