<?php
	session_start(); 
	ob_start();
	require("include/constant.php");
	require("include/database_connect.php");
?>
	<link rel="icon" type="image/png" href="images/favicon.ico">
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="css/navigation.css">
<?php
	
//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if ( $errno ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>header.php'.'</td><td>'.$error.' near line 18.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		if(!isset($_SESSION['SESS_USER_NAME']) || (trim($_SESSION['SESS_USER_NAME']) == '')) 
		{
			header("location: login.php");
			exit();
		} else{
			$session_roleId = $_SESSION['SESS_ROLE_ID'];
			$session_roleId  = array();
			$session_roleId = explode(',',$_SESSION["SESS_ROLE_ID"]); //user permission

				foreach ($session_roleId as $key => $roleId) {

					$qryR = mysqli_prepare($db, "CALL sp_Role_Query(?)");
					mysqli_stmt_bind_param($qryR, 'i', $roleId);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR);
					$processError = mysqli_error($db);

					if ( !empty($processError) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>header.php'.'</td><td>'.$processError.' near line 42.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						$row = mysqli_fetch_assoc($resultR);
						$permitID[] = $row['perm_id'];
					}

					$db->next_result();
					$resultR->close(); 
				}
				$permit = '0'.implode(',',$permitID);
			
			$session_Permit  = array();
			$session_Permit = explode(',',$permit); //user permission

			$PermitUserID = $_SESSION['SESS_USER_ID'];

			$qryU = mysqli_prepare($db, "CALL sp_User_Division_Query(?)");
			mysqli_stmt_bind_param($qryU, 'i', $PermitUserID);
			$qryU->execute();
			$resultU = mysqli_stmt_get_result($qryU);
			$processErrorU = mysqli_error($db);

			if ( !empty($processErrorU) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>header.php'.'</td><td>'.$processErrorU.' near line 42.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				// $row = mysqli_fetch_assoc($resultU);
				while ( $row = mysqli_fetch_assoc( $resultU ) ){
					$compounds = $row['compounds'];
					$pipes = $row['pipes'];
					$corporate = $row['corporate'];
					$ppr = $row['ppr'];
				}
			}

			$db->next_result();
			$resultU->close(); 

	?>
			<div class="head_title_home">

				<a href="comsys.php" id="head">
					<h1> C O M S Y S </h1> 
				</a>

				<div class="welcome">
					Welcome, <?php echo $_SESSION["SESS_FIRST_NAME"].' '. $_SESSION["SESS_LAST_NAME"];?> .
					<label class="logout"> <a href="logout.php" >Logout</a> </label>
				</div>

		        <ul id="menu">
		            <li><a href="#">MANAGE</a>
		               <ul>
		                    <?php $init = "%";
							//	PG_RAW_MATERIAL_TYPE_HOME
								if(array_search(175, $session_Permit)){
									echo "<li><a href='raw_material_types.php?page=1&search=&qsone='>RM TYPES</a></li>";
									$_SESSION['rm_type'] = true;
								}else{
									unset($_SESSION['rm_type']);
								}
							//	PG_SUPPLY_TYPE_HOME
								if(array_search(1, $session_Permit)){
									echo "<li><a href='".PG_SUPPLY_TYPE_HOME."'>SUPPLY TYPES</a></li>";
									$_SESSION['supply_type'] = true;
								}else{
									unset($_SESSION['supply_type']);
								}
							//	PG_UOM_HOME
								if(array_search(7, $session_Permit)){
									// echo "<li><a href='".PG_UOM_HOME."'>UOM</a></li>";
									$_SESSION['uom'] = true;
								}else{
									unset($_SESSION['uom']);
								}
										
								echo "</li>";
							//	PG_SUPPLIER_HOME
								if(array_search(13, $session_Permit)){
									echo "<li><a href='".PG_SUPPLIER_HOME."'>SUPPLIERS</a></li>";
									$_SESSION['supplier'] = true;
								}else{
									unset($_SESSION['supplier']);
								}
							//	PG_CUSTOMER_HOME
								if(array_search(14, $session_Permit)){
									echo "<li><a href='".PG_CUSTOMER_HOME."'>CUSTOMERS</a></li>";
									$_SESSION['customer'] = true;
								}else{
									unset($_SESSION['customer']);
								}
							//	PG_DEPARTMENT_HOME
								if(array_search(16, $session_Permit)){
									echo "  	  <li><a href='".PG_DEPARTMENT_HOME."'>DEPARTMENTS</a></li>";
									$_SESSION['department'] = true;
								}else{
									unset($_SESSION['department']);
								}
							//	PG_USER_HOME
								if(array_search(11, $session_Permit)){
									echo "<li><a href='".PG_USER_HOME."'>USERS</a>";
									$_SESSION['user'] = true;
								}else{
									unset($_SESSION['user']);
									if(array_search(12, $session_Permit) || array_search(16, $session_Permit) || array_search(17, $session_Permit))
									{
										echo "<li><a href='#'>USERS</a>";
									}
								}
							// 	PG_ROLE_HOME
								if(array_search(12, $session_Permit)){
									echo "  	  <li><a href='".PG_ROLE_HOME."'>ROLES</a></li>";
									$_SESSION['role'] = true;
								}else{
									unset($_SESSION['role']);
								}
							// 	Permissions
								if(array_search(17, $session_Permit)){
									echo " 		  <li><a href='".PG_PERMISSION_HOME."'>PERMISSIONS</a></li>";
									$_SESSION['permission'] = true;
								}else{
									unset($_SESSION['permission']);
								}
							//	Data Migration
								if(array_search(15, $session_Permit)){
									echo "<li><a href='".PG_MIGRATION_HOME."'>DATA MIGRATION</a></li>";
									$_SESSION['migration'] = true;
								}else{
									unset($_SESSION['migration']);
								}
							//	Error Viewing
								if(array_search(31, $session_Permit)){
									echo "<li><a href='caught_errors.php'>ERRORS</a></li>";
									$_SESSION['error'] = true;
								}else{
									unset($_SESSION['error']);
								}
							?>
		                </ul>
		            </li>
		            <li><a href="#">COMPOUNDS</a>
		                <ul>
		                	<?php 
		                		if( array_search(8, $session_Permit) || array_search(43, $session_Permit)
		                			|| array_search(59, $session_Permit) || array_search(44, $session_Permit)
		                			|| array_search(47, $session_Permit) || array_search(45, $session_Permit)
		                			|| array_search(46, $session_Permit) || array_search(76, $session_Permit)
		                			|| array_search(75, $session_Permit) ){
		                	?>
				                    <li><a href="#">WPD</a>
				                        <ul>
				                            <?php $init = "%";
											//	PG_SUPPLIES_HOME
												if(array_search(8, $session_Permit)){
													echo "<li><a href='".PG_SUPPLIES_HOME."'>SUPPLIES</a></li>";
													$_SESSION['supplies'] = true;
												}else{
													unset($_SESSION['supplies']);
												}
											//	Raw Materials
												if(array_search(43, $session_Permit)){
													echo "<li><a href='raw_materials.php?page=1&name_text=&code_text=&type_text='>RAW MATERIALS</a></li>";
													$_SESSION['rm'] = true;
												}else{
													unset($_SESSION['rm']);
												}
											//	Finished Goods
												if(array_search(59, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='finished_goods.php?page=1&search=&qsone='>FINISHED GOODS</a></li>";
													$_SESSION['fg'] = true;
												}else{
													unset($_SESSION['fg']);
												}
											//	LMR
												// if(array_search(44, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='lmr.php?page=1&search=&qsone='>LMR</a></li>";
												// 	$_SESSION['lmr'] = true;
												// }else{
												// 	unset($_SESSION['lmr']);
												// }
											//	Trucks
												// if(array_search(47, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='trucks.php?page=1&search=&qsone='>TRUCKS</a></li>";
												// 	$_SESSION['trucks'] = true;
												// }else{
												// 	unset($_SESSION['trucks']);
												// }
											//	Toll Points
												// if(array_search(45, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='toll_points.php?page=1&search=&qsone='>TOLL POINTS</a></li>";
												// 	$_SESSION['toll_points'] = true;
												// }else{
												// 	unset($_SESSION['toll_points']);
												// }
											//	Toll Matrix
												// if(array_search(46, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='toll_matrix.php?page=1&search=&qsone='>TOLL MATRIX</a></li>";
												// 	$_SESSION['toll_matrix'] = true;
												// }else{
												// 	unset($_SESSION['toll_matrix']);
												// }
											//	Positions
												// if(array_search(76, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='positions.php?page=1&search=&qsone='>POSITIONS</a></li>";
												// 	$_SESSION['position'] = true;
												// }else{
												// 	unset($_SESSION['position']);
												// }
											//	Labor
												// if(array_search(75, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='labor.php?page=1&search=&qsone='>LABOR</a></li>";
												// 	$_SESSION['labor'] = true;
												// }else{
												// 	unset($_SESSION['labor']);
												// }
											?>
				                        </ul>
				                    </li>
		                    <?php 
		                		} 

			                    if( array_search(48, $session_Permit) || array_search(49, $session_Permit)
			                    			|| array_search(50, $session_Permit) || array_search(51, $session_Permit)
			                    			|| array_search(77, $session_Permit) || array_search(78, $session_Permit)
			                    			|| array_search(79, $session_Permit) || array_search(82, $session_Permit) 
			                    			|| array_search(146, $session_Permit) ){
			                ?>
				                    <li><a href="#">PAM</a>
				                        <ul>
				                            <?php $init = "%";
											//	Mixers
												if(array_search(146, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='mixers.php?page=1&search=&qsone='>MIXERS</a></li>";
													$_SESSION['mixers'] = true;
												}else{
													unset($_SESSION['mixers']);
												}	
											//	Equipment
												// if(array_search(48, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='".PG_EQUIPMENT_HOME."'>EQUIPMENT</a></li>";
												// 	$_SESSION['equipment'] = true;
												// }else{
												// 	unset($_SESSION['equipment']);
												// }	
											//	Location
												// if(array_search(49, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='".PG_LOCATION_HOME."'>LOCATION</a></li>";
												// 	$_SESSION['location'] = true;
												// }else{
												// 	unset($_SESSION['location']);
												// }	
											//	Equipment Status
												// if(array_search(50, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='".PG_EQUIPMENT_STATUS_HOME."'>EQUIPMENT STATUS</a></li>";
												// 	$_SESSION['e_status'] = true;
												// }else{
												// 	unset($_SESSION['e_status']);
												// }	
											//	Equipment Measure
												// if(array_search(51, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='".PG_EQUIPMENT_MEASURE_HOME."'>EQUIPMENT MEASURE</a></li>";
												// 	$_SESSION['e_measure'] = true;
												// }else{
												// 	unset($_SESSION['e_measure']);
												// }		
											//	Engineering Items Records
												// if(array_search(77, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='engineering_item_records.php?page=1&search=&qsone='>ENGINEERING ITEM RECORDS</a></li>";
												// 	$_SESSION['e_records'] = true;
												// }else{
												// 	unset($_SESSION['e_records']);
												// }	
											// Line Copmponents
												// if(array_search(78, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='line_components.php?page=1&search=&qsone='>LINE COMPONENTS</a></li>";
												// 	$_SESSION['components'] = true;
												// }else{
												// 	unset($_SESSION['components']);
												// }
											//	kw Rate
												// if(array_search(82, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='monthly_kw_rate.php?page=1&search=&qsone='>MONTHLY KW RATE</a></li>";
												// 	$_SESSION['monthly_kw_rate'] = true;
												// }else{
												// 	unset($_SESSION['monthly_kw_rate']);
												// }
											//	PAM Analysis
												// if(array_search(79, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='pam_analysis.php?page=1&search=&qsone='>PAM ANALYSIS</a></li>";
												// 	$_SESSION['pam_analysis'] = true;
												// }else{
												// 	unset($_SESSION['pam_analysis']);
												// }	
											?>
				                        </ul>
				                    </li>
		                    <?php 
		                		} 
		                    
		                    	if( array_search(39, $session_Permit) || array_search(74, $session_Permit) ){
		                    ?>
			                        <li><a href="#">TECHNICAL</a>
			                            <ul>
			                                <?php $init = "%";
											//	FINISHED GOOD CATEGORY
												if(array_search(197, $session_Permit)){
													echo "<li><a href='finished_goods_category.php?page=1&search=&qsone='>FG CATEGORY</a></li>";
													$_SESSION['fg_category'] = true;
												}else{
													unset($_SESSION['fg_category']);
												}
											//	NRM
												if(array_search(126, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='".PG_NRM_HOME."'>NRM</a></li>";
													$_SESSION['nrm_home'] = true;
												}else{
													unset($_SESSION['nrm_home']);
												}

											//	JRD
												if(array_search(182, $session_Permit)){
													echo "<li><a href='jrd.php?page=1&search=&qsone='>R&D</a></li>";
													$_SESSION['jrd'] = true;
												}else{
													unset($_SESSION['jrd']);
												}

											//	BAtch Ticket
												if(array_search(129, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='batch_ticket.php?page=1&search=&qsone='>BATCH TICKET</a></li>";
													$_SESSION['bat_home'] = true;
												}else{
													unset($_SESSION['bat_home']);
												}

												$init = "%";
											//	Moisture Content
												if(array_search(74, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='moisture_content.php?page=1&name_text=&code_text=&type_text='>MOISTURE CONTENT</a></li>";
													$_SESSION['mc'] = true;
												}else{
													unset($_SESSION['mc']);
												}
											?>
			                            </ul>
			                        </li>
		                    <?php 
		                    	}
		                   		if( array_search(81, $session_Permit) || array_search(115, $session_Permit) ){
		                   	?>
			                        <li><a href="#">QMS</a>
			                            <ul>
			                                <?php $init = "%";
											//	CPIAR Monitoring
												if(array_search(81, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='cpiar_monitoring.php?page=1&search=&qsone='>CPIAR MONITORING</a></li>";
													$_SESSION['cpr'] = true;
												}else{
													unset($_SESSION['cpr']);
												}
											//	ICC Monitoring
												if(array_search(81, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='icc_monitoring.php?page=1&search=&qsone='>ICC MONITORING</a></li>";
													$_SESSION['icc'] = true;
												}else{
													unset($_SESSION['icc']);
												}
											//	CCR Monitoring
												if(array_search(115, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='ccr_monitoring.php?page=1&search=&qsone='>CCR MONITORING</a></li>";
													$_SESSION['ccr'] = true;
												}else{
													unset($_SESSION['ccr']);
												}
											?>
			                            </ul>
			                        </li>
		                    <?php 
		                 		} 
		                    	if( array_search(83, $session_Permit) || array_search(88, $session_Permit) ){
		                    ?>
			                        <li><a href="#">SALES</a>
			                            <ul>
			                                <?php $init = "%";
											//	Sales Order
												if(array_search(83, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='sales_order.php?page=1&so_no=&so_date=&so_type='>SALES ORDER</a></li>";
													$_SESSION['sales_order'] = true;
												}else{
													unset($_SESSION['sales_order']);
												}
											//	Credit Terms
												if(array_search(110, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='credit_terms.php?page=1&search=&qsone='>LC (EXPORT)</a></li>";
													$_SESSION['credit_terms'] = true;
												}else{
													unset($_SESSION['credit_terms']);
												}
											//	Sales Forecast Monthly
												// if(array_search(88, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
												// 	echo "<li><a href='sales_forecast.php?page=1&search=&qsone='>SALES FORECAST (MONTHLY)</a></li>";
												// 	$_SESSION['sales_forecast'] = true;
												// }else{
												// 	unset($_SESSION['sales_forecast']);
												// }
											//	Sales Forecast Annual
												if(array_search(105, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='sales_forecast_annual.php?page=1&fg=&customer&year='>SALES FORECAST (ANNUAL)</a></li>";
													$_SESSION['sales_forecast_annual'] = true;
												}else{
													unset($_SESSION['sales_forecast_annual']);
												}
											//	Sales Quote
												if(array_search(105, $session_Permit)){ // && $_SESSION["SESS_COMPOUNDS"] == 1
													echo "<li><a href='sales_quote.php?page=1&search=&qsone='>SALES QUOTE</a></li>";
													$_SESSION['sales_quote'] = true;
												}else{
													unset($_SESSION['sales_quote']);
												}
											?>
			                            </ul>
			                        </li>
		                    <?php 
		                     	} 
		                     ?>
		                </ul>
		        	</li>
		            <li><a href="#">CORPORATE</a>
						<ul>
							<?php 
								if( array_search(34, $session_Permit) || array_search(52, $session_Permit) ){
							?>
									<li><a href="#">PURCHASING</a>
										<ul>
											<?php $init = "%";
											//	PRE Monitoring
												if(array_search(34, $session_Permit)){
													echo "<li><a href='".PG_PRE_HOME."'>P.R.E.</a></li>";
													$_SESSION['pre_home'] = true;
												}else{
													unset($_SESSION['pre_home']);
												}
											//	CANVASSING
												if(array_search(183, $session_Permit)){
													echo "<li><a href='canvass.php?page=1&search=&qsone='>CANVASSING</a></li>";
													$_SESSION['canvass_home'] = true;
												}else{
													unset($_SESSION['canvass_home']);
												}	
											//	PO Monitoring
												if(array_search(52, $session_Permit)){
													echo "<li><a href='".PG_PO_HOME."'>P.O.</a></li>";
													$_SESSION['po_home'] = true;
												}else{
													unset($_SESSION['po_home']);
												}	
											?>		
										</ul>
									</li>
		                    <?php 
		                		} 
		                 		if( array_search(89, $session_Permit) || array_search(92, $session_Permit)  || array_search(102, $session_Permit) ){
		                 	?>
									<li><a href="#">ADMIN</a>
										<ul>
											<?php $init = "%";
											//	Property Type
												if(array_search(89, $session_Permit)){
													echo "<li><a href='property_type.php?page=1&search=&qsone='>ASSET TYPE</a></li>";
													$_SESSION['property_type'] = true;
												}else{
													unset($_SESSION['property_type']);
												}
											//	Property Transfer
												if(array_search(92, $session_Permit)){
													echo "<li><a href='property_transfer.php?page=1&search=&qsone='>PTS</a></li>";
													$_SESSION['property_transfer'] = true;
												}else{
													unset($_SESSION['property_transfer']);
												}
											?>		
										</ul>
									</li>
							<?php 
								} 
								if( array_search(140, $session_Permit) ){
							?>
									<li><a href="#">C&C</a>
										<ul>
											<?php $init = "%";
											//	Customer Jacket
												if(array_search(140, $session_Permit)){
													echo "<li><a href='customer_jacket.php?page=1&search=&qsone='>CUSTOMER JACKET</a></li>";
													$_SESSION['customer_jacket'] = true;
												}else{
													unset($_SESSION['customer_jacket']);
												}
											?>		
										</ul>
									</li>
							<?php 
								} 
							?>
						</ul>
					</li>
					<?php 
						if( array_search(148, $session_Permit) ){ // && $_SESSION["SESS_COMPOUNDS"] == 1
					?>
							<li>
								<!-- <a href="resin_calculator.php">RESIN CALCULATOR</a> -->
							</li>
					<?php 
							$_SESSION['resin_calculator'] = true;
						}else{
							unset($_SESSION['resin_calculator']);
						}

						if( array_search(178, $session_Permit) ){ // && $_SESSION["SESS_COMPOUNDS"] == 1
					?>
							<li>
								<!-- <a href="ticketing_system.php?page=1&search=RECEIVED&qsone=">TICKETS</a> -->
							</li>
					<?php 
							$_SESSION['ticketing_system'] = true;
						}else{
							unset($_SESSION['ticketing_system']);
						}
						
						// if(array_search(199, $session_Permit) || array_search(200, $session_Permit) || array_search(201, $session_Permit) || array_search(202, $session_Permit) || array_search(203, $session_Permit) 
						// 	|| array_search(204, $session_Permit)|| array_search(205, $session_Permit)|| array_search(206, $session_Permit)|| array_search(207, $session_Permit)|| array_search(208, $session_Permit)){
					?>
							<li><a href="#">BUDGET</a>
								<ul>
									<?php
										// if(array_search(199, $session_Permit) || array_search(200, $session_Permit) || array_search(201, $session_Permit) || array_search(202, $session_Permit) || array_search(203, $session_Permit)){
									?>
											<li><a href="#"> ACCOUNT CODES </a>
												<ul>
													<?php
														// if(array_search(199, $session_Permit)){
													?>
															<li> <a href="expense_code.php?page=1&search=&qsone="> EXPENSE CODE </a> </li>
													<?php
														// 	$_SESSION['expense_code_home'] = true;
														// }else{
														// 	unset($_SESSION['expense_code_home']);
														// }

														// if(array_search(203, $session_Permit)){
													?>
															<li> <a href="budget_department_code.php?page=1&search=&qsone="> DEPARTMENT CODE </a> </li>
													<?php
														// 	$_SESSION['department_code_home'] = true;
														// }else{
														// 	unset($_SESSION['department_code_home']);
														// }
													?>
												</ul>
											</li>
									<?php
										// }
										// if(array_search(204, $session_Permit) || array_search(205, $session_Permit) || array_search(206, $session_Permit) || array_search(207, $session_Permit) || array_search(208, $session_Permit)
										// 	|| array_search(209, $session_Permit) || array_search(210, $session_Permit) || array_search(211, $session_Permit) || array_search(212, $session_Permit) || array_search(213, $session_Permit) 
										// 	|| array_search(214, $session_Permit) || array_search(215, $session_Permit)){
									?>
											<li><a href="#"> BUDGET MONITORING </a>
												<ul>
													<?php
														// if(array_search(204, $session_Permit)){
													?>
															<li> <a href="budget_monitoring_oc.php?page=1&search=&qsone="> SUPPLIES BUDGET </a> </li>
													<?php
														// 	$_SESSION['oc_budget_home'] = true;
														// }else{
														// 	unset($_SESSION['oc_budget_home']);
														// }

														// if(array_search(209, $session_Permit)){
													?>
															<li> <a href="budget_monitoring_departmental.php?page=1&search=&qsone="> DEPARTMENTAL BUDGET </a> </li>
													<?php
														// 	$_SESSION['departmental_budget_home'] = true;
														// }else{
														// 	unset($_SESSION['departmental_budget_home']);
														// }
													?>
												</ul>
											</li>
									<?php
										// }
									?>
								</ul>
							</li>
					<?php 
						// }	
					?>
		        </ul>
		    </div>
<?php 
		} 
	} 
?> 


