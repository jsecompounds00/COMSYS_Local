<?php

	require("database_connect.php");

	$qryDM = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Dynamic_Milling_Recommendation_Query( ? , ?)");
	mysqli_stmt_bind_param($qryDM, 'is', $TypeID, $Type);
	$qryDM->execute();
	$resultDM = mysqli_stmt_get_result($qryDM);
	$processErrorDM = mysqli_error($db);
?>
	
	<table class="results_child_tables_form">
		<?php

			if ( !empty($processErrorDM) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_dynamic_milling_recommendation.php'.'</td><td>'.$processErrorDM.' near line 28.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultDM)){
					$DMJRDTestID = $row['DMJRDTestID'];
					$DMJRDFGSoft = $row['DMJRDFGSoft'];

					$DMClientsProduct = $row['DMClientsProduct']; 
					$DMCACCProduct = $row['DMCACCProduct']; 

					$DMClientsColorConform = $row['DMClientsColorConform']; 
					$DMCACCColorConform = $row['DMCACCColorConform'];

					$DMClientsHeatStability = $row['DMClientsHeatStability']; 
					$DMCACCHeatStability = $row['DMCACCHeatStability'];
					
					$DMClientsProcess = $row['DMClientsProcess']; 
					$DMCACCProcess = $row['DMCACCProcess'];

					$DMClientsSampling = $row['DMClientsSampling']; 
					$DMCACCSampling = $row['DMCACCSampling']; 
		?>
					<tr>
						<th></th>
						<th> <?php echo $DMClientsProduct; ?> </th>
						<th> <?php echo $DMCACCProduct; ?> </th>
					</tr>
					<!-- ###################### SAMPLINGCX	 -->
						<tr>
							<td> Sampling </td>
							<td> <?php echo $DMClientsColorConform; ?> </td>
							<td> <?php echo $DMCACCColorConform; ?> </td>
						</tr>
					<!-- ###################### Color Conformance	 -->
						<tr>
							<td> Color Conformance </td>
							<td> <?php echo $DMClientsHeatStability; ?> </td>
							<td> <?php echo $DMCACCHeatStability; ?> </td>
						</tr>
					<!-- ###################### Heat Stability	 -->	
						<tr>
							<td> Heat Stability </td>
							<td> <?php echo $DMClientsProcess; ?> </td>
							<td> <?php echo $DMCACCProcess; ?> </td>
						</tr>
					<!-- ###################### Processability	 -->	
						<tr>
							<td> Processability </td>
							<td> <?php echo $DMClientsSampling; ?> </td>
							<td> <?php echo $DMCACCSampling; ?> </td>
						</tr>
		<?php
				}
				$db->next_result();
				$resultDM->close();
			}
		?>
	</table> 
	<br>

<?php	

	require("database_close.php");
?>