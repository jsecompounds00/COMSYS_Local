<?php

	require("database_connect.php");
	########## Getting data for Test Evaluation
		$qryN = mysqli_prepare($db, "CALL sp_BAT_Complete_Testing_Recommendation_Query( ?, ? )");
		mysqli_stmt_bind_param($qryN, 'is', $TypeID, $Type);
		$qryN->execute();
		$resultN = mysqli_stmt_get_result($qryN); 
		$processErrorN = mysqli_error($db);
		
		###################### Trial Numbers ######################
			if ( !empty($processErrorN) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>complete_testing_recommendation.php'.'</td><td>'.$processErrorN.' near line 78.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultN)){
					$CTtrial_no = $row['CTtrial_no'];
					$CTFGName = $row['CTFGName'];
					$cttrial_count = $row['cttrial_count'];
					$CTFGSoft = $row['CTFGSoft'];
					$CToven_aging = $row['CToven_aging'];
					$CToil_aging = $row['CToil_aging'];
					$StandardFromSG = $row['StandardFromSG'];
					$StandardToSG = $row['StandardToSG'];
					$StandardFromHA = $row['StandardFromHA'];
					$StandardToHA = $row['StandardToHA'];
					$StandardFromHD = $row['StandardFromHD'];
					$StandardToHD = $row['StandardToHD'];
					$StandardVR = $row['StandardVR'];
					$StandardUT = $row['StandardUT'];
					$StandardUE = $row['StandardUE'];
					$StandardOVT = $row['StandardOVT'];
					$StandardOVE = $row['StandardOVE'];
					$StandardOIT = $row['StandardOIT'];
					$StandardOIE = $row['StandardOIE'];
					$FGCategory = $row['FGCategory'];

					$CTTrial1 = $row['CTTrial1']; $CTTrial2 = $row['CTTrial2']; $CTTrial3 = $row['CTTrial3']; $CTTrial4 = $row['CTTrial4']; $CTTrial5 = $row['CTTrial5'];
					$CTTrial6 = $row['CTTrial6']; $CTTrial7 = $row['CTTrial7']; $CTTrial8 = $row['CTTrial8']; $CTTrial9 = $row['CTTrial9']; $CTTrial10 = $row['CTTrial10'];
					$CTTrial11 = $row['CTTrial11']; $CTTrial12 = $row['CTTrial12']; $CTTrial13 = $row['CTTrial13']; $CTTrial14 = $row['CTTrial14']; $CTTrial15 = $row['CTTrial15'];
					$CTTrial16 = $row['CTTrial16']; $CTTrial17 = $row['CTTrial17']; $CTTrial18 = $row['CTTrial18']; $CTTrial19 = $row['CTTrial19']; $CTTrial20 = $row['CTTrial20'];

					$SG1 = $row['SG1']; $SG2 = $row['SG2']; $SG3 = $row['SG3']; $SG4 = $row['SG4']; $SG5 = $row['SG5'];
					$SG6 = $row['SG6']; $SG7 = $row['SG7']; $SG8 = $row['SG8']; $SG9 = $row['SG9']; $SG10 = $row['SG10'];
					$SG11 = $row['SG11']; $SG12 = $row['SG12']; $SG13 = $row['SG13']; $SG14 = $row['SG14']; $SG15 = $row['SG15'];
					$SG16 = $row['SG16']; $SG17 = $row['SG17']; $SG18 = $row['SG18']; $SG19 = $row['SG19']; $SG20 = $row['SG20'];

					$HA1 = $row['HA1']; $HA2 = $row['HA2']; $HA3 = $row['HA3']; $HA4 = $row['HA4']; $HA5 = $row['HA5'];
					$HA6 = $row['HA6']; $HA7 = $row['HA7']; $HA8 = $row['HA8']; $HA9 = $row['HA9']; $HA10 = $row['HA10'];
					$HA11 = $row['HA11']; $HA12 = $row['HA12']; $HA13 = $row['HA13']; $HA14 = $row['HA14']; $HA15 = $row['HA15'];
					$HA16 = $row['HA16']; $HA17 = $row['HA17']; $HA18 = $row['HA18']; $HA19 = $row['HA19']; $HA20 = $row['HA20'];

					$VR1 = $row['VR1']; $VR2 = $row['VR2']; $VR3 = $row['VR3']; $VR4 = $row['VR4']; $VR5 = $row['VR5'];
					$VR6 = $row['VR6']; $VR7 = $row['VR7']; $VR8 = $row['VR8']; $VR9 = $row['VR9']; $VR10 = $row['VR10'];
					$VR11 = $row['VR11']; $VR12 = $row['VR12']; $VR13 = $row['VR13']; $VR14 = $row['VR14']; $VR15 = $row['VR15'];
					$VR16 = $row['VR16']; $VR17 = $row['VR17']; $VR18 = $row['VR18']; $VR19 = $row['VR19']; $VR20 = $row['VR20'];

					$HD1 = $row['HD1']; $HD2 = $row['HD2']; $HD3 = $row['HD3']; $HD4 = $row['HD4']; $HD5 = $row['HD5'];
					$HD6 = $row['HD6']; $HD7 = $row['HD7']; $HD8 = $row['HD8']; $HD9 = $row['HD9']; $HD10 = $row['HD10'];
					$HD11 = $row['HD11']; $HD12 = $row['HD12']; $HD13 = $row['HD13']; $HD14 = $row['HD14']; $HD15 = $row['HD15'];
					$HD16 = $row['HD16']; $HD17 = $row['HD17']; $HD18 = $row['HD18']; $HD19 = $row['HD19']; $HD20 = $row['HD20'];

					$UnageTS1 = $row['UnageTS1']; $UnageTS2 = $row['UnageTS2']; $UnageTS3 = $row['UnageTS3']; $UnageTS4 = $row['UnageTS4']; $UnageTS5 = $row['UnageTS5'];
					$UnageTS6 = $row['UnageTS6']; $UnageTS7 = $row['UnageTS7']; $UnageTS8 = $row['UnageTS8']; $UnageTS9 = $row['UnageTS9']; $UnageTS10 = $row['UnageTS10'];
					$UnageTS11 = $row['UnageTS11']; $UnageTS12 = $row['UnageTS12']; $UnageTS13 = $row['UnageTS13']; $UnageTS14 = $row['UnageTS14']; $UnageTS15 = $row['UnageTS15'];
					$UnageTS16 = $row['UnageTS16']; $UnageTS17 = $row['UnageTS17']; $UnageTS18 = $row['UnageTS18']; $UnageTS19 = $row['UnageTS19']; $UnageTS20 = $row['UnageTS20'];

					$UnageE1 = $row['UnageE1']; $UnageE2 = $row['UnageE2']; $UnageE3 = $row['UnageE3']; $UnageE4 = $row['UnageE4']; $UnageE5 = $row['UnageE5'];
					$UnageE6 = $row['UnageE6']; $UnageE7 = $row['UnageE7']; $UnageE8 = $row['UnageE8']; $UnageE9 = $row['UnageE9']; $UnageE10 = $row['UnageE10'];
					$UnageE11 = $row['UnageE11']; $UnageE12 = $row['UnageE12']; $UnageE13 = $row['UnageE13']; $UnageE14 = $row['UnageE14']; $UnageE15 = $row['UnageE15'];
					$UnageE16 = $row['UnageE16']; $UnageE17 = $row['UnageE17']; $UnageE18 = $row['UnageE18']; $UnageE19 = $row['UnageE19']; $UnageE20 = $row['UnageE20'];

					$OvenTS1 = $row['OvenTS1']; $OvenTS2 = $row['OvenTS2']; $OvenTS3 = $row['OvenTS3']; $OvenTS4 = $row['OvenTS4']; $OvenTS5 = $row['OvenTS5'];
					$OvenTS6 = $row['OvenTS6']; $OvenTS7 = $row['OvenTS7']; $OvenTS8 = $row['OvenTS8']; $OvenTS9 = $row['OvenTS9']; $OvenTS10 = $row['OvenTS10'];
					$OvenTS11 = $row['OvenTS11']; $OvenTS12 = $row['OvenTS12']; $OvenTS13 = $row['OvenTS13']; $OvenTS14 = $row['OvenTS14']; $OvenTS15 = $row['OvenTS15'];
					$OvenTS16 = $row['OvenTS16']; $OvenTS17 = $row['OvenTS17']; $OvenTS18 = $row['OvenTS18']; $OvenTS19 = $row['OvenTS19']; $OvenTS20 = $row['OvenTS20'];

					$OvenE1 = $row['OvenE1']; $OvenE2 = $row['OvenE2']; $OvenE3 = $row['OvenE3']; $OvenE4 = $row['OvenE4']; $OvenE5 = $row['OvenE5'];
					$OvenE6 = $row['OvenE6']; $OvenE7 = $row['OvenE7']; $OvenE8 = $row['OvenE8']; $OvenE9 = $row['OvenE9']; $OvenE10 = $row['OvenE10'];
					$OvenE11 = $row['OvenE11']; $OvenE12 = $row['OvenE12']; $OvenE13 = $row['OvenE13']; $OvenE14 = $row['OvenE14']; $OvenE15 = $row['OvenE15'];
					$OvenE16 = $row['OvenE16']; $OvenE17 = $row['OvenE17']; $OvenE18 = $row['OvenE18']; $OvenE19 = $row['OvenE19']; $OvenE20 = $row['OvenE20'];

					$OilTS1 = $row['OilTS1']; $OilTS2 = $row['OilTS2']; $OilTS3 = $row['OilTS3']; $OilTS4 = $row['OilTS4']; $OilTS5 = $row['OilTS5'];
					$OilTS6 = $row['OilTS6']; $OilTS7 = $row['OilTS7']; $OilTS8 = $row['OilTS8']; $OilTS9 = $row['OilTS9']; $OilTS10 = $row['OilTS10'];
					$OilTS11 = $row['OilTS11']; $OilTS12 = $row['OilTS12']; $OilTS13 = $row['OilTS13']; $OilTS14 = $row['OilTS14']; $OilTS15 = $row['OilTS15'];
					$OilTS16 = $row['OilTS16']; $OilTS17 = $row['OilTS17']; $OilTS18 = $row['OilTS18']; $OilTS19 = $row['OilTS19']; $OilTS20 = $row['OilTS20'];

					$OilE1 = $row['OilE1']; $OilE2 = $row['OilE2']; $OilE3 = $row['OilE3']; $OilE4 = $row['OilE4']; $OilE5 = $row['OilE5'];
					$OilE6 = $row['OilE6']; $OilE7 = $row['OilE7']; $OilE8 = $row['OilE8']; $OilE9 = $row['OilE9']; $OilE10 = $row['OilE10'];
					$OilE11 = $row['OilE11']; $OilE12 = $row['OilE12']; $OilE13 = $row['OilE13']; $OilE14 = $row['OilE14']; $OilE15 = $row['OilE15'];
					$OilE16 = $row['OilE16']; $OilE17 = $row['OilE17']; $OilE18 = $row['OilE18']; $OilE19 = $row['OilE19']; $OilE20 = $row['OilE20'];
		?>
			<table class="results_child_tables_form">
			
				<tr>
					<td colspan='<?php echo ($CTtrial_no+1); ?>'> <?php  echo $CTFGName; ?> </td>
				</tr>	
				<tr>
					<th></th>
					<th> <i> Standard Specification </i> </th>
					<?php
						if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial1; ?> </th>
					<?php		
						}

						if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial2; ?> </th>
					<?php		
						}

						if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial3; ?> </th>
					<?php		
						}

						if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial4; ?> </th>
					<?php		
						}

						if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial5; ?> </th>
					<?php		
						}

						if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial6; ?> </th>
					<?php		
						}

						if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial7; ?> </th>
					<?php		
						}

						if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial8; ?> </th>
					<?php		
						}

						if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial9; ?> </th>
					<?php		
						}

						if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial10; ?> </th>
					<?php		
						}

						if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial11; ?> </th>
					<?php		
						}

						if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial12; ?> </th>
					<?php		
						}

						if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial13; ?> </th>
					<?php		
						}

						if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial14; ?> </th>
					<?php		
						}

						if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial15; ?> </th>
					<?php		
						}

						if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial16; ?> </th>
					<?php		
						}

						if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial17; ?> </th>
					<?php		
						}

						if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial18; ?> </th>
					<?php		
						}

						if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial19; ?> </th>
					<?php		
						}

						if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
					?>
							<th> <?php echo 'Trial '.$CTTrial20; ?> </th>
					<?php		
						}
					?>
				</tr>
			<!-- ###################### Specific Gravity	 -->	
				<tr>
					<td> Specific Gravity </td>
					<td> <?php echo $StandardFromSG.' - '.$StandardToSG; ?> </td>
					<?php
						if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
					?>
							<td> <?php echo $SG1; ?> </td>
					<?php		
						}

						if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
					?>
							<td> <?php echo $SG2; ?> </td>
					<?php		
						}

						if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
					?>
							<td> <?php echo $SG3; ?> </td>
					<?php		
						}

						if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
					?>
							<td> <?php echo $SG4; ?> </td>
					<?php		
						}

						if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
					?>
							<td> <?php echo $SG5; ?> </td>
					<?php		
						}

						if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
					?>
							<td> <?php echo $SG6; ?> </td>
					<?php		
						}

						if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
					?>
							<td> <?php echo $SG7; ?> </td>
					<?php		
						}

						if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
					?>
							<td> <?php echo $SG8; ?> </td>
					<?php		
						}

						if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
					?>
							<td> <?php echo $SG9; ?> </td>
					<?php		
						}

						if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
					?>
							<td> <?php echo $SG10; ?> </td>
					<?php		
						}

						if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
					?>
							<td> <?php echo $SG11; ?> </td>
					<?php		
						}

						if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
					?>
							<td> <?php echo $SG12; ?> </td>
					<?php		
						}

						if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
					?>
							<td> <?php echo $SG13; ?> </td>
					<?php		
						}

						if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
					?>
							<td> <?php echo $SG14; ?> </td>
					<?php		
						}

						if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
					?>
							<td> <?php echo $SG15; ?> </td>
					<?php		
						}

						if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
					?>
							<td> <?php echo $SG16; ?> </td>
					<?php		
						}

						if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
					?>
							<td> <?php echo $SG17; ?> </td>
					<?php		
						}

						if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
					?>
							<td> <?php echo $SG18; ?> </td>
					<?php		
						}

						if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
					?>
							<td> <?php echo $SG19; ?> </td>
					<?php		
						}

						if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
					?>
							<td> <?php echo $SG20; ?> </td>
					<?php		
						}
					?>
				</tr>
			<?php
				if ( $CTFGSoft ){
			?>
			<!-- ###################### H-Shore A	 -->
					<tr>
						<td> H-Shore A </td>
						<td> <?php echo $StandardFromHA.' - '.$StandardToHA; ?> </td>
						<?php
							if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
						?>
								<td> <?php echo $HA1; ?> </td>
						<?php		
							}

							if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
						?>
								<td> <?php echo $HA2; ?> </td>
						<?php		
							}

							if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
						?>
								<td> <?php echo $HA3; ?> </td>
						<?php		
							}

							if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
						?>
								<td> <?php echo $HA4; ?> </td>
						<?php		
							}

							if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
						?>
								<td> <?php echo $HA5; ?> </td>
						<?php		
							}

							if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
						?>
								<td> <?php echo $HA6; ?> </td>
						<?php		
							}

							if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
						?>
								<td> <?php echo $HA7; ?> </td>
						<?php		
							}

							if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
						?>
								<td> <?php echo $HA8; ?> </td>
						<?php		
							}

							if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
						?>
								<td> <?php echo $HA9; ?> </td>
						<?php		
							}

							if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
						?>
								<td> <?php echo $HA10; ?> </td>
						<?php		
							}

							if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
						?>
								<td> <?php echo $HA11; ?> </td>
						<?php		
							}

							if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
						?>
								<td> <?php echo $HA12; ?> </td>
						<?php		
							}

							if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
						?>
								<td> <?php echo $HA13; ?> </td>
						<?php		
							}

							if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
						?>
								<td> <?php echo $HA14; ?> </td>
						<?php		
							}

							if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
						?>
								<td> <?php echo $HA15; ?> </td>
						<?php		
							}

							if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
						?>
								<td> <?php echo $HA16; ?> </td>
						<?php		
							}

							if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
						?>
								<td> <?php echo $HA17; ?> </td>
						<?php		
							}

							if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
						?>
								<td> <?php echo $HA18; ?> </td>
						<?php		
							}

							if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
						?>
								<td> <?php echo $HA19; ?> </td>
						<?php		
							}

							if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
						?>
								<td> <?php echo $HA20; ?> </td>
						<?php		
							}
						?>
					</tr>
			<!-- ###################### Volume Resistivity	 -->
					<tr>
						<td> Volume Resistivity </td>
						<td> <?php echo $StandardVR; ?> </td>
						<?php
							if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
						?>
								<td> <?php echo $VR1; ?> </td>
						<?php		
							}

							if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
						?>
								<td> <?php echo $VR2; ?> </td>
						<?php		
							}

							if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
						?>
								<td> <?php echo $VR3; ?> </td>
						<?php		
							}

							if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
						?>
								<td> <?php echo $VR4; ?> </td>
						<?php		
							}

							if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
						?>
								<td> <?php echo $VR5; ?> </td>
						<?php		
							}

							if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
						?>
								<td> <?php echo $VR6; ?> </td>
						<?php		
							}

							if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
						?>
								<td> <?php echo $VR7; ?> </td>
						<?php		
							}

							if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
						?>
								<td> <?php echo $VR8; ?> </td>
						<?php		
							}

							if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
						?>
								<td> <?php echo $VR9; ?> </td>
						<?php		
							}

							if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
						?>
								<td> <?php echo $VR10; ?> </td>
						<?php		
							}

							if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
						?>
								<td> <?php echo $VR11; ?> </td>
						<?php		
							}

							if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
						?>
								<td> <?php echo $VR12; ?> </td>
						<?php		
							}

							if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
						?>
								<td> <?php echo $VR13; ?> </td>
						<?php		
							}

							if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
						?>
								<td> <?php echo $VR14; ?> </td>
						<?php		
							}

							if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
						?>
								<td> <?php echo $VR15; ?> </td>
						<?php		
							}

							if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
						?>
								<td> <?php echo $VR16; ?> </td>
						<?php		
							}

							if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
						?>
								<td> <?php echo $VR17; ?> </td>
						<?php		
							}

							if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
						?>
								<td> <?php echo $VR18; ?> </td>
						<?php		
							}

							if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
						?>
								<td> <?php echo $VR19; ?> </td>
						<?php		
							}

							if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
						?>
								<td> <?php echo $VR20; ?> </td>
						<?php		
							}
						?>
					</tr>
			<!-- ###################### Unaged Properties	 -->
				<tr>
					<td colspan='<?php echo ($cttrial_count + 3); ?>'> <strong> Unaged Properties </strong> </td>
				</tr>
				<!-- ###################### Unaged Tensile	 -->
					<tr>
						<td style='padding-left:15px;'> > Tensile Strength </td>
						<td> <?php echo $StandardUT; ?> </td>
						<?php
							if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
						?>
								<td> <?php echo $UnageTS1; ?> </td>
						<?php		
							}

							if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
						?>
								<td> <?php echo $UnageTS2; ?> </td>
						<?php		
							}

							if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
						?>
								<td> <?php echo $UnageTS3; ?> </td>
						<?php		
							}

							if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
						?>
								<td> <?php echo $UnageTS4; ?> </td>
						<?php		
							}

							if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
						?>
								<td> <?php echo $UnageTS5; ?> </td>
						<?php		
							}

							if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
						?>
								<td> <?php echo $UnageTS6; ?> </td>
						<?php		
							}

							if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
						?>
								<td> <?php echo $UnageTS7; ?> </td>
						<?php		
							}

							if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
						?>
								<td> <?php echo $UnageTS8; ?> </td>
						<?php		
							}

							if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
						?>
								<td> <?php echo $UnageTS9; ?> </td>
						<?php		
							}

							if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
						?>
								<td> <?php echo $UnageTS10; ?> </td>
						<?php		
							}

							if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
						?>
								<td> <?php echo $UnageTS11; ?> </td>
						<?php		
							}

							if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
						?>
								<td> <?php echo $UnageTS12; ?> </td>
						<?php		
							}

							if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
						?>
								<td> <?php echo $UnageTS13; ?> </td>
						<?php		
							}

							if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
						?>
								<td> <?php echo $UnageTS14; ?> </td>
						<?php		
							}

							if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
						?>
								<td> <?php echo $UnageTS15; ?> </td>
						<?php		
							}

							if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
						?>
								<td> <?php echo $UnageTS16; ?> </td>
						<?php		
							}

							if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
						?>
								<td> <?php echo $UnageTS17; ?> </td>
						<?php		
							}

							if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
						?>
								<td> <?php echo $UnageTS18; ?> </td>
						<?php		
							}

							if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
						?>
								<td> <?php echo $UnageTS19; ?> </td>
						<?php		
							}

							if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
						?>
								<td> <?php echo $UnageTS20; ?> </td>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Unaged Elongation	 -->
					<tr>
						<td style='padding-left:15px;'> > Elongation </td>
						<td> <?php echo $StandardUE; ?> </td>
						<?php
							if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
						?>
								<td> <?php echo $UnageE1; ?> </td>
						<?php		
							}

							if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
						?>
								<td> <?php echo $UnageE2; ?> </td>
						<?php		
							}

							if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
						?>
								<td> <?php echo $UnageE3; ?> </td>
						<?php		
							}

							if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
						?>
								<td> <?php echo $UnageE4; ?> </td>
						<?php		
							}

							if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
						?>
								<td> <?php echo $UnageE5; ?> </td>
						<?php		
							}

							if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
						?>
								<td> <?php echo $UnageE6; ?> </td>
						<?php		
							}

							if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
						?>
								<td> <?php echo $UnageE7; ?> </td>
						<?php		
							}

							if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
						?>
								<td> <?php echo $UnageE8; ?> </td>
						<?php		
							}

							if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
						?>
								<td> <?php echo $UnageE9; ?> </td>
						<?php		
							}

							if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
						?>
								<td> <?php echo $UnageE10; ?> </td>
						<?php		
							}

							if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
						?>
								<td> <?php echo $UnageE11; ?> </td>
						<?php		
							}

							if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
						?>
								<td> <?php echo $UnageE12; ?> </td>
						<?php		
							}

							if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
						?>
								<td> <?php echo $UnageE13; ?> </td>
						<?php		
							}

							if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
						?>
								<td> <?php echo $UnageE14; ?> </td>
						<?php		
							}

							if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
						?>
								<td> <?php echo $UnageE15; ?> </td>
						<?php		
							}

							if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
						?>
								<td> <?php echo $UnageE16; ?> </td>
						<?php		
							}

							if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
						?>
								<td> <?php echo $UnageE17; ?> </td>
						<?php		
							}

							if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
						?>
								<td> <?php echo $UnageE18; ?> </td>
						<?php		
							}

							if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
						?>
								<td> <?php echo $UnageE19; ?> </td>
						<?php		
							}

							if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
						?>
								<td> <?php echo $UnageE20; ?> </td>
						<?php		
							}
						?>
					</tr>
			<!-- ###################### AGE CONDITIONS	 -->
					<?php
						if ( $CToven_aging || $CToil_aging ){
					?>	
							<tr>
								<th colspan="<?php echo ($cttrial_count + 3); ?>"> <i> CONDITION </i> </th>
							</tr>
							<tr>
								<th colspan="<?php echo ($cttrial_count + 3); ?>" align="center">
									<b><i>
										<?php
											if ( $FGCategory == "60" ){
										?>	
												100º ± 1º at 168 hrs
										<?php
											}elseif ( $FGCategory == "75" ){
										?>	
												121º ± 1º at 168 hrs
										<?php
											}elseif ( $FGCategory == "90" || $FGCategory == "105" ){
										?>	
												136º ± 1º at 168 hrs
										<?php
											}
										?>
									</i></b>
								</th>
							</tr>
					<?php
						}
					?>

			<!-- ###################### Oven Aging	 -->
				<?php
					if ( $CToven_aging ){
				?>
						<tr>
							<td colspan='<?php echo ($cttrial_count + 3); ?>'> <strong> Oven Aging </strong> </td>
						</tr>
						<!-- ###################### Oven Aged Tensile	 -->
							<tr>
								<td style='padding-left:15px;'> > %, Tensile Strength </td>
								<td> <?php echo $StandardOVT; ?> </td>
								<?php
									if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS1 != 0 ){
													$PercOvenTS1 = (($OvenTS1 / $UnageTS1) * 100); 
													echo number_format((float)($PercOvenTS1), 2, '.', '');
												}else{
													echo $OvenTS1;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS2 != 0 ){
													$PercOvenTS2 = (($OvenTS2 / $UnageTS2) * 100); 
													echo number_format((float)($PercOvenTS2), 2, '.', '');
												}else{
													echo $OvenTS2;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS3 != 0 ){
													$PercOvenTS3 = (($OvenTS3 / $UnageTS3) * 100); 
													echo number_format((float)($PercOvenTS3), 2, '.', '');
												}else{
													echo $OvenTS3;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS4 != 0 ){
													$PercOvenTS4 = (($OvenTS4 / $UnageTS4) * 100); 
													echo number_format((float)($PercOvenTS4), 2, '.', '');
												}else{
													echo $OvenTS4;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS5 != 0 ){
													$PercOvenTS5 = (($OvenTS5 / $UnageTS5) * 100); 
													echo number_format((float)($PercOvenTS5), 2, '.', '');
												}else{
													echo $OvenTS5;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS6 != 0 ){
													$PercOvenTS6 = (($OvenTS6 / $UnageTS6) * 100); 
													echo number_format((float)($PercOvenTS6), 2, '.', '');
												}else{
													echo $OvenTS6;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS7 != 0 ){
													$PercOvenTS7 = (($OvenTS7 / $UnageTS7) * 100); 
													echo number_format((float)($PercOvenTS7), 2, '.', '');
												}else{
													echo $OvenTS7;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS8 != 0 ){
													$PercOvenTS8 = (($OvenTS8 / $UnageTS8) * 100); 
													echo number_format((float)($PercOvenTS8), 2, '.', '');
												}else{
													echo $OvenTS8;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS9 != 0 ){
													$PercOvenTS9 = (($OvenTS9 / $UnageTS9) * 100); 
													echo number_format((float)($PercOvenTS9), 2, '.', '');
												}else{
													echo $OvenTS9;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS10 != 0 ){
													$PercOvenTS10 = (($OvenTS10 / $UnageTS10) * 100); 
													echo number_format((float)($PercOvenTS10), 2, '.', '');
												}else{
													echo $OvenTS10;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS11 != 0 ){
													$PercOvenTS11 = (($OvenTS11 / $UnageTS11) * 100); 
													echo number_format((float)($PercOvenTS11), 2, '.', '');
												}else{
													echo $OvenTS11;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS12 != 0 ){
													$PercOvenTS12 = (($OvenTS12 / $UnageTS12) * 100); 
													echo number_format((float)($PercOvenTS12), 2, '.', '');
												}else{
													echo $OvenTS12;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS13 != 0 ){
													$PercOvenTS13 = (($OvenTS13 / $UnageTS13) * 100); 
													echo number_format((float)($PercOvenTS13), 2, '.', '');
												}else{
													echo $OvenTS13;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS14 != 0 ){
													$PercOvenTS14 = (($OvenTS14 / $UnageTS14) * 100); 
													echo number_format((float)($PercOvenTS14), 2, '.', '');
												}else{
													echo $OvenTS14;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS15 != 0 ){
													$PercOvenTS15 = (($OvenTS15 / $UnageTS15) * 100); 
													echo number_format((float)($PercOvenTS15), 2, '.', '');
												}else{
													echo $OvenTS15;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS16 != 0 ){
													$PercOvenTS16 = (($OvenTS16 / $UnageTS16) * 100); 
													echo number_format((float)($PercOvenTS16), 2, '.', '');
												}else{
													echo $OvenTS16;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS17 != 0 ){
													$PercOvenTS17 = (($OvenTS17 / $UnageTS17) * 100); 
													echo number_format((float)($PercOvenTS17), 2, '.', '');
												}else{
													echo $OvenTS17;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS18 != 0 ){
													$PercOvenTS18 = (($OvenTS18 / $UnageTS18) * 100); 
													echo number_format((float)($PercOvenTS18), 2, '.', '');
												}else{
													echo $OvenTS18;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS19 != 0 ){
													$PercOvenTS19 = (($OvenTS19 / $UnageTS19) * 100); 
													echo number_format((float)($PercOvenTS19), 2, '.', '');
												}else{
													echo $OvenTS19;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS20 != 0 ){
													$PercOvenTS20 = (($OvenTS20 / $UnageTS20) * 100); 
													echo number_format((float)($PercOvenTS20), 2, '.', '');
												}else{
													echo $OvenTS20;
												}
											?> 
										</td>
								<?php		
									}
								?>
							</tr>
						<!-- ###################### Oven Aged Elongation	 -->
							<tr>
								<td style='padding-left:15px;'> > %, Elongation </td>
								<td> <?php echo $StandardOVE; ?> </td>
								<?php
									if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE1 != 0 ){
													$PercOvenE1 = (($OvenE1 / $UnageE1) * 100); 
													echo number_format((float)($PercOvenE1), 2, '.', '');
												}else{
													echo $OvenE1;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE2 != 0 ){
													$PercOvenE2 = (($OvenE2 / $UnageE2) * 100); 
													echo number_format((float)($PercOvenE2), 2, '.', '');
												}else{
													echo $OvenE2;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE3 != 0 ){
													$PercOvenE3 = (($OvenE3 / $UnageE3) * 100); 
													echo number_format((float)($PercOvenE3), 2, '.', '');
												}else{
													echo $OvenE3;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE4 != 0 ){
													$PercOvenE4 = (($OvenE4 / $UnageE4) * 100); 
													echo number_format((float)($PercOvenE4), 2, '.', '');
												}else{
													echo $OvenE4;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE5 != 0 ){
													$PercOvenE5 = (($OvenE5 / $UnageE5) * 100); 
													echo number_format((float)($PercOvenE5), 2, '.', '');
												}else{
													echo $OvenE5;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE6 != 0 ){
													$PercOvenE6 = (($OvenE6 / $UnageE6) * 100); 
													echo number_format((float)($PercOvenE6), 2, '.', '');
												}else{
													echo $OvenE6;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE7 != 0 ){
													$PercOvenE7 = (($OvenE7 / $UnageE7) * 100); 
													echo number_format((float)($PercOvenE7), 2, '.', '');
												}else{
													echo $OvenE7;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE8 != 0 ){
													$PercOvenE8 = (($OvenE8 / $UnageE8) * 100); 
													echo number_format((float)($PercOvenE8), 2, '.', '');
												}else{
													echo $OvenE8;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE9 != 0 ){
													$PercOvenE9 = (($OvenE9 / $UnageE9) * 100); 
													echo number_format((float)($PercOvenE9), 2, '.', '');
												}else{
													echo $OvenE9;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE10 != 0 ){
													$PercOvenE10 = (($OvenE10 / $UnageE10) * 100); 
													echo number_format((float)($PercOvenE10), 2, '.', '');
												}else{
													echo $OvenE10;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE1 != 0 ){
													$PercOvenE1 = (($OvenE1 / $UnageE1) * 100); 
													echo number_format((float)($PercOvenE1), 2, '.', '');
												}else{
													echo $OvenE1;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE12 != 0 ){
													$PercOvenE12 = (($OvenE12 / $UnageE12) * 100); 
													echo number_format((float)($PercOvenE12), 2, '.', '');
												}else{
													echo $OvenE12;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE13 != 0 ){
													$PercOvenE13 = (($OvenE13 / $UnageE13) * 100); 
													echo number_format((float)($PercOvenE13), 2, '.', '');
												}else{
													echo $OvenE13;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE14 != 0 ){
													$PercOvenE14 = (($OvenE14 / $UnageE14) * 100); 
													echo number_format((float)($PercOvenE14), 2, '.', '');
												}else{
													echo $OvenE14;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE15 != 0 ){
													$PercOvenE15 = (($OvenE15 / $UnageE15) * 100); 
													echo number_format((float)($PercOvenE15), 2, '.', '');
												}else{
													echo $OvenE15;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE16 != 0 ){
													$PercOvenE16 = (($OvenE16 / $UnageE16) * 100); 
													echo number_format((float)($PercOvenE16), 2, '.', '');
												}else{
													echo $OvenE16;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE17 != 0 ){
													$PercOvenE17 = (($OvenE17 / $UnageE17) * 100); 
													echo number_format((float)($PercOvenE17), 2, '.', '');
												}else{
													echo $OvenE17;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE18 != 0 ){
													$PercOvenE18 = (($OvenE18 / $UnageE18) * 100); 
													echo number_format((float)($PercOvenE18), 2, '.', '');
												}else{
													echo $OvenE18;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE19 != 0 ){
													$PercOvenE19 = (($OvenE19 / $UnageE19) * 100); 
													echo number_format((float)($PercOvenE19), 2, '.', '');
												}else{
													echo $OvenE19;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE20 != 0 ){
													$PercOvenE20 = (($OvenE20 / $UnageE20) * 100); 
													echo number_format((float)($PercOvenE20), 2, '.', '');
												}else{
													echo $OvenE20;
												}
											?> 
										</td>
								<?php		
									}
								?>
							</tr>
				<?php
					}
				?>
			<!-- ###################### Oil Aging	 -->
				<?php
					if ( $CToil_aging ){
				?>
						<tr>
							<td colspan='<?php echo ($cttrial_count+3); ?>'> <strong> Oil Aging </strong> </td>
						</tr>
						<!-- ###################### Oil Aged Tensile	 -->
							<tr>
								<td style='padding-left:15px;'> > %, Tensile Strength </td>
								<td> <?php echo $StandardOIT; ?> </td>
								<?php
									if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS1 != 0 ){
													$PercOilTS1 = (($OilTS1 / $UnageTS1) * 100); 
													echo number_format((float)($PercOilTS1), 2, '.', '');
												}else{
													echo $OilTS1;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS2 != 0 ){
													$PercOilTS2 = (($OilTS2 / $UnageTS2) * 100); 
													echo number_format((float)($PercOilTS2), 2, '.', '');
												}else{
													echo $OilTS2;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS3 != 0 ){
													$PercOilTS3 = (($OilTS3 / $UnageTS3) * 100); 
													echo number_format((float)($PercOilTS3), 2, '.', '');
												}else{
													echo $OilTS3;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS4 != 0 ){
													$PercOilTS4 = (($OilTS4 / $UnageTS4) * 100); 
													echo number_format((float)($PercOilTS4), 2, '.', '');
												}else{
													echo $OilTS4;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS5 != 0 ){
													$PercOilTS5 = (($OilTS5 / $UnageTS5) * 100); 
													echo number_format((float)($PercOilTS5), 2, '.', '');
												}else{
													echo $OilTS5;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS6 != 0 ){
													$PercOilTS6 = (($OilTS6 / $UnageTS6) * 100); 
													echo number_format((float)($PercOilTS6), 2, '.', '');
												}else{
													echo $OilTS6;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS7 != 0 ){
													$PercOilTS7 = (($OilTS7 / $UnageTS7) * 100); 
													echo number_format((float)($PercOilTS7), 2, '.', '');
												}else{
													echo $OilTS7;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS8 != 0 ){
													$PercOilTS8 = (($OilTS8 / $UnageTS8) * 100); 
													echo number_format((float)($PercOilTS8), 2, '.', '');
												}else{
													echo $OilTS8;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS9 != 0 ){
													$PercOilTS9 = (($OilTS9 / $UnageTS9) * 100); 
													echo number_format((float)($PercOilTS9), 2, '.', '');
												}else{
													echo $OilTS9;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS10 != 0 ){
													$PercOilTS10 = (($OilTS10 / $UnageTS10) * 100); 
													echo number_format((float)($PercOilTS10), 2, '.', '');
												}else{
													echo $OilTS10;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS11 != 0 ){
													$PercOilTS11 = (($OilTS11 / $UnageTS11) * 100); 
													echo number_format((float)($PercOilTS11), 2, '.', '');
												}else{
													echo $OilTS11;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS12 != 0 ){
													$PercOilTS12 = (($OilTS12 / $UnageTS12) * 100); 
													echo number_format((float)($PercOilTS12), 2, '.', '');
												}else{
													echo $OilTS12;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS13 != 0 ){
													$PercOilTS13 = (($OilTS13 / $UnageTS13) * 100); 
													echo number_format((float)($PercOilTS13), 2, '.', '');
												}else{
													echo $OilTS13;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS14 != 0 ){
													$PercOilTS14 = (($OilTS14 / $UnageTS14) * 100); 
													echo number_format((float)($PercOilTS14), 2, '.', '');
												}else{
													echo $OilTS14;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS15 != 0 ){
													$PercOilTS15 = (($OilTS15 / $UnageTS15) * 100); 
													echo number_format((float)($PercOilTS15), 2, '.', '');
												}else{
													echo $OilTS15;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS16 != 0 ){
													$PercOilTS16 = (($OilTS16 / $UnageTS16) * 100); 
													echo number_format((float)($PercOilTS16), 2, '.', '');
												}else{
													echo $OilTS16;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS17 != 0 ){
													$PercOilTS17 = (($OilTS17 / $UnageTS17) * 100); 
													echo number_format((float)($PercOilTS17), 2, '.', '');
												}else{
													echo $OilTS17;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS18 != 0 ){
													$PercOilTS18 = (($OilTS18 / $UnageTS18) * 100); 
													echo number_format((float)($PercOilTS18), 2, '.', '');
												}else{
													echo $OilTS18;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS19 != 0 ){
													$PercOilTS19 = (($OilTS19 / $UnageTS19) * 100); 
													echo number_format((float)($PercOilTS19), 2, '.', '');
												}else{
													echo $OilTS19;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageTS20 != 0 ){
													$PercOilTS20 = (($OilTS20 / $UnageTS20) * 100); 
													echo number_format((float)($PercOilTS20), 2, '.', '');
												}else{
													echo $OilTS20;
												}
											?> 
										</td>
								<?php		
									}
								?>
							</tr>
						<!-- ###################### Oil Aged Elongation	 -->
							<tr>
								<td style='padding-left:15px;'> > %, Elongation </td>
								<td> <?php echo $StandardOIE; ?> </td>
								<?php
									if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE1 != 0 ){
													$PercOilE1 = (($OilE1 / $UnageE1) * 100); 
													echo number_format((float)($PercOilE1), 2, '.', '');
												}else{
													echo $OilE1;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE2 != 0 ){
													$PercOilE2 = (($OilE2 / $UnageE2) * 100); 
													echo number_format((float)($PercOilE2), 2, '.', '');
												}else{
													echo $OilE2;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE3 != 0 ){
													$PercOilE3 = (($OilE3 / $UnageE3) * 100); 
													echo number_format((float)($PercOilE3), 2, '.', '');
												}else{
													echo $OilE3;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE4 != 0 ){
													$PercOilE4 = (($OilE4 / $UnageE4) * 100); 
													echo number_format((float)($PercOilE4), 2, '.', '');
												}else{
													echo $OilE4;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE5 != 0 ){
													$PercOilE5 = (($OilE5 / $UnageE5) * 100); 
													echo number_format((float)($PercOilE5), 2, '.', '');
												}else{
													echo $OilE5;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE6 != 0 ){
													$PercOilE6 = (($OilE6 / $UnageE6) * 100); 
													echo number_format((float)($PercOilE6), 2, '.', '');
												}else{
													echo $OilE6;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE7 != 0 ){
													$PercOilE7 = (($OilE7 / $UnageE7) * 100); 
													echo number_format((float)($PercOilE7), 2, '.', '');
												}else{
													echo $OilE7;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE8 != 0 ){
													$PercOilE8 = (($OilE8 / $UnageE8) * 100); 
													echo number_format((float)($PercOilE8), 2, '.', '');
												}else{
													echo $OilE8;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE9 != 0 ){
													$PercOilE9 = (($OilE9 / $UnageE9) * 100); 
													echo number_format((float)($PercOilE9), 2, '.', '');
												}else{
													echo $OilE9;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE10 != 0 ){
													$PercOilE10 = (($OilE10 / $UnageE10) * 100); 
													echo number_format((float)($PercOilE10), 2, '.', '');
												}else{
													echo $OilE10;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE11 != 0 ){
													$PercOilE11 = (($OilE11 / $UnageE11) * 100); 
													echo number_format((float)($PercOilE11), 2, '.', '');
												}else{
													echo $OilE11;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE12 != 0 ){
													$PercOilE12 = (($OilE12 / $UnageE12) * 100); 
													echo number_format((float)($PercOilE12), 2, '.', '');
												}else{
													echo $OilE12;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE13 != 0 ){
													$PercOilE13 = (($OilE13 / $UnageE13) * 100); 
													echo number_format((float)($PercOilE13), 2, '.', '');
												}else{
													echo $OilE13;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE14 != 0 ){
													$PercOilE14 = (($OilE14 / $UnageE14) * 100); 
													echo number_format((float)($PercOilE14), 2, '.', '');
												}else{
													echo $OilE14;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE15 != 0 ){
													$PercOilE15 = (($OilE15 / $UnageE15) * 100); 
													echo number_format((float)($PercOilE15), 2, '.', '');
												}else{
													echo $OilE15;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE16 != 0 ){
													$PercOilE16 = (($OilE16 / $UnageE16) * 100); 
													echo number_format((float)($PercOilE16), 2, '.', '');
												}else{
													echo $OilE16;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE17 != 0 ){
													$PercOilE17 = (($OilE17 / $UnageE17) * 100); 
													echo number_format((float)($PercOilE17), 2, '.', '');
												}else{
													echo $OilE17;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE18 != 0 ){
													$PercOilE18 = (($OilE18 / $UnageE18) * 100); 
													echo number_format((float)($PercOilE18), 2, '.', '');
												}else{
													echo $OilE18;
												}
											?> 
										</td>
								<?php		
									}

									if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE19 != 0 ){
													$PercOilE19 = (($OilE19 / $UnageE19) * 100); 
													echo number_format((float)($PercOilE19), 2, '.', '');
												}else{
													echo $OilE19;
												}
											?> 
										</td>
										<td> <?php echo $OilE19; ?> </td>
								<?php		
									}

									if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
								?>
										<td> 
											<?php 
												if ( $UnageE20 != 0 ){
													$PercOilE20 = (($OilE20 / $UnageE20) * 100); 
													echo number_format((float)($PercOilE20), 2, '.', '');
												}else{
													echo $OilE20;
												}
											?> 
										</td>
								<?php		
									}
								?>
							</tr>
				<?php
					}
				?>			
			<?php		
				}else{
			?>
			<!-- ###################### H-Shore D	 -->
					<tr>
						<td> H-Shore D </td>
						<td> <?php echo $StandardFromHD.' - '.$StandardToHD; ?> </td>
						<?php
							if ( $CTTrial1 <= $CTtrial_no &&$CTTrial1!= '-' ){
						?>
								<td> <?php echo $HD1; ?> </td>
						<?php		
							}

							if ( $CTTrial2 <= $CTtrial_no && $CTTrial2 != '-' ){
						?>
								<td> <?php echo $HD2; ?> </td>
						<?php		
							}

							if ( $CTTrial3 <= $CTtrial_no && $CTTrial3 != '-' ){
						?>
								<td> <?php echo $HD3; ?> </td>
						<?php		
							}

							if ( $CTTrial4 <= $CTtrial_no && $CTTrial4 != '-' ){
						?>
								<td> <?php echo $HD4; ?> </td>
						<?php		
							}

							if ( $CTTrial5 <= $CTtrial_no && $CTTrial5 != '-' ){
						?>
								<td> <?php echo $HD5; ?> </td>
						<?php		
							}

							if ( $CTTrial6 <= $CTtrial_no && $CTTrial6 != '-' ){
						?>
								<td> <?php echo $HD6; ?> </td>
						<?php		
							}

							if ( $CTTrial7 <= $CTtrial_no && $CTTrial7 != '-' ){
						?>
								<td> <?php echo $HD7; ?> </td>
						<?php		
							}

							if ( $CTTrial8 <= $CTtrial_no && $CTTrial8 != '-' ){
						?>
								<td> <?php echo $HD8; ?> </td>
						<?php		
							}

							if ( $CTTrial9 <= $CTtrial_no && $CTTrial9 != '-' ){
						?>
								<td> <?php echo $HD9; ?> </td>
						<?php		
							}

							if ( $CTTrial10 <= $CTtrial_no && $CTTrial10 != '-' ){
						?>
								<td> <?php echo $HD10; ?> </td>
						<?php		
							}

							if ( $CTTrial11 <= $CTtrial_no && $CTTrial11 != '-' ){
						?>
								<td> <?php echo $HD11; ?> </td>
						<?php		
							}

							if ( $CTTrial12 <= $CTtrial_no && $CTTrial12 != '-' ){
						?>
								<td> <?php echo $HD12; ?> </td>
						<?php		
							}

							if ( $CTTrial13 <= $CTtrial_no && $CTTrial13 != '-' ){
						?>
								<td> <?php echo $HD13; ?> </td>
						<?php		
							}

							if ( $CTTrial14 <= $CTtrial_no && $CTTrial14 != '-' ){
						?>
								<td> <?php echo $HD14; ?> </td>
						<?php		
							}

							if ( $CTTrial15 <= $CTtrial_no && $CTTrial15 != '-' ){
						?>
								<td> <?php echo $HD15; ?> </td>
						<?php		
							}

							if ( $CTTrial16 <= $CTtrial_no && $CTTrial16 != '-' ){
						?>
								<td> <?php echo $HD16; ?> </td>
						<?php		
							}

							if ( $CTTrial17 <= $CTtrial_no && $CTTrial17 != '-' ){
						?>
								<td> <?php echo $HD17; ?> </td>
						<?php		
							}

							if ( $CTTrial18 <= $CTtrial_no && $CTTrial18 != '-' ){
						?>
								<td> <?php echo $HD18; ?> </td>
						<?php		
							}

							if ( $CTTrial19 <= $CTtrial_no && $CTTrial19 != '-' ){
						?>
								<td> <?php echo $HD19; ?> </td>
						<?php		
							}

							if ( $CTTrial20 <= $CTtrial_no && $CTTrial20 != '-' ){
						?>
								<td> <?php echo $HD20; ?> </td>
						<?php		
							}
						?>
					</tr>
			<?php		
				}
			?>
				</table>
		
		<?php

				}
				$db->next_result();
				$resultN->close();
			}

	require("database_close.php");
?>