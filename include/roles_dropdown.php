<?php

$dept1 = intval($_GET['dept1']);
$dept2 = intval($_GET['dept2']);
$Compounds = intval($_GET['Compounds']);
$Pipes = intval($_GET['Pipes']);
$Corporate = intval($_GET['Corporate']);
$PPR = intval($_GET['PPR']);

require("database_connect.php");

if(!empty($errno))
{
	$error = mysqli_connect_error();
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>department_dropdown.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}
else
{

	$qry = mysqli_prepare($db, "CALL sp_Role_Dropdown(?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iiiiii', $dept1, $dept2, $Compounds, $Pipes, $Corporate, $PPR);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query

	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>department_dropdown.php'.'</td><td>'.$processError.' near line 33.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{ 
		while( $row = mysqli_fetch_assoc( $result ) ){
			$roleID = array();
			$roleID[] = $row['id'];
			$roleName = $row['name'];

			foreach($roleID as $key => $itemValue)
				echo"<input type='checkbox' name='chkRole[]' value='".$itemValue."'><label> ".$roleName."</label><br>";
		}

		$db->next_result();
		$result->close(); 
	}


}
require("database_close.php");
?> 