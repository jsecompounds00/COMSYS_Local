<?php

$sltCustomer = intval($_GET['sltCustomer']);
// $CCRID = intval($_GET['CCRID']);
$fg_id = intval($_GET['fg_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown_ccr.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qry = mysqli_prepare($db, "CALL sp_CCR_FG_Dropdown( ? )");
		mysqli_stmt_bind_param($qry, 'i', $sltCustomer);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown_ccr.php'.'</td><td>'.$processError.' near line 23.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
				echo "<option></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				
				$FgId = $row['FGID'];
				$Fg = $row['FGItem'];

				
					if ($FgId == $fg_id){
						echo "<option value=".$FgId." selected>".$Fg."</option>";
					}else {
						echo "<option value=".$FgId.">".$Fg."</option>";
					}
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>