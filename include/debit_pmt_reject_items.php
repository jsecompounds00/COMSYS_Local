<?php
	session_start();
	ob_start();

	$debitId = intval($_GET["hidDebitId"]);

	require("database_connect.php");
	require("/init_unset_values/debit_invoice_init_value.php");

	if ( $debitId ){

		$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_Query( ? )");
		mysqli_stmt_bind_param($qry, 'i', $debitId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);

		if ( !empty($processError) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_invoice.php'.'</td><td>'.$processError.' near line 12.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			$db_jacket_item_id = array();
			$db_invoice_number = array();
			$db_amount = array();
			$db_discount = array();
			while($row = mysqli_fetch_assoc($result)){
				$db_jacket_id = $row['jacket_id'];
				$db_process_type = $row['process_type'];
				$db_customer_id = $row['customer_id'];
				$db_transaction_type = $row['transaction_type'];
				$db_invoice_date = $row['invoice_date'];
				$db_check_returned_date = $row['check_returned_date'];
				$db_check_date = $row['check_date'];
				$db_created_at = $row['created_at'];
				$db_created_id = $row['created_id'];
				$db_name = $row['name'];

				$db_check_number = $row['check_number'];
				$db_bank_code_id = $row['bank_code_id'];
				$db_bank_code = $row['bank_code'];

				$db_jacket_item_id[] = $row['jacket_item_id'];
				$db_invoice_number[] = $row['invoice_number'];
				$db_amount[] = $row['amount'];
				$db_discount[] = $row['discount'];
			}
			$db->next_result();
			$result->close();
			$count = count( $db_jacket_item_id );
		}
	}
?>
<tr>
	<th colspan='2'>Check No.</th>
	<th>Invoice No.</th>
	<th>Amount</th>
	<th>Discount</th>
</tr>
<tr valign='top'>
	<td id='tdBankCode'>
		<?php
			if ( $debitId ){
		?>	
				<input type="text" value="<?php echo $db_bank_code;?>" readOnly>
				<input type="hidden" name="sltBankCode" id="sltBankCode" value="<?php echo $db_bank_code_id;?>" readOnly>
		<?php
			}else{
		?>
				<select name='sltBankCode' id='sltBankCode'
					onchange="showInvoices('<?php echo $debitId;?>'), showAmounts('<?php echo $debitId;?>'), showDiscounts('<?php echo $debitId;?>')">
					<?php
						$qryBankCode = "CALL sp_Bank_Code_Dropdown()";
						$resultBankCode = mysqli_query( $db, $qryBankCode );
						$processErrorBankCode = mysqli_error( $db );

						if ( !empty($processErrorBankCode) ){
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_invoice.php'.'</td><td>'.$processErrorBankCode.' near line 12.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}else{
								echo "<option></option>";

								$y = 0;

							while($row = mysqli_fetch_assoc($resultBankCode)){
								$bank_code_id = $row['id'];
								$bank_code = $row['bank_code'];

								if ( $initCJDIkCode == $bank_code_id ){
									echo "<option id='optionBankCode".$y."' value='".$bank_code_id."' selected>".$bank_code."</option>";
								}else{
									echo "<option id='optionBankCode".$y."' value='".$bank_code_id."'>".$bank_code."</option>";
								}
								
								$y++;

							}
							$db->next_result();
							$resultBankCode->close();
						}
					?>
				</select>
		<?php
			}
		?>
	</td>
	<td id='tdChecknumber'>	
		<input type='text' name='txtChecknumber' id='txtChecknumber' value="<?php echo ( $debitId ? $db_check_number : $initCJDIcknumber );?>"
			<?php echo ( $debitId ? "readOnly" : "" );?>
			onkeyup="showInvoices('<?php echo $debitId;?>'), showAmounts('<?php echo $debitId;?>'), showDiscounts('<?php echo $debitId;?>')" 
			onblur="showInvoices('<?php echo $debitId;?>'), showAmounts('<?php echo $debitId;?>'), showDiscounts('<?php echo $debitId;?>')" 
			onchange="showInvoices('<?php echo $debitId;?>'), showAmounts('<?php echo $debitId;?>'), showDiscounts('<?php echo $debitId;?>')">
	</td>
	<td id='tdInvoiceNumber'>	
		<input type='text' name='txtInvoiceNumber[]' id='txtInvoiceNumber'>
		<input type='hidden' name='hidDebitItemID[]' id='hidDebitItemID'>
	</td>
	<td id='tdAmount'>	
		<input type='text' name='txtAmount[]' id='txtAmount'>
	</td>
	<td id='tdDiscount'>	
		<input type='text' name='txtDiscount[]' id='txtDiscount'>
	</td>
</tr>
<?php
	require("database_close.php");

	require("/init_unset_values/debit_invoice_unset_value.php");
?>