<?php
require '/database_connect.php';
$code = intval($_GET['code']);

$qry = "SELECT code FROM equipment WHERE id=$code";
$result = mysqli_query($db, $qry);
$processError = mysqli_error($db);

if ( !empty($processError) ){
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>code.php'.'</td><td>'.$processError.' near line 10.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}else {
	while($row = mysqli_fetch_assoc($result)){
		$code = $row['code'];

		echo "<input type='text' name='txtCode' value='".$code."' readOnly>";
	}
}
	require("database_close.php");

?>