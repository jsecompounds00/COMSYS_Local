<?php

	require("database_connect.php");

	$qryCC = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Color_Change_Query( ?, ?, ? )");
	mysqli_stmt_bind_param($qryCC, 'isi', $JRDNoReplicateID, $product_for_evaluation, $FGID);
	$qryCC->execute();
	$resultCC = mysqli_stmt_get_result($qryCC);
	$processErrorCC = mysqli_error($db);

	if ( !empty($processErrorCC) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_color_change.php'.'</td><td>'.$processErrorCC.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultCC)){
			$cc_color_conformance = $row['color_conformance'];
			$cc_heat_stability = $row['heat_stability'];
			$CCcreated_at = $row['created_at'];
			$CCcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultCC->close();
	}

?>
	
	<table class='results_child_tables_form'>
		<col width='250'></col>
		<tr></tr>
		<tr>
			<td>Color Conformance:</td>
			<td>
				<input type='radio' name='radCCTColor' id='ECCTColor' value='Excellent' <?php echo ( $color_change_id ? ( $cc_color_conformance == 'Excellent' ? "checked" : "" ) : ( $initNRCCTColor == 'Excellent' ? "checked" : "" ) ); ?>> 
					<label for='ECCTColor'> Excellent </label>

				<input type='radio' name='radCCTColor' id='GCCTColor' value='Good' <?php echo ( $color_change_id ? ( $cc_color_conformance == 'Good' ? "checked" : "" ) : ( $initNRCCTColor == 'Good' ? "checked" : "" ) ); ?>> 
					<label for='GCCTColor'> Good </label>

				<input type='radio' name='radCCTColor' id='PCCTColor' value='Poor' <?php echo ( $color_change_id ? ( $cc_color_conformance == 'Poor' ? "checked" : "" ) : ( $initNRCCTColor == 'Poor' ? "checked" : "" ) ); ?>> 
					<label for='PCCTColor'> Poor </label>
			</td>
		</tr>
		<tr>
			<td>Heat Stability:</td>
			<td>
				<input type='radio' name='radCCTHeat' id='ECCTHeat' value='Excellent' <?php echo ( $color_change_id ? ( $cc_heat_stability == 'Excellent' ? "checked" : "" ) : ( $initNdCCTHeat == 'Excellent' ? "checked" : "" ) ); ?>> 
					<label for='ECCTHeat'> Excellent </label>

				<input type='radio' name='radCCTHeat' id='GCCTHeat' value='Good' <?php echo ( $color_change_id ? ( $cc_heat_stability == 'Good' ? "checked" : "" ) : ( $initNdCCTHeat == 'Good' ? "checked" : "" ) ); ?>> 
					<label for='GCCTHeat'> Good </label>

				<input type='radio' name='radCCTHeat' id='PCCTHeat' value='Poor' <?php echo ( $color_change_id ? ( $cc_heat_stability == 'Poor' ? "checked" : "" ) : ( $initNdCCTHeat == 'Poor' ? "checked" : "" ) ); ?>> 
					<label for='PCCTHeat'> Poor </label>
			</td>
		</tr>
		</tr>
		<input type='hidden' name='hidCCTCreatedAt' value='<?php echo ( $color_change_id ? $CCcreated_at : date('Y-m-d H:i:s') );?>'>
		<input type='hidden' name='hidCCTCreatedId' value='<?php echo ( $color_change_id ? $CCcreated_id : 0 );?>'>
	</table>
<?php	 require("database_close.php"); ?>
