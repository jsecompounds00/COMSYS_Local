<?php

	require("database_connect.php");

	$qrySH = mysqli_prepare($db, "CALL sp_Batch_Ticket_Static_Heat_Query( ? )");
	mysqli_stmt_bind_param($qrySH, 'i', $TrialID);
	$qrySH->execute();
	$resultSH = mysqli_stmt_get_result($qrySH);
	$processErrorSH = mysqli_error($db);

	if ( !empty($processErrorSH) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>static_heating.php'.'</td><td>'.$processErrorSH.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultSH)){
			$sh_color_conformance = $row['color_conformance'];
			$sh_heat_stability = $row['heat_stability'];
			$SHcreated_at = $row['created_at'];
			$SHcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultSH->close();
	}

?>
	
	<table class="results_child_tables_form">
		<col width="250"></col>
		<tr></tr>
		<input type='hidden' name='hidStaticHeatingID' value='<?php echo $static_heat_id; ?>'>
		<tr>
			<td>Color Conformance:</td>
			<td>
				<input type='radio' name='radSHColor' id='ESHColor' value='Excellent' <?php echo ( $static_heat_id ? ( $sh_color_conformance == "Excellent" ? "checked" : "" ) : ( $initSHColor_BT == "Excellent" ? "checked" : "" ) );?>>
					<label for='ESHColor'> Excellent </label>

				<input type='radio' name='radSHColor' id='GSHColor' value='Good' <?php echo ( $static_heat_id ? ( $sh_color_conformance == "Good" ? "checked" : "" ) : ( $initSHColor_BT == "Good" ? "checked" : "" ) );?>>
					<label for='GSHColor'> Good </label>

				<input type='radio' name='radSHColor' id='PSHColor' value='Poor' <?php echo ( $static_heat_id ? ( $sh_color_conformance == "Poor" ? "checked" : "" ) : ( $initSHColor_BT == "Poor" ? "checked" : "" ) );?>>
					<label for='PSHColor'> Poor </label>
			</td>
		</tr>
		<tr>
			<td>Heat Stability:</td>
			<td>
				<input type='radio' name='radSHHeat' id='ESHHeat' value='Excellent' <?php echo ( $static_heat_id ? ( $sh_heat_stability == "Excellent" ? "checked" : "" ) : ( $initSHHeat_BT == "Excellent" ? "checked" : "" ) );?>>
					<label for='ESHHeat'> Excellent </label>

				<input type='radio' name='radSHHeat' id='GSHHeat' value='Good' <?php echo ( $static_heat_id ? ( $sh_heat_stability == "Good" ? "checked" : "" ) : ( $initSHHeat_BT == "Good" ? "checked" : "" ) );?>>
					<label for='GSHHeat'> Good </label>

				<input type='radio' name='radSHHeat' id='PSHHeat' value='Poor' <?php echo ( $static_heat_id ? ( $sh_heat_stability == "Poor" ? "checked" : "" ) : ( $initSHHeat_BT == "Poor" ? "checked" : "" ) );?>>
					<label for='PSHHeat'> Poor </label>
			</td>
		</tr>
		</tr>
	</table>

<?php		
	
	if ( $static_heat_id ){
		echo "<input type='hidden' name='hidSHCreatedAt' value='".$SHcreated_at."'>";
		echo "<input type='hidden' name='hidSHCreatedId' value='".$SHcreated_id."'>";	
	}else{
		echo "<input type='hidden' name='hidSHCreatedAt' value='".date('Y-m-d H:i:s')."'>";
		echo "<input type='hidden' name='hidSHCreatedId' value=''>";	
	}

	require("database_close.php");
?>