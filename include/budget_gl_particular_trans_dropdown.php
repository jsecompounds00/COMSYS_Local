<?php

	$sltGLCode = intval($_GET['sltGLCode']);
	$sltDepartment = intval($_GET['sltDepartment']);
	$txtTransactionDate = $_GET['txtTransactionDate'];


	require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>budget_gl_particular_trans_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare( $db, "CALL sp_Budget_GL_Particulars_Trans_Dropdown(?,?,?)" );
		mysqli_stmt_bind_param( $qry, 'iis', $sltDepartment, $sltGLCode, $txtTransactionDate );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>budget_gl_particular_trans_dropdown.php'.'</td><td>'.$processError.' near line 21.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result))
			{
				$DDItemID = $row['DDItemID'];
				$DDItemName = $row['DDItemName'];
				
				echo "<option value='".$DDItemID."'>".$DDItemName."</option>";
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?> 