<?php

	$sltMiscType = intval($_GET['sltMiscType']);
	
	require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>budget_miscellaneous_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare( $db, "CALL sp_Budget_Miscellaneous_Dropdown(?,1)" );
		mysqli_stmt_bind_param( $qry, 'i', $sltMiscType );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>budget_miscellaneous_dropdown.php'.'</td><td>'.$processError.' near line 21.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result))
			{
				$DDItemID = $row['DDItemID'];
				$DDItemName = $row['DDItemName'];
				
				echo "<option value='".$DDItemID."'>".$DDItemName."</option>";
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?> 