<?php

$rnd_activity = strval($_GET['rnd_activity']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>activity_dropdown.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_Batch_Ticket_Activity_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 's', $rnd_activity);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>activity_dropdown.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$id = $row['id'];
				$activity = $row['activity'];
				
				// if ($id == $activity){
				// 	echo "<option value=".$id." selected>".$activity."</option>";
				// }else {
					echo "<option value=".$id.">".$activity."</option>";
				// }
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>