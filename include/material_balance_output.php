
		<tr>
			<th> Date Transferred </th>
			<th> MIRS# </th>
			<th colspan="2"> Bag# </th>
			<th> TOS (kgs) </th>
			<th> Underpack (kgs) </th>
		</tr>
<?php

$batch_id = strval($_GET['batch_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_output.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_Output_Details_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 'i', $batch_id);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_output.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result))
			{
				$date_transferred = $row["TempTransDate"];
				$tos_number = $row["TempRefNumber"];
				$start_bag_number = $row["TempStartBag"];
				$end_bag_number = $row["TempEndBag"];
				$underpack = $row["TempQuantity"];
				$total_underpack = $row["TotalUnderpack"];
				$bag_quantity = $row["TotalQty"];
				$quantity_per_tos = $row["TempQtyPerTOS"];
?>	
				<tr align="center">
					<td class="border_right border_bottom"> <?php echo $date_transferred;?> </td>
					<td class="border_right border_bottom"> <?php echo $tos_number;?> </td>
					<td class="border_right border_bottom"> <?php echo $start_bag_number;?> </td>
					<td class="border_right border_bottom"> <?php echo $end_bag_number;?> </td>
					<td class="border_right border_bottom"> <?php echo $quantity_per_tos;?> </td>
					<td class="border_bottom"> <?php echo $underpack;?> </td>
				</tr>
<?php
			}
			if ( $batch_id != '' ){
?>		
				<tr align="center" class="bgTotal">
					<td colspan="4" align="right" class="border_bottom border_right"> <label class="constant_total"> TOTAL </label> </td>

					<td class="border_bottom border_right"> <label class="constant_total"> <?php echo $bag_quantity;?> </label> </td>

					<td class="border_bottom"> 
						<label class="constant_total"> <?php echo $total_underpack;?> </label> 
					</td>
				</tr>

				<tr align="center" class="bgTotal">
					<input type="hidden" id="hidTotalOutput" value="<?php echo $bag_quantity;?> ">

					<td colspan="4" align="right" class="border_right"> <label class="constant_total"> GOOD OUTPUT (kgs) </label> </td>

					<td id="tdTotalOutput" class="border_right"> 
						<label class="constant_total"> <?php echo $bag_quantity;?> </label> 
					</td>

					<input type="hidden" id="hidGoodOutput" value="<?php echo $bag_quantity;?>">
					
					<td> 
						<label class="constant_total"> <?php echo $total_underpack;?> </label> 
					</td>

					<input type="hidden" id="hidUnderpack" value="<?php echo $total_underpack;?>">
				</tr>
<?php
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>