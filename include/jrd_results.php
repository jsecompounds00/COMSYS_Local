<?php
		session_start();
		ob_start();      
		require("database_connect.php");
		require("/init_unset_values/jrd_init_value.php");

		$TypeID = intval($_GET['sltJRD']);
		$RecommendationID = 0;
		$Type = 'JRD';

		$qry3 = "SELECT * FROM jrd WHERE id = $TypeID";
		$result3 = mysqli_query($db, $qry3);
		$processError3 = mysqli_error($db);

		if(!empty($processError3)) 
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_results.php'.'</td><td>'.$processError3.' near line 21.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			while ( $row = mysqli_fetch_assoc( $result3 ) ){
				$type_of_request = $row['type_of_request'];
				$tag_eval_replicate = $row['tag_eval_replicate'];
			}
			$db->next_result();
			$result3->close();
		}

		$qry2 = "SELECT * FROM jrd_client_response WHERE jrd_id = $TypeID";
		$result2 = mysqli_query($db, $qry2);
		$processError2 = mysqli_error($db);

		if(!empty($processError2)) 
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_results.php'.'</td><td>'.$processError2.' near line 44.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			while ( $row = mysqli_fetch_assoc( $result2 ) ){
				$RecommendationID = $row['id'];
				$jrd_id = $row['jrd_id'];
				$Recommendation = htmlspecialchars($row['client_response']);
				$TestObjective = htmlspecialchars($row['jrd_test_objective']);
				$Introduction = htmlspecialchars($row['jrd_test_objective']);
				$Methodology = htmlspecialchars($row['jrd_methodology']);
				$created_at = $row['created_at'];
				$created_id = $row['created_id'];
			}
			$db->next_result();
			$result2->close();
		}

		
	######### Getting data for Test Evaluation

		$qry1 = mysqli_prepare($db, "CALL sp_Batch_Ticket_for_Testing_Query(?, ?)");
		mysqli_stmt_bind_param($qry1, 'is', $TypeID, $Type);
		$qry1->execute();
		$result1 = mysqli_stmt_get_result($qry1);
		$processError1 = mysqli_error($db);

		if(!empty($processError1))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_results.php'.'</td><td>'.$processError1.' near line 48.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			while ( $row = mysqli_fetch_assoc( $result1 ) ){
				$fg_soft = $row['fg_soft'];
				$dynamic_milling = $row['dynamic_milling'];
				$oil_aging = $row['oil_aging'];
				$oven_aging = $row['oven_aging'];
				$strand_inspection = $row['strand_inspection'];
				$pellet_inspection = $row['pellet_inspection'];
				$impact_test = $row['impact_test'];
				$static_heating = $row['static_heating'];
				$color_change_test = $row['color_change_test'];
				$water_immersion = $row['water_immersion'];
				$cold_inspection = $row['cold_inspection'];
				$DMCount = $row['DMCount'];
				$SICount = $row['SICount'];
				$PICount = $row['PICount'];
				$ITCount = $row['ITCount'];
				$SHCount = $row['SHCount'];
				$CCTCount = $row['CCTCount'];
				$WICount = $row['WICount'];
				$CICount = $row['CICount'];
			}
			$db->next_result();
			$result1->close();
		}

		$qry4 = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_for_Recommendation_Query(?, ?)");
		mysqli_stmt_bind_param($qry4, 'is', $TypeID, $Type);
		$qry4->execute();
		$result4 = mysqli_stmt_get_result($qry4);
		$processError4 = mysqli_error($db);

		if(!empty($processError4))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_results.php'.'</td><td>'.$processError4.' near line 48.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			while ( $row = mysqli_fetch_assoc( $result4 ) ){
				$jrd_dynamic_milling = $row['jrd_dynamic_milling'];
				$jrd_oil_aging = $row['jrd_oil_aging'];
				$jrd_oven_aging = $row['jrd_oven_aging'];
				$jrd_strand_inspection = $row['jrd_strand_inspection'];
				$jrd_pellet_inspection = $row['jrd_pellet_inspection'];
				$jrd_impact_test = $row['jrd_impact_test'];
				$jrd_static_heating = $row['jrd_static_heating'];
				$jrd_color_change_test = $row['jrd_color_change_test'];
				$jrd_water_immersion = $row['jrd_water_immersion'];
				$JNDMCount = $row['JNDMCount'];
				$JNSICount = $row['JNSICount'];
				$JNPICount = $row['JNPICount'];
				$JNITCount = $row['JNITCount'];
				$JNSHCount = $row['JNSHCount'];
				$JNCCTCount = $row['JNCCTCount'];
				$JNWICount = $row['JNWICount'];
			}
			$db->next_result();
			$result4->close();
		}
				if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
					echo '<ul class="err">';
					foreach($_SESSION['ERRMSG_ARR'] as $msg) {
						echo '<li>'.$msg.'</li>'; 
					}
					echo '</ul>';
					unset($_SESSION['ERRMSG_ARR']);
				}
?>

			<!-- ########## COMPLETE TESTING ########## -->
			<div class="results_wrapper">
				<table class="results_parent_tables_form">
					<tr>
						<td colspan="3"> <u> Part I: MECHANICAL, ELECTRIAL & PHYSICAL PROPERTIES </u> </td>
					</tr>
				</table>
				<?php
					if ( $type_of_request == "Evaluation" ){
				?>
						<label class="title_font"> EVALUATION : </label>
				<?php	
						require("/jrd_noreplicate_complete_testing_recommendation.php");
					}
					if ( $type_of_request == "Evaluation" && $tag_eval_replicate == 1 ){
				?>
						<label class="title_font"> REPLICATION : </label>
				<?php	
						require("/complete_testing_recommendation.php"); 
					}elseif ( $type_of_request != "Evaluation" ){
						require("/complete_testing_recommendation.php"); 
					}
				?>
			</div>
			<!-- ########## DYNAMIC MILLING ########## -->
			<?php
				if ( $dynamic_milling > 0 || $jrd_dynamic_milling > 0 ){
			?>
					<div class='results_wrapper'>
						<table class="results_parent_tables_form">
							<tr>
								<td> <u> Part II: DYNAMIC MILLING; TEST OF HEAT STABILITY & PROCESSABILITY </u> </td>
							</tr>
						</table>
						<?php
							if ( $type_of_request == 'Evaluation' ){
						?>
								<label class="title_font"> EVALUATION : </label>
						<?php
								require("/jrd_noreplicate_dynamic_milling_recommendation.php");  
							}
							if ( $type_of_request == 'Evaluation' && $tag_eval_replicate == 1 ){
						?> 
								<label class="title_font"> REPLICATION : </label>
						<?php
								require("/dynamic_milling_recommendation.php"); 
							}elseif ( $type_of_request != "Evaluation" ){
								require("/dynamic_milling_recommendation.php"); 
							}
						?>
					</div> 
			<?php
				}
			?>
			<!-- ########## STRANDS INSPECTION ########## -->
			<?php
				if ( $strand_inspection > 0 || $jrd_strand_inspection > 0 ){
			?>
					<div class='results_wrapper'>
						<table class="results_parent_tables_form">
							<tr>
								<td> <u> Part 
									<?php
										if ( ($DMCount == 1 && $strand_inspection > 0) || ($JNDMCount == 1 && $jrd_strand_inspection > 0) ){
											echo "III";
										}elseif ( ($DMCount == 0 && $strand_inspection > 0) || ($JNDMCount == 0 && $jrd_strand_inspection > 0) ){
											echo "II";
										}
									?>	
									: STRANDS / SHEETS INSPECTION </u> 
								</td>
							</tr>
						</table>
						<?php
							if ( $type_of_request == 'Evaluation' ){
						?>
								<label class="title_font"> EVALUATION : </label>
						<?php 
								require("/jrd_noreplicate_strand_inspection_recommendation.php");  
							}
								if ( $type_of_request == 'Evaluation' && $tag_eval_replicate == 1 ){
							?>
									<label class="title_font"> REPLICATION : </label>
							<?php
									require("/strand_inspection_recommendation.php"); 
								}elseif ( $type_of_request != "Evaluation" ){
									require("/strand_inspection_recommendation.php"); 
								}
							?>
						<table class="results_child_tables_form">
							<tr></tr>
							<tr>
								<td rowspan='3'> <strong> LEGEND: </strong> </td>
								<td> Classification of FISHEYES/BLISTERS: </td>
								<td> N - None </td>
								<td> F - Fine </td>
								<td colspan="3"> L - Large </td>
							</tr>
							<tr>
								<td> Evaluation for FISHEYES/BLISTERS & PIN HOLES: </td>
								<td> N - None </td>
								<td> O - Occasional </td>
								<td> I - Isolated </td>
								<td> F - Few </td>
								<td> R - Rampant </td>
							</tr>
							<tr>
								<td> Overall Evaluation Remarks: </td>
								<td> P - Passed </td>
								<td> F - Failed </td>
								<td colspan="4"> T - Tolerable </td>
							</tr>
						</table>
					</div>
			<?php
				}
			?>
			<!-- ########## PELLET INSPECTION ########## -->
			<?php
				if ( $pellet_inspection > 0 || $jrd_pellet_inspection > 0 ){
			?>
					<div class='results_wrapper'>
						<table class="results_parent_tables_form">
							<tr>
								<td> <u> Part 
									<?php
										if ( (($DMCount + $SICount ) == 2 && $pellet_inspection > 0) || (($JNDMCount + $JNSICount ) == 2 && $jrd_pellet_inspection > 0) ){
											echo "IV";
										}elseif ( (($DMCount + $SICount ) == 1 && $pellet_inspection > 0) || (($JNDMCount + $JNSICount ) == 1 && $jrd_pellet_inspection > 0) ){
											echo "III";
										}elseif ( (($DMCount + $SICount ) == 0 && $pellet_inspection > 0) || (($JNDMCount + $JNSICount ) == 0 && $jrd_pellet_inspection > 0) ){
											echo "II";
										}
									?>	: PELLET INSPECTION </u> 
								</td>
							</tr>
						</table>
						<?php
							if ( $type_of_request == 'Evaluation' ){
						?>
								<label class="title_font"> EVALUATION : </label>
						<?php 
								require("/jrd_noreplicate_pellet_inspection_recommendation.php"); 
							}
							if ( $type_of_request == 'Evaluation' && $tag_eval_replicate == 1 ){
						?>
								<label class="title_font"> REPLICATION : </label>
						<?php
								require("/pellet_inspection_recommendation.php"); 
							}elseif ( $type_of_request != "Evaluation" ){
								require("/pellet_inspection_recommendation.php"); 
							}
						?>
					</div>
			<?php
				}
			?>
			<!-- ########## IMPACT TEST ########## -->
			<?php
				if ( $impact_test > 0 || $jrd_impact_test > 0 ){
			?>
					<div class='results_wrapper'>
						<table class="results_parent_tables_form">
							<tr>
								<td> <u> Part 
									<?php
										if ( (($DMCount + $SICount + $PICount) == 3 && $impact_test > 0) || (($JNDMCount + $JNSICount + $JNPICount) == 3 && $jrd_impact_test > 0) ){
											echo "V";
										}elseif ( (($DMCount + $SICount + $PICount) == 2 && $impact_test > 0) || (($JNDMCount + $JNSICount + $JNPICount) == 2 && $jrd_impact_test > 0) ){
											echo "IV";
										}elseif ( (($DMCount + $SICount + $PICount) == 1 && $impact_test > 0) || (($JNDMCount + $JNSICount + $JNPICount) == 1 && $jrd_impact_test > 0) ){
											echo "III";
										}elseif ( (($DMCount + $SICount + $PICount) == 0 && $impact_test > 0) || (($JNDMCount + $JNSICount + $JNPICount) == 0 && $jrd_impact_test > 0) ){
											echo "II";
										}
									?>	: IMPACT STRENGTH TEST </u> 
								</td>
							</tr>
						</table>
						<?php
							if ( $type_of_request == 'Evaluation' ){
						?>
								<label class="title_font"> EVALUATION : </label>
						<?php 
								require("/jrd_noreplicate_impact_test_recommendation.php"); 
							}
							if ( $type_of_request == 'Evaluation' && $tag_eval_replicate == 1 ){
						?>
								<label class="title_font"> REPLICATION : </label>
						<?php	
								require("/impact_test_recommendation.php"); 
							}elseif ( $type_of_request != "Evaluation" ){
								require("/impact_test_recommendation.php"); 
							}
						?>
					</div>
			<?php
				}
			?>
			<!-- ########## STATIC HEATING ########## -->
			<?php
				if ( $static_heating > 0 || $jrd_static_heating > 0  ){
			?>
					<div class='results_wrapper'>
						<table class="results_parent_tables_form">
							<tr>
								<td> <u> Part 
									<?php
										if ( (($DMCount + $SICount + $PICount + $ITCount) == 4 && $static_heating > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount) == 4 && $jrd_static_heating > 0) ){
											echo "VI";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount) == 3 && $static_heating > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount) == 3 && $jrd_static_heating > 0) ){
											echo "V";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount) == 2 && $static_heating > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount) == 2 && $jrd_static_heating > 0) ){
											echo "IV";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount) == 1 && $static_heating > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount) == 1 && $jrd_static_heating > 0) ){
											echo "III";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount) == 0 && $static_heating > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount) == 0 && $jrd_static_heating > 0) ){
											echo "II";
										}
									?>	: STATIC HEATING </u> 
								</td>
							</tr>
						</table>
						<?php
							if ( $type_of_request == 'Evaluation' ){
						?>
								<label class="title_font"> EVALUATION : </label>
						<?php
								require("/jrd_noreplicate_static_heating_recommendation.php");  
							}
							if ( $type_of_request == 'Evaluation' && $tag_eval_replicate == 1 ){
						?>
								<label class="title_font"> REPLICATION : </label>
						<?php	
								require("/static_heating_recommendation.php"); 
							}elseif ( $type_of_request != "Evaluation" ){
								require("/static_heating_recommendation.php"); 
							}
						?>
					</div>
			<?php
				}
			?>
			<!-- ########## COLOR CHANGE TEST ########## -->
			<?php
				if ( $color_change_test > 0 || $jrd_color_change_test > 0  ){
			?>
					<div class='results_wrapper'>
						<table class="results_parent_tables_form">
							<tr>
								<td> <u> Part 
									<?php
										if ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 5 && $color_change_test > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount) == 5 && $jrd_color_change_test > 0) ){
											echo "VII";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 4 && $color_change_test > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount) == 4 && $jrd_color_change_test > 0) ){
											echo "VI";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 3 && $color_change_test > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount) == 3 && $jrd_color_change_test > 0) ){
											echo "V";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 2 && $color_change_test > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount) == 2 && $jrd_color_change_test > 0) ){
											echo "IV";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 1 && $color_change_test > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount) == 1 && $jrd_color_change_test > 0) ){
											echo "III";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 0 && $color_change_test > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount) == 0 && $jrd_color_change_test > 0) ){
											echo "II";
										}
									?>	: COLOR CHANGE TEST </u> 
								</td>
							</tr>
						</table>
						<?php
							if ( $type_of_request == 'Evaluation' ){
						?>
								<label class="title_font"> EVALUATION : </label>
						<?php 
								require("/jrd_noreplicate_color_change_test_recommendation.php");  
							}
							if ( $type_of_request == 'Evaluation' && $tag_eval_replicate == 1 ){
						?>
								<label class="title_font"> REPLICATION : </label>
						<?php	
								require("/color_change_test_recommendation.php"); 
							}elseif ( $type_of_request != "Evaluation" ){
								require("/color_change_test_recommendation.php"); 
							}
						?>
					</div>
			<?php
				}
			?>
			<!-- ########## WATER IMMERSION ########## -->
			<?php
				if ( $water_immersion > 0 || $jrd_water_immersion > 0 ){
			?>
					<div class='results_wrapper'>
						<table class="results_parent_tables_form">
								<td> <u> Part 
									<?php
										if ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 6 && $water_immersion > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount + $JNCCTCount) == 6 && $jrd_water_immersion > 0) ){
											echo "VIII";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 5 && $water_immersion > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount + $JNCCTCount) == 5 && $jrd_water_immersion > 0) ){
											echo "VII";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 4 && $water_immersion > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount + $JNCCTCount) == 4 && $jrd_water_immersion > 0) ){
											echo "VI";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 3 && $water_immersion > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount + $JNCCTCount) == 3 && $jrd_water_immersion > 0) ){
											echo "V";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 2 && $water_immersion > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount + $JNCCTCount) == 2 && $jrd_water_immersion > 0) ){
											echo "IV";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 1 && $water_immersion > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount + $JNCCTCount) == 1 && $jrd_water_immersion > 0) ){
											echo "III";
										}elseif ( (($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 0 && $water_immersion > 0) || (($JNDMCount + $JNSICount + $JNPICount + $JNITCount + $JNSHCount + $JNCCTCount) == 0 && $jrd_water_immersion > 0) ){
											echo "II";
										}
									?>	: LONG TERM ELECTRICAL PROPERTY </u> 
								</td>
							</tr>
						</table>
						<?php
							if ( $type_of_request == 'Evaluation' ){
						?>
								<label class="title_font"> EVALUATION : </label>
						<?php 
								require("/jrd_noreplicate_water_immersion_recommendation.php");  
							}
							if ( $type_of_request == 'Evaluation' && $tag_eval_replicate == 1 ){
						?>
								<label class="title_font"> REPLICATION : </label>
						<?php	
								require("/water_immersion_recommendation.php"); 
							}elseif ( $type_of_request != "Evaluation" ){
								require("/water_immersion_recommendation.php"); 
							}
						?>
					</div>
			<?php
				}
			?>
			<br><br>
			<table class='parent_tables_form'>
				<tr valign="top">
					<td> Test Objective: </td>
					<td>
						<textarea name="txtTestObjective" class="paragraph"><?php
							if ( $RecommendationID ){
								if ( $TestObjective == "" || is_null( $TestObjective ) ){
									echo $initTestObjective;
								}else{
									echo $TestObjective;
								}
							}else{
								echo $initTestObjective;
							}
						?></textarea>
					</td>
				</tr>
				<tr valign="top">
					<td> Introduction: </td>
					<td>
						<textarea name="txtIntroduction" class="paragraph"><?php
							if ( $RecommendationID ){
								if ( $Introduction == "" || is_null( $Introduction ) ){
									echo $initIntroduction;
								}else{
									$Introduction;
								}
							}else{
								echo $initIntroduction;
							}
						?></textarea>
					</td>
				</tr>
				<tr valign="top">
					<td> Methodology: </td>
					<td>
						<textarea name="txtMethodology" class="paragraph"><?php
							if ( $RecommendationID ){
								if ( $Methodology == "" || is_null( $Methodology ) ){
									echo $initMethodology;
								}else{
									echo $Methodology;
								}
							}else{
								echo $initMethodology;
							}
						?></textarea>
					</td>
				</tr>
				<tr valign='top'>
					<td>Client's Response:</td>
					<td>	
						<textarea name='txtRecommendation' class="paragraph"><?php
							if ( $RecommendationID ){
								if ( $Recommendation == "" || is_null( $Recommendation ) ){
									echo $initRecommendation;
								}else{
									echo $Recommendation;
								}
							}else{
								echo $initRecommendation;
							}
						?></textarea>
					</td>
				</tr>
				<tr class="align_bottom">
					<td >
						<?php
							$page = $_SESSION["page"];
							$search = htmlspecialchars($_SESSION["search"]);
							$qsone = htmlspecialchars($_SESSION["qsone"]);
							$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
							$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
							$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
							$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
						?>
						<input type="submit" name="btnSave" value="Save">	
						<input type="button" name="btnCancel" value="Cancel" onclick="location.href='jrd.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">	
						<input type='hidden' name='hidRecommendationID' value="<?php echo $RecommendationID;?>">
						<?php
							if ( $RecommendationID ){
								echo "<input type='hidden' name='hidCreatedAt' value='".$created_at."'>";
								echo "<input type='hidden' name='hidCreatedId' value='".$created_id."'>";
							}else{
								echo "<input type='hidden' name='hidCreatedAt' value='".date('Y-m-d H:i:s')."'>";
								echo "<input type='hidden' name='hidCreatedId' value='".$_SESSION['SESS_USER_ID']."'>";
							}
						?>
					</td>
				</tr>
			</table>
		    <?php 
				require("/init_unset_values/jrd_unset_value.php");
			?>
