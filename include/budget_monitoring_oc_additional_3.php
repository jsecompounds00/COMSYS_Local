<?php
	require("/database_connect.php");
	
	$count = intval($_GET["counter"]);

	if ( $count == 0 ){
		$count = 60;
	}

	for ($i=$count; $i < ($count+20) ; $i++) { 
?>
		<tr>
			<td>
				<select name="sltMiscType[]" id="sltMiscType<?php echo $i;?>" onchange="showItem(<?php echo $i;?>)">
					<?php
						$qryMT = "CALL sp_Budget_Miscellaneous_Type_Dropdown(1)";
						$resultMT = mysqli_query($db, $qryMT);
						$processErrorMT = mysqli_error($db);

						if(!empty($processErrorMT))
						{
							error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_budget_monitoring_oc.php"."</td><td>".$processErrorMT." near line 43.</td></tr></tbody>", 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							while($row = mysqli_fetch_assoc($resultMT))
							{
								$DDMTID = $row["DDMTID"];
								$DDMTName = $row["DDMTName"];

								echo "<option value='".$DDMTID."'>".$DDMTName."</option>";
							}
							
							$db->next_result();
							$resultMT->close();
						}
					?>
				</select>
			</td>

			<td>
				<select name="sltMiscellaneous[]" id="sltMiscellaneous<?php echo $i;?>" onchange="showUOM(<?php echo $i;?>), showUnitPrice(<?php echo $i;?>)">
					
				</select>
			</td>

			<td id="tdUOM<?php echo $i;?>">
				
			</td>

			<td id="tdUnitPrice<?php echo $i;?>">
				
			</td>

			<td>
				<input type="text" name="txtJAN[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtFEB[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtMAR[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtAPR[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtMAY[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtJUN[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtJUL[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtAUG[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtSEP[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtOCT[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtNOV[]" class="short_text_2">
			</td>

			<td>
				<input type="text" name="txtDEC[]" class="short_text_2">
			</td>
		</tr>
<?php
	}
	require("/database_close.php");
?>	