<?php
		session_start();
		ob_start();      
		require("database_connect.php");
		require("/init_unset_values/nrm_init_value.php");

		$TypeID = intval($_GET['sltNRM']);
		$RecommendationID = 0;
		$Type = 'NRM';

		$qry2 = "SELECT * FROM nrm_recommendation WHERE nrm_id = $TypeID";
		$result2 = mysqli_query( $db, $qry2);
		$processError2 = mysqli_error($db);

		if(!empty($processError2))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError2.' near line 21.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			while ( $row = mysqli_fetch_assoc( $result2 ) ){
				$RecommendationID = $row['id'];
				$nrm_id = $row['nrm_id'];
				$Recommendation = htmlspecialchars($row['recommendation']);
				// $recommendation_array = explode('<br>', $Recommendation);
				$created_at = $row['created_at'];
				$created_id = $row['created_id'];
				$TestObjective = htmlspecialchars($row['nrm_test_objective']);
				// $test_objective_array = explode('<br>', $TestObjective);
				$Introduction = htmlspecialchars($row['nrm_introduction']);
				// $introduction_array = explode('<br>', $Introduction);
				$Methodology = htmlspecialchars($row['nrm_methodology']);
				// $methodology_array = explode('<br>', $Methodology);
			}
		}

		
	######### Getting data for Test Evaluation

		$qry1 = mysqli_prepare($db, "CALL sp_Batch_Ticket_for_Testing_Query(?, ?)");
		mysqli_stmt_bind_param($qry1, 'is', $TypeID, $Type);
		$qry1->execute();
		$result1 = mysqli_stmt_get_result($qry1);
		$processError1 = mysqli_error($db);

		if(!empty($processError1))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError1.' near line 48.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			while ( $row = mysqli_fetch_assoc( $result1 ) ){
				$fg_soft = $row['fg_soft'];
				$dynamic_milling = $row['dynamic_milling'];
				$oil_aging = $row['oil_aging'];
				$oven_aging = $row['oven_aging'];
				$strand_inspection = $row['strand_inspection'];
				$pellet_inspection = $row['pellet_inspection'];
				$impact_test = $row['impact_test'];
				$static_heating = $row['static_heating'];
				$color_change_test = $row['color_change_test'];
				$water_immersion = $row['water_immersion'];
				$cold_inspection = $row['cold_inspection'];
				$DMCount = $row['DMCount'];
				$SICount = $row['SICount'];
				$PICount = $row['PICount'];
				$ITCount = $row['ITCount'];
				$SHCount = $row['SHCount'];
				$CCTCount = $row['CCTCount'];
				$WICount = $row['WICount'];
				$CICount = $row['CICount'];
			}
			$db->next_result();
			$result1->close();
		}
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';
						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}
						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);
					}
?>
				<div class='results_wrapper'>
					<table class="results_parent_tables_form">
						<tr>
							<td> <u> Part I: MECHANICAL, ELECTRIAL & PHYSICAL PROPERTIES </u> </td>
						</tr>
					</table>
					<?php
						require("/complete_testing_recommendation.php"); 
					?>
				</div>
				<?php
					### Dynamic Milling
					if ( $dynamic_milling > 0 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> Part II: DYNAMIC MILLING; TEST OF HEAT STABILITY & PROCESSABILITY </u> </td>
								</tr>
							</table>
							<?php require("/dynamic_milling_recommendation.php");  ?>
						</div>
				<?php
					}

					### Strands / Sheets Inspection
					if ( $strand_inspection > 0 ){
				?>
						<div class='results_wrapper'>
							<table class='results_parent_tables_form'>
								<tr>
									<td> <u> Part 
									<?php
										if ( $DMCount == 0 && $strand_inspection > 0 ){
											echo "II";
										}elseif ( $DMCount == 1 && $strand_inspection > 0 ){
											echo "III";
										}
									?>	
									: STRANDS / SHEETS INSPECTION </u> 
									</td>
								</tr>
							</table>
							<?php require("/strand_inspection_recommendation.php");  ?>
						</div>
				<?php
					}

					### Pellet Inspection
					if ( $pellet_inspection > 0 ){
				?>
						<div class='results_wrapper'>
							<table class='results_parent_tables_form'>
								<tr>
									<td> <u> Part 
									<?php
										if ( ($DMCount + $SICount ) == 0 && $pellet_inspection > 0 ){
											echo "II";
										}elseif ( ($DMCount + $SICount ) == 1 && $pellet_inspection > 0 ){
											echo "III";
										}elseif ( ($DMCount + $SICount ) == 2 && $pellet_inspection > 0 ){
											echo "IV";
										}
									?>	: PELLET INSPECTION </u> 
									</td>
								</tr>
							</table>
							<?php require("/pellet_inspection_recommendation.php"); ?>
						</div>
				<?php
					}

					### Impact Strength Test
					if ( $impact_test > 0 ){
				?>
						<div class='results_wrapper'>
							<table class='results_parent_tables_form'>
								<tr>
									<td> <u> Part 
									<?php
										if ( ($DMCount + $SICount + $PICount) == 0 && $impact_test > 0 ){
											echo "II";
										}elseif ( ($DMCount + $SICount + $PICount) == 1 && $impact_test > 0 ){
											echo "III";
										}elseif ( ($DMCount + $SICount + $PICount) == 2 && $impact_test > 0 ){
											echo "IV";
										}elseif ( ($DMCount + $SICount + $PICount) == 3 && $impact_test > 0 ){
											echo "V";
										}
									?>	: IMPACT STRENGTH TEST </u> 
									</td>
								</tr>
							</table>
							<?php require("/impact_test_recommendation.php"); ?>
						</div>
				<?php
					}

					### Static Heating
					if ( $static_heating > 0 ){
				?>
						<div class='results_wrapper'>
							<table class='results_parent_tables_form'>
								<tr>
									<td> <u> Part 
									<?php
										if ( ($DMCount + $SICount + $PICount + $ITCount) == 0 && $static_heating > 0 ){
											echo "II";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount) == 1 && $static_heating > 0 ){
											echo "III";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount) == 2 && $static_heating > 0 ){
											echo "IV";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount) == 3 && $static_heating > 0 ){
											echo "V";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount) == 4 && $static_heating > 0 ){
											echo "VI";
										}
									?>	: STATIC HEATING </u> 
									</td>
								</tr>
							</table>
							<?php require("/static_heating_recommendation.php"); ?>
						</div>
				<?php
					}

					### Color Change Test
					if ( $color_change_test > 0 ){
				?>
						<div class='results_wrapper'>
							<table class='results_parent_tables_form'>
								<tr>
									<td> <u> Part 
									<?php
										if ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 0 && $color_change_test > 0 ){
											echo "II";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 1 && $color_change_test > 0 ){
											echo "III";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 2 && $color_change_test > 0 ){
											echo "IV";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 3 && $color_change_test > 0 ){
											echo "V";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 4 && $color_change_test > 0 ){
											echo "VI";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount) == 5 && $color_change_test > 0 ){
											echo "VII";
										}
									?>	: COLOR CHANGE TEST </u> 
									</td>
								</tr>
							</table>
							<?php require("/color_change_test_recommendation.php"); ?>
						</div>
				<?php
					}

					### Water Immersion
					if ( $water_immersion > 0 ){
				?>
						<div class='results_wrapper'>
							<table class='results_parent_tables_form'>
								<tr>
									<td> <u> Part 
									<?php
										if ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 0 && $water_immersion > 0 ){
											echo "II";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 1 && $water_immersion > 0 ){
											echo "III";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 2 && $water_immersion > 0 ){
											echo "IV";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 3 && $water_immersion > 0 ){
											echo "V";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 4 && $water_immersion > 0 ){
											echo "VI";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 5 && $water_immersion > 0 ){
											echo "VII";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount) == 6 && $water_immersion > 0 ){
											echo "VIII";
										}
									?>	: LONG TERM ELECTRICAL PROPERTY </u> 
									</td>
								</tr>
							</table>
							<?php require("/water_immersion_recommendation.php"); ?>
						</div>
				<?php
					}

					### Cold Inspection
					if ( $cold_inspection > 0 ){
				?>
						<div class='results_wrapper'>
							<table class='results_parent_tables_form'>
								<tr>
									<td> <u> Part 
									<?php
										if ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount + $WICount) == 0 && $cold_inspection > 0 ){
											echo "II";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount + $WICount) == 1 && $cold_inspection > 0 ){
											echo "III";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount + $WICount) == 2 && $cold_inspection > 0 ){
											echo "IV";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount + $WICount) == 3 && $cold_inspection > 0 ){
											echo "V";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount + $WICount) == 4 && $cold_inspection > 0 ){
											echo "VI";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount + $WICount) == 5 && $cold_inspection > 0 ){
											echo "VII";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount + $WICount) == 6 && $cold_inspection > 0 ){
											echo "VIII";
										}elseif ( ($DMCount + $SICount + $PICount + $ITCount + $SHCount + $CCTCount + $WICount) == 7 && $cold_inspection > 0 ){
											echo "IX";
										}
									?>	: COLD BEND TEST </u> 
									</td>
								</tr>
							</table>
							<?php require("/cold_inspection_recommendation.php"); ?>
						</div>
				<?php
					}
				?>
				<br><br>
				<table class='parent_tables_form'>
					<tr valign="top">
						<td> Test Objective: </td>
						<td>
							<textarea name="txtTestObjective" class="paragraph"><?php
								if ( $RecommendationID ){
									if ( $TestObjective == "" || is_null( $TestObjective ) ){
										echo $initNRMTestObjective;
									}else{
										echo $TestObjective;
									}
								}else{
									echo $initNRMTestObjective;
								}
							?></textarea>
						</td>
					</tr>
					<tr valign="top">
						<td> Introduction: </td>
						<td>
							<textarea name="txtIntroduction" class="paragraph"><?php
								if ( $RecommendationID ){
									if ( $Introduction == "" || is_null( $Introduction ) ){
										echo $initNRMIntroduction;
									}else{
										echo $Introduction;
									}
								}else{
									echo $initNRMIntroduction;
								}
							?></textarea>
						</td>
					</tr>
					<tr valign="top">
						<td> Methodology: </td>
						<td>
							<textarea name="txtMethodology" class="paragraph"><?php
								if ( $RecommendationID ){
									if ( $Methodology == "" || is_null( $Methodology ) ){
										echo $initNRMMethodology;
									}else{
										echo $Methodology;
									}
								}else{
									echo $initNRMMethodology;
								}
							?></textarea>
						</td>
					</tr>
					<tr valign='top'>
						<td>Recommendation:</td>
						<td>
							<textarea name='txtRecommendation' class="paragraph"><?php
							if ( $RecommendationID ){
								if ( $Recommendation == "" || is_null( $Recommendation ) ){
									echo $initNRMRecommendation;
								}else{
									echo $Recommendation;
								}
							}else{
								echo $initNRMRecommendation;
							}
						?></textarea>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='nrm.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidRecommendationID' value='<?php echo $RecommendationID;?>'>
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $RecommendationID ? $created_at : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $RecommendationID ? $created_id : $_SESSION['SESS_USER_ID'] );?>'>
						</td>
					</tr>
				</table>
			    <?php 
					require("/init_unset_values/nrm_unset_value.php");
				?>