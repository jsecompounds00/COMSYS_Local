<?php

	require("database_connect.php");

	$qryWI = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Water_Immersion_Query( ?, ?, ? )");
	mysqli_stmt_bind_param($qryWI, 'isi', $JRDNoReplicateID, $product_for_evaluation, $FGID);
	$qryWI->execute();
	$resultWI = mysqli_stmt_get_result($qryWI);
	$processErrorWI = mysqli_error($db);

	if ( !empty($processErrorWI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_water_immersion.php'.'</td><td>'.$processErrorWI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultWI)){
			$start_date = $row['start_date'];
			$end_date = $row['end_date'];
			$day0 = $row['day0'];
			$day1 = $row['day1'];
			$day2 = $row['day2'];
			$day3 = $row['day3'];
			$day4 = $row['day4'];
			$day5 = $row['day5'];
			$day6 = $row['day6'];
			$day7 = $row['day7'];
			$day8 = $row['day8'];
			$day9 = $row['day9'];
			$day10 = $row['day10'];
			$day11 = $row['day11'];
			$day12 = $row['day12'];
			$day13 = $row['day13'];
			$day14 = $row['day14'];
			$WIcreated_at = $row['created_at'];
			$WIcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultWI->close();
	}

?>
	<script src="js/datetimepicker_css.js"></script>
	
	<table class='results_child_tables_form'>
		<col width='250'></col>
		<tr></tr>
		<tr>
			<td>Start Date:</td>
			<td colspan='3'>  
				<input type='text' name='txtImmersionStartDate' id='txtImmersionStartDate' value="<?php echo ( $water_immersion_id ? $start_date : $initNRWIImmersionStartDate ); ?>">
				<img src="js/cal.gif" onclick="javascript:NewCssCal('txtImmersionStartDate')" style="cursor:pointer" name="picker" />
			</td>
			<td>End Date:</td>
			<td colspan='3'>  
				<input type='text' name='txtImmersionEndDate' id='txtImmersionEndDate' value="<?php echo ( $water_immersion_id ? $end_date : $initNRWIImmersionEndDate ); ?>">
				<img src="js/cal.gif" onclick="javascript:NewCssCal('txtImmersionEndDate')" style="cursor:pointer" name="picker" />
			</td>
		</tr>
		<tr>
			<th colspan='15'>
				Ohm-cm/day
				<label class="instruction">
				  	(e.g. 1.23E12)
				</label>
			</th>
		</tr>
		<tr>
			<th>0</th>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
			<th>12</th>
			<th>13</th>
			<th>14</th>
		</tr>
		<tr>
			<td colspan="15" class="title_font">
				<?php echo ( $FGID == 0 ? $product_for_evaluation : $fg_name ); ?>
			</td>
		</tr>
		<tr>
			<td>
				<input type='text' name='txtDay0' size='3' value="<?php echo ( $water_immersion_id ? $day0 : $initNRWIDay0 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay1' size='3' value="<?php echo ( $water_immersion_id ? $day1 : $initNRWIDay1 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay2' size='3' value="<?php echo ( $water_immersion_id ? $day2 : $initNRWIDay2 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay3' size='3' value="<?php echo ( $water_immersion_id ? $day3 : $initNRWIDay3 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay4' size='3' value="<?php echo ( $water_immersion_id ? $day4 : $initNRWIDay4 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay5' size='3' value="<?php echo ( $water_immersion_id ? $day5 : $initNRWIDay5 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay6' size='3' value="<?php echo ( $water_immersion_id ? $day6 : $initNRWIDay6 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay7' size='3' value="<?php echo ( $water_immersion_id ? $day7 : $initNRWIDay7 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay8' size='3' value="<?php echo ( $water_immersion_id ? $day8 : $initNRWIDay8 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay9' size='3' value="<?php echo ( $water_immersion_id ? $day9 : $initNRWIDay9 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay10' size='3' value="<?php echo ( $water_immersion_id ? $day10 : $initNRWIDay10 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay11' size='3' value="<?php echo ( $water_immersion_id ? $day11 : $initNRWIDay11 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay12' size='3' value="<?php echo ( $water_immersion_id ? $day12 : $initNRWIDay12 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay13' size='3' value="<?php echo ( $water_immersion_id ? $day13 : $initNRWIDay13 ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay14' size='3' value="<?php echo ( $water_immersion_id ? $day14 : $initNRWIDay14 ); ?>">
			</td>
		</tr>
	</table>
	<br>
	<input type='hidden' name='hidWICreatedAt' value='<?php echo ( $water_immersion_id ? $WIcreated_at : date('Y-m-d H:i:s') );?>'>
	<input type='hidden' name='hidWICreatedId' value='<?php echo ( $water_immersion_id ? $WIcreated_id : 0 );?>'>
<?php	 require("database_close.php"); ?>