<?php

	require("database_connect.php");

	$qryCT = mysqli_prepare($db, "CALL sp_Batch_Ticket_Complete_Testing_Query( ? )");
	mysqli_stmt_bind_param($qryCT, 'i', $TrialID);
	$qryCT->execute();
	$resultCT = mysqli_stmt_get_result($qryCT);
	$processErrorCT = mysqli_error($db);

	if ( !empty($processErrorCT) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>complete_testing.php'.'</td><td>'.$processErrorCT.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultCT)){
			$specific_gravity = $row['specific_gravity'];
			$hardness_a = $row['hardness_a'];
			$hardness_d = $row['hardness_d'];
			$volume_resistivity = $row['volume_resistivity'];
			$unaged_tensile = $row['unaged_tensile'];
			$unaged_elongation = $row['unaged_elongation'];
			$ovenaged_tensile = $row['ovenaged_tensile'];
			$ovenaged_elongation = $row['ovenaged_elongation'];
			$oilaged_tensile = $row['oilaged_tensile'];
			$oilaged_elongation = $row['oilaged_elongation'];
			$CTcreated_at = $row['created_at'];
			$CTcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultCT->close();
	}
?>
	
	<table class="results_child_tables_form">
		<col width="250"></col>
		<tr></tr>
		<input type='hidden' name='hidCompleteTestingID' value='<?php echo $complete_testing_id; ?>'>
		<tr>
			<td>Specific Gravity:</td>
			<td colspan="4">
				<input type='text' name='txtSpecificGravity' value="<?php echo ( $complete_testing_id ? $specific_gravity : $initSpecificGravity_BT ); ?>">
			</td>
		</tr>
		<?php
			if ( $fg_soft == 1 ){
		?>
				<tr>
					<td>H - Shore A:</td>
					<td colspan="4">
						<input type='text' name='txtHardnessA' value="<?php echo ( $complete_testing_id ? $hardness_a : $initHardnessA_BT ); ?>">
					</td>
				</tr>
				<tr>
					<td>Volume Resistivity:</td>
					<td colspan="4">
						<input type='text' name='txtVolumeResistivity' value="<?php echo ( $complete_testing_id ? $volume_resistivity : $initVolumeResistivity_BT ); ?>">
						<label class="instruction">
	    				  	(e.g. 1.23E12)
	    				</label>
					</td>
				</tr>
		<?php
			}else{
		?>
				<tr>
					<td>H - Shore D:</td>
					<td colspan="4">
						<input type='text' name='txtHardnessD' value="<?php echo ( $complete_testing_id ? $hardness_d : $initHardnessD_BT ); ?>">
					</td>
				</tr>
		<?php
			}

			if ( $fg_soft == 1 ){
		?>
				<tr>
					<th></th>
					<th>
						Unaged Properties
					</th>
					<?php
						echo ( $oven_aging == 1 ? "<th> Oven Aged </th>" : NULL );
						echo ( $oil_aging == 1 ? "<th> Oil Aged </th>" : NULL );
					?>
				</tr>
				<tr>
					<td> Tensile Strength:</td>
					<td>
						<input type='text' name='txtUnagedTS' value="<?php echo ( $complete_testing_id ? $unaged_tensile : $initUnagedTS_BT ); ?>">
					</td>
					<?php
						if ( $oven_aging == 1 ){
					?>
							<td>
								<input type='text' name='txtOvenTS' value="<?php echo ( $complete_testing_id ? $ovenaged_tensile : $initOvenTS_BT ); ?>">
							</td>
					<?php
						}

						if ( $oil_aging == 1 ){
					?>
							<td>
								<input type='text' name='txtOilTS' value="<?php echo ( $complete_testing_id ? $oilaged_tensile : $initOilTS_BT ); ?>">
							</td>
					<?php
						}
					?>
				</tr>
				<tr>
					<td>Elongation:</td>
					<td>
						<input type='text' name='txtUnagedE' value="<?php echo ( $complete_testing_id ? $unaged_elongation : $initUnagedE_BT ); ?>">
					</td>
					<?php
						if ( $oven_aging == 1 ){
					?>
							<td>
								<input type='text' name='txtOvenE' value="<?php echo ( $complete_testing_id ? $ovenaged_elongation : $initOvenE_BT ); ?>">
							</td>
					<?php
						}

						if ( $oil_aging == 1 ){
					?>
							<td>
								<input type='text' name='txtOilE' value="<?php echo ( $complete_testing_id ? $oilaged_elongation : $initOilE_BT ); ?>">
							</td>
					<?php
						}
					?>
				</tr>
			</table>
	<?php
		}
	
		if ( $complete_testing_id ){
			echo "<input type='hidden' name='hidCTCreatedAt' value='".$CTcreated_at."'>";
			echo "<input type='hidden' name='hidCTCreatedId' value='".$CTcreated_id."'>";	
		}else{
			echo "<input type='hidden' name='hidCTCreatedAt' value='".date('Y-m-d H:i:s')."'>";
			echo "<input type='hidden' name='hidCTCreatedId' value=''>";	
		}

		require("database_close.php");
	?>