<?php
	session_start();
	require("/database_connect.php");
	require("/init_unset_values/jrd_init_value.php");
	$JRDId = intval($_GET["JRDId"]);

	$qry = mysqli_prepare($db, "CALL sp_JRD_Query(?)");
	mysqli_stmt_bind_param($qry, 'i', $JRDId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>product_evaluation.php'.'</td><td>'.$processError.' near line 13.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		while($row = mysqli_fetch_assoc($result))
		{
			$mod_eval_finished_good_id = $row['mod_eval_finished_good_id'];
			$eval_finished_good = $row['eval_finished_good'];
			$eval_manufacturer = $row['eval_manufacturer'];
			$tag_eval_replicate = $row['tag_eval_replicate'];
		}
	}
	$db->next_result();
	$result->close();
?>
<table class='child_tables_form'>
	<tr>
		<td> 
			Product Name: 
		</td>
		<td class="border_right"> 
			<input type='text' name='txtCompetitorsProduct' value="<?php echo( $JRDId ? $eval_finished_good : $initCompetitorsProduct );?>">
		</td>
		<td> 
			Source / Manufacturer:
		</td>
		<td> 
			<input type='text' name='txtCompetitorsName' value="<?php echo( $JRDId ? $eval_manufacturer : $initCompetitorsName );?>">
		</td>
	</tr>
	<tr>
		<td> 
			Product for Comparison:
		</td>
		<td class="border_right"> 
			<select name='sltFGItem'>
				<?php
					$qryFG = "CALL sp_FG_Calcu_Dropdown()";
					$resultFG = mysqli_query($db, $qryFG);
					$processErrorFG = mysqli_error($db);

					if ( !empty($processErrorFG) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>product_evaluation.php'.'</td><td>'.$processErrorFG.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						echo "<option></option>";
						while ( $row = mysqli_fetch_assoc( $resultFG ) ){

							if ( $JRDId ){
								if ( $mod_eval_finished_good_id == $row["id"] ){
									echo " <option value='".$row['id']."' selected> ".$row['name']." </option> ";
								}else{
									echo " <option value='".$row['id']."'> ".$row['name']." </option> ";
								}
							}else{
								if ( $initFGItem == $row["id"] ){
									echo " <option value='".$row['id']."' selected> ".$row['name']." </option> ";
								}else{
									echo " <option value='".$row['id']."'> ".$row['name']." </option> ";
								}
							}
						}
						$db->next_result();
						$resultFG->close();
					}

				?>
			</select>
		</td>
		<td> 
			Product evaluated needs replication?
		</td>
		<td> 
			<input type='checkbox' name='chkNeedsReplication' id='chkNeedsReplication' value='1' <?php echo( $JRDId ? ( $tag_eval_replicate ? "checked" : "" ) : ( $initTagReplicate ? "checked" : "" ) ); ?>> 
				<label for='chkNeedsReplication'>Yes</label> 
		</td>
	</tr>
</table>
<?php
	require("/init_unset_values/jrd_unset_value.php");
	require("/database_close.php");
?>