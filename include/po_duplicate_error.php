<?php

$txtPONum = strval($_GET['txtPONum']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>po_duplicate_error.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_PO_Duplicate_Query(?)");
		mysqli_stmt_bind_param($qry, 's', $txtPONum);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>po_duplicate_error.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			$row = mysqli_fetch_assoc($result);
			$id = $row["id"];
			if ( !empty($id) ){
				echo "<input type='hidden' name='hidErrorDuplicate' value='1'>";
				echo "<label style='color:red; font-size:13px; font-style:italic; vertical-align: top;'>* PO number already exists.</label>";
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>