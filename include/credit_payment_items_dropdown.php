<tr>
	<th>Invoice Inclusion</th>
	<th>Invoice No.</th>
	<th>A/R</th>
	<th>Paid Amount</th>
	<th>Credit Memo</th>
	<th>Debit Memo</th>
	<th>Discount</th>
	<th>Balance</th>
	<th>Payment</th>
</tr>
<?php

	session_start();
	ob_start();

	$sltCustomer = intval($_GET["sltCustomer"]);
	$hidCreditId = intval($_GET["hidCreditId"]);


	require("database_connect.php");
	require("/init_unset_values/credit_payment_init_value.php");

	$qry = mysqli_prepare($db, "CALL sp_Credit_Payment_Items_Dropdown( ?, ? )");
	mysqli_stmt_bind_param($qry, 'ii', $sltCustomer, $hidCreditId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);

	$processError = mysqli_error($db);

	if(!empty($processError))
	{
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>credit_payment_items_dropdown.php'.'</td><td>'.$processError.' near line 37.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$i = 0;
		while($row = mysqli_fetch_assoc($result))
		{
			
?>			
			<tr>
				<td> 
					<input type="checkbox" name="chkIncludeInvoice[<?php echo $i;?>]" id="chkIncludeInvoice<?php echo $i;?>" 
						onchange="enableCreditItems(<?php echo $i;?>), computeBalance(<?php echo $i;?>), computeTotalPayment()"
						value = "1"> 
				</td>

				<td> 
					<?php echo $row["invoice_number"];  ?> 
					<input type="hidden" name="hidInvoiceNumber[<?php echo $i;?>]" id="hidInvoiceNumber<?php echo $i;?>" value="<?php echo $row["invoice_number"]; ?>" disabled>
					<input type="hidden" name="refInvoiceNumber[<?php echo $i;?>]" value="<?php echo $row["invoice_number"]; ?>">
				</td>

				<td> 
					<?php echo number_format((float)($row["invoice_balance"]), 2, ".", ",");  ?> 
					<input type="hidden" id="hidARAmount<?php echo $i;?>" value="<?php echo $row["invoice_balance"]; ?>" disabled>
				</td>

				<td> 
					<?php echo ( $row["paid_amount"] <> 0 ? number_format((float)($row["paid_amount"]), 2, ".", ",") : "-" );  ?> 
					<input type="hidden" id="hidPaidAmount<?php echo $i;?>" value="<?php echo $row["paid_amount"]; ?>" disabled>
				</td>

				<td>
					<?php echo ( $row["credit_memo_amount"] <> 0 ? number_format((float)($row["credit_memo_amount"]), 2, ".", ",") : "-" );  ?> 
					<input type="hidden" id="hidCreditAmount<?php echo $i;?>" value="<?php echo $row["credit_memo_amount"]; ?>" disabled>
				</td>

				<td>
					<?php echo ( $row["debit_memo_amount"] <> 0 ? number_format((float)($row["debit_memo_amount"]), 2, ".", ",") : "-" );  ?> 
					<input type="hidden" id="hidDebitAmount<?php echo $i;?>" value="<?php echo $row["debit_memo_amount"]; ?>" disabled>
				</td>

				<td>
					<input type="text" name="txtDiscount[<?php echo $i;?>]" id="txtDiscount<?php echo $i;?>" readOnly 
						value="<?php echo ( $hidCreditId ? "" : $initCJCPDiscount[$i] );?>"
						onkeyup="computeBalance(<?php echo $i;?>)" 
						onblur="computeBalance(<?php echo $i;?>)">
				</td>

				<td>
					<input type="text" name="txtBalance[<?php echo $i;?>]" id="txtBalance<?php echo $i;?>" readOnly>
				</td>

				<td>
					<input type="text" name="txtAmount[<?php echo $i;?>]" id="txtAmount<?php echo $i;?>" readOnly  class="sample"
						onkeyup="computeTotalPayment()" value="<?php echo ( $hidCreditId ? "" : $initCJCPAmount[$i] );?>">
					<input type="hidden" name="hidCreditItemID[<?php echo $i;?>]" id="hidCreditItemID<?php echo $i;?>">
				</td>
			</tr>
<?php
			$i++;
		}
?>
			<tr>
				<td colspan="8" align="right"> <b><i> TOTAL: </i></b> </td>
				<td> <input type="text" name="txtTotalPayment"  id="txtTotalPayment" readOnly> </td>
			</tr>
			<tr>
				<td colspan="6"></td>

				<td colspan="3" align="right">
					<label style="color:red; font-size:11px; font-style:italic; vertical-align: top; margin-left:20px;">
						<div id="lblErrorMessage"></div>
						<input type="hidden" name="hidErrorMessage" id="hidErrorMessage">
					</label>
				</td>
			</tr>
<?php
	}
	$db->next_result();
	$result->close();

	require("/init_unset_values/credit_payment_unset_value.php");
	require("database_close.php");
?>