<?php
########## Getting last inserted batch ticket no.
		$qryB = mysqli_prepare($db, "CALL sp_Batch_Ticket_Num( ? )");
		mysqli_stmt_bind_param($qryB, 'i', $BatchTicketID);
		$qryB->execute();
		$resultB = mysqli_stmt_get_result($qryB); 
		$processError1 = mysqli_error($db);

		if(!empty($processError1))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processError1.' near line 42.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultB))
			{
				$bat_num = $row['db_BatchTicketNumber'];
				
			}
			$db->next_result();
			$resultB->close();
		}
########## Getting last inserted trial no.
		$qryT = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_Num( ?, ? )");
		mysqli_stmt_bind_param($qryT, 'ii', $BatchTicketID, $BatchTicketTrialID);
		$qryT->execute();
		$resultT = mysqli_stmt_get_result($qryT); 
		$processError2 = mysqli_error($db);

		if(!empty($processError2))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processError2.' near line 118.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultT))
			{
				$db_BatchTicketTrial = $row['db_BatchTicketTrial'];
			}
			$db->next_result();
			$resultT->close();
		}
########## BATCH TICKET QUERY - ADD ANOTHER TRIAL
		$qryN = mysqli_prepare($db, "CALL sp_Batch_Ticket_Query( ? )");
		mysqli_stmt_bind_param($qryN, 'i', $BatchTicketID);
		$qryN->execute();
		$resultN = mysqli_stmt_get_result($qryN); 
		$processErrorN = mysqli_error($db);

		if ( !empty($processErrorN) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processErrorN.' near line 78.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			while($row = mysqli_fetch_assoc($resultN)){
				$db_Requesttype = $row['request_type'];
				$db_ActivityID = $row['activity_id'];
				$db_BatchTicketNo = $row['batch_ticket_no'];
				$db_BatchTicketDate = $row['batch_ticket_date'];
				$db_FGSoft = $row['fg_soft'];
				$db_FinishedGoodID = $row['finished_good_id'];
				$db_NewFinishedGood = htmlspecialchars($row['new_finished_good']);
				$db_FormulaTypeID = $row['formula_type_id'];
				$db_hidBatCreatedAt = $row['created_at'];
				$db_hidBatCreatedID = $row['created_id'];
				$db_DynamicMilling = $row['dynamic_milling'];
				$db_OilAging = $row['oil_aging'];
				$db_OvenAging = $row['oven_aging'];
				$db_StrandInspection = $row['strand_inspection'];
				$db_PelletInspection = $row['pellet_inspection'];
				$db_ImpactTest = $row['impact_test'];
				$db_StaticHeating = $row['static_heating'];
				$db_ColorChangeTest = $row['color_change_test'];
				$db_WaterImmersion = $row['water_immersion'];
				$db_ColdInspection = $row['cold_inspection'];
			}
			$db->next_result();
			$resultN->close();
		}

########## BATCH TICKET QUERY - EDIT TRIAL
		$qryBT = mysqli_prepare($db, "CALL sp_Batch_Ticket_Trial_Query( ?, ? )");
		mysqli_stmt_bind_param($qryBT, 'ii', $BatchTicketID, $BatchTicketTrialID);
		$qryBT->execute();
		$resultBT = mysqli_stmt_get_result($qryBT); 
		$processErrorBT = mysqli_error($db);

		if(!empty($processErrorBT))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processErrorBT.' near line 148.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			$db_trialItemId = array();
			$db_raw_material_type_id = array();
			$db_rm_id = array();
			$db_phr = array();
			$db_qty = array();
			while($row = mysqli_fetch_assoc($resultBT))
			{
				$db_trial_id = $row['trial_id'];
				$db_nrm_id = $row['nrm_id'];
				$db_jrd_id = $row['jrd_id'];
				$db_ter_no = htmlspecialchars($row['ter_no']);
				$db_trial_no = $row['trial_no'];
				$db_lot_no = $row['lot_no'];
				$db_trial_date = $row['trial_date'];
				$db_ext_dar_no = $row['ext_dar_no'];
				$db_ext_z1 = $row['ext_z1'];
				$db_ext_z2 = $row['ext_z2'];
				$db_ext_dh = $row['ext_dh'];
				$db_ext_screw_speed = $row['ext_screw_speed'];
				$db_ext_cutter_speed = $row['ext_cutter_speed'];
				$db_mix_dar_no = $row['mix_dar_no'];
				$db_mix_parameter1 = $row['mix_parameter1'];
				$db_mix_parameter2 = $row['mix_parameter2'];
				$db_mix_parameter3 = $row['mix_parameter3'];
				$db_mix_parameter4 = $row['mix_parameter4'];
				$db_mix_parameter5 = $row['mix_parameter5'];
				$db_mix_sequence1 = $row['mix_sequence1'];
				$db_mix_sequence2 = $row['mix_sequence2'];
				$db_mix_sequence3 = $row['mix_sequence3'];
				$db_mix_sequence4 = $row['mix_sequence4'];
				$db_mix_sequence5 = $row['mix_sequence5'];
				$db_multiplier = $row['multiplier'];
				$db_remarks = htmlspecialchars($row['remarks']);
				// $db_remarks_array = explode('<br>', $db_remarks);
				$db_bat_trial_created_at = $row['created_at'];
				$db_bat_trial_created_id = $row['created_id'];

				$db_trialItemId[] = $row['trial_item_id'];	

				$db_raw_material_type_id[] = $row['raw_material_type_id'];	

				$db_rm_id[] = $row['raw_material_id'];	

				$db_rm[] = $row['raw_material'];	

				$db_phr[] = $row['phr'];	

				$db_qty[] = $row['quantity'];	
			}
			$db->next_result();
			$resultBT->close();
		}
?>