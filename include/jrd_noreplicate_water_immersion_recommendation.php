<?php

	require("database_connect.php");

	$qryWI = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Water_Immersion_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qryWI, 'is', $TypeID, $Type);
	$qryWI->execute();
	$resultWI = mysqli_stmt_get_result($qryWI);
	$processErrorWI = mysqli_error($db);

	if ( !empty($processErrorWI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_water_immersion_recommendation.php'.'</td><td>'.$processErrorWI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
			$WIJRDID = array();
			$WIJRDTestID = array();
			$WIJRDFGSoft = array();
			$ProductName = array();
			$day0 = array();
			$day1 = array();
			$day2 = array();
			$day3 = array();
			$day4 = array();
			$day5 = array();
			$day6 = array();
			$day7 = array();
			$day8 = array();
			$day9 = array();
			$day10 = array();
			$day11 = array();
			$day12 = array();
			$day13 = array();
			$day14 = array();
		while($row = mysqli_fetch_assoc($resultWI)){
			$WIJRDID[] = $row['WIJRDID'];
			$WIJRDTestID[] = $row['WIJRDTestID'];
			$WIJRDFGSoft[] = $row['WIJRDFGSoft'];
			$ProductName[] = $row['ProductName'];
			$immersion_start = $row['immersion_start'];
			$immersion_end = $row['immersion_end'];
			$day0[] = $row['day0'];
			$day1[] = $row['day1'];
			$day2[] = $row['day2'];
			$day3[] = $row['day3'];
			$day4[] = $row['day4'];
			$day5[] = $row['day5'];
			$day6[] = $row['day6'];
			$day7[] = $row['day7'];
			$day8[] = $row['day8'];
			$day9[] = $row['day9'];
			$day10[] = $row['day10'];
			$day11[] = $row['day11'];
			$day12[] = $row['day12'];
			$day13[] = $row['day13'];
			$day14[] = $row['day14'];
		}
		$db->next_result();
		$resultWI->close();
	}

?>
	<script src="js/datetimepicker_css.js"></script>
	
	<table class="results_child_tables_form">
		<colgroup><col width="200px"></col></colgroup>
		<tr>
			<td colspan='2' rowspan='2' valign='top'> Immersion Date: </td>
			<td> Start: </td>
			<td colspan='2'> <?php echo $immersion_start;?> </td>
		</tr>
		<tr>
			<td> End: </td>
			<td colspan='2'> <?php echo $immersion_end;?> </td>
		</tr>
	</table>
	<table class="results_child_tables_form">
		<tr>
			<th rowspan='2'>Product Name</th>
			<th colspan='15'>
				VOLUME RESISTIVITY, Ohm-cm/day
			</th>
		</tr>
		<tr>
			<th>0</th>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
			<th>12</th>
			<th>13</th>
			<th>14</th>
		</tr>
		<?php
			foreach( $WIJRDID as $key => $value ){
		?>		<tr>
					<td colspan='16' class="title_font"> <?php echo $ProductName[$key]; ?> </td>
				</tr>
				<tr>
					<td></td>
					<td> <?php echo $day0[$key]; ?> </td>
					<td> <?php echo $day1[$key]; ?> </td>
					<td> <?php echo $day2[$key]; ?> </td>
					<td> <?php echo $day3[$key]; ?> </td>
					<td> <?php echo $day4[$key]; ?> </td>
					<td> <?php echo $day5[$key]; ?> </td>
					<td> <?php echo $day6[$key]; ?> </td>
					<td> <?php echo $day7[$key]; ?> </td>
					<td> <?php echo $day8[$key]; ?> </td>
					<td> <?php echo $day9[$key]; ?> </td>
					<td> <?php echo $day10[$key]; ?> </td>
					<td> <?php echo $day11[$key]; ?> </td>
					<td> <?php echo $day12[$key]; ?> </td>
					<td> <?php echo $day13[$key]; ?> </td>
					<td> <?php echo $day14[$key]; ?> </td>
				</tr>
		<?php	
			}
		?>
	</table>
	<br>
<?php	

	require("database_close.php");
?>