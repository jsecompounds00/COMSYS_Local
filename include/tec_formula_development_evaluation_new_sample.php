<table class='trials_child_tables_form'>
	<tr>
		<th>RM Type</th>
		<th>Raw Materials</th>
		<th>PHR</th>
		<th>Quantity</th>
	</tr>
	<tbody>
		<?php
			for ( $i = 0; $i < 15; $i++ ){
		?>
		<tr>
			<td>
				<input type='hidden' name='hidBatchTicketTrialItemId[]' value='0'>
				<select name='txtRM_type_id[]' id='txtRM_type_id<?php echo $i;?>' onchange='showRM(this.value, <?php echo $i?>)'>
					<?php
						$qryRT = "CALL sp_RMType_BAT_Dropdown(0)";
						$resultRT = mysqli_query( $db, $qryRT );
						$processErrorRT = mysqli_error( $db ) ;
						if ($processErrorRT){
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>tec_formula_developement_evaluation_new_sample.php'.'</td><td>'.$processErrorRT.' near line 327.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}else{
							echo "<option></option>";

							while ( $row = mysqli_fetch_assoc( $resultRT ) ){
								$RMTypeID = $row['rm_type_id'];
								$code = $row['code'];

								echo "<option value='".$RMTypeID."'>$code</option>";
							}
							$db->next_result();
							$resultRT->close();
						}
					?>
				</select>
			</td>
			<td>
				<select name='txtRM_id[]' id='txtRM_id<?php echo $i;?>'>
					
				</select>
			</td>
			<td>
				<input type='text' name='txtPhr[]' id='txtPhr<?php echo $i; ?>' onchange='autoMultiply(), autoSum()' onkeyup='autoMultiply(), autoSum()'>
				<input type='hidden' name='hidPhr[]' id='hidPhr<?php echo $i; ?>' 
			</td>
			<td>
				<input type='text' name='txtQty[]' id='txtQty<?php echo $i; ?>' value='0' readOnly>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td>
				<input type='hidden' name='txtNRm_type_id' value='1000'>
				<input type='hidden' name='txtNRm_id' value='0'>
				<input type='hidden' name='hidNBatchTicketTrialItemId' value='0'>
				<input type='hidden' name='txtNPHR' id='txtNPHR' onchange='autoMultiply(), autoSum()' onkeyup='autoMultiply(), autoSum()' value='0'>
				<input type='hidden' name='txtNQuantity' id='txtNQuantity' readonly value='0'>
			</td>
		</tr>
	</tbody>
	<tr id='total' class="spacing border_top">
		<td colspan='3' align="right"><b>TOTAL</b></td>
		<td>
			<input type='text' name='txtTotalQty' id='txtTotalQty' readonly value=''>
		</td>
	</tr>
</table>