<?php

	require("database_connect.php");

	$qryWI = mysqli_prepare($db, "CALL sp_Batch_Ticket_Water_Immersion_Query( ? )");
	mysqli_stmt_bind_param($qryWI, 'i', $TrialID);
	$qryWI->execute();
	$resultWI = mysqli_stmt_get_result($qryWI);
	$processErrorWI = mysqli_error($db);

	if ( !empty($processErrorWI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>static_heating.php'.'</td><td>'.$processErrorWI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultWI)){
			$start_date = $row['start_date'];
			$end_date = $row['end_date'];
			$day0 = $row['day0'];
			$day1 = $row['day1'];
			$day2 = $row['day2'];
			$day3 = $row['day3'];
			$day4 = $row['day4'];
			$day5 = $row['day5'];
			$day6 = $row['day6'];
			$day7 = $row['day7'];
			$day8 = $row['day8'];
			$day9 = $row['day9'];
			$day10 = $row['day10'];
			$day11 = $row['day11'];
			$day12 = $row['day12'];
			$day13 = $row['day13'];
			$day14 = $row['day14'];
			$WIcreated_at = $row['created_at'];
			$WIcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultWI->close();
	}

?>
	<script src="js/datetimepicker_css.js"></script>
	
	<table class='results_child_tables_form'>
		<col width='250'></col>
		<tr></tr>
		<input type='hidden' name='hidWaterImmersionID' value='<?php echo $water_immersion_id; ?>'>
		<tr>
			<td></td>
			<td>Start Date:</td>
			<td colspan='3'>  
				<input type='text' name='txtImmersionStartDate' id='txtImmersionStartDate' value="<?php echo ( $water_immersion_id ? $start_date : $initImmersionStartDate_BT );?>">        
				<img src="js/cal.gif" onclick="javascript:NewCssCal('txtImmersionStartDate')" style="cursor:pointer" name="picker" />
			</td>
			<td>End Date:</td>
			<td colspan='3'>  
				<input type='text' name='txtImmersionEndDate' id='txtImmersionEndDate' value="<?php echo ( $water_immersion_id ? $end_date : $initImmersionEndDate_BT );?>">
				<img src="js/cal.gif" onclick="javascript:NewCssCal('txtImmersionEndDate')" style="cursor:pointer" name="picker" />
			</td>
		</tr>
		<tr>
			<th rowspan='2'>Trial</th>
			<th colspan='15'>
				Ohm-cm/day
				<label style='instruction'>
				  	(e.g. 1.23E12)
				</label>
			</th>
		</tr>
		<tr>
			<th>0</th>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
			<th>12</th>
			<th>13</th>
			<th>14</th>
		</tr>
		<tr>
			<td>
				<?php
					echo $trial_no;
				?>
			</td>
			<td>
				<input type='text' name='txtDay0' size='9' value="<?php echo ( $water_immersion_id ? $day0 : $initDay0_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay1' size='9' value="<?php echo ( $water_immersion_id ? $day1 : $initDay1_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay2' size='9' value="<?php echo ( $water_immersion_id ? $day2 : $initDay2_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay3' size='9' value="<?php echo ( $water_immersion_id ? $day3 : $initDay3_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay4' size='9' value="<?php echo ( $water_immersion_id ? $day4 : $initDay4_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay5' size='9' value="<?php echo ( $water_immersion_id ? $day5 : $initDay5_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay6' size='9' value="<?php echo ( $water_immersion_id ? $day6 : $initDay6_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay7' size='9' value="<?php echo ( $water_immersion_id ? $day7 : $initDay7_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay8' size='9' value="<?php echo ( $water_immersion_id ? $day8 : $initDay8_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay9' size='9' value="<?php echo ( $water_immersion_id ? $day9 : $initDay9_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay10' size='9' value="<?php echo ( $water_immersion_id ? $day10 : $initDay10_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay11' size='9' value="<?php echo ( $water_immersion_id ? $day11 : $initDay11_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay12' size='9' value="<?php echo ( $water_immersion_id ? $day12 : $initDay12_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay13' size='9' value="<?php echo ( $water_immersion_id ? $day13 : $initDay13_BT ); ?>">
			</td>
			<td>
				<input type='text' name='txtDay14' size='9' value="<?php echo ( $water_immersion_id ? $day14 : $initDay14_BT ); ?>">
			</td>
		</tr>
	</table>

<?php			
	
	if ( $water_immersion_id ){
		echo "<input type='hidden' name='hidWICreatedAt' value='".$WIcreated_at."'>";
		echo "<input type='hidden' name='hidWICreatedId' value='".$WIcreated_id."'>";	
	}else{
		echo "<input type='hidden' name='hidWICreatedAt' value='".date('Y-m-d H:i:s')."'>";
		echo "<input type='hidden' name='hidWICreatedId' value=''>";	
	}

	require("database_close.php");
?>