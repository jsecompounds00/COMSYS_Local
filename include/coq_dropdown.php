<?php
	
	$fgid = intval($_GET['fgid']);
	$lotnumber = strval($_GET['lotnumber']);
	
	require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>coq_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare( $db, "CALL sp_COQ_Dropdown(?, ?)" );
		mysqli_stmt_bind_param( $qry, 'is', $fgid, $lotnumber );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>coq_dropdown.php'.'</td><td>'.$processError.' near line 22.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
?>
			<table class="child_tables_form">
				<tr>
					<th> COQ Date </th>
					<th> Production Date </th>
					<th> Bag Number </th>
					<th> Oven Aged </th>
					<th> Oil Aged </th>
					<th> </th>
				</tr>
				<?php
					while ( $row = mysqli_fetch_assoc( $result ) ){
						$COQId = $row['COQId'];
						$COQDate = $row['COQDate'];
						$ProductionDate = $row['ProductionDate'];
						$BagNumber = $row['BagNumber'];
						$ONTSOvenAged = $row['ONTSOvenAged'];
						$ONEOvenAged = $row['ONEOvenAged'];
						$ONTSOilAged = $row['ONTSOilAged'];
						$ONEOilAged = $row['ONEOilAged'];
						$Category = $row['Category'];
				?>

						<tr>
							<td >
								<input type='hidden' name='txtCOQDate' value='<?php echo $COQDate;?>'><?php echo $COQDate;?>
							</td>
							<td >
								<input type='hidden' name='txtProductionDate' value='<?php echo $ProductionDate;?>'><?php echo $ProductionDate;?>
							</td>
							<td >
								<input type='hidden' name='txtBagNumber' value='<?php echo $BagNumber;?>'><?php echo $BagNumber;?>
							</td>
							<td >
								<?php
									if ( $Category != 'PMH' && $Category != 'RIGID' )
									{
										if ( $ONTSOvenAged == 1 && $ONEOvenAged == 1 ){
											echo "<input type='checkbox' checked disabled>";
										}else{
											echo "<input type='checkbox' disabled>";
										}
									}else{
										echo "N/A";
									}
								?>	
							</td>
							<td>
								<?php
									if ( $Category != 'PMH' && $Category != 'RIGID' )
									{
										if ( $ONTSOilAged == 1 && $ONEOilAged == 1 ){
											echo "		<input type='checkbox' checked disabled>";
										}else{
											echo "		<input type='checkbox' disabled>";
										}
									}else{
										echo "N/A";
									}
								?>
							</td>
							</td>
							<td>
								<input type='button' name='btnEdit' value='Edit' onclick="location.href='edit_coq.php?coq_id=<?php echo $COQId;?>&fg_id=<?php echo $fgid;?>'">
							</td>
						</tr>
				<?php
					}
					$db->next_result();
					$result->close();
				?>
			</table>
<?php
		}

	}
	require("/database_close.php");
?>