<?php

$nrm = intval($_GET['nrm']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>activity_dropdown.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_NRM_Number_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 'i', $nrm);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>activity_dropdown.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{	
			if ( $nrm )
				echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$nrm_id = $row['nrm_id'];
				$nrm_no = $row['nrm_no'];
				
				echo "<option value=".$nrm_id.">".$nrm_no."</option>";
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>