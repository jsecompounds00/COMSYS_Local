<?php
$fg_type = strval($_GET['fg_type']);
$fg_id = strval($_GET['fg_id']);
$forecast_id = intval($_GET['forecast_id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown_forecast.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		// echo $fg_type;
		$qry = "CALL sp_FG_Dropdown_Sales($fg_type)";
		$result = mysqli_query($db, $qry);
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown_forecast.php'.'</td><td>'.$processError.' near line 23.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			if(!$forecast_id)
				echo "<option></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				
				$FgId = $row['id'];
				$Fg = $row['name'];

				if($forecast_id){
					if ($FgId == $fg_id){
						echo "<option value=".$FgId." selected>".$Fg."</option>";
					}
				}else{
					if ($FgId == $fg_id){
						echo "<option value=".$FgId." selected>".$Fg."</option>";
					}else {
						echo "<option value=".$FgId.">".$Fg."</option>";
					}
				}
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?>