<?php

	$hidCanvassID = intval($_GET['hidCanvassID']);
	$Division = strval($_GET['Division']);
	$hidPREIDs = strval($_GET['hidPREIDs']);

	$preId = array();
	$preId = explode(',', $hidPREIDs);
	$preId = array_unique($preId);

	require("database_connect.php");

	$i=0;

	
	if (!empty($errno)){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvas_pre_number_dropdown.php'.'</td><td>'.$error.' near line 14.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{

		$qryG = mysqli_prepare($db, "CALL sp_Canvass_PRE_Group_Dropdown( ?, ? )");
		mysqli_stmt_bind_param($qryG, 'si', $Division, $hidCanvassID);
		$qryG->execute();
		$resultG = mysqli_stmt_get_result($qryG); //return results of query
		$processErrorG = mysqli_error($db);

		if(!empty($processErrorG))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvas_pre_number_dropdown.php'.'</td><td>'.$processErrorG.' near line 24.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			$item_type = array();
			$item_type_id = array();
			while($row = mysqli_fetch_assoc($resultG))
			{
				$item_type[] = $row["item_type"];
				$item_type_id[] = $row["item_type_id"];

			}

			$db->next_result();
			$resultG->close();

		}

		foreach ($item_type as $key => $value) {
			echo "<optgroup label='".$value."'>";
				$qry = mysqli_prepare($db, "CALL sp_Canvass_PRE_Number_Dropdown( ?, ?, ?)");
				mysqli_stmt_bind_param($qry, 'isi', $item_type_id[$key], $Division, $hidCanvassID);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry);
				$processError = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvas_pre_number_dropdown.php'.'</td><td>'.$processError.' near line 24.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					while($row = mysqli_fetch_assoc($result))
					{
						$db_preId = $row["preId"];
						$db_pre_number = $row["pre_number"];

						if ( is_null($hidPREIDs) ){
							echo "	<option value='".$db_preId."'>".$db_pre_number."</option>";
						}else{
							if ( in_array( $db_preId, $preId ) ) {
								echo "	<option value='".$db_preId."' selected>".$db_pre_number."</option>";
							}
							else {
								echo "	<option value='".$db_preId."'>".$db_pre_number."</option>";
							}
						}

					}
					$db->next_result();
					$result->close();
				}
			echo "</optgroup>";
		}
	}

	require("database_close.php");

?>