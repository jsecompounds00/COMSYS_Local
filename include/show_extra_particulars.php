<?php
require("/init_value.php");

session_start();
ob_start();

require("/database_connect.php");

$count = $_GET['count'];

for ( $i = $count; $i < ($count + 10); $i++ ){
?>
	<tr>
		<td>	
			<input type='hidden' name='hidParID[]' value='0'>
			<input type='text' name='txtDescription[]' size='50'>
		</td>
		<td>
			<select name="sltUOM[]">
				<?php 
					$qryST = "CALL sp_UOM_Dropdown(1)";
					$resultST = mysqli_query($db, $qryST);
					$processError1 = mysqli_error($db);

					if(!empty($processError1))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_expense_code.php'.'</td><td>'.$processError1.' near line 115.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($resultST))
						{
							$uomID = $row['id'];
							$uCode = $row['code'];

							echo "<option value='".$uomID."'>".$uCode."</option>";
						}

						$db->next_result();
						$resultST->close();
					}
				?>
			</select>
		</td>
		<td>
			<input type='text' name='txtUnitPrice[]' size='20'>
		</td>
			<input type='hidden' name='hidCreatedAt[]' value='<?php echo date('Y-m-d H:i:s'); ?>'>
			<input type='hidden' name='hidCreatedID[]' value='<?php echo $_SESSION['SESS_USER_ID']; ?>'>
	</tr>
<?php	
}	
require("database_close.php");
?>