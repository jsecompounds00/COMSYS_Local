<?php

	require("database_connect.php");

	$qryPI = mysqli_prepare($db, "CALL sp_Batch_Ticket_Pellet_Inspection_Query( ? )");
	mysqli_stmt_bind_param($qryPI, 'i', $TrialID);
	$qryPI->execute();
	$resultPI = mysqli_stmt_get_result($qryPI);
	$processErrorPI = mysqli_error($db);

	if ( !empty($processErrorPI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pellet_inspection.php'.'</td><td>'.$processErrorPI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultPI)){
			$clarity_color = $row['clarity_color'];
			$PIcreated_at = $row['created_at'];
			$PIcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultPI->close();
	}

?>
	
	<table class="results_child_tables_form">
		<col width="250"></col>
		<tr></tr>
		<input type='hidden' name='hidPelletInspectionID' value='<?php echo $pellet_inspect_id; ?>'>
		<tr>
			<td >Clarity / Color Conform:</td>
			<td>
				<input type='checkbox' name='chkClarityColorConform' <?php echo ( $pellet_inspect_id ? ( $clarity_color ? "checked" : "" ) : ( $initClarityColorConform_BT ? "checked" : "" ) );?>>
			</td>
		</tr>
	</table>

<?php		
	
	if ( $pellet_inspect_id ){
		echo "<input type='hidden' name='hidPICreatedAt' value='".$PIcreated_at."'>";
		echo "<input type='hidden' name='hidPICreatedId' value='".$PIcreated_id."'>";	
	}else{
		echo "<input type='hidden' name='hidPICreatedAt' value='".date('Y-m-d H:i:s')."'>";
		echo "<input type='hidden' name='hidPICreatedId' value=''>";	
	}

	require("database_close.php");
?>