
<table class="results_child_tables_form">
	<tr>
		<th colspan="3"> <i> CONDITIONS OF TEST </i> </th>
	</tr>
	<tr>
		<th> <i> Front Roll Mill, ºC </i> </th>
		<th> <i> Rear Roll Mill, ºC </i> </th>
		<th> <i> Duration, minutes </i> </th>
	</tr>	
	<tr>
		<td align="center"> <b> 180 </b> </td>
		<td align="center"> <b> 170 </b> </td>
		<td align="center"> <b> 60 </b> </td>
	</tr>
</table>
<?php

	require("database_connect.php");

	$qryDM = mysqli_prepare($db, "CALL sp_BAT_Dynamic_Milling_Recommendation_Query( ? , ?)");
	mysqli_stmt_bind_param($qryDM, 'is', $TypeID, $Type);
	$qryDM->execute();
	$resultDM = mysqli_stmt_get_result($qryDM);
	$processErrorDM = mysqli_error($db);

	if ( !empty($processErrorDM) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dynamic_milling_recommendation.php'.'</td><td>'.$processErrorDM.' near line 28.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultDM)){
				$DMtrial_no = $row['DMtrial_no'];
				$DMFGName = $row['DMFGName'];

				$DMTrial1 = $row['DMTrial1']; $DMTrial2 = $row['DMTrial2']; $DMTrial3 = $row['DMTrial3']; $DMTrial4 = $row['DMTrial4']; $DMTrial5 = $row['DMTrial5'];
				$DMTrial6 = $row['DMTrial6']; $DMTrial7 = $row['DMTrial7']; $DMTrial8 = $row['DMTrial8']; $DMTrial9 = $row['DMTrial9']; $DMTrial10 = $row['DMTrial10'];
				$DMTrial11 = $row['DMTrial11']; $DMTrial12 = $row['DMTrial12']; $DMTrial13 = $row['DMTrial13']; $DMTrial14 = $row['DMTrial14']; $DMTrial15 = $row['DMTrial15'];
				$DMTrial16 = $row['DMTrial16']; $DMTrial17 = $row['DMTrial17']; $DMTrial18 = $row['DMTrial18']; $DMTrial19 = $row['DMTrial19']; $DMTrial20 = $row['DMTrial20'];

				$DMColor1 = $row['DMColor1']; $DMColor2 = $row['DMColor2']; $DMColor3 = $row['DMColor3']; $DMColor4 = $row['DMColor4']; $DMColor5 = $row['DMColor5'];
				$DMColor6 = $row['DMColor6']; $DMColor7 = $row['DMColor7']; $DMColor8 = $row['DMColor8']; $DMColor9 = $row['DMColor9']; $DMColor10 = $row['DMColor10'];
				$DMColor11 = $row['DMColor11']; $DMColor12 = $row['DMColor12']; $DMColor13 = $row['DMColor13']; $DMColor14 = $row['DMColor14']; $DMColor15 = $row['DMColor15'];
				$DMColor16 = $row['DMColor16']; $DMColor17 = $row['DMColor17']; $DMColor18 = $row['DMColor18']; $DMColor19 = $row['DMColor19']; $DMColor20 = $row['DMColor20'];

				$DMHeat1 = $row['DMHeat1']; $DMHeat2 = $row['DMHeat2']; $DMHeat3 = $row['DMHeat3']; $DMHeat4 = $row['DMHeat4']; $DMHeat5 = $row['DMHeat5'];
				$DMHeat6 = $row['DMHeat6']; $DMHeat7 = $row['DMHeat7']; $DMHeat8 = $row['DMHeat8']; $DMHeat9 = $row['DMHeat9']; $DMHeat10 = $row['DMHeat10'];
				$DMHeat11 = $row['DMHeat11']; $DMHeat12 = $row['DMHeat12']; $DMHeat13 = $row['DMHeat13']; $DMHeat14 = $row['DMHeat14']; $DMHeat15 = $row['DMHeat15'];
				$DMHeat16 = $row['DMHeat16']; $DMHeat17 = $row['DMHeat17']; $DMHeat18 = $row['DMHeat18']; $DMHeat19 = $row['DMHeat19']; $DMHeat20 = $row['DMHeat20'];

				$DMProcess1 = $row['DMProcess1']; $DMProcess2 = $row['DMProcess2']; $DMProcess3 = $row['DMProcess3']; $DMProcess4 = $row['DMProcess4']; $DMProcess5 = $row['DMProcess5'];
				$DMProcess6 = $row['DMProcess6']; $DMProcess7 = $row['DMProcess7']; $DMProcess8 = $row['DMProcess8']; $DMProcess9 = $row['DMProcess9']; $DMProcess10 = $row['DMProcess10'];
				$DMProcess11 = $row['DMProcess11']; $DMProcess12 = $row['DMProcess12']; $DMProcess13 = $row['DMProcess13']; $DMProcess14 = $row['DMProcess14']; $DMProcess15 = $row['DMProcess15'];
				$DMProcess16 = $row['DMProcess16']; $DMProcess17 = $row['DMProcess17']; $DMProcess18 = $row['DMProcess18']; $DMProcess19 = $row['DMProcess19']; $DMProcess20 = $row['DMProcess20'];

				$DMSampling1 = $row['DMSampling1']; $DMSampling2 = $row['DMSampling2']; $DMSampling3 = $row['DMSampling3']; $DMSampling4 = $row['DMSampling4']; $DMSampling5 = $row['DMSampling5'];
				$DMSampling6 = $row['DMSampling6']; $DMSampling7 = $row['DMSampling7']; $DMSampling8 = $row['DMSampling8']; $DMSampling9 = $row['DMSampling9']; $DMSampling10 = $row['DMSampling10'];
				$DMSampling11 = $row['DMSampling11']; $DMSampling12 = $row['DMSampling12']; $DMSampling13 = $row['DMSampling13']; $DMSampling14 = $row['DMSampling14']; $DMSampling15 = $row['DMSampling15'];
				$DMSampling16 = $row['DMSampling16']; $DMSampling17 = $row['DMSampling17']; $DMSampling18 = $row['DMSampling18']; $DMSampling19 = $row['DMSampling19']; $DMSampling20 = $row['DMSampling20'];
		?>
			<table class="results_child_tables_form">
				<tr>
					<td colspan='<?php echo ($DMtrial_no+1); ?>'> <?php  echo $DMFGName; ?> </td>
				</tr>
				<tr>
					<th></th>
					<?php
						if ( $DMTrial1 <= $DMtrial_no &&$DMTrial1!= '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial1; ?> </th>
					<?php		
						}

						if ( $DMTrial2 <= $DMtrial_no && $DMTrial2 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial2; ?> </th>
					<?php		
						}

						if ( $DMTrial3 <= $DMtrial_no && $DMTrial3 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial3; ?> </th>
					<?php		
						}

						if ( $DMTrial4 <= $DMtrial_no && $DMTrial4 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial4; ?> </th>
					<?php		
						}

						if ( $DMTrial5 <= $DMtrial_no && $DMTrial5 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial5; ?> </th>
					<?php		
						}

						if ( $DMTrial6 <= $DMtrial_no && $DMTrial6 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial6; ?> </th>
					<?php		
						}

						if ( $DMTrial7 <= $DMtrial_no && $DMTrial7 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial7; ?> </th>
					<?php		
						}

						if ( $DMTrial8 <= $DMtrial_no && $DMTrial8 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial8; ?> </th>
					<?php		
						}

						if ( $DMTrial9 <= $DMtrial_no && $DMTrial9 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial9; ?> </th>
					<?php		
						}

						if ( $DMTrial10 <= $DMtrial_no && $DMTrial10 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial10; ?> </th>
					<?php		
						}

						if ( $DMTrial11 <= $DMtrial_no && $DMTrial11 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial11; ?> </th>
					<?php		
						}

						if ( $DMTrial12 <= $DMtrial_no && $DMTrial12 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial12; ?> </th>
					<?php		
						}

						if ( $DMTrial13 <= $DMtrial_no && $DMTrial13 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial13; ?> </th>
					<?php		
						}

						if ( $DMTrial14 <= $DMtrial_no && $DMTrial14 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial14; ?> </th>
					<?php		
						}

						if ( $DMTrial15 <= $DMtrial_no && $DMTrial15 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial15; ?> </th>
					<?php		
						}

						if ( $DMTrial16 <= $DMtrial_no && $DMTrial16 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial16; ?> </th>
					<?php		
						}

						if ( $DMTrial17 <= $DMtrial_no && $DMTrial17 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial17; ?> </th>
					<?php		
						}

						if ( $DMTrial18 <= $DMtrial_no && $DMTrial18 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial18; ?> </th>
					<?php		
						}

						if ( $DMTrial19 <= $DMtrial_no && $DMTrial19 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial19; ?> </th>
					<?php		
						}

						if ( $DMTrial20 <= $DMtrial_no && $DMTrial20 != '-' ){
					?>
							<th> <?php echo 'Trial '.$DMTrial20; ?> </th>
					<?php		
						}
					?>
				</tr>
		<!-- ###################### SAMPLINGCX	 -->
				<tr>
					<td> Sampling </td>
					<?php
						if ( $DMTrial1 <= $DMtrial_no && $DMTrial1 != '-' ){
					?>
							<td> <?php echo $DMSampling1; ?> </td>
					<?php		
						}

						if ( $DMTrial2 <= $DMtrial_no && $DMTrial2 != '-' ){
					?>
							<td> <?php echo $DMSampling2; ?> </td>
					<?php		
						}

						if ( $DMTrial3 <= $DMtrial_no && $DMTrial3 != '-' ){
					?>
							<td> <?php echo $DMSampling3; ?> </td>
					<?php		
						}

						if ( $DMTrial4 <= $DMtrial_no && $DMTrial4 != '-' ){
					?>
							<td> <?php echo $DMSampling4; ?> </td>
					<?php		
						}

						if ( $DMTrial5 <= $DMtrial_no && $DMTrial5 != '-' ){
					?>
							<td> <?php echo $DMSampling5; ?> </td>
					<?php		
						}

						if ( $DMTrial6 <= $DMtrial_no && $DMTrial6 != '-' ){
					?>
							<td> <?php echo $DMSampling6; ?> </td>
					<?php		
						}

						if ( $DMTrial7 <= $DMtrial_no && $DMTrial7 != '-' ){
					?>
							<td> <?php echo $DMSampling7; ?> </td>
					<?php		
						}

						if ( $DMTrial8 <= $DMtrial_no && $DMTrial8 != '-' ){
					?>
							<td> <?php echo $DMSampling8; ?> </td>
					<?php		
						}

						if ( $DMTrial9 <= $DMtrial_no && $DMTrial9 != '-' ){
					?>
							<td> <?php echo $DMSampling9; ?> </td>
					<?php		
						}

						if ( $DMTrial10 <= $DMtrial_no && $DMTrial10 != '-' ){
					?>
							<td> <?php echo $DMSampling10; ?> </td>
					<?php		
						}

						if ( $DMTrial11 <= $DMtrial_no && $DMTrial11 != '-' ){
					?>
							<td> <?php echo $DMSampling11; ?> </td>
					<?php		
						}

						if ( $DMTrial12 <= $DMtrial_no && $DMTrial12 != '-' ){
					?>
							<td> <?php echo $DMSampling12; ?> </td>
					<?php		
						}

						if ( $DMTrial13 <= $DMtrial_no && $DMTrial13 != '-' ){
					?>
							<td> <?php echo $DMSampling13; ?> </td>
					<?php		
						}

						if ( $DMTrial14 <= $DMtrial_no && $DMTrial14 != '-' ){
					?>
							<td> <?php echo $DMSampling14; ?> </td>
					<?php		
						}

						if ( $DMTrial15 <= $DMtrial_no && $DMTrial15 != '-' ){
					?>
							<td> <?php echo $DMSampling15; ?> </td>
					<?php		
						}

						if ( $DMTrial16 <= $DMtrial_no && $DMTrial16 != '-' ){
					?>
							<td> <?php echo $DMSampling16; ?> </td>
					<?php		
						}

						if ( $DMTrial17 <= $DMtrial_no && $DMTrial17 != '-' ){
					?>
							<td> <?php echo $DMSampling17; ?> </td>
					<?php		
						}

						if ( $DMTrial18 <= $DMtrial_no && $DMTrial18 != '-' ){
					?>
							<td> <?php echo $DMSampling18; ?> </td>
					<?php		
						}

						if ( $DMTrial19 <= $DMtrial_no && $DMTrial19 != '-' ){
					?>
							<td> <?php echo $DMSampling19; ?> </td>
					<?php		
						}

						if ( $DMTrial20 <= $DMtrial_no && $DMTrial20 != '-' ){
					?>
							<td> <?php echo $DMSampling20; ?> </td>
					<?php		
						}
					?>
				</tr>
		<!-- ###################### Color Conformance	 -->
				<tr>
					<td> Color Conformance </td>
					<?php
						if ( $DMTrial1 <= $DMtrial_no && $DMTrial1 != '-' ){
					?>
							<td> <?php echo $DMColor1; ?> </td>
					<?php		
						}

						if ( $DMTrial2 <= $DMtrial_no && $DMTrial2 != '-' ){
					?>
							<td> <?php echo $DMColor2; ?> </td>
					<?php		
						}

						if ( $DMTrial3 <= $DMtrial_no && $DMTrial3 != '-' ){
					?>
							<td> <?php echo $DMColor3; ?> </td>
					<?php		
						}

						if ( $DMTrial4 <= $DMtrial_no && $DMTrial4 != '-' ){
					?>
							<td> <?php echo $DMColor4; ?> </td>
					<?php		
						}

						if ( $DMTrial5 <= $DMtrial_no && $DMTrial5 != '-' ){
					?>
							<td> <?php echo $DMColor5; ?> </td>
					<?php		
						}

						if ( $DMTrial6 <= $DMtrial_no && $DMTrial6 != '-' ){
					?>
							<td> <?php echo $DMColor6; ?> </td>
					<?php		
						}

						if ( $DMTrial7 <= $DMtrial_no && $DMTrial7 != '-' ){
					?>
							<td> <?php echo $DMColor7; ?> </td>
					<?php		
						}

						if ( $DMTrial8 <= $DMtrial_no && $DMTrial8 != '-' ){
					?>
							<td> <?php echo $DMColor8; ?> </td>
					<?php		
						}

						if ( $DMTrial9 <= $DMtrial_no && $DMTrial9 != '-' ){
					?>
							<td> <?php echo $DMColor9; ?> </td>
					<?php		
						}

						if ( $DMTrial10 <= $DMtrial_no && $DMTrial10 != '-' ){
					?>
							<td> <?php echo $DMColor10; ?> </td>
					<?php		
						}

						if ( $DMTrial11 <= $DMtrial_no && $DMTrial11 != '-' ){
					?>
							<td> <?php echo $DMColor11; ?> </td>
					<?php		
						}

						if ( $DMTrial12 <= $DMtrial_no && $DMTrial12 != '-' ){
					?>
							<td> <?php echo $DMColor12; ?> </td>
					<?php		
						}

						if ( $DMTrial13 <= $DMtrial_no && $DMTrial13 != '-' ){
					?>
							<td> <?php echo $DMColor13; ?> </td>
					<?php		
						}

						if ( $DMTrial14 <= $DMtrial_no && $DMTrial14 != '-' ){
					?>
							<td> <?php echo $DMColor14; ?> </td>
					<?php		
						}

						if ( $DMTrial15 <= $DMtrial_no && $DMTrial15 != '-' ){
					?>
							<td> <?php echo $DMColor15; ?> </td>
					<?php		
						}

						if ( $DMTrial16 <= $DMtrial_no && $DMTrial16 != '-' ){
					?>
							<td> <?php echo $DMColor16; ?> </td>
					<?php		
						}

						if ( $DMTrial17 <= $DMtrial_no && $DMTrial17 != '-' ){
					?>
							<td> <?php echo $DMColor17; ?> </td>
					<?php		
						}

						if ( $DMTrial18 <= $DMtrial_no && $DMTrial18 != '-' ){
					?>
							<td> <?php echo $DMColor18; ?> </td>
					<?php		
						}

						if ( $DMTrial19 <= $DMtrial_no && $DMTrial19 != '-' ){
					?>
							<td> <?php echo $DMColor19; ?> </td>
					<?php		
						}

						if ( $DMTrial20 <= $DMtrial_no && $DMTrial20 != '-' ){
					?>
							<td> <?php echo $DMColor20; ?> </td>
					<?php		
						}
					?>
				</tr>
			<!-- ###################### Heat Stability	 -->	
				<tr>
					<td> Heat Stability </td>
					<?php
						if ( $DMTrial1 <= $DMtrial_no && !is_null($DMTrial1) ){
					?>
							<td> <?php echo $DMHeat1; ?> </td>
					<?php		
						}

						if ( $DMTrial2 <= $DMtrial_no && $DMTrial2 != '-' ){
					?>
							<td> <?php echo $DMHeat2; ?> </td>
					<?php		
						}

						if ( $DMTrial3 <= $DMtrial_no && $DMTrial3 != '-' ){
					?>
							<td> <?php echo $DMHeat3; ?> </td>
					<?php		
						}

						if ( $DMTrial4 <= $DMtrial_no && $DMTrial4 != '-' ){
					?>
							<td> <?php echo $DMHeat4; ?> </td>
					<?php		
						}

						if ( $DMTrial5 <= $DMtrial_no && $DMTrial5 != '-' ){
					?>
							<td> <?php echo $DMHeat5; ?> </td>
					<?php		
						}

						if ( $DMTrial6 <= $DMtrial_no && $DMTrial6 != '-' ){
					?>
							<td> <?php echo $DMHeat6; ?> </td>
					<?php		
						}

						if ( $DMTrial7 <= $DMtrial_no && $DMTrial7 != '-' ){
					?>
							<td> <?php echo $DMHeat7; ?> </td>
					<?php		
						}

						if ( $DMTrial8 <= $DMtrial_no && $DMTrial8 != '-' ){
					?>
							<td> <?php echo $DMHeat8; ?> </td>
					<?php		
						}

						if ( $DMTrial9 <= $DMtrial_no && $DMTrial9 != '-' ){
					?>
							<td> <?php echo $DMHeat9; ?> </td>
					<?php		
						}

						if ( $DMTrial10 <= $DMtrial_no && $DMTrial10 != '-' ){
					?>
							<td> <?php echo $DMHeat10; ?> </td>
					<?php		
						}

						if ( $DMTrial11 <= $DMtrial_no && $DMTrial11 != '-' ){
					?>
							<td> <?php echo $DMHeat11; ?> </td>
					<?php		
						}

						if ( $DMTrial12 <= $DMtrial_no && $DMTrial12 != '-' ){
					?>
							<td> <?php echo $DMHeat12; ?> </td>
					<?php		
						}

						if ( $DMTrial13 <= $DMtrial_no && $DMTrial13 != '-' ){
					?>
							<td> <?php echo $DMHeat13; ?> </td>
					<?php		
						}

						if ( $DMTrial14 <= $DMtrial_no && $DMTrial14 != '-' ){
					?>
							<td> <?php echo $DMHeat14; ?> </td>
					<?php		
						}

						if ( $DMTrial15 <= $DMtrial_no && $DMTrial15 != '-' ){
					?>
							<td> <?php echo $DMHeat15; ?> </td>
					<?php		
						}

						if ( $DMTrial16 <= $DMtrial_no && $DMTrial16 != '-' ){
					?>
							<td> <?php echo $DMHeat16; ?> </td>
					<?php		
						}

						if ( $DMTrial17 <= $DMtrial_no && $DMTrial17 != '-' ){
					?>
							<td> <?php echo $DMHeat17; ?> </td>
					<?php		
						}

						if ( $DMTrial18 <= $DMtrial_no && $DMTrial18 != '-' ){
					?>
							<td> <?php echo $DMHeat18; ?> </td>
					<?php		
						}

						if ( $DMTrial19 <= $DMtrial_no && $DMTrial19 != '-' ){
					?>
							<td> <?php echo $DMHeat19; ?> </td>
					<?php		
						}

						if ( $DMTrial20 <= $DMtrial_no && $DMTrial20 != '-' ){
					?>
							<td> <?php echo $DMHeat20; ?> </td>
					<?php		
						}
					?>
				</tr>
			<!-- ###################### Processability	 -->	
				<tr>
					<td> Processability </td>
					<?php
						if ( $DMTrial1 <= $DMtrial_no && !is_null($DMTrial1) ){
					?>
							<td> <?php echo $DMProcess1; ?> </td>
					<?php		
						}

						if ( $DMTrial2 <= $DMtrial_no && $DMTrial2 != '-' ){
					?>
							<td> <?php echo $DMProcess2; ?> </td>
					<?php		
						}

						if ( $DMTrial3 <= $DMtrial_no && $DMTrial3 != '-' ){
					?>
							<td> <?php echo $DMProcess3; ?> </td>
					<?php		
						}

						if ( $DMTrial4 <= $DMtrial_no && $DMTrial4 != '-' ){
					?>
							<td> <?php echo $DMProcess4; ?> </td>
					<?php		
						}

						if ( $DMTrial5 <= $DMtrial_no && $DMTrial5 != '-' ){
					?>
							<td> <?php echo $DMProcess5; ?> </td>
					<?php		
						}

						if ( $DMTrial6 <= $DMtrial_no && $DMTrial6 != '-' ){
					?>
							<td> <?php echo $DMProcess6; ?> </td>
					<?php		
						}

						if ( $DMTrial7 <= $DMtrial_no && $DMTrial7 != '-' ){
					?>
							<td> <?php echo $DMProcess7; ?> </td>
					<?php		
						}

						if ( $DMTrial8 <= $DMtrial_no && $DMTrial8 != '-' ){
					?>
							<td> <?php echo $DMProcess8; ?> </td>
					<?php		
						}

						if ( $DMTrial9 <= $DMtrial_no && $DMTrial9 != '-' ){
					?>
							<td> <?php echo $DMProcess9; ?> </td>
					<?php		
						}

						if ( $DMTrial10 <= $DMtrial_no && $DMTrial10 != '-' ){
					?>
							<td> <?php echo $DMProcess10; ?> </td>
					<?php		
						}

						if ( $DMTrial11 <= $DMtrial_no && $DMTrial11 != '-' ){
					?>
							<td> <?php echo $DMProcess11; ?> </td>
					<?php		
						}

						if ( $DMTrial12 <= $DMtrial_no && $DMTrial12 != '-' ){
					?>
							<td> <?php echo $DMProcess12; ?> </td>
					<?php		
						}

						if ( $DMTrial13 <= $DMtrial_no && $DMTrial13 != '-' ){
					?>
							<td> <?php echo $DMProcess13; ?> </td>
					<?php		
						}

						if ( $DMTrial14 <= $DMtrial_no && $DMTrial14 != '-' ){
					?>
							<td> <?php echo $DMProcess14; ?> </td>
					<?php		
						}

						if ( $DMTrial15 <= $DMtrial_no && $DMTrial15 != '-' ){
					?>
							<td> <?php echo $DMProcess15; ?> </td>
					<?php		
						}

						if ( $DMTrial16 <= $DMtrial_no && $DMTrial16 != '-' ){
					?>
							<td> <?php echo $DMProcess16; ?> </td>
					<?php		
						}

						if ( $DMTrial17 <= $DMtrial_no && $DMTrial17 != '-' ){
					?>
							<td> <?php echo $DMProcess17; ?> </td>
					<?php		
						}

						if ( $DMTrial18 <= $DMtrial_no && $DMTrial18 != '-' ){
					?>
							<td> <?php echo $DMProcess18; ?> </td>
					<?php		
						}

						if ( $DMTrial19 <= $DMtrial_no && $DMTrial19 != '-' ){
					?>
							<td> <?php echo $DMProcess19; ?> </td>
					<?php		
						}

						if ( $DMTrial20 <= $DMtrial_no && $DMTrial20 != '-' ){
					?>
							<td> <?php echo $DMProcess20; ?> </td>
					<?php		
						}
					?>
				</tr>
			</table>
		<?php
				}
				$db->next_result();
				$resultDM->close();
			}

	require("database_close.php");
?>