
		<tr>
			<th> Date Transferred </th>
			<th> MIRS# </th>
			<th colspan="2"> Bag# </th>
			<th> TOS (kgs) </th>
			<th> Underpack (kgs) </th>
		</tr>
<?php

$FGID = intval($_GET['FGID']);
$sltIssuedFormula = intval($_GET['sltIssuedFormula']);
$LotNumbersImplode = strval($_GET['LotNumbersImplode']);

$LotNumberExplode = explode(',',$LotNumbersImplode);
$LotNumberUnique = array_unique($LotNumberExplode);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_additional_output.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$TotalBagQty = 0;
		$TotalUnderpackQty = 0;

		foreach ($LotNumberUnique as $key => $LotNumber) {
			$qry = mysqli_prepare($db, "CALL sp_Output_Add_Details_Dropdown(?, ?, ?)");
			mysqli_stmt_bind_param($qry, 'sii', $LotNumber, $FGID, $sltIssuedFormula);
			$qry->execute();
			$result = mysqli_stmt_get_result($qry);
			$processError = mysqli_error($db);
		
			if ($processError){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_additional_output.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{	
				while($row = mysqli_fetch_assoc($result))
				{ 
					$date_transferred = $row["TempTransDate"];
					$tos_number = $row["TempRefNumber"];
					$start_bag_number = $row["TempStartBag"];
					$end_bag_number = $row["TempEndBag"];
					$underpack = $row["TempQuantity"];
					$total_underpack = $row["TotalUnderpack"];
					$bag_quantity = $row["TotalQty"];
					$quantity_per_tos = $row["TempQtyPerTOS"];
?>	
					<tr align="center">
						<td class="border_right border_bottom"> <?php echo $date_transferred;?> </td>
						<td class="border_right border_bottom"> <?php echo $tos_number;?> </td>
						<td class="border_right border_bottom"> <?php echo $start_bag_number;?> </td>
						<td class="border_right border_bottom"> <?php echo $end_bag_number;?> </td>
						<td class="border_right border_bottom"> <?php echo $quantity_per_tos;?> </td>
						<td class="border_bottom"> <?php echo $underpack;?> </td>
					</tr>
<?php
				}
				$db->next_result();
				$result->close();
			}

			$TotalBagQty = $TotalBagQty + $bag_quantity;
			$TotalUnderpackQty = $TotalUnderpackQty + $total_underpack;
		}
?>		
					<tr align="center" class="bgTotal">
						<td colspan="4" align="right" class="border_bottom border_right"> <label class="constant_total"> TOTAL </label> </td>

						<td class="border_bottom border_right"> <label class="constant_total"> <?php echo $TotalBagQty;?> </label> </td>

						<td class="border_bottom"> 
							<label class="constant_total"> <?php echo $TotalUnderpackQty;?> </label> 
						</td>
					</tr>

					<tr align="center" class="bgTotal">
						<input type="hidden" id="hidTotalOutput" value="<?php echo $TotalBagQty;?> ">

						<td colspan="4" align="right" class="border_right"> <label class="constant_total"> GOOD OUTPUT (kgs) </label> </td>

						<td id="tdTotalOutput" class="border_right"> 
							<label class="constant_total"> <?php echo $TotalBagQty;?> </label> 
						</td>

						<input type="hidden" id="hidGoodOutput" value="<?php echo $TotalBagQty;?>">
						
						<td> 
							<label class="constant_total"> <?php echo $TotalUnderpackQty;?> </label> 
						</td>

						<input type="hidden" id="hidUnderpack" value="<?php echo $TotalUnderpackQty;?>">
					</tr>
					<!-- <tr class="thoutputborder">
						<td colspan="4" class="rightalign"> TOTAL </td>
						<td> <?php //echo $TotalBagQty;?> </td>
						<td> 
							<?php //echo $TotalUnderpackQty;?> 
						</td>
					</tr>
					<tr class="thoutputborder" style="font-style:italic; color:red;">
							<input type="hidden" id="hidTotalOutput" value="<?php //echo $TotalBagQty;?> ">
						<td colspan="4" class="rightalign"> GOOD OUTPUT (kgs) </td>
						<td id="tdTotalOutput"> 
							<?php //echo $TotalBagQty;?>
						</td>
							<input type="hidden" id="hidGoodOutput" value="<?php //echo $TotalBagQty;?>">
						<td> 
							<?php //echo $TotalUnderpackQty;?>
						</td>
							<input type="hidden" id="hidUnderpack" value="<?php //echo $TotalUnderpackQty;?>">
					</tr> -->
<?php
	}
	require("database_close.php");
?>