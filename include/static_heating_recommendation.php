<?php

	require("database_connect.php");

	$qrySH = mysqli_prepare($db, "CALL sp_BAT_Static_Heat_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qrySH, 'is', $TypeID, $Type);
	$qrySH->execute();
	$resultSH = mysqli_stmt_get_result($qrySH);
	$processErrorSH = mysqli_error($db);

	if ( !empty($processErrorSH) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>static_heating_recommendation.php'.'</td><td>'.$processErrorSH.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultSH)){
				$SHtrial_no = $row['SHtrial_no'];
				$SHFGName = $row['SHFGName'];

				$SHTrial1 = $row['SHTrial1']; $SHTrial2 = $row['SHTrial2']; $SHTrial3 = $row['SHTrial3']; $SHTrial4 = $row['SHTrial4']; $SHTrial5 = $row['SHTrial5'];
				$SHTrial6 = $row['SHTrial6']; $SHTrial7 = $row['SHTrial7']; $SHTrial8 = $row['SHTrial8']; $SHTrial9 = $row['SHTrial9']; $SHTrial10 = $row['SHTrial10'];
				$SHTrial11 = $row['SHTrial11']; $SHTrial12 = $row['SHTrial12']; $SHTrial13 = $row['SHTrial13']; $SHTrial14 = $row['SHTrial14']; $SHTrial15 = $row['SHTrial15'];
				$SHTrial16 = $row['SHTrial16']; $SHTrial17 = $row['SHTrial17']; $SHTrial18 = $row['SHTrial18']; $SHTrial19 = $row['SHTrial19']; $SHTrial20 = $row['SHTrial20'];

				$SHColor1 = $row['SHColor1']; $SHColor2 = $row['SHColor2']; $SHColor3 = $row['SHColor3']; $SHColor4 = $row['SHColor4']; $SHColor5 = $row['SHColor5'];
				$SHColor6 = $row['SHColor6']; $SHColor7 = $row['SHColor7']; $SHColor8 = $row['SHColor8']; $SHColor9 = $row['SHColor9']; $SHColor10 = $row['SHColor10'];
				$SHColor11 = $row['SHColor11']; $SHColor12 = $row['SHColor12']; $SHColor13 = $row['SHColor13']; $SHColor14 = $row['SHColor14']; $SHColor15 = $row['SHColor15'];
				$SHColor16 = $row['SHColor16']; $SHColor17 = $row['SHColor17']; $SHColor18 = $row['SHColor18']; $SHColor19 = $row['SHColor19']; $SHColor20 = $row['SHColor20'];

				$SHHeat1 = $row['SHHeat1']; $SHHeat2 = $row['SHHeat2']; $SHHeat3 = $row['SHHeat3']; $SHHeat4 = $row['SHHeat4']; $SHHeat5 = $row['SHHeat5'];
				$SHHeat6 = $row['SHHeat6']; $SHHeat7 = $row['SHHeat7']; $SHHeat8 = $row['SHHeat8']; $SHHeat9 = $row['SHHeat9']; $SHHeat10 = $row['SHHeat10'];
				$SHHeat11 = $row['SHHeat11']; $SHHeat12 = $row['SHHeat12']; $SHHeat13 = $row['SHHeat13']; $SHHeat14 = $row['SHHeat14']; $SHHeat15 = $row['SHHeat15'];
				$SHHeat16 = $row['SHHeat16']; $SHHeat17 = $row['SHHeat17']; $SHHeat18 = $row['SHHeat18']; $SHHeat19 = $row['SHHeat19']; $SHHeat20 = $row['SHHeat20'];
		?>
				<table class='results_child_tables_form'>
					<tr>
						<td colspan='<?php echo ($SHtrial_no+1); ?>'> <?php  echo $SHFGName; ?> </td>
					</tr>
					<tr>
						<th></th>
						<?php
							if ( $SHTrial1 <= $SHtrial_no &&$SHTrial1!= '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial1; ?> </th>
						<?php		
							}

							if ( $SHTrial2 <= $SHtrial_no && $SHTrial2 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial2; ?> </th>
						<?php		
							}

							if ( $SHTrial3 <= $SHtrial_no && $SHTrial3 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial3; ?> </th>
						<?php		
							}

							if ( $SHTrial4 <= $SHtrial_no && $SHTrial4 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial4; ?> </th>
						<?php		
							}

							if ( $SHTrial5 <= $SHtrial_no && $SHTrial5 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial5; ?> </th>
						<?php		
							}

							if ( $SHTrial6 <= $SHtrial_no && $SHTrial6 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial6; ?> </th>
						<?php		
							}

							if ( $SHTrial7 <= $SHtrial_no && $SHTrial7 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial7; ?> </th>
						<?php		
							}

							if ( $SHTrial8 <= $SHtrial_no && $SHTrial8 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial8; ?> </th>
						<?php		
							}

							if ( $SHTrial9 <= $SHtrial_no && $SHTrial9 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial9; ?> </th>
						<?php		
							}

							if ( $SHTrial10 <= $SHtrial_no && $SHTrial10 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial10; ?> </th>
						<?php		
							}

							if ( $SHTrial11 <= $SHtrial_no && $SHTrial11 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial11; ?> </th>
						<?php		
							}

							if ( $SHTrial12 <= $SHtrial_no && $SHTrial12 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial12; ?> </th>
						<?php		
							}

							if ( $SHTrial13 <= $SHtrial_no && $SHTrial13 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial13; ?> </th>
						<?php		
							}

							if ( $SHTrial14 <= $SHtrial_no && $SHTrial14 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial14; ?> </th>
						<?php		
							}

							if ( $SHTrial15 <= $SHtrial_no && $SHTrial15 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial15; ?> </th>
						<?php		
							}

							if ( $SHTrial16 <= $SHtrial_no && $SHTrial16 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial16; ?> </th>
						<?php		
							}

							if ( $SHTrial17 <= $SHtrial_no && $SHTrial17 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial17; ?> </th>
						<?php		
							}

							if ( $SHTrial18 <= $SHtrial_no && $SHTrial18 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial18; ?> </th>
						<?php		
							}

							if ( $SHTrial19 <= $SHtrial_no && $SHTrial19 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial19; ?> </th>
						<?php		
							}

							if ( $SHTrial20 <= $SHtrial_no && $SHTrial20 != '-' ){
						?>
								<th> <?php echo 'Trial '.$SHTrial20; ?> </th>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Color Conformance	 -->	
					<tr>
						<td> Color Conformance </td>
						<?php
							if ( $SHTrial1 <= $SHtrial_no && $SHTrial1 != '-' ){
						?>
								<td> <?php echo $SHColor1; ?> </td>
						<?php		
							}

							if ( $SHTrial2 <= $SHtrial_no && $SHTrial2 != '-' ){
						?>
								<td> <?php echo $SHColor2; ?> </td>
						<?php		
							}

							if ( $SHTrial3 <= $SHtrial_no && $SHTrial3 != '-' ){
						?>
								<td> <?php echo $SHColor3; ?> </td>
						<?php		
							}

							if ( $SHTrial4 <= $SHtrial_no && $SHTrial4 != '-' ){
						?>
								<td> <?php echo $SHColor4; ?> </td>
						<?php		
							}

							if ( $SHTrial5 <= $SHtrial_no && $SHTrial5 != '-' ){
						?>
								<td> <?php echo $SHColor5; ?> </td>
						<?php		
							}

							if ( $SHTrial6 <= $SHtrial_no && $SHTrial6 != '-' ){
						?>
								<td> <?php echo $SHColor6; ?> </td>
						<?php		
							}

							if ( $SHTrial7 <= $SHtrial_no && $SHTrial7 != '-' ){
						?>
								<td> <?php echo $SHColor7; ?> </td>
						<?php		
							}

							if ( $SHTrial8 <= $SHtrial_no && $SHTrial8 != '-' ){
						?>
								<td> <?php echo $SHColor8; ?> </td>
						<?php		
							}

							if ( $SHTrial9 <= $SHtrial_no && $SHTrial9 != '-' ){
						?>
								<td> <?php echo $SHColor9; ?> </td>
						<?php		
							}

							if ( $SHTrial10 <= $SHtrial_no && $SHTrial10 != '-' ){
						?>
								<td> <?php echo $SHColor10; ?> </td>
						<?php		
							}

							if ( $SHTrial11 <= $SHtrial_no && $SHTrial11 != '-' ){
						?>
								<td> <?php echo $SHColor11; ?> </td>
						<?php		
							}

							if ( $SHTrial12 <= $SHtrial_no && $SHTrial12 != '-' ){
						?>
								<td> <?php echo $SHColor12; ?> </td>
						<?php		
							}

							if ( $SHTrial13 <= $SHtrial_no && $SHTrial13 != '-' ){
						?>
								<td> <?php echo $SHColor13; ?> </td>
						<?php		
							}

							if ( $SHTrial14 <= $SHtrial_no && $SHTrial14 != '-' ){
						?>
								<td> <?php echo $SHColor14; ?> </td>
						<?php		
							}

							if ( $SHTrial15 <= $SHtrial_no && $SHTrial15 != '-' ){
						?>
								<td> <?php echo $SHColor15; ?> </td>
						<?php		
							}

							if ( $SHTrial16 <= $SHtrial_no && $SHTrial16 != '-' ){
						?>
								<td> <?php echo $SHColor16; ?> </td>
						<?php		
							}

							if ( $SHTrial17 <= $SHtrial_no && $SHTrial17 != '-' ){
						?>
								<td> <?php echo $SHColor17; ?> </td>
						<?php		
							}

							if ( $SHTrial18 <= $SHtrial_no && $SHTrial18 != '-' ){
						?>
								<td> <?php echo $SHColor18; ?> </td>
						<?php		
							}

							if ( $SHTrial19 <= $SHtrial_no && $SHTrial19 != '-' ){
						?>
								<td> <?php echo $SHColor19; ?> </td>
						<?php		
							}

							if ( $SHTrial20 <= $SHtrial_no && $SHTrial20 != '-' ){
						?>
								<td> <?php echo $SHColor20; ?> </td>
						<?php		
							}
						?>
					</tr>
				<!-- ###################### Heat Stability	 -->	
					<tr>
						<td> Heat Stability </td>
						<?php
							if ( $SHTrial1 <= $SHtrial_no && !is_null($SHTrial1) ){
						?>
								<td> <?php echo $SHHeat1; ?> </td>
						<?php		
							}

							if ( $SHTrial2 <= $SHtrial_no && $SHTrial2 != '-' ){
						?>
								<td> <?php echo $SHHeat2; ?> </td>
						<?php		
							}

							if ( $SHTrial3 <= $SHtrial_no && $SHTrial3 != '-' ){
						?>
								<td> <?php echo $SHHeat3; ?> </td>
						<?php		
							}

							if ( $SHTrial4 <= $SHtrial_no && $SHTrial4 != '-' ){
						?>
								<td> <?php echo $SHHeat4; ?> </td>
						<?php		
							}

							if ( $SHTrial5 <= $SHtrial_no && $SHTrial5 != '-' ){
						?>
								<td> <?php echo $SHHeat5; ?> </td>
						<?php		
							}

							if ( $SHTrial6 <= $SHtrial_no && $SHTrial6 != '-' ){
						?>
								<td> <?php echo $SHHeat6; ?> </td>
						<?php		
							}

							if ( $SHTrial7 <= $SHtrial_no && $SHTrial7 != '-' ){
						?>
								<td> <?php echo $SHHeat7; ?> </td>
						<?php		
							}

							if ( $SHTrial8 <= $SHtrial_no && $SHTrial8 != '-' ){
						?>
								<td> <?php echo $SHHeat8; ?> </td>
						<?php		
							}

							if ( $SHTrial9 <= $SHtrial_no && $SHTrial9 != '-' ){
						?>
								<td> <?php echo $SHHeat9; ?> </td>
						<?php		
							}

							if ( $SHTrial10 <= $SHtrial_no && $SHTrial10 != '-' ){
						?>
								<td> <?php echo $SHHeat10; ?> </td>
						<?php		
							}

							if ( $SHTrial11 <= $SHtrial_no && $SHTrial11 != '-' ){
						?>
								<td> <?php echo $SHHeat11; ?> </td>
						<?php		
							}

							if ( $SHTrial12 <= $SHtrial_no && $SHTrial12 != '-' ){
						?>
								<td> <?php echo $SHHeat12; ?> </td>
						<?php		
							}

							if ( $SHTrial13 <= $SHtrial_no && $SHTrial13 != '-' ){
						?>
								<td> <?php echo $SHHeat13; ?> </td>
						<?php		
							}

							if ( $SHTrial14 <= $SHtrial_no && $SHTrial14 != '-' ){
						?>
								<td> <?php echo $SHHeat14; ?> </td>
						<?php		
							}

							if ( $SHTrial15 <= $SHtrial_no && $SHTrial15 != '-' ){
						?>
								<td> <?php echo $SHHeat15; ?> </td>
						<?php		
							}

							if ( $SHTrial16 <= $SHtrial_no && $SHTrial16 != '-' ){
						?>
								<td> <?php echo $SHHeat16; ?> </td>
						<?php		
							}

							if ( $SHTrial17 <= $SHtrial_no && $SHTrial17 != '-' ){
						?>
								<td> <?php echo $SHHeat17; ?> </td>
						<?php		
							}

							if ( $SHTrial18 <= $SHtrial_no && $SHTrial18 != '-' ){
						?>
								<td> <?php echo $SHHeat18; ?> </td>
						<?php		
							}

							if ( $SHTrial19 <= $SHtrial_no && $SHTrial19 != '-' ){
						?>
								<td> <?php echo $SHHeat19; ?> </td>
						<?php		
							}

							if ( $SHTrial20 <= $SHtrial_no && $SHTrial20 != '-' ){
						?>
								<td> <?php echo $SHHeat20; ?> </td>
						<?php		
							}
						?>
					</tr>
				</table>
	<?php
			}
			$db->next_result();
			$resultSH->close();
		}

	require("database_close.php");
?>