<?php

$hidSalesID = intval($_GET['hidSalesID']);
$fg_id = intval($_GET['fg_id']);
$sltCustomer = intval($_GET['sltCustomer']);
$sltItemType = strval($_GET['sltItemType']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown_forecast_annual.php'.'</td><td>'.$error.' near line 13.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{	
		$qry = mysqli_prepare( $db, "CALL sp_Forecast_Annual_FG_Dropdown(?,?)" );
		mysqli_stmt_bind_param( $qry, 'si', $sltItemType, $sltCustomer );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>fg_dropdown_forecast_annual.php'.'</td><td>'.$processError.' near line 25.</td></tr>', 3, "errors.php");
			// header("location: error_message.html");
		}
		else
		{
			$i = 0;
			
			while($row = mysqli_fetch_assoc($result))
			{
				
				$count = count($row['FGID']);
				$FgId = $row['FGID'];
				$Fg = $row['FGItem'];
				$ItemType = $row['ItemType'];
	?>
				<tr valign="top">
					<td class="border_bottom">
						<input type='hidden' name='hidItemType[]' value='<?php echo $ItemType;?>'>
						<?php echo $ItemType;?>
					</td>
					<td class="border_bottom">
						<input type='hidden' name='hidForecastItemID[]' value="0">
						<input type='hidden' name='hidFGID[]' value="<?php echo $FgId;?>" >
						<?php echo $Fg;?>
					</td>
					<td class="border_bottom">
						<input type='text' name='txtPrice[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtJanuary[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtFebruary[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtMarch[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtApril[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtMay[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtJune[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtJuly[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtAugust[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtSeptember[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtOctober[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtNovember[]' value='' class="short_text_3">
					</td>
					<td class="border_bottom">
						<input type='text' name='txtDecember[]' value='' class="short_text_3">
					</td>
				</tr>
<?php
			}
			$db->next_result();
			$result->close();
		}
	}

	require("database_close.php");
?>