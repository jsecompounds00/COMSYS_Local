<?php

$department_id = intval($_GET['department_id']);
$division = strval($_GET['division']);
$attentionTo = intval($_GET['attentionTo']);


require("database_connect.php");

if(!empty($errno))
{
	$error = mysqli_connect_error();
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>user_dropdown.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}
else
{
	$qry = mysqli_prepare($db, "CALL sp_Ticket_Receiver_Dropdown(?, ?)");
	mysqli_stmt_bind_param($qry, 'is', $department_id, $division);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

	if ($processError){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>user_dropdown.php'.'</td><td>'.$processError.' near line 55.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		echo "<option value='0'></option>";

		while($row = mysqli_fetch_assoc($result)){
			$user_id = $row['user_id'];
			$User = $row['User'];
			if ( $user ){	
				if ( $user==$user_id )
					echo "<option value='".$user_id."' selected>".$User."</option>";
				else echo "<option value='".$user_id."'>".$User."</option>";
			}else{
				echo "<option value='".$user_id."'>".$User."</option>";
			}
		}
		

		$db->next_result();
		$result->close();
	}
}
require("database_close.php");
?> 