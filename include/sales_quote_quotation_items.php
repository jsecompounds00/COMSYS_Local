<style type="text/css">
	tr.sales_quote_items:nth-child(odd){
		background: #d9f3ff;
		line-height: 180%;
	}
</style>

<?php

$sltYear = strval($_GET['sltYear']);
$sltMonth = strval($_GET['sltMonth']);
$sltCustomer = intval($_GET['sltCustomer']);
$hidQuoteID = intval($_GET['hidQuoteID']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>sales_quote_quotation_items.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$YearMonth = $sltYear.$sltMonth;

		$qry = mysqli_prepare($db, "CALL sp_Product_Costing_Dropdown(?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'iii', $sltCustomer, $YearMonth, $hidQuoteID);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
		$row_count = mysqli_affected_rows($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>sales_quote_quotation_items.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
				$FGID = array();
				$FGItem = array();
				$FGCategory = array();
				$CostNoVATPREVIOUS = array();
				$CostNoVATCURRENT = array();
				$CostDifference = array();
				$LastMonthQuote = array();

			    $EditQuoteItemID = array();
				$EditQuoteDiffOrig = array();
				$EditCurrMonthQuoteOrig = array();
				$EditQuoteDiffRevised = array();
				$EditCurrMonthQuoteRevised = array();
			    $EditCreatedAt = array();
			    $EditCreatedID = array();
			while($row = mysqli_fetch_assoc($result))
			{
				$FGID[] = $row["FGID"];
				$FGItem[] = $row["FGItem"];
				$FGCategory[] = $row["FGCategory"];
				$CostNoVATPREVIOUS[] = $row["CostNoVATPREVIOUS"];
				$CostNoVATCURRENT[] = $row["CostNoVATCURRENT"];
				$CostDifference[] = $row["CostDifference"];
				$LastMonthQuote[] = $row["LastMonthQuote"];

				$EditQuoteItemID[] = $row["EditQuoteItemID"];
				$EditQuoteDiffOrig[] = $row["EditQuoteDiffOrig"];
				$EditCurrMonthQuoteOrig[] = $row["EditCurrMonthQuoteOrig"];
				$EditQuoteDiffRevised[] = $row["EditQuoteDiffRevised"];
				$EditCurrMonthQuoteRevised[] = $row["EditCurrMonthQuoteRevised"];
				$EditCreatedAt[] = $row["EditCreatedAt"];
				$EditCreatedID[] = $row["EditCreatedID"];

				$CurrForex = $row["CurrForex"];
				$PrevForex = $row["PrevForex"];
				$FGLocal = $row["FGLocal"];
			}
			if ( $row_count > 0 ){
				if ( $FGLocal  == "Export" ){
?>
					<tr>
						<td></td>
						<td align="center"><b> @ForEx <br> <?php echo $PrevForex; ?> </b></td>
						<td align="center"><b> @ForEx <br> <?php echo $CurrForex; ?> </b></td>
					</tr>
<?php
				}
			}

			foreach ($FGID as $FGID_key => $FGID_value) {
?>
				<tr class="sales_quote_items">
					<td> 
						<?php echo $FGItem[$FGID_key]; ?> 
						<input type="hidden" name="hidFGID[]" value="<?php echo $FGID_value; ?>">
						<input type="hidden" name="hidFGItem[]" value="<?php echo $FGItem[$FGID_key]; ?>">
					</td>
					<td align="right"> <?php echo $CostNoVATPREVIOUS[$FGID_key]; ?> </td>
					<td align="right"> <?php echo $CostNoVATCURRENT[$FGID_key]; ?> </td>
					<td align="right"> <?php echo $CostDifference[$FGID_key]; ?> </td>
					<td align="right"> 
						<?php echo $LastMonthQuote[$FGID_key]; ?> 
						<input type="hidden" name="hidLastMonthQuote[]" id="hidLastMonthQuote<?php echo $FGID_key; ?>" value="<?php echo ( is_null($LastMonthQuote[$FGID_key]) ? 0 : $LastMonthQuote[$FGID_key] ); ?>">
					</td>

					<td> 
						<input type="text" name="txtQuoteDiff[]" id="txtQuoteDiff<?php echo $FGID_key; ?>" value="<?php echo $EditQuoteDiffOrig[$FGID_key];?>" onkeyup="showCurrMonthQuote(<?php echo $FGID_key;?>), showCurrMonthQuoteRevised(<?php echo $FGID_key;?>)"> 
					</td>

					<!-- - - - -->
					<td align="right" id="tdCurrMonthQuote<?php echo $FGID_key; ?>">
						<?php echo $EditCurrMonthQuoteOrig[$FGID_key]; ?>	
					</td>
						<input type="hidden" name="hidCurrMonthQuote[]" id="hidCurrMonthQuote<?php echo $FGID_key; ?>" value="<?php echo $EditCurrMonthQuoteOrig[$FGID_key]; ?>" >
					<!-- - - - -->
					<td>
						<input type="text" name="txtQuoteDiffRevised[]" id="txtQuoteDiffRevised<?php echo $FGID_key; ?>" value="<?php echo $EditQuoteDiffRevised[$FGID_key]?>" onkeyup="showCurrMonthQuoteRevised(<?php echo $FGID_key;?>)">
					</td>

					<!-- - - - -->
					<td align="right" id="tdCurrMonthQuoteRevised<?php echo $FGID_key; ?>">
						<?php echo $EditCurrMonthQuoteRevised[$FGID_key]; ?>	
					</td>
						<input type="hidden" name="hidCurrMonthQuoteRevised[]" id="hidCurrMonthQuoteRevised<?php echo $FGID_key; ?>" value="<?php echo $EditCurrMonthQuoteRevised[$FGID_key]; ?>" readOnly>
					<!-- - - - -->
					<?php
						if($EditQuoteItemID[$FGID_key]){	
							echo "<input type='hidden' name='hidQuoteItemID[]' value='".$EditQuoteItemID[$FGID_key]."'>";
							echo "<input type='hidden' name='hidCreatedItemAt[]' value='".$EditCreatedAt[$FGID_key]."'>";
							echo "<input type='hidden' name='hidCreatedItemId[]' value='".$EditCreatedID[$FGID_key]."'>";
						}
						else{ 
							echo "<input type='hidden' name='hidQuoteItemID[]' value='0'>";
							echo "<input type='hidden' name='hidCreatedItemAt[]' value='".date('Y-m-d H:i:s')."'>";
							echo "<input type='hidden' name='hidCreatedItemId[]' value=''>";
						}
					?>
				</tr>
<?php
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>