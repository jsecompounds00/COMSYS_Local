<?php

	$id = intval($_GET['id']);
	$supply_value = intval($_GET['supply']);
	
	require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>supply_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare( $db, "CALL sp_Supplies_Dropdown(?)" );
		mysqli_stmt_bind_param( $qry, 'i', $id );
		$qry->execute();
		$result = mysqli_stmt_get_result( $qry );
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>supply_dropdown.php'.'</td><td>'.$processError.' near line 21.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "<option value=0></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$supplyId = $row['id'];
				$supply = $row['name'];
				$quantityAdd = $row['quantityAdd'];
				$quantitySub = $row['quantitySub'];
				$balance = number_format((float)($quantityAdd - $quantitySub), 2, '.', '');
				
				if ( $supply_value ){	
					if ( $supply_value==$supplyId )
					echo "<option value='".$supplyId.";".$balance."' selected>".$supply." - ".$balance." available</option>";
				else echo "<option value='".$supplyId.";".$balance."'>".$supply." - ".$balance." available</option>";
				}else{
					echo "<option value='".$supplyId.";".$balance."'>".$supply." - ".$balance." available</option>";
				}
				
				
			}
		}
	}

	$db->next_result();
	$result->close();
	require("database_close.php");
?> 