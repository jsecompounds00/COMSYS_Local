<?php

	require("database_connect.php");

	$qrySI = mysqli_prepare($db, "CALL sp_JRD_Noreplicate_Strand_Inspection_Query( ?, ?, ? )");
	mysqli_stmt_bind_param($qrySI, 'isi', $JRDNoReplicateID, $product_for_evaluation, $FGID);
	$qrySI->execute();
	$resultSI = mysqli_stmt_get_result($qrySI);
	$processErrorSI = mysqli_error($db);

	if ( !empty($processErrorSI) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_strand_inspection.php'.'</td><td>'.$processErrorSI.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultSI)){
			$fisheye_blister_evaluation = $row['fisheye_blister_evaluation'];
			$fisheye_blister_classification = $row['fisheye_blister_classification'];
			$pinhole_evaluation = $row['pinhole_evaluation'];
			$SIcolor_conform = $row['color_conform'];
			$porous = $row['porous'];
			$fc_present = $row['fc_present'];
			$overall_remarks = $row['overall_remarks'];
			$SIcreated_at = $row['created_at'];
			$SIcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultSI->close();
	}

?>
	
	<table class="results_child_tables_form">
		<col width="250"></col>
		<tr></tr>
		<tr>
			<td>Fish Eyes / Blisters:</td>
			<td>
				<select name='sltFisheyeEvaluation'>
					<?php
						$qry1 = "CALL sp_BAT_Evaluation_Fisheye_Pinhole_Dropdown()";
						$result1 = mysqli_query( $db, $qry1 );
						$processError1 = mysqli_error($db);

						if(!empty($processError1))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError1.' near line 27.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{ 
							while ( $row = mysqli_fetch_assoc( $result1 ) ){
								$evaluation_fisheye_id = $row['id'];
								$evaluation_fisheye = $row['evaluation'];
					?>
								<option value="<?php echo $evaluation_fisheye_id;?>" <?php echo ( $strand_inspect_id ? ( $fisheye_blister_evaluation == $evaluation_fisheye_id ? "selected" : "" ) : ( $initNRSIFisheyeEvaluation == $evaluation_fisheye_id ? "selected" : "" ) ); ?>><?php echo $evaluation_fisheye; ?></option>
					<?php
							}
							$db->next_result();
							$result1->close();
						}
					?>
				</select>
				<select name='sltFisheyeClass'>
					<?php
						$qry2 = "CALL sp_BAT_Class_Fisheye_Dropdown()";
						$result2 = mysqli_query( $db, $qry2 );
						$processError2 = mysqli_error($db);

						if(!empty($processError2))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError2.' near line 27.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{ 
							while ( $row = mysqli_fetch_assoc( $result2 ) ){
								$class_fisheye_id = $row['id'];
								$class_fisheye = $row['classification'];
					?>
								<option value='<?php echo $class_fisheye_id;?>' <?php echo ( $strand_inspect_id ? ( $fisheye_blister_classification == $class_fisheye_id ? "selected" : "" ) : ( $initNRSIFisheyeClass == $class_fisheye_id ? "selected" : "" ) ); ?>><?php echo $class_fisheye;?></option>
					<?php
							}
							$db->next_result();
							$result2->close();
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Pin Holes:</td>
			<td>
				<select name='sltPinholeEvaluation'>
					<?php
						$qry3 = "CALL sp_BAT_Evaluation_Fisheye_Pinhole_Dropdown()";
						$result3 = mysqli_query( $db, $qry3 );
						$processError3 = mysqli_error($db);

						if(!empty($processError3))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError3.' near line 27.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{ 
							while ( $row = mysqli_fetch_assoc( $result3 ) ){
								$evaluation_pinhole_id = $row['id'];
								$evaluation_pinhole = $row['evaluation'];
					?>
								<option value='<?php echo $evaluation_pinhole_id;?>' <?php echo ( $strand_inspect_id ? ( $pinhole_evaluation == $evaluation_pinhole_id ? "selected" : "" ) : ( $initNRSIPinholeEvaluation == $evaluation_pinhole_id ? "selected" : "" ) ); ?>><?php echo $evaluation_pinhole;?></option>
					<?php
							}
							$db->next_result();
							$result3->close();
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Color Conform:</td>
			<td>
				<input type='checkbox' name='chkColorConform' <?php echo ( $strand_inspect_id ? ( $SIcolor_conform ? "checked" : "" ) : ( $initNRSIColorConform ? "checked" : "" ) ); ?> >
			</td>
		</tr>
		<tr>
			<td>Porous:</td>
			<td>
				<input type='checkbox' name='chkPorous' <?php echo ( $strand_inspect_id ? ( $porous ? "checked" : "" ) : ( $initNRSIPorous ? "checked" : "" ) ); ?> >
			</td>
		</tr>
		<tr>
			<td>FC Present:</td>
			<td>
				<input type='checkbox' name='chkFCPresent' <?php echo ( $strand_inspect_id ? ( $fc_present ? "checked" : "" ) : ( $initNRSIFCPresent ? "checked" : "" ) ); ?> >
			</td>
		</tr>
		<tr>
			<td>Overall Evaluation Remarks:</td>
			<td>
				<select name='sltOverallRemarks'>
					<?php
						$qry4 = "CALL sp_BAT_Overall_Remarks_Dropdown()";
						$result4 = mysqli_query( $db, $qry4 );
						$processError4 = mysqli_error($db);

						if(!empty($processError4))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError4.' near line 47.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{ 
							while ( $row = mysqli_fetch_assoc( $result4 ) ){
								$overall_remarks_id = $row['id'];
								$overall_remarks = $row['remarks'];
					?>
								<option value='<?php echo $overall_remarks_id;?>' <?php echo ( $strand_inspect_id ? ( $overall_remarks == $overall_remarks_id ? "selected" : "" ) : "" ); ?>><?php echo $overall_remarks;?></option>
					<?php
							}
							$db->next_result();
							$result4->close();
						}
					?>
				</select>
			</td>
		</tr>
		<input type='hidden' name='hidSICreatedAt' value='<?php echo ( $strand_inspect_id ? $SIcreated_at : date('Y-m-d H:i:s') );?>'>
		<input type='hidden' name='hidSICreatedId' value='<?php echo ( $strand_inspect_id ? $SIcreated_id : 0 );?>'>
	</table>
<?php	 require("database_close.php"); ?>