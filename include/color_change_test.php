<?php

	require("database_connect.php");

	$qryCC = mysqli_prepare($db, "CALL sp_Batch_Ticket_Color_Change_Query( ? )");
	mysqli_stmt_bind_param($qryCC, 'i', $TrialID);
	$qryCC->execute();
	$resultCC = mysqli_stmt_get_result($qryCC);
	$processErrorCC = mysqli_error($db);

	if ( !empty($processErrorCC) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>static_heating.php'.'</td><td>'.$processErrorCC.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultCC)){
			$cc_color_conformance = $row['color_conformance'];
			$cc_heat_stability = $row['heat_stability'];
			$CCcreated_at = $row['created_at'];
			$CCcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultCC->close();
	}

?>
	
	<table class="results_child_tables_form">
		<col width="250"></col>
		<tr></tr>
		<input type='hidden' name='hidColorChangeID' value='<?php echo $color_change_id; ?>'>
		<tr>
			<td>Color Conformance:</td>
			<td>
				<input type='radio' name='radCCTColor' id='ECCTColor' value='Excellent' <?php echo ( $color_change_id ? ( $cc_color_conformance == "Excellent" ? "checked" : "" ) : ( $initCCTColor_BT == "Excellent" ? "checked" : "" ) );?>>
					<label for='ECCTColor'> Excellent </label>

				<input type='radio' name='radCCTColor' id='GCCTColor' value='Good' <?php echo ( $color_change_id ? ( $cc_color_conformance == "Good" ? "checked" : "" ) : ( $initCCTColor_BT == "Good" ? "checked" : "" ) );?>>
					<label for='GCCTColor'> Good </label>

				<input type='radio' name='radCCTColor' id='PCCTColor' value='Poor' <?php echo ( $color_change_id ? ( $cc_color_conformance == "Poor" ? "checked" : "" ) : ( $initCCTColor_BT == "Poor" ? "checked" : "" ) );?>>
					<label for='PCCTColor'> Poor </label>
			</td>
		</tr>
		<tr>
			<td>Heat Stability:</td>
			<td>
				<input type='radio' name='radCCTHeat' id='ECCTHeat' value='Excellent' <?php echo ( $color_change_id ? ( $cc_heat_stability == "Excellent" ? "checked" : "" ) : ( $initCCTHeat_BT == "Excellent" ? "checked" : "" ) );?>>
					<label for='ECCTHeat'> Excellent </label>

				<input type='radio' name='radCCTHeat' id='GCCTHeat' value='Good' <?php echo ( $color_change_id ? ( $cc_heat_stability == "Good" ? "checked" : "" ) : ( $initCCTHeat_BT == "Good" ? "checked" : "" ) );?>>
					<label for='GCCTHeat'> Good </label>

				<input type='radio' name='radCCTHeat' id='PCCTHeat' value='Poor' <?php echo ( $color_change_id ? ( $cc_heat_stability == "Poor" ? "checked" : "" ) : ( $initCCTHeat_BT == "Poor" ? "checked" : "" ) );?>>
					<label for='PCCTHeat'> Poor </label>
			</td>
		</tr>
		</tr>
	</table>

<?php			
	
	if ( $color_change_id ){
		echo "<input type='hidden' name='hidCCTCreatedAt' value='".$CCcreated_at."'>";
		echo "<input type='hidden' name='hidCCTCreatedId' value='".$CCcreated_id."'>";	
	}else{
		echo "<input type='hidden' name='hidCCTCreatedAt' value='".date('Y-m-d H:i:s')."'>";
		echo "<input type='hidden' name='hidCCTCreatedId' value=''>";	
	}

	require("database_close.php");
?>