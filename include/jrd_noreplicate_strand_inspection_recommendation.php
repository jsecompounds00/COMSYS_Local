<?php

	require("database_connect.php");

	$qrySI = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Strand_Inspection_Recommendation_Query( ?, ? )");
	mysqli_stmt_bind_param($qrySI, 'is', $TypeID, $Type);
	$qrySI->execute();
	$resultSI = mysqli_stmt_get_result($qrySI);
	$processErrorSI = mysqli_error($db);

?>
	
	<table class="results_child_tables_form">
		<?php

			if ( !empty($processErrorSI) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_strand_inspection_recommendation.php'.'</td><td>'.$processErrorSI.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultSI)){
					$SIClientsProduct = $row['SIClientsProduct'];
					$SICACCProduct = $row['SICACCProduct'];
					$SIClientsFisheyeBlister = $row['SIClientsFisheyeBlister']; 
					$SICACCFisheyeBlister = $row['SICACCFisheyeBlister']; 
					$SIClientsPinhole = $row['SIClientsPinhole']; 
					$SICACCPinhole = $row['SICACCPinhole']; 
					$SIClientsColorConform = $row['SIClientsColorConform']; 
					$SICACCColorConform = $row['SICACCColorConform']; 
					$SIClientsPorous = $row['SIClientsPorous']; 
					$SICACCPorous = $row['SICACCPorous']; 
					$SIClientsFCPresent = $row['SIClientsFCPresent']; 
					$SICACCFCPresent = $row['SICACCFCPresent']; 
					$SIClientsOverallRemarks = $row['SIClientsOverallRemarks']; 
					$SICACCOverallRemarks = $row['SICACCOverallRemarks']; 
					$SIJRDTestID = $row['SIJRDTestID']; 
					$SIJRDFGSoft = $row['SIJRDFGSoft']; 

		?>
					<tr>
						<th></th>
						<th> <?php echo $SIClientsProduct; ?> </th>
						<th> <?php echo $SICACCProduct; ?> </th>
					</tr>
			<!-- ###################### Fish Eyes / Blisters	 -->	
					<tr>
						<td> Fish Eyes / Blisters </td>
						<td> <?php echo $SIClientsFisheyeBlister; ?> </td>
						<td> <?php echo $SICACCFisheyeBlister; ?> </td>
					</tr>
				<!-- ###################### Pin Holes	 -->	
					<tr>
						<td> Pin Holes </td>
						<td> <?php echo $SIClientsPinhole; ?> </td>
						<td> <?php echo $SICACCPinhole; ?> </td>
					</tr>
				<!-- ###################### Color Conform	 -->	
					<tr>
						<td> Color Conform </td>
						<td> <?php echo $SIClientsColorConform; ?> </td>
						<td> <?php echo $SICACCColorConform; ?> </td>
					</tr>
				<!-- ###################### Porous	 -->	
					<tr>
						<td> Porous </td>
						<td> <?php echo $SIClientsPorous; ?> </td>
						<td> <?php echo $SICACCPorous; ?> </td>
					</tr>
				<!-- ###################### FC Present	 -->	
					<tr>
						<td> FC Present </td>
						<td> <?php echo $SIClientsFCPresent; ?> </td>
						<td> <?php echo $SICACCFCPresent; ?> </td>
					</tr>
				<!-- ###################### Overall Evaluation Remark	 -->	
					<tr>
						<td> Overall Evaluation Remark </td>
						<td> <?php echo $SIClientsOverallRemarks; ?> </td>
						<td> <?php echo $SICACCOverallRemarks; ?> </td>
					</tr>
		<?php
				}
				$db->next_result();
				$resultSI->close();
			}
		?>
	</table>
	<br>
<?php	

	require("database_close.php");
?>