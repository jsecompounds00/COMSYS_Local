<?php

	require("database_connect.php");

	$qryIT = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_Impact_Test_Query( ?, ?, ? )");
	mysqli_stmt_bind_param($qryIT, 'isi', $JRDNoReplicateID, $product_for_evaluation, $FGID);
	$qryIT->execute();
	$resultIT = mysqli_stmt_get_result($qryIT);
	$processErrorIT = mysqli_error($db);

	if ( !empty($processErrorIT) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>jrd_noreplicate_impact_test.php'.'</td><td>'.$processErrorIT.' near line 12.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		while($row = mysqli_fetch_assoc($resultIT)){
			$drop_weight = $row['drop_weight'];
			$drop_height = $row['drop_height'];
			$no_of_sample = $row['no_of_sample'];
			$no_of_passed = $row['no_of_passed'];
			$ITcreated_at = $row['created_at'];
			$ITcreated_id = $row['created_id'];
		}
		$db->next_result();
		$resultIT->close();
	}

?>
	
	<table class='results_child_tables_form'>
		<col width='250'></col>
		<tr></tr>
		<tr>
			<td>Weight of Drop (kg):</td>
			<td>
				<input type='text' name='txtDropWeight' value="<?php echo ( $impact_test_id ? $drop_weight : $initNRITDropWeight ); ?>">
			</td>
			<td>Height of Drop (m):</td>
			<td>
				<input type='text' name='txtDropHeight' value="<?php echo ( $impact_test_id ? $drop_height : $initNRITDropHeight ); ?>">
			</td>
		</tr>
		<tr>
			<td>No. of Samples:</td>
			<td>
				<input type='text' name='txtSamplesNo' value="<?php echo ( $impact_test_id ? $no_of_sample : $initNRITSamplesNo ); ?>">
			</td>
			<td>No. of Passed:</td>
			<td>
				<input type='text' name='txtPassedNo' value="<?php echo ( $impact_test_id ? $no_of_passed : $initNRITPassedNo ); ?>">
			</td>
		</tr>
		<input type='hidden' name='hidITCreatedAt' value='<?php echo ( $impact_test_id ? $ITcreated_at : date('Y-m-d H:i:s') );?>'>
		<input type='hidden' name='hidITCreatedId' value='<?php echo ( $impact_test_id ? $ITcreated_id : 0 );?>'>
	</table>
<?php	 require("database_close.php"); ?>