<?php

session_start();
ob_start();   

$batch_id = strval($_GET['batch_id']);
$computation_id = intval($_GET['computation_id']);
$fg_id = intval($_GET['fg_id']);

require("database_connect.php");
require("/init_unset_values/material_balance_init_value.php");

	$qryFG2 = mysqli_prepare($db, "CALL sp_Material_Balance_Query(?, ?)");
	mysqli_stmt_bind_param($qryFG2, 'ii', $fg_id, $computation_id);
	$qryFG2->execute();
	$resultFG2 = mysqli_stmt_get_result($qryFG2);
	$processErrorFG2 = mysqli_error($db);

	if ($processErrorFG2){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_output.php'.'</td><td>'.$processErrorFG2.' near line 20.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
			$E_MaterialBalanceItemID = array();
			$E_ProductionStart = array();
			$E_ProductionEnd = array();
			$E_ActualTimeConsumed = array();
			$E_LotNumber = array();
			$E_ProductionDate = array();
		while($row = mysqli_fetch_assoc($resultFG2)){
		
			$E_LotNumber[] = $row["E_LotNumber"];
			$E_ProductionDate[] = $row["E_ProductionDate"];
			$E_MaterialBalanceItemID[] = $row["E_MaterialBalanceItemID"];
		    $E_ProductionStart[] = $row["E_ProductionStart"];
		    $E_ProductionEnd[] = $row["E_ProductionEnd"];
		    $E_ActualTimeConsumed[] = $row["E_ActualTimeConsumed"];
		}
		$db->next_result();
		$resultFG2->close();
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_lot_proddate.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_Issued_Formula_Details_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 'i', $batch_id);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_lot_proddate.php'.'</td><td>'.$processError.' near line 20.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			while($row = mysqli_fetch_assoc($result))
			{
				$lot_number = $row["lot_number"];
				$production_date = $row["production_date"];
			}
				$db->next_result();
				$result->close();

				$count = ( $computation_id ? count($E_MaterialBalanceItemID) : 5 );
				for( $x = 0 ; $x < 5 ; $x++ ){ 
?>						<input type="hidden" name="counter" id="counter" value="<?php echo $count;?>">
					<tr>
						<input type="hidden" name="hidComputationItemID[]" value="<?php echo( $computation_id && $x <= ($count-1) ? $E_MaterialBalanceItemID[$x] : NULL );?>">
						<td>
							<label for="Edit<?php echo $x;?>"> Edit </label> <input type="checkbox" id="Edit<?php echo $x;?>" onchange="editLotProdDate(<?php echo $x;?>)"> 
						</td>

						<td>
							<input type="text" name="txtLotNumberI[]" id="txtLotNumberI<?php echo $x;?>" value="<?php echo( $computation_id ? ($x <= ($count-1) ? $E_LotNumber[$x] : NULL) : ($batch_id ? $lot_number : NULL) );?>" readonly onkeyup="showAddOutput()" onblur="showAddOutput()">
						</td>

						<td>
							<input type="text" name="txtProductionDateI[]" id="txtProductionDateI<?php echo $x;?>" value="<?php echo( $computation_id ? ($x <= ($count-1) ? $E_ProductionDate[$x] : NULL) : ($batch_id ? $production_date : NULL) );?>" readonly>
						</td>

						<td>
							<input type="text" name="txtProductionStart[]" id="txtProductionStart<?php echo $x;?>" value="<?php echo( $computation_id && $x <= ($count-1) ? $E_ProductionStart[$x] : $initMBCProductionStart[$x] );?>" onchange="showConsumedTime(<?php echo $x;?>), showMCRate(), showMCPerformance()" onblur="showConsumedTime(<?php echo $x;?>), showMCRate(), showMCPerformance()" onkeyup="showConsumedTime(<?php echo $x;?>), showMCRate(), showMCPerformance()">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtProductionStart<?php echo $x;?>')" style="cursor:pointer" name="picker" />
							<label class="instruction2"> <?php echo ( $x == 0 ? '*': "&nbsp;&nbsp;" );?> </label>
						</td>

						<td>
							<input type="text" name="txtProductionEnd[]" id="txtProductionEnd<?php echo $x;?>" value="<?php echo( $computation_id && $x <= ($count-1) ? $E_ProductionEnd[$x] : $initMBCProductionEnd[$x] );?>" onchange="showConsumedTime(<?php echo $x;?>), showMCRate(), showMCPerformance()" onblur="showConsumedTime(<?php echo $x;?>), showMCRate(), showMCPerformance()" onkeyup="showConsumedTime(<?php echo $x;?>), showMCRate(), showMCPerformance()">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtProductionEnd<?php echo $x;?>')" style="cursor:pointer" name="picker" />
							<label class="instruction2"> <?php echo ( $x == 0 ? '*': "&nbsp;&nbsp;" );?> </label>
						</td>

						<td id="tdTimeConsumed<?php echo $x;?>">
							<label class="constant_total"> <?php echo( $computation_id && $x <= ($count-1) ? $E_ActualTimeConsumed[$x] : $initMBCTimeConsumed[$x] );?> </label>
						</td>
						
						<input type="hidden" name="hidTimeConsumed[]" id="hidTimeConsumed<?php echo $x;?>" value="<?php echo( $computation_id && $x <= ($count-1) ? $E_ActualTimeConsumed[$x] : $initMBCTimeConsumed[$x] );?>">
					</tr>
<?php 
				} 
		}
	}
	require("/init_unset_values/material_balance_unset_value.php");
	require("database_close.php");
?>