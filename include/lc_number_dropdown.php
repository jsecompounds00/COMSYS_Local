<?php

$sltCustomer = strval($_GET['sltCustomer']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>lc_number_dropdown.php'.'</td><td>'.$error.' near line 10.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_Credit_Terms_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 'i', $sltCustomer);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>lc_number_dropdown.php'.'</td><td>'.$processError.' near line 22.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{
			echo "	<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{
				$id = $row['id'];
				$lc_number = $row['lc_number'];
				
				// if ($id == $lc_number){
				// 	echo "<option value=".$id." selected>".$lc_number."</option>";
				// }else {
					echo "<option value=".$id.">".$lc_number."</option>";
				// }
			}
			$db->next_result();
			$result->close();
		}
	}
	require("database_close.php");
?>