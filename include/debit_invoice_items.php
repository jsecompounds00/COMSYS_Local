<tr>
	<th>Invoice No.</th>
	<th>Amount</th>
	<th>Discount</th>
</tr>
<?php
	session_start();
	ob_start();

	require("database_connect.php");
	require("/init_unset_values/debit_invoice_init_value.php");

	$debitId = intval($_GET["hidDebitId"]);

	if ( $debitId ){

		$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_Query( ? )");
		mysqli_stmt_bind_param($qry, 'i', $debitId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);

		if ( !empty($processError) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_invoice.php'.'</td><td>'.$processError.' near line 12.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			$db_jacket_item_id = array();
			$db_invoice_number = array();
			$db_check_number = array();
			$db_amount = array();
			$db_discount = array();
			$db_bank_code_id = array();
			$db_bank_code = array();
			while($row = mysqli_fetch_assoc($result)){
				$db_jacket_id = $row['jacket_id'];
				$db_process_type = $row['process_type'];
				$db_customer_id = $row['customer_id'];
				$db_transaction_type = $row['transaction_type'];
				$db_invoice_date = $row['invoice_date'];
				$db_check_returned_date = $row['check_returned_date'];
				$db_check_date = $row['check_date'];
				$db_created_at = $row['created_at'];
				$db_created_id = $row['created_id'];
				$db_name = $row['name'];

				$db_jacket_item_id[] = $row['jacket_item_id'];
				$db_invoice_number[] = $row['invoice_number'];
				$db_check_number[] = $row['check_number'];
				$db_amount[] = $row['amount'];
				$db_discount[] = $row['discount'];
				$db_bank_code_id[] = $row['bank_code_id'];
				$db_bank_code[] = $row['bank_code'];
			}
			$db->next_result();
			$result->close();
			$count = count( $db_jacket_item_id );
		}

		for ( $i = 0; $i < $count; $i++ ){
?>
			<tr valign='top'>
				<td id='tdInvoiceNumber<?php echo $i;?>'>	
					<input type='text' name='txtInvoiceNumber[]' id='txtInvoiceNumber<?php echo $i;?>' value="<?php echo $db_invoice_number[$i]?>">
					<input type='hidden' name='hidDebitItemID[]' id='hidDebitItemID<?php echo $i;?>' value="<?php echo $db_jacket_item_id[$i]?>">
				</td>
				<td id='tdAmount<?php echo $i;?>'>	
					<input type='text' name='txtAmount[]' id='txtAmount<?php echo $i;?>' value="<?php echo $db_amount[$i]?>">
				</td>
				<td id='tdDiscount<?php echo $i;?>'>	
					<input type='text' name='txtDiscount[]' id='txtDiscount<?php echo $i;?>' value="<?php echo $db_discount[$i]?>">
				</td>
			</tr>
<?php
		}
	}else{
		for ( $i = 0; $i < 10; $i++ ){
?>
			<tr valign='top'>
				<td id='tdInvoiceNumber<?php echo $i;?>'>	
					<input type='text' name='txtInvoiceNumber[]' id='txtInvoiceNumber<?php echo $i;?>' value="<?php echo $initCJDIInvoiceNumber[$i]; ?>">
					<input type='hidden' name='hidDebitItemID[]' id='hidDebitItemID<?php echo $i;?>'>
				</td>
				<td id='tdAmount<?php echo $i;?>'>	
					<input type='text' name='txtAmount[]' id='txtAmount<?php echo $i;?>' value="<?php echo $initCJDIAmount[$i]; ?>">
				</td>
				<td id='tdDiscount<?php echo $i;?>'>	
					<input type='text' name='txtDiscount[]' id='txtDiscount<?php echo $i;?>' value="<?php echo $initCJDIDiscount[$i]; ?>">
				</td>
			</tr>
<?php
		}
	}

	require("/init_unset_values/debit_invoice_unset_value.php");
?>