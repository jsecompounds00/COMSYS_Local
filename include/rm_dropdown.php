<?php
$rm = strval($_GET['rm']);
// $id = intval($_GET['id']);

require("database_connect.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>rm_dropdown.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$qry = mysqli_prepare($db, "CALL sp_RM_Dropdown(?)");
		mysqli_stmt_bind_param($qry, 's', $rm);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);
		$processError = mysqli_error($db);
	
		if ($processError){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>rm_dropdown.php'.'</td><td>'.$processError.' near line 26.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{	
			echo "<option value='0'></option>";
			while($row = mysqli_fetch_assoc($result))
			{ 
				$RMID = $row['RMID'];
				$RM = $row['RM'];
				
				echo "<option value='".$RMID."'>".$RM."</option>";
				
			}
			$db->next_result();
			$result->close();
		}
	}

	require("database_close.php");
?>