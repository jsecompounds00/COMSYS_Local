<?php
	session_start(); 
	require("include/constant.php");
	require("include/database_connect.php");
	ob_start();

//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if ( $errno ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>header.php'.'</td><td>'.$error.' near line 17.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		if(!isset($_SESSION['SESS_USER_NAME']) || (trim($_SESSION['SESS_USER_NAME']) == '')) 
		{
			header("location: login.php");
			exit();
		} else{
			$session_roleId = $_SESSION['SESS_ROLE_ID'];
			$session_roleId  = array();
			$session_roleId = explode(',',$_SESSION["SESS_ROLE_ID"]); //user permission

				foreach ($session_roleId as $key => $roleId) {

					// $qryR = "CALL sp_Role_Query('$roleId')";
					// $resultR = mysqli_query($db, $qryR);

					$qryR = mysqli_prepare($db, "CALL sp_Role_Query(?)");
					mysqli_stmt_bind_param($qryR, 'i', $roleId);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR);
					$processError = mysqli_error($db);

					if ( !empty($processError) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>header.php'.'</td><td>'.$processError.' near line 41.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						$row = mysqli_fetch_assoc($resultR);
						$permitID[] = $row['perm_id'];
					}

					$db->next_result();
					$resultR->close(); 
				}
				$permit = '0'.implode(',',$permitID);
			
			$session_Permit  = array();
			$session_Permit = explode(',',$permit); //user permission

	?>
	<link rel="icon" type="image/png" href="images/favicon.ico">
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="css/navigation.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<div id="heading">
		<a href="comsys.php" id="head">C O M S Y S</a>
		<div class="example">
    <div class="menu">
        <span class='nav'>
            <ul id="nav">
                <li><a href="#">MANAGE</a>
                    <div class="subs">
                        <div>
                            <ul><li><ul>
                                <?php $init = "%";
								//	PG_SUPPLY_TYPE_HOME
									if(array_search(1, $session_Permit)){
										echo "<li><a href='".PG_SUPPLY_TYPE_HOME."'>Supply Type</a></li>";
										$_SESSION['supply_type'] = true;
									}else{
										unset($_SESSION['supply_type']);
									}
								//	PG_UOM_HOME
									if(array_search(7, $session_Permit)){
										echo "<li><a href='".PG_UOM_HOME."'>UOM</a></li>";
										$_SESSION['uom'] = true;
									}else{
										unset($_SESSION['uom']);
									}
								//	PG_USER_HOME
									if(array_search(11, $session_Permit)){
										echo "<li><a href='".PG_USER_HOME."'>User</a>";
										$_SESSION['user'] = true;
									}else{
										unset($_SESSION['user']);
										if(array_search(12, $session_Permit) || array_search(16, $session_Permit) || array_search(17, $session_Permit))
										{
											echo "<li><a href='#'>User</a>";
										}
									}
								// 	PG_ROLE_HOME
									if(array_search(12, $session_Permit)){
										echo "  	  <li><a href='".PG_ROLE_HOME."'>Role</a></li>";
										$_SESSION['role'] = true;
									}else{
										unset($_SESSION['role']);
									}
								//	PG_DEPARTMENT_HOME
									if(array_search(16, $session_Permit)){
										echo "  	  <li><a href='".PG_DEPARTMENT_HOME."'>Department</a></li>";
										$_SESSION['department'] = true;
									}else{
										unset($_SESSION['department']);
									}
								// 	Permissions
									if(array_search(17, $session_Permit)){
										echo " 		  <li><a href='".PG_PERMISSION_HOME."'>Permissions</a></li>";
										$_SESSION['permission'] = true;
									}else{
										unset($_SESSION['permission']);
									}
											
									echo "</li>";
								//	PG_SUPPLIER_HOME
									if(array_search(13, $session_Permit)){
										echo "<li><a href='".PG_SUPPLIER_HOME."'>Supplier</a></li>";
										$_SESSION['supplier'] = true;
									}else{
										unset($_SESSION['supplier']);
									}
								//	PG_CUSTOMER_HOME
									if(array_search(14, $session_Permit)){
										echo "<li><a href='".PG_CUSTOMER_HOME."'>Customer</a></li>";
										$_SESSION['customer'] = true;
									}else{
										unset($_SESSION['customer']);
									}
								//	Data Migration
									if(array_search(15, $session_Permit)){
										echo "<li><a href='".PG_MIGRATION_HOME."'>Data Migration</a></li>";
										$_SESSION['migration'] = true;
									}else{
										unset($_SESSION['migration']);
									}
								//	Supplementary Table
									// if(array_search(15, $session_Permit)){
										// echo "<li><a href='supplementary_tables.php?page=1&search=&qsone='>Supplementary Tables</a></li>";
									// 	$_SESSION['migration'] = true;
									// }else{
									// 	unset($_SESSION['migration']);
									// }
								//	Error Viewing
									if(array_search(31, $session_Permit)){
										echo "<li><a href='errors.php'>View Errors</a></li>";
										$_SESSION['error'] = true;
									}else{
										unset($_SESSION['error']);
									}
								?>
                            </ul></li></ul>
                        </div>
                    </div>
                </li>
                <li><a href="#">COMPOUNDS</a>
                    <div class="subs">
                        <div class="wrp2">
                            <ul>
                            	<?php if( array_search(8, $session_Permit) || array_search(43, $session_Permit)
                            			|| array_search(59, $session_Permit) || array_search(44, $session_Permit)
                            			|| array_search(47, $session_Permit) || array_search(45, $session_Permit)
                            			|| array_search(46, $session_Permit) || array_search(76, $session_Permit)
                            			|| array_search(75, $session_Permit) ){?>
                                <li><h3>WPD</h3>
                                    <ul>
                                        <?php $init = "%";
										//	PG_SUPPLIES_HOME
											if(array_search(8, $session_Permit)){
												echo "<li><a href='".PG_SUPPLIES_HOME."'>Supplies</a></li>";
												$_SESSION['supplies'] = true;
											}else{
												unset($_SESSION['supplies']);
											}
										//	Raw Materials
											if(array_search(43, $session_Permit)){
												echo "<li><a href='raw_materials.php?page=1&name_text=&code_text=&type_text='>Raw Materials</a></li>";
												$_SESSION['rm'] = true;
											}else{
												unset($_SESSION['rm']);
											}
										//	Finished Goods
											if(array_search(59, $session_Permit)){
												echo "<li><a href='finished_goods.php?page=1&search=&qsone='>Finished Goods</a></li>";
												$_SESSION['fg'] = true;
											}else{
												unset($_SESSION['fg']);
											}
										//	LMR
											if(array_search(44, $session_Permit)){
												echo "<li><a href='lmr.php?page=1&search=&qsone='>LMR</a></li>";
												$_SESSION['lmr'] = true;
											}else{
												unset($_SESSION['lmr']);
											}
										//	Trucks
											if(array_search(47, $session_Permit)){
												echo "<li><a href='trucks.php?page=1&search=&qsone='>Trucks</a></li>";
												$_SESSION['trucks'] = true;
											}else{
												unset($_SESSION['trucks']);
											}
										//	Toll Points
											if(array_search(45, $session_Permit)){
												echo "<li><a href='toll_points.php?page=1&search=&qsone='>Toll Points</a></li>";
												$_SESSION['toll_points'] = true;
											}else{
												unset($_SESSION['toll_points']);
											}
										//	Toll Matrix
											if(array_search(46, $session_Permit)){
												echo "<li><a href='toll_matrix.php?page=1&search=&qsone='>Toll Matrix</a></li>";
												$_SESSION['toll_matrix'] = true;
											}else{
												unset($_SESSION['toll_matrix']);
											}
										//	Positions
											if(array_search(76, $session_Permit)){
												echo "<li><a href='positions.php?page=1&search=&qsone='>Positions</a></li>";
												$_SESSION['position'] = true;
											}else{
												unset($_SESSION['position']);
											}
										//	Labor
											if(array_search(75, $session_Permit)){
												echo "<li><a href='labor.php?page=1&search=&qsone='>Labor</a></li>";
												$_SESSION['labor'] = true;
											}else{
												unset($_SESSION['labor']);
											}
										?>
                                    </ul>
                                </li>
                                <?php } ?>
                            </ul>
                            <p class="sep"></p>
                            <ul>
                            	<?php if( array_search(48, $session_Permit) || array_search(49, $session_Permit)
                            			|| array_search(50, $session_Permit) || array_search(51, $session_Permit)
                            			|| array_search(77, $session_Permit) || array_search(78, $session_Permit)
                            			|| array_search(79, $session_Permit) || array_search(82, $session_Permit) ){?>
                                <li><h3>PAM</h3>
                                    <ul>
                                        <?php $init = "%";
										//	Equipment
											if(array_search(48, $session_Permit)){
												echo "<li><a href='".PG_EQUIPMENT_HOME."'>Equipment</a></li>";
												$_SESSION['equipment'] = true;
											}else{
												unset($_SESSION['equipment']);
											}	
										//	Location
											if(array_search(49, $session_Permit)){
												echo "<li><a href='".PG_LOCATION_HOME."'>Location</a></li>";
												$_SESSION['location'] = true;
											}else{
												unset($_SESSION['location']);
											}	
										//	Equipment Status
											if(array_search(50, $session_Permit)){
												echo "<li><a href='".PG_EQUIPMENT_STATUS_HOME."'>Equipment Status</a></li>";
												$_SESSION['e_status'] = true;
											}else{
												unset($_SESSION['e_status']);
											}	
										//	Equipment Measure
											if(array_search(51, $session_Permit)){
												echo "<li><a href='".PG_EQUIPMENT_MEASURE_HOME."'>Equipment Measure</a></li>";
												$_SESSION['e_measure'] = true;
											}else{
												unset($_SESSION['e_measure']);
											}		
										//	Engineering Items Records
											if(array_search(77, $session_Permit)){
												echo "<li><a href='engineering_item_records.php?page=1&search=&qsone='>Engineering Item Records</a></li>";
												$_SESSION['e_records'] = true;
											}else{
												unset($_SESSION['e_records']);
											}	
										//	Line Copmponents
											// if(array_search(78, $session_Permit)){
											// 	echo "<li><a href='line_components.php?page=1&search=&qsone='>Line Components</a></li>";
											// 	$_SESSION['components'] = true;
											// }else{
											// 	unset($_SESSION['components']);
											// }
										//	kw Rate
											if(array_search(82, $session_Permit)){
												echo "<li><a href='monthly_kw_rate.php?page=1&search=&qsone='>Monthly kW Rate</a></li>";
												$_SESSION['e_measure'] = true;
											}else{
												unset($_SESSION['e_measure']);
											}
										//	PAM Analysis
											if(array_search(79, $session_Permit)){
												echo "<li><a href='pam_analysis.php?page=1&search=&qsone='>PAM Analysis</a></li>";
												$_SESSION['pam_analysis'] = true;
											}else{
												unset($_SESSION['pam_analysis']);
											}	
										?>
                                    </ul>
                                </li>
                                <?php } ?>
                            </ul>
                            <p class="sep"></p>
                            <ul>
                            	<?php if( array_search(39, $session_Permit) || array_search(74, $session_Permit) ){?>
                                <li><h3>TECHNICAL</h3>
                                    <ul>
                                        <?php $init = "%";
										//	R & D ACTIVITIES
											if(array_search(39, $session_Permit)){
												echo "<li><a href='".PG_NRM_HOME."'>Batch Ticket</a></li>";
												$_SESSION['nrm'] = true;
											}else{
												unset($_SESSION['nrm']);
											} 

											$init = "%";
										//	Moisture Content
											if(array_search(74, $session_Permit)){
												echo "<li><a href='moisture_content.php?page=1&name_text=&code_text=&type_text='>Moisture Content</a></li>";
												$_SESSION['mc'] = true;
											}else{
												unset($_SESSION['mc']);
											}
										?>
                                    </ul>
                                </li>
                                <?php } ?>
                            </ul>
                            <p class="sep"></p>
                            <ul>
                            	<?php if( array_search(81, $session_Permit) ){?>
                                <li><h3>QMS</h3>
                                    <ul>
                                        <?php $init = "%";
										//	CPIAR Monitoring
											if(array_search(81, $session_Permit)){
												echo "<li><a href='cpiar_monitoring.php?page=1&search=&qsone='>CPIAR Monitoring</a></li>";
												$_SESSION['cpr'] = true;
											}else{
												unset($_SESSION['cpr']);
											}
										//	CCR Monitoring
											if(array_search(39, $session_Permit)){
												echo "<li><a href='ccr_monitoring.php?page=1&search=&qsone='>CCR Monitoring</a></li>";
											// 	$_SESSION['nrm'] = true;
											// }else{
											// 	unset($_SESSION['nrm']);
											}
										?>
                                    </ul>
                                </li>
                                <?php } ?>
                            </ul>
                            <p class="sep"></p>
                            <ul>
                            	<?php if( array_search(83, $session_Permit) || array_search(88, $session_Permit) ){?>
                                <li><h3>Sales</h3>
                                    <ul>
                                        <?php $init = "%";
										//	Sales Order
											if(array_search(83, $session_Permit)){
												echo "<li><a href='sales_order.php?page=1&so_no=&so_date=&so_type='>Sales Order</a></li>";
												$_SESSION['sales_order'] = true;
											}else{
												unset($_SESSION['sales_order']);
											}
										//	Sales Forecast
											if(array_search(88, $session_Permit)){
												echo "<li><a href='sales_forecast.php?page=1&search=&qsone='>Sales Forecast</a></li>";
												$_SESSION['sales_forecast'] = true;
											}else{
												unset($_SESSION['sales_forecast']);
											}
										?>
                                    </ul>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </li>
                <li><a href="#">CORPORATE</a>
					<div class='subs'>
						<div class='wrp2'>
							<ul>
								<?php if( array_search(34, $session_Permit) || array_search(52, $session_Permit) ){?>
								<li><h3>PURCHASING</h3></a>
									<ul>
										<?php $init = "%";
										//	PRE Monitoring
											if(array_search(34, $session_Permit)){
												echo "<li><a href='".PG_PRE_HOME."'>P.R.E.</a></li>";
												$_SESSION['pre_home'] = true;
											}else{
												unset($_SESSION['pre_home']);
											}
										//	PO Monitoring
											if(array_search(52, $session_Permit)){
												echo "<li><a href='".PG_PO_HOME."'>P.O.</a></li>";
												$_SESSION['po_home'] = true;
											}else{
												unset($_SESSION['po_home']);
											}	
										?>		
									</ul>
								</li>
                                <?php } ?>
							</ul>
                            <p class="sep"></p>
							<ul>
								<?php if( array_search(89, $session_Permit) || array_search(92, $session_Permit) ){?>
								<li><h3>ADMIN</h3></a>
									<ul>
										<?php $init = "%";
										//	Customer (Pipes)
											if(array_search(89, $session_Permit)){
												echo "<li><a href='customer_pipes.php?page=1&search=&qsone='>Customer (Pipes)</a></li>";
												$_SESSION['pre_home'] = true;
											}else{
												unset($_SESSION['pre_home']);
											}
										//	Property Type
											if(array_search(89, $session_Permit)){
												echo "<li><a href='property_type.php?page=1&search=&qsone='>Asset Type</a></li>";
												$_SESSION['property_type'] = true;
											}else{
												unset($_SESSION['property_type']);
											}
										//	Property Transfer
											if(array_search(92, $session_Permit)){
												echo "<li><a href='property_transfer.php?page=1&search=&qsone='>PTS</a></li>";
												$_SESSION['property_transfer'] = true;
											}else{
												unset($_SESSION['property_transfer']);
											}
										?>		
									</ul>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</li>
            </ul>
        </span>
    </div>
    	<br>
		<div class="login">
			Welcome, <?php 
				echo $_SESSION["SESS_FIRST_NAME"].' '. $_SESSION["SESS_LAST_NAME"];
			?> .
			<span class="logout"> <a href="logout.php">Logout</a> </span>
		</div>
</div>


	</div>
<?php } require("include/database_connect.php");
} ?> 
<script type="text/javascript">
jQuery(window).load(function() {
 
    $("#nav > li > a").click(function (e) { // binding onclick
        if ($(this).parent().hasClass('selected')) {
            $("#nav .selected div div").slideUp(100); // hiding popups
            $("#nav .selected").removeClass("selected");
        } else {
            $("#nav .selected div div").slideUp(100); // hiding popups
            $("#nav .selected").removeClass("selected");
 
            if ($(this).next(".subs").length) {
                $(this).parent().addClass("selected"); // display popup
                $(this).next(".subs").children().slideDown(200);
            }
        }
        e.stopPropagation();
    });
 
    $("body").click(function () { // binding onclick to body
        $("#nav .selected div div").slideUp(100); // hiding popups
        $("#nav .selected").removeClass("selected");
    });
 
});
</script>

