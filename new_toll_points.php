<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_toll_points.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$tollPointsId = $_GET['id'];

				if($tollPointsId)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_TollPoints_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $tollPointsId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_toll_points.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$area = $row['area'];
							$exit_code = $row['exit_code'];
							$exit_name = $row['exit_name'];
							$active = $row['active'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.toll_points";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_toll_points.php'.'</td><td>'.$processErrorPI.' near line 58.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI))
						{
							$id[] = $row['id'];
						} // end while loop

					} // end if-else processErrorPI

					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($tollPointsId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_toll_points.php</td><td>The user tries to edit a non-existing toll_points_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");

					} //end !in_array
					
					echo "<title>Toll Points - Edit</title>";

				} // end if tollPointsId
				else
				{
					
					echo "<title>Toll Points - Add</title>";

				} //end else tollPointsId

			} // if-else errno

		?>
	</head>
	<body>

		<form method='post' action='process_new_toll_points.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if ( $tollPointsId ){
					if( $_SESSION['edit_points'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['add_points'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $tollPointsId ? "Edit ".$exit_name : "New Toll Points" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}// end for loop

						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);

					} // end if isset session error message
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Area:</td>
						<td>
							<?php
								if ( $tollPointsId ){
							?>
									<input type='text' name='sltArea' value='<?php echo $area;?>' readonly>
							<?php
								}else{
							?>
									<select name="sltArea">
										<option value=''></option>	
										<option value='NLEX'>NLEX</option>
										<option value='SLEX'>SLEX</option>
										<option value='CAVITEX'>Cavitex</option>
										<option value='SCTEX'>SCTEX</option>
									</select>
							<?php 
								} 
							?>
						</td>
					</tr>
					<tr>
						<td>Exit Code:</td>
						<td>
							<input type='text' name='txtExitCode' value='<?php echo ( $tollPointsId ? $exit_code : "" );?>' <?php echo ( $tollPointsId ? "readonly" : "" );?>>
						</td>
					</tr>
					<tr>
						<td>Exit Name:</td>
						<td>
							<input type='text' name='txtExitName' value='<?php echo ( $tollPointsId ? $exit_name : ""  );?>'>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type='checkbox' name='chkActive' <?php echo ( $tollPointsId ? ( $active ? "checked" : "" ) : "" );?>>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<input type="submit" name="btnSaveTollPoints" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='toll_points.php?page=1&search=&qsone='">
							<input type='hidden' name='hidtollPointsId' value="<?php echo $tollPointsId;?>">
							<input type='hidden' name='hidCreatedAt' value="<?php echo ( $tollPointsId ? $createdAt : date('Y-m-d H:i:s') );?>">
							<input type='hidden' name='hidCreatedId' value="<?php echo ( $tollPointsId ? $createdId : $_SESSION["SESS_USER_ID"] );?>">
						</td>	
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>