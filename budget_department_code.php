<html>
	<head>
		<title>Department Code - Home</title>
		<?php 
			require("include/database_connect.php");
			
			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>

		<?php
			require ("/include/header.php");
			require("/include/init_unset_values/department_unset_value.php");

			// if( $_SESSION["department"] == false) 
			// {
			// 	$_SESSION["ERRMSG_ARR"] ="Access denied!";
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Department Code </h3> </span>

			<div class="search_box">
				<form method="get" action="budget_department_code.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<input type="hidden" name="qsone" value="<?php echo $qsone; ?>">
					<table class="search_tables_form">
						<tr>
							<td> Department: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_department_code.php"."</td><td>".$error." near line 41.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryD = mysqli_prepare($db, "CALL sp_Budget_Department_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryD, "s", $search);
					$qryD->execute();
					$resultD = mysqli_stmt_get_result($qryD);
					$total_results = mysqli_num_rows($resultD); //return number of rows of result

					$db->next_result();
					$resultD->close();
					
					$targetpage = "budget_department_code.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Budget_Department_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>budget_department_code.php"."</td><td>".$processError." near line 62.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) {
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan="8">
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
								<th> Code </th>
							    <th> Department </th>
							    <th> Active </th>
							    <th> Account Code </th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)){
									$code = $row["code"];
									$name = htmlspecialchars($row["name"]);
									$active = ( $row["active"] ? "Y" : "N" );
									$acct_code = $row["acct_code"];
							?>

									<tr>
									    <td> <?php echo $code;?> </td>
									    <td> <?php echo $name;?> </td>
									    <td> <?php echo $active;?> </td>
									    <td> <?php echo $acct_code;?> </td>
									</tr>
							<?php
								}
								$db->next_result();
								$result->close();
							?>
							<tr>
								<td colspan="8">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>