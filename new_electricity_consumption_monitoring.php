<html>
	<head>
		<?php
			require("/include/database_connect.php"); 

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_electricity_consumption_monitoring.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{				
				$fgid = $_GET['id'];

				$qryE = "SELECT id FROM electricity_consumption_monitoring WHERE finished_good_id = $fgid";
				$resultE = mysqli_query($db, $qryE);
				$processErrorE = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_electricity_consumption_monitoring.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{	
					$row = mysqli_fetch_assoc($resultE);
					$eid = $row['id'];
					$db->next_result();
					$resultE->close();
				}

				if ( $eid )
					$qry = mysqli_prepare($db, "CALL sp_Electricity_Consumption_Monitoring_Query_2(?)");
				else 
					$qry = mysqli_prepare($db, "CALL sp_Electricity_Consumption_Monitoring_Query(?)");
				mysqli_stmt_bind_param($qry, 'i', $fgid);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry); //return results of query
				$processError = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_electricity_consumption_monitoring.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{	
					$ElctItemMonitoringID = array();
					$machine_type = array();
					$machine_id = array();
					$machine_name = array();
					$trial_1 = array();
					$trial_2 = array();
					$trial_3 = array();
					$trial_1_date = array();
					$trial_2_date = array();
					$trial_3_date = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$ElctMonitoringID = $row['ElctMonitoringID'];
						$FGName = $row['FGName'];
						$FGLocal = $row['FGLocal'];
						$FGSoft = $row['FGSoft'];
						$createdAt = $row['created_at'];
						$createdId = $row['created_id'];

						$ElctItemMonitoringID[] = $row['ElctItemMonitoringID'];

						$machine_type[] = $row['machine_type'];

						$machine_id[] = $row['machine_id'];

						$machine_name[] = $row['machine_name'];

						$trial_1[] = $row['trial_1'];

						$trial_2[] = $row['trial_2'];

						$trial_3[] = $row['trial_3'];

						$trial_1_date[] = $row['trial_1_date'];

						$trial_2_date[] = $row['trial_2_date'];

						$trial_3_date[] = $row['trial_3_date'];

						$finished_good_category[] = $row['finished_good_category'];

					}
					$db->next_result();
					$result->close();
				}

				if ( $ElctMonitoringID ){
					echo "<title>Electricity Consumption Monitoring - Update</title>";
				}else{
					echo "<title>Electricity Consumption Monitoring - Create</title>";
				}

			}
		?>
	  	<script src="js/datetimepicker_css.js"></script>
	  	<script src="js/jscript.js"></script>
	</head>
	<body>

		<form method='post' action='process_new_electricity_consumption_monitoring.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if( $_SESSION['current_reading'] == false) 
				{
					$_SESSION['ERRMSG_ARR'] ='Access denied!';
					session_write_close();
					header("Location:comsys.php");
					exit();
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $ElctMonitoringID ? "Update " : "Create " );?> Electricity Consumption Monitoring </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';
						
						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form"> 
					<tr>
						<td> Product Type: </td>
						<td> <b> <?php echo ( $FGLocal == 0 ? 'Export' : 'Local' ); ?> </b> </td>
					</tr>
					<tr>
						<td> Product Category: </td>
						<td> <b> <?php echo ( $FGSoft == 0 ? 'Rigid' : 'Soft PVC' ); ?> </b> </td>
					</tr>
					<tr>
						<td> Product Item: </td>
						<td> <b> <?php echo $FGName; ?> </b> </td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th rowspan='2'>Line</th>
						<th colspan='3'>Trial 1</th>
						<th colspan='3'>Trial 2</th>
						<th colspan='3'>Trial 3</th>
					</tr>
					<tr>
						<th colspan="2">Date</th>
						<th>Current Reading</th>
						<th colspan="2">Date</th>
						<th>Current Reading</th>
						<th colspan="2">Date</th>
						<th>Current Reading</th>
					</tr>
					<?php
						foreach ($machine_id as $i => $MachineID) {
					?>
							<tr>
								<td>
									<?php echo $machine_name[$i];?>
									<input type='hidden' name='txtMachineName[]' value='<?php echo $machine_name[$i];?>'>
									<input type='hidden' name='hidElctItemMonitoringID[]' value='<?php echo $ElctItemMonitoringID[$i];?>'>
									<input type='hidden' name='txtMachineID[]' value='<?php echo $machine_id[$i];?>'>
									<input type='hidden' name='txtMachineType[]' value='<?php echo $machine_type[$i];?>'>
								</td>
								<?php
									if ( $FGSoft == 0 && $machine_type[$i] == 'E' && $machine_id[$i] != 1 ){
								?>
										<!-- ######## Trial 1 ######## -->
										<td colspan="2">
											<input type='text' class="short_text" name='txtTrialDate1[]' value='<?php echo $trial_1_date[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text" name='txtTrial1[]' value='<?php echo $trial_1[$i];?>' readOnly>
										</td>

										<!-- ######## Trial 2 ######## -->
										<td colspan="2">
											<input type='text' class="short_text" name='txtTrialDate2[]' value='<?php echo $trial_2_date[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text" name='txtTrial2[]' value='<?php echo $trial_2[$i];?>' readOnly>
										</td>

										<!-- ######## Trial 3 ######## -->
										<td colspan="2">
											<input type='text' class="short_text" name='txtTrialDate3[]' value='<?php echo $trial_3_date[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text" name='txtTrial3[]' value='<?php echo $trial_3[$i];?>' readOnly>
										</td>
								<?php
									}elseif ( $FGSoft == 1 && $machine_type[$i] == 'E' && $machine_id[$i] == 1 ){
								?>
										<!-- ######## Trial 1 ######## -->
										<td colspan="2">
											<input type='text' class="short_text" name='txtTrialDate1[]' value='<?php echo $trial_1_date[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text" name='txtTrial1[]' value='<?php echo $trial_1[$i];?>' readOnly>
										</td>

										<!-- ######## Trial 2 ######## -->
										<td colspan="2">
											<input type='text' class="short_text" name='txtTrialDate2[]' value='<?php echo $trial_2_date[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text" name='txtTrial2[]' value='<?php echo $trial_2[$i];?>' readOnly>
										</td>

										<!-- ######## Trial 3 ######## -->
										<td colspan="2">
											<input type='text' class="short_text" name='txtTrialDate3[]' value='<?php echo $trial_3_date[$i];?>' readOnly>
										</td>

										<td>
											<input type='text' class="short_text" name='txtTrial3[]' value='<?php echo $trial_3[$i];?>' readOnly>
										</td>

								<?php
									}else{
								?>
										<!-- ######## Trial 1 ######## -->
										<td>
											<input type='text' class="short_text" name='txtTrialDate1[]' id='txtTrialDate1<?php echo $i;?>' value='<?php echo $trial_1_date[$i];?>'>
										</td>

										<td>
											<img src="js/cal.gif" onclick="javascript:NewCssCal('txtTrialDate1<?php echo $i; ?>')" style="cursor:pointer" name="picker" />
										</td>
													
										<td>
											<input type='text' class="short_text" name='txtTrial1[]' value='<?php echo $trial_1[$i];?>'>
										</td>

										<!-- ####### Trial 2 ######## -->
										<td>
											<input type='text' class="short_text" name='txtTrialDate2[]' id='txtTrialDate2<?php echo $i;?>' value='<?php echo $trial_2_date[$i];?>'>
										</td>

										<td>
											<img src="js/cal.gif" onclick="javascript:NewCssCal('txtTrialDate2<?php echo $i; ?>')" style="cursor:pointer" name="picker" />
										</td>
							
										<td>
											<input type='text' class="short_text" name='txtTrial2[]' value='<?php echo $trial_2[$i];?>'>
										</td>

										<!-- ####### Trial 3 ######## -->
										<td>
											<input type='text' class="short_text" name='txtTrialDate3[]' id='txtTrialDate3<?php echo $i;?>' value='<?php echo $trial_3_date[$i];?>'>
										</td>

										<td>
											<img src="js/cal.gif" onclick="javascript:NewCssCal('txtTrialDate3<?php echo $i; ?>')" style="cursor:pointer" name="picker" />
										</td>
							
										<td>
											<input type='text' class="short_text" name='txtTrial3[]' value='<?php echo $trial_3[$i];?>'>
										</td>
								<?php
									}
								?>
							</tr>
					<?php
						}
					?>
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='pam_analysis.php?page=1&search=&qsone='">
							<input type='hidden' name='hidFinishedGoodID' value="<?php echo $fgid;?>">
							<input type='hidden' name='hidElctMonitoringID' value="<?php echo ( $ElctMonitoringID ? $ElctMonitoringID : 0 );?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $ElctMonitoringID ? $createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $ElctMonitoringID ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>