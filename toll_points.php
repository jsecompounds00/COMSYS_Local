<html>
	<head>
		<title>Toll Points - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : "");
		?>
	</head>
	<body>

		<?php
			require '/include/header.php';

			if( $_SESSION['toll_points'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Toll Points </h3> </span>

			<div class='search_box'>
		 		<form method='get' action='toll_points.php'>
		 			<input type='hidden' name='page' value="<?php echo $page;?>">
		 			<table class="search_tables_form">
		 				<tr>
		 					<td> Name: </td>
		 					<td> <input type='text' name='search' value="<?php echo $_GET['search'];?>"> </td>
		 					<td> Type: </td>
		 					<td>
		 						<select name='qsone' >
		 							<option value=''>ALL</option>
		 							<option value='NLEX' <?php echo ( $qsone == 'NLEX' ? "selected" : "" );?>>NLEX</option>
					 				<option value='SLEX' <?php echo ( $qsone == 'SLEX' ? "selected" : "" );?>>SLEX</option>
					 				<option value='CAVITEX' <?php echo ( $qsone == 'Cavitex' ? "selected" : "" );?>>Cavitex</option>
					 				<option value='SCTEX' <?php echo ( $qsone == 'SCTEX' ? "selected" : "" );?>>SCTEX</option>
		 						</select>
		 					</td>
		 					<td> <input type='submit' value='Search'> </td>
		 					<td>
		 						<?php
			 						if(array_search(57, $session_Permit)){
			 					?>
									 	<input type='button' name='btnAddTollPoints' value='Add Toll Points' onclick="location.href='new_toll_points.php?id=0'">
								<?php
										$_SESSION['add_points'] = true;
									}else{
										unset($_SESSION['add_points']);
									}
								?>
		 					</td>
		 				</tr>
		 			</table>
		 		</form>
		 	 </div>

		 	 <?php
		 	 		if(!empty($errno))
					{
						$error = mysqli_connect_error();
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>toll_points.php'.'</td><td>'.$error.' near line 55.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						$qryTP = mysqli_prepare($db, "CALL sp_TollPoints_Home(?, ?, NULL, NULL)");
						mysqli_stmt_bind_param($qryTP, 'ss', $qsone, $search);
						$qryTP->execute();
						$resultTP = mysqli_stmt_get_result($qryTP); //return results of query

						$total_results = mysqli_num_rows($resultTP); //return number of rows of result

						$db->next_result();
						$resultTP->close();

						$targetpage = "toll_points.php"; 	//your file name  (the name of this file)
						require("include/paginate.php");

						$qry = mysqli_prepare($db, "CALL sp_TollPoints_Home(?, ?, ?, ?)");
						mysqli_stmt_bind_param($qry, 'ssii', $qsone, $search, $start, $end);
						$qry->execute();
						$result = mysqli_stmt_get_result($qry); //return results of query
						$processError = mysqli_error($db);
						
						if(!empty($processError))
						{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>toll_points.php'.'</td><td>'.$processError.' near line 48.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							if( isset($_SESSION['SUCCESS'])) 
							{
								echo '<ul id="success">';
								echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
								echo '</ul>';
								unset($_SESSION['SUCCESS']);
							}
				?>

							<table class="home_pages">
								<tr>
									<td colspan='6'>
										<?php echo $pagination;?>
									</td>
								</tr>
								<tr>
									<th>Area</th>
									<th>Exit Name</th>
									<th>Exit Code</th>
									<th>Active?</th>
									<th></th>
								</tr>
								<?php 
									while($row = mysqli_fetch_assoc($result)) { 
								?>
									<tr>
										<td> <?php echo $row['area']; ?> </td>
										<td> <?php echo $row['exit_name']; ?> </td>
										<td> <?php echo $row['exit_code']; ?> </td>
										<td> <?php echo ( $row['active'] ? 'Y' : 'N' ); ?> </td>
										<td>
											<?php
												if(array_search(58, $session_Permit)){
											?>
													<input type='button' name='btnTP' value='Edit' onclick="location.href='new_toll_points.php?id=<?php echo $row['id'];?>'">
											<?php
													$_SESSION['edit_points'] = true;
												}else{
													unset($_SESSION['edit_points']);
												}
											?>
										</td>
									</tr>
								<?php
									} 
								?>
								<tr>
									<td colspan='6'>
										<?php echo $pagination;?>
									</td>
								</tr>
							</table>
				<?php
						}
					}
				?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>