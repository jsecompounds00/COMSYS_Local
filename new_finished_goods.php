<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dnew_finished_goods.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$FGID = $_GET['id'];

				if($FGID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Finished_Goods_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $FGID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dnew_finished_goods.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							// $fgID = $row['id'];
							$name = $row['name'];
							// $master_batch_type_id = $row['master_batch_type_id'];
							$master_batch_id = $row['master_batch_id'];
							$master_batch_dosage = $row['master_batch_dosage'];
							$CreatedAt = $row['created_at'];
							$CreatedID = $row['creator_id'];

						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.finished_goods";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dnew_finished_goods.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($FGID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_finished_goods.php</td><td>The user tries to edit a non-existing finished_good_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Finished Goods - Edit</title>";
				}
				else{

					echo "<title>Finished Goods - Add</title>";
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_finished_goods.php</td><td>The user tries to edit a non-existing finished_good_id.</td></tr>', 3, "errors.php");
					header("location: error_message.html");

				}
			}
		?>
	</head>
	<body>
		<form method='post' action='process_new_finished_goods.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/fg_master_batch_init_value.php");

				if ( $FGID ){
					if( $_SESSION['master_batch'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $FGID ? "Edit ".$name : "New Finished Goods" );?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Master Batch:</td>
						<td>
							<select name='sltMasterBatch' id='sltMasterBatch'>
								<?php
									$qryMB = 'CALL sp_Master_Batch_Dropdown()';
									$resultMB = mysqli_query($db, $qryMB);
									$processErrorMB = mysqli_error($db);

									if ( !empty($processErrorMB) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>dnew_finished_goods.php'.'</td><td>'.$processErrorMB.' near line 120.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
											echo "<option value='0'></option>";
										while($row = mysqli_fetch_assoc($resultMB)){
											$MBID = $row['MBID'];
											$MB = $row['MB'];
											$MBType = $row['MBType'];
											$MBCode = $row['MBCode'];

											if( $master_batch_id == $MBID )
												echo "<option value='".$MBID."' selected>".$MBType." ".$MBCode." : ".$MB."</option>";
											elseif( $initFGMBMasterBatch == $MBID )
												echo "<option value='".$MBID."' selected>".$MBType." ".$MBCode." : ".$MB."</option>";
											else
												echo "<option value='".$MBID."'>".$MBType." ".$MBCode." : ".$MB."</option>";
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Master Batch Dosage:</td>
						<td colspan='2'>
							<input type='text' name='txtMBDosage' value="<?php echo ( $master_batch_dosage ? $master_batch_dosage : $initFGMBMBDosage );?>">
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveFG" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='finished_goods.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidFGID' value="<?php echo $FGID;?>">
							<input type='hidden' name='hidCreatedId' value="<?php echo ( $FGID ? $CreatedID: $_SESSION["SESS_USER_ID"] );?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $FGID ? $CreatedAt : date("Y-m-d H:i:s") );?>">
						</td>
					</tr>
				</table>

			</div>

		</form>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>