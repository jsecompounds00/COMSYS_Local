<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_nrm.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$nrmID = $_GET['id'];

				if($nrmID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_NRM_Evaluation_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $nrmID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_nrm.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							$nrm_no = htmlspecialchars($row['nrm_no']);
							$nrm_date = htmlspecialchars($row['nrm_date']);
							$material_description = htmlspecialchars($row['material_description']);
							$supplier_id = $row['supplier_id'];
							$other_supplier = htmlspecialchars($row['other_supplier']);
							$classification = $row['classification'];
							$other_classification = htmlspecialchars($row['other_classification']);
							$sample_description = $row['sample_description'];
							$other_sample_description = htmlspecialchars($row['other_sample_description']);
							$msds = $row['msds'];
							$tds = $row['tds'];
							$remarks = htmlspecialchars($row['remarks']);
							// $remarks_array = explode("<br>", $remarks);
							$created_at = $row['created_at'];
							$created_id = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.nrm";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_nrm.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($nrmID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_nrm.php</td><td>The user tries to edit a non-existing nrm_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>NRM Request Evaluation - Edit</title>";
				}
				else{

					echo "<title>NRM Request Evaluation - New</title>";

				}
			}
		?>
		<script src="js/batch_ticket_js.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body onload='enableClassOthers(),enableSampleOthers(),enableOtherSupplier()'>

		<form method='post' action='process_new_nrm.php'>

			<?php
				require("/include/header.php");				
				require("/include/init_unset_values/nrm_init_value.php");
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $nrmID ? "Edit Request for NRM No. ".$nrm_no : "New Request for NRM Evaluation" );?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>NRM No.:</td>
						<td>
							<input type="text" name="txtNRMNo" value="<?php echo ( $nrmID ? $nrm_no : $initNRMNo );?>">
						</td>
						<td>NRM Date:</td>
						<td colspan='2'>
							<input type=text name="txtNRMDate" id="txtNRMDate" value="<?php echo ( $nrmID ? $nrm_date : $initNRMDate ); ?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtNRMDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr>
						<td>Material Description:</td>
						<td>
							<input type="text" name="txtMaterialDescription" value="<?php echo ( $nrmID ? $material_description : $initMaterialDescription ); ?>">
						</td>
						<td>Supplier:</td>
						<td colspan='2'>
							<select name='sltSupplier' id='sltSupplier'>
								<?php
									$qryS = "CALL sp_Supplier_RM_Dropdown()";
									$results = mysqli_query($db, $qryS);
									$processErrors = mysqli_error($db);
									if ( !empty($processErrors) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_nrm.php'.'</td><td>'.$processErrors.' near line 61.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
											$i = 0;
											echo "	<option value='0'></option>";
										while($row = mysqli_fetch_assoc($results)){
											if ( $nrmID ){
												if ( $supplier_id == $row['id'] ){
													echo "	<option value='".$row['id']."' id='optionSupplier$i' selected>".$row['name']."</option>";
												}else{
													echo "	<option value='".$row['id']."' id='optionSupplier$i'>".$row['name']."</option>";
												}
											}else{
												if ( $initSupplier == $row['id'] ){
													echo "	<option value='".$row['id']."' id='optionSupplier$i' selected>".$row['name']."</option>";
												}else{
													echo "	<option value='".$row['id']."' id='optionSupplier$i'>".$row['name']."</option>";
												}
											}
											$i++;
										}
									}
									$db->next_result();
									$results->close();
								?>
							</select>
						</td>
					</tr>
					<tr align="right">
						<td colspan="3">
							<input type='checkbox' name='chkOtherSupplier' id='otherSupplier' value='1' onchange='enableOtherSupplier()' <?php echo ( $nrmID ? ( $supplier_id == 0 ? "checked" : "" ) : ( $initTagOtherSupplier == 1 ? "checked" : "" ) ); ?>>
							<label for='otherSupplier'>Others</label> 
						</td>
						<td>
							<input type='text' name='txtOtherSupplier' id='txtOtherSupplier' value='<?php echo ( $nrmID ? $other_supplier : $initOtherSupplier ); ?>' readOnly>
						</td>
					</tr>
					 <!-- ############## CLASSIFICATION ##############  -->
					<tr>
						<td colspan='2'>
							<b>I. Classification</b>
						</td>
					</tr>
					<tr>
						<td>
							<input type='radio' name='radClass' id='radClassResin' value='Resin' onchange='enableClassOthers()' <?php echo ( $nrmID ? ( $classification == "Resin" ? "checked" : "" ) : ( $initClass == "Resin" ? "checked" : "" ) ); ?>>
							<label for='radClassResin'>Resin</label> 
						</td>
						<td>
							<input type='radio' name='radClass' id='radClassStabilizer' value='Stabilizer' onchange='enableClassOthers()' <?php echo ( $nrmID ? ( $classification == "Stabilizer" ? "checked" : "" ) : ( $initClass == "Stabilizer" ? "checked" : "" ) ); ?>>
							<label for='radClassStabilizer'>Stabilizer</label> 
						</td>
						<td>
							<input type='radio' name='radClass' id='radClassFillers' value='Fillers' onchange='enableClassOthers()' <?php echo ( $nrmID ? ( $classification == "Fillers" ? "checked" : "" ) : ( $initClass == "Fillers" ? "checked" : "" ) ); ?>>
							<label for='radClassFillers'>Fillers</label> 
						</td><td></td>
						<td>
							<input type='radio' name='radClass' id='radClassOthers' value='Others' onchange='enableClassOthers()' <?php echo ( $nrmID ? ( $classification == "Others" ? "checked" : "" ) : ( $initClass == "Others" ? "checked" : "" ) ); ?>>
							<label for='radClassOthers'>Others</label> 
						</td>
					</tr>
					<tr>
						<td>
							<input type='radio' name='radClass' id='radClassPlasticizer' value='Plasticizer' onchange='enableClassOthers()' <?php echo ( $nrmID ? ( $classification == "Plasticizer" ? "checked" : "" ) : ( $initClass == "Plasticizer" ? "checked" : "" ) ); ?>>
							<label for='radClassPlasticizer'>Plasticizer</label> 
						</td>
						<td>
							<input type='radio' name='radClass' id='radClassLubricant' value='Lubricant' onchange='enableClassOthers()' <?php echo ( $nrmID ? ( $classification == "Lubricant" ? "checked" : "" ) : ( $initClass == "Lubricant" ? "checked" : "" ) ); ?>>
							<label for='radClassLubricant'>Lubricant</label> 
						</td>
						<td colspan="2">
							<input type='radio' name='radClass' id='radClassColorant' value='Colorant' onchange='enableClassOthers()' <?php echo ( $nrmID ? ( $classification == "Colorant" ? "checked" : "" ) : ( $initClass == "Colorant" ? "checked" : "" ) ); ?>>
							<label for='radClassColorant'>Colorant</label> 
						<td>
							<input type='text' name='txtOtherClass' id='txtOtherClass' value='<?php echo ( $nrmID ? $other_classification : $initOtherClass ); ?>' readOnly>
						</td>
					</tr>
					<!-- ############## SAMPLE DESCRIPTION ##############  -->
					<tr><td colspan='2'><b>II. Sample Description</b></td></tr>
					<tr>
						<td>
							<input type='radio' name='radSample' id='radSamplePowder' value='Powder' onchange='enableSampleOthers()' <?php echo ( $nrmID ? ( $sample_description == "Powder" ? "checked" : "" ) : ( $initSample == "Powder" ? "checked" : "" ) ); ?>>
							<label for='radSamplePowder'>Powder</label> 
						</td>
						<td>
							<input type='radio' name='radSample' id='radSampleLiquid' value='Liquid' onchange='enableSampleOthers()' <?php echo ( $nrmID ? ( $sample_description == "Liquid" ? "checked" : "" ) : ( $initSample == "Liquid" ? "checked" : "" ) ); ?>>
							<label for='radSampleLiquid'>Liquid</label> 
						</td>
						<td>
							<input type='radio' name='radSample' id='radSampleOthers' value='Others' onchange='enableSampleOthers()' <?php echo ( $nrmID ? ( $sample_description == "Others" ? "checked" : "" ) : ( $initSample == "Others" ? "checked" : "" ) ); ?>>
							<label for='radSampleOthers'>Others</label> 
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type='radio' name='radSample' id='radSamplePellet' value='Pellet' onchange='enableSampleOthers()' <?php echo ( $nrmID ? ( $sample_description == "Pellet" ? "checked" : "" ) : ( $initSample == "Pellet" ? "checked" : "" ) ); ?>>
							<label for='radSamplePellet'>Pellet</label> 
						<td colspan='2'>
							<input type='text' name='txtOtherSample' id='txtOtherSample' value='<?php echo ( $nrmID ? $other_sample_description : $initOtherSample ); ?>' readOnly>
						</td>
					</tr>
					<!-- ############## IMPORTANT DOCUMENTS ############## -->
					<tr>
						<td colspan='2'>
							<b>III. Important Documents</b>
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							<input type='checkbox' name='chkMSDS' id='chkMSDS' <?php echo ( $nrmID ? ( $msds ? "checked" : "" ) : ( $initTagMSDS ? "checked" : "" ) ); ?>>
							<label for='chkMSDS'>Material Safety Data Sheet (MSDS)</label> 
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							<input type='checkbox' name='chkTDS' id='chkTDS' <?php echo ( $nrmID ? ( $tds ? "checked" : "" ) : ( $initTagTDS ? "checked" : "" ) ); ?>>
							<label for='chkTDS'>Technical Data Sheet (TDS)</label> 
						</td>
					</tr>
				</table>
				<table class="comments_buttons">
					<tr valign="top">
						<td>Remarks:</td>
						<td colspan='7'>
							<textarea name='txtRemarks'><?php
								if ( $nrmID ){
									echo $remarks;
								}else{
									echo $initRemarks;
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='nrm.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidNRMID' value='<?php echo $nrmID;?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $nrmID ? $created_id: 0 );?>'>
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $nrmID ? $created_at : date('Y-m-d H:i:s') );?>'>
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>