<html>
	<head>
		<title> Supplier - Home </title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>
		<?php
			require("/include/header.php");
				require("/include/init_unset_values/supplier_unset_value.php");

			if( $_SESSION["supplier"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Suppliers </h3> </span>

			<div class="search_box">
				<form method="get" action="supplier.php">
			 	 	<input type="hidden" name="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]); ?>">
			 		<input type="hidden" name="page" value="<?php echo $page; ?>">
			 		<table class="search_tables_form">
			 			<tr>
			 				<td> Name: </td>
			 				<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
			 				<td> <input type="submit" value="Search"> </td>
			 				<td>
			 					<?php
						 			if(array_search(25, $session_Permit)){
						 		?>
										<input type="button" name="btnAddSupplier" value="Add Supplier" onclick="location.href='<?php echo PG_NEW_SUPPLIER;?>0'">
								<?php
										$_SESSION["add_supplier"] = true;
									}else{
										unset($_SESSION["add_supplier"]);
									}
						 		?>
			 				</td>
			 			</tr>
			 		</table>
			 	</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>supplier.php'.'</td><td>'.$error.' near line 41.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryR = mysqli_prepare($db, "CALL sp_Supplier_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryR, "s", $search);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR); 

					$total_results = mysqli_num_rows($resultR); 
					
					$db->next_result();
					$resultR->close();

					$targetpage = "supplier.php"; 	
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Supplier_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>supplier.php"."</td><td>".$processError." near line 62.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>Name</th>
							    <th>Active</th>
							    <th>RM</th>
							    <th>FG</th>
							    <th>Supplies</th>
							    <th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)){
							?>

									<tr>
									    <td><?php echo $row["name"];?></td>
									    <td><?php echo ( $row["active"] ? "Y" : "N" );?></td>
									    <td><?php echo ( $row["rm"] ? "<img src='images/yes.png' height='20'>" : "" );?></td>
									    <td><?php echo ( $row["fg"] ? "<img src='images/yes.png' height='20'>" : "" );?></td>
									    <td><?php echo ( $row["supplies"] ? "<img src='images/yes.png' height='20'>" : "" );?></td>
									    <td>
											<?php
												if(array_search(26, $session_Permit)){
											?>
													<input type='button' name='btnEdit' value='EDIT' onclick="location.href='<?php echo PG_NEW_SUPPLIER.$row['id'];?>'">
											<?php
													$_SESSION['edit_supplier'] = true;
												}else{
													unset($_SESSION['edit_supplier']);
												}
											?>
										</td>
									</tr>
							<?php
								}
								$db->next_result();
								$result->close();
				?>
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>