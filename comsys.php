<html>
	<head>
		<title>COMSYS - Home</title>
	</head>
	<body>
		<form>
			<?php
				require '/include/header.php';
			?>
			<div class="wrapper">
				<br><br><br>
	            <?php
					if( isset($_SESSION['SUCCESS'])) 
					{
						echo '<ul id="success">';
						echo '	<li>'.$_SESSION['SUCCESS'].'</li>'; 
						echo '</ul>';
						unset($_SESSION['SUCCESS']);
					}

					if( isset($_SESSION['ERRMSG_ARR'])) 
					{
						echo '<ul class="err">';
						echo '	<li>'.$_SESSION['ERRMSG_ARR'].'</li>'; 
						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);
					}
				?>
	            <span> <h3> Home </h3> </span>
			</div>
		</form>
	</body>
	<footer>
	</footer>
</html>


