<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_trucks.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidTruckId = $_POST['hidTruckId'];
		$sltClass = clean($_POST['sltClass']);

		if($hidTruckId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:new_trucks.php?id=".$hidTruckId);
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Trucks_CRU( ?, ? )");
		mysqli_stmt_bind_param($qry, 'ii', $hidTruckId, $sltClass);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_trucks.php'.'</td><td>'.$processError.' near line 55.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidTruckId)
				$_SESSION['SUCCESS']  = 'Successfully updated trucks.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new trucks.';
			//echo $_SESSION['SUCCESS'];
			header("location: trucks.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		// $result->close(); 
		require("include/database_close.php");
	}
?>