<html>
	<head>
		<script src="js/datetimepicker_css.js"></script> 
		<script src="js/jscript.js"></script>
		<script src="js/calibration_validation_js.js"></script>

		<?php
			require("/include/database_connect.php");

			$search = ($_GET['search'] ? $_GET['search'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>schedule_history.php'.'</td><td>'.$error.' near line 8.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$equipmentId = $_GET['id'];

				if($equipmentId)
				{ 
					############ .............

					$qryPI = "SELECT id from comsys.equipment";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>schedule_history.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					if( !in_array($equipmentId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>schedule_history.php</td><td>The user tries to view a non-existing equipment_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					############ .............
				
					$qry = mysqli_prepare( $db, "CALL sp_Equipment_Query( ? )" );
					mysqli_stmt_bind_param( $qry, 'i', $equipmentId );
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>schedule_history.php'.'</td><td>'.$processError.' near line 27.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$equipmentId = $row['id'];
							$name = $row['name'];
						}
					}
					$db->next_result();
					$result->close();
				}
			}
		?>
		<title>History - Schedules</title>
	</head>
	<body>

		<?php
			require '/include/header.php';
			require '/include/init_value.php';
		?>

		<div class="wrapper">
			
			<span> <h2> Schedules for <?php echo $name;?> </h2> </span>

			<a class='back' href='<?php echo PG_EQUIPMENT_HOME;?>'><img src='images/back.png' height="20" name='txtBack'> Back</a>

			<div class="search_box">
				<form method='get' action='schedule_history.php'>
			 		<input type='hidden' name='page' value="<?php echo $page;?>">
			 		<input type='hidden' name='id' value="<?php echo $_GET['id'];?>">
			 		<table class="search_tables_form">
			 			<tr class="spacing2"></tr>
			 			<tr>
			 				<td> <input type='text' name='search' id='qsone' value="<?php echo $_GET['search'];?>"> </td>
			 				<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
			 				<td> <input type='submit' value='Search'> </td>
			 			</tr>
			 		</table>
			 	</form>
			</div>

			<?php
				$qryP = mysqli_prepare($db, "CALL sp_Equipment_Schedule_Query(?, ?, NULL, NULL)");
				mysqli_stmt_bind_param($qryP, 'is', $equipmentId, $search);
				$qryP->execute();
				$resultP = mysqli_stmt_get_result($qryP); //return results of query

				$total_results = mysqli_num_rows($resultP); //return number of rows of result
				
				$db->next_result();
				$resultP->close();
				
				$targetpage = "schedule_history.php"; 	//your file name  (the name of this file)
				require("include/paginate_sched_hist.php");

				$qry = mysqli_prepare($db, "CALL sp_Equipment_Schedule_Query(?, ?, ?, ?)");
				mysqli_stmt_bind_param($qry, 'isii', $equipmentId, $search, $start, $end);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry);

				$processError = mysqli_error($db);

				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>schedule_history.php'.'</td><td>'.$processError.' near line 27.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}elseif( isset($_SESSION['SUCCESS'])) {
						echo '<ul id="success">';

						echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 

						echo '</ul>';

						unset($_SESSION['SUCCESS']);
					}
			?>

					<form method='post' action='process_update_schedule.php'>
						<table class="home_pages">
							<tr>
								<td colspan='11'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
								<th>Schedule Type</th>
								<th width="100">Schedule</th>
								<th width="100" colspan="2">Date Conducted</th>
								<th>Conducted By</th>
								<th colspan="2">Status</th>
								<th>Remarks</th>
								<th colspan="2"></th>
							</tr>
							<?php
								$i=0;
								while($row = mysqli_fetch_assoc($result))
								{
									$schedule_type = $row['schedule_type'];
									$schedule = $row['schedule'];
									$name = $row['name'];
									$status = $row['status'];
									$schedule_id = $row['schedule_id'];
									$date_calibrated = $row['date_calibrated'];
									$date_validated = $row['date_validated'];
									$conducted_by = $row['conducted_by'];
									$remarks = $row['remarks'];
									$cancelled = $row['cancelled'];
									$cancelled_at = $row['cancelled_at'];
									$cancel_reason = $row['cancel_reason'];
									$created_at = $row['created_at'];
									$created_id = $row['created_id'];

									if ( $status == 1 ){
							?>
										<tr>
											<td> <?php echo $schedule_type;?></td>
											<td> <?php echo $schedule;?></td>
											<td colspan="2">
												<?php echo ( $schedule_type == "Calibration" ? $date_calibrated : ( $schedule_type == "Validation" ? $date_validated : "" ) );?>
											</td>
											<td> <?php echo $conducted_by;?></td>
											<td></td>
											<td>
												<?php echo ( $schedule_type == "Calibration" ? "Calibrated" : ( $schedule_type == "Validation" ? "Validated" : "" ) );?>
											</td>
											<td colspan='3'> <?php echo $remarks;?></td>
											</td>
										</tr>
								<?php
									}elseif ( $cancelled == 1 ){
								?>
										<tr>
											<td> <?php echo $schedule_type;?> </td>
											<td> <?php echo $schedule;?> </td>
											<td colspan='7'><b>Cancelled dated <?php echo $cancelled_at;?>:</b> <?php echo $cancel_reason;?> 
											</td>
										</tr>
								<?php
									}else{
								?>
										<tr valign="top">
											<input type='hidden' name='hidScheduleId[]' id='hidScheduleId<?php echo $i;?>' value='<?php echo $schedule_id;?>' disabled>
											<input type='hidden' name='hidEquipmentId' id='hidEquipmentId<?php echo $i;?>' value='<?php echo $equipmentId;?>' disabled>
											<input type='hidden' name='hidCreatedAt[]' id='hidCreatedAt<?php echo $i;?>' value='<?php echo $created_at;?>' disabled>
											<input type='hidden' name='hidCreatedId[]' id='hidCreatedId<?php echo $i;?>' value='<?php echo $created_id;?>' disabled>
											<td>
												<?php echo $schedule_type ;?>
												<input type='hidden' name='hidScheduleType[]' id='hidScheduleType<?php echo $i;?>' value='<?php echo $schedule_type;?>' disabled>
											</td>
											<td>
												<?php echo $schedule ;?>
												<input type='hidden' name='hidSchedule[]' id='hidSchedule<?php echo $i;?>' value='<?php echo $schedule;?>' disabled>
											</td>
											<td>
												<input type='text' name='txtDateConduct[]' id='txtDateConduct<?php echo $i;?>' disabled>
											</td>
											<td> 
												<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDateConduct<?php echo $i;?>')" style="cursor:pointer" name="picker" /> 
											</td>
											<td>
												<input type='text' name='txtConductBy[]' id='txtConductBy<?php echo $i;?>' disabled>
											</td>
											<td>
												<input type='checkbox' name='chkStatus[]' value='1' id='chkStatus<?php echo $i;?>' onchange='changeStatus()'>
												<input type='hidden' name='Status[]'>
											</td>
											<td>
												<label for="chkStatus<?php echo $i;?>">
													<?php echo ( $schedule_type == "Calibration" ? "Calibrated" : ( $schedule_type == "Validation" ? "Validated" : "" ) );?>
												</label>
											</td>
											<td>
												<textarea name='txtRemarks[]' id='txtRemarks<?php echo $i;?>' class="short_paragraph" disabled></textarea>
											</td>
											</td>
											<td>
												<input type='submit' name='btnUpdate[]' id='btnUpdate<?php echo $i;?>' value='Update' disabled>
											</td>
											<td>
												<input type='button' name='btnCancel[]' id='btnCancel<?php echo $i;?>' value='Cancel' onclick='cancelBox(<?php echo $schedule_id;?>,<?php echo $equipmentId;?>)'>
											</td>
										</tr>
							<?php
									} // end if-else $status == 1

									$i++;
								} // end while
								$db->next_result();
								$result->close();
							?>
							<tr>
								<td colspan='11'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
					</form>

			<?php
				}
			?>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>