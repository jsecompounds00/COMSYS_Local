<!-- 
	
</form>	
	<div>
		<?php 
			// require('/include/footer.php'); 
			// require("include/database_close.php");
		?>
	</div>
</div> -->





<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php'.'</td><td>'.$error.' near line 8.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$equipmentId = $_GET['id'];

				if($equipmentId)
				{ 				
					echo "<title>Equipment - Edit</title>";
					
					$qry = mysqli_prepare( $db, "CALL sp_Equipment_Query( ? )" );
					mysqli_stmt_bind_param( $qry, 'i', $equipmentId );
					$qry->execute();
					$result = mysqli_stmt_get_result( $qry );
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php'.'</td><td>'.$processError.' near line 27.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$code = $row['code'];
							$name = $row['name'];
							$brand_model = $row['brand_model'];
							$tolerance_percentage = $row['tolerance_percentage'];
							$tolerance_value = $row['tolerance_value'];
							$location_id = $row['location_id'];
							$equipment_status_id = $row['equipment_status_id'];
							$equipment_measure_id = $row['equipment_measure_id'];
							$input_range = $row['input_range'];
							$output_range = $row['output_range'];
							$frequency_of_validation = $row['frequency_of_validation'];
							$frequency_of_calibration = $row['frequency_of_calibration'];
							$comments = $row['comments'];
							$active = $row['active'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.equipment";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php'.'</td><td>'.$processErrorPI.' near line 58.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($equipmentId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php</td><td>The user tries to edit a non-existing equipment_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
				}
				else
				{				
					echo "<title>Equipment - Add</title>";
				}
			}
		?>
	</head>
	<body>

		<form method='post' action='process_new_equipment.php'>

			<?php 
				require ('/include/header.php');
				require ('/include/init_value.php');
			?>

			<div class="wrapper">
				
				<span> <h3> <?php echo ( $equipmentId ? "Edit ".$name : "New Equipment" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Code:</td>

						<td>
							<input type='text' name='txtCode' value='<?php echo ( $equipmentId ? $code : $Code );?>'>
						</td>

						<td>Status:</td>

						<td>
							<select name='sltStatus'>
								<option value='0'></option>
								<?php
									$qryS = "SELECT * from equipment_status where active=1";
									$resultS = mysqli_query($db, $qryS);
									$process_ErrorS = mysqli_error($db);
									if(!empty($process_ErrorS)){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php'.'</td><td>'.$processErrorS.' near line 95.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{ 
										while($row = mysqli_fetch_assoc($resultS)){
											$status_id = $row['id'];
											$status = $row['status'];

											if ( $equipmentId ){
												if ( $status_id == $equipment_status_id ){
													echo "<option value=".$status_id." selected>".$status."</option>";
												}else {
													echo "<option value='".$status_id."'>".$status."</option>";
												}
											}else{
												if ( $status_id == $Status ){
													echo "<option value=".$status_id." selected>".$status."</option>";
												}else {
													echo "<option value='".$status_id."'>".$status."</option>";
												}
											}
										}
										$db->next_result();
										$resultS->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td>Equipment:</td>

						<td>
							<input type='text' name='txtEquipment' value='<?php echo ( $equipmentId ? $name : $Equipment );?>'>
						</td>

						<td>Measure:</td>

						<td>
							<select name='sltMeasure'>
								<option value='0'></option>
								<?php
									$qryM = "SELECT * from equipment_measure where active=1";
									$resultM = mysqli_query($db, $qryM);
									$process_ErrorM = mysqli_error($db);
									if(!empty($process_ErrorM)){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php'.'</td><td>'.$processErrorM.' near line 143.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{ 
										while($row = mysqli_fetch_assoc($resultM)){
											$measure_id = $row['id'];
											$measure = $row['measure'];

											if ( $equipmentId ){
												if ( $measure_id == $equipment_measure_id ){
													echo "<option value=".$measure_id." selected>".$measure."</option>";
												}else {
													echo "<option value='".$measure_id."'>".$measure."</option>";
												}
											}else{
												if ($measure_id == $Measure){
													echo "<option value=".$measure_id." selected>".$measure."</option>";
												}else {
													echo "<option value='".$measure_id."'>".$measure."</option>";
												}
											}
										}
										$db->next_result();
										$resultM->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr>

						<td>Brand Model:</td>

						<td>
							<input type='text' name='txtModel' value='<?php echo ( $equipmentId ? $brand_model : $Model );?>'>
						</td>

						<td>Location:</td>

						<td>
							<select name='sltLocation'>
								<option value='0'></option>
								<?php
									$qryL = "SELECT * from location where active=1";
									$resultL = mysqli_query($db, $qryL);
									$process_ErrorL = mysqli_error($db);
									if(!empty($process_ErrorL)){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php'.'</td><td>'.$processErrorL.' near line 190.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{ 
										while($row = mysqli_fetch_assoc($resultL)){
											$locationId = $row['id'];
											$location = $row['name'];

											if ( $equipmentId ){
												if ( $locationId == $location_id ){
													echo "<option value=".$locationId." selected>".$location."</option>";
												}else {
													echo "<option value='".$locationId."'>".$location."</option>";
												}
											}else{
												if ($locationId == $Location){
													echo "<option value=".$locationId." selected>".$location."</option>";
												}else {
													echo "<option value='".$locationId."'>".$location."</option>";
												}
											}
										}
										$db->next_result();
										$resultL->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr class="spacing2"></tr>

					<!-- <tr>
						<td>Tolerance Percentage:</td>

						<td>
							<input type='text' name='txtTolerancePercent' value='<?php //echo ( $equipmentId ? $tolerance_percentage : $TolerancePercent );?>'>
						</td>

						<td>Tolerance Value:</td>

						<td>
							<input type='text' name='txtToleranceValue' value='<?php //echo ( $equipmentId ? $tolerance_value : $ToleranceValue );?>'>
						</td>
					</tr>

					<tr>
						<td>Input Range:</td>

						<td>
							<input type='text' name='txtInputRange' value='<?php //echo ( $equipmentId ? $input_range : $InputRange );?>'>
						</td>

						<td>Output Range:</td>

						<td>
							<input type='text' name='txtOutputRange' value='<?php //echo ( $equipmentId ? $output_range : $OutputRange );?>'>
						</td>
					</tr>

					<tr class="spacing2"></tr> -->

					<tr>
						<td>Frequency of Validation:</td>

						<td>
							<select name='sltFrequencyValidation'>
								<option value='0'></option>
								<?php
									$qryF = "SELECT * from frequency";
									$resultF = mysqli_query($db, $qryF);
									$processErrorF = mysqli_error($db);

									if ( !empty($processErrorF) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php'.'</td><td>'.$processErrorF.' near line 272.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{ 
										while($row = mysqli_fetch_assoc($resultF)){
											$frequencyId = $row['id'];
											$frequency = $row['frequency'];

											if ( $equipmentId ){
												if ( $frequency == $frequency_of_validation ){
													echo "<option value=".$frequency." selected>".$frequency."</option>";
												}else {
													echo "<option value='".$frequency."'>".$frequency."</option>";
												}
											}else{
												if ($frequency == $Frequency){
													echo "<option value=".$frequency." selected>".$frequency."</option>";
												}else {
													echo "<option value='".$frequency."'>".$frequency."</option>";
												}
											}
										}
										$db->next_result();
										$resultF->close();
									}
								?>
							</select>
						</td>

						<td>Frequency of Calibration:</td>

						<td>
							<select name='sltFrequencyCalibration'>
								<option value='0'></option>
								<?php
									$qryF = "SELECT * from frequency";
									$resultF = mysqli_query($db, $qryF);
									$processErrorF = mysqli_error($db);

									if ( !empty($processErrorF) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment.php'.'</td><td>'.$processErrorF.' near line 272.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{ 
										while($row = mysqli_fetch_assoc($resultF)){
											$frequencyId = $row['id'];
											$frequency = $row['frequency'];

											if ( $equipmentId ){
												if ( $frequency == $frequency_of_calibration ){
													echo "<option value=".$frequency." selected>".$frequency."</option>";
												}else {
													echo "<option value='".$frequency."'>".$frequency."</option>";
												}
											}else{
												if ($frequency == $Frequency){
													echo "<option value=".$frequency." selected>".$frequency."</option>";
												}else {
													echo "<option value='".$frequency."'>".$frequency."</option>";
												}
											}
										}
										$db->next_result();
										$resultF->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td>Active:</td>

						<td>
							<input type='checkbox' name='chkActive' <?php echo ( $equipmentId ? ( $active ? "checked" : "" ) : ( $Active ? "checked" : "" ) );?>>
						</td>
					</tr>

					<tr class="spacing2"></tr>

					<tr valign='top'>
						<td>Comments:</td>

						<td colspan='3'>
							<textarea rows='4' cols='40' name='txtComments'><?php 
								if ( $equipmentId ){
									$comments_array = explode('<br>', $comments);

									foreach($comments_array as $key => $comments_value){
										echo $comments_value."\n";
									}
								}else{
									$Comments_array = explode('<br>', $Comments);

									foreach($Comments_array as $key => $Comments_value){
										echo $Comments_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>

					<tr class="align_bottom">
						<td>
							<input type="submit" name="btnSaveEquipment" value="Save">
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='<?php echo PG_EQUIPMENT_HOME;?>'">
							<input type='hidden' name='hidEquipmentId' value="<?php echo $equipmentId;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $equipmentId ? $createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $equipmentId ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
