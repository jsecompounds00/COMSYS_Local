<?php
	session_start();
	// if( $_SESSION['cancel_pre'] == false) 
	// {
	// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
	// 	session_write_close();
	// 	header("Location:comsys.php");
	// 	exit();
	// }	

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require("include/database_connect.php");
	require ("include/constant.php");

	$id = intval($_GET['id']);
	$reason = strval($_GET['reason']);
	$closed_at = date('Y-m-d H:i:s');
	$closed_id = $_SESSION['SESS_USER_ID'];
	$closed=1;
	
	$qry = mysqli_prepare($db, "CALL sp_SO_Close(?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'iissi', $id, $closed, $reason, $closed_at, $closed_id);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_close_so.php'.'</td><td>'.$processError.' near line 31.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS'] = 'Sales Order was closed successfully.';
			header("location: sales_order.php?page=".$_SESSION['page']."&so_no=".$_SESSION['so_no']."&so_date".$_SESSION['so_date']."=&so_type=".$_SESSION['so_type']);
		}
			require("include/database_close.php");
?>