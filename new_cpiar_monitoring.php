<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$cpiarID = $_GET["id"];

				if($cpiarID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_CPIAR_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $cpiarID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							$CPIARID = $row["id"];
							$cpr_no = htmlspecialchars($row["cpr_no"]);
							$cpr_date = htmlspecialchars($row["cpr_date"]);
							$due_date = htmlspecialchars($row["due_date"]);
							$issuing_area = $row["issuing_area"];
							$issuing_person = $row["issuing_person"];
							$receiving_area = $row["receiving_area"];
							$receiving_person = $row["receiving_person"];
							$feedback_source = $row["feedback_source"];
							$occurrence = $row["occurrence"];
							$criticality = $row["criticality"];
							$clause = htmlspecialchars($row["clause"]);
							$reminder_date = htmlspecialchars($row["reminder_date"]);
							$final_due_date = htmlspecialchars($row["final_due_date"]);
							$dcc_receipt_date = htmlspecialchars($row["dcc_receipt_date"]);
							$details = htmlspecialchars($row["details"]);
							$created_at = $row["created_at"];
							$created_id = $row["created_id"];
							$root_cause = htmlspecialchars($row["root_cause"]);
							$containment = htmlspecialchars($row["containment"]);
							$containment_implementation = htmlspecialchars($row["containment_implementation"]);
							$action_type = $row["action_type"];
							$action_taken = htmlspecialchars($row["action_taken"]);
							$action_implementation = htmlspecialchars($row["action_implementation"]);
							$man = $row["man"];
							$method = $row["method"];
							$machine = $row["machine"];
							$material = $row["material"];
							$measurement = $row["measurement"];
							$mother_nature = $row["mother_nature"];
							$remarks = htmlspecialchars($row["remarks"]);
							$issuing_division = $row["issuing_division"];
							$receiving_division = $row["receiving_division"];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.cpiar_monitoring";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorPI." near line 61.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($cpiarID, $id, TRUE) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php</td><td>The user tries to edit a non-existing cpiar_id.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>CPIAR Monitoring - Edit</title>";

				}
				else{

					echo "<title>CPIAR Monitoring - Add</title>";

				}
			}
		?>
		<script src="js/jscript.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body <?php if ( !$cpiarID ){ ?> onload="showIssDepartment(0,0), showRecDepartment(0,0)" <?php }?>>

		<form method="post" action="process_new_cpiar_monitoring.php">

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/cpiar_monitoring_init_value.php");

				$zero = 0;
				$one = 1;

				if ( $cpiarID ){
					if( $_SESSION["edit_cpr"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION["add_cpr"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $cpiarID ? "Edit CPIAR# ".$cpr_no : "New CPIAR Monitoring" );?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {
						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);
					}
				?>

				<table class="parent_tables_form">
					<colgroup>
						<col width="180px"></col><col width="350px"></col><col width="180px"></col><col width="350px"></col>
					</colgroup>
					<tr>
						<td> CPIAR No: </td>

						<td>
							<input type="text" name="txtCPRNo" value="<?php echo ( $cpiarID ? $cpr_no : $initCPMCPRNo );?>">
						</td>
					</tr>

					<tr>
						<td> CPIAR Date: </td>

						<td>
							<input type="text" name="txtCPRDate" id="txtCPRDate" value="<?php echo ( $cpiarID ? $cpr_date : $initCPMCPRDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtCPRDate')" style="cursor:pointer" name="picker" />
						</td>

						<td> Due Date: </td>

						<td>
							<input type="text" name="txtDueDate" id="txtDueDate" value="<?php echo ( $cpiarID ? $due_date : $initCPMDueDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDueDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr class="spacing">
						<td class="border_top border_left ">
							<input type="radio" name="radFromDivision" id="FromCompounds" value="Compounds" 
								onchange="showIssDepartment(0,0), showCPIARIssuer(0, 0, 0)"
								<?php echo( $cpiarID ? ( $issuing_division == "Compounds" ? "checked" : "" ) : ( $initCPMFromDivision == "Compounds" ? "checked" : "" ) );?>>
								<label for="FromCompounds">Compounds</label>
						</td>

						<input type="radio" name="radFromDivision" id="FromPipes" class="outcast">

						<td class="border_top border_right">
							<input type="radio" name="radFromDivision" id="FromCorporate" value="Corporate" 
								onchange="showIssDepartment(0,0), showCPIARIssuer(0, 0, 0)"
								<?php echo ( $cpiarID ? ( $issuing_division == "Corporate" ? "checked" : "" ) : ( $initCPMFromDivision == "Corporate" ? "checked" : "" ) );?>>
								<label for="FromCorporate">Corporate</label>
						</td>

						<td class="border_top border_left ">
							<input type="radio" name="radToDivision" id="ToCompounds" value="Compounds" 
								onchange="showRecDepartment(0,0), showCPIARReceiver(0, 0, 0)"
								<?php echo ( $cpiarID ? ( $receiving_division == "Compounds" ? "checked" : "" ) : ( $initCPMToDivision == "Compounds" ? "checked" : "" ) );?>>
								<label for="ToCompounds">Compounds</label>
						</td>

						<input type="radio" name="radToDivision" id="ToPipes" class="outcast">

						<td class="border_top border_right">
							<input type="radio" name="radToDivision" id="ToCorporate" value="Corporate" 
								onchange="showRecDepartment(0,0), showCPIARReceiver(0, 0, 0)"
								<?php echo ( $cpiarID ? ( $receiving_division == "Corporate" ? "checked" : "" ) : ( $initCPMToDivision == "Corporate" ? "checked" : "" ) );?>>
								<label for="ToCorporate">Corporate</label>
						</td>
					</tr>

					<tr>
						<td class="border_left">Issuing Area:</td>

						<td class="border_right">
							<select name="sltIssueDeptID" id="sltIssueDeptID" onchange="showCPIARIssuer(0, this.value, 0)">
								<?php
									if ( $cpiarID ){
										$qryID = mysqli_prepare($db, "CALL sp_Department_Dropdown_New(?, 1)");
										mysqli_stmt_bind_param($qryID, "s", $issuing_division);
										$qryID->execute();
										$resultID = mysqli_stmt_get_result($qryID);
										$processErrorID = mysqli_error($db);
										if ( !empty($processErrorID) ){
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorID." near line 61.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}else{
												echo "<option></option>";	
											while($row = mysqli_fetch_assoc($resultID)){
												if ( $issuing_area == $row["id"] ){
													echo "<option value='".$row["id"]."' selected>".$row["code"]."</option>";
												}else{
													echo "<option value='".$row["id"]."'>".$row["code"]."</option>";
												}
											}
										}
										$db->next_result();
										$resultID->close();
									}
								?>
							</select>
						</td>

						<td class="border_left">Receiving Area:</td>

						<td class="border_right">
							<select name="sltReceiveDeptID" id="sltReceiveDeptID" onchange="showCPIARReceiver(0, this.value, 0)">
								<?php
									if ( $cpiarID ){
										$qryRD = mysqli_prepare($db, "CALL sp_Department_Dropdown_New(?, 1)");
										mysqli_stmt_bind_param($qryRD, "s", $receiving_division);
										$qryRD->execute();
										$resultRD = mysqli_stmt_get_result($qryRD);
										$processErrorRD = mysqli_error($db);

										if ( !empty($processErrorRD) ){
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorRD." near line 61.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}else{
											echo "<option></option>";	

											while($row = mysqli_fetch_assoc($resultRD)){

												if ( $receiving_area == $row["id"] ){
													echo "<option value='".$row["id"]."' selected>".$row["code"]."</option>";
												}
												else{
													echo "<option value='".$row["id"]."'>".$row["code"]."</option>";
												}
												
											}
										}
										$db->next_result();
										$resultRD->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr>

						<td class="border_left border_bottom">Issuing Person:</td>

						<td class="border_right border_bottom">
							<select name="sltIssuerID" id="sltIssuerID">
								<?php
									if ( $cpiarID ){
										$qryII = mysqli_prepare( $db, "CALL sp_User_Dropdown(?, ?, ?, ?)" );
										mysqli_stmt_bind_param( $qryII, "iiis", $issuing_area, $zero, $one, $issuing_division );
										$qryII->execute();
										$resultII = mysqli_stmt_get_result($qryII);
										$processErrorII = mysqli_error($db);
										if ( !empty($processErrorII) ){
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorII." near line 61.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}else{
											echo "<option></option>";	

											while($row = mysqli_fetch_assoc($resultII)){

													if ( $issuing_person == $row["user_id"] ){
														echo "<option value='".$row["user_id"]."' selected>".$row["User"]."</option>";											
													}
													else{
														echo "<option value='".$row["user_id"]."'>".$row["User"]."</option>";
													}

											}
											$db->next_result();
											$resultII->close();
										}
									}
								?>
							</select>
						</td>

						<td class="border_left border_bottom">Receiving Person:</td>

						<td class="border_right border_bottom">
							<select name="sltReceiverID" id="sltReceiverID">
								<?php
									if ( $cpiarID ){
										$qryIII = mysqli_prepare($db, "CALL sp_User_Dropdown(?, ?, ?, ?)");
										mysqli_stmt_bind_param($qryIII, "iiis", $receiving_area, $zero, $one, $receiving_division);
										$qryIII->execute();
										$resultIII = mysqli_stmt_get_result($qryIII);
										$processErrorIII = mysqli_error($db);
										if ( !empty($processErrorIII) ){
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorIII." near line 61.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}else{
												echo "<option></option>";	
											while($row = mysqli_fetch_assoc($resultIII)){
												if ( $cpiarID ){
													if ( $receiving_person == $row["user_id"] ){
														echo "<option value='".$row["user_id"]."' selected>".$row["User"]."</option>";											
													}else{
														echo "<option value='".$row["user_id"]."'>".$row["User"]."</option>";
													}		
												}else{
													if ( $ReceiverID == $row['user_id'] ){
														echo "<option value='".$row["user_id"]."' selected>".$row["User"]."</option>";											
													}else{
														echo "<option value='".$row["user_id"]."'>".$row["User"]."</option>";
													}	
												}
											}
											$db->next_result();
											$resultIII->close();
										}
									}
								?>
							</select>
						</td>
					</tr>

					<tr class="spacing">
						<td> Source of Feedback: </td>

						<td colspan="3">
							<?php
								$qryCS = "SELECT * FROM cpiar_source";
								$resultCS = mysqli_query($db, $qryCS);
								$processErrorCS = mysqli_error($db);
								if ( !empty($processErrorCS) ){
									error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorCS." near line 275.</td></tr>", 3, "errors.php");
									header("location: error_message.html");
								}else{
									while( $row = mysqli_fetch_assoc($resultCS) ){
										$cs_id = $row["id"];
										$cs_name = $row["c_source"];

										if ( $cpiarID ){
											if( $feedback_source == $cs_id ){
												echo "<input type='radio' name='chkSource' id='".$cs_name."' value='".$cs_id."' checked> <label for='".$cs_name."'>".$cs_name."</label>";
											}else{
												echo "<input type='radio' name='chkSource' id='".$cs_name."' value='".$cs_id."'> <label for='".$cs_name."'>".$cs_name."</label>";
											}		
										}else{
											if( $initCPMSource == $cs_id ){
												echo "<input type='radio' name='chkSource' id='".$cs_name."' value='".$cs_id."' checked> <label for='".$cs_name."'>".$cs_name."</label>";
											}else{
												echo "<input type='radio' name='chkSource' id='".$cs_name."' value='".$cs_id."'> <label for='".$cs_name."'>".$cs_name."</label>";
											}
										}
									}
									$db->next_result();
									$resultCS->close();
								}
							?>
						</td>
					</tr>

					<tr class="spacing">
						<td> Occurrence: </td>

						<td colspan="3">
							<?php
								$qryCO = "SELECT * FROM cpiar_occurrence";
								$resultCO = mysqli_query($db, $qryCO);
								$processErrorCO = mysqli_error($db);
								if ( !empty($processErrorCO) ){
									error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorCO." near line 275.</td></tr>", 3, "errors.php");
									header("location: error_message.html");
								}else{
									while( $row = mysqli_fetch_assoc($resultCO) ){
										$co_id = $row["id"];
										$co_name = $row["c_occurrence"];

										if ( $cpiarID ){
											if( $occurrence == $co_id ){
												echo "<input type='radio' name='chkOccurrence' id='".$co_name."' value='".$co_id."' checked> <label for='".$co_name."'>".$co_name."</label>";
											}else{
												echo "<input type='radio' name='chkOccurrence' id='".$co_name."' value='".$co_id."'> <label for='".$co_name."'>".$co_name."</label>";
											}		
										}else{
											if( $initCPMOccurrence == $co_id ){
												echo "<input type='radio' name='chkOccurrence' id='".$co_name."' value='".$co_id."' checked> <label for='".$co_name."'>".$co_name."</label>";
											}else{
												echo "<input type='radio' name='chkOccurrence' id='".$co_name."' value='".$co_id."'> <label for='".$co_name."'>".$co_name."</label>";
											}
										}
									}
									$db->next_result();
									$resultCO->close();
								}
							?>
						</td>
					</tr>

					<tr class="spacing">
						<td> Criticality: </td>
						<td>
							<?php
								$qryCC = "SELECT * FROM cpiar_criticality";
								$resultCC = mysqli_query($db, $qryCC);
								$processErrorCC = mysqli_error($db);
								if ( !empty($processErrorCC) ){
									error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorCC." near line 275.</td></tr>", 3, "errors.php");
									header("location: error_message.html");
								}else{
									while( $row = mysqli_fetch_assoc($resultCC) ){
										$cc_id = $row["id"];
										$cc_name = $row["c_criticality"];

										if ( $cpiarID ){
											if( $criticality == $cc_id ){
												echo "<input type='radio' name='chkCriticality' id='".$cc_name."' value='".$cc_id."' checked> <label for='".$cc_name."'>".$cc_name."</label>";
											}else{
												echo "<input type='radio' name='chkCriticality' id='".$cc_name."' value='".$cc_id."'> <label for='".$cc_name."'>".$cc_name."</label>";
											}		
										}else{
											if( $initCPMCriticality == $cc_id ){
												echo "<input type='radio' name='chkCriticality' id='".$cc_name."' value='".$cc_id."' checked> <label for='".$cc_name."'>".$cc_name."</label>";
											}else{
												echo "<input type='radio' name='chkCriticality' id='".$cc_name."' value='".$cc_id."'> <label for='".$cc_name."'>".$cc_name."</label>";
											}
										}
									}
									$db->next_result();
									$resultCC->close();
								}
							?>
						</td>

						<td> ISO 9001:2008 Clause: </td>

						<td>
							<input type="text" name="txtClause" value="<?php echo ( $cpiarID ? $clause : $initCPMClause );?>">
						</td>
					</tr>

					<tr valign="top">
						<td> Details of Feedback: </td>

						<td colspan="3">
							<textarea name="txtDetails" class="paragraph"><?php
								if ( $cpiarID ){
									echo $details;
								}else{
									echo $initCPMDetails;
								}
							?></textarea>
						</td>
					</tr>

					<tr>
						<td rowspan="2">
							Root Cause Analysis:
						</td>
					</tr>

					<tr class="spacing">
						<td colspan="4">
							<input type="checkbox" name="chkMan" id="Man"
								<?php echo ( $cpiarID ? ( $man ? "checked" : "" ) : ( $initCPMMan ? "checked" : "" ) );?>> 
								<label for="Man">Man</label>

							<input type="checkbox" name="chkMethod" id="Method"
								<?php echo ( $cpiarID ? ( $method ? "checked" : "" ) : ( $initCPMMethod ? "checked" : "" ) );?>> 
								<label for="Method">Method</label>

							<input type="checkbox" name="chkMachine" id="Machine"
								<?php echo ( $cpiarID ? ( $machine ? "checked" : "" ) : ( $initCPMMachine ? "checked" : "" ) );?>>  
								<label for="Machine">Machine</label>

							<input type="checkbox" name="chkMaterial" id="Material"
								<?php echo ( $cpiarID ? ( $material ? "checked" : "" ) : ( $initCPMMaterial ? "checked" : "" ) );?>>  
								<label for="Material">Material</label>

							<input type="checkbox" name="chkMeasurement" id="Measurement"
								<?php echo ( $cpiarID ? ( $measurement ? "checked" : "" ) : ( $initCPMMeasurement ? "checked" : "" ) );?>> 
								<label for="Measurement">Measurement</label>

							<input type="checkbox" name="chkMotherNature" id="MotherNature"
								<?php echo ( $cpiarID ? ( $mother_nature ? "checked" : "" ) : ( $initCPMMotherNature ? "checked" : "" ) );?>> 
								<label for="MotherNature">Mother Nature</label>
						</td>
					</tr>

					<tr>
						<td></td>
						<td colspan="3">
							<textarea class="paragraph" name="txtRootCause"><?php
								if ( $cpiarID ){
									echo $root_cause;
								}else{
									echo $initCPMRootCause;
								}
							?></textarea>
						</td>
					</tr>

					<tr valign="top">
						<td rowspan="2"> Containment Action: </td>

						<td colspan="3">
							<textarea class="paragraph" name="txtContainment"><?php
								if ( $cpiarID ){
									echo $containment;
								}else{
									echo $initCPMContainment;
								}
							?></textarea>
						</td>
					</tr>

					<tr class="spacing" valign="top">
						<td colspan="3">
							Implementation Date:

							<input type="text" name="txtImplementContainment" value="<?php echo ( $cpiarID ? $containment_implementation : $initCPMImplementContainment );?>">
						</td>
					</tr>

					<tr valign="top">
						<td rowspan="3"> Action Taken / <br>Propose Action: </td>

						<td colspan="3">
							<?php
								$qryCA = "SELECT * FROM cpiar_action";
								$resultCA = mysqli_query($db, $qryCA);
								$processErrorCA = mysqli_error($db);
								if ( !empty($processErrorCA) ){
									error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_cpiar_monitoring.php"."</td><td>".$processErrorCA." near line 275.</td></tr>", 3, "errors.php");
									header("location: error_message.html");
								}else{
									while( $row = mysqli_fetch_assoc($resultCA) ){
										$ca_id = $row["id"];
										$ca_name = $row["c_action"];

										if ( $cpiarID ){
											if( $action_type == $ca_id ){
												echo "<input type='radio' name='chkAction' id='".$ca_name."' value='".$ca_id."' checked> <label for='".$ca_name."'>$ca_name</label>";
											}else{
												echo "<input type='radio' name='chkAction' id='".$ca_name."' value='".$ca_id."'> <label for='".$ca_name."'>$ca_name</label>";
											}		
										}else{
											if( $initCPMAction == $ca_id ){
												echo "<input type='radio' name='chkAction' id='".$ca_name."' value='".$ca_id."' checked> <label for='".$ca_name."'>$ca_name</label>";
											}else{
												echo "<input type='radio' name='chkAction' id='".$ca_name."' value='".$ca_id."'> <label for='".$ca_name."'>$ca_name</label>";
											}
										}
									}
									$db->next_result();
									$resultCA->close();
								}
							?>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<textarea class="paragraph" name="txtCorrectPrevent"><?php
								if ( $cpiarID ){
									echo $action_taken;
								}else{
									echo $initCPMCorrectPrevent;
								}
							?></textarea>
						</td>
					</tr>

					<tr class="spacing" valign="top">
						<td colspan="3">
							Implementation Date:

							<input type="text" name="txtImplementCorrectPrevent" value="<?php echo ( $cpiarID ? $action_implementation : $initCPMImplementCorrectPrevent );?>">
						</td>
					</tr>

					<tr>
						<td>Issuance of Reminder:</td>

						<td>
							<input type="text" name="txtMemoDate" id="txtMemoDate" value="<?php echo ( $cpiarID? $reminder_date : $initCPMMemoDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtMemoDate')" style="cursor:pointer" name="picker" />
						</td>

						<td>Final Due Date:</td>

						<td>
							<input type="text" name="txtFinalDueDate" id="txtFinalDueDate" value="<?php echo ( $cpiarID ? $final_due_date : $initCPMFinalDueDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtFinalDueDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr class="spacing">
						<td>Receipt Date (DCC):</td>

						<td>
							<input type="text" name="txtReceiveDateDCC" id="txtReceiveDateDCC" value="<?php echo ( $cpiarID ? $dcc_receipt_date : $initCPMReceiveDateDCC );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtReceiveDateDCC')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr valign="top">
						<td> Remarks: </td>
						<td colspan="3">
							<textarea class="paragraph" name='txtRemarks'><?php
								if ( $cpiarID ){
									echo $remarks;
								}else{
									echo $initCPMRemarks;
								}
							?></textarea>
						</td>
					</tr>

					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='cpiar_monitoring.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidCpiarID" value="<?php echo $cpiarID;?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $cpiarID ? $created_id : $_SESSION["SESS_USER_ID"] );?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $cpiarID ? $created_at : date("Y-m-d H:i:s") );?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>