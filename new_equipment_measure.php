<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment_measure.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$eqptMeasureId = $_GET['id'];

				if($eqptMeasureId)
				{ 
					$qry = mysqli_prepare( $db, "CALL sp_Equipment_Measure_Query( ? )" );
					mysqli_stmt_bind_param( $qry, 'i', $eqptMeasureId );
					$qry->execute();
					$result = mysqli_stmt_get_result( $qry );
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment_measure.php'.'</td><td>'.$processError.' near line 26.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$measure = $row['measure'];
							$comments = $row['comments'];
							$active = $row['active'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

					############ .............
					$qryPI = "SELECT id from comsys.equipment_measure";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment_measure.php'.'</td><td>'.$processErrorPI.' near line 58.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($eqptMeasureId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_equipment_measure.php</td><td>The user tries to edit a non-existing measure_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Equipment Measure - Edit</title>";
				}
				else
				{
					echo "<title>Equipment Measure - Add</title>";
				}
				
			}
		?>
	</head>
	<body>

		<form method='post' action='process_new_equipment_measure.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ($eqptMeasureId ? "Edit ".$measure : "New Equipment Measure" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Measure:</td>
						<td>
							<input type='text' name='txtNewMeasure' value='<?php echo ( $eqptMeasureId ? $measure : "" );?>'>
						</td>
					</tr>
					<tr>
						<td valign='top'>Comments:</td>
						<td>
							<textarea name='txtComments'><?php
								if ( $eqptMeasureId ){
									$comments_array = explode("<br>", $comments);

									foreach ($comments_array as $comments_key => $comments_value) {
										echo $comments_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type='checkbox' name='chkActive' <?php echo ( $eqptMeasureId ? ( $active ? "checked" : "" ) : "" );?>>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<input type="submit" name="btnSaveMeasure" value="Save">
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='<?php echo PG_EQUIPMENT_MEASURE_HOME;?>'">
							<input type='hidden' name='hideqptMeasureId' value="<?php echo $eqptMeasureId;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $eqptMeasureId? $createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $eqptMeasureId ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>