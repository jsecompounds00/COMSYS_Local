<html>
	<head>
		<?php
			require("/include/database_connect.php");
		?>
		<title>Ceritificate of Qualities - Edit</title>
		<script src="js/jscript.js"></script> 
		<?php
			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>edit_coq.php'.'</td><td>'.$error.' near line 13.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$coqid = $_GET['coq_id'];
				$fgid = $_GET['fg_id'];

				if($coqid && $fgid)
				{
					$qry = mysqli_prepare($db, "CALL sp_COQ_Item_Dropdown(?,?)");
					mysqli_stmt_bind_param( $qry, 'ii', $fgid, $coqid );
					$qry->execute();
					$result = mysqli_stmt_get_result( $qry );
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>edit_coq.php'.'</td><td>'.$processError.' near line 36.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else
					{
						$COQItemId = array();
						$COQPropertyId = array();
						$COQProperty = array();
						$TestMethod = array();
						$Result = array();
						$OvenAged = array();
						$OilAged = array();

						while ( $row = mysqli_fetch_assoc($result) ){

							$name = $row['name'];
							$COQId = $row['COQId'];
							$COQDate = $row['COQDate'];
							$ProductionDate = $row['ProductionDate'];
							$LotNumber = $row['LotNumber'];
							$BagNumber = $row['BagNumber'];
							$COQItemId[] = $row['COQItemId'];
							$COQPropertyId[] = $row['COQPropertyId'];
							$COQProperty[] = $row['COQProperty'];
							$TestMethod[] = $row['TestMethod'];
							$Result[] = $row['Result'];
							$OvenAged[] = $row['OvenAged'];
							$OilAged[] = $row['OilAged'];
							$Category = $row['Category'];

						}

						$db->next_result();
						$result->close();
					}
					########### .............
						$qryPI = "SELECT id from comsys.finished_goods";
						$resultPI = mysqli_query($db, $qryPI); 
						$processErrorPI = mysqli_error($db);

						if ( !empty($processErrorPI) ){
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>edit_coq.php'.'</td><td>'.$processErrorPI.' near line 76.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}else{
								$id1 = array();
							while($row = mysqli_fetch_assoc($resultPI))
							{
								$id1[] = $row['id'];
							}
						}
						$db->next_result();
						$resultPI->close();

					############ .............
						if( !in_array($fgid, $id1, TRUE) ){
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>edit_coq.php</td><td>The user tries to edit a non-existing fgid.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
					########### .............
						$qryPI = "SELECT id from comsys.certificate_of_qualities";
						$resultPI = mysqli_query($db, $qryPI); 
						$processErrorPI = mysqli_error($db);

						if ( !empty($processErrorPI) ){
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>edit_coq.php'.'</td><td>'.$processErrorPI.' near line 99.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}else{
								$id = array();
							while($row = mysqli_fetch_assoc($resultPI)){
								$id[] = $row['id'];
							}
						}
						$db->next_result();
						$resultPI->close();

					############ .............
						if( !in_array($coqid, $id, TRUE) ){
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>edit_coq.php</td><td>The user tries to edit a non-existing coqid.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
				}
			}
		?>
	</head>
	<body>

		<form method='post' action='process_update_coq.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");
			?>

			<div class="wrapper">
				
				<span> <h3> Update COQ for <?php echo $name; ?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';
						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}
						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Lot Number : </td>
						<td>
							<b><?php echo $LotNumber ; ?></b>
						</td>
					</tr>
					<tr>
						<td>COQ Date : </td>
						<td>
							<b><?php echo $COQDate ; ?></b>
						</td>
					</tr>
					<tr>
						<td>Production Date : </td>
						<td>
							<b><?php echo $ProductionDate ; ?></b>
						</td>
					</tr>
					<tr>
						<td>Bag Number : </td>
						<td>
							<b><?php echo $BagNumber ; ?></b>
						</td>
					</tr>
				</table>
				<table class='child_tables_form'>
					<tr>
						<th>Properties</th>
						<th>Method</th>
						<th>Results</th>
						<?php
							if ( $Category != 'PMH' && $Category != 'RIGID' )
							{
						?>
								<th>Oven Aged</th>
								<th>Oil Aged</th>
						<?php
							}
						?>
					</tr>
					<?php
						foreach ($COQProperty as $key => $Property) {
					?>
							<tr>
								<?php
									$OvenAged[$key] = ( $OvenAged[$key] != 0 ? $OvenAged[$key] : '' );
									$OilAged[$key] = ( $OilAged[$key] != 0 ? $OilAged[$key] : '' );
								?>
								<input type='hidden' name='hidCOQItemId[]' value="<?php echo $COQItemId[$key];?>">
								<input type='hidden' name='hidCOQPropertyId[]' value="<?php echo $COQPropertyId[$key];?>">
								<td> <?php echo $Property;?> </td>
								<td> <?php echo $TestMethod[$key];?> </td>
								<td> <?php echo $Result[$key];?> </td>
								<td>
								<?php
									if ( $Category != 'PMH' && $Category != 'RIGID' )
									{

										if ( substr($COQProperty[$key], 0, 7) == "TENSILE" )
										{
											echo "<input type='text' name='txtOvenAged[]' value='".$OvenAged[$key]."'>";
										}
										elseif ( substr($COQProperty[$key], 0, 10) == "ELONGATION" )
										{
											echo "<input type='text' name='txtOvenAged[]' value='".$OvenAged[$key]."'>";
										}
										else
										{
											echo "<input type='hidden' name='txtOvenAged[]' value='".$OvenAged[$key]."'>";
										}
									}
									else
									{
										echo "<input type='hidden' name='txtOvenAged[]' value='".$OvenAged[$key]."'></td>";
									}
								?>
								</td>
								<?php

									if ( $Category != 'PMH' && $Category != 'RIGID' )
									{

										if ( substr($COQProperty[$key], 0, 7) == "TENSILE" )
										{
											echo "<td><input type='text' name='txtOilAged[]' value='".$OilAged[$key]."'></td>";
										}
										elseif ( substr($COQProperty[$key], 0, 10) == "ELONGATION" )
										{
											echo "<td><input type='text' name='txtOilAged[]' value='".$OilAged[$key]."'></td>";
										}
										else
										{
											echo "<td><input type='hidden' name='txtOilAged[]' value='".$OilAged[$key]."'></td>";
										}

									}
									else
									{
										echo "<input type='hidden' name='txtOilAged[]' value='".$OilAged[$key]."'>";
									}
								?>
							</tr>
					<?php
						}
					?>
					<tr class='align_bottom'>
						<td >
							<input type='hidden' name='hidCOQId' value="<?php echo $COQId;?>">
							<input type='hidden' name='hidFGId' value="<?php echo $fgid;?>">
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
								if ( $Category != 'PMH' && $Category != 'RIGID' )
									echo "<input type='submit' value='Save'>";
							?>
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='new_coq.php?id=<?php echo $fgid;?>'">
							<input type='button' name='btnBack' value='Back to FG Home' onclick="location.href='finished_goods.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>