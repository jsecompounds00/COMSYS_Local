<html>
	<head>
		<script src="js/jscript.js"></script>
  		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>credit_payment.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$creditId = $_GET['id'];

				if($creditId)
				{ 
					$transType = $_GET['type'];

					echo "<title>Credit: Payment - Edit</title>";

					$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_Query( ? )");
					mysqli_stmt_bind_param($qry, 'i', $creditId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if ( !empty($processError) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>credit_payment.php'.'</td><td>'.$processError.' near line 12.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						$db_jacket_item_id = array();
						$db_invoice_number = array();
						$db_amount = array();
						$db_discount = array();
						$db_bank_code = array();
						// $db_bank_code_id = array();
						// $db_check_number = array();
						while($row = mysqli_fetch_assoc($result)){
							$db_jacket_id = $row['jacket_id'];
							$db_process_type = $row['process_type'];
							$db_customer_id = $row['customer_id'];
							$db_transaction_type = $row['transaction_type'];
							$db_invoice_date = $row['invoice_date'];
							$db_check_returned_date = $row['check_returned_date'];
							$db_check_date = $row['check_date'];
							$db_created_at = $row['created_at'];
							$db_created_id = $row['created_id'];
							$db_name = htmlspecialchars($row['name']);
							$db_check_number = htmlspecialchars($row['check_number']);
							$db_bank_code_id = $row['bank_code_id'];
							$db_check_amount = $row['check_amount'];

							$db_jacket_item_id[] = $row['jacket_item_id'];
							$db_invoice_number[] = $row['invoice_number'];
							$db_amount[] = $row['amount'];
							$db_discount[] = $row['discount'];
							$db_bank_code[] = $row['bank_code'];
						}
						$db->next_result();
						$result->close();
						$count = count( $db_jacket_item_id );
					}

					############ .............
					$qryPI = "SELECT id from comsys.customer_jackets WHERE process_type='sub'";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>credit_payment.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($creditId, $id, TRUE) && $transType == 'cp' ){
						if ( $transType == 'dm' ){
							header("location: debit_memo.php?id=".$creditId."&type=dm");
						}elseif ( $transType == 'di' ){
							header("location: debit_invoice.php?id=".$creditId."&type=di");
						}elseif ( $transType == 'cm' ){
							header("location: credit_memo.php?id=".$creditId."&type=cm");
						}else{
							error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_invoice.php</td><td>The user tries to edit credit payment.</td></tr>', 3, "errors.php");
							header("location: error_message.html");
						}
					}
				}
				else
				{
				
					echo "<title>Credit: Payment - Add</title>";
				}
			}
		?>
	</head>
	<body onload='showCreditPaymentItems()'>

		<form method='post' action='process_credit_payment.php' id='creditForm'>

			<?php 
				require("/include/header.php");
				require("/include/init_unset_values/credit_payment_init_value.php");

				if ( $creditId ){
					if( $_SESSION['edit_credit_payment'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['credit_payment'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $creditId ? "Edit " : "New " ); ?> Credit: Payment </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Customer:</td>
						<td colspan='2'>
						<?php
							if ( $creditId ) {
								echo '<b>'.$db_name.'</b>';
						?>		
								<input type="hidden" name="sltCustomer" id="sltCustomer" value="<?php echo $db_customer_id; ?>">
						<?php		
							}else{
						?>
								<select name="sltCustomer" id="sltCustomer" onchange="showCreditPaymentItems()">
									<option></option>
									<?php
										$qry = "CALL sp_Customer_Dropdown()";
										$result = mysqli_query($db, $qry);
										$processError = mysqli_error($db);

										if ( !empty($processError) ){
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>credit_payment.php'.'</td><td>'.$processError.' near line 12.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}else{
											while($row = mysqli_fetch_assoc($result)){
												$customer_id = $row['id'];
												$customer_name = $row['name'];

												if ( $customer_id ==  $initCJCPCustomer){
													echo "<option value='".$customer_id."' selected>".$customer_name."</option>";
												}else{
													echo "<option value='".$customer_id."'>".$customer_name."</option>";
												}
											}
											$db->next_result();
											$result->close();
										}
									?>
								</select>
						<?php
							}
						?>
						</td>
					</tr
					>
					<tr>
						<td>Transaction Type:</td>

						<td>
							<b> Invoice-PMT </b> <input type="hidden" name="hidTransType" value="IPT">
						</td>
					</tr>

					<tr>
						<td>Check Date:</td>

						<td colspan="2">
							<input type="text" name="txtCheckDate" id="txtCheckDate" value="<?php echo ( $creditId ? $db_check_date : $initCJCPCheckDate ); ?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtCheckDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
						<td>Check No.:</td>
						<td>	
							<select name="sltBankCode">
								<?php
									$qryBankCode = "CALL sp_Bank_Code_Dropdown()";
									$resultBankCode = mysqli_query( $db, $qryBankCode );
									$processErrorBankCode = mysqli_error( $db );

									if ( !empty($processErrorBankCode) ){
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>debit_invoice.php'.'</td><td>'.$processErrorBankCode.' near line 12.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}else{
											echo "<option></option>";

											$y = 0;

										while($row = mysqli_fetch_assoc($resultBankCode)){
											$bank_code_id = $row['id'];
											$bank_code = $row['bank_code'];
								?>
											<option id='optionBankCode<?php echo $i.$y; ?>' value='<?php echo $bank_code_id; ?>' <?php echo ( $creditId ? ( $db_bank_code_id == $bank_code_id ? "selected" : "" ) : ( $initCJCPBankCode == $bank_code_id ? "selected" : "" ) ); ?> >
												<?php echo $bank_code; ?>
											</option>
								<?php
											$y++;

										}
										$db->next_result();
										$resultBankCode->close();
									}
								?>
							</select>
						</td>

						<td> 
							<input type="text" name="txtCheckNo" id="txtCheckNo" value="<?php echo ( $creditId ? $db_check_number : $initCJCPCheckNo ); ?>">
						</td>
					</tr>

					<tr>
						<td> Check Amount: </td>

						<td>
							<input type="text" name="txtCheckAmount" id="txtCheckAmount" onkeyup="computeTotalPayment()" onblur="computeTotalPayment()" value="<?php echo ( $creditId ? $db_check_amount : $initCJCPCheckAmount ); ?>">
						</td>

						<td> 
							<label style="color:red; font-size:11px; font-style:italic; vertical-align: top; margin-left:20px;" id="tdErrorMessage"></label>
						</td>
					</tr>
				</table>
				
				<table class='child_tables_form' id='tbleCreditPaymentItems'>
				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveCust" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='customer_jacket.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidCreditId' id='hidCreditId' value='<?php echo $creditId; ?>'>
							<input type='hidden' name='hidProcessType' value='sub'>	
							<input type='hidden' name='hidJacketType' value='cp'>
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $creditId ? $db_created_at : date('Y-m-d H:i:s') ); ?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $creditId ? $db_created_id : 0 ); ?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php 
			require("include/database_close.php");
		?>
	</footer>
</html>
