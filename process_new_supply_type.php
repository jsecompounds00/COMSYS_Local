<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_supply_type.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidSTId = $_POST['hidSupplyTypeId'];
		$txtNewSupplyType = $_POST['txtNewSupplyType'];
		$txtComments = $_POST['txtComments'];
		//Input Validations

		if(isset($_POST['chkCmpds']))
			$chkCmpds = 1;
		else
			$chkCmpds = 0;
		if(isset($_POST['chkPips']))
			$chkPips = 1;
		else
			$chkPips = 0;
		if(isset($_POST['chkCorp']))
			$chkCorp = 1;
		else
			$chkCorp = 0;
		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;
		

		$_SESSION['SESS_SPT_NewSupplyType'] = $txtNewSupplyType;
		$_SESSION['SESS_SPT_Cmpds'] = $chkCmpds;
		$_SESSION['SESS_SPT_Pips'] = $chkPips;
		$_SESSION['SESS_SPT_Corp'] = $chkCorp;
		$_SESSION['SESS_SPT_Active'] = $chkActive;
		$_SESSION['SESS_SPT_Comments'] = $txtComments;

		if ( empty($txtNewSupplyType) ){
			$errmsg_arr[] = '* Supply type is missing.';
			$errflag = true;
		}
		if ( is_numeric($txtNewSupplyType) ){
			$errmsg_arr[] = '*Invalid Supply type.';
			$errflag = true;
		}

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:".PG_NEW_SUPPLY_TYPE.$hidSTId."");
			exit();
		}

		if($hidSTId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidSTId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

	
		
		$qry = mysqli_prepare($db, "CALL sp_SupplyType_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isiiiisssii', $hidSTId, $txtNewSupplyType, $chkCmpds, $chkPips, $chkCorp, $chkActive
												 , $txtComments, $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_supply_type.php'.'</td><td>'.$processError.' near line 98.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidSTId)
				$_SESSION['SUCCESS']  = 'Successfully updated supply type.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new supply type.';
			//echo $_SESSION['SUCCESS'];
			header("location: supply_type.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}
		unset($_SESSION['page']);
		unset($_SESSION['search']);
		unset($_SESSION['qsone']);

		$db->next_result();
		$result->close();
		require("include/database_close.php");
				
	}

?>