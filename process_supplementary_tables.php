<?php
	session_start();

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require ("include/constant.php");

function prepare_r009(){
    @$db = mysqli_connect('192.168.200.107', 'root', 'qwertyuiop1234', 'crown_asia_production');
    $errno = mysqli_connect_errno();

	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_supplementary_tables.php'.'</td><td>'.$error.' near line 16.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL R009_RM_Below_Critical_Level_Supplemental()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_supplementary_tables.php'.'</td><td>'.$processError.' near line 24.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in preparing table.';
			header("location:supplementary_tables.php?page=1&search=&qsone=");
		}else{
			$_SESSION['SUCCESS'] = 'R-009 Supplementary Table was Succesfully created.';
			header("location:supplementary_tables.php?page=1&search=&qsone=");	
		}
	}

	require ("include/database_close.php");
}


// function migrate_all(){
// 	require ("include/database_connect.php");

// 	if ( !empty($errno) ){
// 		$error = mysqli_connect_error();
// 		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_supplementary_tables.php'.'</td><td>'.$error.' near line 251.</td></tr>', 3, "errors.php");
// 		header("location: error_message.html");
// 	}else{
// 		$qryD = "CALL migrate_CIMS_COMSYS_all()";
// 		$resultD = mysqli_query($db, $qryD);
// 		$processError = mysqli_error($db);

// 		if(!empty($processError)){
// 			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_supplementary_tables.php'.'</td><td>'.$processError.' near line 259.</td></tr>', 3, "errors.php");
// 			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
// 			header("location:".PG_MIGRATION_HOME);
// 		}else{
// 			$_SESSION['SUCCESS'] = 'Succesfully migrated all tables.';
// 			header("location:".PG_MIGRATION_HOME);	
// 		}
// 	}

// 	require ("include/database_close.php");
// }

if(isset($_GET['run_r009'])){
	prepare_r009();
}
// if(isset($_GET['run_all'])){
// 	migrate_all();
// }

?>