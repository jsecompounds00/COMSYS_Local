<?php
	session_start();

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require ("include/constant.php");

function migrate_user(){
	require ("include/database_connect.php");

	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 15.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_users()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 23.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Users table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}

	require ("include/database_close.php");
}

function migrate_department(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 40.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_departments()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 48.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Departments table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}

	require ("include/database_close.php");
}

function migrate_uom(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 65.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
 	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_unit_of_measures()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 73.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Unit of measures table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}

	require ("include/database_close.php");
}

function migrate_supplier(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 90.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_suppliers()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 98.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Supplier table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
		
	require ("include/database_close.php");
}

function migrate_customer(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 115.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_customers()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 125.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Customer table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_formulas(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 142.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_formulas()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 152.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Formulas table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_raw_materials(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 169.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_raw_materials()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 179.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Raw Materials table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_supplies(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 196.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_supplies()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 206.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Supplies table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_trucks(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 224.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_trucks()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 234.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Trucks table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_coq(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 251.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_certificate_of_qualities()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 261.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Certificate of qualities table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_coqitems(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 278.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_certificate_of_quality_items()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 288.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Certificate of quality items table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_personnels(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 305.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_personnels()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 315.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Personnels table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_positions(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 305.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_positions()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 315.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Positions table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_finished_goods(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 359.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_finished_goods()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 369.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Finished Goods table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_raw_material_types(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 359.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_raw_material_types()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 369.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Raw Material Types table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_mixers(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 359.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_mixers()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 369.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Mixers table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}

function migrate_supply_transactions(){
 	require ("include/database_connect.php");

 	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 440.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_supply_transactions()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			// $error = mysqli_connect_error();
			// echo ("$processError");
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 450.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Supply transactions table was succesfully migrated.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}
	
	require ("include/database_close.php");
}


function migrate_all(){
	require ("include/database_connect.php");

	if ( !empty($errno) ){
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$error.' near line 251.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryD = "CALL migrate_CIMS_COMSYS_all()";
		$resultD = mysqli_query($db, $qryD);
		$processError = mysqli_error($db);

		if(!empty($processError)){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_migration.php'.'</td><td>'.$processError.' near line 449.</td></tr>', 3, "errors.php");
			$_SESSION['ERRMSG_ARR'] = 'Error in migrating.';
			header("location:".PG_MIGRATION_HOME);
		}else{
			$_SESSION['SUCCESS'] = 'Succesfully migrated all tables.';
			header("location:".PG_MIGRATION_HOME);	
		}
	}

	require ("include/database_close.php");
}

if(isset($_GET['run_user'])){
	migrate_user();
}
if(isset($_GET['run_department'])){
	migrate_department();
}
if(isset($_GET['run_uom'])){
	migrate_uom();
}
if(isset($_GET['run_supplier'])){
	migrate_supplier();
}
if(isset($_GET['run_customer'])){
	migrate_customer();
}
if(isset($_GET['run_formulas'])){
	migrate_formulas();
}
if(isset($_GET['run_raw_materials'])){
	migrate_raw_materials();
}
if(isset($_GET['run_supplies'])){
	migrate_supplies();
}
if(isset($_GET['run_trucks'])){
	migrate_trucks();
}
if(isset($_GET['run_coq'])){
	migrate_coq();
}
if(isset($_GET['run_coqitems'])){
	migrate_coqitems();
}
if(isset($_GET['run_personnels'])){
	migrate_personnels();
}
if(isset($_GET['run_positions'])){
	migrate_positions();
}
if(isset($_GET['run_finished_goods'])){
	migrate_finished_goods();
}
if(isset($_GET['run_raw_material_types'])){
	migrate_raw_material_types();
}
if(isset($_GET['run_mixer'])){
	migrate_mixers();
}
if(isset($_GET['run_supply_transactions'])){
	migrate_supply_transactions();
}
if(isset($_GET['run_all'])){
	migrate_all();
}

?>