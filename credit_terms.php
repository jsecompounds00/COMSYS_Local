<html>
	<head>
		<title>LC (Export) - Home</title>
		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/unset_value.php");

			if( $_SESSION['credit_terms'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3>  LC (Export) </h3> </span>

			<div class="search_box">
				<form method="get" action="credit_terms.php">
					<input type="hidden" name="page" value="<?php echo $page;?>">
					<table class="search_tables_form">
						<tr>
							<td> LC No.: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
							<td> LC Date: </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>"> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
							<td> <input type='submit' value='Search'> </td>
							<td>
								<?php 		
									if(array_search(111, $session_Permit)){ 
								?>
										<input type='button' value='Add LC' onclick="location.href='new_credit_terms.php?id=0'">
								<?php
										$_SESSION['add_credit'] = true;
									}else{
										unset($_SESSION['add_credit']);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>credit_terms.php'.'</td><td>'.$error.' near line 51.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Credit_Terms_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'ss', $search, $qsone);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "credit_terms.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Credit_Terms_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssii', $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>credit_terms.php'.'</td><td>'.$processError.' near line 77.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='7'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
						    <th>LC No.</th>
						    <th>LC Date</th>
						    <th>Quantity</th>
						    <th></th>
						</tr>
						<?php 
							while($row = mysqli_fetch_assoc($result)) { 
						?>
								<tr>
									<td><?php echo $row['lc_number']; ?></td>
									<td><?php echo $row['lc_date']; ?></td>
									<td><?php echo $row['quantity']; ?></td>
									<td>
									<?php
										if(array_search(112, $session_Permit)){
									?>
											<input type='button' name='btnTH' value='Edit' onclick="location.href='new_credit_terms.php?page=1&id=<?php echo $row['id'];?>'">
									<?php
											$_SESSION['edit_credit'] = true;
										}else{
											unset($_SESSION['edit_credit']);
										}
									?>
									</td>
								</tr>
						<?php 
							} 
						?>
						<tr>
							<td colspan='7'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>