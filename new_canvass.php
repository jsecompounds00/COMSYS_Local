<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_canvass.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$canvassID = $_GET['id'];

				$qryNum = mysqli_prepare($db, "CALL sp_Canvass_Control_Number(?)");
				mysqli_stmt_bind_param($qryNum, 'i', $canvassID);
				$qryNum->execute();
				$resultNum = mysqli_stmt_get_result($qryNum); //return results of query
				$processErrorNum = mysqli_error($db);

				if(!empty($processErrorNum))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_canvass.php'.'</td><td>'.$processErrorNum.' near line 35.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					while($row = mysqli_fetch_assoc($resultNum))
					{
						$db_CanvassNumber = $row['db_CanvassNumber'];
					}
				}
				$db->next_result();
				$resultNum->close();

				if($canvassID)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Canvass_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $canvassID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);

					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_canvass.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						$PREIDsArray = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$CanvassDate = $row['canvass_date'];
							$Division = $row['division'];
							$Remarks = htmlspecialchars($row['remarks']);
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];

							$PREIDsArray[] = $row['pre_id'];
						}
						$db->next_result();
						$result->close();

						$PREIDsString = implode(',',  array_unique($PREIDsArray));
					}

		############ .............
					$qryPI = "SELECT id from comsys.canvass";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_canvass.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($canvassID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_canvass.php</td><td>The user tries to edit a non-existing customer_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Canvass - Edit</title>";

				}
				else{

					echo "<title>Canvass - Add</title>";

				}

			}
		?>
		<script src="js/datetimepicker_css.js"></script>
		<script src="js/canvass_js.js"></script>
	<body onload="showPRENumList(), showPREDetails()">

		<form method='post' action='process_new_canvass.php'>

			<?php
			 	require("/include/header.php");
				require("/include/init_unset_values/canvass_init_value.php");

				if ( $canvassID ){
					if( $_SESSION['canvass_edit'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['canvass_new'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $canvassID ? "Edit Canvass" : "New Canvass" );?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<!-- <colgroup><col width="120px"></col><col width="220px"></col><col width="120px"></col><col width="150px"></col></col><col width="150px"></col></colgroup> -->
					<tr>
						<td> Control Number: </td>

						<td>
							<b> <?php echo $db_CanvassNumber;?> </b>
							<input type="hidden" name="hidControlNumber" value="<?php echo $db_CanvassNumber;?>">
						</td>

						<td> Canvass Date: </td>

						<td> 
							<input type="text" name="txtDate" id="txtDate" value="<?php echo ( $canvassID  ? $CanvassDate : ( is_null($initCANVASSDate) ? date('Y-m-d') : $initCANVASSDate ));?>">
						</td>

						<td>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
						<td> Division: </td>

						<td>
							<input type="radio" name="radDivision" value="Compounds" onchange="showPRENumList()" 
								<?php echo ( $canvassID  ? ( $Division == "Compounds" ? "checked" : NULL ) : ( $initCANVASSDivision == "Compounds" ? "checked" : NULL ));?>>
								<label for="Compounds"> Compounds </label>

							<input type="radio" name="radDivision" value="Corporate" onchange="showPRENumList()" 
								<?php echo ( $canvassID  ? ( $Division == "Corporate" ? "checked" : NULL ) : ( $initCANVASSDivision == "Corporate" ? "checked" : NULL ));?>>
								<label for="Corporate"> Corporate	</label>
						</td>
					</tr>

					<tr>
						<td valign="top">PRE No.:</td>

						<td>
							<select name="sltPRE[]" id="sltPRE" multiple="true" size="7" onchange="sethidPREIDs(), showPREDetails()" <?php echo ( $canvassID ? "disabled" : "" );?>>
							</select>
						</td>

						<td colspan="2">
							<label class="instruction">
								(Hold down the Ctrl button to select multiple PRE)
							</label>
						</td>
					</tr>

				</table>

				<table class="child_tables_form collapsed">

					<tbody class="tbCanvassItems" id="tbCanvassItems"></tbody>

				</table>

				<table class="comments_buttons">
					<tr>
						<td>
							<input type="submit" name="btnSaveCust" value="Save" onclick="enableMultipleSelect()">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='canvass.php?page=1&search=&qsone='">	
							<input type='hidden' name='hidCanvassID' id='hidCanvassID' value="<?php echo $canvassID;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $canvassID ? $createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedID' value='<?php echo ( $canvassID ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
							<input type='hidden' id='hidPREIDs' value='<?php echo ( $canvassID ? $PREIDsString : $initCANVASSPRE );?>'>
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
</html>
