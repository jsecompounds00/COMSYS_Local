<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast_annual.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$forecastID = $_GET['id'];

				if($forecastID)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Sales_Forecast_Annual_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $forecastID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast_annual.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						$sfai_id = array();
						$sfa_item_type = array();
						$finished_good_id = array();
						$fg = array();
						$unit_price = array();
						$january = array();
						$february = array();
						$march = array();
						$april = array();
						$may = array();
						$june = array();
						$july = array();
						$august = array();
						$september = array();
						$october = array();
						$november = array();
						$december = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$sfa_id = $row['sfa_id'];
							$forecast_year = $row['forecast_year'];
							$forex = $row['forex'];
							$customer_id = $row['customer_id'];
							$customer = $row['customer'];
							$remarks = $row['remarks'];
							$CreatedAt = $row['created_at'];
							$CreatedID = $row['created_id'];

							$sfai_id[] = $row['sfai_id'];

							$finished_good_id[] = $row['finished_good_id'];

							$fg[] = $row['fg'];

							$sfa_item_type[] = $row['sfa_item_type'];

							$unit_price[] = $row['unit_price'];

							$january[] = $row['january'];

							$february[] = $row['february'];

							$march[] = $row['march'];

							$april[] = $row['april'];

							$may[] = $row['may'];

							$june[] = $row['june'];

							$july[] = $row['july'];

							$august[] = $row['august'];

							$september[] = $row['september'];

							$october[] = $row['october'];

							$november[] = $row['november'];

							$december[] = $row['december'];

						}
						$db->next_result();
						$result->close();
					}

					$count = count($sfai_id);

		############ .............
					$qryPI = "SELECT id from comsys.sales_forecast_annual";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast_annual.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($forecastID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast_annual.php</td><td>The user tries to edit a non-existing forecast_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Sales Forecast - Edit</title>";
				}
				else{

					echo "<title>Sales Forecast - Add</title>";

				}
			}
		?>
		<script src="js/jscript.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>

		<form method='post' action='process_new_sales_forecast_annual.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");

				if ( $forecastID ){
					if( $_SESSION['edit_sales_forecast_annual'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['new_sales_forecast_annual'] == false) 

					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo( $forecastID ? "Edit " : "New " );?> Annual Sales Forecast </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table  class="parent_tables_form">
					<tr>
						<td>Forecast Year :</td>

						<td>
							<input type='text' name='txtYear' id='txtYear' value='<?php echo ( $forecastID ? $forecast_year : "" ); ?>'>
						</td>

						<td>Forex:</td>

						<td>
							<input type='text' name='txtForex' id='txtForex' value='<?php echo ( $forecastID ? $forex : "" ); ?>'>
						</td>
					</tr>

					<tr>
						<td>Customer :</td>
						<td>
							<?php
								if ( $forecastID ){
									echo $customer;
							?>		<input type='hidden' name='sltCustomer' value='<?php echo $customer_id; ?>'>
							<?php
								}else{
							?>		
									<select name='sltCustomer' id='sltCustomer' onchange="showFGSalesForecastAnnual('ALL', 0)">
										<option></option>
										<?php
											$qryC = "CALL sp_Customer_Dropdown()";
											$resultC = mysqli_query($db, $qryC);
											$processErrorC = mysqli_error($db);

											if ( !empty($processErrorC) ){
												error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast_annual.php'.'</td><td>'.$processErrorPI.' near line 157.</td></tr>', 3, "errors.php");
												header("location: error_message.html");
											}else{
												while($row = mysqli_fetch_assoc($resultC)){
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
												$db->next_result();
												$resultC->close();
											}
										?>
									</select>
							<?php
								}
							?>
						</td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th>Item Type</th>
						<th>Item Description</th>
						<th>Price</th>
						<th>Jan</th>
						<th>Feb</th>
						<th>Mar</th>
						<th>Apr</th>
						<th>May</th>
						<th>Jun</th>
						<th>Jul</th>
						<th>Aug</th>
						<th>Sep</th>
						<th>Oct</th>
						<th>Nov</th>
						<th>Dec</th>
					</tr>
					<?php
						if ( $forecastID ){
							for ( $i = 0; $i < $count; $i++ ){
					?>
								<tr valign="top">
									<td class="border_bottom">
										<input type='hidden' name='hidForecastItemID[]' value='<?php echo $sfai_id[$i]; ?>'>
										<input type='hidden' name='hidFGID[]' value='<?php echo $finished_good_id[$i]; ?>'>
										<input type='hidden' name='hidItemType[]' value='<?php echo $sfa_item_type[$i]; ?>'>
										<?php echo $sfa_item_type[$i]; ?>
									</td>
									<td class="border_bottom">
										<?php echo $fg[$i]; ?>
									</td>
									<td class="border_bottom">
										<input type='text' name='txtPrice[]' value='<?php echo $unit_price[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtJanuary[]' value='<?php echo $january[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtFebruary[]' value='<?php echo $february[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtMarch[]' value='<?php echo $march[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtApril[]' value='<?php echo $april[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtMay[]' value='<?php echo $may[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtJune[]' value='<?php echo $june[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtJuly[]' value='<?php echo $july[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtAugust[]' value='<?php echo $august[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtSeptember[]' value='<?php echo $september[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtOctober[]' value='<?php echo $october[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtNovember[]' value='<?php echo $november[$i]; ?>' class="short_text_3">
									</td>
									<td class="border_bottom">
										<input type='text' name='txtDecember[]' value='<?php echo $december[$i]; ?>' class="short_text_3">
									</td>	
								</tr>
					<?php
							}
					?>	
							<tbody id="tbAddForecastItems">
								<tr class="align_bottom">
									<td>
										<input type='button' value='+' onclick='showAddForecastItems(<?php echo $count;?>)'>
									</td>
								</tr>
							</tbody>
					<?php
						}else{
					?>	
							<tbody id='sltFGItem'>
							</tbody>
					<?php
						}
					?>
				</table>

				<table class="comments_buttons">
					<tr valign='top'>
						<td>Remarks:</td>
						<td>
							<textarea name='txtRemarks'>
							<?php
								if ( $forecastID ){
									echo $remarks;
								}else{
									echo $Remarks;
								};
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<?php
								$page = $_SESSION["page_a"];
								$fg = htmlspecialchars($_SESSION["fg_a"]);
								$year = htmlspecialchars($_SESSION["year_a"]);
								$customer = htmlspecialchars($_SESSION["customer_a"]);
								$fg = ( (strpos(($fg), "\\")+1) > 0 ? str_replace("\\", "", $fg) : $fg );
								$fg = ( (strpos(($fg), "'")+1) > 0 ? str_replace("'", "\'", $fg) : $fg );
								$year = ( (strpos(($year), "\\")+1) > 0 ? str_replace("\\", "", $year) : $year );
								$year = ( (strpos(($year), "'")+1) > 0 ? str_replace("'", "\'", $year) : $year );
								$customer = ( (strpos(($customer), "\\")+1) > 0 ? str_replace("\\", "", $customer) : $customer );
								$customer = ( (strpos(($customer), "'")+1) > 0 ? str_replace("'", "\'", $customer) : $customer );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='sales_forecast_annual.php?page=<?php echo $page;?>&fg=<?php echo $fg;?>&customer=<?php echo $customer;?>&year=<?php echo $year;?>'">
							<input type='hidden' name='hidForecastID' id='hidForecastID' value="<?php echo $forecastID;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $forecastID ? $CreatedAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $forecastID ? $CreatedID : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>