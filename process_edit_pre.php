<?php
############# Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_edit_pre.php'.'</td><td>'.$error.' near line 32.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		foreach ($_POST['hidPREID'] as $PREID) {
			$PREID = array();
			$PREID = $_POST['hidPREID'];
		}

		$count = count($PREID);

		foreach ($_POST["hidPREID"] as $preKey => $preValue) {
			$preID = $_POST["hidPREID"][$preKey];
			$receivedDate = $_POST["txtReceivedDate"][$preKey];
			$approvedDate = $_POST["txtApprovedDate"][$preKey];
			$preNumber = $_POST["hidPRENumber"][$preKey];

			$valReceivedDate = validateDate($receivedDate, 'Y-m-d');
			$valApprovedDate = validateDate($approvedDate, 'Y-m-d');

			if ( $receivedDate != '' && $valReceivedDate != 1 ){
				$errmsg_arr[] = "* Invalid received date on pre number ".$preNumber.".";
				$errflag = true;
			}

			if ( $approvedDate != '' && $valApprovedDate != 1 ){
				$errmsg_arr[] = "* Invalid approved date on pre number ".$preNumber.".";
				$errflag = true;
			}
			
		}

		############# If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("Location:edit_pre.php");
				exit();
			}

			$updatedAt = date('Y-m-d H:i:s');
			$updatedID = $_SESSION["SESS_USER_ID"];

		foreach ($_POST["hidPREID"] as $preKey => $preValue) {
			$preID = $_POST["hidPREID"][$preKey];
			$receivedDate = $_POST["txtReceivedDate"][$preKey];
			$approvedDate = $_POST["txtApprovedDate"][$preKey];
			$preNumber = $_POST["hidPRENumber"][$preKey];

			if ( $receivedDate == '' ){
				$receivedDate = NULL;
			}

			if ( $approvedDate == '' ){
				$approvedDate = NULL;
			}

			if ( $receivedDate != '' || $approvedDate != '' ){
				$qry = mysqli_prepare($db, "CALL sp_PRE_Received_Approved_Date_CRU(?, ?, ?, ?, ?)");
				mysqli_stmt_bind_param($qry, 'ssssi', $preID, $receivedDate, $approvedDate, $updatedAt, $updatedID);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry); //return results of query
				$processError = mysqli_error($db);

				if ( !empty($processError) ){
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_edit_pre.php'.'</td><td>'.$processError.' near line 90.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}else{
					$_SESSION['SUCCESS']  = 'PRE/s are updated successfully.';
					header("location: pre_monitoring.php?page=".$_SESSION["page"]."&item=".$_SESSION["item"]."&pre_number=".$_SESSION["pre_number"]."&pre_date=".$_SESSION["pre_date"]);
				}
			}
		}
	}

?>