<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_lmr.php'.'</td><td>'.$error.' near line 12.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$lmrId = $_GET['id'];

				if($lmrId)
				{ 					
					$qry = mysqli_prepare($db, "CALL sp_LMR_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $lmrId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);

					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_lmr.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						$LMRItemId = array();
						$toll_matrix_id = array();
						$entry_time = array();
						$exit_time = array();
						$toll_fee = array();
						$departure_time = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$lmrid = $row['LMRId'];
							$lmr_no = $row['lmr_no'];
							$delivery_date = $row['delivery_date'];
							$cims_dsq_id = $row['cims_dsq_id'];
							$class = $row['class'];
							$departure_time = $row['departure_time'];
							$arrival_time = $row['arrival_time'];
							$km_out = $row['km_out'];
							$km_in = $row['km_in'];
							$diesel_used = $row['diesel_used'];
							$diesel_unit_price = $row['diesel_unit_price'];
							$remarks = $row['remarks'];
							$underload_remarks = $row['underload_remarks'];
							$sample_replace_remarks = $row['sample_replace_remarks'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];

							$LMRItemId[] = $row['LMRItemId'];

							$toll_matrix_id[] = $row['toll_matrix_id'];

							$entry_time[] = $row['entry_time'];

							$exit_time[] = $row['exit_time'];

							$toll_fee[] = $row['toll_fee'];

						}
					}
					$db->next_result();
					$result->close(); 

					$departure_hour = substr($departure_time, 0, strpos($departure_time, ':'));
					$departure_min = substr($departure_time, (strpos($departure_time, ':')+1), strrpos($departure_time, ':')-3);
					$arrival_hour = substr($arrival_time, 0, strpos($arrival_time, ':'));
					$arrival_min = substr($arrival_time, (strpos($arrival_time, ':')+1), strrpos($arrival_time, ':')-3);
		############ .............
					$qryPI = "SELECT id from comsys.load_manifest";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_lmr.php'.'</td><td>'.$processErrorPI.' near line 58.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI))
						{
							$id[] = $row['id'];
						} // end while loop

					} // end if-else processErrorPI

					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($lmrId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_lmr.php</td><td>The user tries to edit a non-existing lmr_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");

					} //end !in_array
		############ .............
					$qryTM = "SELECT area from comsys.toll_matrix GROUP BY area";
					$resultTM = mysqli_query($db, $qryTM); 
					$processErrorTM = mysqli_error($db);

					if ( !empty($processErrorTM) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_lmr.php'.'</td><td>'.$processErrorTM.' near line 58.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$ref_area = array();
						while($row = mysqli_fetch_assoc($resultTM))
						{
							$ref_area[] = $row['area'];
						} // end while loop

					} // end if-else processErrorPI

					$db->next_result();
					$resultTM->close();

		############ .............	

					$vehicle_class = $cims_dsq_id."-".$class;

					echo "<title>Load Manifest - Edit</title>";

				} // end if lmrId
				else
				{
					echo "<title>Load Manifest - Add</title>";

				} //end else lmrId

			} // if-else errno

			require("/include/header.php");
			require("/include/init_value.php");

		?>
		<script src="js/jscript.js"></script>  
		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body onload='showDSQ(<?php echo ( $lmrId ? $cims_dsq_id : 0 );?>), autoSummation(), showTollPoints(<?php echo ( $lmrId ? $vehicle_class : $iDSQ );?>)'>

		<form method='post' action='process_new_lmr.php'>

			<?php
				if ( $lmrId ){
					if( $_SESSION['edit_lmr'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{
					if( $_SESSION['add_lmr'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $lmrId ? "Edit " : "New " );?> Load Manifest Report </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}// end for loop

						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);

					} // end if isset session error message
				?>

				<table class="parent_tables_form">
					<tr>
						<td>LMR No.:</td>
						<td>
							<input type='text' name='txtLMRNo' value='<?php echo ( $lmrId ? $lmr_no : $LMRNo );?>'>
						</td>
					</tr>
					<tr>
						<td>Delivery Date:</td>
						<td>
							<input type='text' name='txtDate' id='txtDate' value='<?php echo ( $lmrId ? $delivery_date : $Date );?>' 
								onchange='showDSQ(<?php echo ( $lmrId ? $cims_dsq_id : 0 );?>)' 
								<?php echo ( $lmrId ? "readonly" : "" );?>>
						</td>
						<td>
							<?php
								if ( !$lmrId ){
							?>
									<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDate')" style="cursor:pointer" name="picker" />
							<?php
								}
							?>
						</td>
					</tr>
					<tr>
						<td>DSQ:</td>
						<td>
							<select name='sltDSQ' id='sltDSQ' onchange='showTollPoints(this.value)'>
							</select>
						</td>
					</tr>
					<tr>
						<td>Departure Time:</td>
						<td>
							<input type='text' name='txtDepartureHr' class="short_text_3" maxlength="2" value='<?php echo ( $lmrId ? $departure_hour : $DepartureHr );?>'>
							:
							<input type='text' name='txtDepartureMin' class="short_text_3" maxlength="2" value='<?php echo ( $lmrId ? $departure_min : $DepartureMin );?>'>
						</td>
						<td>
							<label class="instruction">(Military Time)</label>
						</td>
					</tr>
					<tr>
						<td>Arrival Time:</td>
						<td>
							<input type='text' name='txtArrivalHr' class="short_text_3" maxlength="2" value='<?php echo ( $lmrId ? $arrival_hour : $ArrivalHr );?>'>
							:
							<input type='text' name='txtArrivalMin' class="short_text_3" maxlength="2" value='<?php echo ( $lmrId ? $arrival_min : $ArrivalMin );?>'>
						</td>
						<td>
							<label class="instruction">(Military Time)</label>
						</td>
					</tr>
					<tr>
						<td>KM Reading (In):</td>
						<td>
							<input type='text' name='txtKMReadingIn' id='txtKMReadingIn' value='<?php echo ( $lmrId ? $km_in : $KMReadingIn );?>'>
						</td>
					</tr>
					<tr>
						<td>KM Reading (Out):</td>
						<td>
							<input type='text' name='txtKMReadingOut' id='txtKMReadingOut' value='<?php echo ( $lmrId ? $km_out : $KMReadingOut );?>'>
						</td>
					</tr>
					<tr>
						<td>Diesel Consumed (L):</td>
						<td>
							<input type='text' name='txtDieselConsumed' value='<?php echo ( $lmrId ? $diesel_used : $DieselConsumed );?>'>
						</td>
					</tr>
					<tr>
						<td>Price/Liter:</td>
						<td>
							<input type='text' name='txtUnitPrice' value='<?php echo ( $lmrId ? $diesel_unit_price : $initLMRUnitPrice );?>'>
						</td>
					</tr>
				</table>

				<table class="child_tables_form">
					<tr>
						<th>Entry-Exit Pt.</th>
						<th>Entry Time</th>
						<th>Exit Time</th>
						<th>Toll Fee</th>
					</tr>
					<?php 
						if ( $lmrId ){
							$ctr = count($LMRItemId); 

							for ( $i = 0; $i < $ctr; $i++ ) { 
								$entry_hour[$i] = substr($entry_time[$i], 0, strpos($entry_time[$i], ':'));
								$entry_min[$i] = substr($entry_time[$i], (strpos($entry_time[$i], ':')+1), strrpos($entry_time[$i], ':')-3);
								$exit_hour[$i] = substr($exit_time[$i], 0, strpos($exit_time[$i], ':'));
								$exit_min[$i] = substr($exit_time[$i], (strpos($exit_time[$i], ':')+1), strrpos($exit_time[$i], ':')-3);
					?>	
								<tr>
									<input type='hidden' name='hidlmrTransItemId[]' value='<?php echo $LMRItemId[$i];?>'>	
									<td>
										<select name='sltEntryExitPt[]' id='sltEntryExitPt<?php echo $i;?>' onchange='showToll(this.value, <?php echo $i;?>)' onblur='autoSummation()'>
											<?php
												echo "<option></option> ";
												foreach ($ref_area as $key => $value) {
													echo "<optgroup label='$value'>";
														$qryEE = "CALL sp_entryExitPoint_Dropdown('$value', '$class', 1);";
														$resultEE = mysqli_query($db, $qryEE);
														$processErrorEE = mysqli_error($db);

														if (!empty($processErrorEE)){
															error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_lmr.php'.'</td><td>'.$processErrorEE.' near line 201.</td></tr>', 3, "errors.php");
														}else{
															while ( $row = mysqli_fetch_assoc($resultEE) ) {
																if ( $toll_matrix_id[$i] == $row['tollMatrixId'] )
																	echo "<option value='".$row['tollMatrixId']."' selected>".$row['entryPoint']." - ".$row['exitPoint']."</option>";
																else
																	echo "<option value='".$row['tollMatrixId']."'>".$row['entryPoint']." - ".$row['exitPoint']."</option>";
															}
															$db->next_result();
															$resultEE->close();
														}
													echo "</optgroup>";
												}
											?>
										</select>
									</td>
									<td>
										<input type='text' name='txtEntryTimeHr[]' class="short_text_3" maxlength="2" value='<?php echo $entry_hour[$i];?>'> : 
										<input type='text' name='txtEntryTimeMin[]' class="short_text_3" maxlength="2" value='<?php echo $entry_min[$i];?>'>
									</td>
									<td>
										<input type='text' name='txtExitTimeHr[]' class="short_text_3" maxlength="2" value='<?php echo $exit_hour[$i];?>'> : 
										<input type='text' name='txtExitTimeMin[]' class="short_text_3" maxlength="2" value='<?php echo $exit_min[$i];?>'>
									</td>
									<td id='txtTollfee<?php echo $i;?>'>
										<input type='text' name='txtToll[]' id='txtToll<?php echo $i;?>' value='<?php echo $toll_fee[$i];?>' readonly>
									</td>
								</tr>
					<?php
							}
						}else{
							for ( $i = 0; $i < 8; $i++ ) { 
					?>
								<tr>
									<input type='hidden' name='hidlmrTransItemId[]' value="">	
									<td>
										<select name='sltEntryExitPt[]' id='sltEntryExitPt<?php echo $i;?>' onchange='showToll(this.value, <?php echo $i;?>)' onblur='autoSummation()'>
										</select>
									</td>
									<td>
										<input type='text' name='txtEntryTimeHr[]' class="short_text_3" maxlength="2" value=''> : 
										<input type='text' name='txtEntryTimeMin[]' class="short_text_3" maxlength="2" value=''>
									</td>
									<td>
										<input type='text' name='txtExitTimeHr[]' class="short_text_3" maxlength="2" value=''> : 
										<input type='text' name='txtExitTimeMin[]' class="short_text_3" maxlength="2" value=''>
									</td>
									<td id='txtTollfee<?php echo $i;?>' class="total_value">
										<input type='hidden' value='' readonly>
									</td>
								</tr>
					<?php 
							} 
						}
					?>
					<tr valign='bottom' class="spacing">
						<td colspan='3' class="border_top " align="right"> <label class="constant"> Total: </label> </td>
						<td colspan='2' class="border_top">
							<input type='text' name='txtTotal' id='txtTotal' value='' readonly>
						</td>
					</tr>
				</table>

				<table class="comments_buttons">
					<tr>
						<td valign='top'>Remarks:</td>
						<td>
							<textarea name='txtRemarks' class="paragraph" maxlength='1000'><?php
								if ( $lmrId ){
									$remarks_array = explode("<br>", $remarks);

									foreach ($remarks_array as $remarks_key => $remarks_value) {
										echo $remarks_value."\n";
									}
								}else{
									$Remarks_array = explode("<br>", $Remarks);

									foreach ($Remarks_array as $Remarks_key => $Remarks_value) {
										echo $Remarks_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td valign='top'>Underload Incident (if any):</td>
						<td>
							<textarea name='txtUnderloadRemarks' class="paragraph" maxlength='1000'><?php
								if ( $lmrId ){
									$underload_remarks_array = explode("<br>", $underload_remarks);

									foreach ($underload_remarks_array as $underload_remarks_key => $underload_remarks_value) {
										echo $underload_remarks_value."\n";
									}
								}else{
									$UnderloadRemarks_array = explode("<br>", $UnderloadRemarks);

									foreach ($UnderloadRemarks_array as $UnderloadRemarks_key => $UnderloadRemarks_value) {
										echo $UnderloadRemarks_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td valign='top'>Sample / Replacement Delivery (if any):</td>
						<td>
							<textarea name='txtSampleReplaceRemarks' class="paragraph" maxlength='1000'><?php
								if ( $lmrId ){
									$sample_replace_remarks_array = explode("<br>", $sample_replace_remarks);

									foreach ($sample_replace_remarks_array as $sample_replace_remarks_key => $sample_replace_remarks_value) {
										echo $sample_replace_remarks_value."\n";
									}
								}else{
									$SampleReplaceRemarks_array = explode("<br>", $SampleReplaceRemarks);

									foreach ($SampleReplaceRemarks_array as $SampleReplaceRemarks_key => $SampleReplaceRemarks_value) {
										echo $SampleReplaceRemarks_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" name="btnSaveLMR" value="Save">
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='lmr.php?page=1&search=&qsone='">
							<input type='hidden' name='hidLMRId' value="<?php echo $lmrId;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $lmrId ? $createdAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $lmrId ? $createdId : $_SESSION["SESS_USER_ID"] );?>'>
						</td>	
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>