<html>
	<head>
		<title> Property Transfer History</title>
		<?php
			require("include/database_connect.php");

			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone='';

			$propertyID = $_GET['id'];

			############ .............
			$qryL = mysqli_prepare($db, "CALL sp_Company_Properties_Query(?)");
			mysqli_stmt_bind_param($qryL, 'i', $propertyID);
			$qryL->execute();
			$resultL = mysqli_stmt_get_result($qryL);
			$rowL = mysqli_fetch_assoc($resultL);
			$propertyName = $rowL['property_name'];
			$db->next_result();
			$resultL->close();

			############ .............

			$qryPI = "SELECT id from comsys.company_properties";
			$resultPI = mysqli_query($db, $qryPI); 
			$processErrorPI = mysqli_error($db);

			if ( !empty($processErrorPI) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>property_transfer_history.php'.'</td><td>'.$processErrorPI.' near line 57.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
					$id = array();
				while($row = mysqli_fetch_assoc($resultPI)){
					$id[] = $row['id'];
				}
			}
			$db->next_result();
			$resultPI->close();

			############ .............
			if( !in_array($propertyID, $id, TRUE) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>property_transfer_history.php</td><td>The user tries to view transaction history of a non-existing property_id.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		?>
	</head>
	<body>

		<?php
			require ('/include/header.php');
			// require ("include/unset_value.php");

			if( $_SESSION['property_transfer_history'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$page_a = $_SESSION["page"];
			$search_a = htmlspecialchars($_SESSION["search"]);
			$qsone_a = htmlspecialchars($_SESSION["qsone"]);
			$search_a = ( (strpos(($search_a), "\\")+1) > 0 ? str_replace("\\", "", $search_a) : $search_a );
			$search_a = ( (strpos(($search_a), "'")+1) > 0 ? str_replace("'", "\'", $search_a) : $search_a );
			$qsone_a = ( (strpos(($qsone_a), "\\")+1) > 0 ? str_replace("\\", "", $qsone_a) : $qsone_a );
			$qsone_a = ( (strpos(($qsone_a), "'")+1) > 0 ? str_replace("'", "\'", $qsone_a) : $qsone_a );
		?>

		<input type='hidden' name='page' value="<?php echo $page;?>">

		<div class="wrapper">

			<span> <h2> Transaction History for <?php echo $propertyName; ?> </h2> </span>

			<a class='back' href='property_transfer.php?page=<?php echo $page_a;?>&search=<?php echo $search_a;?>&qsone=<?php echo $qsone_a;?>'><img src='images/back.png' height="20" name='txtBack'> Back</a>
			
			<?php
				$qryCPH = mysqli_prepare($db, "CALL sp_Property_Transfer_History(?, NULL, NULL)");
				mysqli_stmt_bind_param($qryCPH, 'i', $propertyID);
				$qryCPH->execute();
				$resultCPH = mysqli_stmt_get_result($qryCPH);
				$total_results = mysqli_num_rows($resultCPH); //return number of rows of result

				$db->next_result();
				$resultCPH->close();

				$targetpage = "property_transfer_history.php"; 	//your file name  (the name of this file)
				require("include/paginate_hist.php");

				$qry = mysqli_prepare($db, "CALL sp_Property_Transfer_History(?, ?, ?)");
				mysqli_stmt_bind_param($qry, 'iii', $propertyID, $start, $end);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry);

				$processError = mysqli_error($db);
				
				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>property_transfer_history.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					if( isset($_SESSION['SUCCESS'])) 
					{
						echo '<ul id="success">';
						echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
						echo '</ul>';
						unset($_SESSION['SUCCESS']);
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='13'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
							<th>Type</th>
							<th>Asset Condition</th>
							<th>Asset Specification</th>
							<th>Quantity Received</th>
							<th>Unit Price</th>
							<th>Inventory Source</th>
							<th>Reference No.</th>
							<th>Reference Date</th>
							<th>Received From</th>
							<th>Issued To</th>
							<th>Remarks</th>
							<th></th>
						</tr>
						<?php
							if(!empty($processError))
							{
								error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>property_transfer_history.php'.'</td><td>'.$processError.' near line 120.</td></tr>', 3, "errors.php");
								header("location: error_message.html");
							}
							else
							{
								while($row = mysqli_fetch_assoc($result)){
									$InventoryTransactionID = $row['InventoryTransactionID'];
									$asset_condition = $row['asset_condition'];
									$inventory_type = $row['inventory_type'];
									$inventory_source = $row['inventory_source'];
									$reference_number = $row['reference_number'];
									$reference_date = $row['reference_date'];
									$ReceivedFrom = $row['ReceivedFrom'];
									$IssuedTo = $row['IssuedTo'];
									$remarks = $row['remarks'];
									$specifications = $row['specifications'];
									$quantity_received = $row['quantity_received'];
									$unit_price = $row['unit_price'];
							?>	
									<tr>
										<td>
											<?php
												if ( $inventory_type == 'sub' ){
											?> 
													<label style='background-color:#ffeaea; padding:3px;'> Deduct </label>
											<?php
												}else{
											?>
													<label style='background-color:#e3fbe9;padding:3px;'> Add </label>
											<?php
												}
											?>
										</td>
										<td> <?php echo $asset_condition; ?> </td>
										<td> <?php echo $specifications; ?> </td>
										<td> <?php echo $quantity_received; ?> </td>
										<td> <?php echo $unit_price; ?> </td>
										<td> <?php echo $inventory_source; ?> </td>
										<td> <?php echo $reference_number; ?> </td>
										<td> <?php echo $reference_date; ?> </td>
										<td> 
											<?php
												if ( $inventory_type == 'add' ){
											?>
													<?php echo $ReceivedFrom; ?> </td>
											<?php
												}
											?>
										</td>
										<td> <?php echo $IssuedTo; ?> </td>
										<td> <?php echo $remarks; ?> </td>
										<td> 
											<?php
												if ( $inventory_type == 'sub' ){

													if(array_search(99, $session_Permit)){
											?>
													<input type='button' value='Edit' onclick="location.href='deduct_property_inventory.php?id=<?php echo $InventoryTransactionID; ?>'">
											<?php
													$_SESSION['property_trans_edit'] = true;
												}else{
													unset($_SESSION['property_trans_edit']);
												}

											}else{

												if(array_search(99, $session_Permit)){
											?>
													<input type='button' value='Edit' onclick="location.href='add_property_inventory.php?id=<?php echo $InventoryTransactionID; ?>'">
											<?php
													$_SESSION['property_trans_edit'] = true;
												}else{
													unset($_SESSION['property_trans_edit']);
												}

											}
											?>
										</td>
									</tr>
						<?php
								}
								$db->next_result();
								$result->close();
							}

						?>
						<tr>
							<td colspan='13'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>