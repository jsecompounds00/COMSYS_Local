<html>
	<head>
		<title>New Raw Materials - Home</title>
		<?php
			require("/include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>
		<?php
			require("/include/header.php");
			require("/include/init_unset_values/nrm_unset_value.php");

			if( $_SESSION['nrm_home'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>	

		<div class="wrapper">

			<span> <h3> Request for New Raw Materials Evaluation </h3> </span>

			<div class="search_box">
				<form method="get" action="nrm.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_form">
						<tr>
							<td> NRM Date: </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>"> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
							<td> NRM No.: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td> <input type="button" name="btnAddNRM" value="New NRM" onclick="location.href='new_nrm.php?id=0'"> </td>
							<td> <input type="button" value="Add Recommendation" onclick="location.href='new_nrm_recommendation.php'"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>nrm.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_NRM_Evaluation_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'ss', $search, $qsone);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "nrm.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_NRM_Evaluation_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssii', $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>nrm.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='7'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>NRM Date</th>
							    <th>NRM No.</th>
							    <th>Material Description</th>
							    <th>Classification</th>
							    <th>Sample Description</th>
							    <th>Documents</th>
							    <th></th>
							</tr>
							<?php
								while( $row = mysqli_fetch_assoc( $result ) )
								{
									$id	= $row["id"];
									$nrm_no = $row["nrm_no"];
									$nrm_date = $row["nrm_date"];
									$material_description = $row["material_description"];
									$classification = $row["classification"];
									$sample_description = $row["sample_description"];
									$Documents = $row["Documents"];
							?>
									<tr>
										<td> <?php echo $nrm_date;?> </td>
										<td> <?php echo $nrm_no;?> </td>
										<td> <?php echo $material_description;?> </td>
										<td> <?php echo $classification;?> </td>
										<td> <?php echo $sample_description;?> </td>
										<td> <?php echo $Documents;?> </td>
										<td>
											<input type='button' value='Edit' onclick="location.href='new_nrm.php?id=<?php echo $row['id'];?>'">
										</td>
									</tr>
							<?php
								}
							?>
							<tr>
								<td colspan='7'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>