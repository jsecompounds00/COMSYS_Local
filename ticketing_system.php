<html>
	<head>
		<script src="js/datetimepicker_css.js"></script>
		<script src="js/tickets_js.js"></script>  
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> <!--  -->
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);

			$ticket = "ticketing_system.php?page=1&search=RECEIVED&qsone=";
			$sec = "300";
		?>
			<meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $ticket?>'">
		<?php
			$qryTC = mysqli_prepare($db, "CALL sp_Ticket_Count(?)");
			mysqli_stmt_bind_param($qryTC, 'i', $_SESSION["SESS_USER_ID"]);
			$qryTC->execute();
			$resultTC = mysqli_stmt_get_result($qryTC);
			while ( $row = mysqli_fetch_assoc( $resultTC ) ){
				$unread = $row["TotalUnread"];
			}
			$db->next_result();
			$resultTC->close();
		?>

		<title>Tickets <?php echo ( $unread == 0 ? "" : "(".$unread.")" ) ?> </title>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/unset_value.php");
			

			if( $_SESSION['ticketing_system'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION['page'] = $_GET['page'];
			$_SESSION['search'] = $_GET['search'];
			$_SESSION['qsone'] = $_GET['qsone'];
		?>

		<div class="wrapper">

			<span> <h3> Tickets </h3> </span>

			<div class="search_box">
				<form method="get" action="ticketing_system.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_form">
						<tr>
							<td> Request Date: </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>"> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
							<td>
								<select name="search">
									<option value="ALL">ALL</option>
									<option value="RECEIVED" <?php echo ( $_GET['search'] == "RECEIVED" ? "selected" : "" ); ?>>RECEIVED</option>
									<option value="SENT" <?php echo ( $_GET['search'] == "SENT" ? "selected" : "" ); ?>>SENT</option>
								</select>
							</td>  
							<td> <input type="submit" value="Search"> </td>
							<td> <input type="button" name="btnAddRequest" value="New Ticket" onclick="location.href='new_ticket.php?id=0'"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>ticketing_system.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Ticket_Home(?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'ssi', $qsone, $search, $_SESSION["SESS_USER_ID"]);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM);

					$total_results = mysqli_num_rows($resultCM);

					$db->next_result();
					$resultCM->close();

					$targetpage = "ticketing_system.php"; 
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Ticket_Home(?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssiii', $qsone, $search, $_SESSION["SESS_USER_ID"], $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>ticketing_system.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='7'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
								<?php echo ( $_GET["search"] == "ALL" ? "<th></th>" : "" ) ?>
							    <th>Request Date</th>
							    <th>Subject</th>
							    <th>Request</th>
							    <th>Recipient</th>
							    <th>Sender</th>
							    <th>Status</th>
							    <th></th>
							</tr>
							<?php
								$i = 0;
								while ( $row = mysqli_fetch_assoc( $result ) ){
									$ticketID = $row["ticketID"];
									$request_date = $row["request_date"];
									$subject = $row["subject"];
									$request = $row["request"];
									$Recipient = $row["Recipient"];
									$Sender = $row["Sender"];
									$request_type = $row["request_type"];
									$status = $row["status"];
									$attention_to_id = $row["attention_to_id"];
									$viewed = $row["viewed"];
							?>	
									<tr valign="top">
										<?php echo ( $_GET["search"] == "ALL" ? "<td> $request_type </td>" : "" ) ?>
										<td> 
											<?php 
												if ( $_SESSION["SESS_USER_ID"] == $attention_to_id && $viewed == 0 ){
													echo "<b>".$request_date."</b>";
												}else{
													echo $request_date; 
												}
											?> 
										</td>
										<td> 
											<?php 
												if ( $_SESSION["SESS_USER_ID"] == $attention_to_id && $viewed == 0 ){
													echo "<b>".$subject."</b>";
												}else{
													echo $subject; 
												}
											?> 
										</td>
										<td> 
											<?php 
												if ( $_SESSION["SESS_USER_ID"] == $attention_to_id && $viewed == 0 ){
													echo "<b>".$request."</b>";
												}else{
													echo $request; 
												}
											?> 
										</td>
										<td> 
											<?php 
												if ( $_SESSION["SESS_USER_ID"] == $attention_to_id && $viewed == 0 ){
													echo "<b>".$Recipient."</b>";
												}else{
													echo $Recipient; 
												}
											?> 
										</td>
										<td>
											<?php 
												if ( $_SESSION["SESS_USER_ID"] == $attention_to_id && $viewed == 0 ){
													echo "<b>".$request_date."</b>";
												}else{
													echo $request_date; 
												}
											?> 
										</td>
										<td> 
											<?php 
												if ( is_null( $status ) ){
													if ( $_SESSION["SESS_USER_ID"] == $attention_to_id ){
											?>			<input type="checkbox" name="chkDone" id="chkDone<?php echo $i;?>" onchange="confirmStatus(<?php echo $ticketID;?>,<?php echo $i;?>)" value="Done"> 
															<label for="chkDone<?php echo $i;?>"> Done</label>
											<?php
													}else{
														echo "Pending";
													}
												}else{
													echo $status;
												}
											?> 
										</td>
										<td> <input type='button' name='btnAddRequest' value='View Details' onclick="location.href='new_ticket.php?id=<?php echo $ticketID; ?>'"> </td>
									</tr>
							<?php
									$i++;
								}
									$db->next_result();
									$result->close();
							?>
							<tr>
								<td colspan='7'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
				// phpinfo();
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
