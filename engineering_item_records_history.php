<html>
	<head>
		<title>Engineering Item Records - History</title>
		<?php
			require("include/database_connect.php");

			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone= '';

			$supplyId = $_GET['id'];

			$qryS = mysqli_prepare($db, "CALL sp_Supplies_Query(?)");
			mysqli_stmt_bind_param($qryS, 'i', $supplyId);
			$qryS->execute();
			$resultS = mysqli_stmt_get_result($qryS);

			$processError1 = mysqli_error($db);
			$rowS = mysqli_fetch_assoc($resultS);
			$supplyname = $rowS['supply'];
			$db->next_result();
			$resultS->close();

			$qryPI = "SELECT id from comsys.supplies";
			$resultPI = mysqli_query($db, $qryPI); 
			$processErrorPI = mysqli_error($db);

			if ( !empty($processErrorPI) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>engineering_item_records_history.php'.'</td><td>'.$processErrorPI.' near line 65.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
					$id = array();
				while($row = mysqli_fetch_assoc($resultPI)){
					$id[] = $row['id'];
				}
			}
			$db->next_result();
			$resultPI->close();

			if( !in_array($supplyId, $id, TRUE) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>supply_history.php</td><td>The user tries to view transaction history of a non-existing supply_id.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		?>
	</head>
	<body>

		<?php
			require ('/include/header.php');
			require ("include/unset_value.php");

			// if( $_SESSION['engineering_item_records_history'] == false) 
			// {
			// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }
		?>

		<input type='hidden' name='page' value='<?php echo $page; ?>'>

		<div class="wrapper">	
			
			<span> <h2> Record History for <?php echo $supplyname;?> </h2> </span>

			<a class='back' href='engineering_item_records.php?page=1&search=&qsone='><img src='images/back.png' height="20" name='txtBack'> Back</a>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>engineering_item_records_history.php'.'</td><td>'.$error.' near line 38.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryL = mysqli_prepare($db, "CALL sp_Engineering_Item_Record_History(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryL, 'i', $supplyId);
					$qryL->execute();
					$resultL = mysqli_stmt_get_result($qryL);
					$total_results = mysqli_num_rows($resultL); //return number of rows of result

					$db->next_result();
					$resultL->close();

					$targetpage = "engineering_item_records_history.php"; 	//your file name  (the name of this file)
					require("include/paginate_hist.php");

					$qry = mysqli_prepare($db, "CALL sp_Engineering_Item_Record_History(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'iii', $supplyId, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);

					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>engineering_item_records_history.php'.'</td><td>'.$processError.' near line 103.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th width="100px">Record Date</th>
							    <th>Comments</th>
							    <th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)){
									$record_id = $row['id'];
									$record_date = $row['record_date'];
									$comments = $row['comments'];
							?>
									<tr>
										<td> <?php echo $record_date;?> </td>
										<td> <?php echo $comments;?> </td>
										<td>
											<input type='button' value='Edit' onclick="location.href='new_engineering_item_records.php?id=<?php echo $record_id;?>'">
										</td>
									</tr>
							<?php
								}
									$db->next_result();
									$result->close();
							?>
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>