<html>
	<head>
		<title>Batch Ticket - Home</title>
		<?php
			require("include/database_connect.php");
			$id = $_GET["id"];
			$tid = $_GET["tid"];
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			// require("/include/unset_value.php");	
		?>

		<div class="wrapper">

			<span> <h3> Batch Ticket for: </h3> </span>

			<table class="parent_tables_form">
				<tr>
					<td>
						<input type='button' value='NRM' onclick="location.href='new_batch_ticket_nrm.php?id=<?php echo $id;?>&tid=<?php echo $tid;?>'">
					</td>
					<td>
						<input type='button' value='JRD' onclick="location.href='new_batch_ticket_jrd.php?id=<?php echo $id;?>&tid=<?php echo $tid;?>'">
					</td>
					<td>
						<input type='button' value='Cancel' onclick="location.href='batch_ticket.php?page=1&search=&qsone='">
					</td>
				</tr>
			</table>
			
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>