<html>
	<head>
		<title>Customer - Home</title>
		<?php 
			require("include/database_connect.php");
			
			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>
		<?php
			require ("/include/header.php");
			require ("/include/init_unset_values/customers_unset_value.php");

			if( $_SESSION["customer"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Customers </h3> </span>

			<div class="search_box">
				<form method="get" action="customer.php">
					<input type="hidden" name="page" value="<?php echo $_GET["page"]; ?>">
					<input type="hidden" name="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]); ?>">
					<table class="search_tables_form">
						<tr>
							<td> Name: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>customer.php"."</td><td>".$error." near line 42.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryC = mysqli_prepare($db, "CALL sp_Customer_Home(?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryC, "sii", $search, $compounds, $corporate);
					$qryC->execute();
					$resultC = mysqli_stmt_get_result($qryC);
					$total_results = mysqli_num_rows($resultC); //return number of rows of result

					$db->next_result();
					$resultC->close();

					$targetpage = "customer.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Customer_Home(?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, "siiii", $search, $compounds, $corporate, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>customer.php"."</td><td>".$processError." near line 63.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th> Name </th>
							    <th> Active? </th>
							    <th> RM </th>
							    <th> FG </th>
							    <th> Supplies </th>
								<th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result))
								{
									$custId = $row["id"];
									$custName = $row["name"];
									$active = ( $row["active"] ? "Y" : "N" );
									$rm = ( $row["rm"] ? "Y" : "N" );
									$fg = ( $row["fg"] ? "Y" : "N" );
									$supplies = ( $row["supplies"] ? "Y" : "N" );
							?>		
									<tr>
										<td> <?php echo $custName; ?> </td>
										<td> <?php echo $active; ?> </td>
										<td> <?php echo $rm; ?> </td>
										<td> <?php echo $fg; ?> </td>
										<td> <?php echo $supplies; ?> </td>
										<td>
										<?php
												if(array_search(24, $session_Permit)){
										?>
													<input type="button" name="btnEdit" value="EDIT" style="width: 50px" onclick="location.href='<?php echo PG_NEW_CUSTOMER.$row["id"];?>'">
										<?php
													$_SESSION["edit_customer"] = true;
												}else{
													unset($_SESSION["edit_customer"]);
												}
										?>
										</td>
									</tr>
							<?php 
								}
								$db->next_result();
								$result->close();
							?>
							<tr>
								<td colspan="6">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>