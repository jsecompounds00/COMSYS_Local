<html>
	<head>
		<title> Sales Quote - Home </title>
		<?php
			require("include/database_connect.php");

			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/unset_value.php");
			

			if( $_SESSION['sales_quote'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Sales Quote </h3> </span>

			<div class="search_box">
				<form method="get" action="sales_quote.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_form">
						<tr>
							<td> <label>Customer:</label> </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <label>YearMonth (YYYYMM):</label> </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<input type="button" name="btnAddQuote" value="New Quote" onclick="location.href='new_sales_quote.php?id=0'">
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>sales_quote.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Quote_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'si', $search, $qsone);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "sales_quote.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Quote_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'siii', $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>sales_quote.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='16'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>Date</th>
							    <th>Customer</th>
							    <th></th>
							</tr>
							<?php
								while ( $row = mysqli_fetch_assoc( $result ) ){
									$quote_id = $row["quote_id"];
									$quote_date = $row["quote_date"];
									$customer_name = $row["customer_name"];
							?>	
									<tr>
										<td> <?php echo $quote_date;?> </td>
										<td> <?php echo $customer_name;?> </td>
										<td>
											<input type="button" value="Edit" onclick="location.href='new_sales_quote.php?id=<?php echo $quote_id;?>'">
										</td>
									</tr>
							<?php
								}
								$db->next_result();
								$result->close();
							?>
							<tr>
								<td colspan='16'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>