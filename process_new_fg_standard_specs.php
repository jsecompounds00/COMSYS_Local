<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_fg_standard_specs.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidFGID = $_POST['hidFGID'];
		$hidStandardSpecsID = $_POST['hidStandardSpecsID'];
		$hidFGSoft = $_POST['hidFGSoft'];

		$txtFromSpecificGravity = $_POST['txtFromSpecificGravity'];
		$txtToSpecificGravity = $_POST['txtToSpecificGravity'];

		$txtFromHardnessA = $_POST['txtFromHardnessA'];
		$txtToHardnessA = $_POST['txtToHardnessA'];

		$txtFromHardnessD = $_POST['txtFromHardnessD'];
		$txtToHardnessD = $_POST['txtToHardnessD'];

		$txtVolumeResistivity = $_POST['txtVolumeResistivity'];
		$txtUnagedTS = $_POST['txtUnagedTS'];
		$txtOvenTS = $_POST['txtOvenTS'];
		$txtOilTS = $_POST['txtOilTS'];
		$txtUnagedE = $_POST['txtUnagedE'];
		$txtOvenE = $_POST['txtOvenE'];
		$txtOilE = $_POST['txtOilE'];

########## Input Validations
		if ( empty( $txtFromSpecificGravity ) || empty( $txtToSpecificGravity ) ){
			$errmsg_arr[] = '* Specific gravity can\'t be blank.';
			$errflag = true;
		}
		if ( $hidFGSoft == 1 ){        
			if ( empty( $txtFromHardnessA ) || empty( $txtToHardnessA ) ){
				$errmsg_arr[] = '* Hardness can\'t be blank.';
				$errflag = true;
			}
			if ( empty( $txtVolumeResistivity ) ){
				$errmsg_arr[] = '* Volume resistivity can\'t be blank.';
				$errflag = true;
			}
			if ( empty( $txtUnagedTS ) ){
				$errmsg_arr[] = '* Tensile strength (unaged) can\'t be blank.';
				$errflag = true;
			}
			if ( empty( $txtUnagedE ) ){
				$errmsg_arr[] = '* Elongation (unaged) can\'t be blank.';
				$errflag = true;
			}
			if ( $txtOvenTS == '' ){
				$txtOvenTS = NULL;				
			}
			if ( $txtOvenE == '' ){
				$txtOvenE = NULL;				
			}
			if ( $txtOilTS == '' ){
				$txtOilTS = NULL;				
			}
			if ( $txtOilE == '' ){
				$txtOilE = NULL;
			}
			// if ( $hidOvenAging ){
			// 	if ( !is_numeric( $txtOvenTS ) ){
			// 		$errmsg_arr[] = '* Invalid value for tensile strength (oven aged).';
			// 		$errflag = true;
			// 	}
			// 	if ( !is_numeric( $txtOvenE ) ){
			// 		$errmsg_arr[] = '* Invalid value for elongation (oven aged).';
			// 		$errflag = true;
			// 	}
			// }else{
			// 	$txtOvenTS = NULL;
			// 	$txtOvenE = NULL;
			// }
			// if ( $hidOilAging == 1 ){
			// 	if ( !is_numeric( $txtOilTS ) ){
			// 		$errmsg_arr[] = '* Invalid value for tensile strength (oil aged).';
			// 		$errflag = true;
			// 	}
			// 	if ( !is_numeric( $txtOilE ) ){
			// 		$errmsg_arr[] = '* Invalid value for elongation (oil aged).';
			// 		$errflag = true;
			// 	}
			// }else{
			// 	$txtOilTS = NULL;
			// 	$txtOilE = NULL;
			// }

			$txtFromHardnessD = NULL;
			$txtToHardnessD = NULL;
		}else{
			if ( empty( $txtFromHardnessD ) || empty( $txtToHardnessD ) ){
				$errmsg_arr[] = '* Hardness can\'t be blank.';
				$errflag = true;

				$txtFromHardnessA = NULL;
				$txtToHardnessA = NULL;
				$txtVolumeResistivity = '';
				$txtUnagedTS = NULL;
				$txtUnagedE = NULL;
				$txtOvenTS = NULL;
				$txtOvenE = NULL;
				$txtOilTS = NULL;
				$txtOilE = NULL;
			}

		}

		if($hidFGID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidFGID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# SESSION, keeping last input value
		$_SESSION['SESS_FGSPECS_FromSpecificGravity'] = $txtFromSpecificGravity;
		$_SESSION['SESS_FGSPECS_ToSpecificGravity'] = $txtToSpecificGravity;
		$_SESSION['SESS_FGSPECS_FromHardnessA'] = $txtFromHardnessA;
		$_SESSION['SESS_FGSPECS_ToHardnessA'] = $txtToHardnessA;
		$_SESSION['SESS_FGSPECS_FromHardnessD'] = $txtFromHardnessD;
		$_SESSION['SESS_FGSPECS_ToHardnessD'] = $txtToHardnessD;
		$_SESSION['SESS_FGSPECS_VolumeResistivity'] = $txtVolumeResistivity;
		$_SESSION['SESS_FGSPECS_UnagedTS'] = $txtUnagedTS;
		$_SESSION['SESS_FGSPECS_OvenTS'] = $txtOvenTS;
		$_SESSION['SESS_FGSPECS_OilTS'] = $txtOilTS;
		$_SESSION['SESS_FGSPECS_UnagedE'] = $txtUnagedE;
		$_SESSION['SESS_FGSPECS_OvenE'] = $txtOvenE;
		$_SESSION['SESS_FGSPECS_OilE'] = $txtOilE;

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_fg_standard_specs.php?id=$hidFGID");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_BAT_FG_Standard_Specs_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
	mysqli_stmt_bind_param($qry, 'iiddiiiisiiiiiissii', $hidStandardSpecsID, $hidFGID
												   , $txtFromSpecificGravity, $txtToSpecificGravity
												   , $txtFromHardnessA, $txtToHardnessA
												   , $txtFromHardnessD, $txtToHardnessD
												   , $txtVolumeResistivity, $txtUnagedTS, $txtUnagedE, $txtOvenTS
												   , $txtOvenE, $txtOilTS, $txtOilE
										   		   , $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_fg_standard_specs.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		if($hidFGID)
			$_SESSION['SUCCESS']  = 'Successfully updated finished goods standard specification.';
		else
			$_SESSION['SUCCESS']  = 'Successfully added new finished goods standard specification.';
		//echo $_SESSION['SUCCESS'];
		header("location: finished_goods.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
	}	
############# Committing in Database

		require("include/database_close.php");

	}
?>