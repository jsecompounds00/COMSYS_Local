<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_supply_type.php"."</td><td>".$error." near line 9.</td></tr></tbody>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$supplyTypeId = $_GET["id"];

				if($supplyTypeId)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_SupplyType_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $supplyTypeId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_supply_type.php"."</td><td>".$processError." near line 35.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$supplyTId = $row["id"];
							$supplyType = htmlspecialchars($row["supply_type"]);
							$active = $row["active"];
							$comments = htmlspecialchars($row["comments"]);
							$createdAt = $row["created_at"];
							$createdId = $row["created_id"];
						}
						$db->next_result();
						$result->close();
					}

					############ .............
					$qryPI = "SELECT id from comsys.supply_type";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_supply_type.php"."</td><td>".$processErrorPI." near line 62.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($supplyTypeId, $id, TRUE) ){
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_supply_type.php</td><td>The user tries to edit a non-existing supply_type_id.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					
					echo "<title>Supply Type - Edit</title>";
				}
				else
				{

					echo "<title>Supply Type - Add</title>";

				}
			}
		?>
	</head>
	<body>
		<form method="post" action="process_new_supply_type.php">
			<?php
				require("/include/header.php");
				require("/include/init_unset_values/supply_types_init_value.php");

				if ( $supplyTypeId ){
					if( $_SESSION['edit_supplytype'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{	
					if( $_SESSION['add_supplytype'] == false) 
					{
						$_SESSION['ERRMSG_ARR'] ='Access denied!';
						session_write_close();
						header("Location:comsys.php");
						exit();
					}				
				}

			?>
			<div class="wrapper">

				<span> <h3> <?php echo ( $supplyTypeId ? "Edit ".$supplyType : "New Supply Types" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Supply Type:</td>
						<td>
							<input type="text" name="txtNewSupplyType" value="<?php echo ( $supplyTypeId ? $supplyType : $initSPTNewSupplyType ) ?>">
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type="checkbox" name="chkActive" <?php echo ( $supplyTypeId ? ( $active ? "checked" : "" ) : ( $initSPTActive ? "checked" : "" ) ); ?>>
						</td>
					</tr>
					<tr>
						<td valign="top">Comments:</td>
						<td>
							<textarea name="txtComments"><?php
								if ( $supplyTypeId ){
									echo $comments;
								}else{
									echo $initSPTComments;
								}
							?></textarea>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveSupplyType" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='supply_type.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidSupplyTypeId" value="<?php echo $supplyTypeId;?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $supplyTypeId ? $createdAt : date('Y-m-d H:i:s') ); ?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $supplyTypeId ? $createdId : $_SESSION["SESS_USER_ID"] ); ?>">
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>