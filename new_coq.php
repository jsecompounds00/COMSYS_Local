<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_coq.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$fgid = $_GET['id'];

				if($fgid)
				{ 
					
					$qryFG = "SELECT name from comsys.finished_goods where id = $fgid" ;
					$resultFG = mysqli_query($db, $qryFG); 
					$processErrorFG = mysqli_error($db);

					if ( !empty($processErrorFG) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_coq.php'.'</td><td>'.$processErrorFG.' near line 57.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						while($row = mysqli_fetch_assoc($resultFG))
						{
							$name = $row['name'];
						}
					}
					$db->next_result();
					$resultFG->close();

					########### .............
					$qryPI = "SELECT id from comsys.finished_goods";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_coq.php'.'</td><td>'.$processErrorPI.' near line 57.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI))
						{
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($fgid, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_coq.php</td><td>The user tries to edit a non-existing fgid.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Certificate of Qualities - Search</title>";
				}else{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_coq.php</td><td>The user tries to edit a non-existing fgid.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				
			}
		?>
		<script src="js/jscript.js"></script>  
	</head>
	<body>

		<?php 
			require("/include/header.php");
			require("/include/init_value.php");

			if ( $fgid ){
				if( $_SESSION['update_coq'] == false) 
				{
					$_SESSION['ERRMSG_ARR'] ='Access denied!';
					session_write_close();
					header("Location:comsys.php");
				}
			}
		?>

		<div class="wrapper">
			
			<?php
				if ( $fgid ){
					echo "<span> <h3> Search COQ for ".$name." </h3> </span>";
				}

				if( isset($_SESSION['SUCCESS'])) 
				{
					echo '<ul id="success">';
					echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
					echo '</ul>';
					unset($_SESSION['SUCCESS']);
				}
			?>

			<table class="parent_tables_form">
				<tr>
				    <td >Lot Numbers:
						<select name="sltLotNum" id="sltLotNum" onchange="showCOQ('<?php echo $fgid;?>')">
							<option ></option>
					    	<?php
					    		$qryLD = mysqli_prepare( $db, "CALL sp_LotNumber_Dropdown(?)" );
					    		mysqli_stmt_bind_param( $qryLD , 'i', $fgid );
					    		$qryLD->execute();
					    		$resultLD = mysqli_stmt_get_result( $qryLD );
					    		$processErrorLD = mysqli_error($db);

					    		if ( !empty($processErrorLD) ){

									error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_coq.php'.'</td><td>'.$processErrorLD.' near line 88.</td></tr>', 3, "errors.php");
									header("location: error_message.html");

								}else{

									while ( $row = mysqli_fetch_assoc($resultLD) ){

										echo "<option value='".$row['lot_number']."'>".$row['lot_number']."</option>";

									}

									$db->next_result();
									$resultLD->close();
								}
					    	?>
					    </select>
				    </td>
				</tr>
			</table>
			<div id='coqdd'>
			</div>
			<table class="parent_tables_form">
				<tr class="align_bottom">
					<td>
						<?php
							$page = $_SESSION["page"];
							$search = htmlspecialchars($_SESSION["search"]);
							$qsone = htmlspecialchars($_SESSION["qsone"]);
							$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
							$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
							$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
							$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
						?>
						<input type='button' name='btnCancel' value='Cancel' onclick="location.href='finished_goods.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
					</td>
				</tr>
			</table>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>