<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_schedule.php'.'</td><td>'.$error.' near line 8.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$equipmentId = $_GET['id'];
				$v = $_GET['v'];
				$c = $_GET['c'];

				if($equipmentId)
				{ 
					############ .............

					$qryPI = "SELECT id from comsys.equipment";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_schedule.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					if( !in_array($equipmentId, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_schedule.php</td><td>The user tries to add a schedule for a non-existing equipment_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					############ .............

					$qry = mysqli_prepare( $db, "CALL sp_Equipment_Query( ? )" );
					mysqli_stmt_bind_param( $qry, 'i', $equipmentId );
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_schedule.php'.'</td><td>'.$processError.' near line 27.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$equipmentId = $row['id'];
							$name = $row['name'];
						}
					}
					$db->next_result();
					$result->close();
				}else{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_schedule.php</td><td>The user tries to edit a non-existing equipment_id.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
			}
		?>
		<title> Add Schedule </title>
		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>

		<form method='post' action='process_new_schedule.php'>

			<?php
				require ('/include/header.php');
				require ('/include/init_value.php');
			?>

			<div class="wrapper">

				<span> <h3> New Schedule for <?php echo $name;?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Schedule Type:</td>
						<td>
							<select name='sltSchedType'>
								<option value='Calibration' <?php echo ( $c == 1 && $v == 0 ? "selected" : "hidden" );?>>Calibration</option>
								<option value='Validation' <?php echo ( $v == 1 && $c == 0 ? "selected" : "hidden" );?>>Validation</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Date:</td>
						<td>
							<input type='text' name='txtDate' id='txtDate' value='<?php echo $Date;?>'>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<input type="submit" name="btnSaveSchedule" value="Save">
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='<?php echo PG_EQUIPMENT_HOME;?>'">
							<input type='hidden' name='hidScheduleId' value='0'>
							<input type='hidden' name='hidCreatedAt' value='<?php echo date('Y-m-d H:i:s');?>'>
							<input type='hidden' name='hidEquipmentId' value='<?php echo $equipmentId;?>'>
							<input type='hidden' name='hidCal' value='<?php echo $c;?>'>
							<input type='hidden' name='hidVal' value='<?php echo $v;?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo $_SESSION["SESS_USER_ID"]?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>