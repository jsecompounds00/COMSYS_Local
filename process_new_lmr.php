<?php
############# Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}
############# Function to validate time
	function isTime($time,$is24Hours=true,$seconds=false) {
	    $pattern = "/^".($is24Hours ? "([0-2][0-3]|[01]?[0-9])" : "(1[0-2]|0?[0-9])").":([0-5]?[0-9])".($seconds ? ":([0-5]?[0-9])" : "")."$/";
	    if (preg_match($pattern, $time)) {
	        return true;
	    }
	    return false;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_lmr.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitize the POST values
		$hidLMRId = $_POST['hidLMRId'];
		$txtLMRNo = clean($_POST['txtLMRNo']);
		$txtDate = clean($_POST['txtDate']);
		$sltDSQ = clean($_POST['sltDSQ']);
		$txtDepartureHr = clean($_POST['txtDepartureHr']);
		$txtDepartureMin = clean($_POST['txtDepartureMin']);
		$txtArrivalHr = clean($_POST['txtArrivalHr']);
		$txtArrivalMin = clean($_POST['txtArrivalMin']);
		$txtKMReadingIn = clean($_POST['txtKMReadingIn']);
		$txtKMReadingOut = clean($_POST['txtKMReadingOut']);
		$txtDieselConsumed = clean($_POST['txtDieselConsumed']);
		$txtUnitPrice = clean($_POST['txtUnitPrice']);
		$txtRemarks = clean($_POST['txtRemarks']);
		// $txtDrumDelivery = clean($_POST['txtDrumDelivery']);
		$txtUnderloadRemarks = clean($_POST['txtUnderloadRemarks']);
		$txtSampleReplaceRemarks = clean($_POST['txtSampleReplaceRemarks']);
		
		$txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		$txtUnderloadRemarks = str_replace('\\r\\n', '<br>', $txtUnderloadRemarks);
		$txtSampleReplaceRemarks = str_replace('\\r\\n', '<br>', $txtSampleReplaceRemarks);
						
		$txtRemarks = str_replace('\\', '', $txtRemarks);
		$txtUnderloadRemarks = str_replace('\\', '', $txtUnderloadRemarks);
		$txtSampleReplaceRemarks = str_replace('\\', '', $txtSampleReplaceRemarks);

		$DSQ = substr($sltDSQ, 0, strpos($sltDSQ, '-'));
		$Class = substr($sltDSQ, (strpos($sltDSQ, '-')+1));

		if($hidLMRId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
			$updatedAt = date('Y-m-d H:i:s');
		if($hidLMRId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
			$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation
		if ( $txtLMRNo == '' || !is_numeric($txtLMRNo )){
			$errmsg_arr[] = '* LMR Number is invalid.';
			$errflag = true;
		}
		$valDate = validateDate($txtDate, 'Y-m-d');
		if ( $valDate != 1 ){
			$errmsg_arr[] = '* Invalid Delivery Date.';
			$errflag = true;
		}
		if ( !$sltDSQ ){
			$errmsg_arr[] = '* Invalid DSQ.';
			$errflag = true;
		}
		// $txtDepartureHr = ( empty($txtDepartureHr) ? '00' : $txtDepartureHr );
		// $txtDepartureMin = ( empty($txtDepartureMin) ? '00' : $txtDepartureMin );
		$Departure = $txtDepartureHr.':'.$txtDepartureMin;
		$valDeparture = isTime($Departure, true, false);
		if ( $valDeparture == false ){
			$errmsg_arr[] = '* Invalid time of departure.';
			$errflag = true;
		}
		// $txtArrivalHr = ( empty($txtArrivalHr) ? '00' : $txtArrivalHr );
		// $txtArrivalMin = ( empty($txtArrivalMin) ? '00' : $txtArrivalMin );
		$Arrival = $txtArrivalHr.':'.$txtArrivalMin;
		$valArrival = isTime($Arrival, true, false);
		if ( $valArrival == false ){
			$errmsg_arr[] = '* Invalid time of arrival.';
			$errflag = true;
		}
		if ( !is_numeric($txtKMReadingIn) ){
			$errmsg_arr[] = '* Invalid KM reading in.';
			$errflag = true;
		}
		if ( !is_numeric($txtKMReadingOut) ){
			$errmsg_arr[] = '* Invalid KM reading out.';
			$errflag = true;
		}
		if ( !is_numeric($txtDieselConsumed) ){
			$errmsg_arr[] = '* Invalid quantity of diesel consumed.';
			$errflag = true;
		}
		if ( !is_numeric($txtUnitPrice) ){
			$errmsg_arr[] = '* Invalid unit price of diesel.';
			$errflag = true;
		}
		if ( $txtUnderloadRemarks == '' ){
			$txtUnderloadRemarks = NULL;
		}
		if ( $txtSampleReplaceRemarks == '' ){
			$txtSampleReplaceRemarks = NULL;
		}
	############# Validation on LMR Items
		
		foreach ($_POST['hidlmrTransItemId'] as $hidlmrTransItemId) {
			$hidlmrTransItemId = array();
			$hidlmrTransItemId = $_POST['hidlmrTransItemId'];
		}
		foreach ($_POST['sltEntryExitPt'] as $sltEntryExitPt) {
			$sltEntryExitPt = array();
			$sltEntryExitPt = $_POST['sltEntryExitPt'];
			// echo $sltEntryExitPt;
		}
		foreach ($_POST['txtEntryTimeHr'] as $txtEntryTimeHour) {
			$txtEntryTimeHour = array();
			$txtEntryTimeHour = $_POST['txtEntryTimeHr'];
		}
		foreach ($_POST['txtEntryTimeMin'] as $txtEntryTimeMinute) {
			$txtEntryTimeMinute = array();
			$txtEntryTimeMinute = $_POST['txtEntryTimeMin'];
		}
		foreach ($_POST['txtExitTimeHr'] as $txtExitTimeHour) {
			$txtExitTimeHour = array();
			$txtExitTimeHour = $_POST['txtExitTimeHr'];
		}
		foreach ($_POST['txtExitTimeMin'] as $txtExitTimeMinute) {
			$txtExitTimeMinute = array();
			$txtExitTimeMinute = $_POST['txtExitTimeMin'];
		}
		foreach ($_POST['txtToll'] as $txtToll) {
			$txtToll = array();
			$txtToll = $_POST['txtToll'];
		}

		$i=0;

		do{
			// echo $sltEntryExitPt[$i].'<br>';
			if ( $sltEntryExitPt[$i] == 0 ){
				if ($i == 0){
					$errmsg_arr[] = '* Entry-Exit Point is missing. (line '.($i+1).')';
					$errflag = true;
				}
				else{
					if ( !empty($txtEntryTimeHour[$i]) || !empty($txtEntryTimeMinute[$i]) || !empty($txtExitTimeHour[$i]) || !empty($txtExitTimeMinute[$i]) ){
						$errmsg_arr[] = '* Entry-Exit Point is missing. (line '.($i+1).')';
						$errflag = true;
					}
				}
			}else{
				if ( $sltEntryExitPt[$i-1] == 0 && $i != 0 ){
					$errmsg_arr[] = '* Skip Line is not allowed';
					$errflag = true;
				}else{
					$EntryTime[$i] = $txtEntryTimeHour[$i].':'.$txtEntryTimeMinute[$i];
					$valEntryTime[$i] = isTime($EntryTime[$i], true, false);
					if ( $valEntryTime[$i] == false ){
						$errmsg_arr[] = '* Invalid time of entry. (line '.($i+1).')';
						$errflag = true;
					}
					$ExitTime[$i] = $txtExitTimeHour[$i].':'.$txtExitTimeMinute[$i];
					$valExitTime[$i] = isTime($ExitTime[$i], true, false);
					if ( $valExitTime[$i] == false ){
						$errmsg_arr[] = '* Invalid time of exit. (line '.($i+1).')';
						$errflag = true;
					}
				}
			}
			$i++;
		}while ($i<8);
############# Input Validation

############# SESSION, keeping last input value
		$_SESSION['txtLMRNo'] = $txtLMRNo;
		$_SESSION['txtDate'] = $txtDate;
		$_SESSION['sltDSQ'] = $sltDSQ;
		$_SESSION['txtDepartureHr'] = $txtDepartureHr;
		$_SESSION['txtDepartureMin'] = $txtDepartureMin;
		$_SESSION['txtArrivalHr'] = $txtArrivalHr;
		$_SESSION['txtArrivalMin'] = $txtArrivalMin;
		$_SESSION['txtKMReadingIn'] = $txtKMReadingIn;
		$_SESSION['txtKMReadingOut'] = $txtKMReadingOut;
		$_SESSION['txtDieselConsumed'] = $txtDieselConsumed;
		$_SESSION['SESS_LMR_UnitPrice'] = $txtUnitPrice;
		$_SESSION['txtRemarks'] = $txtRemarks;
		// $_SESSION['txtDrumDelivery'] = $txtDrumDelivery;
		$_SESSION['txtUnderloadRemarks'] = $txtUnderloadRemarks;
		$_SESSION['txtSampleReplaceRemarks'] = $txtSampleReplaceRemarks;
		// $_SESSION['ffClass'] = $Class;
		
############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_lmr.php?id=$hidLMRId");
			exit();
		}
############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_LMR_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'issiissddddsssssii', $hidLMRId, $txtLMRNo, $txtDate, $DSQ, $Class, $Departure, $Arrival
												  , $txtKMReadingOut, $txtKMReadingIn, $txtDieselConsumed, $txtUnitPrice
												  , $txtRemarks, $txtUnderloadRemarks, $txtSampleReplaceRemarks
												  , $createdAt, $updatedAt, $createdId, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_lmr.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidLMRId ) {
				$lmrTransId = $hidLMRId;
			}else{
				$lmrTransId = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['sltEntryExitPt'] as $key => $itemValue)
			{	
				// if (!$hidLMRId) {
				// 	$hidlmrTransItemId = 0;
				// }

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_LMR_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiissdssii', $hidlmrTransItemId[$key], $lmrTransId, $sltEntryExitPt[$key],
																$EntryTime[$key], $ExitTime[$key], $txtToll[$key], $createdAt,
																$updatedAt, $createdId, $updatedId);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_lmr.php'.'</td><td>'.$processError1.' near line 237.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if ( $hidLMRId )
							$_SESSION['SUCCESS']  = 'LMR successfully updated.';
						else
							$_SESSION['SUCCESS']  = 'LMR successfully added.';
						header("location: lmr.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					}				
				}
			}
		}
	}	
############# Committing in Database
	}
		require("include/database_close.php");
?>