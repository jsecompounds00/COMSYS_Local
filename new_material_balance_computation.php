<html>
	<head>
		<title>Material Balance</title>
		<script src="js/material_balance_js.js"></script>
		<script src="js/datetimepicker_css_2.js"></script>
		<?php
			require("include/database_connect.php");

			$FGID = $_GET['fg_id'];
			$ComputationID = $_GET['mbc_id'];

			$qryFG1 = mysqli_prepare($db, "CALL sp_Finished_Goods_Query(?)");
			mysqli_stmt_bind_param($qryFG1, 'i', $FGID);
			$qryFG1->execute();
			$resultFG1 = mysqli_stmt_get_result($qryFG1);
			$processErrorFG1 = mysqli_error($db);
		
			if ($processErrorFG1){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_output.php'.'</td><td>'.$processErrorFG1.' near line 20.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				while($row = mysqli_fetch_assoc($resultFG1))
				{
					$name = $row["name"];
				}
				$db->next_result();
				$resultFG1->close();
			}

			$qryFG2 = mysqli_prepare($db, "CALL sp_Material_Balance_Query(?, ?)");
			mysqli_stmt_bind_param($qryFG2, 'ii', $FGID, $ComputationID);
			$qryFG2->execute();
			$resultFG2 = mysqli_stmt_get_result($qryFG2);
			$processErrorFG2 = mysqli_error($db);
		
			if ($processErrorFG2){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>material_balance_output.php'.'</td><td>'.$processErrorFG2.' near line 20.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
					$E_MaterialBalanceItemID = array();
					$E_StartBagEP = array();
					$E_EndBagEP = array();
					$E_UnderpackEP = array();
				while($row = mysqli_fetch_assoc($resultFG2)){
					$E_MaterialBalanceItemID[] = $row["E_MaterialBalanceItemID"];
				    $E_StartBagEPM[] = $row["E_StartBagEPM"];
				    $E_EndBagEPM[] = $row["E_EndBagEPM"];
				    $E_UnderpackEPM[] = $row["E_UnderpackEPM"];

					$E_CustomerID = $row["E_CustomerID"];
					$E_MaterialBalanceID = $row["E_MaterialBalanceID"];
				    $E_MaterialBalanceComputationID = $row["E_MaterialBalanceComputationID"];
				    $E_FGID = $row["E_FGID"];
				    $E_IssFormulaID = $row["E_IssFormulaID"];
				    $E_HeatUp = $row["E_HeatUp"];
				    $E_PlannedShutdown = $row["E_PlannedShutdown"];
				    $E_UnplannedShutdown = $row["E_UnplannedShutdown"];
				    $E_RequiredOutput = $row["E_RequiredOutput"];
				    $E_MaterialIncorporation = $row["E_MaterialIncorporation"];
				    $E_MasterBatch = $row["E_MasterBatch"];
				    $E_LabSamplesPlanned = $row["E_LabSamplesPlanned"];
				    $E_LabSamplesUnplanned = $row["E_LabSamplesUnplanned"];
				    $E_OnholdPellet = $row["E_OnholdPellet"];
				    $E_OnholdPowder = $row["E_OnholdPowder"];
				    $E_ScreenChange = $row["E_ScreenChange"];
				    $E_ChangeGrade = $row["E_ChangeGrade"];
				    $E_EndRun = $row["E_EndRun"];
				    $E_FloorsweepingPellet = $row["E_FloorsweepingPellet"];
				    $E_FloorSweepingPowder = $row["E_FloorSweepingPowder"];
				    $E_StartUp = $row["E_StartUp"];
				    $E_MixedPellets = $row["E_MixedPellets"];
				    $E_UnplannedScrap = $row["E_UnplannedScrap"];
				    $E_ScrapedCooling = $row["E_ScrapedCooling"];
				    $E_ScrapedMixer = $row["E_ScrapedMixer"];
				    $E_ReturnedBigBatch = $row["E_ReturnedBigBatch"];
				    $E_ReturnedSmallBatch = $row["E_ReturnedSmallBatch"];
				    $E_IdealSpeed = $row["E_IdealSpeed"];
				    $E_TotalInput = $row["E_TotalInput"];
				    $E_TotalGoodOutput = $row["E_TotalGoodOutput"];
				    $E_TotalScrap = $row["E_TotalScrap"];
				    $E_MachineRate = $row["E_MachineRate"];
				    $E_MachinePerformance = $row["E_MachinePerformance"];
				    $E_ActualVsRequired = $row["E_ActualVsRequired"];
				    $E_TOSVsActual = $row["E_TOSVsActual"];
				    $E_InputVsOutput = $row["E_InputVsOutput"];
				    $E_MaterialYield = $row["E_MaterialYield"];
				    $E_Remarks = $row["E_Remarks"];
				    $E_Remarks_Array = explode('<br>', $E_Remarks);
				    $E_CreatedAt = $row["E_CreatedAt"];
				    $E_CreatedID = $row["E_CreatedID"];
				    $E_RecoveryOnprocess = $row["E_RecoveryOnprocess"];
				    $E_RecoveryLeftOver = $row["E_RecoveryLeftOver"];
				}
				$db->next_result();
				$resultFG2->close();
			}

			require("/include/header.php");
			require("/include/init_unset_values/material_balance_init_value.php");
		?>
	</head>
	<body onload="showBigSmallBatch(),showMixer(), showExtruder(), showLotProdDate(), showOutput(),  showTotalInput(), showTotalInput2(), showGoodOutputEPM(), showUnderpackEPM(), showOverallTotalOutput(), showTotalScrap(), showMCRate(), showMCPerformance(), showActReqd(), showVarInOut(), showTOSAct(), showMaterialYield(), showTotalOutput()">

		<form method='post' action='process_new_material_balance_computation.php'>

			<div class="wrapper">

				<span> <h3> <?php echo ( $ComputationID ? "Edti " : "New " );?> Material Balance Computation for <?php echo $name;?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {

						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<colgroup>
						<col width="200px"></col><col width="250px"></col><col width="200px"></col><col width="250px">
					</colgroup>

					<tr class="spacing" valign="top">
						<td colspan="4">
							<label class="instruction2"> All fields with '*' are REQUIRED. </label>
						</td>
					</tr>

					<!-- ======================== FORMULA, MACHINE, CUSTOMER ======================== -->

					<tr>
						<td> 
							Issued Formula: 
							<label class="instruction2">*</label>
						</td>

						<td>
							<select name="sltIssuedFormula" id="sltIssuedFormula" onchange="showMixer(), showExtruder(), showLotProdDate(),showBigSmallBatch(), showOutput(),  showTotalInput(), showTotalInput2()">
								
								<?php		
									$qry = mysqli_prepare($db, "CALL sp_Issued_Formula_Dropdown(?, ?)");
									mysqli_stmt_bind_param($qry, 'ii', $FGID, $ComputationID);
									$qry->execute();
									$result = mysqli_stmt_get_result($qry); //return results of query
									$processError = mysqli_error($db);

									if(!empty($processError))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_material_balance_computation.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while ( $row = mysqli_fetch_assoc( $result ) ){
											$db_IssuedFormulaID = $row["issued_formula_id"];
											$db_IssuedFormula = $row["issued_formula"];
											$db_IssuedBatchID = $row["batch_id"];
											$db_LotNumber = $row["lot_number"];
											$db_CtrlNumber = $row["control_number"];

											if ( $initMBCIssuedFormula == $db_IssuedFormulaID ){
								?>
												<option value="<?php echo $db_IssuedFormulaID; ?>" selected><?php echo $db_CtrlNumber." / ".$db_LotNumber; ?></option>
								<?php
											}elseif ( $ComputationID && $db_IssuedFormulaID == $E_IssFormulaID ){
								?>
												<option value="<?php echo $db_IssuedFormulaID; ?>" selected><?php echo $db_CtrlNumber." / ".$db_LotNumber; ?></option>
								<?php
											}else{
								?>	
												<option value="<?php echo $db_IssuedFormulaID; ?>" ><?php echo $db_CtrlNumber." / ".$db_LotNumber; ?></option>
								<?php		
											} // if elseif else
										} // while
										$db->next_result();
										$result->close();
									} // if processError
								?>
							</select>
						</td>

						<td> 
							Customer: 
							<label class="instruction2">*</label> 
						</td>

						<td>
							<select name="sltCustomer">
								<option></option>
								<?php
									$qryFG = "CALL sp_Customer_Dropdown()";
									$resultFG = mysqli_query($db, $qryFG);
									$processErrorFG = mysqli_error($db);
									if(!empty($processErrorFG))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_material_balance_computation.php'.'</td><td>'.$processErrorFG.' near line 63.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{	
										while ( $row = mysqli_fetch_assoc( $resultFG ) ){
											$db_CustomerID = $row["id"];
											$db_Customer = $row["name"];

											if ( $initMBCCustomer == $db_CustomerID ){
								?>
												<option value="<?php echo $db_CustomerID;?>" selected><?php echo $db_Customer;?></option>
								<?php
											}elseif ( $ComputationID && $db_CustomerID == $E_CustomerID ){
								?>
												<option value="<?php echo $db_CustomerID; ?>" selected><?php echo $db_Customer; ?></option>
								<?php
											}else{
								?>	
												<option value="<?php echo $db_CustomerID;?>"><?php echo $db_Customer;?></option>
								<?php
											} // if elseif else
										} // while
										$db->next_result();
										$resultFG->close();
									} // if processErrorFG
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td> Mixer: </td>

						<td id="hidMixer">  </td>

						<td> Extruder: </td>

						<td id="hidExtruder">  </td>
					</tr>
					
				</table>

				<!-- ======================== LOT NUMBERS, PRODUCTIONS DATE, PRODUCTION START-END, TIME CONSUMED ======================== -->

				<table class="child_tables_form collapsed">
					<tr>
						<th colspan="2"> Lot Number </th>
						<th> Production Date </th>
						<th> Production Start </th>
						<th> Production End </th>
						<th> Actual Time Consumed (mins) </th>
					</tr>

					<tbody id="tbProductionDetails"></tbody>

				</table>

				<!-- ======================== SHUTDOWN, HEAT UP, REQUIRED OUTPUT ======================== --> <br><br>

				<table class="parent_tables_form">
					<colgroup>
						<col width="200px"></col><col width="250px"></col><col width="200px"></col><col width="250px">
					</colgroup>

					<tr>
						<td> Planned Shutdown (min): </td>

						<td>
							<input type="text" name="txtPlannedShutdown" id="txtPlannedShutdown" value="<?php echo ( $ComputationID && !is_null($E_PlannedShutdown) ? $E_PlannedShutdown : $initMBCPlannedShutdown );?>" onkeyup="showMCRate(), showMCPerformance()" onblur="showMCRate(), showMCPerformance()">
						</td>

						<td> Unplanned Shutdown (min): </td>

						<td>
							<input type="text" name="txtUnplannedShutdown" id="txtUnplannedShutdown" value="<?php echo ( $ComputationID && !is_null($E_UnplannedShutdown) ? $E_UnplannedShutdown : $initMBCUnplannedShutdown );?>" onkeyup="showMCRate(), showMCPerformance()" onblur="showMCRate(), showMCPerformance()">
						</td>
					</tr>

					<tr>
						<td> Heat-up: </td>

						<td>
							<input type="text" name="txtHeatUp" value="<?php echo ( $ComputationID && !is_null($E_HeatUp) ? $E_HeatUp : $initMBCHeatUp );?>">
						</td>

						<td> Required Output (kgs): <label class="instruction2">*</label> </td>

						<td>
							<input type="text" name="txtRequiredOutput" id="txtRequiredOutput" value="<?php echo ( $ComputationID && !is_null($E_RequiredOutput) ? $E_RequiredOutput : $initMBCRequiredOutput );?>" onkeyup="showActReqd()" onblur="showActReqd()">
						</td>
					</tr>
				</table>

				<!-- ======================== INPUT ======================== -->

				<table class="child_tables_form collapsed">

					<tr class="spacing">
						<td colspan="6" class="border_bottom"> <b> INPUT </b> </td>
					</tr>

					<tbody id="hidBigSmallBatch" class="thicker"></tbody>

					<tr class="spacing" valign="bottom">
						<td> Master Batch (kgs): </td>

						<td colspan="2">
							<input type="text" name="txtMasterBatch" id="txtMasterBatch" value="<?php echo ( $ComputationID && !is_null($E_MasterBatch) ? $E_MasterBatch : $initMBCMasterBatch );?>" onblur="showOverallInput(), showVarInOut(), showMaterialYield()" onkeyup="showOverallInput(), showVarInOut(), showMaterialYield()">
						</td>

						<td> Material Incorporation (kgs): </td>

						<td colspan="2">
							<input type="text" name="txtMaterialIncorporation" id="txtMaterialIncorporation" value="<?php echo ( $ComputationID && !is_null($E_MaterialIncorporation) ? $E_MaterialIncorporation : $initMBCMaterialIncorporation );?>" onblur="showGoodOutputEPM(), showOverallTotalOutput(), showMCRate(), showMCPerformance(), showActReqd(), showVarInOut(), showTOSAct(), showMaterialYield(), showTotalOutput()" onkeyup="showGoodOutputEPM(), showOverallTotalOutput(), showMCRate(), showMCPerformance(), showActReqd(), showVarInOut(), showTOSAct(), showTotalOutput(), showMaterialYield()">
						</td>
					</tr>

					<tr class="spacing" valign="top">
						<td> Recovery - On Process (kgs): </td>

						<td colspan="2">
							<input type="text" name="txtRecoveryOnprocess" id="txtRecoveryOnprocess" value="<?php echo ( $ComputationID && !is_null($E_RecoveryOnprocess) ? $E_RecoveryOnprocess: $initMBCRecoveryOnprocess );?>" onblur="showOverallInput(), showVarInOut(), showMaterialYield()" onkeyup="showOverallInput(), showVarInOut(), showMaterialYield()">
						</td>

						<td> Recovery - Left Over from RM Section (kgs): </td>

						<td colspan="2">
							<input type="text" name="txtRecoveryLeftOver" id="txtRecoveryLeftOver" value="<?php echo ( $ComputationID && !is_null($E_RecoveryLeftOver) ? $E_RecoveryLeftOver: $initMBCRecoveryLeftOver );?>" onblur="showOverallInput(), showVarInOut(), showMaterialYield()" onkeyup="showOverallInput(), showVarInOut(), showMaterialYield()">
						</td>
					</tr>

					<tr class="bgTotal">
						<td colspan="3" align="right"> 
							<label class="constant_total"> TOTAL INPUT (kgs) </label> 
						</td>

						<td id="tdTotalInput" align="left" colspan="3"> 
							<label class="constant_total"> 0.0000 </label>
						</td>
					</tr>

					<tr >
						<td id="tdOverall" colspan="6"></td>
					</tr>

				</table>

				<!-- ======================== OUTPUT ======================== -->

				<table class="child_tables_form collapsed">

					<tr class="spacing">
						<td colspan="6" class="border_bottom"> <b> OUTPUT </b> <i> (through EPM) </i> </td>
					</tr>

					<tr>
						<th colspan="2"> From - Bag# </th>
						<th colspan="2"> To - Bag# </th>
						<th colspan="2"> Underpack </th>
					</tr>

					<?php
						$count = ( $ComputationID ? count($E_MaterialBalanceItemID) : 5 );
						for( $x = 0 ; $x < 5 ; $x++ ){
					?>
							<tr align="center">

								<td colspan="2" class="border_right">
									<input type="text" name="txtFromBagEPM[]" id="txtFromBagEPM<?php echo $x;?>" value="<?php echo ( $ComputationID && $x <= ($count-1) ? $E_StartBagEPM[$x] : $initMBCFromBagEPM[$x] );?>" onkeyup="showGoodOutputEPM(), showOverallTotalOutput(), showMCRate(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut(), showTotalOutput(), showMaterialYield()" onblur="showGoodOutputEPM(), showOverallTotalOutput(), showMCRate(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut(), showTotalOutput(), showMaterialYield()">
								</td>

								<td colspan="2" class="border_right">
									<input type="text" name="txtThruBagEPM[]" id="txtThruBagEPM<?php echo $x;?>" value="<?php echo ( $ComputationID && $x <= ($count-1) ? $E_EndBagEPM[$x] : $initMBCThruBagEPM[$x] );?>" onkeyup="showGoodOutputEPM(), showOverallTotalOutput(), showMCRate(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut(), showTotalOutput(), showMaterialYield()" onblur="showGoodOutputEPM(), showOverallTotalOutput(), showMCRate(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut(), showTotalOutput(), showMaterialYield()">
								</td>

								<td colspan="2">
									<input type="text" name="txtUnderpackEPM[]" id="txtUnderpackEPM<?php echo $x;?>" value="<?php echo ( $ComputationID && $x <= ($count-1) ? $E_UnderpackEPM[$x] : $initMBCUnderpackEPM[$x] );?>" onkeyup="showUnderpackEPM(), showOverallTotalOutput(), showMCRate(), showMCPerformance(), showVarInOut(), showTOSAct(), showTotalOutput(), showMaterialYield()" onblur="showUnderpackEPM(), showOverallTotalOutput(), showMCRate(), showMCPerformance(), showVarInOut(), showTOSAct(), showTotalOutput(), showMaterialYield()">
								</td>
							</tr>
					<?php
						}
					?>

					<tr class="bgTotal" align="center">

						<td colspan="2" align="right"> 
							<label class="constant_total"> TOTAL </label>
						</td>

						<td colspan="2" id="tdTotalOutputEPM">
							
						</td>
						<td colspan="2" id="tdTotalUnderpackEPM"> 
							
						</td>
					</tr>

					<tr class="bgTotal" align="center">

						<td colspan="2" align="right"> 
							<label class="constant_total"> GOOD OUTPUT (kgs) </label>
						</td>

						<input type="hidden" name="hidGoodOutputEPM" id="hidGoodOutputEPM">
						<input type="hidden" name="hidGoodUnderpackEPM" id="hidGoodUnderpackEPM">

						<td colspan="2" id="tdGoodOutputEPM"> 

						</td>
						<td colspan="2" id="tdGoodUnderpackEPM"> 

						</td>
					</tr>

					<tr class="spacing">
						<!-- ======================== SAMPLES, HOLD, SLABS, FLOOR SWEEPING ======================== -->
					</tr>

					<tr>
						<th class="border_top" colspan="2"> ON-HOLD (kgs) </th>

						<th class="border_top" colspan="2"> PLANNED SLABS (kgs) </th>

						<th class="border_top" colspan="2"> FLOOR SWEEPING (kgs) </th>
					</tr>

					<tr>
						<td> <dd> Pellet: </dd> </td>

						<td>
							<input type="text" name="txtOnholdPellet" id="txtOnholdPellet" value="<?php echo ( $ComputationID && !is_null($E_OnholdPellet) ? $E_OnholdPellet : $initMBCOnholdPellet );?>" onblur="showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td class="border_left"> <dd> Screen Change: </dd> </td>

						<td class="border_right">
							<input type="text" name="txtScreenChange" id="txtScreenChange" value="<?php echo ( $ComputationID && !is_null($E_ScreenChange) ? $E_ScreenChange : $initMBCScreenChange );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td> <dd> Pellet: </dd> </td>

						<td class="rightborder">
							<input type="text" name="txtFloorSweepingPellet" id="txtFloorSweepingPellet" value="<?php echo ( $ComputationID && !is_null($E_FloorsweepingPellet) ? $E_FloorsweepingPellet : $initMBCFloorSweepingPellet );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>
					</tr>

					<tr>
						<td> <dd> Powder: </dd> </td>

						<td>
							<input type="text" name="txtOnholdPowder" id="txtOnholdPowder" value="<?php echo ( $ComputationID && !is_null($E_OnholdPowder) ? $E_OnholdPowder : $initMBCOnholdPowder );?>" onblur="showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td class="border_left"> <dd> Change Grade: </dd> </td>

						<td class="border_right">
							<input type="text" name="txtChangeGrade" id="txtChangeGrade" value="<?php echo ( $ComputationID && !is_null($E_ChangeGrade) ? $E_ChangeGrade : $initMBCChangeGrade );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td> <dd> Powder: </dd> </td>

						<td>
							<input type="text" name="txtFloorSweepingPowder" id="txtFloorSweepingPowder" value="<?php echo ( $ComputationID && !is_null($E_FloorSweepingPowder) ? $E_FloorSweepingPowder : $initMBCFloorSweepingPowder );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>
					</tr>

					<tr>
						<td colspan="2"></td>

						<td class="border_left"> <dd> End Run: </dd> </td>

						<td class="border_right">
							<input type="text" name="txtEndRun" id="txtEndRun" value="<?php echo ( $ComputationID && !is_null($E_EndRun) ? $E_EndRun : $initMBCEndRun );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td colspan="2"></td>
					</tr>

					<tr class="spacing">
						<td colspan="2" align="right" class="border_right border_top"> <b> Samples (kgs) </b> </td>

						<td class="border_top"> Planned Lab Samples: <label class="instruction2">*</label> </td>

						<td class="border_top">
							<input type="text" name="txtLabSamples" id="txtLabSamples" value="<?php echo ( $ComputationID && !is_null($E_LabSamplesPlanned) ? $E_LabSamplesPlanned : $initMBCLabSamples );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td class="border_top"> Unplanned Lab Samples: </td>

						<td class="border_top">
							<input type="text" name="txtUnplanLabSamples" id="txtUnplanLabSamples" value="<?php echo ( $ComputationID && !is_null($E_LabSamplesUnplanned) ? $E_LabSamplesUnplanned : $initMBCUnplanLabSamples );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>
					</tr>

					<tr class="spacing">
						<td class="border_top border_right" colspan="2" align="right"> <b> Scraped Powder (kgs) </b> </td>

						<td class="border_top"> Cooling Blender: </td>

						<td class="border_top">
							<input type="text" name="txtScrapedCooling" id="txtScrapedCooling" value="<?php echo ( $ComputationID && !is_null($E_ScrapedCooling) ? $E_ScrapedCooling : $initMBCScrapedCooling );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td class="border_top"> Mixer: </td>

						<td class="border_top">
							<input type="text" name="txtScrapedMixer" id="txtScrapedMixer" value="<?php echo ( $ComputationID && !is_null($E_ScrapedMixer) ? $E_ScrapedMixer : $initMBCScrapedMixer );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>
					</tr>

					<tr class="spacing">
						<td class="border_top border_bottom"> Start-up (kgs): </td>

						<td class="border_top border_bottom">
							<input type="text" name="txtStartUp" id="txtStartUp" value="<?php echo ( $ComputationID && !is_null($E_StartUp) ? $E_StartUp : $initMBCStartUp );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td class="border_top border_bottom"> Mixed Pellets (kgs): </td>

						<td class="border_top border_bottom">
							<input type="text" name="txtMixedPellets" id="txtMixedPellets" value="<?php echo ( $ComputationID && !is_null($E_MixedPellets) ? $E_MixedPellets : $initMBCMixedPellets );?>" onblur="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showMCPerformance(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>

						<td class="border_top border_bottom"> Unplanned Scrap (kgs): </td>

						<td class="border_top border_bottom">
							<input type="text" name="txtUnplannedScrap" id="txtUnplannedScrap" value="<?php echo ( $ComputationID && !is_null($E_UnplannedScrap) ? $E_UnplannedScrap: $initMBCUnplannedScrap );?>"  onblur="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()" onkeyup="showTotalScrap(), showOverallTotalOutput(), showTOSAct(), showActReqd(), showVarInOut()">
						</td>
					</tr>

					<tr class="spacing2">
						<!-- ======================== TOTAL SCRAP ======================== -->
					</tr>

					<tr class="bgTotal">
						<td colspan="5" align="right"> <label class="constant_total"> TOTAL SCRAP (kgs): </label> </td>

						<td id="tdTotalScrap">
						</td>

						<input type="hidden" name="hidTotalScrap" id="hidTotalScrap">
					</tr>

					<tr class="spacing2">
						<!-- ======================== OVERALL TOTAL OUTPUT ======================== -->
					</tr>

					<tr class="bgTotal">
						<td colspan="5" align="right"> <label class="constant_total"> OVERALL TOTAL OUTPUT (kgs): </label> </td>

						<td id="tdOverallTotalOutput">

						</td>

						<input type="hidden" name="hidOverallTotalOutput" id="hidOverallTotalOutput">
					</tr>

					<tr class="spacing2">
						<!-- ======================== MACHINE RATE ======================== -->
					</tr>

					<tr class="bgTotal">
						<td colspan="5" align="right"> <label class="constant_total"> MACHINE RATE (kgs/mins): </label> </td>

						<td id="tdMCRate">
						</td>

						<input type="hidden" name="hidMCRate" id="hidMCRate">
					</tr>

					<tr class="spacing2">
						<!-- ======================== IDEAL SPEED ======================== -->
					</tr>

					<tr class="spacing">
						<td class="border_top border_bottom"> Ideal Speed: </td>

						<td class="border_top border_bottom" colspan="5">
							<input type="text" name="txtIdealSpeed" id="txtIdealSpeed" value="<?php echo ( $ComputationID && !is_null($E_IdealSpeed) ? $E_IdealSpeed : $initMBCIdealSpeed );?>" onblur="showMCPerformance()" onkeyup="showMCPerformance()">
						</td>
					</tr>

					<tr class="spacing2">
						<!-- ======================== MACHINE PERFORMANCE ======================== -->
					</tr>

					<tr class="bgTotal">
						<td colspan="5" align="right"> <label class="constant_total"> MACHINE PERFORMANCE, %: </label> </td>

						<td id="tdMCPerformance">
						</td>

						<input type="hidden" name="hidMCPerformance" id="hidMCPerformance">
					</tr>

					<tr class="spacing">
						<!-- ======================== OUTPUT ======================== -->
					</tr>

					
					<tr class="spacing">
						<td colspan="6" class="border_top border_bottom"> <b> OUTPUT </b> <i> (through MIRS) </i> </td>
					</tr>

					<tbody id="hidOutput" class="thicker"></tbody>

					<tr class="spacing">
						<!-- ======================== RETURNED BATCH ======================== -->
					</tr>
					
					<tr class="spacing">
						<td align="center" colspan="2" class="border_top border_bottom"> <b> Returned Batches </b> </td>

						<td class="border_top border_bottom"> Big: </td>

						<td class="border_top border_bottom">
							<input type="text" name="txtReturnedBatch" id="txtReturnedBatch" value="<?php echo ( $ComputationID && !is_null($E_ReturnedBigBatch) ? $E_ReturnedBigBatch : $initMBCReturnedBatch );?>" onkeyup="showVarInOut(), showMaterialYield()" onchange="showVarInOut(), showMaterialYield()">
						</td>

						<td class="border_top border_bottom"> Small: </td>

						<td class="border_top border_bottom">
							<input type="text" name="txtReturnedBatchSmall" id="txtReturnedBatchSmall" value="<?php echo ( $ComputationID && !is_null($E_ReturnedSmallBatch) ? $E_ReturnedSmallBatch : $initMBCReturnedBatchSmall );?>" onkeyup="showVarInOut(), showMaterialYield()" onchange="showVarInOut(), showMaterialYield()">
						</td>
					</tr>

					<tr class="spacing">
						<!-- ======================== RETURNED BATCH ======================== -->
					</tr>
					
					<tr class="spacing bgTotal">
						<td> <label class="constant_total"> Output <br>(Actual vs. Required) </label> </td>

						<td id="tdActReqd">
						</td>

						<input type="hidden" name="hidActReqd" id="hidActReqd">

						<td> <label class="constant_total"> Output <br>(TOS vs. Actual) </label> </td>

						<td id="tdTOSAct">
							<label class="constant_total"> <?php echo ( $ComputationID ? $E_TOSVsActual : $initMBCTOSAct );?> </label>
						</td>

						<input type="hidden" name="hidTOSAct" id="hidTOSAct" value="<?php echo ( $ComputationID ? $E_TOSVsActual : $initMBCTOSAct );?>">

						<td> <label class="constant_total"> Variance <br>(T.Input vs. T.Output) </label> </td>

						<td id="tdVarInOut">
							<label class="constant_total"> <?php echo ( $ComputationID ? $E_InputVsOutput : $initMBCVarInOut );?> </label>
						</td>

						<input type="hidden" name="hidVarInOut" id="hidVarInOut" value="<?php echo ( $ComputationID ? $E_InputVsOutput : $initMBCVarInOut );?>">
					</tr>

					<tr class="spacing2">
						<!-- ======================== RETURNED BATCH ======================== -->
					</tr>
					
					<tr class="bgTotal">
						<td colspan="5" align="right"> <label class="constant_total"> MATERIAL YIELD, %: </label> </td>

						<td id="tdMaterialYield">
							<?php echo ( $ComputationID ? $E_MaterialYield : $initMBCMaterialYield );?>
						</td>

						<input type="hidden" name="hidMaterialYield" id="hidMaterialYield" value="<?php echo ( $ComputationID ? $E_MaterialYield : $initMBCMaterialYield );?>">
					</tr>

				</table>

				<!-- ======================== BUTTONS ======================== -->

				<table class="comments_buttons">
					<tr valign="top">
						<td> Remarks: </td>
						<td colspan="5">
							<textarea class="paragraph" name="txtRemarks"><?php
								if ( $ComputationID && $E_Remarks != "" ){
									foreach ($E_Remarks_Array as $key => $E_Remarks_value) {
										echo $E_Remarks_value."\n";
									}
								}else{
									$initMBCRemarks_array = explode("<br>", $initMBCRemarks);

									foreach ($initMBCRemarks_array as $key => $initMBCRemarks_value) {
										echo $initMBCRemarks_value."\n";
									}
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" name="btnSave" value="Save">
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='pam_analysis.php?search=<?php echo $_SESSION['search'];?>&page=<?php //echo $_SESSION['page'];?>&qsone=<?php //echo $_SESSION['qsone'];?>'">	
							<input type="hidden" name="hidFGID" id="hidFGID" value="<?php echo $FGID;?>">
							<input type="hidden" name="hidComputationID" id="hidComputationID" value="<?php echo $ComputationID;?>">
							<input type="hidden" name="hidTotalComputationID" id="hidTotalComputationID" value="<?php echo ($ComputationID ? $E_MaterialBalanceComputationID : NULL);?>">
							<inpuT type="hidden" name="hidCreatedAt" value="<?php echo ($ComputationID ? $E_CreatedAt : date('Y-m-d H:i:s'));?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ($ComputationID ? $E_CreatedID : NULL);?>">
						</td>
					</tr>					
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>