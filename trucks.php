<html>
	<head>
		<title>Trucks - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = "";
		?>
	</head>
	<body>

		<?php
			require '/include/header.php';

			if( $_SESSION["trucks"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Trucks </h3> </span>

			<div class='search_box'>
				<form method='get' action='trucks.php'>
					<input type='hidden' name='page' value="<?php echo $page;?>">
					<input type='hidden' name='qsone' value="<?php echo $qsone;?>">
					<table class="search_tables_form">
						<tr>
							<td> Truck No.: </td>
							<td> <input type='text' name='search' value="<?php echo $_GET['search'];?>"> </td>
							<td> <input type='submit' value='Search'> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>trucks.php'.'</td><td>'.$error.' near line 55.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryTP = mysqli_prepare($db, "CALL sp_Trucks_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryTP, "s", $search);
					$qryTP->execute();
					$resultTP = mysqli_stmt_get_result($qryTP);
					$total_results = mysqli_num_rows($resultTP); //return number of rows of result

					$db->next_result();
					$resultTP->close();

					$targetpage = "trucks.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					
					$qry = mysqli_prepare($db, "CALL sp_Trucks_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>trucks.php'.'</td><td>'.$processError.' near line 48.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
								<th>Vehicle Class</th>
								<th>Plate Number</th>
								<th>Description</th>
								<th>Capacity</th>
								<th>Active</th>
								<th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)) { 
							?>
									<tr>
										<td>
											<?php echo ( $row['vehicle_class'] ? 'Class '.$row['vehicle_class'] : '' ); ?>
										</td>
										<td> <?php echo $row['plate_number']; ?> </td>
										<td> <?php echo $row['description']; ?> </td>
										<td> <?php echo $row['capacity']; ?> </td>
										<td> <?php echo ( $row['active'] ? 'Y' : 'N' ); ?> </td>
										<td> 
											<input type='button' name='btnTrucks' value='Edit' onclick="location.href='new_trucks.php?id=<?php echo $row['id'];?>'"> 
										</td>
									</tr>
							<?php 
								} 
							?>
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
