<html>
	<head>
		<title>Research and Development - Home</title>
		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/init_unset_values/jrd_unset_value.php");

			if( $_SESSION["jrd"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Job Request for Research and Development </h3> </span>

			<div class="search_box">
				<form method="get" action="jrd.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_form">
						<tr>
							<td> JRD Date: </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>"> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
							<td> JRD No.: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td> <input type="button" name="btnAddJRD" value="New JRD" onclick="location.href='new_jrd.php?id=0'"> </td>
							<td> <input type="button" name="btnAddTests" value="Add Testing (Product Evaluation w/o Replication)" onclick="location.href='new_jrd_test_evaluation_noreplicate.php'"> </td>
							<td> <input type="button" name="btnAddResponse" value="Add Client Response" onclick="location.href='new_jrd_client_response.php'"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>jrd.php"."</td><td>".$error." near line 42.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_JRD_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, "ss", $search, $qsone);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "jrd.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_JRD_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, "ssii", $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>jrd.php"."</td><td>".$processError." near line 63.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class='home_pages'>
							<tr>
								<td colspan='7'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>JRD Date</th>
							    <th>JRD No.</th>
							    <th>Customer</th>
							    <th>Type of Request</th>
							    <th>New Product</th>
							    <th>Existing Product</th>
							    <th colspan="2"></th>
							</tr>
							<?php
								while ( $row = mysqli_fetch_assoc( $result ) ){
									$JRDID = $row["JRDID"];
									$jrd_number = $row["jrd_number"];
									$date_requested = $row["date_requested"];
									$Customer = $row["Customer"];
									$type_of_request = $row["type_of_request"];
									$FinishedGood = $row["FinishedGood"];
									$NewProduct = $row["NewProduct"];
							?>
									<tr>
										<td> <?php echo $date_requested; ?> </td>
										<td> <?php echo $jrd_number; ?> </td>
										<td> <?php echo $Customer; ?> </td>
										<td> <?php echo $type_of_request; ?> </td>
										<td> <?php echo $NewProduct; ?> </td>
										<td> <?php echo $FinishedGood; ?> </td>
										<td > 
											<input type="button" name="btnJRDEdit" value="Edit" onclick="location.href='new_jrd.php?id=<?php echo $JRDID;?>'">
										</td>
									</tr>
							<?php
								}
							?>
							<tr>
								<td colspan='7'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>