<html>
	<head>
		<title> Customer Jacket - History </title>
		<?php
			require("include/database_connect.php");

			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone='';

			$customerID = $_GET['id'];

			############ .............
			$qryL = mysqli_prepare($db, "CALL sp_Customer_Query(?)");
			mysqli_stmt_bind_param($qryL, 'i', $customerID);
			$qryL->execute();
			$resultL = mysqli_stmt_get_result($qryL);
			$rowL = mysqli_fetch_assoc($resultL);
			$customerName = $rowL['name'];
			$customerType = $rowL['local'];

			$customerType = ($customerType == 1) ? 'Local' : 'Export' ;

			$db->next_result();
			$resultL->close();

			############ .............
			$qryPI = "SELECT id from comsys.customers";
			$resultPI = mysqli_query($db, $qryPI); 
			$processErrorPI = mysqli_error($db);

			if ( !empty($processErrorPI) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>customer_jacket_history.php'.'</td><td>'.$processErrorPI.' near line 57.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
					$id = array();
				while($row = mysqli_fetch_assoc($resultPI)){
					$id[] = $row['id'];
				}
			}
			$db->next_result();
			$resultPI->close();

			############ .............
			if( !in_array($customerID, $id, TRUE) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>customer_jacket_history.php</td><td>The user tries to view transaction history of a non-existing property_id.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
		?>
	</head>
	<body>

		<?php
			require '/include/header.php';
			// require ("include/unset_value.php");
			// require ("include/init_unset_values/credit_payment_unset_value.php");
			// require ("include/init_unset_values/debit_invoice_unset_value.php");

			if( $_SESSION['customer_jacket_history'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}
			
			$page_a = $_SESSION["page"];
			$search_a = htmlspecialchars($_SESSION["search"]);
			$qsone_a = htmlspecialchars($_SESSION["qsone"]);
			$search_a = ( (strpos(($search_a), "\\")+1) > 0 ? str_replace("\\", "", $search_a) : $search_a );
			$search_a = ( (strpos(($search_a), "'")+1) > 0 ? str_replace("'", "\'", $search_a) : $search_a );
			$qsone_a = ( (strpos(($qsone_a), "\\")+1) > 0 ? str_replace("\\", "", $qsone_a) : $qsone_a );
			$qsone_a = ( (strpos(($qsone_a), "'")+1) > 0 ? str_replace("'", "\'", $qsone_a) : $qsone_a );
		?>

		<input type='hidden' name='page' value="<?php echo ( $page );?>">

		<div class="wrapper">

			<span> <h2> Transaction History for <?php echo $customerName." (".$customerType.")"; ?> </h2> </span>

			<a class="back" href='customer_jacket.php?page=<?php echo $page_a;?>&search=<?php echo $search_a;?>&qsone=<?php echo $qsone_a;?>'><img src='images/back.png' height="20" name='txtBack'> Back</a>

			<?php
				$qryCPH = mysqli_prepare($db, "CALL sp_Customer_Jacket_History(?, NULL, NULL)");
				mysqli_stmt_bind_param($qryCPH, 'i', $customerID);
				$qryCPH->execute();
				$resultCPH = mysqli_stmt_get_result($qryCPH);
				$total_results = mysqli_num_rows($resultCPH); //return number of rows of result

				$db->next_result();
				$resultCPH->close();

				$targetpage = "customer_jacket_history.php"; 	//your file name  (the name of this file)
				require("include/paginate_hist.php");

				$qry = mysqli_prepare($db, "CALL sp_Customer_Jacket_History(?, ?, ?)");
				mysqli_stmt_bind_param($qry, 'iii', $customerID, $start, $end);
				$qry->execute();
				$result = mysqli_stmt_get_result($qry);

				$processError = mysqli_error($db);
				
				if(!empty($processError))
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>customer_jacket_history.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					if( isset($_SESSION['SUCCESS'])) 
					{
						echo '<ul id="success">';
						echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
						echo '</ul>';
						unset($_SESSION['SUCCESS']);
					}

			?>
					<table class="home_pages">
						<tr>
							<td colspan='7'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
							<th>Trxn Date</th>
							<th>Type</th>
							<th>Invoice No.</th>
							<th>Check No.</th>
							<th>Debit</th>
							<th>Credit</th>
							<th></th>
						</tr>
						<?php
							while ( $row = mysqli_fetch_assoc($result) ){
								$jacket_id = $row['jacket_id'];
								$transaction_date = $row['transaction_date'];
								$transaction_type = $row['transaction_type'];
								$invoice_number = $row['invoice_number'];
								$check_number = $row['check_number'];
								$debit = $row['debit'];
								$credit = $row['credit'];
								$currency = $row['currency'];
								$process_type = $row['process_type'];
						?>
								<tr>
									<td> <?php echo $transaction_date; ?> </td>

									<td> 
										<?php
											if ( $process_type == 'sub' ){
										?>
												<label style='background-color:#ffeaea; padding:3px;'>
										<?php
											}else{
										?>		
												<label style='background-color:#e3fbe9;padding:3px;'>
										<?php
											}
													echo $transaction_type;
										?> 
												</label>
									</td>

									<td> <?php echo $invoice_number; ?> </td>

									<td> <?php echo $check_number; ?> </td>

									<td> <?php echo $currency." ".number_format((float)($debit), 2, '.', ','); ?> </td>

									<td> <?php echo $currency." ".number_format((float)($credit), 2, '.', ','); ?> </td>

									<td>
										<?php
											if ( $transaction_type == 'Invoice-PMT' || $transaction_type == 'Invoice-DSC' ){
												if(array_search(145, $session_Permit)){
										?>
													<input type='button' value='Edit' onclick="location.href='credit_payment.php?id=<?php echo $jacket_id; ?>&type=cp'"> 
										<?php
													$_SESSION['edit_credit_payment'] = true;
												}else{
													unset($_SESSION['edit_credit_payment']);
												}
											}elseif ( $transaction_type == 'Invoice-AR' || $transaction_type == 'PMT-RC' || $transaction_type == 'PMT-VoidDSC' ){
												if(array_search(144, $session_Permit)){
										?>
													<input type='button' value='Edit' onclick="location.href='debit_invoice.php?id=<?php echo $jacket_id; ?>&type=di'">
										<?php
													$_SESSION['edit_debit_invoice'] = true;
												}else{
													unset($_SESSION['edit_debit_invoice']);
												}
											}elseif ( $transaction_type == 'Debit Memo' ){
												if(array_search(169, $session_Permit)){
										?>
													<input type='button' value='Edit' onclick="location.href='debit_memo.php?id=<?php echo $jacket_id; ?>&type=dm'">
										<?php
													$_SESSION['edit_debit_memo'] = true;
												}else{
													unset($_SESSION['edit_debit_memo']);
												}
											}elseif ( $transaction_type == 'Credit Memo' ){
												if(array_search(168, $session_Permit)){
										?>
													<input type='button' value='Edit' onclick="location.href='credit_memo.php?id=<?php echo $jacket_id; ?>&type=cm'">
										<?php
													$_SESSION['edit_credit_memo'] = true;
												}else{
													unset($_SESSION['edit_credit_memo']);
												}
											}
										?>
									</td>
								</tr>
						<?php
							}
							$db->next_result();
							$result->close();
						?>
						<tr>
							<td colspan='7'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>