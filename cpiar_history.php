<html>
	<head>
		<title>CPIAR Monitoring - History</title>
		<?php
			require("include/database_connect.php");

			$page = ($_GET['page'] ? $_GET['page'] : NULL);
			$qsone='';

			$cid = $_GET['id'];

			$qryC = mysqli_prepare($db, "CALL sp_CPIAR_Query(?)");
			mysqli_stmt_bind_param($qryC, 'i', $cid);
			$qryC->execute();
			$resultC = mysqli_stmt_get_result($qryC); //return results of query
			$processErrorC = mysqli_error($db);
			if(!empty($processErrorC))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>cpiar_history.php'.'</td><td>'.$processErrorC.' near line 33.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				$row = mysqli_fetch_assoc($resultC);
				$cpr_no = $row['cpr_no'];
			}
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");

			if( $_SESSION['history_cpr'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}
			
			$page_a = $_SESSION["page"];
			$search_a = htmlspecialchars($_SESSION["search"]);
			$qsone_a = htmlspecialchars($_SESSION["qsone"]);
			$search_a = ( (strpos(($search_a), "\\")+1) > 0 ? str_replace("\\", "", $search_a) : $search_a );
			$search_a = ( (strpos(($search_a), "'")+1) > 0 ? str_replace("'", "\'", $search_a) : $search_a );
			$qsone_a = ( (strpos(($qsone_a), "\\")+1) > 0 ? str_replace("\\", "", $qsone_a) : $qsone_a );
			$qsone_a = ( (strpos(($qsone_a), "'")+1) > 0 ? str_replace("'", "\'", $qsone_a) : $qsone_a );
		?>

		<div class="wrapper">

			<input type='hidden' name='page' value='<?php echo $_GET['page']; ?>'>

			<span> <h2> CPIAR Monitoring History for CPIAR No. <?php echo $cpr_no; ?> </h2> </span>

			<a href='cpiar_monitoring.php?page=<?php echo $page_a;?>&search=<?php echo $search_a;?>&qsone=<?php echo $qsone_a;?>' class="back"><img src='images/back.png' height="20" name='txtBack'> Back</a>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>cpiar_history.php'.'</td><td>'.$error.' near line 49.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_CPIAR_Verification_History(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'i', $search);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "cpiar_history.php"; 	//your file name  (the name of this file)
					require("include/paginate_hist.php");

					$qry = mysqli_prepare($db, "CALL sp_CPIAR_Verification_History(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'iii', $cid, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>cpiar_history.php'.'</td><td>'.$processError.' near line 75.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
			
					<table class="home_pages">
						<col></col><col width='700'></col>
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
						    <th> CPIAR Date </th>
						    <th> Verification Statement </th>
						    <th> Status </th>
						    <th> Next Verification Date </th>
						    <th></th>
						</tr>
						<?php 
							while($row = mysqli_fetch_assoc($result)) { 
						?>
								<tr >
									<td> <?php echo $row['CPIARVerificationDate']; ?> </td>
									<td> <?php echo nl2br($row['CPIARVerificationStatement']); ?> </td>
									<td> <?php echo $row['CPIARStatus']; ?> </td>
									<td> <?php echo $row['NextVerificationDate']; ?> </td>
									<td>
										<?php
											if(array_search(122, $session_Permit)){
										?>
												<input type='button' name='btnTP' value='Edit' onclick="location.href='cpiar_history.php?id=<?php echo $row['CPIARID'];?>&cid=<?php echo $row['CPIARVerificationID'];?>'">
										<?php
												$_SESSION['edit_cpr_verification'] = true;
											}else{
												unset($_SESSION['edit_cpr_verification']);
											}
										?>
									</td>
								</tr>
						<?php
							} 
							$db->next_result();
							$result->close();
						?>
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>