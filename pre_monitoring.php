<html>
	<head> 
		<title>P.R.E. - Home</title>
		<script src="js/datetimepicker_css.js"></script>
		<script src="js/jscript.js"></script> 
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> <!--   -->
		<?php
			require("include/database_connect.php");

			$pre_number = ($_GET['pre_number'] ? "%".$_GET['pre_number']."%" : "");
			$item = ($_GET['item'] ? "%".$_GET['item']."%" : "");
			$pre_date = ($_GET['pre_date'] ? $_GET['pre_date'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
	</head>
	<body>

		<?php
			require '/include/header.php';
			require("/include/unset_value.php");

			if( $_SESSION['pre_home'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["pre_number"] = $_GET["pre_number"];
			$_SESSION["item"] = $_GET["item"];
			$_SESSION["pre_date"] = $_GET["pre_date"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> P.R.E. Monitoring </h3> </span>

			<div class="search_box">
				<form method='get' action='pre_monitoring.php'>
					<input type='hidden' name='page' value='<?php echo $page; ?>'>
					<table class="search_tables_form">
						<tr>
							<td> P.R.E. Date: </td>
							<td> <input type='text' name='pre_date' id='pre_date' value='<?php echo $_GET['pre_date'];?>'> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('pre_date')" style="cursor:pointer" name="picker" /> </td>
							<td> P.R.E. No.: </td>
							<td> <input type='text' name='pre_number' value='<?php echo $_GET['pre_number']; ?>'> </td>
							<td> Item: </td> 
							<td> <input type='text' name='item' value='<?php echo $_GET['item']; ?>'> </td> 
							<td> <input type='submit' value='Search'> </td>
							<td>
								<?php
									if(array_search(35, $session_Permit)){ 
								?>
										<input type='button' name='btnAddPRE' value='New P.R.E.' onclick="location.href='new_pre.php?id=0'">
								<?php 
										$_SESSION['add_pre'] = true;
									}else{
										unset($_SESSION['add_pre']);
									}
								?>
							</td>
							<td>
								<?php
									if(array_search(36, $session_Permit)){ 
								?>
										<input type='button' value='Edit P.R.E.' onclick="location.href='edit_pre.php'">
								<?php 
										$_SESSION['edit_pre'] = true;
									}else{
										unset($_SESSION['edit_pre']);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pre_monitoring.php'.'</td><td>'.$error.' near line 60.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{

					$qryP = mysqli_prepare($db, "CALL sp_PRE_Home_new(?, ?, ?, ?, ?, ?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryP, 'sssiiiis', $pre_date, $pre_number, $item, $compounds, $pipes, $corporate, $ppr, $_SESSION['SESS_ROLE_ID']);
					$qryP->execute();
					$resultP = mysqli_stmt_get_result($qryP); //return results of query

					$total_results = mysqli_num_rows($resultP); //return number of rows of result
					
					$db->next_result();
					$resultP->close();

					$targetpage = "pre_monitoring.php"; 	//your file name  (the name of this file)
					require("include/paginate_pre.php");

					$qry = mysqli_prepare($db, "CALL sp_PRE_Home_new(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sssiiiisii', $pre_date, $pre_number, $item, $compounds, $pipes, $corporate, $ppr, $_SESSION['SESS_ROLE_ID'], $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>pre_monitoring.php'.'</td><td>'.$processError.' near line 86.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
					 	<table class="home_pages">
							<tr>
								<td colspan='10'> 
									<?php echo $pagination;?>
								</td>
							</tr>

							<tr>
								<th>P.R.E. No.</th>
								<th>P.R.E. Date.</th>
								<th>Item Type</th>
								<th></th>
								<th>Date Needed</th>
								<th>Date Received</th>
								<th>Date Approved</th>
								<th>PRE<br>Encoder</th>
								<th>Purchasing<br>Encoder</th>
								<th></th>
							</tr>
							<?php
								while( $row = mysqli_fetch_assoc( $result ) )
								{
									$id	= $row['id'];
									$pre_number = $row['pre_number'];
									$pre_date = $row['pre_date'];
									$date_needed = $row['date_needed'];
									$date_received = $row['date_received'];
									$canceled = $row['canceled'];
									$cancel_reason = $row['cancel_reason'];
									$PRECancelled = $row['PRECancelled'];
									$with_po = $row['with_po'];
									$approved_date = $row['approved_date'];
									$ItemType = $row['ItemType'];
									$pcf = $row['pcf'];
									$Item = $row['Item'];
									$purchasing_encoder = $row['purchasing_encoder'];

									if ( $canceled == 1 ){
							?>
										<tr>
											<td>
												<a> <?php echo $pre_number;?> </a>
										 	</td>
											<td colspan='9' align="center"><b>Cancelled :</b> <?php echo $cancel_reason;?> </td>
										</tr>
										
							<?php
									}else{
							?>
										<tr>
											<td>
												<?php
													if(array_search(36, $session_Permit)){
												?>
														<a href='new_pre.php?id=<?php echo $id;?>' class='button_transparent'> <?php echo $pre_number;?> </a>
												<?php
														$_SESSION['edit_pre'] = true;
													}else{
														echo $pre_number;
														unset($_SESSION['edit_pre']);
													}
												?>
											</td>
											<td> <?php echo $pre_date;?> </td>
											<td> <?php echo $ItemType;?> </td>
											<td> <?php echo ($item != '' ? $Item : '');?> </td>
											<td> <?php echo $date_needed;?> </td>
											<td> <?php echo $date_received;?> </td>
											<td> <?php echo $approved_date;?> </td>
											<td> <?php echo $row['creator'];?> </td>
											<td> <?php echo $row['purchasing_encoder'];?> </td>
											<td>
												<?php
													if(array_search(37, $session_Permit)){
														if ( $PRECancelled == 1 || $canceled == 1 || $with_po <> 0 || $pcf <> 0 ){
												?>
															<input type='button' name='btnCancelPRE' value='Cancel PRE' onclick="cancelBox(<?php echo $id;?>, '<?php echo $pre_number;?>')" disabled>
												<?php
														}else{
												?>
															<input type='button' name='btnCancelPRE' value='Cancel PRE' onclick="cancelBox(<?php echo $id;?>, '<?php echo $pre_number;?>')">
												<?php
														}

														$_SESSION['cancel_pre'] = true;
													}else{
														unset($_SESSION['cancel_pre']);
													}
												?>
											</td>
												
										</tr>
							<?php
									}
								}
							?>

							<tr>
								<td colspan='10'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>			
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>