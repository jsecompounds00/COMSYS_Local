<html>
	<head>
		<title>Monthly Kilowatt Rate - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			require("/include/unset_value.php");

			if( $_SESSION['monthly_kw_rate'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION['page'] = $_GET['page'];
			$_SESSION['search'] = $_GET['search'];
			$_SESSION['qsone'] = $_GET['qsone'];
		?>

		<div class="wrapper">
			
			<span> <h3> Monthly Kilowatt Rate </h3> </span>

			<div class="search_box">
		 		<form method='get' action='monthly_kw_rate.php'>
		 			<input type='hidden' name='page' value="<?php echo $page;?>">
		 			<input type='hidden' name='qsone' value='<?php echo $_GET['qsone'];?>'>
		 			<table class="search_tables_form">
		 				<tr>
		 					<td> Month: </td>
		 					<td> <input type='text' name='search' value='<?php echo $_GET['search'];?>'> </td>
		 					<td> <input type='submit' value='Search'> </td>
		 					<td> <input type='button' name='btnAddPrice' value='Add Price' onclick="location.href='new_monthly_kw_rate.php?id=0'"> </td>
		 				</tr>		 				
		 			</table>
			 	</form>
			 </div>

			 <?php	
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>monthly_kw_rate.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryLC = mysqli_prepare($db, "CALL sp_Monthly_KW_Rate_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryLC, 'si', $search, $qsone);
					$qryLC->execute();
					$resultLC = mysqli_stmt_get_result($qryLC); //return results of query

					$total_results = mysqli_num_rows($resultLC); //return number of rows of result

					$db->next_result();
					$resultLC->close();

					$targetpage = "monthly_kw_rate.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Monthly_KW_Rate_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'siii', $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>monthly_kw_rate.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
						    <th> Date </th>
						    <th> Rate </th>
						    <th>  </th>
						</tr>
						<?php 
							while($row = mysqli_fetch_assoc($result)) { 
						?>
								<tr>
									<td> <?php echo $row['month'].' '.$row['year'];?> </td>
									<td> <?php echo $row['price'];?> </td>
									<td>
										<input type='button' name='btnP' value='Edit' onclick="location.href='new_monthly_kw_rate.php?id=<?php echo $row['id'];?>'">
									</td>
								</tr>
						<?php
							}
							$db->next_result();
							$result->close();
						?>
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>