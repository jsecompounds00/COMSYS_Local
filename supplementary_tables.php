<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/row_color.css">
		<title>Supplementary Tables</title>
	</head>
	<body>
		<?php 
			require("/include/header.php");
			if( $_SESSION['migration'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}
		?>
		<form method='post'>
			<span>Supplementary Tables</span>
			<?php
				if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
					echo '<ul class="err">';
					foreach($_SESSION['ERRMSG_ARR'] as $msg) {
						echo '<li>'.$msg.'</li>'; 
						}
					echo '</ul>';
					unset($_SESSION['ERRMSG_ARR']);
				} 
				elseif( isset($_SESSION['SUCCESS'])) 
				{
					echo '<ul id="success">';
					echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
					echo '</ul>';
					unset($_SESSION['SUCCESS']);
				}
			?>
			<table class='home3'>
				<!--?php if(array_search(60, $session_Permit)){ ?>
				<tr height='70'>
					<td class='borderless'>
						<label><a name='liRun' onclick="confirmation(100)" class='button'>Migrate all</a></label>
					</td>
				</tr> -->
				<?php 
					// } 
				?>
				<tr>
					<th>
						Report Usage
					</th>
					<th>
						
					</th>
				</tr>
				<?php if(array_search(61, $session_Permit)){ ?>
				<tr>
					<td>
						<label>R-009 RM Below Critical Level Report</label>
					</td>
					<td align='center'>
						<label><a name='liPrepare' onclick="confirmation(1)" class='button'>Prepare Table</a></label>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td class='borderless' colspan=6 height=50>
						<!--?php echo $pagination; ?-->
					</td>
				</tr>
			</table>
		</form>

		  <div>
		    <?php require('/include/footer.php'); ?>
		  </div>

		<script type='text/javascript'>
		function confirmation(id){
			var r=window.confirm('Prepare Table?');
			if (r==true)
			  { 
			  	if(id == 1){
			  		window.location.assign('process_supplementary_tables.php?run_r009=true');
			  	}else{
			  		alert("Error preparing table!");
			  	}
			  }
			else
			  { 
			  	window.location.assign('supplementary_tables.php'); 
			  }
		}
		</script>
	</body>
</html>
