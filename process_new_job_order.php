<?php
########## -- Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## -- Array to store validation errors
	$errmsg_arr = array();
 
########## -- Validation error flag
	$errflag = false;
 
########## -- Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}
########## --

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_job_order.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## -- Sanitize the POST values
		$hidJobOrderID = $_POST['hidJobOrderID'];
		$hidControlNumber = $_POST['hidControlNumber'];
		$hidRequestDate = $_POST['hidRequestDate'];
		$radRequestingDept = $_POST['radRequestingDept'];
		$txtJobDetail = $_POST['txtJobDetail'];
		$radSuperior = $_POST['radSuperior'];
		$hidEmail = $_POST['hidEmail'];
		if( $hidJobOrderID ){
			$ParentEmail = $hidEmail;
		}else{
			foreach( $hidEmail as $key => $email ){
				$ParentEmail = $email;
			}
		}
		$btnSave = $_POST["btnSave"];

		$createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		$createdId = $_POST['hidRequisitionerID'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		$qryD = mysqli_prepare($db, "CALL sp_Department_Query(?)");
		mysqli_stmt_bind_param($qryD, 'i', $radRequestingDept);
		$qryD->execute();
		$resultD = mysqli_stmt_get_result($qryD); //return results of query
		if(!empty($processErrorD))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_job_order.php'.'</td><td>'.$processErrorD.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else
		{ 
			while( $row = mysqli_fetch_assoc($resultD) ){
				$DepartmentName = $row["name"];
			}
			$db->next_result();
			$resultD->close(); 
		} // if-else !empty($processErrorD)

		$qryU = mysqli_prepare($db, "CALL sp_User_Query(?)");
		mysqli_stmt_bind_param($qryU, 'i', $createdId);
		$qryU->execute();
		$resultU = mysqli_stmt_get_result($qryU); //return results of query
		if(!empty($processErrorU))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_job_order.php'.'</td><td>'.$processErrorU.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else
		{ 
			while( $row = mysqli_fetch_assoc($resultU) ){
				$FirstName = $row["fname"];
				$LastName = $row["lname"];
			}
			$db->next_result();
			$resultU->close(); 
		} // if-else !empty($processErrorU)

		$RequisitionerName = $FirstName." ".$LastName;

		if ( !$hidJobOrderID ){
			########## -- Input Validations
				if ( $txtJobDetail == "" ){
					$errmsg_arr[] = "* Details can't be blank.";
					$errflag = true;
				}
				if (!isset($radSuperior)){
					$errmsg_arr[] = "* Select department head / superior. ";
					$errflag = true;
				}
				if( $ParentEmail == "Email is not available" && $btnSave == "Save & Send" ){
					$errmsg_arr[] = "* Job Request could not be sent. Email address of superior is required. ";
					$errflag = true;
				}

			########## -- Input Validation

			########## -- SESSION, keeping last input value
				$_SESSION['SESS_JOF_JobDetail'] = $txtJobDetail;

			########## -- If there are input validations, redirect back to the login form
				if($errflag) {
					$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
					session_write_close();
					header("Location: new_job_order.php?id=$hidJobOrderID");
					exit();
				}
		}
########## -- Committing to database
		$sent = 0;

		if( !$hidJobOrderID ){
			$qry = mysqli_prepare($db, "CALL sp_Job_Order_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			mysqli_stmt_bind_param($qry, 'isissiissii', $hidJobOrderID, $hidRequestDate, $radRequestingDept, $txtJobDetail
													  , $hidControlNumber, $radSuperior, $Sent
													  , $createdAt, $updatedAt, $createdId, $updatedId);
			$qry->execute();
			$result = mysqli_stmt_get_result($qry); //return results of query
		}

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_job_order.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if ( $btnSave == "Save" ){
				if($hidJobOrderID)
					$_SESSION['SUCCESS']  = 'Successfully updated job request.';
				else
					$_SESSION['SUCCESS']  = 'Successfully added new job request.';
				header("location:job_order.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
			} // if save
			elseif ( $btnSave == "Save & Send" || $btnSave == "Send" ){				

				$Subject= "JOB ORDER FOR APPROVAL";
				$Body = "Control Number: ".$hidControlNumber."<br>"
						."Date: ".$hidRequestDate."<br>"
						."From: ".$RequisitionerName." - ".$DepartmentName."<br><br>"
						."Details of Job Request: ".$txtJobDetail;

				require("process_send_job_order.php");

				if(!$mail->send()){
					if($hidJobOrderID){
						if( $btnSave == "Save & Send" ){
							$_SESSION['SUCCESS']  = "Job request was updated but could not be sent. Please check your internet connection.";
							header("location:job_order.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
						}elseif( $btnSave == "Send" ){
							$errmsg_arr[] = "Message could not be sent. Please check your internet connection.";
							$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
							session_write_close();
							header("Location: new_job_order.php?id=$hidJobOrderID");
							exit();
						}
					}else{
						$errmsg_arr[]  = "Job request was added but could not be sent. Please check your internet connection";
						$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
						session_write_close();
						header("Location: new_job_order.php?id=$hidJobOrderID");
						exit();
					}
				}else{
					$sent = 1;
					$qryUS = mysqli_prepare($db, "CALL sp_Job_Order_Update_Sent(?, ?)");
					mysqli_stmt_bind_param($qryUS, 'si', $hidControlNumber, $sent);
					$qryUS->execute();
					$resultUS = mysqli_stmt_get_result($qryUS); //return results of query
					if(!empty($processErrorUS))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_job_order.php'.'</td><td>'.$processErrorUS.' near line 93.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else
					{ 
						if($hidJobOrderID)
							$_SESSION['SUCCESS']  = 'Successfully sent job request.';
						else
							$_SESSION['SUCCESS']  = 'Successfully added and sent new job request.';
						header("location:job_order.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					} // if-else !empty($processErrorUS)
				} // if-else !$mail->send
			} // else if save & send || send
			elseif( $btnSave == "Approve & Send" ){
				$hidSuperiorName = $_POST["hidSuperiorName"];
				$Subject= "NEW APPROVED JOB ORDER";
				$Body = "Control Number: ".$hidControlNumber."<br>"
						."Date: ".$hidRequestDate."<br>"
						."From: ".$RequisitionerName." - ".$DepartmentName."<br>"
						."Approved by: ".$hidSuperiorName."<br><br>"
						."Details of Job Request: ".$txtJobDetail;

				require("process_send_job_order.php");

				if(!$mail->send()){
					$sent = 1;
					$qryAC = mysqli_prepare($db, "CALL sp_Job_Order_Approve_CRU(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qryAC, 'iisi', $hidJobOrderID, $sent, $updatedAt, $updatedId);
					$qryAC->execute();
					$resultAC = mysqli_stmt_get_result($qryAC); //return results of query
					if(!empty($processErrorAC))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_job_order.php'.'</td><td>'.$processErrorAC.' near line 93.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else
					{ 
						$errmsg_arr[] = "Job request was approved but could not be sent. Please check your internet connection.";
						$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
						session_write_close();
						header("Location: new_job_order.php?id=$hidJobOrderID");
						exit();
					}
				}else{
					$sent = 2;
					$qryAC = mysqli_prepare($db, "CALL sp_Job_Order_Approve_CRU(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qryAC, 'iisi', $hidJobOrderID, $sent, $updatedAt, $updatedId);
					$qryAC->execute();
					$resultAC = mysqli_stmt_get_result($qryAC); //return results of query
					if(!empty($processErrorAC))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_job_order.php'.'</td><td>'.$processErrorAC.' near line 93.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else
					{ 
						$_SESSION['SUCCESS']  = "Job request was approved and sent successfully.";
						header("location:job_order.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
					} // if-else !empty($processErrorAC)
				}
			} // else if approve & send || send
		}
		require("include/database_close.php");
	}
?>
	