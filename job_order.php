<html>
	<head>
		<title>Job Order - Home</title>
		<?php 
			require("include/database_connect.php");
			
			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
		<script src="js/datetimepicker_css.js"></script>
		<script src="js/job_order_js.js"></script>
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> <!--   -->
	</head>
	<body>
		<?php
			require ("/include/header.php");
			require ("/include/init_unset_values/job_order_unset_value.php");

			// if( $_SESSION["customer"] == false) 
			// {
			// 	$_SESSION["ERRMSG_ARR"] ="Access denied!";
			// 	session_write_close();
			// 	header("Location:comsys.php");
			// 	exit();
			// }

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Job Order </h3> </span>

			<div class="search_box">
				<form method="get" action="job_order.php">
					<input type="hidden" name="page" value="<?php echo $_GET["page"]; ?>">
					<table class="search_tables_form">
						<tr>
							<td> Control Number: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]); ?>"> </td>
							<td> Date: </td>
							<td> <input type="text" name="qsone" id="qsone" value="<?php echo htmlspecialchars($_GET["qsone"]);?>"> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
							<td> <input type="submit" value="Search"> </td>
							<td> <input type="button" value="New Request" onclick="location.href='new_job_order.php?id=0'"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>job_order.php"."</td><td>".$error." near line 42.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryC = mysqli_prepare($db, "CALL sp_Job_Order_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryC, "ss", $search, $qsone);
					$qryC->execute();
					$resultC = mysqli_stmt_get_result($qryC);
					$total_results = mysqli_num_rows($resultC); //return number of rows of result

					$db->next_result();
					$resultC->close();

					$targetpage = "job_order.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Job_Order_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, "ssii", $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>job_order.php"."</td><td>".$processError." near line 63.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan="7">
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th> Date </th>
							    <th> Control Number </th>
							    <th> Department </th>
							    <th> Details </th>
							    <th> Status </th>
							    <th colspan="2"> </th>
							</tr>
							<?php
								while( $row = mysqli_fetch_assoc($result) ){
									$id = $row["id"];
									$date_of_request = $row["date_of_request"];
									$department_id = $row["department_id"];
									$department = $row["department"];
									$details = $row["details"];
								    $control_number = $row["control_number"];
								    $sent = $row["sent"];
								    $job_status = $row["job_status"];
								    $cancelled = $row["cancelled"];
							?>	
									<tr valign="top">
										<td> <?php echo $date_of_request;?> </td>
										<td> <?php echo $control_number;?> </td>
										<td> <?php echo $department;?> </td>
										<td> <?php echo nl2br($details);?> </td>
										<td> <?php echo ($cancelled == 1 ? "CANCELLED" : $job_status);?> </td>
										<td>
											<input type="button" value="View" onclick="location.href='new_job_order.php?id=<?php echo $id;?>'">
										</td>
										<td>
											<input type="button" value="Cancel" 
												onclick="cancelJobOrder('<?php echo $id;?>','<?php echo $control_number;?>')"
												<?php echo ($cancelled == 1 ? "disabled" : "");?>>
										</td>
									</tr>
							<?php
								}
							?>
							<tr>
								<td colspan="7">
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>