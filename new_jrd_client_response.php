<html>
	<head>
		<title>Client Response</title>
		<script src="js/batch_ticket_js.js"></script>
		<script src="js/datetimepicker_css.js"></script> 
		<?php
			require("include/database_connect.php");
		?>
	</head>
	<body onload='showJRDResults()'>

		<form method='post' action='process_new_jrd_client_response.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/jrd_init_value.php");
			?>

			<div class="wrapper">
				
				<span> <h3> New Client Response </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';
						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>
							JRD Number:
						</td>
						<td>
							<select name='sltJRD' id='sltJRD' onchange='showJRDResults()'>
								<?php
									$qry1 = "CALL sp_JRD_Dropdown()";
									$result1 = mysqli_query($db, $qry1);
									$processError1 = mysqli_error($db);

									if(!empty($processError1))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_jrd_client_response.php'.'</td><td>'.$processError1.' near line 31.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{ 
										while ( $row = mysqli_fetch_assoc( $result1 ) ){
											$jrd_id = $row['jrd_id'];
											$jrd_number = $row['jrd_number'];

											if ( $initJRDID == $jrd_id ){
												echo "<option value='".$jrd_id."' selected>".$jrd_number."</option>";	
											}else{
												echo "<option value='".$jrd_id."'>".$jrd_number."</option>";
											}
											
										}
										$db->next_result();
										$result1->close();
									}
								?>
							</select>
						</td>
					</tr>
				</table>

				<div id='JRDResults'>
				</div>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>