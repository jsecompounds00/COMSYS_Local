<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_toll_points.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidtollPointsId = $_POST['hidtollPointsId'];
		$sltArea = clean($_POST['sltArea']);
		$txtExitName = clean($_POST['txtExitName']);
		$txtExitCode = clean($_POST['txtExitCode']);

		if ( isset($_POST['chkActive']) )
			$chkActive = 1;
		else
			$chkActive = 0;

		// $_SESSION['sltArea'] = $sltArea;
		// $_SESSION['txtExitName'] = $txtExitName;
		// $_SESSION['txtExitCode'] = $txtExitCode;

		//Input Validations
		if ( empty($sltArea) ){
			$errmsg_arr[] = '* Area is missing.';
			$errflag = true;
		}
		if ( $txtExitName == '' || is_numeric($txtExitName) ){
			$errmsg_arr[] = '* Invalid exit name.';
			$errflag = true;
		}
		if ( $txtExitCode == '' || is_numeric($txtExitCode) || strlen($txtExitCode) > 3 ){
			$errmsg_arr[] = '* Invalid exit code.';
			$errflag = true;
		}


		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:new_toll_points.php?id=$hidtollPointsId");
			exit();
		}

		if($hidtollPointsId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidtollPointsId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		$qry = mysqli_prepare($db, "CALL sp_TollPoints_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'isssissii', $hidtollPointsId, $sltArea, $txtExitCode, $txtExitName, $chkActive, $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_toll_points.php'.'</td><td>'.$processError.' near line 86.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidtollPointsId)
				$_SESSION['SUCCESS']  = 'Successfully updated toll point.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new toll point.';
			//echo $_SESSION['SUCCESS'];
			header("location:toll_points.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		// $result->close(); 
		require("include/database_close.php");
	}
?>
	