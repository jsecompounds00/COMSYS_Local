<html>
	<head>
		<title>P.O. - Home</title>
		<?php
			require("include/database_connect.php");

			$po_number = ($_GET['po_number'] ? "%".$_GET['po_number']."%" : "");
			$pre_number = ($_GET['pre_number'] ? "%".$_GET['pre_number']."%" : "");
			$po_date = ($_GET['po_date'] ? $_GET['po_date'] : NULL);
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
		<script src="js/datetimepicker_css.js"></script>
		<script src="js/jscript.js"></script>  
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> <!--   -->
	</head>
	<body>

		<?php
			require '/include/header.php';
			require("/include/init_unset_values/po_monitoring_unset_value.php");

			if( $_SESSION["po_home"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["page"] = $_GET["page"];
			$_SESSION["po_date"] = $_GET["po_date"];
			$_SESSION["po_number"] = $_GET["po_number"];
			$_SESSION["pre_number"] = $_GET["pre_number"];
		?>

		<div class="wrapper">

			<span> <h3> P.O. Monitoring </h3> </span>

			<div class="search_box">
				<form method="get" action="po_monitoring.php">
					<input type="hidden" name="page" value="<?php echo $page; ?>">
					<table class="search_tables_form">
						<tr>
							<td> <label> P.O. Date:</label> </td>
							<td> <input type="text" name="po_date" id="po_date" value="<?php echo htmlspecialchars($_GET["po_date"]);?>"> </td>
							<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('po_date')" style="cursor:pointer" name="picker" /> </td>
							<td> <label>P.O. No.:</label> </td>
							<td> <input type="text" name="po_number" value="<?php echo htmlspecialchars($_GET["po_number"]); ?>"> </td>
							<td> <label>P.R.E. No.:</label> </td>
							<td> <input type="text" name="pre_number" value="<?php echo htmlspecialchars($_GET["pre_number"]); ?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php
									if(array_search(108, $session_Permit)){ 
								?>
										<input type='button' name='btnAddPO' value='New P.O.' onclick="location.href='new_po.php?id=0'">
								<?php 
									$_SESSION['add_po'] = true;
									}else{
										unset($_SESSION['add_po']);
									}
								?>
							</td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				// 
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>po_monitoring.php'.'</td><td>'.$error.' near line 60.</td></tr>', 3, "po_monitoring.php");
					header("location: error_message.html");
				}
				else
				{
					$qryP = mysqli_prepare($db, "CALL sp_PO_Home(?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryP, 'sss', $po_date, $po_number, $pre_number);
					$qryP->execute();
					$resultP = mysqli_stmt_get_result($qryP); //return results of query

					$total_results = mysqli_num_rows($resultP); //return number of rows of result
					
					$db->next_result();
					$resultP->close();

					$targetpage = "po_monitoring.php"; 	//your file name  (the name of this file)
					require("include/paginate_po_monitoring.php");

					$qry = mysqli_prepare($db, "CALL sp_PO_Home(?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sssii', $po_date, $po_number, $pre_number, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>po_monitoring.php'.'</td><td>'.$processError.' near line 79.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='10'> 
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
								<th> Purchased Order No. </th>
								<th> Transaction Date </th>
								<th> Expected Date of Delivery </th>
								<th> PREs </th>
								<th> Item Type </th>
								<th> Remarks </th>
								<th> Encoder </th>
								<?php
									if( array_search(109, $session_Permit) || array_search(170, $session_Permit) || array_search(171, $session_Permit) ){ 
								?>
										<th colspan='3'></th>
								<?php 
									} 
								?>
							</tr>
							<?php
								while( $row = mysqli_fetch_assoc( $result ) )
								{
									$id	= $row['id'];
									$po_number = $row['po_number'];
									$po_date = $row['po_date'];
									$exp_delivery_date = $row['exp_delivery_date'];
									$remarks = $row['remarks'];
									$PRES = $row['PRES'];
									$ItemType = $row['ItemType'];
									$username = $row['username'];
									$po_close = $row['po_close'];
									$po_cancel = $row['po_cancel'];
							?>
									<tr>
										<td> <?php echo $po_number;?> </td>

										<?php
											if ( $po_close == 1 || $po_cancel == 1 ){
												echo "<td colspan='9' align='center'>";
													if ( $po_close ){
														echo "<b>Closed: </b>".$remarks;
													}elseif ( $po_cancel ){
														echo "<b>Cancelled: </b>".$remarks;
													}
												echo "</td>";
											}else{
										?>
												<td> <?php echo $po_date;?> </td>
												<td> <?php echo $exp_delivery_date;?> </td>
												<td> <?php echo $PRES;?> </td>
												<td> <?php echo $ItemType;?> </td>
												<td> <?php echo $remarks;?> </td>
												<td> <?php echo $username;?> </td>
												<td>
													<?php
														if(array_search(109, $session_Permit)){
													?>
															<input type='button' name='btnEditPO' value='Edit PO' onclick="location.href='<?php echo PG_NEW_PO.$id;?>'">
													<?php
															$_SESSION['edit_po'] = true;
														}else{
															unset($_SESSION['edit_po']);
														}
													?>
												</td>

												<td>
													<?php
														if(array_search(170, $session_Permit)){
													?>
															<input type='button' name='btnClosePO' value='Close PO' onclick="closePOBox(<?php echo $id;?>, '<?php echo $po_number;?>')">
													<?php
														$_SESSION['close_po'] = true;
														}else{
															unset($_SESSION['close_po']);
														}
													?>
												</td>

												<td>
													<?php
														if(array_search(171, $session_Permit)){
													?>
															<input type='button' name='btnCancelPO' value='Cancel PO' onclick="cancelPOBox(<?php echo $id;?>, '<?php echo $po_number;?>')">
													<?php
														$_SESSION['cancel_po'] = true;
														}else{
															unset($_SESSION['cancel_po']);
														}
													?>
												</td>
										<?php
											}
										?>
									</tr>
							<?php
								}
							?>
							<tr>
								<td colspan='10'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>