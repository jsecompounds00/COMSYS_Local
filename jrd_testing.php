<html>
	<head>
		<script src="js/datetimepicker_css.js"></script> 
		<script src="js/batch_ticket_js.js"></script>

		<?php
			require("/include/database_connect.php");

			$JRDNoReplicateID = intval($_GET['test_id']);
			$JRDID = intval($_GET['jrd_id']);
			$FGID = intval($_GET['fg_id']);
			
			########## Getting data for Test Evaluation
			$qryTER = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_For_TestingIDS_Query( ?, ? )");
			mysqli_stmt_bind_param($qryTER, 'ii', $JRDNoReplicateID, $FGID);
			$qryTER->execute();
			$resultTER = mysqli_stmt_get_result($qryTER);
			$processErrorTER = mysqli_error($db);

			if ( !empty($processErrorTER) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>queries_new_batch_ticket.php'.'</td><td>'.$processErrorTER.' near line 212.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				while($row = mysqli_fetch_assoc($resultTER)){
					$complete_testing_id = $row['complete_testing_id'];
					$dynamic_milling_id = $row['dynamic_milling_id'];
					$water_immersion_id = $row['water_immersion_id'];
					$strand_inspect_id = $row['strand_inspect_id'];
					$pellet_inspect_id = $row['pellet_inspect_id'];
					$impact_test_id = $row['impact_test_id'];
					$static_heat_id = $row['static_heat_id'];
					$color_change_id = $row['color_change_id'];
					$cold_inspection_id = $row['cold_inspection_id'];
				}
				$db->next_result();
				$resultTER->close();
			}

			$qry1 = mysqli_prepare($db, "CALL sp_JRD_NoReplicate_For_Testing_Query(?, ?)");
			mysqli_stmt_bind_param($qry1, 'ii', $JRDNoReplicateID, $FGID);
			$qry1->execute();
			$result1 = mysqli_stmt_get_result($qry1);
			$processError1 = mysqli_error($db);

			if(!empty($processError1))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>batch_ticket_testing.php'.'</td><td>'.$processError1.' near line 31.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{ 
				while ( $row = mysqli_fetch_assoc( $result1 ) ){
					$JRDTestID = $row['JRDTestID'];
					$fg_name = $row['fg_name']; //product for comparison
					$product_for_evaluation = $row['product_for_evaluation'];
					$jrd_id = $row['jrd_id'];
					$fg_soft = $row['fg_soft'];
					$dynamic_milling = $row['dynamic_milling'];
					$oil_aging = $row['oil_aging'];
					$oven_aging = $row['oven_aging'];
					$strand_inspection = $row['strand_inspection'];
					$pellet_inspection = $row['pellet_inspection'];
					$impact_test = $row['impact_test'];
					$static_heating = $row['static_heating'];
					$color_change_test = $row['color_change_test'];
					$water_immersion = $row['water_immersion'];
					$cold_inspection = $row['cold_inspection'];
				}
				$db->next_result();
				$result1->close();
			}
		?>
		<title>Test Evaluation Results</title>
	</head>
	<body>

		<form method='post' action='process_jrd_testing.php'>

			<?php
				require("/include/header.php");
				require("/include/init_unset_values/jrd_init_value.php"); 
			?>

			<div class='wrapper'>
				
				<span> <h3> Test Evaluation for <b><?php echo ( $FGID == 0 ? $product_for_evaluation : $fg_name ); ?></b> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<div class='results_wrapper'>
					<table class="results_parent_tables_form">
						<tr>
							<td> <u> MECHANICAL, ELECTRIAL & PHYSICAL PROPERTIES </u> </td>
						</tr>
					</table>
					<?php
						require("/include/jrd_noreplicate_complete_testing.php"); 
					?>
				</div>
				<?php

					### Dynamic Milling
					if ( $dynamic_milling == 1 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> DYNAMIC MILLING </u> </td>
								</tr>
							</table>	
							<?php
								require("/include/jrd_noreplicate_dynamic_milling.php"); 
							?>
						</div>
				<?php
					}

					### Strands / Sheets Inspection
					if ( $strand_inspection == 1 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> STRANDS / SHEETS INSPECTION </u> </td>
								</tr>
							</table>	
							<?php
								require("/include/jrd_noreplicate_strand_inspection.php"); 
							?>
						</div>
				<?php
					}

					### Pellet Inspection
					if ( $pellet_inspection == 1 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> PELLET INSPECTION </u> </td>
								</tr>
							</table>	
							<?php
								require("/include/jrd_noreplicate_pellet_inspection.php"); 
							?>
						</div>
				<?php
					}

					### Impact Strength Test
					if ( $impact_test == 1 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> IMPACT / STRENGTH TEST </u> </td>
								</tr>
							</table>	
							<?php
								require("/include/jrd_noreplicate_impact_test.php"); 
							?>
						</div>
				<?php
					}

					### Static Heating
					if ( $static_heating == 1 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> STATIC HEATING </u> </td>
								</tr>
							</table>
							<?php
								require("/include/jrd_noreplicate_static_heating.php"); 
							?>
						</div>
				<?php
					}

					### Color Change Test
					if ( $color_change_test == 1 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> COLOR CHANGE TEST </u> </td>
								</tr>
							</table>
							<?php
								require("/include/jrd_noreplicate_color_change_test.php"); 
							?>
						</div>
				<?php
					}

					### Water Immersion
					if ( $water_immersion == 1 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> WATER IMMERSION </u> </td>
								</tr>
							</table>
							<?php
								require("/include/jrd_noreplicate_water_immersion.php"); 
							?>
						</div>
				<?php
					}

					### Cold Bend Test
					if ( $cold_inspection == 1 ){
				?>
						<div class='results_wrapper'>
							<table class="results_parent_tables_form">
								<tr>
									<td> <u> COLD BEND TEST </u> </td>
								</tr>
							</table>
							<?php
								require("/include/jrd_noreplicate_cold_inspection.php"); 
							?>
						</div>
				<?php
					}
				?>

				<table>
					<tr class="align_bottom">
						<td >
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type="button" name="btnBack" value="Back" onclick="location.href='new_jrd_test_evaluation_noreplicate.php'">	
							<input type="button" name="btnHome" value="JRD Home" onclick="location.href='jrd.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">	
							<input type='hidden' name='hidDynamicMilling' value='<?php echo $dynamic_milling;?>'>
							<input type='hidden' name='hidStrandInspection' value='<?php echo $strand_inspection;?>'>
							<input type='hidden' name='hidPelletInspection' value='<?php echo $pellet_inspection;?>'>
							<input type='hidden' name='hidImpactTest' value='<?php echo $impact_test;?>'>
							<input type='hidden' name='hidStaticHeating' value='<?php echo $static_heating;?>'>
							<input type='hidden' name='hidColorChangeTest' value='<?php echo $color_change_test;?>'>
							<input type='hidden' name='hidWaterImmersion' value='<?php echo $water_immersion;?>'>
							<input type='hidden' name='hidColdInspection' value='<?php echo $cold_inspection;?>'>
							<input type='hidden' name='hidOvenAging' value='<?php echo $oven_aging;?>'>
							<input type='hidden' name='hidOilAging' value='<?php echo $oil_aging;?>'>

							<input type='hidden' name='hidJRDNoReplicateID' value='<?php echo $JRDNoReplicateID;?>'>
							<input type='hidden' name='hidJRDID' value='<?php echo $JRDID;?>'>
							<input type='hidden' name='hidProductForComparisonID' value='<?php echo $FGID;?>'>
							<input type='hidden' name='hidProductForEvaluation' value='<?php echo ( $FGID == 0 ? $product_for_evaluation : NULL );?>'>
							<input type='hidden' name='hidFGSoft' value='<?php echo $fg_soft;?>'>
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			// require("include/database_close.php");
		?>
	</footer>
</html>