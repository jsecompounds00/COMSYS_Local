<html>
	<head>
		<title>Supply Type - Home</title>
		<?php 
			require("include/database_connect.php");
			
			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>
		<?php
			require ("/include/header.php");
			require("/include/init_unset_values/supply_types_unset_value.php");

			if( $_SESSION["supply_type"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">
			
			<span> <h3> Supply Types </h3> </span>

			<div class="search_box">
				<form method="get" action="supply_type.php">
					<input type="hidden" name="page" value="<?php echo $page;?>">
		 			<input type="hidden" name="qsone" value="<?php echo $qsone;?>">
					<table class="search_tables_form">
						<tr>
							<td> Name: </td>
							<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
							<td> <input type="submit" value="Search"> </td>
							<td>
								<?php
									if(array_search(2, $session_Permit)){
								?>
										<input type="button" name="btnAddSupplyType" value="Add Supply Type" onclick="location.href='<?php echo PG_NEW_SUPPLY_TYPE;?>0'">
								<?php
										$_SESSION["add_supplytype"] = true;
									}else{
										unset($_SESSION["add_supplytype"]);
									}
			 					?>
							</td>
						</tr>
					</table>
 				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>supply_type.php"."</td><td>".$error." near line 43.</td></tr></tbody>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					
					$qryST = mysqli_prepare($db, "CALL sp_SupplyType_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryST, "s", $search);
					$qryST->execute();
					$resultST = mysqli_stmt_get_result($qryST);
					$total_results = mysqli_num_rows($resultST); //return number of rows of result

					$db->next_result();
					$resultST->close();

					$targetpage = "supply_type.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					
					$qry = mysqli_prepare($db, "CALL sp_SupplyType_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>supply_type.php"."</td><td>".$processError." near line 64.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan="4">
									<?php echo $pagination; ?>
								</td>
							</tr>
						 	<tr>
							 	<th>Name</th>
								<th>Active?</th>
								<th>Comments</th>
								<th></th>
							</tr>
							<?php			
								while($row = mysqli_fetch_assoc($result))
								{
									$supplyTypeId = $row["id"];
									$compounds = ( $row["compounds"] ? "Y" : "N" );
									$active = ( $row["active"] ? "Y" : "N" );
									$supply_type = htmlspecialchars($row["supply_type"]);
									$comments = nl2br(htmlspecialchars($row["comments"]));
							?>
									<tr valign="top">
										<td><?php echo $supply_type?></td>
										<td><?php echo $active?></td>
										<td><?php echo $comments?></td>
										<td>
										<?php
											if(array_search(3, $session_Permit)){	
										?>
												<input type="button" name="btnEdit" value="EDIT" style="width: 50px" onclick="location.href='<?php echo PG_NEW_SUPPLY_TYPE.$supplyTypeId;?>'">
										<?php
												$_SESSION["edit_supplytype"] = true;
											}else{
												unset($_SESSION["edit_supplytype"]);
											}
										?>			
										</td>
									</tr>
							<?php
								} 
								$db->next_result();
								$result->close();
							?>
							<tr>
								<td colspan="6">
									<?php echo $pagination; ?>
								</td>
							</tr>
						</table>
			<?php
					} 
				}
			?>
		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>