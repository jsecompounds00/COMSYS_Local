<?php
	session_start();
	// if( $_SESSION['cancel_trial'] == false) 
	// {
	// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
	// 	session_write_close();
	// 	header("Location:comsys.php");
	// 	exit();
	// }	

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require("include/database_connect.php");
	require ("include/constant.php");

	$id = intval($_GET['id']);
	$remarks = strval($_GET['remarks']);
	$updated_at = date('Y-m-d H:i:s');
	$updated_id = $_SESSION['SESS_USER_ID'];
	$status = "Done";
	
	$qry = mysqli_prepare($db, "CALL sp_Ticket_Update_Status_CRU(?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isssi', $id, $status, $remarks, $updated_at, $updated_id);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_update_ticket_status.php'.'</td><td>'.$processError.' near line 32.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS'] = 'Status successfully updated.';
			header("location:ticketing_system.php?page=1&search=RECEIVED&qsone=");
		}
			require("include/database_close.php");
?>