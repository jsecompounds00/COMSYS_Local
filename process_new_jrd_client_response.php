<?php
########## Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

########## Array to store validation errors
	$errmsg_arr = array();
 
########## Validation error flag
	$errflag = false;
 
########## Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_jrd_client_response.php'.'</td><td>'.$error.' near line 25.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
########## Sanitize the POST values
		$hidRecommendationID = $_POST['hidRecommendationID'];
		$sltJRD = $_POST['sltJRD'];
		$txtRecommendation = $_POST['txtRecommendation'];
		$txtTestObjective = $_POST['txtTestObjective'];
		$txtIntroduction = $_POST['txtIntroduction'];
		$txtMethodology = $_POST['txtMethodology'];

		

########## Input Validations
		if ( !($sltJRD) ){
			$errmsg_arr[] = "*Select New Raw Materials.";
			$errflag = true;
		}
		if ( $txtRecommendation == "" ){
			$errmsg_arr[] = "*Recommendation can't be blank.";
			$errflag = true;
		}
		if ( $txtTestObjective == "" ){
			$errmsg_arr[] = "*Test objective can't be blank.";
			$errflag = true;
		}
		if ( $txtIntroduction == "" ){
			$errmsg_arr[] = "*Introduction can't be blank.";
			$errflag = true;
		}
		if ( $txtMethodology == "" ){
			$errmsg_arr[] = "*Methodology can't be blank.";
			$errflag = true;
		}

		if($hidRecommendationID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidRecommendationID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

	// $txtRecommendation = str_replace('\\r\\n', '<br>', $txtRecommendation);
	// $txtTestObjective = str_replace('\\r\\n', '<br>', $txtTestObjective);
	// $txtIntroduction = str_replace('\\r\\n', '<br>', $txtIntroduction);
	// $txtMethodology = str_replace('\\r\\n', '<br>', $txtMethodology);
	
	// $txtRecommendation = str_replace('\\', '', $txtRecommendation);
	// $txtTestObjective = str_replace('\\', '', $txtTestObjective);
	// $txtIntroduction = str_replace('\\', '', $txtIntroduction);
	// $txtMethodology = str_replace('\\', '', $txtMethodology);

############# SESSION, keeping last input value
		$_SESSION['sltJRD'] = $sltJRD;
		$_SESSION['txtRecommendation'] = $txtRecommendation;
		$_SESSION['txtTestObjective'] = $txtTestObjective;
		$_SESSION['txtIntroduction'] = $txtIntroduction;
		$_SESSION['txtMethodology'] = $txtMethodology;

########## If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_jrd_client_response.php?id=".$hidRecommendationID);
			exit();
		}
########## Committing to Database	
		$qry = mysqli_prepare($db, "CALL sp_BAT_JRD_Recommendation_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'iisssiisss', $hidRecommendationID, $sltJRD, $txtRecommendation
											    , $createdAt, $updatedAt, $createdId, $updatedId
											    , $txtTestObjective, $txtIntroduction, $txtMethodology);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry);	

		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_jrd_client_response.php'.'</td><td>'.$processError.' near line 139.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidRecommendationID)
				$_SESSION['SUCCESS']  = 'Successfully updated recommendation.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new recommendation.';
			//echo $_SESSION['SUCCESS'];
			header("location:jrd.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>