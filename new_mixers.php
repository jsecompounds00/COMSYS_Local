<html>
	<head>
		<title>Mixers - Edit</title>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_mixers.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$mixerId = $_GET['id'];

				if($mixerId)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Mixers_Query( ? )");
					mysqli_stmt_bind_param($qry, 'i', $mixerId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);

					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_mixers.php'.'</td><td>'.$processError.' near line 37.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$mixerId = $row['id'];
							$name = htmlspecialchars($row['name']);
							$brand = htmlspecialchars($row['brand']);
							$capacity = htmlspecialchars($row['capacity']);
						}
						$db->next_result();
						$result->close();
					}

					############ .............
					$qryPI = "SELECT id from comsys.mixers";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_mixers.php'.'</td><td>'.$processErrorPI.' near line 58.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($mixerId, $id) ){	//, TRUE
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_mixers.php</td><td>The user tries to edit a non-existing mixer_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					
				}
				else
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_mixers.php</td><td>The user tries to edit a non-existing mixer_id.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
			}
		?>
	</head>
	<body>

		<form method='post' action='process_new_mixers.php'>

			<?php 
				require("/include/header.php");
				require("/include/init_value.php");

				if( $_SESSION['edit_mixers'] == false) 
				{
					$_SESSION['ERRMSG_ARR'] ='Access denied!';
					session_write_close();
					header("Location:comsys.php");
					exit();
				}
			?>

			<div class="wrapper">
				
				<span> <h3> Edit <?php echo $name;?> (<?php echo $brand;?>) </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Capacity (kg):</td>
						<td>
							<input type='text' name='txtCapacity' value="<?php echo $capacity;?>">
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMixer" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='mixers.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidMixerId' value="<?php echo $mixerId;?>">
						</td>
					</tr>
				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>