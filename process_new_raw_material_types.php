<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>process_new_supply_type.php"."</td><td>".$error." near line 26.</td></tr>", 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidRMTypeID = $_POST["hidRMTypeID"];
		$txtRMCode = $_POST["txtRMCode"];
		$txtRMDescription = $_POST["txtRMDescription"];

		if(isset($_POST["chkCompounds"]))
			$chkCompounds = 1;
		else
			$chkCompounds = 0;
		if(isset($_POST["chkPipes"]))
			$chkPipes = 1;
		else
			$chkPipes = 0;
		if(isset($_POST["chkCorporate"]))
			$chkCorporate = 1;
		else
			$chkCorporate = 0;
		if(isset($_POST["chkPPR"]))
			$chkPPR = 1;
		else
			$chkPPR = 0;

		//Input Validations

		if ( $txtRMCode == "" ){
			$errmsg_arr[] = "* RM code can't be blank.";
			$errflag = true;
		}

		$_SESSION["SESS_RMT_NewSupplyType"] = $txtNewSupplyType;
		$_SESSION["SESS_RMT_Compounds"] = $chkCompounds;
		$_SESSION["SESS_RMT_Pipes"] = $chkPipes;
		$_SESSION["SESS_RMT_Corporate"] = $chkCorporate;
		$_SESSION["SESS_RMT_PPR"] = $chkPPR;
		$_SESSION["SESS_RMT_RMDescription"] = $txtRMDescription;

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION["ERRMSG_ARR"] = $errmsg_arr;
			session_write_close();
			header("Location: new_raw_material_types.php?id=".$hidRMTypeID);
			exit();
		}

		if($hidRMTypeID == 0)
			$createdAt = date("Y-m-d H:i:s");
		else $createdAt = $_POST["hidCreatedAt"];
		$updatedAt = date("Y-m-d H:i:s");
		if($hidRMTypeID == 0)
			$createdId = $_SESSION["SESS_USER_ID"];
		else $createdId = $_POST["hidCreatedId"];
		$updatedId = $_SESSION["SESS_USER_ID"];


		$qry = mysqli_prepare($db, "CALL sp_RawMaterial_Type_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, "issiiiissii", $hidRMTypeID, $txtRMCode, $txtRMDescription	
												  , $chkCompounds, $chkPipes, $chkCorporate, $chkPPR
												  , $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>process_new_supply_type.php"."</td><td>".$processError." near line 98.</td></tr>", 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidRMTypeID)
				$_SESSION["SUCCESS"]  = "Successfully updated rm type.";
			else
				$_SESSION["SUCCESS"]  = "Successfully added new rm type.";
			//echo $_SESSION["SUCCESS"];
			header("location: raw_material_types.php?page=".$_SESSION["page"]."&search=".$_SESSION["search"]."&qsone=".$_SESSION["qsone"]);	
		}

		$db->next_result();
		$result->close();
		require("include/database_close.php");
				
	}

?>