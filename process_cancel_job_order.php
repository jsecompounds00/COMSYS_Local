<?php
	session_start();
	// if( $_SESSION['cancel_trial'] == false) 
	// {
	// 	$_SESSION['ERRMSG_ARR'] ='Access denied!';
	// 	session_write_close();
	// 	header("Location:comsys.php");
	// 	exit();
	// }	

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
	require("include/database_connect.php");
	require ("include/constant.php");

	$id = intval($_GET['id']);
	$reason = strval($_GET['reason']);
	$control_number = strval($_GET['control_number']);
	$cancelled_at = date('Y-m-d H:i:s');
	$cancelled_id = $_SESSION['SESS_USER_ID'];
	
	$qry = mysqli_prepare($db, "CALL sp_Job_Order_Cancel(?, ?, ?, ?)");
	mysqli_stmt_bind_param($qry, 'isis', $id, $reason, $cancelled_id, $cancelled_at);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry);
	$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_cancel_job_order.php'.'</td><td>'.$processError.' near line 32.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			$_SESSION['SUCCESS'] = "Job order # $control_number was cancelled successfully.";
			header("location:job_order.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}
			require("include/database_close.php");
?>