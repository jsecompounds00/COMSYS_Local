<?php
	//Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_update_coq.php'.'</td><td>'.$error.' near line 27.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$hidCOQId = $_POST['hidCOQId'];
		$hidFGId = $_POST['hidFGId'];
	
		foreach ($_POST['hidCOQItemId'] as $COQItemId) {
			$COQItemId = array();
			$COQItemId = $_POST['hidCOQItemId'];
		}
		foreach ($_POST['hidCOQPropertyId'] as $COQPropertyId) {
			$COQPropertyId = array();
			$COQPropertyId = $_POST['hidCOQPropertyId'];
		}
		foreach ($_POST['txtOvenAged'] as $OvenAged) {
			$OvenAged = array();
			$OvenAged = $_POST['txtOvenAged'];
		}
		foreach ($_POST['txtOilAged'] as $OilAged) {
			$OilAged = array();
			$OilAged = $_POST['txtOilAged'];
		}
			
			$ctr = count($COQItemId);
			$i = 0;

			do{
				if ( ($OvenAged[$i] != '') && (!is_numeric($OvenAged[$i])) ){
					$errmsg_arr[] = "* Invalid value of oven aged (line $i).";
					$errflag = true;
				}
				if ( ($OilAged[$i] != '') && (!is_numeric($OilAged[$i])) ){
					$errmsg_arr[] = "* Invalid value of oil aged (line $i).";
					$errflag = true;
				}
				$i++;
			}while ( $i < $ctr );

		$updatedAt = date('Y-m-d H:i:s');
		$updatedId = $_SESSION['SESS_USER_ID'];

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: edit_coq.php?coq_id=$hidCOQId&fg_id=$hidFGId");
			exit();
		}

############# Committing in Database
	$qry = mysqli_prepare($db, "CALL sp_COQ_CRU( ?, ?, ? )");
	mysqli_stmt_bind_param($qry, 'isi', $hidCOQId, $updatedAt, $updatedId);
	$qry->execute();
	$result = mysqli_stmt_get_result($qry); //return results of query
	$processError = mysqli_error($db);

	if ( !empty($processError) ){
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_update_coq.php'.'</td><td>'.$processError.' near line 86.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}else{
		$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

		while($row = mysqli_fetch_assoc($qryLastId))
		{
			if ( $hidCOQId ) {
				$COQId = $hidCOQId;
			}else{
				$COQId = $row['LAST_INSERT_ID()'];
			}	
		}
		
		if(mysqli_affected_rows($db) > 0 ) 
		{
			foreach($_POST['hidCOQItemId'] as $key => $itemValue)
			{	
				if (!$hidCOQId) {
					$hidCOQItemId = 0;
				}

				if ($itemValue)
				{
					$qryItems = mysqli_prepare($db, "CALL sp_COQ_Item_CRU( ?, ?, ?, ?, ?, ?, ? )");
					mysqli_stmt_bind_param($qryItems, 'iiiddsi', $COQItemId[$key], $COQId, $COQPropertyId[$key],
																$OvenAged[$key], $OilAged[$key], $updatedAt, $updatedId);
					$qryItems->execute();
					$result = mysqli_stmt_get_result($qryItems); //return results of query
					$processError1 = mysqli_error($db);

					if ( !empty($processError1) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_update_coq.php'.'</td><td>'.$processError1.' near line 119.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
						if($hidCOQId)
							$_SESSION['SUCCESS']  = 'Successfully updated COQ.';
						else
							$_SESSION['SUCCESS']  = 'Successfully added new COQ.';
						// echo $_SESSION['SUCCESS'];
						header("location: new_coq.php?id=$hidFGId");
					}				
				}
			}
		}
	}
		require("include/database_close.php");
				
	}

?>