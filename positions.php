<html>
	<head>
		<title>Position - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['qsone'] ? "%".$_GET['qsone']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = "";
		?>
	</head>
	<body>

		<?php
			require ('/include/header.php');
			require ("include/unset_value.php");
			
			if( $_SESSION['position'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>


		<div class="wrapper">

			<span> <h3> Position </h3> </span>

			<div class="search_box">
		 		<form method='get' action='positions.php'>
					<input type='hidden' name='qsone' value="<?php echo $qsone;?>">
					<input type='hidden' name='page' value="<?php echo $page;?>">
					<table class="search_tables_form">
						<tr>
							<td> Position: </td>
							<td> <input type='text' name='search' value="<?php echo $_GET['qsone'];?>"> </td>
							<td> <input type='submit' value='Search'> </td>
							<!-- <td> <input type='button' name='btnAddPosition' value='Add Position' onclick="location.href='new_positions.php?id=0'"> </td> -->
						</tr>
					</table>
				</form>
			</div>

			<?php	
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>positions.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryC = mysqli_prepare($db, "CALL sp_Position_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryC, "s", $search);
					$qryC->execute();
					$resultC = mysqli_stmt_get_result($qryC);
					$total_results = mysqli_num_rows($resultC); //return number of rows of result

					$db->next_result();
					$resultC->close();

					$targetpage = "positions.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					
					$qry = mysqli_prepare($db, "CALL sp_Position_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sii", $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>positions.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						 <table class="home_pages">
							<tr>
								<td colspan='6'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>Name</th>
							    <th>Active?</th>
							    <th></th>
							</tr>
							<?php
								while($row = mysqli_fetch_assoc($result)){
							?>
									<tr>
										<td><?php echo $row['name'];?></td>
										<td><?php echo $row['active'] ? "Y" : "N";?></td>
										<td>
											<input type='button' name='btnEdit' value='EDIT' onclick="location.href='new_positions.php?id=<?php echo $row['id'];?>'">
										</td>
									</tr>
							<?php
								}
								$db->next_result();
								$result->close();
							?>
							<tr>
								<td colspan="6">
										<?php echo $pagination;?>
							  	</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>