<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast.php'.'</td><td>'.$error.' near line 11.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$forecastID = $_GET['id'];

				if($forecastID)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Sales_Forecast_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $forecastID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						$SFItemID = array();
						$FGID = array();
						$Quantity = array();
						while($row = mysqli_fetch_assoc($result))
						{
							$SFID = $row['SFID'];
							$FGLocal = $row['FGLocal'];
							$ForecastDate = $row['ForecastDate'];
							$remarks = htmlspecialchars($row['remarks']);
							$CreatedAt = $row['CreatedAt'];
							$CreatedID = $row['CreatedID'];

							$SFItemID[] = $row['SFItemID'];

							$FGID[] = $row['FGID'];

							$quantity[] = $row['quantity'];

						}
						$db->next_result();
						$result->close();
					}

					$count = count($SFItemID);

		############ .............
					$qryPI = "SELECT id from comsys.sales_forecasts";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($forecastID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast.php</td><td>The user tries to edit a non-existing forecast_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Sales Forecast - Edit</title>";
				}
				else{

					echo "<title>Sales Forecast - Add</title>";

				}
			}
		?>
		<script src="js/jscript.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body <?php if ( !$forecastID ) echo "onload='showFG_Forecast(0, 0)'" ?>>

		<form method='post' action='process_new_sales_forecast.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");			
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $forecastID ? "Edit " : "New " );?> Sales Forecast </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Forecast Date :</td>

						<td>
							<input type='text' name='txtMonthYear' id='txtMonthYear' value='<?php echo ( $forecastID ? $ForecastDate : $MonthYear ); ?>'>
						</td>

						<td>
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtMonthYear')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr>
						<td>FG Type :</td>

						<td>
							<input type='radio' name='radFGType' id='radLocal'  value='Local' onchange='showFG_Forecast(0, 0)'
								<?php echo ( $forecastID ? (  $FGLocal == 1 ? "checked" : "" ) : ( $FGType == 1 ? " checked" : "" ) );?>> 
								<label for='radLocal'>Local</label>

							<input type='radio' name='radFGType' id='radExport'  value='Export' onchange='showFG_Forecast(0, 0)'
								<?php echo ( $forecastID ? (  $FGLocal == 0 ? "checked" : "" ) : ( $FGType == 0 ? " checked" : "" ) );?>> 
								<label for='radExport'>Export</label>
						</td>
					</tr>
				</table>
				<table class="child_tables_form">
					<tr>
						<th>FG Item</th>
						<th>Quantity</th>
					</tr>
					<?php  
						if ( $forecastID ){
							for ( $i = 0; $i < $count; $i++ ){
					?>
								<tr>
									<td>
										<input type='hidden' name='hidForecastItemID[]' id='hidForecastItemID<?php echo $i; ?>' value='<?php echo $SFItemID[$i]; ?>'>
										<select name='sltFGItem[]' id='sltFGItem<?php echo $i; ?>'>
											<?php
												$qrySF = mysqli_prepare( $db, "CALL sp_FG_Dropdown_Sales(?)" );
												mysqli_stmt_bind_param( $qrySF, 'i', $FGLocal );
												$qrySF->execute();
												$resultSF = mysqli_stmt_get_result( $qrySF );
												$processErrorSF = mysqli_error($db);

												if(!empty($processError))
												{
													error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_sales_forecast.php'.'</td><td>'.$processErrorSF.' near line 23.</td></tr>', 3, "errors.php");
													header("location: error_message.html");
												}
												else
												{
														echo "<option></option>";
													while($row = mysqli_fetch_assoc($resultSF))
													{
														
														$FgId = $row['id'];
														$Fg = $row['name'];

														if ($FgId == $FGID[$i]){
															echo "<option value=".$FgId." selected>".$Fg."</option>";
														}else {
															echo "<option value=".$FgId.">".$Fg."</option>";
														}
													}
													$db->next_result();
													$resultSF->close();
												}
											?>
										</select>
									</td>
									<td>
										<input type='text' name='txtQuantity[]' id='txtQuantity<?php echo $i; ?>' value='<?php echo $quantity[$i]; ?>'>
									</td>
								</tr>
					<?php
							}
						}else{
							for ( $i = 0; $i < 20; $i++ ){
					?>
								<tr>
									<td>
										<input type='hidden' name='hidForecastItemID[]' id='hidForecastItemID<?php echo $i; ?>'>
										<select name='sltFGItem[]' id='sltFGItem<?php echo $i; ?>'>
										</select>
									</td>
									<td>
										<input type='text' name='txtQuantity[]' id='txtQuantity<?php echo $i; ?>' value='<?php echo $Quantity[$i]; ?>'>
									</td>
								</tr>
					<?php
							}
						}
					?>
				</table>

				<table class="comments_buttons">
					<tr valign='top'>
						<td>Remarks:</td>
						<td>
							<textarea name='txtRemarks'><?php
								if ( $forecastID ){
									echo $remarks;
								}else{
									echo $Remarks;
								};
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSave" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='sales_forecast.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidForecastID' id='hidForecastID' value="<?php echo $forecastID;?>">
							<input type='hidden' name='hidCreatedAt' value='<?php echo ( $forecastID ? $CreatedAt : date('Y-m-d H:i:s') );?>'>
							<input type='hidden' name='hidCreatedId' value='<?php echo ( $forecastID ? $CreatedID : $_SESSION["SESS_USER_ID"] );?>'>
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>