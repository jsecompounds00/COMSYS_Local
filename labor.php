<html>
	<head>
		<title> Labor - Home </title>
		<script src="js/datetimepicker_css.js"></script>
		<?php
			require("include/database_connect.php");

			$search = "";
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
		?>
	</head>
	<body>

		<?php
			require ('/include/header.php');
			require ("include/unset_value.php");

			if( $_SESSION['labor'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Labor </h3> </span>

			<div class="search_box">
		 		<form method='get' action='labor.php'>
		 			<input type='hidden' name='search' value='<?php echo $search;?>'>
		 			<input type='hidden' name='page' value="<?php echo $page;?>">
		 			<table class="search_tables_form">
		 				<tr>
		 					<td> Delivery Date: </td>
		 					<td> <input type='text' name='qsone' id='qsone' value="<?php echo $_GET['qsone'];?>"> </td>
		 					<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
		 					<td> <input type='submit' value='Search'> </td>
		 					<td> <input type='button' name='btnAddLabor' value='Add Labor' onclick="location.href='new_labor.php?id=0'"> </td>
		 				</tr>
		 			</table>
		 		</form>
		 	 </div>

		 	<?php
	 	 		if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>labor.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryL = mysqli_prepare($db, "CALL sp_Labor_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryL, 's', $qsone);
					$qryL->execute();
					$resultL = mysqli_stmt_get_result($qryL);
					$total_results = mysqli_num_rows($resultL); //return number of rows of result

					$db->next_result();
					$resultL->close();

					$targetpage = "labor.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Labor_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);

					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>labor.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						 <table class="home_pages">
							<tr>
								<td colspan='8'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th rowspan='2'> DSQ No. </th>
							    <th rowspan='2'> Date </th>
							    <th rowspan='2'> Truck No. </th>
							    <th colspan='2'> Head Count </th>
							    <th colspan='2'> Total No. of OT </th>
							    <th rowspan='2'></th>
							</tr>
							<tr>
							    <th> Driver </th>
							    <th> Helper </th>
							    <th> Driver </th>
							    <th> Helper </th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)){
									$laborID = $row['id'];
							?>
									<tr>
										<td> <?php echo $row['control_number'];?> </td>
										<td> <?php echo $row['del_date'];?> </td>
										<td> <?php echo $row['plate_number'];?> </td>
										<td> <?php echo $row['no_of_driver'];?> </td>
										<td> <?php echo $row['no_of_helper'];?> </td>
										<td> <?php echo $row['ot_driver'];?> </td>
										<td> <?php echo $row['ot_helper'];?> </td>
										<td>
											<input type='button' name='btnEdit' value='EDIT' onclick="location.href='new_labor.php?id=<?php echo $laborID;?>'">
										</td>
									</tr>
							<?php
								}
								$db->next_result();
								$result->close();
							?>
							<tr>
								<td colspan='8'>
										<?php echo $pagination; ?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>