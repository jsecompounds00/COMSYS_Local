<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_positions.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$posID = $_GET['id'];

				if($posID)
				{ 
					echo "<title>Positions - Edit</title>";

					$qry = mysqli_prepare($db, "CALL sp_Positions_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $posID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_positions.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$name = $row['name'];
							$daily_rate = $row['daily_rate'];
							$active = $row['active'];
							$createdAt = $row['created_at'];
							$createdId = $row['created_id'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.positions";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_positions.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($posID, $id, TRUE) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_positions.php</td><td>The user tries to edit a non-existing positions_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
				}
				else
				{
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_positions.php</td><td>The user tries to edit a non-existing positions_id.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
			}
			
		?>
	</head>
	<body>

		<form method='post' action='process_new_position.php'>

			<?php
				require("/include/header.php");
				require("/include/init_value.php");
			?>

			<div class="wrapper">

				<span> <h3> Edit <?php echo $name;?> </h3> </span>

				<?php
					if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR'])) {
						echo '<ul class="err">';

						foreach($_SESSION['ERRMSG_ARR'] as $msg) {
							echo '<li>'.$msg.'</li>'; 
						}

						echo '</ul>';

						unset($_SESSION['ERRMSG_ARR']);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Daily Rate:</td>
						<td>
							<input type='text' name='txtRate' value="<?php echo ( $posID ? $daily_rate : "" );?>">
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td colspan="3">
							<input type='checkbox' name='chkActive' <?php echo ( $posID ? ( $active ? "checked" : "" ) : "" );?>>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<input type="submit" name="btnSavePosition" value="Save">	
							<input type='hidden' name='txtName' value='<?php echo $name;?>'>
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='positions.php?page=1&search=&qsone='">
							<input type='hidden' name='hidPosID' value="<?php echo $posID;?>">
							<input type='hidden' name='hidCreatedAt' value="<?php echo ( $posID ? $createdAt : date('Y-m-d H:i:s') );?>">
							<input type='hidden' name='hidCreatedId' value="<?php echo ( $posID ? $createdId : $_SESSION["SESS_USER_ID"] );?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
