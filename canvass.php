<html>
	<head>
		<title>Canvassing - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
			$qsone = ($_GET['qsone'] ? $_GET['qsone'] : NULL);
		?>
		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body>
		<?php
			require("/include/header.php");
			require("/include/init_unset_values/canvass_unset_value.php");
			

			if( $_SESSION['canvass_home'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Canvassing </h3> </span>

			<div class="search_box">
				<form method="get" action="canvass.php">
					<input type="hidden" name="page" value="<?php echo $page;?>">
					<table class="search_tables_form">
						<td> Control Number: </td>
						<td> <input type="text" name="search" value="<?php echo $_GET["search"];?>"> </td>
						<td> Canvass Date: </td>
						<td> <input type="text" name="qsone" id="qsone" value="<?php echo $_GET["qsone"];?>"> </td>
						<td> <img src="js/cal.gif" onclick="javascript:NewCssCal('qsone')" style="cursor:pointer" name="picker" /> </td>
						<td> <input type="submit" value="Search"> </td>
						<td>
							<?php
								if(array_search(184, $session_Permit)){
							?>		
									<input type="button" value="Add Canvass" onclick="location.href='new_canvass.php?id=0'">
							<?php
									$_SESSION['canvass_new'] = true;
								}else{
									unset($_SESSION['canvass_new']);
								}
							?>
						</td>
					</table>						
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvass.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_Canvass_Home(?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'ss', $search, $qsone);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM); //return results of query

					$total_results = mysqli_num_rows($resultCM); //return number of rows of result

					$db->next_result();
					$resultCM->close();

					$targetpage = "canvass.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_Canvass_Home(?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssii', $search, $qsone, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>canvass.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='8'>
								<?php echo $pagination; ?>
							</td>
						</tr>
						<tr>
						    <th>Control No.</th>
						    <th>Date</th>
						    <th>PRE Number/s</th>
						    <th>Division</th>
						    <th></th>
						</tr>
						<?php
							while( $row = mysqli_fetch_assoc( $result ) ){
								$CanvassID = $row["CanvassID"];
								$ControlNumber = $row["ControlNumber"];
								$CanvassDate = $row["CanvassDate"];
								$PRENumbers = $row["PRENumbers"];
								$Division = $row["Division"];
								$PREItemID = $row["PREItemID"];
								$ItemType = $row["ItemType"];
								$ItemID = $row["ItemID"];
								$ItemDescription = $row["ItemDescription"];
						?>
								 <tr>
								 	<td> <?php echo $ControlNumber;?> </td>
								 	<td> <?php echo $CanvassDate;?> </td>
								 	<td> <?php echo $PRENumbers;?> </td>
								 	<td> <?php echo $Division;?> </td>
								 	<td>
								 		<?php
											if(array_search(185, $session_Permit)){
										?>		
												<input type="button" value="Edit Canvass" onclick="location.href='new_canvass.php?id=<?php echo $CanvassID;?>'">
										<?php
												$_SESSION['canvass_edit'] = true;
											}else{
												unset($_SESSION['canvass_edit']);
											}
										?>
								 	</td>
								 </tr>
						<?php
							}
							$db->next_result();
							$result->close();
						?>
						<tr>
							<td colspan='8'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>