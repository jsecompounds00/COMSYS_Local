<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	function validateDateTime($date, $format = "Y-m-d H:i") {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>process_new_material_balance_computation.php"."</td><td>".$error." near line 26.</td></tr>", 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidFGID = clean($_POST["hidFGID"]);
		$hidComputationID = $_POST["hidComputationID"];
		$sltIssuedFormula = clean($_POST["sltIssuedFormula"]);
		$sltCustomer = clean($_POST["sltCustomer"]);
		$txtPlannedShutdown = clean($_POST["txtPlannedShutdown"]);
		$txtUnplannedShutdown = clean($_POST["txtUnplannedShutdown"]);
		$txtRequiredOutput = clean($_POST["txtRequiredOutput"]);
		$txtMasterBatch = clean($_POST["txtMasterBatch"]);
		$txtMaterialIncorporation = clean($_POST["txtMaterialIncorporation"]);
		$txtLabSamples = clean($_POST["txtLabSamples"]);
		$txtUnplanLabSamples = clean($_POST["txtUnplanLabSamples"]);
		$txtOnholdPellet = clean($_POST["txtOnholdPellet"]);
		$txtScreenChange = clean($_POST["txtScreenChange"]);
		$txtFloorSweepingPellet = clean($_POST["txtFloorSweepingPellet"]);
		$txtOnholdPowder = clean($_POST["txtOnholdPowder"]);
		$txtChangeGrade = clean($_POST["txtChangeGrade"]);
		$txtFloorSweepingPowder = clean($_POST["txtFloorSweepingPowder"]);
		$txtEndRun = clean($_POST["txtEndRun"]);
		$txtScrapedCooling = clean($_POST["txtScrapedCooling"]);
		$txtScrapedMixer = clean($_POST["txtScrapedMixer"]);
		$txtStartUp = clean($_POST["txtStartUp"]);
		$txtMixedPellets = clean($_POST["txtMixedPellets"]);
		$txtUnplannedScrap = clean($_POST["txtUnplannedScrap"]);
		$txtReturnedBatch = clean($_POST["txtReturnedBatch"]);
		$txtRemarks = clean($_POST["txtRemarks"]);
		$txtHeatUp = clean($_POST["txtHeatUp"]);
		$txtIdealSpeed = clean($_POST["txtIdealSpeed"]);
		$txtFromBagEPM = clean($_POST["txtFromBagEPM"]);
		$txtThruBagEPM = clean($_POST["txtThruBagEPM"]);
		$txtUnderpackEPM = clean($_POST["txtUnderpackEPM"]);
		$txtReturnedBatchSmall = clean($_POST["txtReturnedBatchSmall"]);
		$txtRecoveryOnprocess = clean($_POST["txtRecoveryOnprocess"]);
		$txtRecoveryLeftOver = clean($_POST["txtRecoveryLeftOver"]);

		################ material balance computation ################

		$hidTotalComputationID = clean($_POST["hidTotalComputationID"]);
		$hidTotalInput = clean($_POST["hidTotalInput"]);
		$TotalGoodOutput = clean($_POST["hidGoodOutputEPM"]) + clean($_POST["hidGoodUnderpackEPM"]);
		$hidTotalScrap = clean($_POST["hidTotalScrap"]);
		$hidMCRate = clean($_POST["hidMCRate"]);
		$hidMCPerformance = clean($_POST["hidMCPerformance"]);
		$hidActReqd = clean($_POST["hidActReqd"]);
		$hidTOSAct = clean($_POST["hidTOSAct"]);
		$hidVarInOut = clean($_POST["hidVarInOut"]);
		$hidMaterialYield = clean($_POST["hidMaterialYield"]);

		################ material balance computation ################

		$counter = clean($_POST["counter"]);
			
		$txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
		
		$txtRemarks = str_replace('\\', '', $txtRemarks);

		foreach ($_POST['hidComputationItemID'] as $ComputationItemID) {
			$ComputationItemID = array();
			$ComputationItemID = $_POST['hidComputationItemID'];
		}

		foreach ($_POST['txtProductionStart'] as $ProductionStart) {
			$ProductionStart = array();
			$ProductionStart = $_POST['txtProductionStart'];
		}

		foreach ($_POST['txtProductionEnd'] as $ProductionEnd) {
			$ProductionEnd = array();
			$ProductionEnd = $_POST['txtProductionEnd'];
		}

		foreach ($_POST['hidTimeConsumed'] as $TimeConsumed) {
			$TimeConsumed = array();
			$TimeConsumed = $_POST['hidTimeConsumed'];
		}

		foreach ($_POST['txtLotNumberI'] as $LotNumberI) {
			$LotNumberI = array();
			$LotNumberI = $_POST['txtLotNumberI'];
		}

		foreach ($_POST['txtProductionDateI'] as $ProductionDateI) {
			$ProductionDateI = array();
			$ProductionDateI = $_POST['txtProductionDateI'];
		}

		foreach ($_POST['txtFromBagEPM'] as $FromBagEPM) {
			$FromBagEPM = array();
			$FromBagEPM = $_POST['txtFromBagEPM'];
		}

		foreach ($_POST['txtThruBagEPM'] as $ThruBagEPM) {
			$ThruBagEPM = array();
			$ThruBagEPM = $_POST['txtThruBagEPM'];
		}

		foreach ($_POST['txtUnderpackEPM'] as $UnderpackEPM) {
			$UnderpackEPM = array();
			$UnderpackEPM = $_POST['txtUnderpackEPM'];
		}

		//Input Validations
		if ( !($sltIssuedFormula) ){
			$errmsg_arr[] = "* Select issued formula.";
			$errflag = true;
		}
		if ( !($sltCustomer) ){
			$errmsg_arr[] = "* Select customer.";
			$errflag = true;
		}
		if ( $txtPlannedShutdown != '' && !is_numeric($txtPlannedShutdown) ){
			$errmsg_arr[] = "* Invalid value for planned shutdown.";
			$errflag = true;
		}elseif( $txtPlannedShutdown =="" ){
			$txtPlannedShutdown = NULL;
		}
		if ( $txtUnplannedShutdown != "" && !is_numeric($txtUnplannedShutdown) ){
			$errmsg_arr[] = "* Invalid value for unplanned shutdown.";
			$errflag = true;
		}elseif( $txtUnplannedShutdown =="" ){
			$txtUnplannedShutdown = NULL;
		}
		if ( !is_numeric($txtRequiredOutput) ){
			$errmsg_arr[] = "* Invalid value for required output.";
			$errflag = true;
		}
		if ( $txtHeatUp != "" && !is_numeric($txtHeatUp) ){
			$errmsg_arr[] = "* Invalid value for heat-up.";
			$errflag = true;
		}elseif ( $txtHeatUp == "" ){
			$txtHeatUp = NULL;
		}
		if ( $txtMasterBatch != "" && !is_numeric($txtMasterBatch) ){
			$errmsg_arr[] = "* Invalid value for master batch.";
			$errflag = true;
		}elseif( $txtMasterBatch =="" ){
			$txtMasterBatch = NULL;
		}
		if ( $txtMaterialIncorporation != "" && !is_numeric($txtMaterialIncorporation) ){
			$errmsg_arr[] = "* Invalid value for material incorporation.";
			$errflag = true;
		}elseif( $txtMaterialIncorporation =="" ){
			$txtMaterialIncorporation = NULL;
		}
		if ( !is_numeric($txtLabSamples) ){
			$errmsg_arr[] = "* Invalid value for planned lab samples.";
			$errflag = true;
		}
		if ( $txtUnplanLabSamples != "" && !is_numeric($txtUnplanLabSamples) ){
			$errmsg_arr[] = "* Invalid value for unplanned lab samples.";
			$errflag = true;
		}elseif( $txtUnplanLabSamples =="" ){
			$txtUnplanLabSamples = NULL;
		}
		if ( $txtOnholdPellet != "" && !is_numeric($txtOnholdPellet) ){
			$errmsg_arr[] = "* Invalid value for on-hold pellet.";
			$errflag = true;
		}elseif( $txtOnholdPellet =="" ){
			$txtOnholdPellet = NULL;
		}
		if ( $txtOnholdPowder != "" && !is_numeric($txtOnholdPowder) ){
			$errmsg_arr[] = "* Invalid value for on-hold powder.";
			$errflag = true;
		}elseif( $txtOnholdPowder =="" ){
			$txtOnholdPowder = NULL;
		}
		if ( $txtScreenChange != "" && !is_numeric($txtScreenChange) ){
			$errmsg_arr[] = "* Invalid value for screen change.";
			$errflag = true;
		}elseif( $txtScreenChange =="" ){
			$txtScreenChange = NULL;
		}
		if ( $txtChangeGrade != "" && !is_numeric($txtChangeGrade) ){
			$errmsg_arr[] = "* Invalid value for change grade.";
			$errflag = true;
		}elseif( $txtChangeGrade =="" ){
			$txtChangeGrade = NULL;
		}
		if ( $txtEndRun != "" && !is_numeric($txtEndRun) ){
			$errmsg_arr[] = "* Invalid value for end run.";
			$errflag = true;
		}elseif( $txtEndRun =="" ){
			$txtEndRun = NULL;
		}
		if ( $txtFloorSweepingPellet != "" && !is_numeric($txtFloorSweepingPellet) ){
			$errmsg_arr[] = "* Invalid value for floor sweeping pellet.";
			$errflag = true;
		}elseif( $txtFloorSweepingPellet =="" ){
			$txtFloorSweepingPellet = NULL;
		}
		if ( $txtFloorSweepingPowder != "" && !is_numeric($txtFloorSweepingPowder) ){
			$errmsg_arr[] = "* Invalid value for floor sweeping powder.";
			$errflag = true;
		}elseif( $txtFloorSweepingPowder =="" ){
			$txtFloorSweepingPowder = NULL;
		}
		if ( $txtScrapedCooling != "" && !is_numeric($txtScrapedCooling) ){
			$errmsg_arr[] = "* Invalid value for scraped powder in cooling blender.";
			$errflag = true;
		}elseif( $txtScrapedCooling =="" ){
			$txtScrapedCooling = NULL;
		}
		if ( $txtScrapedMixer != "" && !is_numeric($txtScrapedMixer) ){
			$errmsg_arr[] = "* Invalid value for scraped powder in mixer.";
			$errflag = true;
		}elseif( $txtScrapedMixer =="" ){
			$txtScrapedMixer = NULL;
		}
		if ( $txtStartUp != "" && !is_numeric($txtStartUp) ){
			$errmsg_arr[] = "* Invalid value for start-up.";
			$errflag = true;
		}elseif( $txtStartUp =="" ){
			$txtStartUp = NULL;
		}
		if ( $txtMixedPellets != "" && !is_numeric($txtMixedPellets) ){
			$errmsg_arr[] = "* Invalid value for mixed pellets.";
			$errflag = true;
		}elseif( $txtMixedPellets =="" ){
			$txtMixedPellets = NULL;
		}
		if ( $txtUnplannedScrap != "" && !is_numeric($txtUnplannedScrap) ){
			$errmsg_arr[] = "* Invalid value for unplanned scrap.";
			$errflag = true;
		}elseif( $txtUnplannedScrap =="" ){
			$txtUnplannedScrap = NULL;
		}
		if ( $txtReturnedBatch != "" && !is_numeric($txtReturnedBatch) ){
			$errmsg_arr[] = "* Invalid value for returned batches (big).";
			$errflag = true;
		}elseif( $txtReturnedBatch =="" ){
			$txtReturnedBatch = NULL;
		}
		if ( $txtReturnedBatchSmall != "" && !is_numeric($txtReturnedBatchSmall) ){
			$errmsg_arr[] = "* Invalid value for returned batches (small).";
			$errflag = true;
		}elseif( $txtReturnedBatchSmall =="" ){
			$txtReturnedBatchSmall = NULL;
		}
		if ( $txtIdealSpeed != "" && !is_numeric($txtIdealSpeed) ){
			$errmsg_arr[] = "* Invalid value for ideal speed.";
			$errflag = true;
		}elseif( $txtIdealSpeed =="" ){
			$txtIdealSpeed = NULL;
		}
		if ( $txtRecoveryOnprocess != "" && !is_numeric($txtRecoveryOnprocess) ){
			$errmsg_arr[] = "* Invalid value for recovery - on process.";
			$errflag = true;
		}elseif( $txtRecoveryOnprocess =="" ){
			$txtRecoveryOnprocess = NULL;
		}
		if ( $txtRecoveryLeftOver != "" && !is_numeric($txtRecoveryLeftOver) ){
			$errmsg_arr[] = "* Invalid value for recovery- left over from RM section.";
			$errflag = true;
		}elseif( $txtRecoveryLeftOver =="" ){
			$txtRecoveryLeftOver = NULL;
		}

			$_SESSION['SESS_MBC_IssuedFormula'] = $sltIssuedFormula;
			$_SESSION['SESS_MBC_Customer'] = $sltCustomer;
			$_SESSION['SESS_MBC_PlannedShutdown'] = $txtPlannedShutdown;
			$_SESSION['SESS_MBC_UnplannedShutdown'] = $txtUnplannedShutdown;
			$_SESSION['SESS_MBC_RequiredOutput'] = $txtRequiredOutput;
			$_SESSION['SESS_MBC_MasterBatch'] = $txtMasterBatch;
			$_SESSION['SESS_MBC_MaterialIncorporation'] = $txtMaterialIncorporation;
			$_SESSION['SESS_MBC_LabSamples'] = $txtLabSamples;
			$_SESSION['SESS_MBC_UnplanLabSamples'] = $txtUnplanLabSamples;
			$_SESSION['SESS_MBC_OnholdPellet'] = $txtOnholdPellet;
			$_SESSION['SESS_MBC_OnholdPowder'] = $txtOnholdPowder;
			$_SESSION['SESS_MBC_ScreenChange'] = $txtScreenChange;
			$_SESSION['SESS_MBC_ChangeGrade'] = $txtChangeGrade;
			$_SESSION['SESS_MBC_EndRun'] = $txtEndRun;
			$_SESSION['SESS_MBC_FloorSweepingPellet'] = $txtFloorSweepingPellet;
			$_SESSION['SESS_MBC_FloorSweepingPowder'] = $txtFloorSweepingPowder;
			$_SESSION['SESS_MBC_ScrapedCooling'] = $txtScrapedCooling;
			$_SESSION['SESS_MBC_ScrapedMixer'] = $txtScrapedMixer;
			$_SESSION['SESS_MBC_StartUp'] = $txtStartUp;
			$_SESSION['SESS_MBC_MixedPellets'] = $txtMixedPellets;
			$_SESSION['SESS_MBC_UnplannedScrap'] = $txtUnplannedScrap;
			$_SESSION['SESS_MBC_ReturnedBatch'] = $txtReturnedBatch;
			$_SESSION['SESS_MBC_Remarks'] = $txtRemarks;
			$_SESSION['SESS_MBC_HeatUp'] = $txtHeatUp;
			$_SESSION['SESS_MBC_IdealSpeed'] = $txtIdealSpeed;
			$_SESSION['SESS_MBC_ReturnedBatchSmall'] = $txtReturnedBatchSmall;
			$_SESSION['SESS_MBC_TOSAct'] = $hidTOSAct;
			$_SESSION['SESS_MBC_VarInOut'] = $hidVarInOut;
			$_SESSION['SESS_MBC_MaterialYield'] = $hidMaterialYield;
			$_SESSION['SESS_MBC_RecoveryOnprocess'] = $txtRecoveryOnprocess;
			$_SESSION['SESS_MBC_RecoveryLeftOver'] = $txtRecoveryLeftOver;

			for ($i=0; $i < $counter; $i++) { 
				$_SESSION['SESS_MBC_ProductionStart'][$i] = $ProductionStart[$i];
				$_SESSION['SESS_MBC_ProductionEnd'][$i] = $ProductionEnd[$i];
				$_SESSION['SESS_MBC_TimeConsumed'][$i] = $TimeConsumed[$i];
				$_SESSION['SESS_MBC_FromBagEPM'][$i] = $FromBagEPM[$i];
				$_SESSION['SESS_MBC_ThruBagEPM'][$i] = $ThruBagEPM[$i];
				$_SESSION['SESS_MBC_UnderpackEPM'][$i] = $UnderpackEPM[$i];
			}

		if($hidComputationID == 0)
			$createdAt = date("Y-m-d H:i:s");
		else $createdAt = $_POST["hidCreatedAt"];
		$updatedAt = date("Y-m-d H:i:s");
		if($hidComputationID == 0)
			$createdId = $_SESSION["SESS_USER_ID"];
		else $createdId = $_POST["hidCreatedId"];
		$updatedId = $_SESSION["SESS_USER_ID"];

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION["ERRMSG_ARR"] = $errmsg_arr;
			session_write_close();
			header("Location: new_material_balance_computation.php?fg_id=$hidFGID&mbc_id=$hidComputationID");
			exit();
		}

		$qry = mysqli_prepare($db, "CALL sp_Material_Balance_Computation_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
																		   , ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, "iiiiiidddddddddddddddddisssisididd", $hidComputationID, $hidFGID, $sltIssuedFormula
																		    , $sltCustomer, $txtPlannedShutdown, $txtUnplannedShutdown, $txtRequiredOutput
																		    , $txtMaterialIncorporation, $txtMasterBatch, $txtLabSamples, $txtUnplanLabSamples
																		    , $txtOnholdPellet, $txtOnholdPowder, $txtScreenChange, $txtChangeGrade
																		    , $txtEndRun, $txtFloorSweepingPellet, $txtFloorSweepingPowder, $txtStartUp
																		    , $txtMixedPellets, $txtUnplannedScrap, $txtScrapedCooling, $txtScrapedMixer
																		    , $txtReturnedBatch, $txtRemarks, $createdAt, $updatedAt, $createdId
																		    , $updatedId, $txtHeatUp, $txtIdealSpeed, $txtReturnedBatchSmall
																		    , $txtRecoveryOnprocess, $txtRecoveryLeftOver);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if ( !empty($processError) ){
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_material_balance_computation.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}else{
			$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

			while($row = mysqli_fetch_assoc($qryLastId))
			{
				if ( $hidComputationID ) {
					$ComputationID = $hidComputationID;
				}else{
					$ComputationID = $row['LAST_INSERT_ID()'];
				}	
			}
			
			if(mysqli_affected_rows($db) > 0 ) 
			{
				foreach($_POST['txtProductionStart'] as $key => $itemValue)
				{	
					if ( !$FromBagEPM[$key] ){
						$FromBagEPM[$key] = NULL;
					}
					if ( !$ThruBagEPM[$key] ){
						$ThruBagEPM[$key] = NULL;
					}
					if ( !$UnderpackEPM[$key] ){
						$UnderpackEPM[$key] = NULL;
					}

					if (!$hidComputationID) {
						$ComputationItemID[$key] = 0;
					}

					if ($itemValue)
					{
						$qryItems = mysqli_prepare($db, "CALL sp_Material_Balance_Computation_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
						mysqli_stmt_bind_param($qryItems, 'iississiid', $ComputationItemID[$key], $ComputationID, $ProductionStart[$key]
																	   , $ProductionEnd[$key], $TimeConsumed[$key], $LotNumberI[$key]
																	   , $ProductionDateI[$key], $FromBagEPM[$key], $ThruBagEPM[$key]
																	   , $UnderpackEPM[$key]);
						$qryItems->execute();
						$result = mysqli_stmt_get_result($qryItems); //return results of query
						$processError1 = mysqli_error($db);

						if(!empty($processError1))
						{
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>process_new_material_balance_computation.php"."</td><td>".$processError1." near line 93.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}			
					}
				}
			}

			$qryCompute = mysqli_prepare($db, "CALL sp_Material_Balance_Total_Computation_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
			mysqli_stmt_bind_param($qryCompute, 'iiiddddddddd', $hidTotalComputationID, $ComputationID, $sltIssuedFormula, $hidTotalInput, $TotalGoodOutput
															  , $hidTotalScrap, $hidMCRate, $hidMCPerformance, $hidActReqd, $hidTOSAct, $hidVarInOut
															  , $hidMaterialYield);
			$qryCompute->execute();
			$resultCompute = mysqli_stmt_get_result($qryCompute); //return results of query
			$processError2 = mysqli_error($db);

			if(!empty($processError2))
			{
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>process_new_material_balance_computation.php"."</td><td>".$processError2." near line 93.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{ 
				if($hidComputationID)
					$_SESSION["SUCCESS"]  = "Successfully updated material balance computation.";
				else
					$_SESSION["SUCCESS"]  = "Successfully added new material balance computation.";
				header("location:pam_analysis.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
			}

		}	

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");
	}
?>
	