<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_customer.php"."</td><td>".$error." near line 9.</td></tr></tbody>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$custId = $_GET["id"];

				if($custId)
				{ 
					
					echo "<title>Customer - Edit</title>";

					$qry = mysqli_prepare($db, "CALL sp_Customer_Query( ? )");
					mysqli_stmt_bind_param($qry, "i", $custId);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_customer.php"."</td><td>".$processError." near line 35.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							// $custId = $row["id"];
							$name = htmlspecialchars($row["name"]);
							$active = $row["active"];
							$rm = $row["rm"];
							$fg = $row["fg"];
							$local = $row["local"];
							$supplies = $row["supplies"];
							$terms_days = htmlspecialchars($row["terms_days"]);
							$terms_credit = $row["terms_credit"];
							$credit_limit_amount = $row["credit_limit_amount"];
							$credit_limit_currency = $row["credit_limit_currency"];
							$agent = $row["agent"];
							$address_street = htmlspecialchars($row["address_street"]);
							$address_baranggay = htmlspecialchars($row["address_baranggay"]);
							$address_city_province = htmlspecialchars($row["address_city_province"]);
							$zip_code = htmlspecialchars($row["zip_code"]);
							$contact_title = $row["contact_title"];
							$contact_person_fname = htmlspecialchars($row["contact_person_fname"]);
							$contact_person_lname = htmlspecialchars($row["contact_person_lname"]);
							$contact_number = htmlspecialchars($row["contact_number"]);
							$terms_grace_period = $row["terms_grace_period"];
						}
					}
					$db->next_result();
					$result->close();

					############ .............
					$qryPI = "SELECT id from comsys.customers";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_customer.php"."</td><td>".$processErrorPI." near line 61.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($custId, $id, TRUE) ){ 
						error_log("<tbody><tr><td>".date("F d, Y H:i:s")."</td><td>new_customer.php</td><td>The user tries to edit a non-existing customer_id.</td></tr></tbody>", 3, "errors.php");
						header("location: error_message.html");
					}
				}
				else
				{
				
					echo "<title>Customer - Add</title>";
				}
			}
		?>
		<script src="js/jscript.js"></script>
	</head>
	<body>
		<form method="post" action="process_new_customer.php">
			<?php 
				require("/include/header.php");
				require ("/include/init_unset_values/customers_init_value.php");

				$address_street = (is_null($address_street) || $address_street == "" ? $initCUSAddressStreet : $address_street);
				$address_baranggay = (is_null($address_baranggay) || $address_baranggay == "" ? $initCUSAddressBaranggay : $address_baranggay);
				$address_city_province = (is_null($address_city_province) || $address_city_province == "" ? $initCUSAddressCityProvince : $address_city_province);
				$zip_code = (is_null($zip_code) || $zip_code == "" ? $initCUSZipCode : $zip_code);
				$local = (is_null($local) || $local == "" ? $initCUSCustType : $local);
				$terms_days = (is_null($terms_days) || $terms_days == "" ? $initCUSTermsDays : $terms_days);
				$terms_credit = (is_null($terms_credit) || $terms_credit == "" ? $initCUSTerms : $terms_credit);
				$credit_limit_amount = (is_null($credit_limit_amount) || $credit_limit_amount == "" ? $initCUSCreditLimit : $credit_limit_amount);
				$credit_limit_currency = (is_null($credit_limit_currency) || $credit_limit_currency == "" ? $initCUSCreditLimitUnit : $credit_limit_currency);
				$agent = (is_null($agent) || $agent == "" ? $initCUSAgent : $agent);
				$contact_title = (is_null($contact_title) || $contact_title == "" ? $initCUSTitle : $contact_title);
				$contact_person_fname = (is_null($contact_person_fname) || $contact_person_fname == "" ? $initCUSContactFName : $contact_person_fname);
				$contact_person_lname = (is_null($contact_person_lname) || $contact_person_lname == "" ? $initCUSContactLName : $contact_person_lname);
				$contact_number = (is_null($contact_number) || $contact_number == "" ? $initCUSContactNumber : $contact_number);
				$terms_grace_period = (is_null($terms_grace_period) || $terms_grace_period == "" ? $initCUSGracePeriod : $terms_grace_period);

				if ( $custId ){
					if( $_SESSION["edit_customer"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}
			?>
			<div class="wrapper">
				
				<span> <h3> <?php echo ( $custId ? "Edit ".$name : "New Customer" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {
						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";
						unset($_SESSION["ERRMSG_ARR"]);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Name:</td>
						<td colspan="2">
							<b> <?php echo $name;?> </b>
						</td>
					</tr>
					<tr>
						<td>Address 1 (Street):</td>
						<td colspan="2">
							<input type="text" name="txtAddressStreet" size="40" value="<?php echo $address_street;?>"> 
						</td>
					</tr>
					<tr>
						<td>Address 2 (Baranggay):</td>
						<td colspan="2"> 
							<input type="text" name="txtAddressBaranggay" size="40" value="<?php echo $address_baranggay;?>">
						</td>
					</tr>
					<tr>
						<td>Address 3 (City/Province):</td>
						<td colspan="2"> 
							<input type="text" name="txtAddressCityProvince" size="40" value="<?php echo $address_city_province;?>">
						</td>
					</tr>
					<tr>
						<td>Zip Code:</td>
						<td colspan="2"> 
							<input type="text" name="txtZipCode" value="<?php echo $zip_code;?>">
						</td>
					</tr>
					<tr>
						<td> 
							Contact:
							<label class="instruction">
								(Optional)
							</label>
						</td>
					</tr>
					<tr>
						<td> <dd> Title: </dd> </td>
						<td>
							<input type="radio" name="radTitle" id="Sir" value="Sir" <?php echo ( $contact_title == "Sir" ? "checked" : "" );?>> <label for="Sir">Sir</label>
							<input type="radio" name="radTitle" id="Madam" value="Madam" <?php echo ( $contact_title == "Madam" ? "checked" : "" );?>> <label for="Madam">Madam</label>
						</td>
					</tr>
					<tr>
						<td> <dd> First Name: </dd> </td>
						<td> 
							<input type="text" name="txtContactFName" value="<?php echo $contact_person_fname;?>">
						</td>
						<td> Last Name: </td>
						<td> 
							<input type="text" name="txtContactLName" value="<?php echo $contact_person_lname;?>">
						</td>
					</tr>
					<tr>
						<td> <dd> Number: </dd> </td>
						<td> 
							<input type="text" name="txtContactNumber" value="<?php echo $contact_number;?>"> 
						</td>
					</tr>
					<tr>
						<td> Type: </td>
						<td colspan="2">
							<input type="radio" name="radCustType" id="local" value="local" <?php echo ( $local == 1 ? "checked" : "" );?>> <label for="local"> Local </label>
							<input type="radio" name="radCustType" id="export" value="export" <?php echo ( $local == 1 ? "" : "checked" );?>> <label for="export"> Export </label>
						</td>
					</tr>
					<tr>
						<td> Terms: </td>
						<td>
							<input type='text' name='txtTerms' id='txtTerms' size='3' value="<?php echo $terms_days;?>">
							
							Days
							
							<select name="sltTerms" id="sltTerms" onchange="enableTerms()">
								<option value="LC" <?php echo ( $terms_credit == "LC" ? "selected" : "" ); ?>>LC</option>
								<option value="TT" <?php echo ( $terms_credit == "TT" ? "selected" : "" ); ?>>TT</option>
								<option value="PDC" <?php echo ( $terms_credit == "PDC" ? "selected" : "" ); ?>>PDC</option>
								<option value="COD" <?php echo ( $terms_credit == "COD" ? "selected" : "" ); ?>>COD</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Grace Period:</td>
						<td>
							<input type="text"  name="txtGracePeriod" size="3" value="<?php echo $terms_grace_period;?>">
								
							Days
						</td>
					</tr>
					<tr>
						<td>Credit Limit:</td>
						<td colspan="2"> 
							<input type="radio" name="radCreditLimitUnit" id="Php" value="Php" <?php echo ( $credit_limit_currency == "Php" ? "checked" : "" );?>> <label for="Php"> Php</label>
							<input type="radio" name="radCreditLimitUnit" id="USD" value="USD" <?php echo ( $credit_limit_currency == "USD" ? "checked" : "" );?>> <label for="USD"> USD</label> 
							<input type='text' name='txtCreditLimit' value="<?php echo $credit_limit_amount;?>">
						</td>
					</tr>
					<tr>
						<td>Agent:</td>
						<td>
							<select name="sltAgent">
								<option value="EHV" <?php echo ( $agent == "EHV" ? "selected" : "" );?>>EHV</option>
								<option value="JPO" <?php echo ( $agent == "JPO" ? "selected" : "" );?>>JPO</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<b> <?php echo ( $active ? "Yes" : "No" );?> </b>
						</td>
					</tr>
					<tr>
						<td>Applicable To:</td>
						<td>
							<b>
							<?php
								if($rm) echo "RM";
								if($fg) echo "FG";
								if($supplies) echo "Supplies";
							?>
							</b>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveCust" value="Save">
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='customer.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidcustId" value="<?php echo $custId;?>">
						</td>
					</tr>
				</table>

			</div>
		</form>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
