<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_icc_monitoring.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$iccID = $_GET["id"];

				if($iccID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_ICC_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $iccID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_icc_monitoring.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							// $ICCID = $row["id"];
							$icc_number = htmlspecialchars($row["icc_number"]);
							$icc_date = $row["icc_date"];
							$due_date = $row["due_date"];
							$issuing_area = $row["issuing_area"];
							$receiving_area = $row["receiving_area"];
							$reminder_date = $row["reminder_date"];
							$final_due_date = $row["final_due_date"];
							$dcc_receipt_date = $row["dcc_receipt_date"];
							$nature_of_complaint = htmlspecialchars($row["nature_of_complaint"]);
							$created_at = $row["created_at"];
							$created_id = $row["created_id"];
							$root_cause_analysis = htmlspecialchars($row["root_cause_analysis"]);
							$containment_action = htmlspecialchars($row["containment_action"]);
							$containment_implementation = htmlspecialchars($row["containment_implementation"]);
							$containment_responsible = htmlspecialchars($row["containment_responsible"]);
							$corrective_action = htmlspecialchars($row["corrective_action"]);
							$corrective_implementation = htmlspecialchars($row["corrective_implementation"]);
							$corrective_responsible = htmlspecialchars($row["corrective_responsible"]);
							$root_category_man = $row["root_category_man"];
							$root_category_method = $row["root_category_method"];
							$root_category_machine = $row["root_category_machine"];
							$root_category_material = $row["root_category_material"];
							$root_category_measurement = $row["root_category_measurement"];
							$root_category_mother_nature = $row["root_category_mother_nature"];
							$remarks = htmlspecialchars($row["remarks"]);
							$issuing_division = $row["issuing_division"];
							$receiving_division = $row["receiving_division"];
							$initial_verification_month_id = $row["initial_verification_month_id"];
							$initial_verification_year = $row["initial_verification_year"];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.icc_monitoring";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_icc_monitoring.php"."</td><td>".$processErrorPI." near line 61.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($iccID, $id, TRUE) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_icc_monitoring.php</td><td>The user tries to edit a non-existing icc_id.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>ICC Monitoring - Edit</title>";
				}
				else{

					echo "<title>ICC Monitoring - Add</title>";

				}
			}
		?>
		<script src="js/jscript.js"></script>  
  		<script src="js/datetimepicker_css.js"></script>
	</head>
	<body <?php if( !$iccID ){ ?> onload="showIssDepartment(0,0), showRecDepartment(0,0)" <?php } ?>>

		<form method="post" action="process_new_icc_monitoring.php">

			<?php
				require("/include/header.php");
			require("/include/init_unset_values/icc_monitoring_init_value.php");

				$zero = 0;
				$one = 1;
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $iccID ? "Edit ICC# ".$icc_number : "New ICC Monitoring" ); ?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {
						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);
					}
				?>

				<table class="parent_tables_form">
					<colgroup>
						<col width="180px"></col><col width="350px"></col><col width="180px"></col><col width="350px"></col>
					</colgroup>

					<tr>
						<td>ICC No:</td>

						<td>
							<input type="text" name="txtICCNo" value="<?php echo ( $iccID ? $icc_number : $initICMICCNo );?>">
						</td>
					</tr>

					<tr>
						<td>ICC Date:</td>

						<td>
							<input type="text" name="txtICCDate" id="txtICCDate" value="<?php echo ( $iccID ? $icc_date : $initICMICCDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtICCDate')" style="cursor:pointer" name="picker" />
						</td>

						<td>Due Date:</td>

						<td>
							<input type="text" name="txtDueDate" id="txtDueDate" value="<?php echo ( $iccID ? $due_date : $initICMDueDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtDueDate')" style="cursor:pointer" name="picker" />
						</td>

						<td></td>
					</tr>

					<tr class="spacing">
						<td class="border_top border_left ">
							<input type="radio" name="radFromDivision" id="FromCompounds" value="Compounds" 
								onchange="showIssDepartment(0,0)"
								<?php echo( $iccID ? ( $issuing_division == "Compounds" ? "checked" : "" ) : ( $initICMFromDivision == "Compounds" ? "checked" : "" ) );?>>
								<label for="FromCompounds">Compounds</label>
						</td>

						<input type="radio" name="radFromDivision" id="FromPipes" value="Pipes" class="outcast">

						<td class="border_top border_right">
							<input type="radio" name="radFromDivision" id="FromCorporate" value="Corporate" 
								onchange="showIssDepartment(0,0)"
								<?php echo( $iccID ? ( $issuing_division == "Corporate" ? "checked" : "" ) : ( $initICMFromDivision == "Corporate" ? "checked" : "" ) );?>>
								<label for="FromCorporate">Corporate</label>
						</td>

						<td class="border_top border_left ">
							<input type="radio" name="radToDivision" id="ToCompounds" value="Compounds" 
								onchange="showRecDepartment(0,0)"
								<?php echo ( $iccID ? ( $receiving_division == "Compounds" ? "checked" : "" ) : ( $initICMToDivision == "Compounds" ? "checked" : "" ) );?>>
								<label for="ToCompounds">Compounds</label>
						</td>

						<input type="radio" name="radToDivision" id="ToPipes" class="outcast">

						<td class="border_top border_right">
							<input type="radio" name="radToDivision" id="ToCorporate" value="Corporate" 
								onchange="showRecDepartment(0,0)"
								<?php echo ( $iccID ? ( $receiving_division == "Corporate" ? "checked" : "" ) : ( $initICMToDivision == "Corporate" ? "checked" : "" ) );?>>
								<label for="ToCorporate">Corporate</label>
						</td>
					</tr>

					<tr>
						<td class="border_left border_bottom">Issuing Area:</td>

						<td class="border_right border_bottom">
							<select name="sltIssueDeptID" id="sltIssueDeptID">
								<?php
									if ( $iccID ){
										$qryID = mysqli_prepare($db, "CALL sp_Department_Dropdown_New(?, 1)");
										mysqli_stmt_bind_param($qryID, "s", $issuing_division);
										$qryID->execute();
										$resultID = mysqli_stmt_get_result($qryID);
										$processErrorID = mysqli_error($db);
										if ( !empty($processErrorID) ){
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_icc_monitoring.php"."</td><td>".$processErrorID." near line 61.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}else{
												echo "<option></option>";	
											while($row = mysqli_fetch_assoc($resultID)){
												if ( $issuing_area == $row["id"] ){
													echo "<option value='".$row["id"]."' selected>".$row["code"]."</option>";
												}else{
													echo "<option value='".$row["id"]."'>".$row["code"]."</option>";
												}
											}
										}
										$db->next_result();
										$resultID->close();
									}
								?>
							</select>
						</td>

						<td class="border_left border_bottom">Receiving Area:</td>

						<td class="border_right border_bottom">
							<select name="sltReceiveDeptID" id="sltReceiveDeptID">
								<?php
									if ( $iccID ){
										$qryRD = mysqli_prepare($db, "CALL sp_Department_Dropdown_New(?, 1)");
										mysqli_stmt_bind_param($qryRD, "s", $receiving_division);
										$qryRD->execute();
										$resultRD = mysqli_stmt_get_result($qryRD);
										$processErrorRD = mysqli_error($db);

										if ( !empty($processErrorRD) ){
											error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_icc_monitoring.php"."</td><td>".$processErrorRD." near line 61.</td></tr>", 3, "errors.php");
											header("location: error_message.html");
										}else{
											echo "<option></option>";	

											while($row = mysqli_fetch_assoc($resultRD)){

												if ( $receiving_area == $row["id"] ){
													echo "<option value='".$row["id"]."' selected>".$row["code"]."</option>";
												}
												else{
													echo "<option value='".$row["id"]."'>".$row["code"]."</option>";
												}
												
											}
										}
										$db->next_result();
										$resultRD->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr valign="top">
						<td> Nature of Complaint: </td>

						<td colspan="3">
							<textarea class="paragraph" name="txtNatureOfComplaint"><?php
								if ( $iccID ){
									echo $nature_of_complaint;
								}else{
									echo $initICMNatureOfComplaint;
								}
							?></textarea>
						</td>
					</tr>

					<tr>
						<td rowspan="2">
							Root Cause Analysis:
						</td>
					</tr>

					<tr class="spacing">
						<td colspan="4">
							<input type="checkbox" name="chkMan" id="Man"
								<?php echo ( $iccID ? ( $root_category_man ? "checked" : "" ) : ( $initICMMan ? "checked" : "" ) );?>> 
								<label for="Man">Man</label>

							<input type="checkbox" name="chkMethod" id="Method"
								<?php echo ( $iccID ? ( $root_category_method ? "checked" : "" ) : ( $initICMMethod ? "checked" : "" ) );?>> 
								<label for="Method">Method</label>

							<input type="checkbox" name="chkMachine" id="Machine"
								<?php echo ( $iccID ? ( $root_category_machine ? "checked" : "" ) : ( $initICMMachine ? "checked" : "" ) );?>>  
								<label for="Machine">Machine</label>

							<input type="checkbox" name="chkMaterial" id="Material"
								<?php echo ( $iccID ? ( $root_category_material ? "checked" : "" ) : ( $initICMMaterial ? "checked" : "" ) );?>>  
								<label for="Material">Material</label>

							<input type="checkbox" name="chkMeasurement" id="Measurement"
								<?php echo ( $iccID ? ( $root_category_measurement ? "checked" : "" ) : ( $initICMMeasurement ? "checked" : "" ) );?>> 
								<label for="Measurement">Measurement</label>

							<input type="checkbox" name="chkMotherNature" id="MotherNature"
								<?php echo ( $iccID ? ( $root_category_mother_nature ? "checked" : "" ) : ( $initICMMotherNature ? "checked" : "" ) );?>> 
								<label for="MotherNature">Mother Nature</label>
						</td>
					</tr>

					<tr>
						<td></td>
						<td colspan="3">
							<textarea class="paragraph" name="txtRootCause"><?php
								if ( $iccID ){
									echo $root_cause_analysis;
								}else{
									echo $initICMRootCause;
								}
							?></textarea>
						</td>
					</tr>

					<tr valign="top">
						<td rowspan="2"> Containment Action: </td>
						
						<td colspan="3">
							<textarea class="paragraph" name="txtContainmentAction"><?php
								if ( $iccID ){
									echo $containment_action;
								}else{
									echo $initICMContainmentAction;
								}
							?></textarea>
						</td>
					</tr>

					<tr class="spacing">
						<td colspan="2"> 
							Implementation Date: 
							<input type="text" name="txtContainmentImplement" value="<?php echo ( $iccID ? $containment_implementation : $initICMContainmentImplement );?>">
						</td>

						<td colspan="2">
							Person Responsible:
							<input type="text" name="txtContainmentResponsible" value="<?php echo ( $iccID ? $containment_responsible : $initICMContainmentResponsible );?>">
						</td>
					</tr>

					<tr valign="top">
						<td rowspan="2"> Corrective Action: </td>
						
						<td colspan="3">
							<textarea class="paragraph" name="txtCorrectiveAction"><?php
								if ( $iccID ){
									echo $corrective_action;
								}else{
									echo $initICMCorrectiveAction;
								}
							?></textarea>
						</td>
					</tr>

					<tr class="spacing">
						<td colspan="2"> 
							Implementation Date:
							<input type="text" name="txtCorrectiveImplementation" value="<?php echo ( $iccID ? $corrective_implementation : $initICMCorrectiveImplementation );?>">
						</td>

						<td colspan="2">
							Person Responsible:
							<input type="text" name="txtCorrectiveResponsible" value="<?php echo ( $iccID ? $corrective_responsible : $initICMCorrectiveResponsible );?>">
						</td>
					</tr>

					<tr class="spacing">
						<td>Issuance of Reminder:</td>

						<td>
							<input type="text" name="txtMemoDate" id="txtMemoDate" value="<?php echo ( $iccID ? $reminder_date : $initICMMemoDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtMemoDate')" style="cursor:pointer" name="picker" />
						</td>

						<td>Final Due Date:</td>

						<td>
							<input type="text" name="txtFinalDueDate" id="txtFinalDueDate" value="<?php echo ( $iccID ? $final_due_date : $initICMFinalDueDate );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtFinalDueDate')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr class="spacing">
						<td>Receipt Date (DCC):</td>

						<td>
							<input type="text" name="txtReceiveDateDCC" id="txtReceiveDateDCC" value="<?php echo ( $iccID ? $dcc_receipt_date : $initICMReceiveDateDCC );?>">
							<img src="js/cal.gif" onclick="javascript:NewCssCal('txtReceiveDateDCC')" style="cursor:pointer" name="picker" />
						</td>
					</tr>

					<tr class="spacing">
						<td>Initial Verification:</td> 

						<td>
							Month: 
							<select name="sltMonth">
								<?php
									$qryM = "SELECT * FROM months ORDER BY equal_int";
									$resultM = mysqli_query($db, $qryM);
									$processErrorM = mysqli_error($db);
									if ( !empty($processErrorM) ){
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_icc_monitoring.php"."</td><td>".$processErrorM." near line 275.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}else{
										echo "<option></option>";
										while( $row = mysqli_fetch_assoc($resultM) ){
											$month_id = $row["equal_int"];
											$month_name = $row["month"];


											if ( $iccID ){
												if( $initial_verification_month_id == $month_id ){
													echo "<option value='".$month_id."' selected>".$month_name."</option>";
												}else{
													echo "<option value='".$month_id."'>".$month_name."</option>";
												}		
											}else{
												if( $initICMMonth == $month_id ){
													echo "<option value='".$month_id."' selected>".$month_name."</option>";
												}else{
													echo "<option value='".$month_id."'>".$month_name."</option>";
												}		
											}
											

										}
										$db->next_result();
										$resultM->close();
									}
								?>
							</select>

							Year:
							<select name="sltYear">
								<?php
									$qryM = "SELECT * FROM years";
									$resultM = mysqli_query($db, $qryM);
									$processErrorM = mysqli_error($db);
									if ( !empty($processErrorM) ){
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_icc_monitoring.php"."</td><td>".$processErrorM." near line 275.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}else{
										echo "<option></option>";
										while( $row = mysqli_fetch_assoc($resultM) ){
											$year = $row["year"];

											if ( $iccID ){
												if( $initial_verification_year == $year ){
													echo "<option value='".$year."' selected>".$year."</option>";
												}else{
													echo "<option value='".$year."'>".$year."</option>";
												}		
											}else{
												if( $initICMYear == $year ){
													echo "<option value='".$year."' selected>".$year."</option>";
												}else{
													echo "<option value='".$year."'>".$year."</option>";
												}	
											}

										}
										$db->next_result();
										$resultM->close();
									}
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td> Remarks: </td>
						<td colspan="3">
							<textarea class="paragraph" name="txtRemarks"><?php
								if ( $iccID ){
									echo $remarks;
								}else{
									echo $initICMRemarks;
								}
							?></textarea>
						</td>
					</tr>
					<tr class="align_bottom">
						<td >
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='icc_monitoring.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidICCID" value="<?php echo $iccID;?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $iccID ? $created_at : date("Y-m-d H:i:s") );?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $iccID ? $created_id : $_SESSION["SESS_USER_ID"] );?>">
						</td>
					</tr>
					
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>