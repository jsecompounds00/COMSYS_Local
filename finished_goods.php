<html>
	<head>
		<title>Finished Goods - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET['search'] ? "%".$_GET['search']."%" : "");
			$qsone = "";
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
	</head>
	<body>
		<?php
			require('/include/header.php');
			require("/include/init_unset_values/fg_master_batch_unset_value.php");
			require("/include/init_unset_values/fg_specs_unset_value.php");

			if( $_SESSION['fg'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION['page'] = $_GET['page'];
			$_SESSION['search'] = $_GET['search'];
			$_SESSION['qsone'] = $_GET['qsone'];
		?>

		<div class="wrapper">
			<span> <h3> Finished Goods </h3> </span>

			<div class="search_box">
			 	 <form method='get' action='finished_goods.php'>
			 		<input type='hidden' name='page' value="<?php echo $page;?>">
			 		<input type="hidden" name="qsone" value="<?php echo $qsone;?>">
			 		<table class="search_tables_form">
			 			<tr>
			 				<td> Name: </td>
			 				<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
			 				<td> <input type='submit' value='Search'> </td>
			 			</tr>
			 		</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>finished_goods.php'.'</td><td>'.$error.' near line 36.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryR = mysqli_prepare($db, "CALL sp_FinishedGoods_Home(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryR, 's', $search);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR); //return results of query

					$total_results = mysqli_num_rows($resultR); //return number of rows of result

					$db->next_result();
					$resultR->close();

					$targetpage = "finished_goods.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare($db, "CALL sp_FinishedGoods_Home(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'sii', $search, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>finished_goods.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td  colspan='8'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>Name</th>
							    <th>Uom</th>
							    <th>Customer</th>
							    <th>Active</th>
							    <th colspan="4">
							    </th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)) {
							?>
									<tr>
										<td> <?php echo $row['FG'];?> </td>
										<td> <?php echo $row['UOM'];?> </td>
										<td> <?php echo $row['Customer'];?> </td>
										<td> <?php echo ( $row['active'] ? 'Y' : 'N' );?> </td>
										<td> 
											<?php
												if(array_search(137, $session_Permit)){
											?>
													<input type='button' value='Master Batch' onclick="location.href='new_finished_goods.php?id=<?php echo $row['FGId'];?>'">
											<?php
													$_SESSION['master_batch'] = true;
												}else{
													unset($_SESSION['master_batch']);
												}
											?>
										</td>
										<td> 
											<?php
												if(array_search(138, $session_Permit)){
											?>
													<input type='button' value='Update COQ' onclick="location.href='new_coq.php?id=<?php echo $row['FGId'];?>'">
											<?php
													$_SESSION['update_coq'] = true;
												}else{
													unset($_SESSION['update_coq']);
												}
											?>
										</td>
										<td> 
											<?php
												if(array_search(139, $session_Permit)){
											?>
													<input type='button' value='Add Specs' onclick="location.href='new_fg_standard_specs.php?id=<?php echo $row['FGId'];?>'">
											<?php
													$_SESSION['add_specs'] = true;
												}else{
													unset($_SESSION['add_specs']);
												}
											?>
										</td>
										<td> 
											<?php
												if(array_search(138, $session_Permit)){
											?>
													<input type='button' value='Add Name for Label' onclick="location.href='new_fg_label.php?id=<?php echo $row['FGId'];?>'">
											<?php
													$_SESSION['label_name'] = true;
												}else{
													unset($_SESSION['label_name']);
												}
											?>
										</td>

									</tr>
							<?php
								}

								$db->next_result();
								$result->close();
							?>
							<tr>
								<td class='borderless' colspan='8' height='50' valign='bottom'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php
					}
				}
			?>

		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>
