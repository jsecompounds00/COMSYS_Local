<?php

$qry = mysqli_prepare($db, "CALL sp_User_Query(?)");
mysqli_stmt_bind_param($qry, 'i', $sltAttentionTo);
$qry->execute();
$result = mysqli_stmt_get_result($qry);
$processError = mysqli_error($db);

if(!empty($processError))
{
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_send_email.php'.'</td><td>'.$processError.' near line 16.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}
else
{
	while($row = mysqli_fetch_assoc($result))
	{
		// $userId = $row['user_id'];
		$fname = htmlspecialchars($row['fname']);
		$lname = htmlspecialchars($row['lname']);
		$email = htmlspecialchars($row['email']);
		$dept1 = $row['department_id_1'];
		$dept2 = $row['department_id_2'];
	}
}
$db->next_result();
$result->close();

$qryU = mysqli_prepare($db, "CALL sp_User_Query(?)");
mysqli_stmt_bind_param($qryU, 'i', $createdId);
$qryU->execute();
$resultU = mysqli_stmt_get_result($qryU);
$processErrorU = mysqli_error($db);

if(!empty($processErrorU))
{
	error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_send_email.php'.'</td><td>'.$processErrorU.' near line 16.</td></tr>', 3, "errors.php");
	header("location: error_message.html");
}
else
{
	while($row = mysqli_fetch_assoc($resultU))
	{
		// $userId = $row['user_id'];
		$sender_fname = htmlspecialchars($row['fname']);
		$sender_lname = htmlspecialchars($row['lname']);
		$sender_email = htmlspecialchars($row['email']);
		$dept1 = $row['department_id_1'];
		$dept2 = $row['department_id_2'];
	}
}
$db->next_result();
$resultU->close();


require 'phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = "smtp.gmail.com";					// Specify main and backup SMTP servers
$mail->SMTPAuth = true; 							// Authentication must be disabled
$mail->Username = 'caccbackup@gmail.com';
$mail->Password = 'CACCBackup1234'; 								// Leave this blank
$mail->Port = 465;                                    // TCP port to connect to
$mail->SMTPSecure = 'ssl';
$mail->SMTPDebug = 0;


$mail->setFrom('caccbackup@gmail.com', 'COMSYS Server');
$mail->addAddress($email, ($fname." ".$lname));     // Add a recipient
// $mail->addAddress('caccbackup@gmail.com');               // Name is optional
// $mail->addReplyTo('caccbackup@gmail.com', 'Information');
// $mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = $txtSubject;
$mail->Body    = "<b>Message from ".$sender_fname." ".$sender_lname.":</b><br><br>".$txtRequest;
// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';




// require("include/database_close.php");


?>