<html>
	<head>
		<title>Equipment - Home</title>
		<?php
			require ("include/database_connect.php");

			$name_text = ($_GET['name_text'] ? "%".$_GET['name_text']."%" : "");
			$code_text = ($_GET['code_text'] ? "%".$_GET['code_text']."%" : "");
			$type_text = ($_GET['type_text'] ? $_GET['type_text'] : "");
			$page = ($_GET['page'] ? $_GET['page'] : 1);
		?>
	</head>
	<body>

		<?php 
			require ("/include/header.php");
			require ("/include/unset_value.php");

			if( $_SESSION["equipment"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["name_text"] = $_GET["name_text"];
			$_SESSION["code_text"] = $_GET["code_text"];
			$_SESSION["type_text"] = $_GET["type_text"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Equipment </h3> </span>

			<div class="search_box">
			 	<form method='get' action='equipment.php'>
					<input type='hidden' name='page' value='<?php echo $page; ?>'>
					<table class="search_tables_form">
						<tr>
							<td> Name: </td>
							<td> <input type='text' name='name_text' value='<?php echo $_GET['name_text'];?>'> </td>
							<td> Code: </td>
							<td> <input type='text' name='code_text' value='<?php echo $_GET['code_text'];?>'> </td>
							<td>
								<select name='type_text'>
									<option value=0>ALL</option>
									<option value=1 <?php echo ( $type_text == 1 ? "selected" : "" )?>>Validation</option>
									<option value=2 <?php echo ( $type_text == 2 ? "selected" : "" )?>>Calibration</option>
								</select>						
							</td>
							<td> <input type='submit' value='Search'> </td>
							<td> <input type='button' name='btnAddEquipment' value='Add Equipment' onclick="location.href='<?php echo PG_NEW_EQUIPMENT;?>0'"> </td>
						</tr>
					</table>
				</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>equipment.php'.'</td><td>'.$error.' near line 53.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryD = mysqli_prepare($db, "CALL sp_Equipment_Home(?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryD, 'ssi', $name_text, $code_text, $type_text);
					$qryD->execute();
					$resultD = mysqli_stmt_get_result($qryD); //return results of query
					$total_results = mysqli_num_rows($resultD); //return number of rows of result

					$db->next_result();
					$resultD->close();
					
					$targetpage = "equipment.php"; 	//your file name  (the name of this file)
					require("include/paginate_raw_material.php");

					$qry = mysqli_prepare($db, "CALL sp_Equipment_Home(?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'ssiii', $name_text, $code_text, $type_text, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>equipment.php'.'</td><td>'.$processError.' near line 77.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{		
						if( isset($_SESSION['SUCCESS'])) {
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan='8'>
									<?php echo $pagination;?>
								</td>
							</tr>
							<tr>
							    <th>Equipment</th>
							    <th>Code</th>
							    <th>Status</th>
							    <th>Location</th>
							    <th>Active</th>
							    <th>Last Calibration Date</th>
							    <th>Last Validation Date</th>
							    <th></th>
							</tr>
							<?php 
								while($row = mysqli_fetch_assoc($result)){
									$freqv = ($row['validation'] ? 1 : 0);
									$freqc = ($row['calibration'] ? 1 : 0);
							?>
									<tr>
									    <td> <?php echo $row['equipment'];?> </td>
									    <td> <?php echo $row['code'];?> </td>
									    <td> <?php echo $row['status'];?> </td>
									    <td> <?php echo $row['location'];?> </td>
									    <td> <?php echo ($row['active']?'Y':'N');?> </td>
									    <td> <?php echo $row['last_calibration_date'];?> </td>
									    <td> <?php echo $row['last_validation_date'];?> </td>
										<td>
											<input type='button' name='btnEdit' value='EDIT' onclick="location.href='<?php echo PG_NEW_EQUIPMENT.$row['e_id'];?>'">
											<input type='button' name='btnAddSched' value='Add Schedule' onclick="location.href='new_schedule.php?id=<?php echo $row['e_id'];?>&v=<?php echo $freqv;?>&c=<?php echo $freqc;?>'">
											<input type='button' name='btnHistory' value='History' onclick="location.href='schedule_history.php?id=<?php echo $row['e_id'];?>&page=1&search=&qsone='">
										</td>
									</tr> 
							<?php
									}

									$db->next_result();
									$result->close();
							?>
							<tr>
								<td colspan='8'>
									<?php echo $pagination;?>
								</td>
							</tr>
						</table>
			<?php 
					}
				} 
			?>
			
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>