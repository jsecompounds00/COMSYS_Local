<?php
	//Start session
	session_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	//Array to store validation errors
	$errmsg_arr = array();
 
	//Validation error flag
	$errflag = false;
 
	//Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_property_type.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		//Sanitize the POST values
		$hidSTId = $_POST['hidPropertyTypeId'];
		$txtNewPropertyType = $_POST['txtNewPropertyType'];
		//Input Validations

		if(isset($_POST['chkCmpds']))
			$chkCmpds = 1;
		else
			$chkCmpds = 0;
		if(isset($_POST['chkPips']))
			$chkPips = 1;
		else
			$chkPips = 0;
		if(isset($_POST['chkCorp']))
			$chkCorp = 1;
		else
			$chkCorp = 0;
		if(isset($_POST['chkActive']))
			$chkActive = 1;
		else
			$chkActive = 0;
		if(isset($_POST['chkPPR']))
			$chkPPR = 1;
		else
			$chkPPR = 0;

		$txtComments = $_POST['txtComments'];
		
		// $txtComments = str_replace('\\r\\n', '<br>', $txtComments);
		
		// $txtComments = str_replace('\\', '', $txtComments);

		// $_SESSION['txtNewPropertyType'] = $txtNewPropertyType;
		$_SESSION['chkCmpds'] = $chkCmpds;
		$_SESSION['chkPips'] = $chkPips;
		$_SESSION['chkCorp'] = $chkCorp;
		$_SESSION['chkPPR'] = $chkPPR;
		$_SESSION['chkActive'] = $chkActive;
		$_SESSION['txtComments'] = htmlspecialchars($txtComments);

		if ( empty($txtNewPropertyType) ){
			$errmsg_arr[] = '* Property type is missing.';
			$errflag = true;
		}
		if ( is_numeric($txtNewPropertyType) ){
			$errmsg_arr[] = '*Invalid Property type.';
			$errflag = true;
		}

		//If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_property_type.php?id=".$hidSTId."");
			exit();
		}

		if($hidSTId == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidSTId == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

		$qry = mysqli_prepare($db, "CALL sp_PropertyType_CRU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		mysqli_stmt_bind_param($qry, 'isiiiisssiii', $hidSTId, $txtNewPropertyType, $chkCmpds
												  , $chkPips, $chkCorp, $chkActive, $txtComments
												  , $createdAt, $updatedAt, $createdId, $updatedId, $chkPPR);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); //return results of query
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_property_type.php'.'</td><td>'.$processError.' near line 98.</td></tr>', 3, "errors.php");
			// header("location: error_message.html");
		}
		else
		{ 
			if($hidSTId)
				$_SESSION['SUCCESS']  = 'Successfully updated asset type.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new asset type.';
			//echo $_SESSION['SUCCESS'];
			header("location: property_type.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close();
		require("include/database_close.php");
				
	}

?>