<?php
	##########Start session
	session_start();
	ob_start();

	require ("include/database_connect.php");
	require ("include/constant.php");

	##########Array to store validation errors
	$errmsg_arr = array();
 
	##########Validation error flag
	$errflag = false;
 
	##########Function to sanitize values received from the form. Prevents SQL injection
	// function clean($str) {
	// 	$str = @trim($str);
	// 	if(get_magic_quotes_gpc()) {
	// 		$str = stripslashes($str);
	// 	}
	// 	return mysql_real_escape_string($str);
	// }
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_quote.php'.'</td><td>'.$error.' near line 30.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
		$hidQuoteID = $_POST['hidQuoteID'];
		$sltYear = $_POST['sltYear'];
		$sltMonth = $_POST['sltMonth'];
		$sltCustomer = $_POST['sltCustomer'];

		$quote_date = $sltYear.'-'.$sltMonth.'-01';

		if($hidQuoteID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
			$updatedAt = date('Y-m-d H:i:s');
		if($hidQuoteID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
			$updatedId = $_SESSION['SESS_USER_ID'];

		
		// foreach ($_POST['hidQuoteItemID'] as $QuoteItemID) {
		// 	$QuoteItemID = array();
		// 	$QuoteItemID = $_POST['hidQuoteItemID'];
		// }
		// foreach ($_POST['hidFGID'] as $FGID) {
		// 	$FGID = array();
		// 	$FGID = $_POST['hidFGID'];
		// }
		// foreach ($_POST['hidFGItem'] as $FGItem) {
		// 	$FGItem = array();
		// 	$FGItem = $_POST['hidFGItem'];
		// }
		// foreach ($_POST['txtQuoteDiff'] as $QuoteDiff) {
		// 	$QuoteDiff = array();
		// 	$QuoteDiff = $_POST['txtQuoteDiff'];
		// }
		// foreach ($_POST['hidCurrMonthQuote'] as $CurrMonthQuote) {
		// 	$CurrMonthQuote = array();
		// 	$CurrMonthQuote = $_POST['hidCurrMonthQuote'];
		// }
		// foreach ($_POST['txtQuoteDiffRevised'] as $QuoteDiffRevised) {
		// 	$QuoteDiffRevised = array();
		// 	$QuoteDiffRevised = $_POST['txtQuoteDiffRevised'];
		// }
		// foreach ($_POST['hidCurrMonthQuoteRevised'] as $CurrMonthQuoteRevised) {
		// 	$CurrMonthQuoteRevised = array();
		// 	$CurrMonthQuoteRevised = $_POST['hidCurrMonthQuoteRevised'];
		// }
		// foreach ($_POST['hidCreatedItemAt'] as $CreatedItemAt) {
		// 	$CreatedItemAt = array();
		// 	$CreatedItemAt = $_POST['hidCreatedItemAt'];
		// }
		// foreach ($_POST['hidCreatedItemId'] as $CreatedItemId) {
		// 	$CreatedItemId = array();
		// 	$CreatedItemId = $_POST['hidCreatedItemId'];
		// }

		##############---------- INPUT VALIDATION
		if ( !$sltYear ){
			$errmsg_arr[] = '* Select year.';
			$errflag = true;
		}
		if ( !$sltMonth ){
			$errmsg_arr[] = '* Select month.';
			$errflag = true;
		}
		if ( !$sltCustomer ){
			$errmsg_arr[] = '* Select customer.';
			$errflag = true;
		}



		##############---------- INPUT VALIDATION
			$txtQuoteDiff = array();
			$hidQuoteItemID = array();
			$hidFGID = array();
			$hidFGItem = array();
			$hidCurrMonthQuote = array();
			$txtQuoteDiffRevised = array();
			$hidCurrMonthQuoteRevised = array();
			$hidCreatedItemAt = array();
			$hidCreatedItemId = array();
			$hidUpdatedItemAt = array();
			$hidUpdatedItemId = array();

		foreach ($_POST["txtQuoteDiff"] as $key => $value) {
			$txtQuoteDiff[] = $_POST["txtQuoteDiff"][$key];
			$hidQuoteItemID[] = $_POST["hidQuoteItemID"][$key];
			$hidFGID[] = $_POST["hidFGID"][$key];
			$hidFGItem[] = $_POST["hidFGItem"][$key];
			$hidCurrMonthQuote[] = $_POST["hidCurrMonthQuote"][$key];
			$txtQuoteDiffRevised[] = $_POST["txtQuoteDiffRevised"][$key];
			$hidCurrMonthQuoteRevised[] = $_POST["hidCurrMonthQuoteRevised"][$key];
			// $hidCreatedItemAt[] = $_POST["hidCreatedItemAt"][$key];
			// $hidCreatedItemId[] = $_POST["hidCreatedItemId"][$key];
		}

		$i = 0;
		$count = count($hidQuoteItemID);

		foreach ( $txtQuoteDiff as $key => $quote_diff ){
			// $txtQuoteDiff[$key];
			// $hidQuoteItemID[$key];
			// $hidFGID[$key];
			// $hidFGItem[$key];
			// $hidCurrMonthQuote[$key];
			// $txtQuoteDiffRevised[$key];
			// $hidCurrMonthQuoteRevised[$key];
			// $hidCreatedItemAt[$key];
			// $hidCreatedItemId[$key];

			if($hidQuoteItemID[$key] == 0)
				$hidCreatedItemAt[] = date('Y-m-d H:i:s');
			else $hidCreatedItemAt[] = $_POST['hidCreatedItemAt'][$key];
				$hidUpdatedItemAt[] = date('Y-m-d H:i:s');
			if($hidQuoteItemID[$key] == 0)
				$hidCreatedItemId[] = $_SESSION['SESS_USER_ID'];
			else $hidCreatedItemId[] = $_POST['hidCreatedItemId'][$key];
				$hidUpdatedItemId[] = $_SESSION['SESS_USER_ID'];

			if ( $txtQuoteDiff[$key] != "" && !is_numeric($txtQuoteDiff[$key]) ){
				$errmsg_arr[] = "* Invalid quote difference for ".$hidFGItem[$key].".";
				$errflag = true;
			}

			if ( $txtQuoteDiffRevised[$key] != "" && !is_numeric($txtQuoteDiffRevised[$key]) ){
				$errmsg_arr[] = "* Invalid revised quote difference for ".$hidFGItem[$key].".";
				$errflag = true;
			}elseif ($txtQuoteDiffRevised[$key] == ""){
				$txtQuoteDiffRevised[$key] = NULL;
				$hidCurrMonthQuoteRevised[$key] = NULL;
			}

			if ( $txtQuoteDiffRevised[$key] != "" && $txtQuoteDiff[$key] == "" ){
				$errmsg_arr[] = "* Accomplish quote difference (1st discount) first.";
				$errflag = true;
			}
		}

		##############---------- INPUT VALIDATION		

		// $counter2 = 10;

		// $_SESSION['sltInventorySource'] = $sltInventorySource;
		// $_SESSION['txtReferenceNo'] = $txtReferenceNo;
		// $_SESSION['txtReferenceDate'] = $txtReferenceDate;
		// // $_SESSION['sltAssetType'] = $sltAssetType;
		// $_SESSION['sltReceivedType'] = $sltReceivedType;
		// $_SESSION['sltReceivedFrom'] = $sltReceivedFrom;
		// // $_SESSION['txtPurpose'] = $txtPurpose;

		// for ( $i = 0; $i < $counter2; $i++ ) {
		// 	$_SESSION['AssetType'][$i] = $AssetType[$i];
		// 	$_SESSION['sltAsset'][$i] = $Asset[$i];
		// 	$_SESSION['sltAssetSpecification'][$i] = $AssetSpecification[$i];
		// 	$_SESSION['txtQuantityReceived'][$i] = $QuantityReceived[$i];
		// 	$_SESSION['txtUnitPrice'][$i] = $UnitPrice[$i];
		// 	$_SESSION['txtRemarks'][$i] = $Remarks[$i];
		// 	$_SESSION['txtRefPTSNumber'][$i] = $RefPTSNumber[$i];
		// }

		##########If there are input validations, redirect back to the login form
		if($errflag) 
		{
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location:new_sales_quote.php?id=".$hidQuoteID);
			exit();
		} 

		############# Committing in Database
			$qry = mysqli_prepare($db, "CALL sp_Quotes_CRU(?, ?, ?, ?, ?, ?, ?)");
			mysqli_stmt_bind_param($qry, 'isissii', $hidQuoteID, $quote_date, $sltCustomer
												  , $createdAt, $updatedAt, $createdId, $updatedId);
			$qry->execute();
			$result = mysqli_stmt_get_result($qry); //return results of query
			$processError = mysqli_error($db);

			if ( !empty($processError) ){
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_quote.php'.'</td><td>'.$processError.' near line 280.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				$qryLastId = mysqli_query($db, "SELECT LAST_INSERT_ID()");

				while($row = mysqli_fetch_assoc($qryLastId))
				{
					if ( $hidQuoteID ) {
						$QuoteID  = $hidQuoteID;
					}else{
						$QuoteID  = $row['LAST_INSERT_ID()'];
					}	
				}
				
				if(mysqli_affected_rows($db) > 0 ) 
				{
					foreach($_POST['txtQuoteDiff'] as $key => $itemValue)
					{	
						if (!$hidQuoteID) {
							$QuoteItemID = 0;
						}

						if ($itemValue)
						{
							$qryItems = mysqli_prepare($db, "CALL sp_Quotes_Items_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
							mysqli_stmt_bind_param($qryItems, 'iiiddddssii', $hidQuoteItemID[$key], $QuoteID , $hidFGID[$key]
																		   , $txtQuoteDiff[$key], $hidCurrMonthQuote[$key]
																		   , $txtQuoteDiffRevised[$key], $hidCurrMonthQuoteRevised[$key]
																		   , $hidCreatedItemAt[$key], $hidUpdatedItemAt[$key]
																		   , $hidCreatedItemId[$key], $hidUpdatedItemId[$key]);
							$qryItems->execute();
							$result = mysqli_stmt_get_result($qryItems); //return results of query
							$processError1 = mysqli_error($db);

							if ( !empty($processError1) ){
								error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_sales_quote.php'.'</td><td>'.$processError1.' near line 314.</td></tr>', 3, "errors.php");
								header("location: error_message.html");
							}else{
								if ( $hidQuoteID )
									$_SESSION['SUCCESS']  = 'Sales quote successfully updated.';
								else
									$_SESSION['SUCCESS']  = 'Sales quote successfully added.';
								header("location: sales_quote.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
							}				
						}
					}
				}
			}	
		############# Committing in Database		

		require("include/database_close.php"); 
	}
?>