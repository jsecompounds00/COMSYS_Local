<html>
	<head>
		<title>Asset Type - Home</title>
		<?php
			require("include/database_connect.php");

			$search = ($_GET["search"] ? "%".$_GET["search"]."%" : "");
			$qsone = ($_GET["qsone"] ? $_GET["qsone"] : NULL);
			$page = ($_GET["page"] ? $_GET["page"] : 1);
		?>
	</head>
	<body>

		<?php
			require ("/include/header.php");
			require("/include/unset_value.php");

			if( $_SESSION['property_type'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["search"] = $_GET["search"];
			$_SESSION["qsone"] = $_GET["qsone"];
			$_SESSION["page"] = $_GET["page"];
		?>

		<div class="wrapper">

			<span> <h3> Asset Type </h3> </span>

			<div class="search_box">
	 			<form method="get" action="property_type.php">
	 				<input type="hidden" name="page" value="<?php echo $page;?>">
	 				<input type="hidden" name="qsone" value="<?php echo $qsone;?>">
	 				<table class="search_tables_form">
	 					<tr>
	 						<td> Name: </td>
	 						<td> <input type="text" name="search" value="<?php echo htmlspecialchars($_GET["search"]);?>"> </td>
	 						<td> <input type="submit" value="Search"> </td>
	 						<td>
	 							<?php
									if(array_search(90, $session_Permit)){
								?>
										<input type="button" name="btnAddPropertyType" value="Add Asset Type" onclick="location.href='new_property_type.php?id=0'">
								<?php
										$_SESSION['add_propertytype'] = true;
									}else{
										unset($_SESSION['add_propertytype']);
									}
								?>
	 						</td>
	 					</tr>
	 				</table>
	 			</form>
	 		</div>

	 		<?php
 				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>property_type.php'.'</td><td>'.$error.' near line 43.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryST = mysqli_prepare( $db, "CALL sp_PropertyType_Home(?, NULL, NULL)" );
					mysqli_stmt_bind_param( $qryST, 's', $search );
					$qryST->execute();
					$resultST = mysqli_stmt_get_result( $qryST );
					$total_results = mysqli_num_rows($resultST); //return number of rows of result

					$db->next_result();
					$resultST->close();

					$targetpage = "property_type.php"; 	//your file name  (the name of this file)
					require("include/paginate.php");

					$qry = mysqli_prepare( $db, "CALL sp_PropertyType_Home(?, ?, ?)" );
					mysqli_stmt_bind_param( $qry, 'sii', $search, $start, $end );
					$qry->execute();
					$result = mysqli_stmt_get_result( $qry );
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>property_type.php'.'</td><td>'.$processError.' near line 64.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
					<table class="home_pages">
						<tr>
							<td colspan='7'> <?php echo $pagination; ?> </td>
						</tr>

					 	<tr>
						 	<th> Name </th>
							<th> Compounds </th>
							<th> Pipes </th>
							<th> Corporate </th>
							<th> PPR </th>
							<th> Active ?</th>
							<th> </th>
						</tr>
						<?php						
							while($row = mysqli_fetch_assoc($result))
							{
								$propertyTypeId = $row['id'];
								$compounds = ( $row["compounds"] ? "Y" : "N" );
								$pipes = ( $row["pipes"] ? "Y" : "N" );
								$corporate = ( $row["corporate"] ? "Y" : "N" );
								$active = ( $row["active"] ? "Y" : "N" );
								$ppr = ( $row["ppr"] ? "Y" : "N" );
						?>
								<tr>
									<td> <?php echo $row['property_type'];?></td>
									<td> <?php echo $compounds;?></td>
									<td> <?php echo $pipes;?></td>
									<td> <?php echo $corporate;?></td>
									<td> <?php echo $ppr;?></td>
									<td> <?php echo $active;?></td>
									<td>
										<?php
											if(array_search(91, $session_Permit)){
										?>
												<input type='button' name='btnEdit' value='EDIT' onclick="location.href='new_property_type.php?id=<?php echo $propertyTypeId;?>'">
										<?php
												$_SESSION['property_type_edit'] = true;
											}else{
												unset($_SESSION['property_type_edit']);
											}
										?>
									</td>
								</tr>
						<?php
							} 
							$db->next_result();
							$result->close();
						?>
						<tr>
							<td colspan='7'> <?php echo $pagination;?> </td>
						</tr>
					</table>
			<?php
				}
			?>
			
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>