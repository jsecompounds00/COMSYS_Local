<html>
	<head>
		<title>Raw Materials - Home</title>
		<?php
			require("include/database_connect.php");

			$name_text = ($_GET["name_text"] ? "%".$_GET["name_text"]."%" : "");
			$code_text = ($_GET["code_text"] ? "%".$_GET["code_text"]."%" : "");
			$type_text = ($_GET["type_text"] ? $_GET["type_text"] : "");
			$page = ($_GET["page"] ? $_GET["page"] : 1);

		?>
		<script src="js/raw_material_js.js"></script>  
		<link rel="stylesheet" type="text/css" href="dist/sweetalert.css"> <!--   -->
	</head>
	<body>
		<?php
			require("/include/header.php");
			require("/include/init_unset_values/raw_materials_unset_value.php");

			if( $_SESSION["rm"] == false) 
			{
				$_SESSION["ERRMSG_ARR"] ="Access denied!";
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			$_SESSION["name_text"] = $_GET["name_text"];
			$_SESSION["code_text"] = $_GET["code_text"];
			$_SESSION["type_text"] = $_GET["type_text"];
			$_SESSION["page"] = $_GET["page"];
		?>
		<div class="wrapper">
			<span> <h3> Raw Materials </h3> </span>

			<div class="search_box">
			 	<form method="get" action="raw_materials.php">
			 		<input type="hidden" name="page" value="<?php echo $page; ?>">
			 		<table class="search_tables_form">
			 			<tr>
			 				<td> Name: </td>
			 				<td> <input type="text" name="name_text" value="<?php echo htmlspecialchars($_GET["name_text"]); ?>"> </td>
			 				<td> Code: </td>
			 				<td> <input type="text" name="code_text" value="<?php echo htmlspecialchars($_GET["code_text"]); ?>"> </td>
			 				<td> Type: </td>
			 				<td>
			 					<select name="type_text">
							 		<?php
							 			$type_text = htmlspecialchars($_GET["type_text"]) ;

							 			$qryRMT = "CALL sp_RMType_Dropdown(1)"; 
							 			$resultRMT = mysqli_query($db, $qryRMT);
							 			$processError1 = mysqli_error($db);

							 			if(!empty($processError1))
							 			{
							 				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>raw_materials.php"."</td><td>".$processError1." near line 43.</td></tr>", 3, "errors.php");
							 				header("location: error_message.html");
							 			}
							 			else
							 			{
							 				while($row = mysqli_fetch_assoc($resultRMT))
							 				{
							 					$rmTypeID = htmlspecialchars($row["id"]);
							 					$rmType = htmlspecialchars($row["code"]);
							 		?>
							 					<option value="<?php echo $rmTypeID; ?>" <?php echo ( $rmTypeID ? ( $rmType == $type_text ? "selected" : "" ) : "" ); ?> > <?php echo $rmType; ?> </option>
									<?php		
							 				}

							 				$db->next_result();
							 				$resultRMT->close();
							 			}
							 		?>
						 		</select>
			 				</td>
			 				<td> <input type="submit" value="Search"> </td>
			 				<td>
			 					<?php
						 			if(array_search(172, $session_Permit)){
						 		?>
						 				<input type="button" name="btnAdd" value="Add Raw Material" onclick="location.href='new_raw_materials.php?id=0&comp=0'">
						 		<?php
										$_SESSION["add_rm"] = true;
						 			}else{
						 				unset($_SESSION["add_rm"]);
						 			}
						 		?>
			 				</td>
			 			</tr>
			 		</table>
			 	</form>
			</div>

			<?php
				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>raw_materials.php"."</td><td>".$error." near line 42.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$qryR = mysqli_prepare($db, "CALL sp_RawMaterials_Home(?, ?, ?, ?, ?, ?, ?, ?, NULL, NULL)");
					mysqli_stmt_bind_param($qryR, "sssiiiis", $name_text, $code_text, $type_text, $compounds, $pipes, $corporate, $pp, $_SESSION["SESS_ROLE_ID"]);
					$qryR->execute();
					$resultR = mysqli_stmt_get_result($qryR);

					$total_results = mysqli_num_rows($resultR); 
					
					$db->next_result();
					$resultR->close();

					$targetpage = "raw_materials.php"; 	
					require("include/paginate_raw_material.php");

					$qry = mysqli_prepare($db, "CALL sp_RawMaterials_Home(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					mysqli_stmt_bind_param($qry, "sssiiiisii", $name_text, $code_text, $type_text, $compounds, $pipes, $corporate, $pp, $_SESSION["SESS_ROLE_ID"], $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>raw_materials.php"."</td><td>".$processError." near line 63.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION["SUCCESS"])) 
						{
							echo "<ul id='success'>";
							echo "<li>".$_SESSION["SUCCESS"]."</li>"; 
							echo "</ul>";
							unset($_SESSION["SUCCESS"]);
						}
			?>
						<table class="home_pages">
							<tr>
								<td colspan="9"> <?php echo $pagination;?> </td>
							</tr>
							<tr>
							    <th>Type</th>
							    <th>Code</th>
							    <th>Name</th>
							    <th>Active</th>
							    <th>KG / Packaging</th>
							    <th>Critical Level</th>
							    <th>Stock Level</th>
							    <th>MSDS</th>
							    <th></th>
							</tr>
							<?php
								$i = 0; 
								while($row = mysqli_fetch_assoc($result)) {
							?>
									<tr>
										<td> <?php echo $row["RMType"]; ?></td>
										<td> <?php echo $row["RMCode"]; ?></td>
										<td> <?php echo $row["RMName"]; ?></td>
										<td> <?php echo ( $row["Active"] ? "Y" : "N" ); ?></td>
										<td> <?php echo ( $row["Packaging"] ? $row["Packaging"] : "" ); ?></td>
										<td> <?php echo ( $row["CriticalLevel"] ? $row["CriticalLevel"] : "" ); ?></td>
										<td> <?php echo ( $row["StockLevel"] ? $row["StockLevel"] : "" ); ?></td>
										<td> 
											<?php
												if ( $row["msds"] ){
													echo "<img src='images/yes.png' height='20'>";
												}else{	
													if(array_search(186, $session_Permit)){ 
											?>
														<input type="checkbox" 
															name="chkMSDS[<?php echo $i;?>]" 
															id="chkMSDS<?php echo $i;?>" 
															onChange="updateRMMSDS('<?php echo $i;?>','<?php echo $row['RMID'];?>')"> 
											<?php
													}
												}
											?>
										</td>
										<td> 
										<?php
											if(array_search(173, $session_Permit)){ 
										?>
												<input type="button" name="btnEdit" value="Edit" 
													onclick="location.href='new_raw_materials.php?id=<?php echo $row['RMID']."&comp=".$row['compounds']; ?>'"> 
										<?php
												$_SESSION["edit_rm"] = true;
											}else{
												unset($_SESSION["edit_rm"]);
											}
										?>
										</td>
									</tr>
							<?php
									$i++;
								}
				 				$db->next_result();
				 				$result->close();
							?>
							<tr>
								<td colspan="9"> <?php echo $pagination;?> </td>
							</tr>
						</table>	
			<?php
					}
				}
			?>

		</div>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>