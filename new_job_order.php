<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_job_order.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$JobOrderID = $_GET['id'];

				if($JobOrderID)
				{ 
					$qry = mysqli_prepare($db, "CALL sp_Job_Order_Query(?)");
					mysqli_stmt_bind_param($qry, 'i', $JobOrderID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_job_order.php'.'</td><td>'.$processError.' near line 35.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							$RequestDate = $row['RequestDate'];
							$ControlNumber = $row['ControlNumber'];
							$DepartmentID = $row['DepartmentID'];
							$JobDetails = htmlspecialchars($row['JobDetails']);
							$RequisitionerID = $row['RequisitionerID'];
							$RequestingDepartment = $row['RequestingDepartment'];
							$Requisitioner = $row['Requisitioner'];
							$JobStatus = $row['JobStatus'];
							$SuperiorID = $row['SuperiorID'];
							$ButtonValue = $row['ButtonValue'];
							$TagPrinted = $row['TagPrinted'];
							$CreatedAt = $row['CreatedAt'];
							$Superior = $row['Superior'];
						}
					}
					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.job_order";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_job_order.php'.'</td><td>'.$processErrorPI.' near line 61.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row['id'];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($JobOrderID, $id) ){	//, TRUE
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_job_order.php</td><td>The user tries to edit a non-existing finished_good_id.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}

					echo "<title>Job Order - Edit</title>";
				}
				else{

					echo "<title>Job Order - Add</title>";

				}
			}
		?>
		<script src="js/job_order_js.js"></script>
	</head>
	<body onload="showApprovers()">

		<form method='post' action='process_new_job_order.php'>

			<?php
				require("/include/header.php");
				require ("/include/init_unset_values/job_order_init_value.php");

				// if ( $JobOrderID ){
				// 	if( $_SESSION['label_name'] == false) 
				// 	{
				// 		$_SESSION['ERRMSG_ARR'] ='Access denied!';
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}
				// }
			?>

			<div class="wrapper">

				<span> <h3> <?php echo ( $JobOrderID ? "View " : "New " );?> Job Request </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<colgroup><col width="200px"></col></colgroup>
					<tr>
						<td> Control Number: </td>
						<td> 
							<?php
								$qryJON = mysqli_prepare($db, "CALL sp_Job_Order_Num(?)");
								mysqli_stmt_bind_param($qryJON, 'i', $JobOrderID);
								$qryJON->execute();
								$resultJON = mysqli_stmt_get_result($qryJON); //return results of query
								$processErrorJON = mysqli_error($db);

								if(!empty($processErrorJON))
								{
									error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_job_order.php'.'</td><td>'.$processErrorJON.' near line 35.</td></tr>', 3, "errors.php");
									header("location: error_message.html");
								}
								else
								{	
									while($row = mysqli_fetch_assoc($resultJON))
									{
										$JobOrderControlNumber = $row['JobOrderControlNumber'];
									}
								}
								$db->next_result();
								$resultJON->close();
							?>
							<?php echo ($JobOrderID ? $ControlNumber : $JobOrderControlNumber); ?>
							<input type="hidden" name="hidControlNumber" value="<?php echo ($JobOrderID ? $ControlNumber : $JobOrderControlNumber); ?>">
						</td>
					</tr>
					<tr>
						<td> Date of Request: </td>
						<td> 
							<?php echo ($JobOrderID ? $RequestDate : date("Y-m-d")); ?>
							<input type="hidden" name="hidRequestDate" value="<?php echo ($JobOrderID ? $RequestDate : date("Y-m-d")); ?>">
						</td>
					</tr>
					<tr valign="top">
						<td> Department: </td>
						<td>
							<?php
								if ( $JobOrderID ){
									echo $RequestingDepartment;	
							?>
									<input type="hidden" name="radRequestingDept" value="<?php echo $DepartmentID;?>">
							<?php
								}else{
									$qryRDD = mysqli_prepare($db, "CALL sp_Job_Order_Dept_Dropdown(?)");
									mysqli_stmt_bind_param($qryRDD, 'i', $_SESSION["SESS_USER_ID"]);
									$qryRDD->execute();
									$resultRDD = mysqli_stmt_get_result($qryRDD); //return results of query
									$processErrorRDD = mysqli_error($db);

									if(!empty($processErrorRDD))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_job_order.php'.'</td><td>'.$processErrorRDD.' near line 35.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{	
										while($row = mysqli_fetch_assoc($resultRDD))
										{
											$RequestingDeptID = $row['RequestingDeptID'];
											$RequestingDept = $row['RequestingDept'];
											$Tag = $row['Tag'];
							?>			
											<input type="radio" name="radRequestingDept" 
												id="<?php echo $RequestingDeptID;?>" 
												value="<?php echo $RequestingDeptID;?>"
												<?php echo ( $Tag == "*" ? "checked" : "" );?>
												onchange="showApprovers()"> 
												<label for="<?php echo $RequestingDeptID;?>"><?php echo $RequestingDept;?></label>
											<br>
							<?php
										}
									}
									$db->next_result();
									$resultRDD->close();
								}
							?>
						</td>
					</tr>
					<tr valign="top">
						<td> Details of Job Request: </td>
						<td>
							<?php
								if( $JobOrderID ){
									echo "<p class='job_order'>".nl2br($JobDetails)."</p>";
							?>
									<input type="hidden" name="txtJobDetail" value="<?php echo $JobDetails;?>">
							<?php
								}else{
							?>
									<textarea name="txtJobDetail" class="paragraph"><?php echo $initJOFJobDetail;?></textarea>
							<?php
								}
							?>
						</td>
					</tr>
					<?php if( $JobOrderID ){?>
						<tr>
							<td> Requisitioner: </td>
							<td> <?php echo $Requisitioner;?> </td>
						</tr>
					<?php }?>
					<tr valign="top">
						<td> Department Head / Superior: </td>
						<td>
							<?php
								if ( $JobOrderID ){
									echo $Superior;

									$qryJOS = mysqli_prepare($db, "CALL sp_Job_Order_Superior_Query(?,?)");
									mysqli_stmt_bind_param($qryJOS, 'ii', $RequisitionerID, $DepartmentID);
									$qryJOS->execute();
									$resultJOS = mysqli_stmt_get_result($qryJOS);
									while ($row = mysqli_fetch_assoc($resultJOS)) {
										$ParentEmail = $row['ParentEmail'];
										$TempParentUserID = $row['TempParentUserID'];
									}
									$db->next_result();
									$resultJOS->close();
							?>
									<input type="hidden" name="radSuperior" value="<?php echo $TempParentUserID;?>">
									<input type="hidden" name="hidEmail" value="<?php echo $ParentEmail;?>">
									<input type="hidden" name="hidSuperiorName" value="<?php echo $Superior;?>">
							<?php
								}else{
							?>
									<div id="tdApproverID"></div>
							<?php
								}
							?>
						</td>
					</tr>
					<?php
						if( $JobOrderID ){
					?>
							<tr>
								<td> Status: </td>
								<td> <b> <?php echo $JobStatus;?> </b> </td>
							</tr>
					<?php
						}
					?>
					<tr class="align_bottom">
						<td colspan="2">
							<?php
								// $ButtonValue = "APPROVED";
								$PAMRoleIDs = array(8,7,32,28,130);
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="hidden" name="hidRequisitionerID" id="hidRequisitionerID" value="<?php echo ( $JobOrderID ? $RequisitionerID : $_SESSION["SESS_USER_ID"] ); ?>">
							<?php
								if ( $JobOrderID ){
									if( $ButtonValue == "NOT SENT" && $RequisitionerID == $_SESSION["SESS_USER_ID"] ){
							?>
										<input type="submit" name="btnSave" value="Send for Approval">
							<?php 	
									}elseif( $ButtonValue == "NOT SENT" && $SuperiorID == $_SESSION["SESS_USER_ID"] ){
							?>
										<!-- <input type="submit" name="btnSave" value="Approve"> -->
										<input type="submit" name="btnSave" value="Approve & Send">
							<?php
									}elseif( $ButtonValue == "FOR APPROVAL" && $SuperiorID == $_SESSION["SESS_USER_ID"] ){
							?>
										<!-- <input type="submit" name="btnSave" value="Approve"> -->
										<input type="submit" name="btnSave" value="Approve & Send">
							<?php
									}elseif( $ButtonValue == "APPROVED" && $SuperiorID == $_SESSION["SESS_USER_ID"] ){
							?>
										<input type="submit" name="btnSave" value="Forward to PAM">
							<?php
									}elseif( ( $ButtonValue == "APPROVED" || $ButtonValue == "FORWARDED" ) &&
											 ( in_array($_SESSION["SESS_RoleID1"], $PAMRoleIDs) || 
											   in_array($_SESSION["SESS_RoleID2"], $PAMRoleIDs) || 
											   in_array($_SESSION["SESS_RoleID3"], $PAMRoleIDs)) &&
											 ( $TagPrinted == 1 ) ){
							?>
										<input type="button" value="Add Remarks">
							<?php
									}
								}else{
							?>
									<!-- <input type="submit" name="btnSave" value="Save">	 -->
									<input type="submit" name="btnSave" value="Save & Send">	
							<?php
								}
							?>
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='job_order.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidJobOrderID' value="<?php echo $JobOrderID;?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $JobOrderID ? $CreatedAt : date("Y-m-d H:i:s")); ?>">
						</td>
					</tr>
				</table>
				
			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>