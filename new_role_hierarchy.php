<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_role_hierarchy.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$hierarchyID = $_GET["id"];

				if($hierarchyID)
				{ 

					$qry = mysqli_prepare($db, "CALL sp_Role_Hierarchy_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $hierarchyID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_role_hierarchy.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						while($row = mysqli_fetch_assoc($result))
						{
							$db_role_id = $row["role_id"];
							$db_department_id = $row["department_id"];
							$db_role = $row["role"];
							$db_department = $row["department"];
							$db_parent_id = $row["parent_id"];
							$db_level = $row["level"];
						}
					}

					$db->next_result();
					$result->close();

		############ .............
					$qryPI = "SELECT id from comsys.role_hierarchy";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_role_hierarchy.php"."</td><td>".$processErrorPI." near line 69.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
					}
					$db->next_result();
					$resultPI->close();

		############ .............
					if( !in_array($hierarchyID, $id, TRUE) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_role_hierarchy.php</td><td>The user tries to edit a non-existing supply_id.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					
					echo "<title>Supply - Edit</title>";
				}
				else
				{
					echo "<title>Supply - Add</title>";
				}
			}
		?>
	</head>
	<body>

		<form method="post" action="process_new_role_hierarchy.php">

			<?php
				require("/include/header.php");
			require("/include/init_unset_values/supplies_init_value.php");

				// if ( $hierarchyID ){
				// 	if( $_SESSION["edit_supplies"] == false) 
				// 	{
				// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}
				// }else{
				// 	if( $_SESSION["add_supplies"] == false) 
				// 	{
				// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
				// 		session_write_close();
				// 		header("Location:comsys.php");
				// 		exit();
				// 	}
				// }
			?>

			<div class="wrapper">
				
				<span> <h3> Select Superior </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					
					<tr>
						<td> Job Title: </td>
						<td> <b> <?php echo $db_role;?> </b> </td>
						<input type="hidden" name="hidJobTitle" value="<?php echo $db_role;?>">
					</tr>
					<tr>
						<td> Department: </td>
						<td> <b> <?php echo $db_department;?> </b> </td>
					</tr>
					<tr>
						<td> Job Level: </td>
						<td>
							<input type="text" name="txtJobLevel" value="<?php echo $db_level;?>">
						</td>
					</tr>
					<tr>
						<td> Superior: </td>
						<td>
							<select name="sltSuperior">
								<?php
									$qryRHD = mysqli_prepare($db, "CALL sp_Role_Hierarchy_Dropdown(?, ?, 1)");
									mysqli_stmt_bind_param($qryRHD, "ii", $db_department_id, $db_role_id);
									$qryRHD->execute();
									$resultRHD = mysqli_stmt_get_result($qryRHD);
									$processErrorRHD = mysqli_error($db);

									if(!empty($processErrorRHD))
									{
										error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_role_hierarchy.php"."</td><td>".$processErrorRHD." near line 35.</td></tr>", 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultRHD))
										{
											$RoleIDDD = $row["role_id"];
											$RoleDD = $row["role"];
								?>
											<option value="<?php echo $RoleIDDD;?>" <?php echo ( $RoleIDDD == $db_parent_id ? "selected" : "" );?>> <?php echo $RoleDD;?> </option>
								<?php
										}
									}

									$db->next_result();
									$resultRHD->close();
								?>
							</select>
						</td>
					</tr>

					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveRole" value="Save">	
							<input type='button' name='btnCancel' value='Cancel' onclick="location.href='role_hierarchy.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type='hidden' name='hidHierarchyID' value="<?php echo $hierarchyID; ?>">
						</td>
					</tr>

				</table>

			</div>

		</form>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>