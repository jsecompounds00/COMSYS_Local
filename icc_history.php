<html>
	<head>
		<title>ICC Monitoring - History</title>
		<?php
			require("include/database_connect.php");

			$page = ($_GET['page'] ? $_GET['page'] : NULL);
			$qsone='';

			$vid = $_GET['id'];

			$qryC = "SELECT icc_number FROM icc_monitoring WHERE id = $vid";
			$resultC = mysqli_query($db, $qryC);
			$processErrorC = mysqli_error($db);
			if(!empty($processErrorC))
			{
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_icc_verification.php'.'</td><td>'.$processErrorC.' near line 33.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}else{
				$row = mysqli_fetch_assoc($resultC);
				$icc_number = $row['icc_number'];
			}
		?>
	</head>
	<body>

		<?php
			require("/include/header.php");
			
			$page_a = $_SESSION["page"];
			$search_a = htmlspecialchars($_SESSION["search"]);
			$qsone_a = htmlspecialchars($_SESSION["qsone"]);
			$search_a = ( (strpos(($search_a), "\\")+1) > 0 ? str_replace("\\", "", $search_a) : $search_a );
			$search_a = ( (strpos(($search_a), "'")+1) > 0 ? str_replace("'", "\'", $search_a) : $search_a );
			$qsone_a = ( (strpos(($qsone_a), "\\")+1) > 0 ? str_replace("\\", "", $qsone_a) : $qsone_a );
			$qsone_a = ( (strpos(($qsone_a), "'")+1) > 0 ? str_replace("'", "\'", $qsone_a) : $qsone_a );
		?>

		<div class="wrapper">

			<input type='hidden' name='page' value='<?php echo $_GET['page'];?>'>

			<span> <h2> ICC Monitoring History for ICC No. <?php echo $icc_number; ?></h2> </span>

			<a href='icc_monitoring.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>' class="back"><img src='images/back.png' height="20" name='txtBack'> Back</a>

			<?php	

				if(!empty($errno))
				{
					$error = mysqli_connect_error();
					error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>icc_history.php'.'</td><td>'.$error.' near line 42.</td></tr>', 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{				
					$qryCM = mysqli_prepare($db, "CALL sp_ICC_Verification_History(?, NULL, NULL)");
					mysqli_stmt_bind_param($qryCM, 'i', $search);
					$qryCM->execute();
					$resultCM = mysqli_stmt_get_result($qryCM);

					$total_results = mysqli_num_rows($resultCM);

					$db->next_result();
					$resultCM->close();

					$targetpage = "icc_history.php"; 
					require("include/paginate_hist.php");

					$qry = mysqli_prepare($db, "CALL sp_ICC_Verification_History(?, ?, ?)");
					mysqli_stmt_bind_param($qry, 'iii', $vid, $start, $end);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry);
					$processError = mysqli_error($db);
					
					if(!empty($processError))
					{
						error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>icc_history.php'.'</td><td>'.$processError.' near line 63.</td></tr>', 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{
						if( isset($_SESSION['SUCCESS'])) 
						{
							echo '<ul id="success">';
							echo '<li>'.$_SESSION['SUCCESS'].'</li>'; 
							echo '</ul>';
							unset($_SESSION['SUCCESS']);
						}
					}
			?>
			
					<table class="home_pages">
						<col></col><col width='700'></col>
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
						<tr>
						    <th>ICC Date</th>
						    <th>Verification Statement</th>
						    <th>Status</th>
						    <th>Next Verification Date</th>
						    <th></th>
						</tr>
						<?php 
							while($row = mysqli_fetch_assoc($result)) { 
						?>
								<tr>
									<td><?php echo $row['ICCVerificationDate']; ?></td>
									<td><?php echo nl2br($row['ICCVerificationStatement']); ?></td>
									<td><?php echo $row['ICCStatus']; ?></td>
									<td><?php echo $row['NextVerificationDate']; ?></td>
									<td>
					 					<?php
											// if(array_search(116, $session_Permit)){
										?>
											<input type='button' name='btnTP' value='Edit Verification' onclick="location.href='new_icc_verification.php?id=<?php echo $row['ICCID'];?>&vid=<?php echo $row['ICCVerificationID'];?>'">
										<?php
											//  	$_SESSION['add_ccr'] = true;
											// }else{
											//  	unset($_SESSION['add_ccr']);
											// }
										?>
									</td>
								</tr>
						<?php
							} 
						?>
						<tr>
							<td colspan='6'>
								<?php echo $pagination;?>
							</td>
						</tr>
					</table>
			<?php
				}
			?>
		</div>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>