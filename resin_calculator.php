<html>
	<head>
		<script src="js/jscript.js"></script>
		<title>Resin Calculator</title>
		<?php 
			require("/include/database_connect.php");
		?>
	</head>
	<body>

		<?php 
			require("/include/header.php");
			require("/include/init_value.php");

			if( $_SESSION['resin_calculator'] == false) 
			{
				$_SESSION['ERRMSG_ARR'] ='Access denied!';
				session_write_close();
				header("Location:comsys.php");
				exit();
			}

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>resin_calculator.php'.'</td><td>'.$error.' near line 9.</td></tr>', 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
		?>
				<div class="wrapper">

					<span> <h3> Resin Calculator </h3> </span>
					
					<table class="parent_tables_form">
						<colgroup><col width='180px'></col><col width='180px'></col><col width='180px'></col></colgroup>

						<tr>
							<td> Mixer: </td>
							<td>
								<select name='sltMixer' id='sltMixer' onchange='showMixerCapacity()'>
									<option></option>
									<?php
										$qryM = "CALL sp_Mixer_Dropdown()";
										$resultM = mysqli_query($db, $qryM);
										$processErrorM = mysqli_error($db);
										if(!empty($processErrorM))
										{
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>resin_calculator.php'.'</td><td>'.$processErrorM.' near line 37.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultM))
											{
												$mixerId = $row['id'];
												$Mixer = $row['name'];

												echo "<option value='".$mixerId."'>".$Mixer."</option>";
											}
										}
										$db->next_result();
										$resultM->close();
									?>
								</select>
							</td>

							<td>Capacity:</td>

							<td id='tdMixerCapacity'></td>
						</tr>

						<tr>
							<td>Finished Goods:</td>

							<td>
								<select name='sltFGItem' id='sltFGItem' onchange='showFormulaType(this.value,0,0), showFormulaPHR(0)'>
									<option></option>
									<?php
										$qryF = "CALL sp_FG_Calcu_Dropdown()";
										$resultF = mysqli_query($db, $qryF);
										$processErrorF = mysqli_error($db);
										if(!empty($processErrorF))
										{
											error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>resin_calculator.php'.'</td><td>'.$processErrorF.' near line 37.</td></tr>', 3, "errors.php");
											header("location: error_message.html");
										}
										else
										{
											while($row = mysqli_fetch_assoc($resultF))
											{
												$FGId = $row['id'];
												$FGItem = $row['name'];

												echo "<option value='".$FGId."'>".$FGItem."</option>";
											}
										}
										$db->next_result();
										$resultF->close();
									?>
								</select>
							</td>
						</tr>

						<tr>
							<td>Formula Type:</td>

							<td>
								<select name='sltFormulaType' id='sltFormulaType' onchange='showFormulaPHR(this.value)'>
								</select>
							</td>
						</tr>

					</table>

					<table class='child_tables_form' id='tble10'>
					</table>

				</div>
		<?php
			}
		?>

	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>