<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_cpiar_monitoring.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidCpiarID = $_POST['hidCpiarID'];
		$txtCPRNo = $_POST['txtCPRNo'];
		$txtCPRDate = $_POST['txtCPRDate'];
		$txtDueDate = $_POST['txtDueDate'];
		$sltIssueDeptID = $_POST['sltIssueDeptID'];
		$sltIssuerID = $_POST['sltIssuerID'];
		$sltReceiveDeptID = $_POST['sltReceiveDeptID'];
		$sltReceiverID = $_POST['sltReceiverID'];
		$chkOccurrence = $_POST['chkOccurrence'];
		$chkCriticality = $_POST['chkCriticality'];
		$chkSource = $_POST['chkSource'];

		$txtClause = $_POST['txtClause'];
		$txtDetails = $_POST['txtDetails'];
		$txtMemoDate = $_POST['txtMemoDate'];
		$txtFinalDueDate = $_POST['txtFinalDueDate'];
		$txtReceiveDateDCC = $_POST['txtReceiveDateDCC'];

		$txtRootCause = $_POST['txtRootCause'];
		$txtContainment = $_POST['txtContainment'];
		$txtImplementContainment = $_POST['txtImplementContainment'];
		$chkAction = $_POST['chkAction'];
		$txtCorrectPrevent = $_POST['txtCorrectPrevent'];
		$txtImplementCorrectPrevent = $_POST['txtImplementCorrectPrevent'];

		$txtRemarks = $_POST['txtRemarks'];

		$radFromDivision = $_POST['radFromDivision'];
		$radToDivision = $_POST['radToDivision'];

		if( isset($_POST['chkMan']) ){
			$chkMan = 1;
		}else{
			$chkMan = 0;
		}
		if( isset($_POST['chkMethod']) ){
			$chkMethod = 1;
		}else{
			$chkMethod = 0;
		}
		if( isset($_POST['chkMachine']) ){
			$chkMachine = 1;
		}else{
			$chkMachine = 0;
		}
		if( isset($_POST['chkMaterial']) ){
			$chkMaterial = 1;
		}else{
			$chkMaterial = 0;
		}
		if( isset($_POST['chkMeasurement']) ){
			$chkMeasurement = 1;
		}else{
			$chkMeasurement = 0;
		}
		if( isset($_POST['chkMotherNature']) ){
			$chkMotherNature = 1;
		}else{
			$chkMotherNature = 0;
		}

############# Input Validation
		if ( $txtCPRNo == '' ){
			$errmsg_arr[] = '* Invalid CPIAR Number.';
			$errflag = true;
		}
		$valCPRDate = validateDate($txtCPRDate, 'Y-m-d');
		if ( $valCPRDate != 1 ){
			$errmsg_arr[] = '* Invalid CPIAR Date.';
			$errflag = true;
		}
		$valDueDate = validateDate($txtDueDate, 'Y-m-d');
		if ( $valDueDate != 1 ){
			$errmsg_arr[] = '* Invalid Due Date.';
			$errflag = true;
		}
		if ( !$sltIssueDeptID ){
			$errmsg_arr[] = '* Invalid Issuing Area.';
			$errflag = true;
		}
		if ( !$sltIssuerID ){
			$errmsg_arr[] = '* Invalid Issuing Person.';
			$errflag = true;
		}
		if ( !$sltReceiveDeptID ){
			$errmsg_arr[] = '* Invalid Receiving Area.';
			$errflag = true;
		}
		if ( !$sltReceiverID ){
			$errmsg_arr[] = '* Invalid Receiving Person.';
			$errflag = true;
		}

		if ( !isset( $chkSource ) ){
			$errmsg_arr[] = '* Choose source of feedback.';
			$errflag = true;
		}

		if ( !isset( $chkOccurrence ) ){
			$errmsg_arr[] = '* Choose occurrence.';
			$errflag = true;
		}

		if ( !isset( $chkCriticality ) ){
				$errmsg_arr[] = '* Choose criticality.';
				$errflag = true;
		}

		if ( isset( $chkCriticality ) && $chkCriticality != 3 ){
			if ( $txtClause == '' ){
				$errmsg_arr[] = '* ISO Clause cannot be blank.';
				$errflag = true;
			}
		}
		if ( $txtDetails == '' ){
			$errmsg_arr[] = '* Details of Feedback cannot be blank.';
			$errflag = true;
		}
		// if ( !isset($_POST['chkMan']) && !isset($_POST['chkMethod']) && !isset($_POST['chkMachine']) && !isset($_POST['chkMaterial']) && !isset($_POST['chkMeasurement']) && !isset($_POST['chkMotherNature']) )
		// {
		// 	$errmsg_arr[] = '* Choose at least one (1) Category of RCA.';
		// 	$errflag = true;
		// }
		// if ( $txtRootCause == '' ){
		// 	$errmsg_arr[] = '* Root Cause cannot be blank.';
		// 	$errflag = true;
		// }
		// if ( $txtContainment == '' ){
		// 	$errmsg_arr[] = '* Containment Action cannot be blank.';
		// 	$errflag = true;
		// }
		// if ( $txtImplementContainment == '' ){
		// 	$errmsg_arr[] = '* Implementation date of containment action cannot be blank.';
		// 	$errflag = true;
		// }
		// if ( !isset( $chkAction ) ){
		// 	$errmsg_arr[] = '* Choose type of action.';
		// 	$errflag = true;
		// }
		// if ( $txtCorrectPrevent == '' ){
		// 	$errmsg_arr[] = '* Action taken / Proposed action cannot be blank.';
		// 	$errflag = true;
		// }
		// if ( $txtImplementCorrectPrevent == '' ){
		// 	$errmsg_arr[] = '* Implementation date of action taken cannot be blank.';
		// 	$errflag = true;
		// }
		$valMemoDate = validateDate($txtMemoDate, 'Y-m-d');
		if ( $txtMemoDate != '' && $valMemoDate != 1 ){
			$errmsg_arr[] = '* Invalid Issuance of reminder.';
			$errflag = true;
		}elseif ( $txtMemoDate == '' ){
			$txtMemoDate = NULL;
		}
		$valFinalDueDate = validateDate($txtFinalDueDate, 'Y-m-d');
		if ( $txtFinalDueDate != '' && $valFinalDueDate != 1 ){
			$errmsg_arr[] = '* Invalid Final due date.';
			$errflag = true;
		}elseif ( $txtFinalDueDate == '' ){
			$txtFinalDueDate = NULL;
		}
		$valReceiveDateDCC = validateDate($txtReceiveDateDCC, 'Y-m-d');
		if ( $txtReceiveDateDCC != '' && $valReceiveDateDCC != 1 ){
			$errmsg_arr[] = '* Invalid Receipt date of DCC.';
			$errflag = true;
		}elseif ( $txtReceiveDateDCC == '' ){
			$txtReceiveDateDCC = NULL;
		}

		// foreach ($_POST as $key => $value) {
		// 	echo $key." => ".$value."<br>";
		// }

		if( isset($_POST['radToDivision']) ){
			echo $_POST['radToDivision'];
		}else{ echo "n"; }

	######### Validation on Checkbox
		if($hidCpiarID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidCpiarID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation

	// $txtDetails = str_replace('\\r\\n', '<br>', $txtDetails);
	// $txtRootCause = str_replace('\\r\\n', '<br>', $txtRootCause);
	// $txtContainment = str_replace('\\r\\n', '<br>', $txtContainment);
	// $txtCorrectPrevent = str_replace('\\r\\n', '<br>', $txtCorrectPrevent);
	// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
	
	// $txtDetails = str_replace('\\', '', $txtDetails);
	// $txtRootCause = str_replace('\\', '', $txtRootCause);
	// $txtContainment = str_replace('\\', '', $txtContainment);
	// $txtCorrectPrevent = str_replace('\\', '', $txtCorrectPrevent);
	// $txtRemarks = str_replace('\\', '', $txtRemarks);


	############# SESSION, keeping last input value
		$_SESSION['SESS_CPM_CPRNo'] = $txtCPRNo;
		$_SESSION['SESS_CPM_CPRDate'] = $txtCPRDate;
		$_SESSION['SESS_CPM_DueDate'] = $txtDueDate;
		$_SESSION['SESS_CPM_IssueDeptID'] = $sltIssueDeptID;
		$_SESSION['SESS_CPM_IssuerID'] = $sltIssuerID;
		$_SESSION['SESS_CPM_ReceiveDeptID'] = $sltReceiveDeptID;
		$_SESSION['SESS_CPM_ReceiverID'] = $sltReceiverID;
		$_SESSION['SESS_CPM_Occurrence'] = $chkOccurrence;
		$_SESSION['SESS_CPM_Criticality'] = $chkCriticality;
		$_SESSION['SESS_CPM_Source'] = $chkSource;
		$_SESSION['SESS_CPM_Clause'] = $txtClause;
		$_SESSION['SESS_CPM_Details'] = $txtDetails;
		$_SESSION['SESS_CPM_MemoDate'] = $txtMemoDate;
		$_SESSION['SESS_CPM_FinalDueDate'] = $txtFinalDueDate;
		$_SESSION['SESS_CPM_ReceiveDateDCC'] = $txtReceiveDateDCC;
		$_SESSION['SESS_CPM_RootCause'] = $txtRootCause;
		$_SESSION['SESS_CPM_Containment'] = $txtContainment;
		$_SESSION['SESS_CPM_ImplementContainment'] = $txtImplementContainment;
		$_SESSION['SESS_CPM_Action'] = $chkAction;
		$_SESSION['SESS_CPM_CorrectPrevent'] = $txtCorrectPrevent;
		$_SESSION['SESS_CPM_ImplementCorrectPrevent'] = $txtImplementCorrectPrevent;
		$_SESSION['SESS_CPM_Man'] = $chkMan;
		$_SESSION['SESS_CPM_Method'] = $chkMethod;
		$_SESSION['SESS_CPM_Machine'] = $chkMachine;
		$_SESSION['SESS_CPM_Material'] = $chkMaterial;
		$_SESSION['SESS_CPM_Measurement'] = $chkMeasurement;
		$_SESSION['SESS_CPM_MotherNature'] = $chkMotherNature;
		$_SESSION['SESS_CPM_Remarks'] = $txtRemarks;
		$_SESSION['SESS_CPM_FromDivision'] = $radFromDivision;
		$_SESSION['SESS_CPM_ToDivision'] = $radToDivision;

############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_cpiar_monitoring.php?id=$hidCpiarID");
			exit();
		}

############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_CPIAR_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'isssiiiiiiisssssssiisssissiiiiiisss', $hidCpiarID, $txtCPRNo, $txtCPRDate, $txtDueDate, $sltIssueDeptID
												, $sltIssuerID, $sltReceiveDeptID, $sltReceiverID, $chkSource, $chkOccurrence
												, $chkCriticality, $txtClause, $txtMemoDate, $txtFinalDueDate, $txtReceiveDateDCC
												, $txtDetails, $createdAt, $updatedAt, $createdId, $updatedId, $txtRootCause
												, $txtContainment, $txtImplementContainment, $chkAction, $txtCorrectPrevent
												, $txtImplementCorrectPrevent, $chkMan, $chkMethod, $chkMachine, $chkMaterial
												, $chkMeasurement, $chkMotherNature, $txtRemarks, $radFromDivision, $radToDivision);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_cpiar_monitoring.php'.'</td><td>'.$processError.' near line 93.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidCpiarID)
				$_SESSION['SUCCESS']  = 'Successfully updated CPIAR.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added new CPIAR.';
			//echo $_SESSION['SUCCESS'];
			header("location:cpiar_monitoring.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>