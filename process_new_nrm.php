<?php
############# Start session
	session_start();

############# Array to store validation errors
	$errmsg_arr = array();
 
############# Validation error flag
	$errflag = false;
 
############# Function to validate date
	function validateDate($date, $format = 'Y-m-d') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	require ("include/database_connect.php");
	require ("include/constant.php");

	if(!empty($errno))
	{
		$error = mysqli_connect_error();
		error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_nrm.php'.'</td><td>'.$error.' near line 26.</td></tr>', 3, "errors.php");
		header("location: error_message.html");
	}
	else
	{
############# Sanitizing POST Values
		$hidNRMID = $_POST['hidNRMID'];
		$txtNRMNo = $_POST['txtNRMNo'];
		$txtNRMDate = $_POST['txtNRMDate'];
		$txtMaterialDescription = $_POST['txtMaterialDescription'];
		$sltSupplier = $_POST['sltSupplier'];
		$chkOtherSupplier = $_POST['chkOtherSupplier'];
		$txtOtherSupplier = $_POST['txtOtherSupplier'];
		$radClass = $_POST['radClass'];
		$txtOtherClass = $_POST['txtOtherClass'];
		$radSample = $_POST['radSample'];
		$txtOtherSample = $_POST['txtOtherSample'];
		$chkMSDS = $_POST['chkMSDS'];
		$chkTDS = $_POST['chkTDS'];
		$txtRemarks = $_POST['txtRemarks'];

############# Input Validation
		if ( $txtNRMNo == '' ){
			$errmsg_arr[] = '* NRM number cannot be blank.';
			$errflag = true;
		}
		$valVerifyDate = validateDate($txtNRMDate, 'Y-m-d');
		if ( $valVerifyDate != 1 ){
			$errmsg_arr[] = '* Invalid NRM date.';
			$errflag = true;
		}
		if ( $txtMaterialDescription == '' ){
			$errmsg_arr[] = '* Material description cannot be blank.';
			$errflag = true;
		}
		if ( !isset( $chkOtherSupplier ) && !$sltSupplier ){
			$errmsg_arr[] = '* Supplier is required.';
			$errflag = true;
		}
		if ( isset( $chkOtherSupplier ) && $txtOtherSupplier == '' ){
			$errmsg_arr[] = '* Please specify other supplier.';
			$errflag = true;
		}
		if ( !isset( $radClass ) ){
			$errmsg_arr[] = '* Choose material classification.';
			$errflag = true;
		}
		if ( isset( $radClass ) && $radClass == 'Others' && $txtOtherClass == '' ){
			$errmsg_arr[] = '* Please specify other material classification.';
			$errflag = true;
		}
		if ( !isset( $radSample ) ){
			$errmsg_arr[] = '* Choose sample description.';
			$errflag = true;
		}
		if ( isset( $radSample ) && $radSample == 'Others' && $txtOtherSample == '' ){
			$errmsg_arr[] = '* Please specify other sample description.';
			$errflag = true;
		}
		if ( isset( $chkMSDS ) ){
			$chkMSDS = 1;
		}else{
			$chkMSDS = 0;
		}
		if ( isset( $chkTDS ) ){
			$chkTDS = 1;
		}else{
			$chkTDS = 0;
		}
		if ( isset( $chkOtherSupplier ) ){
			$chkOtherSupplier = 1;
		}else{
			$chkOtherSupplier = 0;
		}

	######### Validation on Checkbox
		if($hidNRMID == 0)
			$createdAt = date('Y-m-d H:i:s');
		else $createdAt = $_POST['hidCreatedAt'];
		$updatedAt = date('Y-m-d H:i:s');
		if($hidNRMID == 0)
			$createdId = $_SESSION['SESS_USER_ID'];
		else $createdId = $_POST['hidCreatedId'];
		$updatedId = $_SESSION['SESS_USER_ID'];

############# Input Validation
	// $txtRemarks = str_replace('\\r\\n', '<br>', $txtRemarks);
					
	// $txtNRMNo = str_replace('\\', '', $txtNRMNo);
	// $txtNRMDate = str_replace('\\', '', $txtNRMDate);
	// $txtMaterialDescription = str_replace('\\', '', $txtMaterialDescription);
	// $txtOtherSupplier = str_replace('\\', '', $txtOtherSupplier);
	// $txtOtherClass = str_replace('\\', '', $txtOtherClass);
	// $txtOtherSample = str_replace('\\', '', $txtOtherSample);

	############# SESSION, keeping last input value
		$_SESSION['SESS_NRM_NRMNo'] = $txtNRMNo;
		$_SESSION['SESS_NRM_NRMDate'] = $txtNRMDate;
		$_SESSION['SESS_NRM_MaterialDescription'] = $txtMaterialDescription;
		$_SESSION['SESS_NRM_Supplier'] = $sltSupplier;
		$_SESSION['SESS_NRM_OtherSupplier'] = $chkOtherSupplier;
		$_SESSION['SESS_NRM_OtherSupplier'] = $txtOtherSupplier;
		$_SESSION['SESS_NRM_Class'] = $radClass;
		$_SESSION['SESS_NRM_OtherClass'] = $txtOtherClass;
		$_SESSION['SESS_NRM_Sample'] = $radSample;
		$_SESSION['SESS_NRM_OtherSample'] = $txtOtherSample;
		$_SESSION['SESS_NRM_MSDS'] = $chkMSDS;
		$_SESSION['SESS_NRM_TDS'] = $chkTDS;
		$_SESSION['SESS_NRM_Remarks'] = $txtRemarks;

		if ( !isset( $chkOtherSupplier ) && $sltSupplier ){
			$txtOtherSupplier = '';
		}
		if ( isset( $radClass ) && $radClass != 'Others' ){
					$txtOtherClass = '';
				}
		if ( isset( $radSample ) && $radSample != 'Others' ){
					$txtOtherSample = '';
		}

// ############# If there are input validations, redirect back to the login form
		if($errflag) {
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("Location: new_nrm.php?id=$hidNRMID");
			exit();
		}
############# Commiting to Database
		$qry = mysqli_prepare($db, "CALL sp_NRM_Evaluation_CRU( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		mysqli_stmt_bind_param($qry, 'isssisssssiisssii', $hidNRMID, $txtNRMNo, $txtNRMDate, $txtMaterialDescription
												, $sltSupplier, $txtOtherSupplier, $radClass, $txtOtherClass, $radSample
												, $txtOtherSample, $chkMSDS, $chkTDS, $txtRemarks
												, $createdAt, $updatedAt, $createdId, $updatedId);
		$qry->execute();
		$result = mysqli_stmt_get_result($qry); 
		$processError = mysqli_error($db);

		if(!empty($processError))
		{
			error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>process_new_nrm.php'.'</td><td>'.$processError.' near line 116.</td></tr>', 3, "errors.php");
			header("location: error_message.html");
		}
		else
		{ 
			if($hidNRMID)
				$_SESSION['SUCCESS']  = 'Successfully updated request for nrm evaluation.';
			else
				$_SESSION['SUCCESS']  = 'Successfully added request for nrm evaluation.';
			//echo $_SESSION['SUCCESS'];
			header("location:nrm.php?page=".$_SESSION['page']."&search=".$_SESSION['search']."&qsone=".$_SESSION['qsone']);	
		}

		$db->next_result();
		$result->close(); 
		require("include/database_close.php");

	}
?>