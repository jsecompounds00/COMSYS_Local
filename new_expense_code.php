<html>
	<head>
		<?php 
			require("/include/database_connect.php");
			if($errno)
				{
					$error = mysqli_connect_error();
					error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_expense_code.php"."</td><td>".$error." near line 9.</td></tr>", 3, "errors.php");
					header("location: error_message.html");
				}
				else
				{
					$expID = $_GET["id"];

					if($expID)
					{
						$qry = mysqli_prepare($db, "CALL sp_Budget_Expense_Code_Query(?)");
						mysqli_stmt_bind_param($qry, "i", $expID);
						$qry->execute();
						$result = mysqli_stmt_get_result($qry);
						$processError = mysqli_error($db);

						if(!empty($processError))
						{
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_expense_code.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}
						else
						{
							$department_id = array();
							while($row = mysqli_fetch_assoc($result))
							{
								$code = $row["code"];
								$description = htmlspecialchars($row["description"]);
								$active = $row["active"];
								$department_id[] = $row["department_id"];
								$tag_miscellaneous = $row["tag_miscellaneous"];
								$createdAt = $row["created_at"];
								$createdId = $row["created_id"];
							}
						}
						$db->next_result();
						$result->close();

						############ .............
						$qryPI = "SELECT id from comsys.expense_code";
						$resultPI = mysqli_query($db, $qryPI); 
						$processErrorPI = mysqli_error($db);

							if ( !empty($processErrorPI) ){
								error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_expense_code.php"."</td><td>".$processErrorPI." near line 58.</td></tr>", 3, "errors.php");
								header("location: error_message.html");
							}else{
								$id = array();
								while($row = mysqli_fetch_assoc($resultPI)){
									$id[] = $row["id"];
								}
							}
						$db->next_result();
						$resultPI->close();
					############ .............
						if( !in_array($expID, $id, TRUE) ){
							error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_expense_code.php</td><td>The user tries to edit a non-existing department_id.</td></tr>", 3, "errors.php");
							header("location: error_message.html");
						}
						echo "<title>Expense Code - Edit</title>";
					}
					else
					{
						echo "<title>Expense Code - Add</title>";
					}
				}
		?>
</head>
<body>
	<form method="post" action="process_new_expense_code.php">

		<?php
			require("/include/header.php");
			require("/include/init_unset_values/expense_code_init_value.php");

			// if ( $expID ){
			// 	if( $_SESSION["expense_code_edit"] == false) 
			// 	{
			// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
			// 		session_write_close();
			// 		header("Location:comsys.php");
			// 		exit();
			// 	}
			// }else{	
			// 	if( $_SESSION["expense_code_add"] == false) 
			// 	{
			// 		$_SESSION["ERRMSG_ARR"] ="Access denied!";
			// 		session_write_close();
			// 		header("Location:comsys.php");
			// 		exit();
			// 	}			
			// }
		?>

		<div class="wrapper">

			<span> <h3> <?php echo ( $expID ? "Edit ".$code."-".$description : "New Expense" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {

						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);

					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Code:</td>
						<td>
							<input type="text" name="txtCode" value="<?php echo ( $expID ? $code : $initEXPCode ); ?>">
						</td>
					</tr>
					<tr>
						<td>Description:</td>
						<td>
							<input type="text" name="txtDescription" value="<?php echo ( $expID ? $description : $initEXPDescription ); ?>">
						</td>
					</tr>
					<tr>
						<td>Active:</td>
						<td>
							<input type='checkbox' name='chkActive' <?php echo( $expID ? ( $active ? "checked" : "" ) : ( $initEXPActive ? "checked" : "" ) );?>>
						</td>
					</tr>
					<tr>
						<td>Supplies:</td>
						<td>
							<input type='checkbox' name='chkMiscellaneous' <?php echo( $expID ? ( $tag_miscellaneous ? "checked" : "" ) : ( $initEXPMiscellaneous ? "checked" : "" ) );?>>
						</td>
					</tr>
					<tr valign="top">
						<td>Applicable To:</td>
						<td>
							<?php 
									$qryU = "CALL sp_Budget_Department_Dropdown(2)"; //with default blank option for new supply
									$resultU = mysqli_query($db, $qryU);
									$processError2 = mysqli_error($db);

									if(!empty($processError2))
									{
										error_log('<tr><td>'.date('F d, Y H:i:s').'</td><td>new_expense_code.php'.'</td><td>'.$processError2.' near line 238.</td></tr>', 3, "errors.php");
										header("location: error_message.html");
									}
									else
									{
										while($row = mysqli_fetch_assoc($resultU))
										{
											$DDId = $row['DDId'];
											$DDCode = $row['DDCode'];

											echo "<input type='checkbox' name='chkApplicableTo[]' 
														id='chkApplicableTo".$DDId."' value='".$DDId."'
														".($expID ? ( in_array($DDId, $department_id) ? "checked" : "" ) : "" ).">
												  <label for='chkApplicableTo".$DDId."'>".$DDCode."</label>
												  <br>";
											
										}
										
										$db->next_result();
										$resultU->close();
									}
								?>
						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveExpense" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='expense_code.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidEXPID" value="<?php echo $expID;?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $expID ? $createdId : $_SESSION["SESS_USER_ID"] );?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $expID ? $createdAt : date("Y-m-d H:i:s") );?>">
						</td>
					</tr>
				</table>
		</div>
	</form>
</body>
<footer>
	<?php require ("/include/database_close.php") ;?>
</footer>
</html>