<html>
	<head>
		<?php
			require("/include/database_connect.php");

			if($errno)
			{
				$error = mysqli_connect_error();
				error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_raw_material_types.php"."</td><td>".$error." near line 11.</td></tr>", 3, "errors.php");
				header("location: error_message.html");
			}
			else
			{
				$rmTypeID = $_GET["id"];

				if($rmTypeID)
				{ 

					echo "<title>Raw Material Types - Edit</title>";

					$qry = mysqli_prepare($db, "CALL sp_RawMaterial_Types_Query(?)");
					mysqli_stmt_bind_param($qry, "i", $rmTypeID);
					$qry->execute();
					$result = mysqli_stmt_get_result($qry); //return results of query
					$processError = mysqli_error($db);

					if(!empty($processError))
					{
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_raw_material_types.php"."</td><td>".$processError." near line 35.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}
					else
					{	
						while($row = mysqli_fetch_assoc($result))
						{
							$id = $row["id"];
							$code = htmlspecialchars($row["code"]);
							$description = htmlspecialchars($row["description"]);
							$db_compounds = $row["compounds"];
							$db_pipes = $row["pipes"];
							$db_corporate = $row["corporate"];
							$db_ppr = $row["ppr"];
							$created_at = $row["created_at"];
							$created_id = $row["created_id"];
						}
					}
					$db->next_result();
					$result->close();

					############ .............
					$qryPI = "SELECT id from comsys.raw_material_types";
					$resultPI = mysqli_query($db, $qryPI); 
					$processErrorPI = mysqli_error($db);

					if ( !empty($processErrorPI) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_raw_material_types.php"."</td><td>".$processErrorPI." near line 61.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}else{
							$id = array();
						while($row = mysqli_fetch_assoc($resultPI)){
							$id[] = $row["id"];
						}
					}
					$db->next_result();
					$resultPI->close();

					############ .............
					if( !in_array($rmTypeID, $id, TRUE) ){
						error_log("<tr><td>".date("F d, Y H:i:s")."</td><td>new_raw_material_types.php</td><td>The user tries to edit a non-existing property_id.</td></tr>", 3, "errors.php");
						header("location: error_message.html");
					}

				}
				else{

					echo "<title>Raw Material Types - Add</title>";

				}
			}
		?>
	</head>
	<body>
		<form method="post" action="process_new_raw_material_types.php">
			<?php
				require("/include/header.php");
				require("/include/init_unset_values/raw_material_types_init_value.php");

				if ( $rmTypeID ){
					if( $_SESSION["edit_rm_type"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}
				}else{	
					if( $_SESSION["add_rm_type"] == false) 
					{
						$_SESSION["ERRMSG_ARR"] ="Access denied!";
						session_write_close();
						header("Location:comsys.php");
						exit();
					}				
				}

			?>
			<div class="wrapper">

				<span> <h3> <?php echo ( $rmTypeID ? "Edit ".$code : "New Raw Material Types" ) ;?> </h3> </span>

				<?php
					if( isset($_SESSION["ERRMSG_ARR"]) && is_array($_SESSION["ERRMSG_ARR"])) {
						echo "<ul class='err'>";

						foreach($_SESSION["ERRMSG_ARR"] as $msg) {
							echo "<li>".$msg."</li>"; 
						}

						echo "</ul>";

						unset($_SESSION["ERRMSG_ARR"]);
					}
				?>

				<table class="parent_tables_form">
					<tr>
						<td>Code:</td>
						<td>
							<input type="text" name="txtRMCode" value="<?php echo ($rmTypeID ? $code : $initRMTNewSupplyType); ?>">
						</td>
					</tr>
					<tr>
						<td valign="top">Description:</td>
						<td>
							<textarea name="txtRMDescription" ><?php
								if ( $rmTypeID ){
									echo $description;
								}else{
									echo $initRMTRMDescription;
								}
							?></textarea>
						</td>
					</tr>
					<tr>
						<td>Applicable To:</td>
						<td>

							<input type="checkbox" name="chkCompounds" id="Compounds" <?php echo ( $rmTypeID ? ( $db_compounds == 1 ? "checked" : "" ) : ( $initRMTCompounds == 1 ? "checked" : "" ) ); ?> > 
								<label for="Compounds">Compounds</label>

							<input type="checkbox" name="chkPipes" id="Pipes" <?php echo ( $rmTypeID ? ( $db_pipes == 1 ? "checked" : "" ) : ( $initRMTPipes == 1 ? "checked" : "" ) ); ?> > 
								<label for="Pipes">Pipes</label>

							<input type="checkbox" name="chkCorporate" id="Corporate" <?php echo ( $rmTypeID ? ( $db_corporate == 1 ? "checked" : "" ) : ( $initRMTCorporate == 1 ? "checked" : "" ) ); ?> > 
								<label for="Corporate">Corporate</label>

							<input type="checkbox" name="chkPPR" id="PPR" <?php echo ( $rmTypeID ? ( $db_ppr == 1 ? "checked" : "" ) : ( $initRMTPPR == 1 ? "checked" : "" ) ); ?> > 
								<label for="PPR">PPR</label>

						</td>
					</tr>
					<tr class="align_bottom">
						<td>
							<?php
								$page = $_SESSION["page"];
								$search = htmlspecialchars($_SESSION["search"]);
								$qsone = htmlspecialchars($_SESSION["qsone"]);
								$search = ( (strpos(($search), "\\")+1) > 0 ? str_replace("\\", "", $search) : $search );
								$search = ( (strpos(($search), "'")+1) > 0 ? str_replace("'", "\'", $search) : $search );
								$qsone = ( (strpos(($qsone), "\\")+1) > 0 ? str_replace("\\", "", $qsone) : $qsone );
								$qsone = ( (strpos(($qsone), "'")+1) > 0 ? str_replace("'", "\'", $qsone) : $qsone );
							?>
							<input type="submit" name="btnSaveMonitoring" value="Save">	
							<input type="button" name="btnCancel" value="Cancel" onclick="location.href='raw_material_types.php?page=<?php echo $page;?>&search=<?php echo $search;?>&qsone=<?php echo $qsone;?>'">
							<input type="hidden" name="hidRMTypeID" value="<?php echo ( $rmTypeID ); ?>">
							<input type="hidden" name="hidCreatedAt" value="<?php echo ( $rmTypeID ? $created_at : date("Y-m-d H:i:s") ); ?>">
							<input type="hidden" name="hidCreatedId" value="<?php echo ( $rmTypeID ? $created_id : 0 ); ?>">
						</td>
					</tr>
				</table>

			</div>
		</form>
	</body>
	<footer>
		<?php	
			require("include/database_close.php");
		?>
	</footer>
</html>